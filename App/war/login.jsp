<%
String errorMsg = "";
String err = request.getParameter("err");
if (err != null && err.equals("1"))
	errorMsg = "Login failed. Please try again.";
%>
<html>
<title>Display Production System | Avery</title>
<body style="font-family: Verdana, Geneva, sans-serif; font-weight: normal; font-style: normal; font-size: 12px; color: #666; text-align: center;">
<form action="j_spring_security_check">
<div id="container" style="width: 927px; margin: 30px 20px;">
  <div id="hd"      style="background-image:url(images/bg_927x14_roundedTop.gif); height: 14px;"></div>
  <div id="bd"      style="background-image:url(images/bg_927x14_roundedMid.gif); background-repeat: repeat-y; padding: 220px 30px 5px 30px; text-align: right;">
    <div id="logo"  style="background-image:url(images/200x78_logo_en_us.png); background-repeat: no-repeat; margin: 0px 0px 5px 0px; padding: 55px 0px 4px 0px; border-bottom: 1px solid #ccc; font-size: 13px; font-weight: bold; color: #069; text-align:right;">
      Display Production System Login Page
    </div>
      <!--| begin show hide of error message |-->
      <span style="color:#F00; font-weight: bold;">
      <%
        if (errorMsg.length() > 0) out.println(errorMsg);
      %>
      </span><br><br>
      <!--| end   show hide of error message |-->
      <label for="j_username">Username: </label>
      <input type="text" name="j_username" id="j_username" />
      <br/>
      <label for="j_password">Password: </label>
      <input type="password" name="j_password" id="j_password"/>
      <br/><br/>
      <input type="submit" value="Login"/>
  </div>
  <div id="ft"      style="background-image:url(images/bg_927x14_roundedBtm.gif); height: 14px"></div>
</div>
    </form>
</body>
</html>