-- Things to note before running
--
-- 1). If running in SQL plus, you will need to enable the backslash (\) as an escape character
-- run this to set it to the default value of backslash: "set escape on"
-- an insert below needs this in order to run.  This may need to be set in whatever tool is used to run this
-- but not all tools will need this.  See my note in the inserts for the location table below.

-- 2). This requires access to the views set up in PIMODS.  The grant statements to the views
-- are in the create user statement below

-- Create user dmmweb
-- leaving in in case it is needed, but commenting out.
-- **NOTE** 
-- Before creating this user, either remove the default tablespace or create it.
/*
CREATE USER DMMWEB
  IDENTIFIED BY VALUES '3653824AEEB4A844'
  DEFAULT TABLESPACE DMM_DAT1
  TEMPORARY TABLESPACE TEMP
  PROFILE DEFAULT
  ACCOUNT UNLOCK;
  -- 2 Roles for DMMWEB 
  GRANT CONNECT TO DMMWEB;
  GRANT RESOURCE TO DMMWEB;
  ALTER USER DMMWEB DEFAULT ROLE ALL;
  -- 1 System Privilege for DMMWEB 
  GRANT UNLIMITED TABLESPACE TO DMMWEB;
  -- 3 Object Privileges for DMMWEB 
    GRANT SELECT ON PIMODS.V_DMM_PRODUCT_DETAIL TO DMMWEB;
    GRANT SELECT ON PIMODS.V_DMM_PRODUCT_PLANNER TO DMMWEB;
    GRANT SELECT ON PIMODS.V_DMM_VALID_LOCATION TO DMMWEB;
*/

set escape on

/*################################
 Create Tables
################################*/

CREATE TABLE LOCATION
(
  LOCATION_ID  NUMBER(10),
  DC_CODE      VARCHAR2(2 BYTE),
  DC_NAME      VARCHAR2(50 BYTE),
  ACTIVE_FLG   VARCHAR2(1 BYTE)                 DEFAULT 'N'
);


CREATE TABLE MANUFACTURER
(
  MANUFACTURER_ID  NUMBER(10),
  NAME             VARCHAR2(100 BYTE)
);


CREATE TABLE USERS
(
  USER_ID          NUMBER(10),
  USERNAME         VARCHAR2(100 BYTE),
  PASSWORD         VARCHAR2(50 BYTE),
  FIRST_NAME       VARCHAR2(50 BYTE),
  LAST_NAME        VARCHAR2(25 BYTE),
  EMAIL_ADDRESS    VARCHAR2(200 BYTE),
  CREATE_DATE      DATE,
  LAST_LOGIN_DATE  DATE
);


CREATE TABLE ACCESS_PRIV
(
  ACCESS_ID           NUMBER(10),
  ACCESS_DESCRIPTION  VARCHAR2(200 BYTE)
);


CREATE TABLE USER_CHANGE_LOG
(
  USER_CHANGE_LOG_ID  NUMBER(10),
  USERNAME            VARCHAR2(100 BYTE),
  DT_CHANGED          DATE,
  TABLE_NAME          VARCHAR2(30 BYTE),
  FIELD               VARCHAR2(50 BYTE),
  FRIENDLY_NAME       VARCHAR2(50 BYTE),
  SHORT_NAME          VARCHAR2(50 BYTE),
  OLD_VALUE           VARCHAR2(200 BYTE),
  NEW_VALUE           VARCHAR2(200 BYTE),
  DISPLAY_ID          NUMBER(10),
  CHANGE_TYPE         VARCHAR2(30 BYTE),
  PRIMARY_KEY_ID      NUMBER(10)
);


CREATE TABLE CUSTOMER
(
  CUSTOMER_ID           NUMBER(10),
  CUSTOMER_NAME         VARCHAR2(25 BYTE),
  CUSTOMER_DESCRIPTION  VARCHAR2(200 BYTE)
);


CREATE TABLE DISPLAY_SALES_REP
(
  DISPLAY_SALES_REP_ID  NUMBER(10),
  FIRST_NAME            VARCHAR2(25 BYTE),
  LAST_NAME             VARCHAR2(25 BYTE),
  REP_CODE              VARCHAR2(5 BYTE)
);


CREATE TABLE STATUS
(
  STATUS_ID    NUMBER(10),
  STATUS_NAME  VARCHAR2(25 BYTE),
  DESCRIPTION  VARCHAR2(200 BYTE)
);


CREATE TABLE PROMO_PERIOD
(
  PROMO_PERIOD_ID           NUMBER(10),
  PROMO_PERIOD_NAME         VARCHAR2(25 BYTE),
  PROMO_PERIOD_DESCRIPTION  VARCHAR2(200 BYTE),
  PROMO_DATE                DATE
);


CREATE TABLE STRUCTURE
(
  STRUCTURE_ID    NUMBER(10),
  STRUCTURE_NAME  VARCHAR2(50 BYTE),
  STRUCTURE_DESC  VARCHAR2(200 BYTE)
);


CREATE TABLE DISPLAY_SETUP
(
  DISPLAY_SETUP_ID    NUMBER(10),
  DISPLAY_SETUP_DESC  VARCHAR2(200 BYTE)
);


CREATE TABLE USER_ACCESS
(
  USER_ID    NUMBER(10),
  ACCESS_ID  NUMBER(10)
);


CREATE TABLE DISPLAY
(
  DISPLAY_ID            NUMBER(10),
  SKU                   VARCHAR2(15 BYTE),
  DESCRIPTION           VARCHAR2(200 BYTE),
  CUSTOMER_ID           NUMBER(10),
  STATUS_ID             NUMBER(10),
  QTY_FORECAST          NUMBER(10),
  PROMO_PERIOD_ID       NUMBER(10),
  DISPLAY_SALES_REP_ID  NUMBER(10),
  DESTINATION           VARCHAR2(10 BYTE),
  STRUCTURE_ID          NUMBER(10),
  QSI_DOC_NUMBER        VARCHAR2(50 BYTE),
  SKU_MIX_FINAL_FLG     VARCHAR2(1 BYTE)        DEFAULT 'N',
  MANUFACTURER_ID       NUMBER(10),
  DISPLAY_SETUP_ID      NUMBER(10),
  PROMO_YEAR            NUMBER(10),
  CUSTOMER_DISPLAY#     VARCHAR2(30 BYTE),
  DT_CREATED            DATE,
  DT_LAST_CHANGED       DATE,
  USER_CREATED          VARCHAR2(100 BYTE),
  USER_LAST_CHANGED     VARCHAR2(100 BYTE)
);


CREATE TABLE DISPLAY_LOGISTICS
(
  DISPLAY_LOGISTICS_ID  NUMBER(10),
  DISPLAY_ID            NUMBER(10),
  DISPLAY_LENGTH        NUMBER(7,3),
  DISPLAY_WIDTH         NUMBER(7,3),
  DISPLAY_HEIGHT        NUMBER(7,3),
  DISPLAY_WEIGHT        NUMBER(7,3),
  DISPLAY_PER_LAYER     NUMBER(10),
  LAYERS_PER_PALLET     NUMBER(10),
  PALLETS_PER_TRAILER   NUMBER(10),
  DOUBLE_STACK_FLG      VARCHAR2(1 BYTE)        DEFAULT 'N',
  SPECIAL_LABEL_RQT     VARCHAR2(25 BYTE),
  SERIAL_SHIP_FLG       VARCHAR2(1 BYTE)        DEFAULT 'N',
  UNITS                 VARCHAR2(10 BYTE),
  DT_CREATED            DATE,
  DT_LAST_CHANGED       DATE,
  USER_CREATED          VARCHAR2(25 BYTE),
  USER_LAST_CHANGED     VARCHAR2(25 BYTE)
);


CREATE TABLE KIT_COMPONENT
(
  KIT_COMPONENT_ID      NUMBER(10),
  DISPLAY_ID            NUMBER(10),
  SKU_ID                NUMBER(10),
  QTY_PER_FACING        NUMBER(10),
  NUMBER_FACINGS        NUMBER(10),
  PLANT_BREAK_CASE_FLG  VARCHAR2(1 BYTE)        DEFAULT 'N',
  I2_FORECAST_FLG       VARCHAR2(1 BYTE)        DEFAULT 'N',
  REG_CASE_PACK         NUMBER(10),
  INVOICE_UNIT_PRICE    NUMBER(7,2),
  COGS_UNIT_PRICE       NUMBER(7,2),
  SEQUENCE_NUM          NUMBER(10),
  SOURCE_PIM_FLG        VARCHAR2(1 BYTE)        DEFAULT 'N',
  SKU                   VARCHAR2(15 BYTE),
  PRODUCT_NAME          VARCHAR2(100 BYTE),
  BU_DESC               VARCHAR2(50 BYTE),
  VERSION_NO            VARCHAR2(5 BYTE),
  PROMO_FLG             VARCHAR2(1 BYTE)        DEFAULT 'N',
  BREAK_CASE            VARCHAR2(20 BYTE),
  KIT_VERSION_CODE      VARCHAR2(20 BYTE),
  PBC_VERSION           VARCHAR2(5 BYTE),
  FINISHED_GOODS_SKU    VARCHAR2(15 BYTE),
  FINISHED_GOODS_QTY    NUMBER(10),
  DT_CREATED            DATE,
  DT_LAST_CHANGED       DATE,
  USER_CREATED          VARCHAR2(100 BYTE),
  USER_LAST_CHANGED     VARCHAR2(100 BYTE)
);


CREATE TABLE CUSTOMER_MARKETING
(
  CUSTOMER_MARKETING_ID    NUMBER(10),
  DISPLAY_ID               NUMBER(10),
  PRICE_AVAILABILITY_DATE  DATE,
  SENT_SAMPLE_DATE         DATE,
  FINAL_ARTWORK_DATE       DATE,
  PIM_COMPLETED_DATE       DATE,
  PRICING_ADMIN_DATE       DATE,
  INITIAL_RENDERING_FLG    VARCHAR2(1 BYTE)     DEFAULT 'N',
  STRUCTURE_APPROVED_FLG   VARCHAR2(1 BYTE)     DEFAULT 'N',
  ORDER_IN_FLG             VARCHAR2(1 BYTE)     DEFAULT 'N',
  PROJECT_LEVEL_PCT        NUMBER(4),
  COMMENTS                 VARCHAR2(200 BYTE),
  DT_CREATED               DATE,
  DT_LAST_CHANGED          DATE,
  USER_CREATED             VARCHAR2(100 BYTE),
  USER_LAST_CHANGED        VARCHAR2(100 BYTE)
);


CREATE TABLE DISPLAY_COST
(
  DISPLAY_COST_ID        NUMBER(10),
  DISPLAY_ID             NUMBER(10),
  DISPLAY_MATERIAL_COST  NUMBER(8,2),
  FULLFILMENT_COST       NUMBER(8,2),
  CTP_COST               NUMBER(8,2),
  DIE_CUTTING_COST       NUMBER(8,2),
  PRINTING_PLATE_COST    NUMBER(8,2),
  TOTAL_ARTWORK_COST     NUMBER(8,2),
  SHIP_TEST_COST         NUMBER(8,2),
  PALLET_COST            NUMBER(8,2),
  MISC_COST              NUMBER(8,2),
  CORRUGATE_COST         NUMBER(8,2),
  OVERAGE_SCRAP_PCT      NUMBER(4),
  CUSTOMER_PROGRAM_PCT   NUMBER(4),
  CM_PROJECT_NUM         VARCHAR2(40 BYTE),
  SSCID_PROJECT_NUM      VARCHAR2(20 BYTE),
  COST_CENTER            VARCHAR2(40 BYTE),
  DT_CREATED             DATE,
  DT_LAST_CHANGED        DATE,
  USER_CREATED           VARCHAR2(100 BYTE),
  USER_LAST_CHANGED      VARCHAR2(100 BYTE)
);


CREATE TABLE DISPLAY_SHIP_WAVE
(
  DISPLAY_SHIP_WAVE_ID  NUMBER(10),
  DISPLAY_ID            NUMBER(10),
  MUST_ARRIVE_DATE      DATE,
  QUANTITY              NUMBER(10),
  SHIP_WAVE_NUM         NUMBER(10),
  DESTINATION_FLG       VARCHAR2(1 BYTE)        DEFAULT 'N',
  DT_CREATED            DATE,
  DT_LAST_CHANGED       DATE,
  USER_CREATED          VARCHAR2(100 BYTE),
  USER_LAST_CHANGED     VARCHAR2(100 BYTE)
);


CREATE TABLE DISPLAY_MATERIAL
(
  DISPLAY_MATERIAL_ID  NUMBER(10),
  DISPLAY_ID           NUMBER(10),
  VENDOR_PART_NUM      VARCHAR2(20 BYTE),
  GLOVIA_PART          VARCHAR2(20 BYTE),
  DESCRIPTION          VARCHAR2(200 BYTE),
  MANUFACTURER_ID      NUMBER(10),
  UNIT_COST            NUMBER(7,2),
  DELIVERY_DATE        DATE,
  QUANTITY             NUMBER(10),
  SEQUENCE_NUM         NUMBER(10),
  DT_CREATED           DATE,
  DT_LAST_CHANGED      DATE,
  USER_CREATED         VARCHAR2(100 BYTE),
  USER_LAST_CHANGED    VARCHAR2(100 BYTE)
);


CREATE TABLE DISPLAY_DC
(
  DISPLAY_DC_ID      NUMBER(10),
  DISPLAY_ID         NUMBER(10),
  LOCATION_ID        NUMBER(10),
  QUANTITY           NUMBER(10),
  DT_CREATED         DATE,
  DT_LAST_CHANGED    DATE,
  USER_CREATED       VARCHAR2(100 BYTE),
  USER_LAST_CHANGED  VARCHAR2(100 BYTE)
);


CREATE TABLE DISPLAY_PLANNER
(
  DISPLAY_PLANNER_ID  NUMBER(10),
  KIT_COMPONENT_ID    NUMBER(10),
  FIRST_NAME          VARCHAR2(25 BYTE),
  LAST_NAME           VARCHAR2(25 BYTE),
  INITIALS            VARCHAR2(5 BYTE),
  PLANNER_CODE        VARCHAR2(5 BYTE),
  PLANT_CODE          VARCHAR2(50 BYTE),
  LEAD_TIME           VARCHAR2(3 BYTE),
  PLANT_OVERRIDE_FLG  VARCHAR2(1 BYTE)          DEFAULT 'N',
  DISPLAY_DC_ID       NUMBER(10),
  DT_CREATED          DATE,
  DT_LAST_CHANGED     DATE,
  USER_CREATED        VARCHAR2(100 BYTE),
  USER_LAST_CHANGED   VARCHAR2(100 BYTE)
);


CREATE TABLE DISPLAY_PACKOUT
(
  DISPLAY_PACKOUT_ID    NUMBER(10),
  DISPLAY_SHIP_WAVE_ID  NUMBER(10),
  DISPLAY_DC_ID         NUMBER(10),
  SHIP_FROM_DATE        DATE,
  PRODUCT_DUE_DATE      DATE,
  QUANTITY              NUMBER(10),
  DT_CREATED            DATE,
  DT_LAST_CHANGED       DATE,
  USER_CREATED          VARCHAR2(100 BYTE),
  USER_LAST_CHANGED     VARCHAR2(100 BYTE)
);

CREATE TABLE VALID_PLANNER
(
  PLANNER_CODE  VARCHAR2(5 BYTE),
  PLANNER_DESC  VARCHAR2(50 BYTE),
  ACTIVE_FLG    VARCHAR2(1 BYTE)
);

CREATE TABLE VALID_PLANT
(
  PLANT_CODE             VARCHAR2(2 BYTE),
  PLANT_DESC             VARCHAR2(50 BYTE),
  ACTIVE_FLG             VARCHAR2(1 BYTE),
  ACTIVE_ON_FINANCE_FLG  VARCHAR2(1 BYTE)
);

CREATE TABLE valid_bu
(
  bu_code                VARCHAR2(3 BYTE),
  bu_desc                VARCHAR2(50 BYTE),
  active_flg             varchar2(1 BYTE),
  product_category_code  VARCHAR2(10 BYTE)
);

ALTER TABLE valid_bu ADD (
  CONSTRAINT pk_valid_bu
 PRIMARY KEY
 (bu_code));

ALTER TABLE LOCATION ADD (
  CONSTRAINT LOCATION_PK
 PRIMARY KEY
 (LOCATION_ID));


ALTER TABLE MANUFACTURER ADD (
  CONSTRAINT MANUFACTURER_PK
 PRIMARY KEY
 (MANUFACTURER_ID));


ALTER TABLE USERS ADD (
  CONSTRAINT USER_PK
 PRIMARY KEY
 (USER_ID));


ALTER TABLE ACCESS_PRIV ADD (
  CONSTRAINT ACCESS_PK
 PRIMARY KEY
 (ACCESS_ID));


ALTER TABLE USER_CHANGE_LOG ADD (
  CONSTRAINT USER_CHANGE_LOG_PK
 PRIMARY KEY
 (USER_CHANGE_LOG_ID));


ALTER TABLE CUSTOMER ADD (
  CONSTRAINT CUSTOMER_PK
 PRIMARY KEY
 (CUSTOMER_ID));


ALTER TABLE DISPLAY_SALES_REP ADD (
  CONSTRAINT DISPLAY_SALES_REP_PK
 PRIMARY KEY
 (DISPLAY_SALES_REP_ID));


ALTER TABLE STATUS ADD (
  CONSTRAINT STATUS_PK
 PRIMARY KEY
 (STATUS_ID));


ALTER TABLE PROMO_PERIOD ADD (
  CONSTRAINT PROMO_PERIOD_PK
 PRIMARY KEY
 (PROMO_PERIOD_ID));


ALTER TABLE STRUCTURE ADD (
  PRIMARY KEY
 (STRUCTURE_ID));


ALTER TABLE DISPLAY_SETUP ADD (
  CONSTRAINT DISPLAY_SETUP_PK
 PRIMARY KEY
 (DISPLAY_SETUP_ID));


ALTER TABLE USER_ACCESS ADD (
  CONSTRAINT USER_ACCESS_PK
 PRIMARY KEY
 (USER_ID, ACCESS_ID));


ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_PK
 PRIMARY KEY
 (DISPLAY_ID));


ALTER TABLE DISPLAY_LOGISTICS ADD (
  CONSTRAINT DISPLAY_LOGISTICS_PK
 PRIMARY KEY
 (DISPLAY_LOGISTICS_ID));


ALTER TABLE KIT_COMPONENT ADD (
  CONSTRAINT KIT_COMPONENT_PK
 PRIMARY KEY
 (KIT_COMPONENT_ID));


ALTER TABLE CUSTOMER_MARKETING ADD (
  CONSTRAINT CUSTOMER_MARKETING_PK
 PRIMARY KEY
 (CUSTOMER_MARKETING_ID));


ALTER TABLE DISPLAY_COST ADD (
  CONSTRAINT DISPLAY_COST_PK
 PRIMARY KEY
 (DISPLAY_COST_ID));


ALTER TABLE DISPLAY_SHIP_WAVE ADD (
  CONSTRAINT DISPLAY_SHIP_WAVE_PK
 PRIMARY KEY
 (DISPLAY_SHIP_WAVE_ID));


ALTER TABLE DISPLAY_MATERIAL ADD (
  CONSTRAINT DISPLAY_MATERIAL_PK
 PRIMARY KEY
 (DISPLAY_MATERIAL_ID));


ALTER TABLE DISPLAY_DC ADD (
  CONSTRAINT DISPLAY_DC_PK
 PRIMARY KEY
 (DISPLAY_DC_ID));


ALTER TABLE DISPLAY_PLANNER ADD (
  CONSTRAINT DISPLAY_PLANNER_PK
 PRIMARY KEY
 (DISPLAY_PLANNER_ID));


ALTER TABLE DISPLAY_PACKOUT ADD (
  CONSTRAINT DISPLAY_PACKOUT_PK
 PRIMARY KEY
 (DISPLAY_PACKOUT_ID));


ALTER TABLE USER_ACCESS ADD (
  CONSTRAINT USER_ACCESS_USER_ID_FK 
 FOREIGN KEY (USER_ID) 
 REFERENCES USERS (USER_ID));

ALTER TABLE USER_ACCESS ADD (
  CONSTRAINT USER_ACCESS_ACCESS_ID_FK 
 FOREIGN KEY (ACCESS_ID) 
 REFERENCES ACCESS_PRIV (ACCESS_ID));


ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_CUSTOMER_ID_FK 
 FOREIGN KEY (CUSTOMER_ID) 
 REFERENCES CUSTOMER (CUSTOMER_ID));

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_STATUS_ID_FK 
 FOREIGN KEY (STATUS_ID) 
 REFERENCES STATUS (STATUS_ID));

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_PROMO_PERIOD_FK 
 FOREIGN KEY (PROMO_PERIOD_ID) 
 REFERENCES PROMO_PERIOD (PROMO_PERIOD_ID));

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_SALES_REP_ID_FK 
 FOREIGN KEY (DISPLAY_SALES_REP_ID) 
 REFERENCES DISPLAY_SALES_REP (DISPLAY_SALES_REP_ID));

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_STRUCTURE_ID_FK 
 FOREIGN KEY (STRUCTURE_ID) 
 REFERENCES STRUCTURE (STRUCTURE_ID));

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_MANUFACTURER_FK 
 FOREIGN KEY (MANUFACTURER_ID) 
 REFERENCES MANUFACTURER (MANUFACTURER_ID));

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_SETUP_FK 
 FOREIGN KEY (DISPLAY_SETUP_ID) 
 REFERENCES DISPLAY_SETUP (DISPLAY_SETUP_ID));


ALTER TABLE DISPLAY_LOGISTICS ADD (
  CONSTRAINT DISPLAY_LOGISTICS_DISP_ID_FK 
 FOREIGN KEY (DISPLAY_ID) 
 REFERENCES DISPLAY (DISPLAY_ID));


ALTER TABLE KIT_COMPONENT ADD (
  CONSTRAINT KIT_COMPONENT_DISPLAY_FK 
 FOREIGN KEY (DISPLAY_ID) 
 REFERENCES DISPLAY (DISPLAY_ID));


ALTER TABLE CUSTOMER_MARKETING ADD (
  CONSTRAINT CUSTOMER_MARKETING_DISPLAY_FK 
 FOREIGN KEY (DISPLAY_ID) 
 REFERENCES DISPLAY (DISPLAY_ID));


ALTER TABLE DISPLAY_COST ADD (
  CONSTRAINT DISPLAY_COST_DISPLAY_ID_FK 
 FOREIGN KEY (DISPLAY_ID) 
 REFERENCES DISPLAY (DISPLAY_ID));


ALTER TABLE DISPLAY_SHIP_WAVE ADD (
  CONSTRAINT DISPLAY_SHIP_WAVE_ID_FK 
 FOREIGN KEY (DISPLAY_ID) 
 REFERENCES DISPLAY (DISPLAY_ID));


ALTER TABLE DISPLAY_MATERIAL ADD (
  CONSTRAINT DISPLAY_MATERIAL_DISPLAY_FK 
 FOREIGN KEY (DISPLAY_ID) 
 REFERENCES DISPLAY (DISPLAY_ID));

ALTER TABLE DISPLAY_MATERIAL ADD (
  CONSTRAINT DISPLAY_MATERIAL_MANUFA_FK 
 FOREIGN KEY (MANUFACTURER_ID) 
 REFERENCES MANUFACTURER (MANUFACTURER_ID));


ALTER TABLE DISPLAY_DC ADD (
  CONSTRAINT DISPLAY_DC_DISP_ID_FK 
 FOREIGN KEY (DISPLAY_ID) 
 REFERENCES DISPLAY (DISPLAY_ID));

ALTER TABLE DISPLAY_DC ADD (
  CONSTRAINT DISPLAY_DC_LOC_FK 
 FOREIGN KEY (LOCATION_ID) 
 REFERENCES LOCATION (LOCATION_ID));


ALTER TABLE DISPLAY_PLANNER ADD (
  CONSTRAINT DISPLAY_DC_ID_FK 
 FOREIGN KEY (DISPLAY_DC_ID) 
 REFERENCES DISPLAY_DC (DISPLAY_DC_ID));


ALTER TABLE DISPLAY_PACKOUT ADD (
  CONSTRAINT DISPLAY_PACKOUT_DC_FK 
 FOREIGN KEY (DISPLAY_DC_ID) 
 REFERENCES DISPLAY_DC (DISPLAY_DC_ID));

ALTER TABLE DISPLAY_PACKOUT ADD (
  CONSTRAINT DISPLAY_PACKOUT_WAVE_FK 
 FOREIGN KEY (DISPLAY_SHIP_WAVE_ID) 
 REFERENCES DISPLAY_SHIP_WAVE (DISPLAY_SHIP_WAVE_ID));
 
CREATE INDEX user_change_log_date_indx ON user_change_log(dt_changed);
CREATE INDEX user_change_log_table_indx ON user_change_log(table_name);
CREATE INDEX user_change_log_field_indx ON user_change_log(FIELD);
CREATE INDEX usr_change_log_display_id_indx ON user_change_log(display_id);


/*################################
 Create Sequences
################################*/


CREATE SEQUENCE ACCESS_PRIV_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 5
  NOORDER;


CREATE SEQUENCE CUSTOMER_MARKETING_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 50
  NOORDER;


CREATE SEQUENCE CUSTOMER_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 5
  NOORDER;


CREATE SEQUENCE DISPLAY_COST_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 50
  NOORDER;


CREATE SEQUENCE DISPLAY_DC_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 10
  NOORDER;


CREATE SEQUENCE DISPLAY_LOGISTICS_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 50
  NOORDER;


CREATE SEQUENCE DISPLAY_MATERIAL_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 50
  NOORDER;


CREATE SEQUENCE DISPLAY_PACKOUT_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 50
  NOORDER;


CREATE SEQUENCE DISPLAY_PLANNER_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 10
  NOORDER;


CREATE SEQUENCE DISPLAY_SALES_REP_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 10
  NOORDER;


CREATE SEQUENCE DISPLAY_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 50
  NOORDER;


CREATE SEQUENCE DISPLAY_SETUP_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 5
  NOORDER;


CREATE SEQUENCE DISPLAY_SHIP_WAVE_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 50
  NOORDER;


CREATE SEQUENCE KIT_COMPONENT_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 50
  NOORDER;


CREATE SEQUENCE LOCATION_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 10
  NOORDER;


CREATE SEQUENCE MANUFACTURER_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 10
  NOORDER;


CREATE SEQUENCE PROMO_PERIOD_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 10
  NOORDER;


CREATE SEQUENCE STATUS_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 10
  NOORDER;


CREATE SEQUENCE STRUCTURE_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 10
  NOORDER;


CREATE SEQUENCE USERS_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 50
  NOORDER;


CREATE SEQUENCE USER_CHANGE_LOG_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 50
  NOORDER;

/*###############################
 Create triggers
###############################*/

CREATE OR REPLACE TRIGGER customer_marketing_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON customer_marketing
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value,
                   new_value, display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'customer_marketing', 'customer_marketing record', 'N/A',
                   'N/A', :NEW.display_id, v_type, :NEW.customer_marketing_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'customer_marketing',
                   'deleting customer_marketing record', NULL, NULL,
                   :OLD.display_id, v_type, :OLD.customer_marketing_id
                  );
   ELSIF UPDATING
   THEN
      IF :NEW.price_availability_date <> :OLD.price_availability_date
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name,
                      FIELD,
                      friendly_name, short_name,
                      old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing',
                      'price_availability_date',
                      'Price Availability Completed On', 'Price Avail Dt',
                      :OLD.price_availability_date,
                      :NEW.price_availability_date, :OLD.display_id, v_type,
                      :OLD.customer_marketing_id
                     );
      ELSIF :NEW.sent_sample_date <> :OLD.sent_sample_date
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'sent_sample_date',
                      'Sample to Customer By', 'Sample Dt',
                      :OLD.sent_sample_date, :NEW.sent_sample_date,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      ELSIF :NEW.final_artwork_date <> :OLD.final_artwork_date
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'final_artwork_date',
                      'Final Artwork \& PO to Vendor By', 'Final Artwk',
                      :OLD.final_artwork_date, :NEW.final_artwork_date,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      ELSIF :NEW.pim_completed_date <> :OLD.pim_completed_date
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'pim_completed_date',
                      'PIM Completed On', 'PIM Complete',
                      :OLD.pim_completed_date, :NEW.pim_completed_date,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      ELSIF :NEW.pricing_admin_date <> :OLD.pricing_admin_date
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'pricing_admin_date',
                      'Pricing Admin Validation', 'Pricing Admin',
                      :OLD.pricing_admin_date, :NEW.pricing_admin_date,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      ELSIF :NEW.initial_rendering_flg <> :OLD.initial_rendering_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name,
                      FIELD,
                      friendly_name,
                      short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing',
                      'initial_rendering_flg',
                      'Initial Rendering and Est Quote',
                      'Init Rend \& Quote', :OLD.initial_rendering_flg,
                      :NEW.initial_rendering_flg, :OLD.display_id, v_type,
                      :OLD.customer_marketing_id
                     );
      ELSIF :NEW.structure_approved_flg <> :OLD.structure_approved_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name,
                      FIELD, friendly_name,
                      short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing',
                      'structure_approved_flg', 'Structure Approved',
                      'Struct Appr', :OLD.structure_approved_flg,
                      :NEW.structure_approved_flg, :OLD.display_id, v_type,
                      :OLD.customer_marketing_id
                     );
      ELSIF :NEW.order_in_flg <> :OLD.order_in_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'order_in_flg',
                      'Orders Received', 'Order In', :OLD.order_in_flg,
                      :NEW.order_in_flg, :OLD.display_id, v_type,
                      :OLD.customer_marketing_id
                     );
      ELSIF :NEW.project_level_pct <> :OLD.project_level_pct
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'project_level_pct',
                      'Project Confidence Level', 'Prj Lvl',
                      :OLD.project_level_pct, :NEW.project_level_pct,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      ELSIF :NEW.comments <> :OLD.comments
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'comments', 'Comments',
                      'Comments', :OLD.comments, :NEW.comments,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      END IF;
   END IF;
END;
/


CREATE OR REPLACE TRIGGER display_cost_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON display_cost
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_cost', 'new display_cost record', 'N/A', 'N/A',
                   :NEW.display_id, v_type, :NEW.display_cost_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name, FIELD,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_cost', 'delete display_cost_record',
                   'N/A', 'N/A', :OLD.display_id, v_type,
                   :OLD.display_cost_id
                  );
   ELSIF UPDATING
   THEN
      IF :NEW.display_material_cost <> :OLD.display_material_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'display_material_cost',
                      :OLD.display_material_cost,
                      :NEW.display_material_cost, :OLD.display_id, v_type,
                      :OLD.display_cost_id
                     );
      ELSIF :NEW.fullfilment_cost <> :OLD.fullfilment_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'fullfilment_cost',
                      :OLD.fullfilment_cost, :NEW.fullfilment_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.ctp_cost <> :OLD.ctp_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'ctp_cost', :OLD.ctp_cost,
                      :NEW.ctp_cost, :OLD.display_id, v_type,
                      :OLD.display_cost_id
                     );
      ELSIF :NEW.die_cutting_cost <> :OLD.die_cutting_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'die_cutting_cost',
                      :OLD.die_cutting_cost, :NEW.die_cutting_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.printing_plate_cost <> :OLD.printing_plate_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'printing_plate_cost',
                      :OLD.printing_plate_cost, :NEW.printing_plate_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.total_artwork_cost <> :OLD.total_artwork_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'total_artwork_cost',
                      :OLD.total_artwork_cost, :NEW.total_artwork_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.ship_test_cost <> :OLD.ship_test_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'ship_test_cost',
                      :OLD.ship_test_cost, :NEW.ship_test_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.pallet_cost <> :OLD.pallet_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'pallet_cost',
                      :OLD.pallet_cost, :NEW.pallet_cost, :OLD.display_id,
                      v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.misc_cost <> :OLD.misc_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'misc_cost', :OLD.misc_cost,
                      :NEW.misc_cost, :OLD.display_id, v_type,
                      :OLD.display_cost_id
                     );
      ELSIF :NEW.corrugate_cost <> :OLD.corrugate_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'corrugate_cost',
                      :OLD.corrugate_cost, :NEW.corrugate_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.overage_scrap_pct <> :OLD.overage_scrap_pct
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'overage_scrap_pct',
                      :OLD.overage_scrap_pct, :NEW.overage_scrap_pct,
                      :OLD.display_id, v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.customer_program_pct <> :OLD.customer_program_pct
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'customer_program_pct',
                      :OLD.customer_program_pct, :NEW.customer_program_pct,
                      :OLD.display_id, v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.cm_project_num <> :OLD.cm_project_num
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'cm_project_num',
                      :OLD.cm_project_num, :NEW.cm_project_num,
                      :OLD.display_id, v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.sscid_project_num <> :OLD.sscid_project_num
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'sscid_project_num',
                      :OLD.sscid_project_num, :NEW.sscid_project_num,
                      :OLD.display_id, v_type, :OLD.display_cost_id
                     );
      ELSIF :NEW.cost_center <> :OLD.cost_center
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'cost_center',
                      :OLD.cost_center, :NEW.cost_center, :OLD.display_id,
                      v_type, :OLD.display_cost_id
                     );
      END IF;
   END IF;
END;
/


CREATE OR REPLACE TRIGGER display_dc_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON display_dc
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name, short_name,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_dc', 'location_id', 'Pack-Out Location', 'DC',
                   'N/A', :NEW.location_id, :NEW.display_id, v_type,
                   :NEW.display_dc_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed, SYSDATE,
                   'display_dc', 'deleting display_dc record', 'N/A', 'N/A',
                   :OLD.display_id, v_type, :OLD.display_dc_id
                  );
   ELSIF UPDATING
   THEN
      IF :NEW.location_id <> :OLD.location_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display_dc', 'location_id',
                      'Pack-Out Location', 'DC', :OLD.location_id,
                      :NEW.location_id, :OLD.display_id, v_type,
                      :OLD.display_dc_id
                     );
      ELSIF :NEW.quantity <> :OLD.quantity
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display_dc', 'quantity',
                      'Pack-Out Quantity', 'DC Qty', :OLD.quantity,
                      :NEW.quantity, :OLD.display_id, v_type,
                      :OLD.display_dc_id
                     );
      END IF;
   END IF;
END;
/


CREATE OR REPLACE TRIGGER display_logistics_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON display_logistics
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_logistics', 'new display_logistics record',
                   'N/A', 'N/A', :NEW.display_id, v_type,
                   :NEW.display_logistics_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_logistics',
                   'deleting display_logistics_record', 'N/A', 'N/A',
                   :OLD.display_id, v_type, :OLD.display_logistics_id
                  );
   ELSIF UPDATING
   THEN
      IF :NEW.display_length <> :OLD.display_length
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'display_length',
                      'Length(in)', 'L', :OLD.display_length,
                      :NEW.display_length, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      ELSIF :NEW.display_width <> :OLD.display_width
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'display_width',
                      'Width(in)', 'W', :OLD.display_width,
                      :NEW.display_width, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      ELSIF :NEW.display_height <> :OLD.display_height
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'display_height',
                      'Height(in)', 'H', :OLD.display_height,
                      :NEW.display_height, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      ELSIF :NEW.display_weight <> :OLD.display_weight
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'display_weight',
                      'Weight(in)', 'Wt', :OLD.display_weight,
                      :NEW.display_weight, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      ELSIF :NEW.display_per_layer <> :OLD.display_per_layer
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'display_per_layer',
                      'Displays Per Layer', 'Disp/Layer',
                      :OLD.display_per_layer, :NEW.display_per_layer,
                      :OLD.display_id, v_type, :OLD.display_logistics_id
                     );
      ELSIF :NEW.layers_per_pallet <> :OLD.layers_per_pallet
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'layers_per_pallet',
                      'Layers Per Pallet', 'Layers/Pallet',
                      :OLD.layers_per_pallet, :NEW.layers_per_pallet,
                      :OLD.display_id, v_type, :OLD.display_logistics_id
                     );
      ELSIF :NEW.pallets_per_trailer <> :OLD.pallets_per_trailer
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'pallets_per_trailer',
                      'Pallets Per Trailer', 'Pallets/Trailer',
                      :OLD.pallets_per_trailer, :NEW.pallets_per_trailer,
                      :OLD.display_id, v_type, :OLD.display_logistics_id
                     );
      ELSIF :NEW.double_stack_flg <> :OLD.double_stack_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'double_stack_flg',
                      'Double Stack', 'Dbl Stk', :OLD.double_stack_flg,
                      :NEW.double_stack_flg, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      ELSIF :NEW.special_label_rqt <> :OLD.special_label_rqt
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'special_label_rqt',
                      'Placard/Special Label Reqt', 'Spc Label Rqt',
                      :OLD.special_label_rqt, :NEW.special_label_rqt,
                      :OLD.display_id, v_type, :OLD.display_logistics_id
                     );
      ELSIF :NEW.serial_ship_flg <> :OLD.serial_ship_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'serial_ship_flg',
                      'Serial Ship', 'Serial Ship', :OLD.serial_ship_flg,
                      :NEW.serial_ship_flg, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      ELSIF :NEW.units <> :OLD.units
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'units', :OLD.units,
                      :NEW.units, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      END IF;
   END IF;
END;
/


CREATE OR REPLACE TRIGGER display_material_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON display_material
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value,
                   new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_material', 'delivery_date', 'N/A',
                   :NEW.delivery_date, :NEW.display_id, v_type,
                   :NEW.display_material_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed, SYSDATE,
                   'display_material', 'deleting display_material record',
                   'N/A', 'N/A', :OLD.display_id, v_type,
                   :OLD.display_material_id
                  );
   ELSIF UPDATING
   THEN
      IF :NEW.vendor_part_num <> :OLD.vendor_part_num
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'vendor_part_num',
                      :OLD.vendor_part_num, :NEW.vendor_part_num,
                      :OLD.display_id, v_type, :OLD.display_material_id
                     );
      ELSIF :NEW.glovia_part <> :OLD.glovia_part
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'glovia_part',
                      :OLD.glovia_part, :NEW.glovia_part, :OLD.display_id,
                      v_type, :OLD.display_material_id
                     );
      ELSIF :NEW.description <> :OLD.description
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'description',
                      :OLD.description, :NEW.description, :OLD.display_id,
                      v_type, :OLD.display_material_id
                     );
      ELSIF :NEW.manufacturer_id <> :OLD.manufacturer_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'manufacturer_id',
                      :OLD.manufacturer_id, :NEW.manufacturer_id,
                      :OLD.display_id, v_type, :OLD.display_material_id
                     );
      ELSIF :NEW.unit_cost <> :OLD.unit_cost
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'unit_cost',
                      :OLD.unit_cost, :NEW.unit_cost, :OLD.display_id,
                      v_type, :OLD.display_material_id
                     );
      ELSIF :NEW.delivery_date <> :OLD.delivery_date
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'delivery_date',
                      :OLD.delivery_date, :NEW.delivery_date,
                      :OLD.display_id, v_type, :OLD.display_material_id
                     );
      ELSIF :NEW.quantity <> :OLD.quantity
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'quantity',
                      :OLD.quantity, :NEW.quantity, :OLD.display_id, v_type,
                      :OLD.display_material_id
                     );
      ELSIF :NEW.sequence_num <> :OLD.sequence_num
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'sequence_num',
                      :OLD.sequence_num, :NEW.sequence_num, :OLD.display_id,
                      v_type, :OLD.display_material_id
                     );
      END IF;
   END IF;
END;
/


CREATE OR REPLACE TRIGGER display_packout_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON display_packout
   FOR EACH ROW
DECLARE
   v_type         VARCHAR2 (6);
   v_display_id   NUMBER (10);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      SELECT NVL (display_id, 0)
        INTO v_display_id
        FROM display_ship_wave
       WHERE display_ship_wave_id = :NEW.display_ship_wave_id;

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value, display_id,
                   change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_packout', 'ship_from_date', 'Ship From Date',
                   'Ship Date', 'N/A', :NEW.ship_from_date, v_display_id,
                   v_type, :NEW.display_packout_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD,
                   friendly_name, short_name, old_value,
                   new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_packout', 'product_due_date',
                   'Product Due at Packout Date', 'Product Due', 'N/A',
                   :NEW.product_due_date, v_display_id, v_type,
                   :NEW.display_packout_id
                  );
   ELSIF DELETING
   THEN
      SELECT NVL (display_id, 0)
        INTO v_display_id
        FROM display_ship_wave
       WHERE display_ship_wave_id = :OLD.display_ship_wave_id;

      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_packout',
                   'deleting display_packout record', 'N/A', 'N/A',
                   v_display_id, v_type, :OLD.display_packout_id
                  );
   ELSIF UPDATING
   THEN
      SELECT NVL (display_id, 0)
        INTO v_display_id
        FROM display_ship_wave
       WHERE display_ship_wave_id = :OLD.display_ship_wave_id;

      IF :NEW.display_dc_id <> :OLD.display_dc_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout', 'display_dc_id',
                      :OLD.display_dc_id, :NEW.display_dc_id, v_display_id,
                      v_type, :OLD.display_packout_id
                     );
      ELSIF :NEW.ship_from_date <> :OLD.ship_from_date
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout', 'ship_from_date',
                      'Ship From Date', 'Ship Date', :OLD.ship_from_date,
                      :NEW.ship_from_date, v_display_id, v_type,
                      :OLD.display_packout_id
                     );
      ELSIF :NEW.product_due_date <> :OLD.product_due_date
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout', 'product_due_date',
                      'Product Due at Packout Date', 'Product Due',
                      :OLD.product_due_date, :NEW.product_due_date,
                      v_display_id, v_type, :OLD.display_packout_id
                     );
      ELSIF :NEW.quantity <> :OLD.quantity
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout', 'quantity',
                      'Packout Detail Quantity', 'Packout Detail Qty',
                      :OLD.quantity, :NEW.quantity, v_display_id, v_type,
                      :OLD.display_packout_id
                     );
      END IF;
   END IF;
END;
/


CREATE OR REPLACE TRIGGER display_planner_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON display_planner
   FOR EACH ROW
DECLARE
   v_type         VARCHAR2 (6);
   v_display_id   NUMBER (10);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      SELECT nvl(a.display_id, 0)
        INTO v_display_id
        FROM kit_component a
       WHERE a.kit_component_id = :NEW.kit_component_id;

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value,
                   new_value, display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_planner', 'new display_planner record', 'N/A',
                   'N/A', v_display_id, v_type, :NEW.display_planner_id
                  );
   ELSIF DELETING
   THEN
      SELECT nvl(a.display_id, 0)
        INTO v_display_id
        FROM kit_component a
       WHERE a.kit_component_id = :OLD.kit_component_id;

      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_planner',
                   'deleting display_planner record', 'N/A', 'N/A',
                   v_display_id, v_type, :OLD.display_planner_id
                  );
   ELSIF UPDATING
   THEN
      SELECT nvl(a.display_id, 0)
        INTO v_display_id
        FROM kit_component a
       WHERE a.kit_component_id = :OLD.kit_component_id;

      IF :NEW.first_name <> :OLD.first_name
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'first_name',
                      :OLD.first_name, :NEW.first_name, v_display_id,
                      v_type, :OLD.display_planner_id
                     );
      ELSIF :NEW.last_name <> :OLD.last_name
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'last_name',
                      :OLD.last_name, :NEW.last_name, v_display_id, v_type,
                      :OLD.display_planner_id
                     );
      ELSIF :NEW.initials <> :OLD.initials
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'initials', :OLD.initials,
                      :NEW.initials, v_display_id, v_type,
                      :OLD.display_planner_id
                     );
      ELSIF :NEW.planner_code <> :OLD.planner_code
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'planner_code',
                      :OLD.planner_code, :NEW.planner_code, v_display_id,
                      v_type, :OLD.display_planner_id
                     );
      ELSIF :NEW.plant_code <> :OLD.plant_code
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'plant_code',
                      :OLD.plant_code, :NEW.plant_code, v_display_id,
                      v_type, :OLD.display_planner_id
                     );
      ELSIF :NEW.lead_time <> :OLD.lead_time
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'lead_time',
                      :OLD.lead_time, :NEW.lead_time, v_display_id, v_type,
                      :OLD.display_planner_id
                     );
      ELSIF :NEW.plant_override_flg <> :OLD.plant_override_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'plant_override_flg',
                      :OLD.plant_override_flg, :NEW.plant_override_flg,
                      v_display_id, v_type, :OLD.display_planner_id
                     );
      END IF;
   END IF;
END;
/


CREATE OR REPLACE TRIGGER display_ship_wave_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON display_ship_wave
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_ship_wave', 'new display_ship_wave record',
                   'N/A', 'N/A', :NEW.display_id, v_type,
                   :NEW.display_ship_wave_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_ship_wave',
                   'deleting display_ship_wave record', 'N/A', 'N/A',
                   :OLD.display_id, v_type, :OLD.display_ship_wave_id
                  );
   ELSIF UPDATING
   THEN
      IF :NEW.must_arrive_date <> :OLD.must_arrive_date
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_ship_wave', 'must_arrive_date',
                      'Must Arrive Date', 'Must Arrive',
                      :OLD.must_arrive_date, :NEW.must_arrive_date,
                      :OLD.display_id, v_type, :OLD.display_ship_wave_id
                     );
      ELSIF :NEW.quantity <> :OLD.quantity
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_ship_wave', 'quantity',
                      'Ship Wave Quantity', 'SW Qty', :OLD.quantity,
                      :NEW.quantity, :OLD.display_id, v_type,
                      :OLD.display_ship_wave_id
                     );
      ELSIF :NEW.ship_wave_num <> :OLD.ship_wave_num
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_ship_wave', 'ship_wave_num',
                      'Shipping Wave', 'SW', :OLD.ship_wave_num,
                      :NEW.ship_wave_num, :OLD.display_id, v_type,
                      :OLD.display_ship_wave_id
                     );
      ELSIF :NEW.destination_flg <> :OLD.destination_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_ship_wave', 'destination_flg',
                      'Offshore Ind', 'Offshore', :OLD.destination_flg,
                      :NEW.destination_flg, :OLD.display_id, v_type,
                      :OLD.display_ship_wave_id
                     );
      END IF;
   END IF;
END;
/


CREATE OR REPLACE TRIGGER display_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON display
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name, short_name, old_value,
                   new_value, display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display', 'sku', 'Top Level SKU', 'Top SKU', 'N/A',
                   :NEW.sku, :NEW.display_id, v_type, :NEW.display_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display', 'sku_mix_final_flg', 'SKU Mix Final',
                   'SKU Mix Final', 'N/A', :NEW.sku_mix_final_flg,
                   :NEW.display_id, v_type, :NEW.display_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display', 'qty_forecast', 'Display Forecast Quantity',
                   'Qty', 'N/A', :NEW.qty_forecast, :NEW.display_id, v_type,
                   :NEW.display_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name, FIELD, old_value,
                   new_value, display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display', 'deleting display record', 'N/A',
                   'N/A', :OLD.display_id, v_type, :OLD.display_id
                  );
   ELSIF UPDATING
   THEN
      IF :NEW.sku <> :OLD.sku
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'sku', 'Top Level SKU',
                      'Top SKU', :OLD.sku, :NEW.sku, :NEW.display_id,
                      v_type, :OLD.display_id
                     );
      ELSIF :NEW.description <> :OLD.description
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'description', 'Description',
                      'Desc', :OLD.description, :NEW.description,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      ELSIF :NEW.customer_id <> :OLD.customer_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'customer_id', 'Customer',
                      'Cust', :OLD.customer_id, :NEW.customer_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      ELSIF :NEW.status_id <> :OLD.status_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'status_id', :OLD.status_id,
                      :NEW.status_id, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      ELSIF :NEW.qty_forecast <> :OLD.qty_forecast
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'qty_forecast',
                      'Display Forecast Quantity', 'Qty', :OLD.qty_forecast,
                      :NEW.qty_forecast, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      ELSIF :NEW.promo_period_id <> :OLD.promo_period_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'promo_period_id', 'Promo Period',
                      'Period', :OLD.promo_period_id, :NEW.promo_period_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      ELSIF :NEW.display_sales_rep_id <> :OLD.display_sales_rep_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'display_sales_rep_id',
                      'Project Manager', 'PM', :OLD.display_sales_rep_id,
                      :NEW.display_sales_rep_id, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      ELSIF :NEW.destination <> :OLD.destination
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'destination', :OLD.destination,
                      :NEW.destination, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      ELSIF :NEW.structure_id <> :OLD.structure_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'structure_id', 'Structure Type',
                      'Struct', :OLD.structure_id, :NEW.structure_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      ELSIF :NEW.qsi_doc_number <> :OLD.qsi_doc_number
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'qsi_doc_number',
                      'QSI Doc Number', 'QSI', :OLD.qsi_doc_number,
                      :NEW.qsi_doc_number, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      ELSIF :NEW.sku_mix_final_flg <> :OLD.sku_mix_final_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'sku_mix_final_flg',
                      'SKU Mix Final', 'SKU Mix Final',
                      :OLD.sku_mix_final_flg, :NEW.sku_mix_final_flg,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      ELSIF :NEW.manufacturer_id <> :OLD.manufacturer_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'manufacturer_id', 'Manufacturer',
                      'Mfg', :OLD.manufacturer_id, :NEW.manufacturer_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      ELSIF :NEW.display_setup_id <> :OLD.display_setup_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'display_setup_id',
                      'Display Setup Type', 'Display Setup Type',
                      :OLD.display_setup_id, :NEW.display_setup_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      ELSIF :NEW.promo_year <> :OLD.promo_year
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'promo_year', 'Promo Year',
                      'Year', :OLD.promo_year, :NEW.promo_year,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      ELSIF :NEW.customer_display# <> :OLD.customer_display#
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created,
                      SYSDATE, 'display', 'customer_display#',
                      'Customer Display#', 'Cust Disp #',
                      :OLD.customer_display#, :NEW.customer_display#,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;
   END IF;
END;
/


CREATE OR REPLACE TRIGGER kit_component_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON kit_component
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value, new_value, display_id,
                   change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'kit_component', 'sku', 'N/A', :NEW.sku, :NEW.display_id,
                   v_type, :NEW.kit_component_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value, display_id,
                   change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'kit_component', 'kit_version_code', 'Kit Version Code',
                   'KVC', 'N/A', :NEW.kit_version_code, :NEW.display_id,
                   v_type, :NEW.kit_component_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name, short_name,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'kit_component', 'i2_forecast_flg', 'I2 Forecast', 'I2',
                   'N/A', :NEW.i2_forecast_flg, :NEW.display_id, v_type,
                   :NEW.kit_component_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value, display_id,
                   change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'kit_component', 'qty_per_facing', 'Qty Per Facing',
                   'QPF', 'N/A', :NEW.qty_per_facing, :NEW.display_id,
                   v_type, :NEW.kit_component_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name, short_name,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'kit_component', 'number_facings', '# Facings', '#F',
                   'N/A', :NEW.number_facings, :NEW.display_id, v_type,
                   :NEW.kit_component_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'kit_component',
                   'deleting kit_component record', 'N/A', 'N/A',
                   :OLD.display_id, v_type, :OLD.kit_component_id
                  );
   ELSIF UPDATING
   THEN
      IF :NEW.sku_id <> :OLD.sku_id
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'sku_id', :OLD.sku_id,
                      :NEW.sku_id, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.qty_per_facing <> :OLD.qty_per_facing
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'qty_per_facing',
                      'Qty Per Facing', 'QPF', :OLD.qty_per_facing,
                      :NEW.qty_per_facing, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.number_facings <> :OLD.number_facings
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'number_facings',
                      '# Facings', '#F', :OLD.number_facings,
                      :NEW.number_facings, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.plant_break_case_flg <> :OLD.plant_break_case_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'plant_break_case_flg',
                      'PBC BOM', 'PBC BOM', :OLD.plant_break_case_flg,
                      :NEW.plant_break_case_flg, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.i2_forecast_flg <> :OLD.i2_forecast_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'i2_forecast_flg',
                      'I2 Forecast', 'I2', :OLD.i2_forecast_flg,
                      :NEW.i2_forecast_flg, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.reg_case_pack <> :OLD.reg_case_pack
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'reg_case_pack',
                      'Ret Pk/Inner Ctn', 'Ret Pk', :OLD.reg_case_pack,
                      :NEW.reg_case_pack, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.invoice_unit_price <> :OLD.invoice_unit_price
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'invoice_unit_price',
                      'Inv Unit Price', 'Inv Unit', :OLD.invoice_unit_price,
                      :NEW.invoice_unit_price, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.cogs_unit_price <> :OLD.cogs_unit_price
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'cogs_unit_price',
                      'COGS Unit Price', 'COGS', :OLD.cogs_unit_price,
                      :NEW.cogs_unit_price, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.sequence_num <> :OLD.sequence_num
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'sequence_num',
                      :OLD.sequence_num, :NEW.sequence_num, :OLD.display_id,
                      v_type, :OLD.kit_component_id
                     );
      ELSIF :NEW.source_pim_flg <> :OLD.source_pim_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'source_pim_flg',
                      'Source=PIM', 'PIM', :OLD.source_pim_flg,
                      :NEW.source_pim_flg, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.sku <> :OLD.sku
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'sku', 'Component SKU',
                      'Comp SKU', :OLD.sku, :NEW.sku, :OLD.display_id,
                      v_type, :OLD.kit_component_id
                     );
      ELSIF :NEW.product_name <> :OLD.product_name
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'product_name',
                      'Component Desc', 'Desc', :OLD.product_name,
                      :NEW.product_name, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.bu_desc <> :OLD.bu_desc
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'bu_desc', 'Business Unit',
                      'BU', :OLD.bu_desc, :NEW.bu_desc, :OLD.display_id,
                      v_type, :OLD.kit_component_id
                     );
      ELSIF :NEW.version_no <> :OLD.version_no
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'version_no',
                      :OLD.version_no, :NEW.version_no, :OLD.display_id,
                      v_type, :OLD.kit_component_id
                     );
      ELSIF :NEW.promo_flg <> :OLD.promo_flg
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'promo_flg',
                      'Promo Version', 'PV', :OLD.promo_flg, :NEW.promo_flg,
                      :OLD.display_id, v_type, :OLD.kit_component_id
                     );
      ELSIF :NEW.break_case <> :OLD.break_case
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'break_case',
                      'Version Code Setup', 'VCS', :OLD.break_case,
                      :NEW.break_case, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.kit_version_code <> :OLD.kit_version_code
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'kit_version_code',
                      'Kit Version Code', 'KVC', :OLD.kit_version_code,
                      :NEW.kit_version_code, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.pbc_version <> :OLD.pbc_version
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'pbc_version',
                      'PBC Version', 'PBC Vers', :OLD.pbc_version,
                      :NEW.pbc_version, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      ELSIF :NEW.finished_goods_sku <> :OLD.finished_goods_sku
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'finished_goods_sku',
                      'FG Consists of SKU', 'FG SKU',
                      :OLD.finished_goods_sku, :NEW.finished_goods_sku,
                      :OLD.display_id, v_type, :OLD.kit_component_id
                     );
      ELSIF :NEW.finished_goods_qty <> :OLD.finished_goods_qty
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'finished_goods_qty',
                      'FG Quantity', 'FG Qty', :OLD.finished_goods_qty,
                      :NEW.finished_goods_qty, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;
   END IF;
END;
/


/*###############################
 Create Views for reporting
###############################*/

CREATE OR REPLACE FORCE VIEW v_dmm_components (display_id,
                                               sku,
                                               product_name,
                                               bu_desc,
                                               break_case,
                                               promo_flg,
                                               i2_forecast_flg,
                                               source_pim_flg,
                                               kit_version_code,
                                               plant_break_case_flg,
                                               pbc_version,
                                               finished_goods_sku,
                                               finished_goods_qty,
                                               qty_per_facing,
                                               number_facings,
                                               reg_case_pack,
                                               plant_code,
                                               planner_code,
                                               lead_time,
                                               dc_code,
                                               dc_quantity
                                              )
AS
   SELECT kc.display_id, kc.sku, kc.product_name, kc.bu_desc, kc.break_case,
          kc.promo_flg, kc.i2_forecast_flg, kc.source_pim_flg,
          kc.kit_version_code, kc.plant_break_case_flg, kc.pbc_version,
          kc.finished_goods_sku, kc.finished_goods_qty, kc.qty_per_facing,
          kc.number_facings, kc.reg_case_pack, dp.plant_code, dp.planner_code,
          dp.lead_time, l.dc_code, ddc.quantity
     FROM kit_component kc, display_planner dp, display_dc ddc, LOCATION l
    WHERE kc.kit_component_id = dp.kit_component_id(+)
      AND dp.display_dc_id = ddc.display_dc_id(+)
      AND ddc.location_id = l.location_id(+);


CREATE OR REPLACE FORCE VIEW v_dmm_corrugate_vendor (display_id,
                                                     manufacturer_name,
                                                     vendor_part_num,
                                                     glovia_part,
                                                     description,
                                                     unit_cost,
                                                     delivery_date,
                                                     quantity
                                                    )
AS
   SELECT dm.display_id, m.NAME, dm.vendor_part_num, dm.glovia_part,
          dm.description, dm.unit_cost, dm.delivery_date, dm.quantity
     FROM manufacturer m, display_material dm
    WHERE m.manufacturer_id = dm.manufacturer_id;


CREATE OR REPLACE FORCE VIEW v_dmm_display (display_id,
                                            sku,
                                            description,
                                            qty_forecast,
                                            promo_year,
                                            sku_mix_final_flg,
                                            qsi_doc_number,
                                            customer_display#,
                                            sent_sample_date,
                                            final_artwork_date,
                                            structure_approved_flg,
                                            price_availability_date,
                                            pricing_admin_date,
                                            pim_completed_date,
                                            project_level_pct,
                                            comments,
                                            customer_name,
                                            status_name,
                                            structure_name,
                                            promo_period_name,
                                            display_setup_desc,
                                            cost_center,
                                            cm_project_num,
                                            sscid_project_num,
                                            project_rep,
                                            special_label_rqt,
                                            display_length,
                                            display_width,
                                            display_height,
                                            display_weight,
                                            display_per_layer,
                                            layers_per_pallet,
                                            serial_ship_flg,
                                            pallets_per_trailer,
                                            manufacturer_name
                                           )
AS
   SELECT d.display_id, d.sku, d.description, d.qty_forecast, d.promo_year,
          d.sku_mix_final_flg, d.qsi_doc_number, d.customer_display#,
          cm.sent_sample_date, cm.final_artwork_date,
          cm.structure_approved_flg, cm.price_availability_date,
          cm.pricing_admin_date, cm.pim_completed_date, cm.project_level_pct,
          cm.comments, c.customer_name, s.status_name, str.structure_name,
          pp.promo_period_name, ds.display_setup_desc, dc.cost_center,
          dc.cm_project_num, dc.sscid_project_num,
          dsr.first_name || ' ' || dsr.last_name, dl.special_label_rqt,
          dl.display_length, dl.display_width, dl.display_height,
          dl.display_weight, dl.display_per_layer, dl.layers_per_pallet,
          dl.serial_ship_flg, dl.pallets_per_trailer, m.NAME
     FROM display d,
          customer_marketing cm,
          customer c,
          status s,
          STRUCTURE str,
          promo_period pp,
          display_setup ds,
          display_cost dc,
          display_sales_rep dsr,
          display_logistics dl,
          manufacturer m
    WHERE d.display_id = cm.display_id(+)
      AND d.customer_id = c.customer_id(+)
      AND d.status_id = s.status_id(+)
      AND d.structure_id = str.structure_id(+)
      AND d.promo_period_id = pp.promo_period_id(+)
      AND d.display_setup_id = ds.display_setup_id(+)
      AND d.display_id = dc.display_id(+)
      AND d.display_sales_rep_id = dsr.display_sales_rep_id(+)
      AND d.display_id = dl.display_id(+)
      AND d.manufacturer_id = m.manufacturer_id(+);


CREATE OR REPLACE FORCE VIEW v_dmm_packout_detail (display_id,
                                                   must_arrive_date,
                                                   ship_from_date,
                                                   product_due_date,
                                                   packout_quantity,
                                                   dc_quantity,
                                                   dc_code
                                                  )
AS
   SELECT dsw.display_id, l.dc_code, ddc.quantity, dsw.must_arrive_date,
          dpo.ship_from_date, dpo.product_due_date, dpo.quantity
     FROM display_ship_wave dsw,
          display_packout dpo,
          display_dc ddc,
          LOCATION l
    WHERE dsw.display_ship_wave_id = dpo.display_ship_wave_id
      AND dpo.display_dc_id = ddc.display_dc_id
      AND ddc.location_id = l.location_id;


CREATE OR REPLACE FORCE VIEW v_dmm_ship_wave (display_id,
                                              must_arrive_date,
                                              quantity,
                                              destination
                                             )
AS
   SELECT dsw.display_id, dsw.must_arrive_date, dsw.quantity,
          CASE destination_flg
             WHEN 'N'
                THEN 'Domestic'
             ELSE 'Over Seas'
          END
     FROM display_ship_wave dsw;


--
-- Create procedures
--

CREATE OR REPLACE PROCEDURE delete_display (
   in_display_id   IN   NUMBER,
   in_username     IN   VARCHAR2
)
IS
/**********************************************************
 Date: 01/23/2010
 Created By: Lupe Garcia
 Description: Deletes a display by specific display_id and
 all child records in all corresponding tables.

 Modified: 01/25/2010
 Description: Fixed an issue where multiple records  in the
 display_planner and display_packout table we're making the
 stored procedure fail.

 Modified: 01/26/2010
 Description: Added the ability to pass in the username of
 the person deleting the display.  The procedure will then
 add the username to the user_change_log table where the
 display_id passed is matched and the change type is 'Delete'
**********************************************************/
   CURSOR k_cursor
   IS
      SELECT NVL (kit_component_id, 0) kit_component_id
        FROM kit_component
       WHERE display_id = in_display_id;

   CURSOR dsw_cursor
   IS
      SELECT NVL (display_ship_wave_id, 0) display_ship_wave_id
        FROM display_ship_wave
       WHERE display_id = in_display_id;
BEGIN
   FOR k_rec IN k_cursor
   LOOP
      DELETE FROM display_planner
            WHERE kit_component_id = k_rec.kit_component_id;
   END LOOP;

   FOR dsw_rec IN dsw_cursor
   LOOP
      DELETE FROM display_packout
            WHERE display_ship_wave_id = dsw_rec.display_ship_wave_id;
   END LOOP;

--
   DELETE FROM kit_component
         WHERE display_id = in_display_id;

--
   DELETE FROM display_ship_wave
         WHERE display_id = in_display_id;

--
   DELETE FROM display_material
         WHERE display_id = in_display_id;

--
   DELETE FROM display_material
         WHERE display_id = in_display_id;

--
   DELETE FROM display_logistics
         WHERE display_id = in_display_id;

--
   DELETE FROM display_dc
         WHERE display_id = in_display_id;

--
   DELETE FROM display_cost
         WHERE display_id = in_display_id;

--
   DELETE FROM customer_marketing
         WHERE display_id = in_display_id;

--
   DELETE FROM display
         WHERE display_id = in_display_id;

   UPDATE user_change_log
      SET username = in_username
    WHERE display_id = in_display_id AND change_type = 'Delete';

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (   DBMS_UTILITY.format_error_stack ()
                            || ' : '
                            || DBMS_UTILITY.format_error_backtrace ()
                           );
      ROLLBACK;
END delete_display;
/



/**********************************************************
***********************************************************
 This section contains insert statements used to insert
 data into the look up tables.
***********************************************************
**********************************************************/

-- insert into customer table
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Office Depot');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Office Max');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Staples');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Home Depot');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Big Lots');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Bi-Mart');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Costco');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Dollar Tree');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Food 4 Less');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Fred Meyer');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Freds');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'HEB');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Kmart');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Mass');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Meijer');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Peytons/Kroger');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Ralphs');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Rite Aid');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Sam''s');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'SP Richards');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Target');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'United Supply');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'WakeFern');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Walgreens');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Walmart');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Weis Markets');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'CVS');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'L\&R');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Pens Etc.');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Kroger');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'AAFES');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Nexcom');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Marine Corps');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'Government');
insert into customer (customer_id, customer_name) values (customer_seq.nextval, 'LCI');
insert into customer (customer_id, customer_name,CUSTOMER_TYPE_ID) values (customer_seq.nextval, 'Alco Duckwall',2);

-- insert into display_sales_rep table
insert into display_sales_rep (display_sales_rep_id, first_name, last_name) 
values (display_sales_rep_seq.nextval, 'Robert', 'McIver');                                                     
                                                                                
insert into display_sales_rep (display_sales_rep_id, first_name, last_name) 
values (display_sales_rep_seq.nextval, 'Kara', 'Johnson');                                                      
                                                                                
insert into display_sales_rep (display_sales_rep_id, first_name, last_name) 
values (display_sales_rep_seq.nextval, 'Kim', 'True');                                                 
                                                                                
insert into display_sales_rep (display_sales_rep_id, first_name, last_name) 
values (display_sales_rep_seq.nextval, 'Brenda', 'Impellezzeri');

-- Insert into location table

-- insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'NV', '--- SELECT A VALUE ---', 'Y');                                                                   
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'T5', 'T5-Tijuana LFDC', 'Y');                                                                          
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'B4', 'B4-Canadian Direct Import', 'Y');                                                                
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'A1', 'A1-Alliance Display and Packaging', 'N');                                                        
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'FO', 'FO-Fontana Distribution Center', 'Y');                                                           
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'F2', 'F2-Fontana Kit DC', 'Y');                                                                        
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'GE', 'GE-Genco Distributors, Lebanon', 'Y');                                                           
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'G2', 'G2-Genco, Kits and Displays, Lebanon', 'N');                                                     
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'G3', 'G3-Genco, Dixon Ill', 'Y');                                                                      
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'H2', 'H2-Holliston Kit Warehouse', 'Y');                                                              
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'MM', 'MM-Memphis Distribution Center', 'N');                                                          
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'M2', 'M2-Memphis Kit Warehouse', 'Y');                                                                
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'AD', 'AD-Avery Dennison OPG', 'N');                                                                   
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'B2', 'B2-Pickering Kit Warehouse', 'Y');                                                              
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'BV', 'BV-Bowmanville', 'Y');                                                                          
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'C2', 'C2-Crossville plant direct', 'N');                                                              
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'C3', 'C3-Chicopee plant direct', 'Y');                                                                
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'CH', 'CH-Chicopee Plant', 'Y');                                                                       
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'CN', 'CN-Canada', 'Y');                                                                               
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'CR', 'CR-Crossville', 'N');                                                                           
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'EP', 'EP-El Paso (Juarez Plant)', 'Y');                                                               
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'GA', 'GA-Gainesville Plant', 'N');                                                                    
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'GN', 'GN-Great Northern Corp', 'N');                                                                  
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'H3', 'H3-Holliston Alternate Warehouse', 'N');                                                        
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'HO', 'HO-Holliston Distribution Center', 'Y');                                                        
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'LP', 'LP-Linpac', 'N');                                                                               
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'M3', 'M3-Meridian plant direct warehouse', 'Y');                                                      
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'M4', 'M4-Printer Paper overstock (Memphis)', 'Y');                                                    
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'MI', 'MI-Milford Plant', 'N');                                                                        
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'MR', 'MR-Meridian Plant', 'Y');                                                                       
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'PA', 'PA-1st Choice Pking (PCA)', 'Y');                                                               
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'PC', 'PC-PCA - Chicago', 'N');                                                                        
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'PL', 'PL-PELL', 'N');                                                                                 
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'RO', 'RO-Rochelle', 'N');                                                                             
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'S2', 'S2-Springfield Kit Warehouse', 'N');                                                            
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'SP', 'SP-Springfield', 'N');                                                                          
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'TJ', 'TJ-Tijuana Plant', 'Y');                                                                        
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'SA', 'SA-Smurfit-Stone (WSP,Inc)', 'N');                                                              
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'UM', 'UM-US Merchants', 'Y');                                                                         
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'JS', 'JS-Justman', 'N');                                                                              
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'J2', 'J2-Justman 2 (Chino)', 'Y');                                                                    
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'CA', 'CA-Complete Assembly', 'N');                                                                    
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'T3', 'T3-Tijuana Plant Direct', 'Y');                                                                 
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'BI', 'BI-Bekins Distribution-Idaho', 'N');
-- Row below requires an escape character in order for the insert to work
-- the escape character is needed because of the ampersand (&) in the dc_name below ('FC-Kosakura \& Associates')
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'FC', 'FC-Kosakura \& Associates', 'N');                                                                
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'BU', 'BU-Bekins Distribution', 'N');                                                                  
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'F3', 'F3-Tonys Express (FO overflow)', 'Y');                                                          
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'F4', 'F4-Standard Logistics (FO overflow)', 'Y');                                                     
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'GV', 'GV-Givens Logistics', 'N');                                                                     
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'M5', 'M5-Meridian LFDC', 'Y');                                                                        
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'SM', 'SM-Elite Packaging (Smurfit-Stone CC)', 'N');                                                   
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'ST', 'ST-Stomp', 'N');                                                                                
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'AT', 'AT-Aaron Thomas', 'N');                                                                         
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'KL', 'KL-Packout Location', 'N');                                                                     
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'BP', 'BP', 'N');                                                                                      
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'PF', 'PF', 'N');                                                                                      
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'AG', 'AG-Aaron Thomas-Garden Grove', 'N');                                                            
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'UC', 'UC - USC Distribution Services -Carson', 'N');                                                  
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'M6', 'M6-Meridian', 'N');                                                                             
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'DR', 'DR-Digital River, Ecommerce', 'Y');                                                             
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'C4', 'C4', 'Y');                                                                                      
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'E3', 'E3 - Juarez plant direct', 'Y');                                                                
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'DP', 'DP', 'N');                                                                                      
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'HA', 'HA', 'N');                                                                                      
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'HP', 'HP', 'N');                                                                                      
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'MS', 'MS-Meridian', 'Y');                                                                             
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'DI', 'DI-Direct Import', 'Y');                                                                        
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'B3', 'B3-Canadian Packout House', 'Y');                                                               
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'TC', 'TC-Corrugados', 'Y');                                                                           
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'T4', 'T4', 'Y');                                                                                      
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'P', 'P', 'N');                                                                                        
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'Pl', 'Pl', 'N');                                                                                      
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'C5', 'C5-Chicopee LFDC', 'Y');
insert into location (location_id, dc_code, dc_name, active_flg) values (location_seq.nextval, 'AG', 'AG-Rapid Display', 'Y');

--
-- insert into promo_period table
--
insert into promo_period (promo_period_id, promo_period_name) values (promo_period_seq.nextval, 'Q1-Taxtime/BTB');
insert into promo_period (promo_period_id, promo_period_name) values (promo_period_seq.nextval, 'Q2-Fathers Day/Mothers');
insert into promo_period (promo_period_id, promo_period_name) values (promo_period_seq.nextval, 'Q3-BTS');
insert into promo_period (promo_period_id, promo_period_name) values (promo_period_seq.nextval, 'Q4-Fall/Holiday/Falliday');

--
-- insert into status table
--
insert into status (status_id, status_name) values (status_seq.nextval, 'Proposed');
insert into status (status_id, status_name) values (status_seq.nextval, 'Approved');
insert into status (status_id, status_name) values (status_seq.nextval, 'Completed');
insert into status (status_id, status_name) values (status_seq.nextval, 'Cancelled');

--
-- structure table
--
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Full Pallet');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Half Pallet');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, '9 Facing Octagon');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Tray');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, '8 Facing Tower');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Endcap Tray');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Gravity Feed');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Dump Bin');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Clip Strip');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Power Wing');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, '9 Facing Tower');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, '6 Facing Tower');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, '4 Facing Tower');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Quarter Pallet');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Clipstrip');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Swivel Hook');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Fixture');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, '5 Facing Tower');
insert into structure (structure_id, structure_name) values (structure_seq.nextval, 'Permanent Fixture');

--
-- insert into display_setup Table
--
insert into display_setup (display_setup_id, display_setup_desc) values (display_setup_seq.nextval, 'Kit');
insert into display_setup (display_setup_id, display_setup_desc) values (display_setup_seq.nextval, 'Finished Goods - New UPC');
insert into display_setup (display_setup_id, display_setup_desc) values (display_setup_seq.nextval, 'Finished Goods - New Production Version');

--
-- insert into valid_plant Table
--
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('CH', 'Chicopee', 'Y', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('CN', 'Bowmanville', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('EP', 'Juarez Plant 1', 'Y', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('GA', 'Gainesville', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('HO', 'Holliston', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('MR', 'Meridian', 'Y', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('MI', 'Milford', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('AD', 'AD', 'Y', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('BO', 'BO', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('BV', 'BV', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('CR', 'CR', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('KM', 'KM', 'Y', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('MM', 'MM', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('OB', 'OB', 'Y', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('PF', 'Purchased Finished Goods', 'Y', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('RO', 'RO', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('TJ', 'TJ', 'Y', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('TM', 'TM', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('BP', 'Bowmanville PFG', 'N', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('BR', 'BR', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('SP', 'SP', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('M4', 'M4', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('KS', 'KunShan', 'Y', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('PD', 'Purchasing/Dividers', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('DS', 'DS', 'Y', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('FC', 'FC', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('DV', 'Divisional', 'N', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('JZ', 'Juarez Plant 2', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('C4', 'C4', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('T4', 'Tijuana Plant Direct Location', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('DI', 'DI', 'Y', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('Wh', 'Wh', 'N', 'N');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('CL', 'PLANT CODE TO CLOSE DC', 'Y', 'Y');
insert into valid_plant (plant_code, plant_desc, active_flg, active_on_finance_flg) values('NV', '--- SELECT A VALUE ---', 'Y', 'Y');

--
-- insert into valid_planner Table
--
insert into valid_planner (planner_code, planner_desc, active_flg) values('AD01', 'AD01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('AD02', 'AD02', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('AD03', 'AD03', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('AD04', 'AD04', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('AD05', 'AD05', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('AD06', 'AD06', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('AD07', 'AD07', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('AD08', 'AD08', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BN01', 'BN01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BP01', 'BP01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BP02', 'BP02', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BP03', 'BP03', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BP04', 'BP04', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BP05', 'BP05', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BP06', 'BP06', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BV03', 'BV03', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CH01', 'CH01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CH02', 'CH02', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CH03', 'CH03', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CH04', 'CH04', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CH05', 'CH05', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR01', 'CR01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR02', 'CR02', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR03', 'CR03', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR04', 'CR04', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR05', 'CR05', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR06', 'CR06', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR07', 'CR07', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR08', 'CR08', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR09', 'CR09', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR10', 'CR10', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP01', 'EP01', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP02', 'EP02', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP03', 'EP03', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP04', 'EP04', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP05', 'EP05', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP06', 'EP06', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP07', 'EP07', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP08', 'EP08', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP09', 'EP09', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP10', 'EP10', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP11', 'EP11', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP12', 'EP12', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP13', 'EP13', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP14', 'EP14', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('F3', 'F3', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('F5', 'F5', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('F9', 'F9', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('FA', 'FA', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('FC', 'FC', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('FD', 'FD', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('GA01', 'GA01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('GA02', 'GA02', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('GA04', 'GA04', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('GA10', 'GA10', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('KM01', 'KM01', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('LT01', 'LT01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('LT02', 'LT02', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('MA01', 'MA01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('ME01', 'ME01', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('ME02', 'ME02', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('ME03', 'ME03', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('ME99', 'ME99', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('MI01', 'MI01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('MI02', 'MI02', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('MI03', 'MI03', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('MI04', 'MI04', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('OP01', 'OP01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('P4', 'P4', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('P6', 'P6', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('P9', 'P9', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF01', 'PF01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('RO01', 'RO01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('RO04', 'RO04', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP01', 'SP01', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP02', 'SP02', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP03', 'SP03', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP04', 'SP04', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP05', 'SP05', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ01', 'TJ01', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ02', 'TJ02', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ03', 'TJ03', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ04', 'TJ04', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ05', 'TJ05', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ06', 'TJ06', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ07', 'TJ07', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ08', 'TJ08', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ09', 'TJ09', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ10', 'TJ10', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ99', 'TJ99', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TM01', 'TM01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP15', 'EP15', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP16', 'EP16', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('ME04', 'ME04', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EPC1', 'EPC1', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF03', 'PF03', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF02', 'PF02', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ11', 'TJ11', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ12', 'TJ12', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('AD09', 'AD09', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BREA', 'BREA', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PA01', 'PA01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('F4', 'F4', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('G2', 'G2', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('HA', 'HA', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('HE', 'HE', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('P1', 'P1', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('R001', 'R001', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ13', 'TJ13', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('Z9', 'Z9', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP17', 'EP17', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF04', 'PF04', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP18', 'EP18', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF05', 'PF05', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ14', 'TJ14', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ15', 'TJ15', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ16', 'TJ16', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('AP01', 'AP01', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PFG03', 'PFG03', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP19', 'EP19', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ17', 'TJ17', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP06', 'SP06', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP07', 'SP07', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP08', 'SP08', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP09', 'SP09', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP10', 'SP10', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP11', 'SP11', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP12', 'SP12', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP13', 'SP13', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP14', 'SP14', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP15', 'SP15', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP16', 'SP16', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP17', 'SP17', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP20', 'EP20', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF06', 'PF06', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('ME16', 'ME16', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP21', 'EP21', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF07', 'PF07', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF08', 'PF08', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP22', 'EP22', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP23', 'EP23', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP18', 'SP18', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP19', 'SP19', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP20', 'SP20', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP21', 'SP21', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP24', 'EP24', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF09', 'PF09', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF11', 'PF11', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP22', 'SP22', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF10', 'PF10', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP23', 'SP23', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CH', 'Obsolete CH', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('AD', 'Obsolete AD', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BP', 'Obsolete BP', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('DS', 'Obsolete DS', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP', 'Obsolete EP', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('GA', 'Obsolete GA', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('JZ', 'Obsolete JZ', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('KM', 'Obsolete KM', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('KS', 'Obsolete KS', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('MR', 'Obsolete MR', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF', 'Obsolete PF', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ', 'Obsolete TJ', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('NA', 'Obsolete Unknown', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ21', 'TJ21', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP24', 'SP24', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP25', 'SP25', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ22', 'TJ22', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ23', 'TJ23', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ24', 'TJ24', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ25', 'TJ25', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BO', 'BO', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BR', 'BR', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('BV', 'BV', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CR', 'CR', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('M4', 'M4', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('SP', 'SP', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TM', 'TM', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('TJ26', 'TJ26', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('EP9', 'EP9', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('PF12', 'PF12', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('Plant', 'Plant', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CL-CH', 'CLOSED - CH', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('NVG', '---- SELECT A VALUE ----', 'N');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CL-EP', 'CLOSED - EP', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CL-PF', 'CLOSED - PF', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CL-AD', 'CLOSED - AD', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CL-TJ', 'CLOSED - TJ', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CL-MR', 'CLOSED - MR', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CL-KM', 'CLOSED - KM', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CL-DS', 'CLOSED - DS', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CL-KS', 'CLOSED - KS', 'Y');
insert into valid_planner (planner_code, planner_desc, active_flg) values('CL-NA', 'CLOSED - UNKNOWN', 'Y');

--
-- Insert into valid_bu Table
--
insert into valid_bu(bu_code, bu_desc, active_flg, product_category_code) values ('775', 'CARDS/PAPER', 'Y', '100');
insert into valid_bu(bu_code, bu_desc, active_flg, product_category_code) values ('888', 'LABELS', 'Y', '100');
insert into valid_bu(bu_code, bu_desc, active_flg, product_category_code) values ('330', 'DIVIDERS', 'Y', '100');
insert into valid_bu(bu_code, bu_desc, active_flg, product_category_code) values ('660', 'BINDERS', 'Y', '101');
insert into valid_bu(bu_code, bu_desc, active_flg, product_category_code) values ('550', 'WRITING INSTRUMENTS', 'Y', '101');
insert into valid_bu(bu_code, bu_desc, active_flg, product_category_code) values ('770', 'PAPER', 'N', '100');
insert into valid_bu(bu_code, bu_desc, active_flg, product_category_code) values ('331', 'O\&P', 'Y', '101');
insert into valid_bu(bu_code, bu_desc, active_flg, product_category_code) values ('995', 'MEDIA LABELS', 'N', '100');
insert into valid_bu(bu_code, bu_desc, active_flg, product_category_code) values ('900', 'PHOTO ID SYSTEMS', 'N', '100');

--
-- Insert into Manufacturer Table
--
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'OCC');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'Justman');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'International Paper');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'PCA');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'Sonoco-Corrflex');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'Smurfit-Stone');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'Rapid Displays');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'Cornerstone');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'Dual Graphics');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'ULINE');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'FFR');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'United Merchants');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'Felbro');
insert into manufacturer(manufacturer_id, name) values (manufacturer_seq.nextval, 'U.S. Display Group');

--
-- Insert into Country Table
--
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Afghanistan',	'AF');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Albania',	'AL');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Argentina',	'AR');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Armenia',	'AM');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Australia',	'AU');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Austria',	'AT');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Azerbaijan',	'AZ');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Bahrain',	'BH');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Bangladesh',	'BD');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Belgium',	'BE');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Belize',	'BZ');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Bermuda',	'BM');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Bolivia',	'BO');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Bosnia-Hercegovina',	'BA');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Brazil',	'BR');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Brunei',	'BN');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Bulgaria',	'BG');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Cambodia',	'KH');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Canada',	'CA');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Chile',	'CL');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'China (Mainland)',	'CN');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'China (Taiwan)',	'TW');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Colombia',	'CO');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Costa Rica',	'CR');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Crotia',	'HR');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Cuba',	'CU');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Czech Republic',	'CZ');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Czechoslovakia',	'CS');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Denmark',	'DK');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Dominican Republic',	'DO');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Ecuador',	'EC');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Egypt',	'EG');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'El Salvador',	'SV');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Ethiopia',	'ET');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'European Monetary Union',	'EU');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Finland',	'FI');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'France',	'FR');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Georgia',	'GE');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Germany, Fed. Republic',	'DE');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Greece',	'GR');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Guatemala',	'GT');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Haiti',	'HT');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Honduras',	'HN');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Hong Kong',	'HK');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Hungary',	'HU');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'India',	'IN');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Indonesia',	'ID');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Iran',	'IR');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Iraq',	'IQ');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Ireland',	'IE');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Israel',	'IL');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Italy',	'IT');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Jamaica',	'JM');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Japan',	'JP');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Jordan',	'JO');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Kazakstan',	'KK');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Korea, Republic Of',	'KR');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Kuwait',	'KW');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Libya',	'LY');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Luxembourg',	'LU');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Malaysia',	'MY');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Mexico',	'MX');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Netherlands',	'NL');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Netherlands Antilles',	'AN');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'New Zealnd',	'NZ');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'North Korea',	'KP');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Norway',	'NO');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Oman',	'OM');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Pakistan',	'PK');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Panama',	'PA');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Paraguay',	'PY');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Peru',	'PE');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Philippines',	'PH');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Poland',	'PL');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Portugal',	'PT');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Puerto Rico',	'PR');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Republic Of South Africa',	'ZA');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Republic Of Yemen',	'YE');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Romania',	'RO');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Russia',	'RU');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Saudi Arabia',	'SA');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Singapore',	'SG');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Slovakia',	'SK');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Slovenia',	'SI');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Spain',	'ES');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Sudan',	'SD');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Sweden',	'SE');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Switzerland',	'CH');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Thailand',	'TH');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Turkey',	'TR');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Ukraine',	'UA');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'United Arab Emirates',	'AE');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'United Kingdom',	'GB');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'United States Of America',	'US');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Uruguay',	'UY');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Uzbekistan',	'UZ');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Venezuela',	'VE');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Vietnam',	'VN');
insert into country(COUNTRY_ID,COUNTRY_NAME,COUNTRY_CODE) values (country_seq.nextval, 'Yugoslavia',	'YU');
commit;
update country set active_flg='N' where (country_code not in('US','MX','CN')); 
commit;

--
-- Insert into Vendor Table
--
insert into vendor(VENDOR_ID,VENDOR_NAME,VENDOR_CODE) values (vendor_seq.nextval, 'Vendor I',	'V1');
insert into vendor(VENDOR_ID,VENDOR_NAME,VENDOR_CODE) values (vendor_seq.nextval, 'Vendor II',	'V2');
insert into vendor(VENDOR_ID,VENDOR_NAME,VENDOR_CODE) values (vendor_seq.nextval, 'Vendor III',	'v3');
insert into vendor(VENDOR_ID,VENDOR_NAME,VENDOR_CODE) values (vendor_seq.nextval, 'Vendor IV',	'v4');

--
-- Remove the scenarioId constraint in Corrugate_Component table
--
--alter table CORRUGATE_COMPONENT DROP constraint CORR_COMP_SCENARIO_FK;






