-- Table program_bu

CREATE TABLE program_bu(
  program_bu_id NUMBER(10,0) NOT NULL,
  bu_code VARCHAR2(3 ),
  display_id NUMBER(10,0),
  program_pct NUMBER(10,0) DEFAULT 0,
  dt_created DATE,
  dt_last_changed DATE,
  user_created VARCHAR2(100 ),
  user_last_changed VARCHAR2(100 ));

-- Add keys for table program_bu

ALTER TABLE program_bu ADD CONSTRAINT program_bu_pk PRIMARY KEY (program_bu_id);

ALTER TABLE program_bu ADD CONSTRAINT program_bu_display_id_fk FOREIGN KEY (display_id) REFERENCES display (display_id);

ALTER TABLE program_bu ADD CONSTRAINT program_bu_valid_bu_fk FOREIGN KEY (bu_code) REFERENCES valid_bu (bu_code);

-- Calculation Summary table changes

ALTER TABLE calculation_summary
ADD(override_exception_pct NUMBER(12,2) DEFAULT 0,
invoice_program NUMBER(12,2) DEFAULT 0);

-- Trigger program_bu_trig

CREATE OR REPLACE TRIGGER program_bu_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON program_bu
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value, new_value, display_id,
                   change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'program_bu', 'program_bu record', 'N/A', 'N/A', :NEW.display_id,
                   v_type, :NEW.program_bu_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed, SYSDATE,
                   'program_bu', 'deleting program_bu record', NULL, NULL,
                   :OLD.display_id, v_type, :OLD.program_bu_id
                  );
   ELSIF UPDATING
   THEN
      IF NVL (:NEW.program_pct, 0) <> NVL (:OLD.program_pct, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username, dt_changed,
                      table_name, FIELD, friendly_name, short_name,
                      old_value, new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed, SYSDATE,
                      'program_bu', 'program_pct', 'Program Percentage', 'Program %',
                      :OLD.program_pct, :NEW.program_pct, :OLD.display_id, v_type,
                      :OLD.program_bu_id
                     );
      END IF;
   END IF;
END program_bu_trig;
/

CREATE SEQUENCE PROGRAM_BU_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 5
  NOORDER;
  
  -- Alter procedure delete_display in part of program_bu 
  
CREATE OR REPLACE PROCEDURE FILL_PROGRAMS AS 
BEGIN

     FOR y IN (SELECT a.display_id FROM DISPLAY a)
     LOOP
       FOR i IN (SELECT b.bu_code FROM VALID_BU b
               WHERE     b.active_flg = 'Y')
       LOOP
       insert into program_bu 
            (program_bu_id, bu_code, display_id, program_pct, 
            dt_created, dt_last_changed, user_created, user_last_changed)
               VALUES (program_bu_seq.NEXTVAL, i.bu_code, y.display_id, 0,
               SYSDATE, SYSDATE, 'db_proc', 'db_proc');   
       END LOOP;
    END LOOP;
END FILL_PROGRAMS;