SET DEFINE OFF;
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (90, NULL, TO_DATE('03/05/2012 13:09:18', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'Recalculate after TT Binder & Reinforcements are in OLS', 
    1121);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (82, NULL, TO_DATE('02/22/2012 14:26:14', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'corrected components and pricing ', 
    829);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (91, NULL, TO_DATE('03/05/2012 13:10:21', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'Recalculate after TT Binder & Reinforcements are in ILS', 
    1121);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (87, NULL, TO_DATE('03/05/2012 09:14:55', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'Recalculated with added costs of shrink wrapping and inserting Sticky Note value add inside binder', 
    820);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (77, NULL, TO_DATE('02/13/2012 12:55:17', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'Manually added PE for 11983 to clearance price of $1.', 
    996);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (72, NULL, TO_DATE('11/04/2011 13:02:07', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'Added last item - 17164 - noticed invoice price shows as 0', 
    829);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (67, NULL, TO_DATE('10/27/2011 13:35:31', 'MM/DD/YYYY HH24:MI:SS'), 'Jeffrey Wert', 'Fixed and recalculated on 10/27/2011', 
    820);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (97, NULL, TO_DATE('03/08/2012 08:47:41', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'Changed 32201 reinforcements to version 25 then back to version 24 to use up.', 
    1076);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (61, NULL, TO_DATE('04/11/2011 09:10:21', 'MM/DD/YYYY HH24:MI:SS'), 'McIverR', 'Possible ship as a near pack display.', 
    531);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (88, NULL, TO_DATE('03/05/2012 09:50:30', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'need to recalculate when binder version and reinforcements are in ILS ', 
    1115);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (89, NULL, TO_DATE('03/05/2012 11:52:46', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'Need to recalculate when reinforcements are complete in ILS', 
    1120);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (92, NULL, TO_DATE('03/07/2012 09:56:34', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'Need to recalculate when corrugate price is firm and when price changes on dividers Apr 1', 
    1120);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (93, NULL, TO_DATE('03/07/2012 14:32:25', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'Changed component version from 24 to 25', 
    1109);
Insert into DISPLAY_COST_COMMENT
   (DISPLAY_COST_COMMENT_ID, DISPLAY_COST_ID, CREATE_DATE, USERNAME, COST_COMMENT, 
    DISPLAY_SCENARIO_ID)
 Values
   (98, NULL, TO_DATE('03/08/2012 08:52:50', 'MM/DD/YYYY HH24:MI:SS'), 'gail wensink', 'Changed component back to version 24 to use up', 
    1109);
COMMIT;
