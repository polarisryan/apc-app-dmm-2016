SET DEFINE OFF;
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (1, 'T5', 'T5-Tijuana LFDC', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (2, 'B4', 'B4-Harding Display Corp', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (3, 'A1', 'A1-Alliance Display and Packaging', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (4, 'FO', 'FO-Fontana Distribution Center', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (5, 'F2', 'F2-Fontana Kit DC', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (6, 'GE', 'GE-Genco Distributors, Lebanon', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (7, 'G2', 'G2-Genco, Kits and Displays, Lebanon', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (8, 'G3', 'G3-Genco, Dixon Ill', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (9, 'H2', 'H2-Holliston Kit Warehouse', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (10, 'MM', 'MM-Memphis Distribution Center', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (11, 'M2', 'M2-Memphis Kit Warehouse', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (12, 'AD', 'AD-Avery Dennison OPG', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (13, 'B2', 'B2-Pickering Kit Warehouse', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (14, 'BV', 'BV-Bowmanville', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (15, 'C2', 'C2-Crossville plant direct', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (16, 'C3', 'C3-Chicopee plant direct', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (17, 'CH', 'CH-Chicopee Plant', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (18, 'CN', 'CN-Canada', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (19, 'CR', 'CR-Crossville', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (20, 'EP', 'EP-El Paso (Juarez Plant)', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (21, 'GA', 'GA-Gainesville Plant', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (22, 'GN', 'GN-Great Northern Corp', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (23, 'H3', 'H3-Holliston Alternate Warehouse', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (24, 'HO', 'HO-Holliston Distribution Center', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (26, 'M3', 'M3-Meridian plant direct warehouse', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (27, 'M4', 'M4-Printer Paper overstock (Memphis)', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (28, 'MI', 'MI-Milford Plant', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (29, 'MR', 'MR-Meridian Plant', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (30, 'PA', 'PA-1st Choice Pking (PCA)', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (31, 'PC', 'PC-PCA - Chicago', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (32, 'PL', 'PL-PELL', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (33, 'RO', 'RO-Rochelle', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (34, 'S2', 'S2-Springfield Kit Warehouse', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (35, 'SP', 'SP-Springfield', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (36, 'TJ', 'TJ-Tijuana Plant', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (37, 'SA', 'SA-Smurfit-Stone (WSP,Inc)', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (38, 'UM', 'UM-US Merchants', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (39, 'JS', 'JS-Justman', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (40, 'J2', 'J2-Justman 2 (Chino)', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (41, 'CA', 'CA-Complete Assembly', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (42, 'T3', 'T3-Tijuana Plant Direct', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (43, 'BI', 'BI-Bekins Distribution-Idaho', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (44, 'FC', 'FC-Kosakura & Associates', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (45, 'BU', 'BU-Bekins Distribution', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (46, 'F3', 'F3-Tonys Express (FO overflow)', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (47, 'F4', 'F4-Standard Logistics (FO overflow)', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (48, 'GV', 'GV-Givens Logistics', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (49, 'M5', 'M5-Meridian LFDC', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (50, 'SM', 'SM-Elite Packaging (Smurfit-Stone CC)', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (51, 'ST', 'ST-Stomp', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (52, 'AT', 'AT-Aaron Thomas', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (53, 'KL', 'KL-Packout Location', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (54, 'BP', 'BP', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (55, 'PF', 'Purchased Finished Goods (PFG)', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (56, 'AG', 'AG-Aaron Thomas-Garden Grove', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (57, 'UC', 'UC - USC Distribution Services -Carson', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (58, 'M6', 'M6-Meridian', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (59, 'DR', 'DR-Digital River, Ecommerce', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (60, 'C4', 'C4', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (61, 'E3', 'E3 - Juarez plant direct', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (62, 'DP', 'DP', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (63, 'HA', 'HA', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (64, 'HP', 'HP', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (65, 'MS', 'MS-Meridian', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (66, 'DI', 'DI-Direct Import', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (67, 'B3', 'B3-Canadian Packout House', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (68, 'TC', 'TC-Corrugados', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (69, 'T4', 'T4-Tijuana DC', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (70, 'P', 'P', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (71, 'Pl', 'Pl', 'N');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (72, 'C5', 'C5-Chicopee LFDC', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (73, 'AG', 'AG-Rapid Display', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (75, 'SM', 'SM-Rapid Displays', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (74, 'LP', 'LP-Dual Graphics', 'Y');
Insert into LOCATION
   (LOCATION_ID, DC_CODE, DC_NAME, ACTIVE_FLG)
 Values
   (76, 'B6', 'B6-Co-Pak Packaging Corp', 'Y');
COMMIT;
