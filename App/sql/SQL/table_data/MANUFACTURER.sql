SET DEFINE OFF;
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (1, 'OCC', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (2, 'Justman', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (3, 'International Paper', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (4, 'PCA', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (5, 'Sonoco-Corrflex', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (6, 'Smurfit-Stone', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (7, 'Rapid Displays', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (8, 'Cornerstone', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (9, 'Dual Graphics', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (10, 'ULINE', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (11, 'FFR', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (12, 'United Merchants', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (13, 'Felbro', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (14, 'U.S. Display Group', 'N');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (15, 'Vanguard', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (16, 'Ardent', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (17, 'Rand-Whitney', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (18, 'Abbott-Action', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (20, 'N/A', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (28, 'Co-Pak Packaging Corp', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (19, 'Menasha', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (25, 'Harding', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (26, 'Pearce Wellwood', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (27, 'Protagon', 'Y');
Insert into MANUFACTURER
   (MANUFACTURER_ID, NAME, ACTIVE_FLG)
 Values
   (29, 'Reliable Container Corp', 'Y');
COMMIT;
