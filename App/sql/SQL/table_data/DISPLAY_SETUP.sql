SET DEFINE OFF;
Insert into DISPLAY_SETUP
   (DISPLAY_SETUP_ID, DISPLAY_SETUP_DESC, ACTIVE_FLG)
 Values
   (1, 'Kit', 'Y');
Insert into DISPLAY_SETUP
   (DISPLAY_SETUP_ID, DISPLAY_SETUP_DESC, ACTIVE_FLG)
 Values
   (2, 'Finished Goods - New UPC', 'Y');
Insert into DISPLAY_SETUP
   (DISPLAY_SETUP_ID, DISPLAY_SETUP_DESC, ACTIVE_FLG)
 Values
   (3, 'Finished Goods - New Production Version', 'Y');
COMMIT;
