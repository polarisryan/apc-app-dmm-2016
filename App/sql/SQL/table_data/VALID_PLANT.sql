SET DEFINE OFF;
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('CH', 'Chicopee', 'Y', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('CN', 'Bowmanville', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('EP', 'Juarez Plant 1', 'Y', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('GA', 'Gainesville', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('HO', 'Holliston', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('MR', 'Meridian', 'Y', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('MI', 'Milford', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('AD', 'AD', 'Y', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('BO', 'BO', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('BV', 'BV', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('CR', 'CR', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('KM', 'KM', 'Y', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('MM', 'MM', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('OB', 'OB', 'Y', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('PF', 'Purchased Finished Goods', 'Y', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('RO', 'RO', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('TJ', 'TJ', 'Y', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('TM', 'TM', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('BP', 'Bowmanville PFG', 'N', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('BR', 'BR', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('SP', 'SP', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('M4', 'M4', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('KS', 'KunShan', 'Y', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('PD', 'Purchasing/Dividers', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('DS', 'DS', 'Y', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('FC', 'FC', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('DV', 'Divisional', 'N', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('JZ', 'Juarez Plant 2', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('C4', 'C4', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('T4', 'Tijuana Plant Direct Location', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('DI', 'DI', 'Y', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('Wh', 'Wh', 'N', 'N');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('CL', 'PLANT CODE TO CLOSE DC', 'Y', 'Y');
Insert into VALID_PLANT
   (PLANT_CODE, PLANT_DESC, ACTIVE_FLG, ACTIVE_ON_FINANCE_FLG)
 Values
   ('NV', '--- SELECT A VALUE ---', 'Y', 'Y');
COMMIT;
