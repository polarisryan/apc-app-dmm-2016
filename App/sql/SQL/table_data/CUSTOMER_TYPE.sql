SET DEFINE OFF;
Insert into CUSTOMER_TYPE
   (CUSTOMER_TYPE_ID, TYPE_NAME, ACTIVE_FLG)
 Values
   (1, 'Superstore', 'Y');
Insert into CUSTOMER_TYPE
   (CUSTOMER_TYPE_ID, TYPE_NAME, ACTIVE_FLG)
 Values
   (2, 'Mass Market', 'Y');
COMMIT;
