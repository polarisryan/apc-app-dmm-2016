SET DEFINE OFF;
Insert into EXCESS_INVENTORY_TYPE
   (EXCESS_INVENTORY_TYPE_ID, DESCRIPTION, ACTIVE_FLG)
 Values
   (1, 'Destroy', 'Y');
Insert into EXCESS_INVENTORY_TYPE
   (EXCESS_INVENTORY_TYPE_ID, DESCRIPTION, ACTIVE_FLG)
 Values
   (2, 'Discount/Rebate', 'Y');
Insert into EXCESS_INVENTORY_TYPE
   (EXCESS_INVENTORY_TYPE_ID, DESCRIPTION, ACTIVE_FLG)
 Values
   (3, 'Sell-Thru', 'Y');
Insert into EXCESS_INVENTORY_TYPE
   (EXCESS_INVENTORY_TYPE_ID, DESCRIPTION, ACTIVE_FLG)
 Values
   (4, 'Blank/Other', 'Y');
COMMIT;
