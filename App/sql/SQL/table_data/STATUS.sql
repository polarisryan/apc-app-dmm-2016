SET DEFINE OFF;
Insert into STATUS
   (STATUS_ID, STATUS_NAME, DESCRIPTION, ACTIVE_FLG)
 Values
   (1, 'Proposed', NULL, 'Y');
Insert into STATUS
   (STATUS_ID, STATUS_NAME, DESCRIPTION, ACTIVE_FLG)
 Values
   (2, 'Approved', NULL, 'Y');
Insert into STATUS
   (STATUS_ID, STATUS_NAME, DESCRIPTION, ACTIVE_FLG)
 Values
   (3, 'Completed', NULL, 'Y');
Insert into STATUS
   (STATUS_ID, STATUS_NAME, DESCRIPTION, ACTIVE_FLG)
 Values
   (4, 'Cancelled', NULL, 'Y');
COMMIT;
