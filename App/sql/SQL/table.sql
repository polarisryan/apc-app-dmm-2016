CREATE TABLE COUNTRY
(
  COUNTRY_ID          NUMBER(10)                NOT NULL,
  COUNTRY_NAME        VARCHAR2(100 BYTE),
  COUNTRY_CODE        VARCHAR2(2 BYTE),
  ACTIVE_FLG          VARCHAR2(1 BYTE)          DEFAULT 'Y',
  ACTIVE_COUNTRY_FLG  VARCHAR2(1 BYTE)          DEFAULT 'Y'
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX COUNTRY_PK ON COUNTRY
(COUNTRY_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE COUNTRY ADD (
  CONSTRAINT COUNTRY_PK
  PRIMARY KEY
  (COUNTRY_ID)
  USING INDEX COUNTRY_PK
  ENABLE VALIDATE);
CREATE TABLE CORRUGATE
(
  CORRUGATE_ID    NUMBER(10),
  DISPLAY_ID      NUMBER(10),
  CREATE_DATE     DATE,
  CORRUGATE_COST  NUMBER(12,2)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX CORRUGATE_PK ON CORRUGATE
(CORRUGATE_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE CORRUGATE ADD (
  CONSTRAINT CORRUGATE_PK
  PRIMARY KEY
  (CORRUGATE_ID)
  USING INDEX CORRUGATE_PK
  ENABLE VALIDATE);


ALTER TABLE CORRUGATE ADD (
  CONSTRAINT CORRUGATE_DISPLAY_ID_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);
CREATE TABLE CORRUGATE_COMPONENT
(
  CORRUGATE_COMPONENT_ID  NUMBER(10)            NOT NULL,
  DISPLAY_ID              NUMBER(10),
  COUNTRY_ID              NUMBER(10),
  MANUFACTURER_ID         NUMBER(10),
  VENDOR_PART_NUM         VARCHAR2(30 BYTE),
  GLOVIA_PART_NUM         VARCHAR2(20 BYTE),
  DESCRIPTION             VARCHAR2(200 BYTE),
  BOARD_TEST              VARCHAR2(30 BYTE),
  MATERIAL                VARCHAR2(30 BYTE),
  QTY_PER_DISPLAY         NUMBER(10),
  PIECE_PRICE             NUMBER(12,2),
  TOTAL_COST              NUMBER(12,2),
  TOTAL_QTY               NUMBER(10),
  DT_CREATED              DATE,
  DT_LAST_CHANGED         DATE,
  USER_CREATED            VARCHAR2(100 BYTE),
  USER_LAST_CHANGED       VARCHAR2(100 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX CORRUGATE_COMPONENT_PK ON CORRUGATE_COMPONENT
(CORRUGATE_COMPONENT_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER corrugate_component_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON corrugate_component
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'corrugate_component', 'new corrugate_component record',
                   'N/A', 'N/A', :NEW.display_id, v_type,
                   :NEW.corrugate_component_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'corrugate_component',
                   'deleting corrugate_component record', 'N/A', 'N/A',
                   :OLD.corrugate_component_id, v_type,
                   :OLD.corrugate_component_id
                  );
   ELSIF UPDATING
   THEN
      IF NVL (:NEW.vendor_part_num, 'null') <>
                                           NVL (:OLD.vendor_part_num, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'corrugate_component', 'vendor_part_num',
                      :OLD.vendor_part_num, :NEW.vendor_part_num,
                      :NEW.corrugate_component_id, v_type,
                      :OLD.corrugate_component_id
                     );
      END IF;

      IF NVL (:NEW.glovia_part_num, 'null') <>
                                            NVL (:OLD.glovia_part_num, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'corrugate_component', 'glovia_part_num',
                      :OLD.glovia_part_num, :NEW.glovia_part_num,
                      :NEW.corrugate_component_id, v_type,
                      :OLD.corrugate_component_id
                     );
      END IF;

      IF NVL (:NEW.description, 'null') <> NVL (:OLD.description, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'corrugate_component', 'description',
                      :OLD.description, :NEW.description,
                      :NEW.corrugate_component_id, v_type,
                      :OLD.corrugate_component_id
                     );
      END IF;

      IF NVL (:NEW.board_test, 'null') <> NVL (:OLD.board_test, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'corrugate_component', 'board_test',
                      :OLD.board_test, :NEW.board_test,
                      :NEW.corrugate_component_id, v_type,
                      :OLD.corrugate_component_id
                     );
      END IF;

      IF NVL (:NEW.material, 'null') <> NVL (:OLD.material, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'corrugate_component', 'material',
                      :OLD.material, :NEW.material,
                      :NEW.corrugate_component_id, v_type,
                      :OLD.corrugate_component_id
                     );
      END IF;

      IF NVL (:NEW.qty_per_display, 0) <> NVL (:OLD.qty_per_display, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'corrugate_component', 'qty_per_display',
                      :OLD.qty_per_display, :NEW.qty_per_display,
                      :NEW.corrugate_component_id, v_type,
                      :OLD.corrugate_component_id
                     );
      END IF;

      IF NVL (:NEW.piece_price, 0) <> NVL (:OLD.piece_price, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'corrugate_component', 'piece_price',
                      :OLD.piece_price, :NEW.piece_price,
                      :NEW.corrugate_component_id, v_type,
                      :OLD.corrugate_component_id
                     );
      END IF;

      IF NVL (:NEW.total_cost, 0) <> NVL (:OLD.total_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'corrugate_component', 'total_cost',
                      :OLD.total_cost, :NEW.total_cost,
                      :NEW.corrugate_component_id, v_type,
                      :OLD.corrugate_component_id
                     );
      END IF;

      IF NVL (:NEW.total_qty, 0) <> NVL (:OLD.total_qty, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'corrugate_component', 'total_qty',
                      :OLD.total_qty, :NEW.total_qty,
                      :NEW.corrugate_component_id, v_type,
                      :OLD.corrugate_component_id
                     );
      END IF;
   END IF;
END corrugate_component_trig;
/


ALTER TABLE CORRUGATE_COMPONENT ADD (
  CONSTRAINT CORRUGATE_COMPONENT_PK
  PRIMARY KEY
  (CORRUGATE_COMPONENT_ID)
  USING INDEX CORRUGATE_COMPONENT_PK
  ENABLE VALIDATE);


ALTER TABLE CORRUGATE_COMPONENT ADD (
  CONSTRAINT CORRUGATE_COMP_COUNTRY_FK 
  FOREIGN KEY (COUNTRY_ID) 
  REFERENCES COUNTRY (COUNTRY_ID)
  ENABLE VALIDATE);

ALTER TABLE CORRUGATE_COMPONENT ADD (
  CONSTRAINT CORRUGATE_COMP_MANUF_ID_FK 
  FOREIGN KEY (MANUFACTURER_ID) 
  REFERENCES MANUFACTURER (MANUFACTURER_ID)
  ENABLE VALIDATE);

ALTER TABLE CORRUGATE_COMPONENT ADD (
  CONSTRAINT DISPLAY_CORRUG_COMP_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);
CREATE TABLE EXCESS_INVENTORY_TYPE
(
  EXCESS_INVENTORY_TYPE_ID  NUMBER(10),
  DESCRIPTION               VARCHAR2(50 BYTE),
  ACTIVE_FLG                VARCHAR2(1 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX EXCESS_INVENTORY_TYPE_PK ON EXCESS_INVENTORY_TYPE
(EXCESS_INVENTORY_TYPE_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE EXCESS_INVENTORY_TYPE ADD (
  CONSTRAINT EXCESS_INVENTORY_TYPE_PK
  PRIMARY KEY
  (EXCESS_INVENTORY_TYPE_ID)
  USING INDEX EXCESS_INVENTORY_TYPE_PK
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_SALES_REP
(
  DISPLAY_SALES_REP_ID  NUMBER(10),
  FIRST_NAME            VARCHAR2(25 BYTE),
  LAST_NAME             VARCHAR2(25 BYTE),
  REP_CODE              VARCHAR2(5 BYTE),
  ACTIVE_FLG            VARCHAR2(1 BYTE)        DEFAULT 'Y'
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_SALES_REP_PK ON DISPLAY_SALES_REP
(DISPLAY_SALES_REP_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE DISPLAY_SALES_REP ADD (
  CONSTRAINT DISPLAY_SALES_REP_PK
  PRIMARY KEY
  (DISPLAY_SALES_REP_ID)
  USING INDEX DISPLAY_SALES_REP_PK
  ENABLE VALIDATE);
CREATE TABLE MANUFACTURER
(
  MANUFACTURER_ID  NUMBER(10),
  NAME             VARCHAR2(100 BYTE),
  ACTIVE_FLG       VARCHAR2(1 BYTE)             DEFAULT 'Y'
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX MANUFACTURER_PK ON MANUFACTURER
(MANUFACTURER_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE MANUFACTURER ADD (
  CONSTRAINT MANUFACTURER_PK
  PRIMARY KEY
  (MANUFACTURER_ID)
  USING INDEX MANUFACTURER_PK
  ENABLE VALIDATE);
CREATE TABLE LOCATION
(
  LOCATION_ID  NUMBER(10),
  DC_CODE      VARCHAR2(2 BYTE),
  DC_NAME      VARCHAR2(50 BYTE),
  ACTIVE_FLG   VARCHAR2(1 BYTE)                 DEFAULT 'N'
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX LOCATION_PK ON LOCATION
(LOCATION_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE LOCATION ADD (
  CONSTRAINT LOCATION_PK
  PRIMARY KEY
  (LOCATION_ID)
  USING INDEX LOCATION_PK
  ENABLE VALIDATE);
CREATE TABLE STATUS
(
  STATUS_ID    NUMBER(10),
  STATUS_NAME  VARCHAR2(25 BYTE),
  DESCRIPTION  VARCHAR2(200 BYTE),
  ACTIVE_FLG   VARCHAR2(1 BYTE)                 DEFAULT 'Y'
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX STATUS_PK ON STATUS
(STATUS_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE STATUS ADD (
  CONSTRAINT STATUS_PK
  PRIMARY KEY
  (STATUS_ID)
  USING INDEX STATUS_PK
  ENABLE VALIDATE);
CREATE TABLE ACCESS_PRIV
(
  ACCESS_ID           NUMBER(10),
  ACCESS_DESCRIPTION  VARCHAR2(200 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX ACCESS_PK ON ACCESS_PRIV
(ACCESS_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE ACCESS_PRIV ADD (
  CONSTRAINT ACCESS_PK
  PRIMARY KEY
  (ACCESS_ID)
  USING INDEX ACCESS_PK
  ENABLE VALIDATE);
CREATE TABLE CUSTOMER
(
  CUSTOMER_ID           NUMBER(10),
  CUSTOMER_NAME         VARCHAR2(25 BYTE),
  CUSTOMER_DESCRIPTION  VARCHAR2(200 BYTE),
  CUSTOMER_TYPE_ID      NUMBER(10),
  PARENT_ID             NUMBER(10),
  ACTIVE_FLG            VARCHAR2(1 BYTE)        DEFAULT 'Y',
  COMPANY_CODE          VARCHAR2(3 BYTE),
  CUSTOMER_CODE         VARCHAR2(8 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX CUSTOMER_PK ON CUSTOMER
(CUSTOMER_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE CUSTOMER ADD (
  CONSTRAINT CUSTOMER_PK
  PRIMARY KEY
  (CUSTOMER_ID)
  USING INDEX CUSTOMER_PK
  ENABLE VALIDATE);


ALTER TABLE CUSTOMER ADD (
  CONSTRAINT CUSTOMER_TYPE_FK 
  FOREIGN KEY (CUSTOMER_TYPE_ID) 
  REFERENCES CUSTOMER_TYPE (CUSTOMER_TYPE_ID)
  ENABLE VALIDATE);
CREATE TABLE CUSTOMER_TYPE
(
  CUSTOMER_TYPE_ID  NUMBER(10),
  TYPE_NAME         VARCHAR2(100 BYTE),
  ACTIVE_FLG        VARCHAR2(1 BYTE)            DEFAULT 'Y'
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX CUSTOMER_TYPE_PK ON CUSTOMER_TYPE
(CUSTOMER_TYPE_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE CUSTOMER_TYPE ADD (
  CONSTRAINT CUSTOMER_TYPE_PK
  PRIMARY KEY
  (CUSTOMER_TYPE_ID)
  USING INDEX CUSTOMER_TYPE_PK
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_SETUP
(
  DISPLAY_SETUP_ID    NUMBER(10),
  DISPLAY_SETUP_DESC  VARCHAR2(200 BYTE),
  ACTIVE_FLG          VARCHAR2(1 BYTE)          DEFAULT 'Y'
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_SETUP_PK ON DISPLAY_SETUP
(DISPLAY_SETUP_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE DISPLAY_SETUP ADD (
  CONSTRAINT DISPLAY_SETUP_PK
  PRIMARY KEY
  (DISPLAY_SETUP_ID)
  USING INDEX DISPLAY_SETUP_PK
  ENABLE VALIDATE);
CREATE TABLE PROMO_PERIOD
(
  PROMO_PERIOD_ID           NUMBER(10),
  PROMO_PERIOD_NAME         VARCHAR2(50 BYTE),
  PROMO_PERIOD_DESCRIPTION  VARCHAR2(200 BYTE),
  PROMO_DATE                DATE,
  ACTIVE_FLG                VARCHAR2(1 BYTE)    DEFAULT 'Y'
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX PROMO_PERIOD_PK ON PROMO_PERIOD
(PROMO_PERIOD_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE PROMO_PERIOD ADD (
  CONSTRAINT PROMO_PERIOD_PK
  PRIMARY KEY
  (PROMO_PERIOD_ID)
  USING INDEX PROMO_PERIOD_PK
  ENABLE VALIDATE);
CREATE TABLE STRUCTURE
(
  STRUCTURE_ID    NUMBER(10),
  STRUCTURE_NAME  VARCHAR2(50 BYTE),
  STRUCTURE_DESC  VARCHAR2(200 BYTE),
  ACTIVE_FLG      VARCHAR2(1 BYTE)              DEFAULT 'Y'
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX STRUCTURE_PK ON STRUCTURE
(STRUCTURE_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE STRUCTURE ADD (
  CONSTRAINT STRUCTURE_PK
  PRIMARY KEY
  (STRUCTURE_ID)
  USING INDEX STRUCTURE_PK
  ENABLE VALIDATE);
CREATE TABLE USERS
(
  USER_ID          NUMBER(10),
  USERNAME         VARCHAR2(100 BYTE),
  PASSWORD         VARCHAR2(50 BYTE),
  FIRST_NAME       VARCHAR2(50 BYTE),
  LAST_NAME        VARCHAR2(25 BYTE),
  EMAIL_ADDRESS    VARCHAR2(200 BYTE),
  CREATE_DATE      DATE,
  LAST_LOGIN_DATE  DATE
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX USER_PK ON USERS
(USER_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE USERS ADD (
  CONSTRAINT USER_PK
  PRIMARY KEY
  (USER_ID)
  USING INDEX USER_PK
  ENABLE VALIDATE);
CREATE TABLE USER_ACCESS
(
  USER_ID    NUMBER(10),
  ACCESS_ID  NUMBER(10)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX USER_ACCESS_PK ON USER_ACCESS
(USER_ID, ACCESS_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE USER_ACCESS ADD (
  CONSTRAINT USER_ACCESS_PK
  PRIMARY KEY
  (USER_ID, ACCESS_ID)
  USING INDEX USER_ACCESS_PK
  ENABLE VALIDATE);


ALTER TABLE USER_ACCESS ADD (
  CONSTRAINT USER_ACCESS_ACCESS_ID_FK 
  FOREIGN KEY (ACCESS_ID) 
  REFERENCES ACCESS_PRIV (ACCESS_ID)
  ENABLE VALIDATE);

ALTER TABLE USER_ACCESS ADD (
  CONSTRAINT USER_ACCESS_USER_ID_FK 
  FOREIGN KEY (USER_ID) 
  REFERENCES USERS (USER_ID)
  ENABLE VALIDATE);
CREATE TABLE VALID_BU
(
  BU_CODE                VARCHAR2(3 BYTE),
  BU_DESC                VARCHAR2(50 BYTE),
  ACTIVE_FLG             VARCHAR2(1 BYTE)       DEFAULT 'N',
  PRODUCT_CATEGORY_CODE  VARCHAR2(10 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX PK_VALID_BU ON VALID_BU
(BU_CODE)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE VALID_BU ADD (
  CONSTRAINT PK_VALID_BU
  PRIMARY KEY
  (BU_CODE)
  USING INDEX PK_VALID_BU
  ENABLE VALIDATE);
CREATE TABLE VALID_PLANNER
(
  PLANNER_CODE  VARCHAR2(5 BYTE),
  PLANNER_DESC  VARCHAR2(50 BYTE),
  ACTIVE_FLG    VARCHAR2(1 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
CREATE TABLE VALID_PLANT
(
  PLANT_CODE             VARCHAR2(2 BYTE),
  PLANT_DESC             VARCHAR2(50 BYTE),
  ACTIVE_FLG             VARCHAR2(1 BYTE),
  ACTIVE_ON_FINANCE_FLG  VARCHAR2(1 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
CREATE TABLE KIT_COMPONENT
(
  KIT_COMPONENT_ID          NUMBER(10),
  DISPLAY_ID                NUMBER(10),
  SKU_ID                    NUMBER(10),
  QTY_PER_FACING            NUMBER(10),
  NUMBER_FACINGS            NUMBER(10),
  PLANT_BREAK_CASE_FLG      VARCHAR2(1 BYTE)    DEFAULT 'N',
  I2_FORECAST_FLG           VARCHAR2(1 BYTE)    DEFAULT 'N',
  REG_CASE_PACK             NUMBER(10),
  INVOICE_UNIT_PRICE        NUMBER(7,2),
  COGS_UNIT_PRICE           NUMBER(7,2),
  SEQUENCE_NUM              NUMBER(10),
  SOURCE_PIM_FLG            VARCHAR2(1 BYTE)    DEFAULT 'N',
  SKU                       VARCHAR2(15 BYTE),
  PRODUCT_NAME              VARCHAR2(100 BYTE),
  BU_DESC                   VARCHAR2(50 BYTE),
  VERSION_NO                VARCHAR2(5 BYTE),
  PROMO_FLG                 VARCHAR2(1 BYTE)    DEFAULT 'N',
  BREAK_CASE                VARCHAR2(20 BYTE),
  KIT_VERSION_CODE          VARCHAR2(20 BYTE),
  PBC_VERSION               VARCHAR2(5 BYTE),
  FINISHED_GOODS_SKU        VARCHAR2(15 BYTE),
  FINISHED_GOODS_QTY        NUMBER(10),
  PRICE_EXCEPTION           NUMBER(8,2),
  BENCHMARK_PER_EACH        NUMBER(8,2),
  PE_RECALC_DATE            DATE,
  DISPLAY_SCENARIO_ID       NUMBER(10),
  DT_CREATED                DATE,
  DT_LAST_CHANGED           DATE,
  USER_CREATED              VARCHAR2(100 BYTE),
  USER_LAST_CHANGED         VARCHAR2(100 BYTE),
  EXCESS_INVENTORY_TYPE_ID  NUMBER(10),
  EXCESS_INVT_DOLLAR        NUMBER(12,2),
  EXCESS_INVT_PCT           NUMBER(12,2),
  IN_POG_Y_N                VARCHAR2(1 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX KIT_COMPONENT_PK ON KIT_COMPONENT
(KIT_COMPONENT_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER kit_component_trig
   BEFORE DELETE OR INSERT OR UPDATE
   ON kit_component
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value, new_value, display_id,
                   change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'kit_component', 'sku', 'N/A', :NEW.sku, :NEW.display_id,
                   v_type, :NEW.kit_component_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value, display_id,
                   change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'kit_component', 'kit_version_code', 'Kit Version Code',
                   'KVC', 'N/A', :NEW.kit_version_code, :NEW.display_id,
                   v_type, :NEW.kit_component_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name, short_name,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'kit_component', 'i2_forecast_flg', 'I2 Forecast', 'I2',
                   'N/A', :NEW.i2_forecast_flg, :NEW.display_id, v_type,
                   :NEW.kit_component_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value, display_id,
                   change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'kit_component', 'qty_per_facing', 'Qty Per Facing',
                   'QPF', 'N/A', :NEW.qty_per_facing, :NEW.display_id,
                   v_type, :NEW.kit_component_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name, short_name,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'kit_component', 'number_facings', '# Facings', '#F',
                   'N/A', :NEW.number_facings, :NEW.display_id, v_type,
                   :NEW.kit_component_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'kit_component',
                   'deleting kit_component record', 'N/A', 'N/A',
                   :OLD.display_id, v_type, :OLD.kit_component_id
                  );
   ELSIF UPDATING
   THEN
      IF NVL (:NEW.sku_id, 0) <> NVL (:OLD.sku_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'sku_id', :OLD.sku_id,
                      :NEW.sku_id, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.qty_per_facing, 0) <> NVL (:OLD.qty_per_facing, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'qty_per_facing',
                      'Qty Per Facing', 'QPF', :OLD.qty_per_facing,
                      :NEW.qty_per_facing, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.number_facings, 0) <> NVL (:OLD.number_facings, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'number_facings',
                      '# Facings', '#F', :OLD.number_facings,
                      :NEW.number_facings, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.plant_break_case_flg, 'null') <>
                                       NVL (:OLD.plant_break_case_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'plant_break_case_flg',
                      'PBC BOM', 'PBC BOM', :OLD.plant_break_case_flg,
                      :NEW.plant_break_case_flg, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.i2_forecast_flg, 'null') <>
                                            NVL (:OLD.i2_forecast_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'i2_forecast_flg',
                      'I2 Forecast', 'I2', :OLD.i2_forecast_flg,
                      :NEW.i2_forecast_flg, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.reg_case_pack, 0) <> NVL (:OLD.reg_case_pack, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'reg_case_pack',
                      'Ret Pk/Inner Ctn', 'Ret Pk', :OLD.reg_case_pack,
                      :NEW.reg_case_pack, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.invoice_unit_price, 0) <> NVL (:OLD.invoice_unit_price, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'invoice_unit_price',
                      'Inv Unit Price', 'Inv Unit', :OLD.invoice_unit_price,
                      :NEW.invoice_unit_price, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.cogs_unit_price, 0) <> NVL (:OLD.cogs_unit_price, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'cogs_unit_price',
                      'COGS Unit Price', 'COGS', :OLD.cogs_unit_price,
                      :NEW.cogs_unit_price, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.sequence_num, 0) <> NVL (:OLD.sequence_num, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'sequence_num',
                      :OLD.sequence_num, :NEW.sequence_num, :OLD.display_id,
                      v_type, :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.source_pim_flg, 'null') <>
                                             NVL (:OLD.source_pim_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'source_pim_flg',
                      'Source=PIM', 'PIM', :OLD.source_pim_flg,
                      :NEW.source_pim_flg, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.sku, 'null') <> NVL (:OLD.sku, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'sku', 'Component SKU',
                      'Comp SKU', :OLD.sku, :NEW.sku, :OLD.display_id,
                      v_type, :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.product_name, 'null') <> NVL (:OLD.product_name, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'product_name',
                      'Component Desc', 'Desc', :OLD.product_name,
                      :NEW.product_name, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.bu_desc, 'null') <> NVL (:OLD.bu_desc, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'bu_desc', 'Business Unit',
                      'BU', :OLD.bu_desc, :NEW.bu_desc, :OLD.display_id,
                      v_type, :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.version_no, 'null') <> NVL (:OLD.version_no, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'version_no',
                      :OLD.version_no, :NEW.version_no, :OLD.display_id,
                      v_type, :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.promo_flg, 'null') <> NVL (:OLD.promo_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'promo_flg',
                      'Promo Version', 'PV', :OLD.promo_flg, :NEW.promo_flg,
                      :OLD.display_id, v_type, :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.break_case, 'null') <> NVL (:OLD.break_case, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'break_case',
                      'Version Code Setup', 'VCS', :OLD.break_case,
                      :NEW.break_case, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.kit_version_code, 'null') <>
                                           NVL (:OLD.kit_version_code, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'kit_version_code',
                      'Kit Version Code', 'KVC', :OLD.kit_version_code,
                      :NEW.kit_version_code, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.pbc_version, 'null') <> NVL (:OLD.pbc_version, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'pbc_version',
                      'PBC Version', 'PBC Vers', :OLD.pbc_version,
                      :NEW.pbc_version, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.finished_goods_sku, 'null') <>
                                         NVL (:OLD.finished_goods_sku, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'finished_goods_sku',
                      'FG Consists of SKU', 'FG SKU',
                      :OLD.finished_goods_sku, :NEW.finished_goods_sku,
                      :OLD.display_id, v_type, :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.finished_goods_qty, 0) <> NVL (:OLD.finished_goods_qty, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'finished_goods_qty',
                      'FG Quantity', 'FG Qty', :OLD.finished_goods_qty,
                      :NEW.finished_goods_qty, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.price_exception, 999999) <>
                                            NVL (:OLD.price_exception, 999999)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'price_exception',
                      'Price Exception', 'PE', :OLD.price_exception,
                      :NEW.price_exception, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.benchmark_per_each, 0) <> NVL (:OLD.benchmark_per_each, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'benchmark_per_each',
                      'Benchmark Per Each', 'BPE', :OLD.benchmark_per_each,
                      :NEW.benchmark_per_each, :OLD.display_id, v_type,
                      :OLD.kit_component_id
                     );
      END IF;

      IF NVL (:NEW.pe_recalc_date, TRUNC (SYSDATE)) <>
                                    NVL (:OLD.pe_recalc_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'kit_component', 'pe_recalc_date',
                      'Price Exception recalc date', 'PE Recalc',
                      :OLD.pe_recalc_date, :NEW.pe_recalc_date,
                      :OLD.display_id, v_type, :OLD.kit_component_id
                     );
      END IF;
   END IF;
END kit_component_trig;
/


ALTER TABLE KIT_COMPONENT ADD (
  CONSTRAINT KIT_COMPONENT_PK
  PRIMARY KEY
  (KIT_COMPONENT_ID)
  USING INDEX KIT_COMPONENT_PK
  ENABLE VALIDATE);


ALTER TABLE KIT_COMPONENT ADD (
  CONSTRAINT EXCESS_INV_TYPE_ID_FK 
  FOREIGN KEY (EXCESS_INVENTORY_TYPE_ID) 
  REFERENCES EXCESS_INVENTORY_TYPE (EXCESS_INVENTORY_TYPE_ID)
  ENABLE VALIDATE);

ALTER TABLE KIT_COMPONENT ADD (
  CONSTRAINT KIT_COMPONENT_DISPLAY_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_SHIP_WAVE
(
  DISPLAY_SHIP_WAVE_ID  NUMBER(10),
  DISPLAY_ID            NUMBER(10),
  MUST_ARRIVE_DATE      DATE,
  QUANTITY              NUMBER(10),
  SHIP_WAVE_NUM         NUMBER(10),
  DESTINATION_FLG       VARCHAR2(1 BYTE)        DEFAULT 'N',
  DT_CREATED            DATE,
  DT_LAST_CHANGED       DATE,
  USER_CREATED          VARCHAR2(100 BYTE),
  USER_LAST_CHANGED     VARCHAR2(100 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_SHIP_WAVE_PK ON DISPLAY_SHIP_WAVE
(DISPLAY_SHIP_WAVE_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER display_ship_wave_trig
   BEFORE DELETE OR INSERT OR UPDATE
   ON display_ship_wave
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_ship_wave', 'new display_ship_wave record',
                   'N/A', 'N/A', :NEW.display_id, v_type,
                   :NEW.display_ship_wave_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_ship_wave',
                   'deleting display_ship_wave record', 'N/A', 'N/A',
                   :OLD.display_id, v_type, :OLD.display_ship_wave_id
                  );
   ELSIF UPDATING
   THEN
      IF NVL (:NEW.must_arrive_date, TRUNC (SYSDATE)) <>
                                 NVL (:OLD.must_arrive_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_ship_wave', 'must_arrive_date',
                      'Must Arrive Date', 'Must Arrive',
                      :OLD.must_arrive_date, :NEW.must_arrive_date,
                      :OLD.display_id, v_type, :OLD.display_ship_wave_id
                     );
      END IF;

      IF NVL (:NEW.quantity, 0) <> NVL (:OLD.quantity, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_ship_wave', 'quantity',
                      'Ship Wave Quantity', 'SW Qty', :OLD.quantity,
                      :NEW.quantity, :OLD.display_id, v_type,
                      :OLD.display_ship_wave_id
                     );
      END IF;

      IF NVL (:NEW.ship_wave_num, 0) <> NVL (:OLD.ship_wave_num, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_ship_wave', 'ship_wave_num',
                      'Shipping Wave', 'SW', :OLD.ship_wave_num,
                      :NEW.ship_wave_num, :OLD.display_id, v_type,
                      :OLD.display_ship_wave_id
                     );
      END IF;

      IF NVL (:NEW.destination_flg, 'null') <>
                                            NVL (:OLD.destination_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_ship_wave', 'destination_flg',
                      'Offshore Ind', 'Offshore', :OLD.destination_flg,
                      :NEW.destination_flg, :OLD.display_id, v_type,
                      :OLD.display_ship_wave_id
                     );
      END IF;
   END IF;
END display_ship_wave_trig;
/


ALTER TABLE DISPLAY_SHIP_WAVE ADD (
  CONSTRAINT DISPLAY_SHIP_WAVE_PK
  PRIMARY KEY
  (DISPLAY_SHIP_WAVE_ID)
  USING INDEX DISPLAY_SHIP_WAVE_PK
  ENABLE VALIDATE);


ALTER TABLE DISPLAY_SHIP_WAVE ADD (
  CONSTRAINT DISPLAY_SHIP_WAVE_ID_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_PLANNER
(
  DISPLAY_PLANNER_ID  NUMBER(10),
  KIT_COMPONENT_ID    NUMBER(10),
  FIRST_NAME          VARCHAR2(25 BYTE),
  LAST_NAME           VARCHAR2(25 BYTE),
  INITIALS            VARCHAR2(5 BYTE),
  PLANNER_CODE        VARCHAR2(5 BYTE),
  PLANT_CODE          VARCHAR2(50 BYTE),
  LEAD_TIME           VARCHAR2(3 BYTE),
  PLANT_OVERRIDE_FLG  VARCHAR2(1 BYTE)          DEFAULT 'N',
  DISPLAY_DC_ID       NUMBER(10),
  DT_CREATED          DATE,
  DT_LAST_CHANGED     DATE,
  USER_CREATED        VARCHAR2(100 BYTE),
  USER_LAST_CHANGED   VARCHAR2(100 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_PLANNER_PK ON DISPLAY_PLANNER
(DISPLAY_PLANNER_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER display_planner_trig
   BEFORE DELETE OR INSERT OR UPDATE
   ON display_planner
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   v_type         VARCHAR2 (6);
   v_display_id   NUMBER (10);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      SELECT NVL (a.display_id, 0)
        INTO v_display_id
        FROM kit_component a
       WHERE a.kit_component_id = :NEW.kit_component_id;

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value, display_id,
                   change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_planner', 'display_dc_id', 'Packout Location',
                   'PO Location', 'N/A', :NEW.display_dc_id, v_display_id,
                   v_type, :NEW.display_planner_id
                  );
   ELSIF DELETING
   THEN
      SELECT NVL (a.display_id, 0)
        INTO v_display_id
        FROM kit_component a
       WHERE a.kit_component_id = :OLD.kit_component_id;

      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_planner',
                   'deleting display_planner record', 'N/A', 'N/A',
                   v_display_id, v_type, :OLD.display_planner_id
                  );
   ELSIF UPDATING
   THEN
      SELECT NVL (a.display_id, 0)
        INTO v_display_id
        FROM kit_component a
       WHERE a.kit_component_id = :OLD.kit_component_id;

      IF NVL (:NEW.display_dc_id, 0) <> NVL (:OLD.display_dc_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'display_dc_id',
                      'Packout Location', 'PO Location', :OLD.display_dc_id,
                      :NEW.display_dc_id, v_display_id, v_type,
                      :OLD.display_planner_id
                     );
      END IF;

      IF NVL (:NEW.first_name, 'null') <> NVL (:OLD.first_name, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'first_name',
                      :OLD.first_name, :NEW.first_name, v_display_id,
                      v_type, :OLD.display_planner_id
                     );
      END IF;

      IF NVL (:NEW.last_name, 'null') <> NVL (:OLD.last_name, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'last_name',
                      :OLD.last_name, :NEW.last_name, v_display_id, v_type,
                      :OLD.display_planner_id
                     );
      END IF;

      IF NVL (:NEW.initials, 'null') <> NVL (:OLD.initials, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'initials', :OLD.initials,
                      :NEW.initials, v_display_id, v_type,
                      :OLD.display_planner_id
                     );
      END IF;

      IF NVL (:NEW.planner_code, 'null') <> NVL (:OLD.planner_code, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'planner_code', 'Planner',
                      :OLD.planner_code, :NEW.planner_code, v_display_id,
                      v_type, :OLD.display_planner_id
                     );
      END IF;

      IF NVL (:NEW.plant_code, 'null') <> NVL (:OLD.plant_code, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'plant_code', 'Plant',
                      :OLD.plant_code, :NEW.plant_code, v_display_id,
                      v_type, :OLD.display_planner_id
                     );
      END IF;

      IF NVL (:NEW.lead_time, 'null') <> NVL (:OLD.lead_time, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'lead_time',
                      :OLD.lead_time, :NEW.lead_time, v_display_id, v_type,
                      :OLD.display_planner_id
                     );
      END IF;

      IF NVL (:NEW.plant_override_flg, 'null') <>
                                         NVL (:OLD.plant_override_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_planner', 'plant_override_flg',
                      :OLD.plant_override_flg, :NEW.plant_override_flg,
                      v_display_id, v_type, :OLD.display_planner_id
                     );
      END IF;
   END IF;
END display_planner_trig;
/


ALTER TABLE DISPLAY_PLANNER ADD (
  CONSTRAINT DISPLAY_PLANNER_PK
  PRIMARY KEY
  (DISPLAY_PLANNER_ID)
  USING INDEX DISPLAY_PLANNER_PK
  ENABLE VALIDATE);


ALTER TABLE DISPLAY_PLANNER ADD (
  CONSTRAINT DISPLAY_DC_ID_FK 
  FOREIGN KEY (DISPLAY_DC_ID) 
  REFERENCES DISPLAY_DC (DISPLAY_DC_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY_PLANNER ADD (
  CONSTRAINT PLANNER_KIT_FK 
  FOREIGN KEY (KIT_COMPONENT_ID) 
  REFERENCES KIT_COMPONENT (KIT_COMPONENT_ID)
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_PACKOUT
(
  DISPLAY_PACKOUT_ID         NUMBER(10),
  DISPLAY_SHIP_WAVE_ID       NUMBER(10),
  DISPLAY_DC_ID              NUMBER(10),
  SHIP_FROM_DATE             DATE,
  SHIP_FROM_LOCATION_ID      NUMBER(10),
  SHIP_LOC_PRODUCT_DUE_DATE  DATE,
  PRODUCT_DUE_DATE           DATE,
  CORRUGATE_DUE_DATE         DATE,
  QUANTITY                   NUMBER(10),
  DELIVERY_DATE              DATE,
  DT_CREATED                 DATE,
  DT_LAST_CHANGED            DATE,
  USER_CREATED               VARCHAR2(100 BYTE),
  USER_LAST_CHANGED          VARCHAR2(100 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_PACKOUT_PK ON DISPLAY_PACKOUT
(DISPLAY_PACKOUT_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER display_packout_trig
   BEFORE DELETE OR INSERT OR UPDATE
   ON display_packout
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   v_type         VARCHAR2 (6);
   v_display_id   NUMBER (10);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      SELECT NVL (display_id, 0)
        INTO v_display_id
        FROM display_ship_wave
       WHERE display_ship_wave_id = :NEW.display_ship_wave_id;

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value, display_id,
                   change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_packout', 'ship_from_date', 'Ship From Date',
                   'Ship Date', 'N/A', :NEW.ship_from_date, v_display_id,
                   v_type, :NEW.display_packout_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD,
                   friendly_name, short_name, old_value,
                   new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_packout', 'product_due_date',
                   'Product Due at Packout Date', 'Product Due', 'N/A',
                   :NEW.product_due_date, v_display_id, v_type,
                   :NEW.display_packout_id
                  );
   ELSIF DELETING
   THEN
      SELECT NVL (display_id, 0)
        INTO v_display_id
        FROM display_ship_wave
       WHERE display_ship_wave_id = :OLD.display_ship_wave_id;

      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_packout',
                   'deleting display_packout record', 'N/A', 'N/A',
                   v_display_id, v_type, :OLD.display_packout_id
                  );
   ELSIF UPDATING
   THEN
      SELECT NVL (display_id, 0)
        INTO v_display_id
        FROM display_ship_wave
       WHERE display_ship_wave_id = :OLD.display_ship_wave_id;

      IF NVL (:NEW.display_dc_id, 0) <> NVL (:OLD.display_dc_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout', 'display_dc_id',
                      :OLD.display_dc_id, :NEW.display_dc_id, v_display_id,
                      v_type, :OLD.display_packout_id
                     );
      END IF;

      IF NVL (:NEW.ship_from_date, TRUNC (SYSDATE)) <>
                                    NVL (:OLD.ship_from_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout', 'ship_from_date',
                      'Ship From Date', 'Ship Date', :OLD.ship_from_date,
                      :NEW.ship_from_date, v_display_id, v_type,
                      :OLD.display_packout_id
                     );
      END IF;

      IF NVL (:NEW.product_due_date, TRUNC (SYSDATE)) <>
                                  NVL (:OLD.product_due_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout', 'product_due_date',
                      'Product Due at Packout Date', 'Product Due',
                      :OLD.product_due_date, :NEW.product_due_date,
                      v_display_id, v_type, :OLD.display_packout_id
                     );
      END IF;

      IF NVL (:NEW.quantity, 0) <> NVL (:OLD.quantity, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout', 'quantity',
                      'Packout Detail Quantity', 'Packout Detail Qty',
                      :OLD.quantity, :NEW.quantity, v_display_id, v_type,
                      :OLD.display_packout_id
                     );
      END IF;

      IF NVL (:NEW.ship_from_location_id, 0) <>
                                           NVL (:OLD.ship_from_location_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout', 'ship_from_location_id',
                      'Ship From Location', 'Ship Loc',
                      :OLD.ship_from_location_id,
                      :NEW.ship_from_location_id, v_display_id, v_type,
                      :OLD.display_packout_id
                     );
      END IF;

      IF NVL (:NEW.corrugate_due_date, TRUNC (SYSDATE)) <>
                                NVL (:OLD.corrugate_due_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout', 'corrugate_due_date',
                      'Corrugate Due at Packout Date', 'Corrugate Due',
                      :OLD.corrugate_due_date, :NEW.corrugate_due_date,
                      v_display_id, v_type, :OLD.display_packout_id
                     );
      END IF;

      IF NVL (:NEW.ship_loc_product_due_date, TRUNC (SYSDATE)) <>
                         NVL (:OLD.ship_loc_product_due_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name,
                      FIELD,
                      friendly_name, short_name,
                      old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_packout',
                      'ship_loc_product_due_date',
                      'Product due at Ship Location', 'Product Due',
                      :OLD.ship_loc_product_due_date,
                      :NEW.ship_loc_product_due_date, v_display_id, v_type,
                      :OLD.display_packout_id
                     );
      END IF;
   END IF;
END display_packout_trig;
/


ALTER TABLE DISPLAY_PACKOUT ADD (
  CONSTRAINT DISPLAY_PACKOUT_PK
  PRIMARY KEY
  (DISPLAY_PACKOUT_ID)
  USING INDEX DISPLAY_PACKOUT_PK
  ENABLE VALIDATE);


ALTER TABLE DISPLAY_PACKOUT ADD (
  CONSTRAINT DISPLAY_PACKOUT_DC_FK 
  FOREIGN KEY (DISPLAY_DC_ID) 
  REFERENCES DISPLAY_DC (DISPLAY_DC_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY_PACKOUT ADD (
  CONSTRAINT DISPLAY_PACKOUT_LOC_ID_FK 
  FOREIGN KEY (SHIP_FROM_LOCATION_ID) 
  REFERENCES LOCATION (LOCATION_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY_PACKOUT ADD (
  CONSTRAINT DISPLAY_PACKOUT_WAVE_FK 
  FOREIGN KEY (DISPLAY_SHIP_WAVE_ID) 
  REFERENCES DISPLAY_SHIP_WAVE (DISPLAY_SHIP_WAVE_ID)
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_MATERIAL
(
  DISPLAY_MATERIAL_ID  NUMBER(10),
  DISPLAY_ID           NUMBER(10),
  VENDOR_PART_NUM      VARCHAR2(20 BYTE),
  GLOVIA_PART          VARCHAR2(20 BYTE),
  DESCRIPTION          VARCHAR2(200 BYTE),
  MANUFACTURER_ID      NUMBER(10),
  UNIT_COST            NUMBER(7,2),
  DELIVERY_DATE        DATE,
  QUANTITY             NUMBER(10),
  SEQUENCE_NUM         NUMBER(10),
  DT_CREATED           DATE,
  DT_LAST_CHANGED      DATE,
  USER_CREATED         VARCHAR2(100 BYTE),
  USER_LAST_CHANGED    VARCHAR2(100 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_MATERIAL_PK ON DISPLAY_MATERIAL
(DISPLAY_MATERIAL_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER display_material_trig
   BEFORE DELETE OR INSERT OR UPDATE
   ON display_material
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value,
                   new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_material', 'delivery_date', 'N/A',
                   :NEW.delivery_date, :NEW.display_id, v_type,
                   :NEW.display_material_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_material',
                   'deleting display_material record', 'N/A', 'N/A',
                   :OLD.display_id, v_type, :OLD.display_material_id
                  );
   ELSIF UPDATING
   THEN
      IF NVL (:NEW.vendor_part_num, 'null') <>
                                           NVL (:OLD.vendor_part_num, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'vendor_part_num',
                      :OLD.vendor_part_num, :NEW.vendor_part_num,
                      :OLD.display_id, v_type, :OLD.display_material_id
                     );
      END IF;

      IF NVL (:NEW.glovia_part, 'null') <> NVL (:OLD.glovia_part, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'glovia_part',
                      :OLD.glovia_part, :NEW.glovia_part, :OLD.display_id,
                      v_type, :OLD.display_material_id
                     );
      END IF;

      IF NVL (:NEW.description, 'null') <> NVL (:OLD.description, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'description',
                      :OLD.description, :NEW.description, :OLD.display_id,
                      v_type, :OLD.display_material_id
                     );
      END IF;

      IF NVL (:NEW.manufacturer_id, 0) <> NVL (:OLD.manufacturer_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'manufacturer_id',
                      :OLD.manufacturer_id, :NEW.manufacturer_id,
                      :OLD.display_id, v_type, :OLD.display_material_id
                     );
      END IF;

      IF NVL (:NEW.unit_cost, 0) <> NVL (:OLD.unit_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'unit_cost',
                      :OLD.unit_cost, :NEW.unit_cost, :OLD.display_id,
                      v_type, :OLD.display_material_id
                     );
      END IF;

      IF NVL (:NEW.delivery_date, TRUNC (SYSDATE)) <>
                                     NVL (:OLD.delivery_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'delivery_date',
                      :OLD.delivery_date, :NEW.delivery_date,
                      :OLD.display_id, v_type, :OLD.display_material_id
                     );
      END IF;

      IF NVL (:NEW.quantity, 0) <> NVL (:OLD.quantity, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'quantity',
                      :OLD.quantity, :NEW.quantity, :OLD.display_id, v_type,
                      :OLD.display_material_id
                     );
      END IF;

      IF NVL (:NEW.sequence_num, 0) <> NVL (:OLD.sequence_num, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_material', 'sequence_num',
                      :OLD.sequence_num, :NEW.sequence_num, :OLD.display_id,
                      v_type, :OLD.display_material_id
                     );
      END IF;
   END IF;
END display_material_trig;
/


ALTER TABLE DISPLAY_MATERIAL ADD (
  CONSTRAINT DISPLAY_MATERIAL_PK
  PRIMARY KEY
  (DISPLAY_MATERIAL_ID)
  USING INDEX DISPLAY_MATERIAL_PK
  ENABLE VALIDATE);


ALTER TABLE DISPLAY_MATERIAL ADD (
  CONSTRAINT DISPLAY_MATERIAL_DISPLAY_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY_MATERIAL ADD (
  CONSTRAINT DISPLAY_MATERIAL_MANUFA_FK 
  FOREIGN KEY (MANUFACTURER_ID) 
  REFERENCES MANUFACTURER (MANUFACTURER_ID)
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_LOGISTICS
(
  DISPLAY_LOGISTICS_ID  NUMBER(10),
  DISPLAY_ID            NUMBER(10),
  DISPLAY_LENGTH        NUMBER(7,3),
  DISPLAY_WIDTH         NUMBER(7,3),
  DISPLAY_HEIGHT        NUMBER(7,3),
  DISPLAY_WEIGHT        NUMBER(7,3),
  DISPLAY_PER_LAYER     NUMBER(10),
  LAYERS_PER_PALLET     NUMBER(10),
  PALLETS_PER_TRAILER   NUMBER(10),
  DOUBLE_STACK_FLG      VARCHAR2(1 BYTE)        DEFAULT 'N',
  SPECIAL_LABEL_RQT     VARCHAR2(25 BYTE),
  SERIAL_SHIP_FLG       VARCHAR2(1 BYTE)        DEFAULT 'N',
  UNITS                 VARCHAR2(10 BYTE),
  DT_CREATED            DATE,
  DT_LAST_CHANGED       DATE,
  USER_CREATED          VARCHAR2(25 BYTE),
  USER_LAST_CHANGED     VARCHAR2(25 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_LOGISTICS_PK ON DISPLAY_LOGISTICS
(DISPLAY_LOGISTICS_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER display_logistics_trig
   BEFORE DELETE OR INSERT OR UPDATE
   ON display_logistics
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_logistics', 'new display_logistics record',
                   'N/A', 'N/A', :NEW.display_id, v_type,
                   :NEW.display_logistics_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_logistics',
                   'deleting display_logistics_record', 'N/A', 'N/A',
                   :OLD.display_id, v_type, :OLD.display_logistics_id
                  );
   ELSIF UPDATING
   THEN
      IF NVL (:NEW.display_length, 0) <> NVL (:OLD.display_length, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'display_length',
                      'Length(in)', 'L', :OLD.display_length,
                      :NEW.display_length, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      END IF;

      IF NVL (:NEW.display_width, 0) <> NVL (:OLD.display_width, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'display_width',
                      'Width(in)', 'W', :OLD.display_width,
                      :NEW.display_width, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      END IF;

      IF NVL (:NEW.display_height, 0) <> NVL (:OLD.display_height, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'display_height',
                      'Height(in)', 'H', :OLD.display_height,
                      :NEW.display_height, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      END IF;

      IF NVL (:NEW.display_weight, 0) <> NVL (:OLD.display_weight, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'display_weight',
                      'Weight(in)', 'Wt', :OLD.display_weight,
                      :NEW.display_weight, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      END IF;

      IF NVL (:NEW.display_per_layer, 0) <> NVL (:OLD.display_per_layer, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'display_per_layer',
                      'Displays Per Layer', 'Disp/Layer',
                      :OLD.display_per_layer, :NEW.display_per_layer,
                      :OLD.display_id, v_type, :OLD.display_logistics_id
                     );
      END IF;

      IF NVL (:NEW.layers_per_pallet, 0) <> NVL (:OLD.layers_per_pallet, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'layers_per_pallet',
                      'Layers Per Pallet', 'Layers/Pallet',
                      :OLD.layers_per_pallet, :NEW.layers_per_pallet,
                      :OLD.display_id, v_type, :OLD.display_logistics_id
                     );
      END IF;

      IF NVL (:NEW.pallets_per_trailer, 0) <>
                                             NVL (:OLD.pallets_per_trailer, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'pallets_per_trailer',
                      'Pallets Per Trailer', 'Pallets/Trailer',
                      :OLD.pallets_per_trailer, :NEW.pallets_per_trailer,
                      :OLD.display_id, v_type, :OLD.display_logistics_id
                     );
      END IF;

      IF NVL (:NEW.double_stack_flg, 'null') <>
                                           NVL (:OLD.double_stack_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'double_stack_flg',
                      'Double Stack', 'Dbl Stk', :OLD.double_stack_flg,
                      :NEW.double_stack_flg, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      END IF;

      IF NVL (:NEW.special_label_rqt, 'null') <>
                                          NVL (:OLD.special_label_rqt, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'special_label_rqt',
                      'Placard/Special Label Reqt', 'Spc Label Rqt',
                      :OLD.special_label_rqt, :NEW.special_label_rqt,
                      :OLD.display_id, v_type, :OLD.display_logistics_id
                     );
      END IF;

      IF NVL (:NEW.serial_ship_flg, 'null') <>
                                            NVL (:OLD.serial_ship_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'serial_ship_flg',
                      'Serial Ship', 'Serial Ship', :OLD.serial_ship_flg,
                      :NEW.serial_ship_flg, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      END IF;

      IF NVL (:NEW.units, 'null') <> NVL (:OLD.units, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_logistics', 'units', :OLD.units,
                      :NEW.units, :OLD.display_id, v_type,
                      :OLD.display_logistics_id
                     );
      END IF;
   END IF;
END display_logistics_trig;
/


ALTER TABLE DISPLAY_LOGISTICS ADD (
  CONSTRAINT DISPLAY_LOGISTICS_PK
  PRIMARY KEY
  (DISPLAY_LOGISTICS_ID)
  USING INDEX DISPLAY_LOGISTICS_PK
  ENABLE VALIDATE);


ALTER TABLE DISPLAY_LOGISTICS ADD (
  CONSTRAINT DISPLAY_LOGISTICS_DISP_ID_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_DC
(
  DISPLAY_DC_ID      NUMBER(10),
  DISPLAY_ID         NUMBER(10),
  LOCATION_ID        NUMBER(10),
  QUANTITY           NUMBER(10),
  DT_CREATED         DATE,
  DT_LAST_CHANGED    DATE,
  USER_CREATED       VARCHAR2(100 BYTE),
  USER_LAST_CHANGED  VARCHAR2(100 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_DC_PK ON DISPLAY_DC
(DISPLAY_DC_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER display_dc_trig
   BEFORE DELETE OR INSERT OR UPDATE
   ON display_dc
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name, short_name,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_dc', 'location_id', 'Pack-Out Location', 'DC',
                   'N/A', :NEW.location_id, :NEW.display_id, v_type,
                   :NEW.display_dc_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name, FIELD,
                   old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_dc', 'deleting display_dc record',
                   'N/A', 'N/A', :OLD.display_id, v_type,
                   :OLD.display_dc_id
                  );
   ELSIF UPDATING
   THEN
      IF NVL (:NEW.location_id, 0) <> NVL (:OLD.location_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_dc', 'location_id',
                      'Pack-Out Location', 'DC', :OLD.location_id,
                      :NEW.location_id, :OLD.display_id, v_type,
                      :OLD.display_dc_id
                     );
      END IF;

      IF NVL (:NEW.quantity, 0) <> NVL (:OLD.quantity, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_dc', 'quantity',
                      'Pack-Out Quantity', 'DC Qty', :OLD.quantity,
                      :NEW.quantity, :OLD.display_id, v_type,
                      :OLD.display_dc_id
                     );
      END IF;
   END IF;
END display_dc_trig;
/


ALTER TABLE DISPLAY_DC ADD (
  CONSTRAINT DISPLAY_DC_PK
  PRIMARY KEY
  (DISPLAY_DC_ID)
  USING INDEX DISPLAY_DC_PK
  ENABLE VALIDATE);


ALTER TABLE DISPLAY_DC ADD (
  CONSTRAINT DISPLAY_DC_DISP_ID_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY_DC ADD (
  CONSTRAINT DISPLAY_DC_LOC_FK 
  FOREIGN KEY (LOCATION_ID) 
  REFERENCES LOCATION (LOCATION_ID)
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_COST
(
  DISPLAY_COST_ID         NUMBER(10),
  DISPLAY_ID              NUMBER(10),
  DISPLAY_MATERIAL_COST   NUMBER(8,2),
  FULLFILMENT_COST        NUMBER(8,2),
  CTP_COST                NUMBER(8,2),
  DIE_CUTTING_COST        NUMBER(8,2),
  PRINTING_PLATE_COST     NUMBER(8,2),
  TOTAL_ARTWORK_COST      NUMBER(8,2),
  SHIP_TEST_COST          NUMBER(8,2),
  PALLET_COST             NUMBER(8,2),
  MISC_COST               NUMBER(8,2),
  CORRUGATE_COST          NUMBER(8,2),
  OVERAGE_SCRAP_PCT       NUMBER(4),
  CUSTOMER_PROGRAM_PCT    NUMBER(6,2),
  CM_PROJECT_NUM          VARCHAR2(40 BYTE),
  SSCID_PROJECT_NUM       VARCHAR2(20 BYTE),
  COST_CENTER             VARCHAR2(40 BYTE),
  EXPEDITED_FREIGHT_COST  NUMBER(8,2),
  FINES                   NUMBER(8,2),
  CA_DATE                 DATE,
  DISPLAY_SCENARIO_ID     NUMBER(10),
  DT_CREATED              DATE,
  DT_LAST_CHANGED         DATE,
  USER_CREATED            VARCHAR2(100 BYTE),
  USER_LAST_CHANGED       VARCHAR2(100 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_COST_PK ON DISPLAY_COST
(DISPLAY_COST_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER display_cost_trig
   BEFORE DELETE OR INSERT OR UPDATE
   ON display_cost
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;
   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id,
                   display_scenario_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_cost', 'new display_cost record', 'N/A', 'N/A',
                   :NEW.display_id, v_type, :NEW.display_cost_id,
                   :NEW.display_scenario_id
                  );
   END IF;
   IF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name, FIELD,
                   old_value, new_value, display_id, change_type,
                   primary_key_id, display_scenario_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_cost', 'delete display_cost_record',
                   'N/A', 'N/A', :OLD.display_id, v_type,
                   :OLD.display_cost_id, :OLD.display_scenario_id
                  );
   END IF;
   IF UPDATING
   THEN
      IF NVL (:NEW.display_material_cost, 0) <>
                                          NVL (:OLD.display_material_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value,
                      new_value, display_id, change_type,
                      primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'display_material_cost',
                      :OLD.display_material_cost,
                      :NEW.display_material_cost, :OLD.display_id, v_type,
                      :OLD.display_cost_id, :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.fullfilment_cost, 0) <> NVL (:OLD.fullfilment_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'fullfilment_cost',
                      :OLD.fullfilment_cost, :NEW.fullfilment_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id,
                      :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.ctp_cost, 0) <> NVL (:OLD.ctp_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'ctp_cost', :OLD.ctp_cost,
                      :NEW.ctp_cost, :OLD.display_id, v_type,
                      :OLD.display_cost_id, :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.die_cutting_cost, 0) <> NVL (:OLD.die_cutting_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'die_cutting_cost',
                      :OLD.die_cutting_cost, :NEW.die_cutting_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id,
                      :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.printing_plate_cost, 0) <>
                                             NVL (:OLD.printing_plate_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'printing_plate_cost',
                      :OLD.printing_plate_cost, :NEW.printing_plate_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id,
                      :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.total_artwork_cost, 0) <> NVL (:OLD.total_artwork_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'total_artwork_cost',
                      :OLD.total_artwork_cost, :NEW.total_artwork_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id,
                      :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.ship_test_cost, 0) <> NVL (:OLD.ship_test_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'ship_test_cost',
                      :OLD.ship_test_cost, :NEW.ship_test_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id,
                      :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.pallet_cost, 0) <> NVL (:OLD.pallet_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'pallet_cost',
                      :OLD.pallet_cost, :NEW.pallet_cost, :OLD.display_id,
                      v_type, :OLD.display_cost_id, :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.misc_cost, 0) <> NVL (:OLD.misc_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'misc_cost', :OLD.misc_cost,
                      :NEW.misc_cost, :OLD.display_id, v_type,
                      :OLD.display_cost_id, :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.corrugate_cost, 0) <> NVL (:OLD.corrugate_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'corrugate_cost',
                      :OLD.corrugate_cost, :NEW.corrugate_cost,
                      :OLD.display_id, v_type, :OLD.display_cost_id,
                      :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.overage_scrap_pct, 0) <> NVL (:OLD.overage_scrap_pct, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'overage_scrap_pct',
                      :OLD.overage_scrap_pct, :NEW.overage_scrap_pct,
                      :OLD.display_id, v_type, :OLD.display_cost_id,
                      :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.customer_program_pct, 0) <>
                                            NVL (:OLD.customer_program_pct, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'customer_program_pct',
                      :OLD.customer_program_pct, :NEW.customer_program_pct,
                      :OLD.display_id, v_type, :OLD.display_cost_id,
                      :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.cm_project_num, 'null') <>
                                             NVL (:OLD.cm_project_num, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'cm_project_num',
                      :OLD.cm_project_num, :NEW.cm_project_num,
                      :OLD.display_id, v_type, :OLD.display_cost_id,
                      :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.sscid_project_num, 'null') <>
                                          NVL (:OLD.sscid_project_num, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'sscid_project_num',
                      :OLD.sscid_project_num, :NEW.sscid_project_num,
                      :OLD.display_id, v_type, :OLD.display_cost_id,
                      :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.cost_center, 'null') <> NVL (:OLD.cost_center, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'cost_center',
                      :OLD.cost_center, :NEW.cost_center, :OLD.display_id,
                      v_type, :OLD.display_cost_id, :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.expedited_freight_cost, 0) <>
                                          NVL (:OLD.expedited_freight_cost, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value,
                      new_value, display_id, change_type,
                      primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'expedited_freight_cost',
                      :OLD.expedited_freight_cost,
                      :NEW.expedited_freight_cost, :OLD.display_id, v_type,
                      :OLD.display_cost_id, :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.fines, 0) <> NVL (:OLD.fines, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'fines', :OLD.fines,
                      :NEW.fines, :OLD.display_id, v_type,
                      :OLD.display_cost_id, :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.ca_date, TRUNC (SYSDATE)) <>
                                           NVL (:OLD.ca_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'ca_date', :OLD.ca_date,
                      :NEW.ca_date, :OLD.display_id, v_type,
                      :OLD.display_cost_id, :OLD.display_scenario_id
                     );
      END IF;
      IF NVL (:NEW.display_id, 0) <> NVL (:OLD.display_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_cost', 'DISPLAY_ID!!!!!',
                      :OLD.display_id, :NEW.display_id, :OLD.display_id,
                      v_type, :OLD.display_cost_id, :OLD.display_scenario_id
                     );
      END IF;
   END IF;
END display_cost_trig;
/


ALTER TABLE DISPLAY_COST ADD (
  CONSTRAINT DISPLAY_COST_PK
  PRIMARY KEY
  (DISPLAY_COST_ID)
  USING INDEX DISPLAY_COST_PK
  ENABLE VALIDATE);


ALTER TABLE DISPLAY_COST ADD (
  CONSTRAINT DISPLAY_COST_DISPLAY_ID_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);
CREATE TABLE CUSTOMER_MARKETING
(
  CUSTOMER_MARKETING_ID    NUMBER(10),
  DISPLAY_ID               NUMBER(10),
  PRICE_AVAILABILITY_DATE  DATE,
  SENT_SAMPLE_DATE         DATE,
  FINAL_ARTWORK_DATE       DATE,
  PIM_COMPLETED_DATE       DATE,
  PRICING_ADMIN_DATE       DATE,
  INITIAL_RENDERING_FLG    VARCHAR2(1 BYTE)     DEFAULT 'N',
  STRUCTURE_APPROVED_FLG   VARCHAR2(1 BYTE)     DEFAULT 'N',
  ORDER_IN_FLG             VARCHAR2(1 BYTE)     DEFAULT 'N',
  PROJECT_LEVEL_PCT        NUMBER(4),
  COMMENTS                 VARCHAR2(200 BYTE),
  DT_CREATED               DATE,
  DT_LAST_CHANGED          DATE,
  USER_CREATED             VARCHAR2(100 BYTE),
  USER_LAST_CHANGED        VARCHAR2(100 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX CUSTOMER_MARKETING_PK ON CUSTOMER_MARKETING
(CUSTOMER_MARKETING_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER customer_marketing_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON customer_marketing
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value,
                   new_value, display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'customer_marketing', 'customer_marketing record', 'N/A',
                   'N/A', :NEW.display_id, v_type, :NEW.customer_marketing_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'customer_marketing',
                   'deleting customer_marketing record', NULL, NULL,
                   :OLD.display_id, v_type, :OLD.customer_marketing_id
                  );
   ELSIF UPDATING
   THEN
      IF NVL (:NEW.price_availability_date, TRUNC (SYSDATE)) <>
                          NVL (:OLD.price_availability_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name,
                      FIELD,
                      friendly_name, short_name,
                      old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing',
                      'price_availability_date',
                      'Price Availability Completed On', 'Price Avail Dt',
                      :OLD.price_availability_date,
                      :NEW.price_availability_date, :OLD.display_id, v_type,
                      :OLD.customer_marketing_id
                     );
      END IF;

      IF NVL (:NEW.sent_sample_date, TRUNC (SYSDATE)) <>
                                  NVL (:OLD.sent_sample_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'sent_sample_date',
                      'Sample to Customer By', 'Sample Dt',
                      :OLD.sent_sample_date, :NEW.sent_sample_date,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      END IF;

      IF NVL (:NEW.final_artwork_date, TRUNC (SYSDATE)) <>
                                NVL (:OLD.final_artwork_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'final_artwork_date',
                      'Final Artwork & PO to Vendor By', 'Final Artwk',
                      :OLD.final_artwork_date, :NEW.final_artwork_date,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      END IF;

      IF NVL (:NEW.pim_completed_date, TRUNC (SYSDATE)) <>
                                NVL (:OLD.pim_completed_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'pim_completed_date',
                      'PIM Completed On', 'PIM Complete',
                      :OLD.pim_completed_date, :NEW.pim_completed_date,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      END IF;

      IF NVL (:NEW.pricing_admin_date, TRUNC (SYSDATE)) <>
                                NVL (:OLD.pricing_admin_date, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'pricing_admin_date',
                      'Pricing Admin Validation', 'Pricing Admin',
                      :OLD.pricing_admin_date, :NEW.pricing_admin_date,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      END IF;

      IF NVL (:NEW.initial_rendering_flg, 'null') <>
                                      NVL (:OLD.initial_rendering_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name,
                      FIELD,
                      friendly_name,
                      short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing',
                      'initial_rendering_flg',
                      'Initial Rendering and Est Quote',
                      'Init Rend & Quote', :OLD.initial_rendering_flg,
                      :NEW.initial_rendering_flg, :OLD.display_id, v_type,
                      :OLD.customer_marketing_id
                     );
      END IF;

      IF NVL (:NEW.structure_approved_flg, 'null') <>
                                     NVL (:OLD.structure_approved_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name,
                      FIELD, friendly_name,
                      short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing',
                      'structure_approved_flg', 'Structure Approved',
                      'Struct Appr', :OLD.structure_approved_flg,
                      :NEW.structure_approved_flg, :OLD.display_id, v_type,
                      :OLD.customer_marketing_id
                     );
      END IF;

      IF NVL (:NEW.order_in_flg, 'null') <> NVL (:OLD.order_in_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'order_in_flg',
                      'Orders Received', 'Order In', :OLD.order_in_flg,
                      :NEW.order_in_flg, :OLD.display_id, v_type,
                      :OLD.customer_marketing_id
                     );
      END IF;

      IF NVL (:NEW.project_level_pct, 0) <> NVL (:OLD.project_level_pct, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'project_level_pct',
                      'Project Confidence Level', 'Prj Lvl',
                      :OLD.project_level_pct, :NEW.project_level_pct,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      END IF;

      IF NVL (:NEW.comments, 'null') <> NVL (:OLD.comments, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'customer_marketing', 'comments', 'Comments',
                      'Comments', :OLD.comments, :NEW.comments,
                      :OLD.display_id, v_type, :OLD.customer_marketing_id
                     );
      END IF;
   END IF;
END customer_marketing_trig;
/


ALTER TABLE CUSTOMER_MARKETING ADD (
  CONSTRAINT CUSTOMER_MARKETING_PK
  PRIMARY KEY
  (CUSTOMER_MARKETING_ID)
  USING INDEX CUSTOMER_MARKETING_PK
  ENABLE VALIDATE);


ALTER TABLE CUSTOMER_MARKETING ADD (
  CONSTRAINT CUSTOMER_MARKETING_DISPLAY_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);
CREATE TABLE DISPLAY
(
  DISPLAY_ID            NUMBER(10),
  SKU                   VARCHAR2(15 BYTE),
  DESCRIPTION           VARCHAR2(200 BYTE),
  CUSTOMER_ID           NUMBER(10),
  STATUS_ID             NUMBER(10),
  QTY_FORECAST          NUMBER(10),
  PROMO_PERIOD_ID       NUMBER(10),
  DISPLAY_SALES_REP_ID  NUMBER(10),
  DESTINATION           VARCHAR2(10 BYTE),
  STRUCTURE_ID          NUMBER(10),
  QSI_DOC_NUMBER        VARCHAR2(50 BYTE),
  SKU_MIX_FINAL_FLG     VARCHAR2(1 BYTE)        DEFAULT 'N',
  MANUFACTURER_ID       NUMBER(10),
  DISPLAY_SETUP_ID      NUMBER(10),
  PROMO_YEAR            NUMBER(10),
  CUSTOMER_DISPLAY#     VARCHAR2(30 BYTE),
  DT_GONOGO             DATE,
  RYG_STATUS_FLG        VARCHAR2(1 BYTE)        DEFAULT 'G',
  PACKOUT_VENDOR_ID     NUMBER(10),
  ACTUAL_QTY            NUMBER(10),
  CM_PROJECT_NUM        VARCHAR2(40 BYTE),
  USER_LAST_CHANGED     VARCHAR2(100 BYTE),
  USER_CREATED          VARCHAR2(100 BYTE),
  DT_LAST_CHANGED       DATE,
  DT_CREATED            DATE,
  COUNTRY_ID            NUMBER(10)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_PK ON DISPLAY
(DISPLAY_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER display_trig
   BEFORE INSERT OR UPDATE OR DELETE
   ON display
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name, short_name, old_value,
                   new_value, display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display', 'sku', 'Top Level SKU', 'Top SKU', 'N/A',
                   :NEW.sku, :NEW.display_id, v_type, :NEW.display_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value,
                   display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display', 'sku_mix_final_flg', 'SKU Mix Final',
                   'SKU Mix Final', 'N/A', :NEW.sku_mix_final_flg,
                   :NEW.display_id, v_type, :NEW.display_id
                  );

      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, friendly_name,
                   short_name, old_value, new_value, display_id, change_type,
                   primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display', 'qty_forecast', 'Display Forecast Quantity',
                   'Qty', 'N/A', :NEW.qty_forecast, :NEW.display_id, v_type,
                   :NEW.display_id
                  );
   ELSIF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name, FIELD, old_value,
                   new_value, display_id, change_type, primary_key_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display', 'deleting display record', 'N/A',
                   'N/A', :OLD.display_id, v_type, :OLD.display_id
                  );
   ELSIF UPDATING
   THEN
      IF NVL (:NEW.sku, 'null') <> NVL (:OLD.sku, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'sku', 'Top Level SKU',
                      'Top SKU', :OLD.sku, :NEW.sku, :NEW.display_id,
                      v_type, :OLD.display_id
                     );
      END IF;

      IF NVL (:NEW.description, 'null') <> NVL (:OLD.description, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'description', 'Description',
                      'Desc', :OLD.description, :NEW.description,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.customer_id, 0) <> NVL (:OLD.customer_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'customer_id', 'Customer',
                      'Cust', :OLD.customer_id, :NEW.customer_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.status_id, 0) <> NVL (:OLD.status_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'status_id', 'Status',
                      'Status', :OLD.status_id, :NEW.status_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.qty_forecast, 0) <> NVL (:OLD.qty_forecast, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'qty_forecast',
                      'Display Forecast Quantity', 'Qty', :OLD.qty_forecast,
                      :NEW.qty_forecast, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.promo_period_id, 0) <> NVL (:OLD.promo_period_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'promo_period_id', 'Promo Period',
                      'Period', :OLD.promo_period_id, :NEW.promo_period_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.display_sales_rep_id, 0) <>
                                            NVL (:OLD.display_sales_rep_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'display_sales_rep_id',
                      'Project Manager', 'PM', :OLD.display_sales_rep_id,
                      :NEW.display_sales_rep_id, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.destination, 'null') <> NVL (:OLD.destination, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'destination', :OLD.destination,
                      :NEW.destination, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.structure_id, 0) <> NVL (:OLD.structure_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'structure_id', 'Structure Type',
                      'Struct', :OLD.structure_id, :NEW.structure_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.qsi_doc_number, 'null') <>
                                             NVL (:OLD.qsi_doc_number, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'qsi_doc_number',
                      'QSI Doc Number', 'QSI', :OLD.qsi_doc_number,
                      :NEW.qsi_doc_number, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.sku_mix_final_flg, 'null') <>
                                          NVL (:OLD.sku_mix_final_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'sku_mix_final_flg',
                      'SKU Mix Final', 'SKU Mix Final',
                      :OLD.sku_mix_final_flg, :NEW.sku_mix_final_flg,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.manufacturer_id, 0) <> NVL (:OLD.manufacturer_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'manufacturer_id', 'Manufacturer',
                      'Mfg', :OLD.manufacturer_id, :NEW.manufacturer_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.display_setup_id, 0) <> NVL (:OLD.display_setup_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'display_setup_id',
                      'Display Setup Type', 'Display Setup Type',
                      :OLD.display_setup_id, :NEW.display_setup_id,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.promo_year, 0) <> NVL (:OLD.promo_year, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'promo_year', 'Promo Year',
                      'Year', :OLD.promo_year, :NEW.promo_year,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.customer_display#, 'null') <>
                                          NVL (:OLD.customer_display#, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name,
                      old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'customer_display#',
                      'Customer Display#', 'Cust Disp #',
                      :OLD.customer_display#, :NEW.customer_display#,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.dt_gonogo, TRUNC (SYSDATE)) <>
                                         NVL (:OLD.dt_gonogo, TRUNC (SYSDATE))
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value, display_id,
                      change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'dt_gonogo', NULL,
                      NULL, :OLD.dt_gonogo, :NEW.dt_gonogo, :NEW.display_id,
                      v_type, :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.ryg_status_flg, 'null') <>
                                             NVL (:OLD.ryg_status_flg, 'null')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'ryg_status_flg',
                      'RYG Status Flag', 'RYG', :OLD.ryg_status_flg,
                      :NEW.ryg_status_flg, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.packout_vendor_id, 0) <> NVL (:OLD.packout_vendor_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      friendly_name, short_name, old_value,
                      new_value, display_id, change_type,
                      primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'packout_vendor_id',
                      'Packout Vendor', 'PVI', :OLD.packout_vendor_id,
                      :NEW.packout_vendor_id, :NEW.display_id, v_type,
                      :NEW.display_id
                     );
      END IF;

      IF NVL (:NEW.actual_qty, 0) <> NVL (:OLD.actual_qty, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, friendly_name,
                      short_name, old_value, new_value,
                      display_id, change_type, primary_key_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display', 'actual_qty', 'Actual Quantity',
                      'Act Qty', :OLD.actual_qty, :NEW.actual_qty,
                      :NEW.display_id, v_type, :NEW.display_id
                     );
      END IF;
   END IF;
END display_trig;
/


ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_PK
  PRIMARY KEY
  (DISPLAY_ID)
  USING INDEX DISPLAY_PK
  ENABLE VALIDATE);


ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_COUNTRY_FK 
  FOREIGN KEY (COUNTRY_ID) 
  REFERENCES COUNTRY (COUNTRY_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_CUSTOMER_ID_FK 
  FOREIGN KEY (CUSTOMER_ID) 
  REFERENCES CUSTOMER (CUSTOMER_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_MANUFACTURER_FK 
  FOREIGN KEY (MANUFACTURER_ID) 
  REFERENCES MANUFACTURER (MANUFACTURER_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_PACKOUT_VEND_ID_FK 
  FOREIGN KEY (PACKOUT_VENDOR_ID) 
  REFERENCES MANUFACTURER (MANUFACTURER_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_PROMO_PERIOD_FK 
  FOREIGN KEY (PROMO_PERIOD_ID) 
  REFERENCES PROMO_PERIOD (PROMO_PERIOD_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_SALES_REP_ID_FK 
  FOREIGN KEY (DISPLAY_SALES_REP_ID) 
  REFERENCES DISPLAY_SALES_REP (DISPLAY_SALES_REP_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_SETUP_FK 
  FOREIGN KEY (DISPLAY_SETUP_ID) 
  REFERENCES DISPLAY_SETUP (DISPLAY_SETUP_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_STATUS_ID_FK 
  FOREIGN KEY (STATUS_ID) 
  REFERENCES STATUS (STATUS_ID)
  ENABLE VALIDATE);

ALTER TABLE DISPLAY ADD (
  CONSTRAINT DISPLAY_STRUCTURE_ID_FK 
  FOREIGN KEY (STRUCTURE_ID) 
  REFERENCES STRUCTURE (STRUCTURE_ID)
  ENABLE VALIDATE);
CREATE TABLE USER_CHANGE_LOG
(
  USER_CHANGE_LOG_ID   NUMBER(10),
  USERNAME             VARCHAR2(100 BYTE),
  DT_CHANGED           DATE,
  TABLE_NAME           VARCHAR2(30 BYTE),
  FIELD                VARCHAR2(50 BYTE),
  FRIENDLY_NAME        VARCHAR2(50 BYTE),
  SHORT_NAME           VARCHAR2(50 BYTE),
  OLD_VALUE            VARCHAR2(200 BYTE),
  NEW_VALUE            VARCHAR2(200 BYTE),
  DISPLAY_ID           NUMBER(10),
  CHANGE_TYPE          VARCHAR2(30 BYTE),
  PRIMARY_KEY_ID       NUMBER(10),
  DISPLAY_SCENARIO_ID  NUMBER(10)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX CHANGE_LOG_PRIMARY_KEY_INDX ON USER_CHANGE_LOG
(PRIMARY_KEY_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE INDEX USER_CHANGE_LOG_DATE_INDX ON USER_CHANGE_LOG
(DT_CHANGED)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE INDEX USER_CHANGE_LOG_FIELD_INDX ON USER_CHANGE_LOG
(FIELD)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE UNIQUE INDEX USER_CHANGE_LOG_PK ON USER_CHANGE_LOG
(USER_CHANGE_LOG_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE INDEX USER_CHANGE_LOG_TABLE_INDX ON USER_CHANGE_LOG
(TABLE_NAME)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE INDEX USR_CHANGE_LOG_DISPLAY_ID_INDX ON USER_CHANGE_LOG
(DISPLAY_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE USER_CHANGE_LOG ADD (
  CONSTRAINT USER_CHANGE_LOG_PK
  PRIMARY KEY
  (USER_CHANGE_LOG_ID)
  USING INDEX USER_CHANGE_LOG_PK
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_SCENARIO
(
  DISPLAY_SCENARIO_ID    NUMBER(10),
  DISPLAY_ID             NUMBER(10),
  SCENARIO_STATUS_ID     NUMBER(10)             DEFAULT 1,
  SCENARIO_NUM           NUMBER(2),
  DISPLAY_SALES_REP_ID   NUMBER(10),
  PROMO_YEAR             NUMBER(10),
  PROMO_PERIOD_ID        NUMBER(10),
  SKU                    VARCHAR2(15 BYTE),
  PACKOUT_VENDOR_ID      NUMBER(10),
  MANUFACTURER_ID        NUMBER(10),
  QTY_FORECAST           NUMBER(10),
  ACTUAL_QTY             NUMBER(10),
  SCENARIO_APPROVAL_FLG  VARCHAR2(1 BYTE)       DEFAULT 'N',
  USER_LAST_CHANGED      VARCHAR2(100 BYTE),
  USER_CREATED           VARCHAR2(100 BYTE),
  DT_LAST_CHANGED        DATE,
  DT_CREATED             DATE
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_SCENARIO_PK ON DISPLAY_SCENARIO
(DISPLAY_SCENARIO_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE OR REPLACE TRIGGER display_scenario_trig
   BEFORE DELETE OR INSERT OR UPDATE
   ON display_scenario
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   v_type   VARCHAR2 (6);
BEGIN
   IF UPDATING
   THEN
      v_type := 'Update';
   ELSIF DELETING
   THEN
      v_type := 'Delete';
   ELSIF INSERTING
   THEN
      v_type := 'Insert';
   END IF;

   IF INSERTING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username, dt_changed,
                   table_name, FIELD, old_value,
                   new_value, display_id, change_type, primary_key_id,
                   display_scenario_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_created, SYSDATE,
                   'display_scenario', 'new display_scenario record', 'N/A',
                   'N/A', :NEW.display_id, v_type, :NEW.display_scenario_id,
                   :NEW.display_scenario_id
                  );
   END IF;

   IF DELETING
   THEN
      INSERT INTO user_change_log
                  (user_change_log_id, username,
                   dt_changed, table_name,
                   FIELD, old_value, new_value,
                   display_id, change_type, primary_key_id,
                   display_scenario_id
                  )
           VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                   SYSDATE, 'display_scenario',
                   'delete display_scenario_record', 'N/A', 'N/A',
                   :OLD.display_id, v_type, :OLD.display_scenario_id,
                   :OLD.display_scenario_id
                  );
   END IF;

   IF UPDATING
   THEN
      IF NVL (:NEW.scenario_status_id, 0) <> NVL (:OLD.scenario_status_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'scenario_status_id',
                      :OLD.scenario_status_id, :NEW.scenario_status_id,
                      :OLD.display_id, v_type, :OLD.display_scenario_id,
                      :OLD.display_scenario_id
                     );
      END IF;

      IF NVL (:NEW.scenario_num, 0) <> NVL (:OLD.scenario_num, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'scenario_num',
                      :OLD.scenario_num, :NEW.scenario_num, :OLD.display_id,
                      v_type, :OLD.display_scenario_id,
                      :OLD.display_scenario_id
                     );
      END IF;

      IF NVL (:NEW.display_sales_rep_id, 0) <>
                                            NVL (:OLD.display_sales_rep_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'display_sales_rep_id',
                      :OLD.display_sales_rep_id, :NEW.display_sales_rep_id,
                      :OLD.display_id, v_type, :OLD.display_scenario_id,
                      :OLD.display_scenario_id
                     );
      END IF;

      IF NVL (:NEW.promo_year, 0) <> NVL (:OLD.promo_year, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'promo_year',
                      :OLD.promo_year, :NEW.promo_year, :OLD.display_id,
                      v_type, :OLD.display_scenario_id,
                      :OLD.display_scenario_id
                     );
      END IF;

      IF NVL (:NEW.promo_period_id, 0) <> NVL (:OLD.promo_period_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'promo_period_id',
                      :OLD.promo_period_id, :NEW.promo_period_id,
                      :OLD.display_id, v_type, :OLD.display_scenario_id,
                      :OLD.display_scenario_id
                     );
      END IF;

      IF NVL (:NEW.sku, 'N') <> NVL (:OLD.sku, 'N')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD, old_value,
                      new_value, display_id, change_type,
                      primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'sku', :OLD.sku,
                      :NEW.sku, :OLD.display_id, v_type,
                      :OLD.display_scenario_id, :OLD.display_scenario_id
                     );
      END IF;

      IF NVL (:NEW.packout_vendor_id, 0) <> NVL (:OLD.packout_vendor_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'packout_vendor_id',
                      :OLD.packout_vendor_id, :NEW.packout_vendor_id,
                      :OLD.display_id, v_type, :OLD.display_scenario_id,
                      :OLD.display_scenario_id
                     );
      END IF;

      IF NVL (:NEW.manufacturer_id, 0) <> NVL (:OLD.manufacturer_id, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value,
                      display_id, change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'manufacturer_id',
                      :OLD.manufacturer_id, :NEW.manufacturer_id,
                      :OLD.display_id, v_type, :OLD.display_scenario_id,
                      :OLD.display_scenario_id
                     );
      END IF;

      IF NVL (:NEW.qty_forecast, 0) <> NVL (:OLD.qty_forecast, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'qty_forecast',
                      :OLD.qty_forecast, :NEW.qty_forecast, :OLD.display_id,
                      v_type, :OLD.display_scenario_id,
                      :OLD.display_scenario_id
                     );
      END IF;

      IF NVL (:NEW.actual_qty, 0) <> NVL (:OLD.actual_qty, 0)
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value, new_value, display_id,
                      change_type, primary_key_id,
                      display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'actual_qty',
                      :OLD.actual_qty, :NEW.actual_qty, :OLD.display_id,
                      v_type, :OLD.display_scenario_id,
                      :OLD.display_scenario_id
                     );
      END IF;

      IF NVL (:NEW.scenario_approval_flg, 'O') <>
                                         NVL (:OLD.scenario_approval_flg, 'O')
      THEN
         INSERT INTO user_change_log
                     (user_change_log_id, username,
                      dt_changed, table_name, FIELD,
                      old_value,
                      new_value, display_id, change_type,
                      primary_key_id, display_scenario_id
                     )
              VALUES (user_change_log_seq.NEXTVAL, :NEW.user_last_changed,
                      SYSDATE, 'display_scenario', 'scenario_approval_flg',
                      :OLD.scenario_approval_flg,
                      :NEW.scenario_approval_flg, :OLD.display_id, v_type,
                      :OLD.display_scenario_id, :OLD.display_scenario_id
                     );
      END IF;
   END IF;
END display_scenario_trig;
/


ALTER TABLE DISPLAY_SCENARIO ADD (
  CONSTRAINT DISPLAY_SCENARIO_PK
  PRIMARY KEY
  (DISPLAY_SCENARIO_ID)
  USING INDEX DISPLAY_SCENARIO_PK
  ENABLE VALIDATE);


ALTER TABLE DISPLAY_SCENARIO ADD (
  CONSTRAINT DISPLAY_SCENARIO_DISP_ID_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);
CREATE TABLE IMAGE
(
  IMAGE_ID             NUMBER(10)               NOT NULL,
  DISPLAY_COST_ID      NUMBER(10),
  DISPLAY_ID           NUMBER(10),
  CREATE_DATE          DATE,
  URL                  VARCHAR2(100 BYTE),
  IMAGE_NAME           VARCHAR2(50 BYTE),
  DESCRIPTION          VARCHAR2(200 BYTE),
  DISPLAY_SCENARIO_ID  NUMBER(10)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX IMAGE_PK ON IMAGE
(IMAGE_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE IMAGE ADD (
  CONSTRAINT IMAGE_PK
  PRIMARY KEY
  (IMAGE_ID)
  USING INDEX IMAGE_PK
  ENABLE VALIDATE);


ALTER TABLE IMAGE ADD (
  CONSTRAINT DISPLAY_COST_IMAGE_FK 
  FOREIGN KEY (DISPLAY_COST_ID) 
  REFERENCES DISPLAY_COST (DISPLAY_COST_ID)
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_COST_COMMENT
(
  DISPLAY_COST_COMMENT_ID  NUMBER(10)           NOT NULL,
  DISPLAY_COST_ID          NUMBER(10),
  CREATE_DATE              DATE,
  USERNAME                 VARCHAR2(100 BYTE),
  COST_COMMENT             VARCHAR2(500 BYTE),
  DISPLAY_SCENARIO_ID      NUMBER(10)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DISPLAY_COST_COMMENT_PK ON DISPLAY_COST_COMMENT
(DISPLAY_COST_COMMENT_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE DISPLAY_COST_COMMENT ADD (
  CONSTRAINT DISPLAY_COST_COMMENT_PK
  PRIMARY KEY
  (DISPLAY_COST_COMMENT_ID)
  USING INDEX DISPLAY_COST_COMMENT_PK
  ENABLE VALIDATE);


ALTER TABLE DISPLAY_COST_COMMENT ADD (
  CONSTRAINT DISPLAY_COST_COMMENT_FK 
  FOREIGN KEY (DISPLAY_COST_ID) 
  REFERENCES DISPLAY_COST (DISPLAY_COST_ID)
  ENABLE VALIDATE);
CREATE TABLE DISPLAY_COST_HISTORY
(
  DISPLAY_COST_ID         NUMBER(10),
  DISPLAY_ID              NUMBER(10),
  DISPLAY_MATERIAL_ID     NUMBER(8,2),
  FULLFILMENT_COST        NUMBER(8,2),
  CTP_COST                NUMBER(8,2),
  DIE_CUTTING_COST        NUMBER(8,2),
  PRINTING_PLATE_COST     NUMBER(8,2),
  TOTAL_ARTWORK_COST      NUMBER(8,2),
  SHIP_TEST_COST          NUMBER(8,2),
  PALLET_COST             NUMBER(8,2),
  MISC_COST               NUMBER(8,2),
  CORRUGATE_COST          NUMBER(8,2),
  OVERAGE_SCRAP_PCT       NUMBER(4),
  CUSTOMER_PROGRAM_PCT    NUMBER(4),
  CM_PROJECT_NUM          VARCHAR2(40 BYTE),
  SSCID_PROJECT_NUM       VARCHAR2(20 BYTE),
  COST_CENTER             VARCHAR2(40 BYTE),
  EXPEDITED_FREIGHT_COST  NUMBER(8,2),
  FINES                   NUMBER(8,2),
  MODIFIED_DATE           DATE,
  STATUS_ID               NUMBER(10),
  DISPLAY_SCENARIO_ID     NUMBER(10)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
CREATE TABLE PRICE_EXCEPTION
(
  KIT_COMPONENT_ID  NUMBER(10),
  SKU               VARCHAR2(15 BYTE),
  CUSTOMER_ID       NUMBER(10),
  COMPANY_CODE      VARCHAR2(3 BYTE),
  CUSTOMER_CODE     VARCHAR2(8 BYTE)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX PE_COMPONENT_ID_UNQ ON PRICE_EXCEPTION
(KIT_COMPONENT_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;
CREATE TABLE PRICE_EXCEPTION_RESPONSE
(
  KIT_COMPONENT_ID  NUMBER(10),
  SKU               VARCHAR2(15 BYTE),
  COMPANY_CODE      VARCHAR2(3 BYTE),
  CUSTOMER_CODE     VARCHAR2(8 BYTE),
  PRICE_EXCEPTION   VARCHAR2(10 BYTE),
  RUN_DATE          DATE
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX PE_KIT_COMP_ID_INDX ON PRICE_EXCEPTION_RESPONSE
(KIT_COMPONENT_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;
CREATE TABLE CUSTOMER_PCT
(
  CUSTOMER_ID          NUMBER(10),
  BINDERS              NUMBER(8,2),
  CARDS_PAPER          NUMBER(8,2),
  DIVIDERS             NUMBER(8,2),
  LABELS               NUMBER(8,2),
  O_AND_P              NUMBER(8,2),
  WRITING_INSTRUMENTS  NUMBER(8,2)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX CUSTOMER_PCT_PK ON CUSTOMER_PCT
(CUSTOMER_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE CUSTOMER_PCT ADD (
  CONSTRAINT CUSTOMER_PCT_PK
  PRIMARY KEY
  (CUSTOMER_ID)
  USING INDEX CUSTOMER_PCT_PK
  ENABLE VALIDATE);


ALTER TABLE CUSTOMER_PCT ADD (
  CONSTRAINT CUSTOMER_PCT_ID_FK 
  FOREIGN KEY (CUSTOMER_ID) 
  REFERENCES CUSTOMER (CUSTOMER_ID)
  ENABLE VALIDATE);
CREATE TABLE CALCULATION_SUMMARY
(
  CALCULATION_SUMMARY_ID         NUMBER(10),
  DISPLAY_ID                     NUMBER(10),
  DISPLAY_SCENARIO_ID            NUMBER(10),
  FORECAST_QTY                   NUMBER(10),
  ACTUAL_QTY                     NUMBER(10),
  SUM_CORRUGATE_COST             NUMBER(12,2),
  SUM_FULFILLMENT_COST           NUMBER(12,2),
  SUM_OTHER_COST                 NUMBER(12,2),
  SUM_TOT_DISP_COST_NOSHIPPING   NUMBER(12,2),
  SUM_TOT_DISPLAY_COST_SHIPPING  NUMBER(12,2),
  SHIPPING_COST                  NUMBER(12,2),
  SUM_TRADE_SALES                NUMBER(12,2),
  CUST_PROG_DISC                 NUMBER(5,2),
  SUM_NET_SALES_BP               NUMBER(12,2),
  SUM_NET_SALES_AP               NUMBER(12,2),
  SUM_COGS                       NUMBER(12,2),
  SUM_PRODVAR_MARGIN             NUMBER(12,2),
  SUM_DISPLAY_COST               NUMBER(12,2),
  SUM_VM_DOLLAR_BP               NUMBER(12,2),
  SUM_VM_DOLLAR_AP               NUMBER(12,2),
  SUM_VM_PCT_BP                  NUMBER(12,2),
  SUM_VM_PCT_AP                  NUMBER(12,2),
  VM_DOLLAR_HURDLE_RED           NUMBER(12,2),
  VM_DOLLAR_HURDLE_GREEN         NUMBER(12,2),
  VM_PCT_HURDLE_RED              NUMBER(12,2),
  VM_PCT_HURDLE_GREEN            NUMBER(12,2),
  STATUS_ID                      NUMBER(10),
  PROMO_PERIOD_ID                NUMBER(10),
  CUSTOMER_ID                    NUMBER(10),
  SKU                            VARCHAR2(15 BYTE),
  DESCRIPTION                    VARCHAR2(200 BYTE),
  AVERY_SKU                      VARCHAR2(15 BYTE),
  QTY_PER_FACING                 NUMBER(10),
  NUMBER_FACINGS                 NUMBER(10),
  QTY_PER_DISPLAY                NUMBER(10),
  SUM_TOT_ACTUAL_INVOICE         NUMBER(12,2),
  HURDLES                        VARCHAR2(100 BYTE),
  EXCESS_INVT_VMDOLLAR           NUMBER(12,2),
  EXCESS_INVT_VMPCT              NUMBER(12,2)
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX CALCULATION_SUMMARY_PK ON CALCULATION_SUMMARY
(CALCULATION_SUMMARY_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


CREATE UNIQUE INDEX CALC_SUM_SCEN_ID_UNQ ON CALCULATION_SUMMARY
(DISPLAY_SCENARIO_ID)
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL;


ALTER TABLE CALCULATION_SUMMARY ADD (
  CONSTRAINT CALCULATION_SUMMARY_PK
  PRIMARY KEY
  (CALCULATION_SUMMARY_ID)
  USING INDEX CALCULATION_SUMMARY_PK
  ENABLE VALIDATE);

ALTER TABLE CALCULATION_SUMMARY ADD (
  CONSTRAINT CALC_SUM_SCEN_ID_UNQ
  UNIQUE (DISPLAY_SCENARIO_ID)
  USING INDEX CALC_SUM_SCEN_ID_UNQ
  ENABLE VALIDATE);


ALTER TABLE CALCULATION_SUMMARY ADD (
  CONSTRAINT CALC_SUMM_DISPLAY_ID_FK 
  FOREIGN KEY (DISPLAY_ID) 
  REFERENCES DISPLAY (DISPLAY_ID)
  ENABLE VALIDATE);

ALTER TABLE CALCULATION_SUMMARY ADD (
  CONSTRAINT CALC_SUMM_SCENARIO_ID_FK 
  FOREIGN KEY (DISPLAY_SCENARIO_ID) 
  REFERENCES DISPLAY_SCENARIO (DISPLAY_SCENARIO_ID)
  ENABLE VALIDATE);
CREATE TABLE MANUFACTURER_COUNTRY
(
  COUNTRY_ID       NUMBER(10)                   NOT NULL,
  MANUFACTURER_ID  NUMBER(10)                   NOT NULL
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX MANUFACTURER_COUNTRY_PK ON MANUFACTURER_COUNTRY
(COUNTRY_ID, MANUFACTURER_ID)
LOGGING
TABLESPACE DMM_DAT1
NOPARALLEL;


ALTER TABLE MANUFACTURER_COUNTRY ADD (
  CONSTRAINT MANUFACTURER_COUNTRY_PK
  PRIMARY KEY
  (COUNTRY_ID, MANUFACTURER_ID)
  USING INDEX MANUFACTURER_COUNTRY_PK
  ENABLE VALIDATE);


ALTER TABLE MANUFACTURER_COUNTRY ADD (
  CONSTRAINT MANUFACTUERR_COUNTRY_MAN_FK 
  FOREIGN KEY (MANUFACTURER_ID) 
  REFERENCES MANUFACTURER (MANUFACTURER_ID)
  ENABLE VALIDATE);

ALTER TABLE MANUFACTURER_COUNTRY ADD (
  CONSTRAINT MANUFACTURER_COUNTRY_COU_FK 
  FOREIGN KEY (COUNTRY_ID) 
  REFERENCES COUNTRY (COUNTRY_ID)
  ENABLE VALIDATE);
CREATE TABLE LOCATION_COUNTRY
(
  COUNTRY_ID   NUMBER(10)                       NOT NULL,
  LOCATION_ID  NUMBER(10)                       NOT NULL
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX LOCATION_COUNTRY_PK ON LOCATION_COUNTRY
(COUNTRY_ID, LOCATION_ID)
LOGGING
TABLESPACE DMM_DAT1
NOPARALLEL;


ALTER TABLE LOCATION_COUNTRY ADD (
  CONSTRAINT LOCATION_COUNTRY_PK
  PRIMARY KEY
  (COUNTRY_ID, LOCATION_ID)
  USING INDEX LOCATION_COUNTRY_PK
  ENABLE VALIDATE);


ALTER TABLE LOCATION_COUNTRY ADD (
  CONSTRAINT LOCATION_COUNTRY_COU_FK 
  FOREIGN KEY (COUNTRY_ID) 
  REFERENCES COUNTRY (COUNTRY_ID)
  ENABLE VALIDATE);

ALTER TABLE LOCATION_COUNTRY ADD (
  CONSTRAINT LOCATION_COUNTRY_LOC_FK 
  FOREIGN KEY (LOCATION_ID) 
  REFERENCES LOCATION (LOCATION_ID)
  ENABLE VALIDATE);
CREATE TABLE PROMO_PERIOD_COUNTRY
(
  COUNTRY_ID       NUMBER(10)                   NOT NULL,
  PROMO_PERIOD_ID  NUMBER(10)                   NOT NULL
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX PROMO_PERIOD_COUNTRY_PK ON PROMO_PERIOD_COUNTRY
(COUNTRY_ID, PROMO_PERIOD_ID)
LOGGING
TABLESPACE DMM_DAT1
NOPARALLEL;


ALTER TABLE PROMO_PERIOD_COUNTRY ADD (
  CONSTRAINT PROMO_PERIOD_COUNTRY_PK
  PRIMARY KEY
  (COUNTRY_ID, PROMO_PERIOD_ID)
  USING INDEX PROMO_PERIOD_COUNTRY_PK
  ENABLE VALIDATE);


ALTER TABLE PROMO_PERIOD_COUNTRY ADD (
  CONSTRAINT PROMO_PERIOD_COU_FK 
  FOREIGN KEY (COUNTRY_ID) 
  REFERENCES COUNTRY (COUNTRY_ID)
  ENABLE VALIDATE);

ALTER TABLE PROMO_PERIOD_COUNTRY ADD (
  CONSTRAINT PROMO_PERIOD_PRO_FK 
  FOREIGN KEY (PROMO_PERIOD_ID) 
  REFERENCES PROMO_PERIOD (PROMO_PERIOD_ID)
  ENABLE VALIDATE);
CREATE TABLE CUSTOMER_COUNTRY
(
  COUNTRY_ID   NUMBER(10)                       NOT NULL,
  CUSTOMER_ID  NUMBER(10)                       NOT NULL
)
TABLESPACE DMM_DAT1
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX CUSTOMER_COUNTRY_PK ON CUSTOMER_COUNTRY
(COUNTRY_ID, CUSTOMER_ID)
LOGGING
TABLESPACE DMM_DAT1
NOPARALLEL;


ALTER TABLE CUSTOMER_COUNTRY ADD (
  CONSTRAINT CUSTOMER_COUNTRY_PK
  PRIMARY KEY
  (COUNTRY_ID, CUSTOMER_ID)
  USING INDEX CUSTOMER_COUNTRY_PK
  ENABLE VALIDATE);


ALTER TABLE CUSTOMER_COUNTRY ADD (
  CONSTRAINT CUSTOMER_COUNTRY_COU_FK 
  FOREIGN KEY (COUNTRY_ID) 
  REFERENCES COUNTRY (COUNTRY_ID)
  ENABLE VALIDATE);

ALTER TABLE CUSTOMER_COUNTRY ADD (
  CONSTRAINT CUSTOMER_COUNTRY_CUS_FK 
  FOREIGN KEY (CUSTOMER_ID) 
  REFERENCES CUSTOMER (CUSTOMER_ID)
  ENABLE VALIDATE);
CREATE TABLE EXCHANGE_RATE
(
  FISCAL_YEAR      NUMBER(4),
  FISCAL_MON       NUMBER(2),
  COUNTRY_ID       NUMBER(10),
  CONV_COUNTRY_ID  NUMBER(10),
  EXCHANGE_RATE    NUMBER, 
  CONSTRAINT EXCHANGE_RATE_PK
  PRIMARY KEY
  (FISCAL_YEAR, FISCAL_MON, COUNTRY_ID)
  ENABLE VALIDATE
)
ORGANIZATION INDEX
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL
MONITORING;
CREATE TABLE FISCAL_CALENDAR
(
  DAY          DATE,
  FISCAL_YEAR  NUMBER(4),
  FISCAL_MON   NUMBER(2), 
  CONSTRAINT FISCAL_CALENDAR_PK
  PRIMARY KEY
  (DAY)
  ENABLE VALIDATE
)
ORGANIZATION INDEX
LOGGING
TABLESPACE DMM_IDX1
NOPARALLEL
MONITORING;
