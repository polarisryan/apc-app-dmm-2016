CREATE OR REPLACE FORCE VIEW V_CURRENT_WEEK
(
   START_DATE,
   END_DATE
)
AS
   (SELECT TRIM (
              TO_CHAR (TRUNC (NEXT_DAY (SYSDATE - 14, 'Monday')),
                       'mm/dd/yyyy')),
           TRIM (
              TO_CHAR (TRUNC (NEXT_DAY (SYSDATE - 7, 'Sunday')),
                       'mm/dd/yyyy'))
      FROM DUAL);
CREATE OR REPLACE FORCE VIEW V_DISPLAY_MANUFACTURER
(
   MANUFACTURER_ID,
   NAME
)
AS
   SELECT DISTINCT manufacturer_id, name
     FROM Manufacturer A, v_dmm_display B
    WHERE A.name = B.manufacturer_name;
CREATE OR REPLACE FORCE VIEW V_DISPLAY_LOCATION
(
   LOCATION_ID,
   DC_CODE,
   DC_NAME,
   ACTIVE_FLG
)
AS
   (SELECT DISTINCT A."LOCATION_ID",
                    A."DC_CODE",
                    A."DC_NAME",
                    A."ACTIVE_FLG"
      FROM location A, display_dc B
     WHERE A.location_id = B.location_ID);
CREATE OR REPLACE FORCE VIEW V_DMM_PACKOUT_DETAIL1
(
   DISPLAY_DC_ID,
   DISPLAY_ID,
   DC_CODE,
   DC_QUANTITY,
   MUST_ARRIVE_DATE,
   SHIP_FROM_DATE,
   PRODUCT_DUE_DATE,
   PACKOUT_QUANTITY,
   SHIP_WAVE_NUM,
   CORRUGATE_DUE_AT_PACKOUT,
   SHIP_FROM_LOCATION,
   SHIP_LOC_PRODUCT_DUE_DATE,
   SHIP_WAVE_QUANTITY
)
AS
   SELECT ddc.display_dc_id,
          ddc.display_id,
          l.dc_code,
          ddc.quantity,
          dsw.must_arrive_date,
          dpo.ship_from_date,
          dpo.product_due_date,
          dpo.quantity,
          dsw.ship_wave_num,
          dpo.corrugate_due_date,
          ls.dc_code,
          dpo.ship_loc_product_due_date,
          dsw.quantity
     FROM display_ship_wave dsw,
          display_packout dpo,
          display_dc ddc,
          LOCATION l,
          LOCATION ls
    WHERE     dpo.display_ship_wave_id = dsw.display_ship_wave_id(+)
          AND ddc.display_dc_id = dpo.display_dc_id(+)
          AND ddc.location_id = l.location_id(+)
          AND dpo.ship_from_location_id = ls.location_id(+);
CREATE OR REPLACE FORCE VIEW V_DMM_DISPLAY_QTRLY_REPORTING
(
   DISPLAY_ID,
   SKU,
   DESCRIPTION,
   QTY_FORECAST,
   PROMO_YEAR,
   SKU_MIX_FINAL_FLG,
   QSI_DOC_NUMBER,
   CUSTOMER_DISPLAY#,
   SENT_SAMPLE_DATE,
   FINAL_ARTWORK_DATE,
   STRUCTURE_APPROVED_FLG,
   PRICE_AVAILABILITY_DATE,
   PRICING_ADMIN_DATE,
   PIM_COMPLETED_DATE,
   PROJECT_LEVEL_PCT,
   COMMENTS,
   CUSTOMER_NAME,
   STATUS_NAME,
   STRUCTURE_NAME,
   PROMO_PERIOD_NAME,
   DISPLAY_SETUP_DESC,
   PROJECT_REP,
   SPECIAL_LABEL_RQT,
   DISPLAY_LENGTH,
   DISPLAY_WIDTH,
   DISPLAY_HEIGHT,
   DISPLAY_WEIGHT,
   DISPLAY_PER_LAYER,
   LAYERS_PER_PALLET,
   SERIAL_SHIP_FLG,
   PALLETS_PER_TRAILER,
   MANUFACTURER_NAME,
   CREATE_DATE,
   CUSTOMER_TYPE_ID,
   CUSTOMER_PARENT,
   GO_NO_GO_DATE,
   RYG_STATUS,
   DISPLAY_SCENARIO_ID
)
AS
   SELECT d.display_id,
          d.sku,
          d.description,
          d.actual_qty,
          d.promo_year,
          d.sku_mix_final_flg,
          d.qsi_doc_number,
          d.customer_display#,
          cm.sent_sample_date,
          cm.final_artwork_date,
          cm.structure_approved_flg,
          cm.price_availability_date,
          cm.pricing_admin_date,
          cm.pim_completed_date,
          cm.project_level_pct,
          cm.comments,
          c.customer_name,
          s.status_name,
          str.structure_name,
          pp.promo_period_name,
          ds.display_setup_desc,
          dsr.first_name || ' ' || dsr.last_name,
          dl.special_label_rqt,
          dl.display_length,
          dl.display_width,
          dl.display_height,
          dl.display_weight,
          dl.display_per_layer,
          dl.layers_per_pallet,
          dl.serial_ship_flg,
          dl.pallets_per_trailer,
          m.NAME,
          d.dt_created,
          c.customer_type_id,
          c.customer_parent,
          d.dt_gonogo,
          d.ryg_status_flg ryg_status,
          dsp_scnr.display_scenario_id
     FROM display d,
          customer_marketing cm,
          (SELECT A.*, B.customer_name customer_parent
             FROM customer A, customer B
            WHERE A.parent_id = B.customer_id(+)) c,
          status s,
          STRUCTURE str,
          promo_period pp,
          display_setup ds,
          (SELECT *
             FROM display_scenario
            WHERE scenario_approval_flg = 'Y') dsp_scnr,
          display_sales_rep dsr,
          display_logistics dl,
          manufacturer m
    WHERE     d.display_id = cm.display_id(+)
          AND d.customer_id = c.customer_id(+)
          AND d.status_id = s.status_id(+)
          AND d.structure_id = str.structure_id(+)
          AND d.promo_period_id = pp.promo_period_id(+)
          AND d.display_setup_id = ds.display_setup_id(+)
          AND d.display_id = dsp_scnr.display_id(+)
          AND d.display_sales_rep_id = dsr.display_sales_rep_id(+)
          AND d.display_id = dl.display_id(+)
          AND d.manufacturer_id = m.manufacturer_id(+);
CREATE OR REPLACE FORCE VIEW V_DISPLAY_QTRLY_MNGMNT_RPTNG
(
   DISPLAY_ID,
   TL_STATUS,
   TL_CUSTOMER,
   TL_PROMO_YEAR,
   TL_PROMO_PERIOD,
   TL_SKU,
   TL_DESCRIPTION,
   TL_STRUCTURE,
   CP_SKU,
   CP_DESCRIPTION,
   CP_QTY_PER_FACING,
   CP_#_FACINGS,
   CP_QTY_PER_DISPLAY,
   TL_QTY,
   SALES_CASE,
   CORRUGATE_COST,
   FULFILLMENT_COST,
   OTHER_COST,
   TTL_COSTS_WO_SHIP,
   COGS,
   VM_PCT_BEFORE_PROGRAM,
   NET_SALES_BEFORE_PROGRAM,
   HURDLES,
   TTL_SALES,
   VM_DOLLAR_HURDLE,
   CHAR_VM_DOLLAR_HURDLE,
   VM_DOLLAR_HURDLE_COLOR,
   VM_PCT_HURDLE,
   CHAR_VM_PCT_HURDLE,
   VM_PCT_HURDLE_COLOR
)
AS
   SELECT A.DISPLAY_ID DISPLAY_ID,
          A.STATUS_NAME TL_STATUS,
          A.CUSTOMER_NAME TL_CUSTOMER,
          A.PROMO_YEAR TL_PROMO_YEAR,
          A.PROMO_PERIOD_NAME TL_PROMO_PERIOD,
          A.SKU TL_SKU,
          A.DESCRIPTION TL_DESCRIPTION,
          A.STRUCTURE_NAME TL_STRUCTURE,
          B.SKU CP_SKU,
          B.PRODUCT_NAME CP_DESCRIPTION,
          B.QTY_PER_FACING CP_QTY_PER_FACING,
          B.NUMBER_FACINGS CP_#_FACINGS,
          B.QTY_PER_FACING * B.NUMBER_FACINGS CP_QTY_PER_DISPLAY,
          D.ACTUAL_QTY TL_QTY,
          C.SUM_TOT_ACTUAL_INVOICE SALES_CASE,
          C.SUM_CORRUGATE_COST CORRUGATE_COST,
          C.SUM_FULFILLMENT_COST FULFILLMENT_COST,
          C.SUM_OTHER_COST OTHER_COST,
          C.SUM_TOT_DISP_COST_NOSHIPPING TTL_COSTS_WO_SHIP,
          C.SUM_COGS COGS,
          C.SUM_VM_PCT_BP VM_PCT_BEFORE_PROGRAM,
          C.SUM_NET_SALES_BP NET_SALES_BEFORE_PROGRAM,
          C.HURDLES HURDLES,
          C.SUM_TRADE_SALES TTL_SALES,
          NVL (C.VM_DOLLAR_HURDLE_RED, C.VM_DOLLAR_HURDLE_GREEN)
             VM_DOLLAR_HURDLE,
          CASE
             WHEN C.VM_DOLLAR_HURDLE_RED IS NULL
             THEN
                TRIM (TO_CHAR (C.VM_DOLLAR_HURDLE_GREEN))
             ELSE
                ' ' || TRIM (TO_CHAR (C.VM_DOLLAR_HURDLE_RED))
          END,
          CASE
             WHEN C.VM_DOLLAR_HURDLE_RED IS NULL THEN 'GREEN'
             ELSE 'RED'
          END,
          NVL (C.VM_PCT_HURDLE_RED, C.VM_PCT_HURDLE_GREEN) VM_PCT_HURDLE,
          CASE
             WHEN C.VM_PCT_HURDLE_RED IS NULL
             THEN
                DECODE (
                   TRIM (TO_CHAR (C.VM_PCT_HURDLE_GREEN, '9999.99')),
                   NULL, NULL,
                   TRIM (TO_CHAR (C.VM_PCT_HURDLE_GREEN, '9999.99')) || '%')
             ELSE
                ' ' || TRIM (TO_CHAR (C.VM_PCT_HURDLE_RED, '9999.99')) || '%'
          END,
          --CASE WHEN C.VM_PCT_HURDLE_RED IS NULL THEN TRIM(TO_CHAR(C.VM_PCT_HURDLE_GREEN,'9999.99'))||'%' ELSE ' '||TRIM(TO_CHAR(C.VM_PCT_HURDLE_RED,'9999.99'))||'%' END,
          CASE WHEN C.VM_PCT_HURDLE_RED IS NULL THEN 'GREEN' ELSE 'RED' END
     FROM v_dmm_display_qtrly_reporting A,
          KIT_COMPONENT B,
          CALCULATION_SUMMARY C,
          (SELECT DISPLAY_ID, ACTUAL_QTY
             FROM DISPLAY_SCENARIO
            WHERE scenario_approval_flg = 'Y') D
    WHERE     A.DISPLAY_ID = B.DISPLAY_ID(+)
          AND A.DISPLAY_ID = C.DISPLAY_ID(+)
          AND A.DISPLAY_ID = D.DISPLAY_ID(+)
          AND A.DISPLAY_SCENARIO_ID = B.DISPLAY_SCENARIO_ID(+);
CREATE OR REPLACE FORCE VIEW V_DISPLAY_CORRUGATE_COMPONENT
(
   CORRUGATE_COMPONENT_ID,
   DISPLAY_ID,
   COUNTRY,
   COUNTRY_CODE,
   VENDOR_NAME,
   VENDOR_PART_NUM,
   GLOVIA_PART_NUM,
   DESCRIPTION,
   BOARD_TEST,
   MATERIAL,
   QTY_PER_DISPLAY,
   PIECE_PRICE,
   TOTAL_COST,
   TOTAL_QTY
)
AS
   SELECT A.corrugate_component_id,
          A.display_id,
          C.COUNTRY_NAME COUNTRY,
          C.COUNTRY_CODE COUNTRY_CODE,
          B.NAME AS VENDOR_NAME,
          A.vendor_part_num,
          A.glovia_part_num,
          A.description,
          A.board_test,
          A.material,
          A.qty_per_display,
          A.piece_price,
          A.total_cost,
          A.total_qty
     FROM CORRUGATE_COMPONENT A, MANUFACTURER B, COUNTRY C
    WHERE     A.manufacturer_id = B.manufacturer_id(+)
          AND A.country_id = C.country_id(+);
CREATE OR REPLACE FORCE VIEW V_USER_CHANGE_LOG
(
   USER_CHANGE_LOG_ID,
   USERNAME,
   DT_CHANGED,
   TABLE_NAME,
   FIELD,
   FRIENDLY_NAME,
   OLD_VALUE,
   NEW_VALUE,
   DISPLAY_ID,
   CHANGE_TYPE,
   PRIMARY_KEY_ID,
   COMPONENT_SKU,
   TOP_LEVEL_SKU,
   DT_CHANGED_DATE
)
AS
   (                                           --DISPLAY TABLE RELATED CHANGES
    SELECT A."USER_CHANGE_LOG_ID",
           A."USERNAME",
           A."DT_CHANGED",
           A."TABLE_NAME",
           A."FIELD",
           A."FRIENDLY_NAME",
           A."OLD_VALUE",
           A."NEW_VALUE",
           A."DISPLAY_ID",
           A."CHANGE_TYPE",
           A."PRIMARY_KEY_ID",
           '' Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B
     WHERE     LOWER (A.TABLE_NAME) = 'display'
           AND LOWER (A.CHANGE_TYPE) = 'update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
    UNION ALL
    --CUSTOMER_MARKETING TABLE RELATED CHANGES
    SELECT A."USER_CHANGE_LOG_ID",
           A."USERNAME",
           A."DT_CHANGED",
           A."TABLE_NAME",
           A."FIELD",
           A."FRIENDLY_NAME",
           A."OLD_VALUE",
           A."NEW_VALUE",
           A."DISPLAY_ID",
           A."CHANGE_TYPE",
           A."PRIMARY_KEY_ID",
           '' Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B
     WHERE     LOWER (A.TABLE_NAME) = 'customer_marketing'
           AND LOWER (A.CHANGE_TYPE) = 'update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
    UNION ALL
    --DISPLAY_LOGISTICS TABLE RELATED CHANGES
    SELECT A."USER_CHANGE_LOG_ID",
           A."USERNAME",
           A."DT_CHANGED",
           A."TABLE_NAME",
           A."FIELD",
           A."FRIENDLY_NAME",
           A."OLD_VALUE",
           A."NEW_VALUE",
           A."DISPLAY_ID",
           A."CHANGE_TYPE",
           A."PRIMARY_KEY_ID",
           '' Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B
     WHERE     LOWER (A.TABLE_NAME) = 'display_logistics'
           AND LOWER (A.CHANGE_TYPE) = 'update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
    UNION ALL
    --DISPLAY_MATERIAL TABLE RELATED CHANGES
    SELECT A."USER_CHANGE_LOG_ID",
           A."USERNAME",
           A."DT_CHANGED",
           A."TABLE_NAME",
           A."FIELD",
           A."FRIENDLY_NAME",
           A."OLD_VALUE",
           A."NEW_VALUE",
           A."DISPLAY_ID",
           A."CHANGE_TYPE",
           A."PRIMARY_KEY_ID",
           '' Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B
     WHERE     LOWER (A.TABLE_NAME) = 'display_material'
           AND LOWER (A.CHANGE_TYPE) = 'update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
    UNION ALL
    --DISPLAY_MATERIAL TABLE RELATED CHANGES
    SELECT A."USER_CHANGE_LOG_ID",
           A."USERNAME",
           A."DT_CHANGED",
           A."TABLE_NAME",
           A."FIELD",
           A."FRIENDLY_NAME",
           A."OLD_VALUE",
           A."NEW_VALUE",
           A."DISPLAY_ID",
           A."CHANGE_TYPE",
           A."PRIMARY_KEY_ID",
           '' Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B
     WHERE     LOWER (A.TABLE_NAME) = 'display_cost'
           AND LOWER (A.CHANGE_TYPE) = 'update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
    UNION ALL
    --DISPLAY_DC TABLE RELATED CHANGES
    SELECT A."USER_CHANGE_LOG_ID",
           A."USERNAME",
           A."DT_CHANGED",
           A."TABLE_NAME",
           A."FIELD",
           A."FRIENDLY_NAME",
           A."OLD_VALUE",
           A."NEW_VALUE",
           A."DISPLAY_ID",
           A."CHANGE_TYPE",
           A."PRIMARY_KEY_ID",
           '' Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B, DISPLAY_DC C
     WHERE     LOWER (A.TABLE_NAME) = 'display_dc'
           AND LOWER (A.CHANGE_TYPE) = 'update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
           AND A.PRIMARY_KEY_ID = C.display_dc_id
    UNION ALL
    --DISPLAY_PACKOUT TABLE RELATED CHANGES
    SELECT A."USER_CHANGE_LOG_ID",
           A."USERNAME",
           A."DT_CHANGED",
           A."TABLE_NAME",
           A."FIELD",
           A."FRIENDLY_NAME",
           A."OLD_VALUE",
           A."NEW_VALUE",
           A."DISPLAY_ID",
           A."CHANGE_TYPE",
           A."PRIMARY_KEY_ID",
           '' Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B, DISPLAY_PACKOUT C
     WHERE     LOWER (A.TABLE_NAME) = 'display_packout'
           AND LOWER (A.CHANGE_TYPE) = 'update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
           AND A.PRIMARY_KEY_ID = C.display_packout_id
    UNION ALL
    --DISPLAY_SHIP_WAVE TABLE RELATED CHANGES
    SELECT A."USER_CHANGE_LOG_ID",
           A."USERNAME",
           A."DT_CHANGED",
           A."TABLE_NAME",
           A."FIELD",
           A."FRIENDLY_NAME",
           A."OLD_VALUE",
           A."NEW_VALUE",
           A."DISPLAY_ID",
           A."CHANGE_TYPE",
           A."PRIMARY_KEY_ID",
           '' Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B, DISPLAY_SHIP_WAVE C
     WHERE     LOWER (A.TABLE_NAME) = 'display_ship_wave'
           AND LOWER (A.CHANGE_TYPE) = 'update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
           AND A.PRIMARY_KEY_ID = C.display_ship_wave_id
    UNION ALL
    --KIT_COMPONENT TABLE RELATED CHANGES
    SELECT A."USER_CHANGE_LOG_ID",
           A."USERNAME",
           A."DT_CHANGED",
           A."TABLE_NAME",
           A."FIELD",
           A."FRIENDLY_NAME",
           A."OLD_VALUE",
           A."NEW_VALUE",
           A."DISPLAY_ID",
           A."CHANGE_TYPE",
           A."PRIMARY_KEY_ID",
           C.SKU Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B, KIT_COMPONENT C
     WHERE     LOWER (A.TABLE_NAME) = 'kit_component'
           AND A.CHANGE_TYPE = 'Update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
           AND A.PRIMARY_KEY_ID = C.KIT_COMPONENT_ID
    UNION ALL
    --DISPLAY_PLANNER TABLE RELATED CHANGES
    SELECT A."USER_CHANGE_LOG_ID",
           A."USERNAME",
           A."DT_CHANGED",
           A."TABLE_NAME",
           A."FIELD",
           A."FRIENDLY_NAME",
           A."OLD_VALUE",
           A."NEW_VALUE",
           A."DISPLAY_ID",
           A."CHANGE_TYPE",
           A."PRIMARY_KEY_ID",
           C.SKU Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A,
           DISPLAY B,
           KIT_COMPONENT C,
           DISPLAY_PLANNER D
     WHERE     LOWER (A.TABLE_NAME) = 'display_planner'
           AND A.CHANGE_TYPE = 'Update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
           AND A.PRIMARY_KEY_ID = D.display_planner_id
           AND D.kit_component_id = C.kit_component_id
           AND C.DISPLAY_ID = B.DISPLAY_ID
    --FOR INVENTORY REQUIREMENT
    UNION ALL
    SELECT A.user_change_log_id,
           A.username,
           A.dt_changed,
           'kit_component' table_name,
           --A.table_name         ,
           'Inventory Requirement' Field,
           'Inventory Requirement' FRIENDLY_NAME,
           TO_CHAR (A.old_value * C.qty_per_facing * C.number_facings)
              old_value,
           TO_CHAR (A.new_value * C.qty_per_facing * C.number_facings)
              new_value,
           A.display_id,
           A.change_type,
           C.kit_component_id primary_key_id,
           C.SKU Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B, KIT_COMPONENT C
     WHERE     A.TABLE_NAME = 'display'
           AND A.FIELD IN ('qty_forecast')
           AND A.CHANGE_TYPE = 'Update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
           AND A.DISPLAY_ID = C.DISPLAY_ID
    UNION ALL
    SELECT A.user_change_log_id,
           A.username,
           A.dt_changed,
           A.table_name,
           'Inventory Requirement' Field,
           'Inventory Requirement' FRIENDLY_NAME,
           TO_CHAR (A.old_value * B.qty_forecast * C.number_facings)
              old_value,
           TO_CHAR (A.new_value * B.qty_forecast * C.number_facings)
              new_value,
           A.display_id,
           A.change_type,
           A.primary_key_id,
           C.SKU Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B, KIT_COMPONENT C
     WHERE     A.TABLE_NAME = 'kit_component'
           AND A.FIELD IN ('qty_per_facing')
           AND A.CHANGE_TYPE = 'Update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
           AND A.PRIMARY_KEY_ID = C.KIT_COMPONENT_ID
    UNION ALL
    SELECT A.user_change_log_id,
           A.username,
           A.dt_changed,
           A.table_name,
           'Inventory Requirement' Field,
           'Inventory Requirement' FRIENDLY_NAME,
           TO_CHAR (A.old_value * B.qty_forecast * C.qty_per_facing)
              old_value,
           TO_CHAR (A.new_value * B.qty_forecast * C.qty_per_facing)
              new_value,
           A.display_id,
           A.change_type,
           A.primary_key_id,
           C.SKU Component_SKU,
           B.SKU Top_Level_SKU,
           TRUNC (A.DT_CHANGED) DT_CHANGED_DATE
      FROM USER_CHANGE_LOG A, DISPLAY B, KIT_COMPONENT C
     WHERE     A.TABLE_NAME = 'kit_component'
           AND A.FIELD IN ('number_facings')
           AND A.CHANGE_TYPE = 'Update'
           AND A.DISPLAY_ID = B.DISPLAY_ID
           AND A.PRIMARY_KEY_ID = C.KIT_COMPONENT_ID);
CREATE OR REPLACE FORCE VIEW V_USER_CHANGE_LOG_REPORT
(
   USER_CHANGE_LOG_ID,
   USERNAME,
   DT_CHANGED,
   TABLE_NAME,
   FIELD,
   FRIENDLY_NAME,
   OLD_VALUE,
   NEW_VALUE,
   DISPLAY_ID,
   CHANGE_TYPE,
   PRIMARY_KEY_ID,
   COMPONENT_SKU,
   TOP_LEVEL_SKU,
   DT_CHANGED_DATE
)
AS
   (SELECT USER_CHANGE_LOG_ID,
           USERNAME,
           DT_CHANGED,
           TABLE_NAME,
           FIELD,
           FRIENDLY_NAME,
           --OLD_VALUE,
           CASE
              WHEN LOWER (A.FIELD) = 'status_id'
              THEN
                 (SELECT B.STATUS_NAME
                    FROM STATUS B
                   WHERE A.OLD_VALUE = B.status_id(+))
              WHEN LOWER (A.FIELD) = 'display_sales_rep_id'
              THEN
                 (SELECT dsr.first_name || ' ' || dsr.last_name
                    FROM display_sales_rep DSR
                   WHERE A.OLD_VALUE = DSR.DISPLAY_SALES_REP_ID(+))
              WHEN LOWER (A.FIELD) = 'manufacturer_id'
              THEN
                 (SELECT MFG.NAME
                    FROM MANUFACTURER MFG
                   WHERE A.OLD_VALUE = MFG.MANUFACTURER_ID(+))
              WHEN LOWER (A.FIELD) = 'location_id'
              THEN
                 (SELECT LOC.DC_CODE
                    FROM LOCATION LOC
                   WHERE A.OLD_VALUE = LOC.LOCATION_ID(+))
              WHEN LOWER (A.FIELD) = 'customer_id'
              THEN
                 (SELECT CUST.CUSTOMER_NAME
                    FROM CUSTOMER CUST
                   WHERE A.OLD_VALUE = CUST.CUSTOMER_ID(+))
              WHEN LOWER (A.FIELD) = 'promo_period_id'
              THEN
                 (SELECT PPD.PROMO_PERIOD_NAME
                    FROM PROMO_PERIOD PPD
                   WHERE A.OLD_VALUE = PPD.PROMO_PERIOD_ID(+))
              WHEN LOWER (A.FIELD) = 'display_setup_id'
              THEN
                 (SELECT DSTP.DISPLAY_SETUP_DESC
                    FROM DISPLAY_SETUP DSTP
                   WHERE A.OLD_VALUE = DSTP.DISPLAY_SETUP_ID(+))
              WHEN LOWER (A.FIELD) = 'structure_id'
              THEN
                 (SELECT STRUCT.STRUCTURE_NAME
                    FROM STRUCTURE STRUCT
                   WHERE A.OLD_VALUE = STRUCT.STRUCTURE_ID(+))
              WHEN LOWER (A.FIELD) = 'display_dc_id'
              THEN
                 (SELECT LOCN.DC_CODE
                    FROM display_dc DDC, LOCATION LOCN
                   WHERE     DDC.LOCATION_ID = LOCN.LOCATION_ID
                         AND A.OLD_VALUE = DDC.DISPLAY_DC_ID)
              ELSE
                 OLD_VALUE
           END
              OLD_VALUE,
           --NEW_VALUE,
           CASE
              WHEN LOWER (A.FIELD) = 'status_id'
              THEN
                 (SELECT B.STATUS_NAME
                    FROM STATUS B
                   WHERE A.NEW_VALUE = B.status_id(+))
              WHEN LOWER (A.FIELD) = 'display_sales_rep_id'
              THEN
                 (SELECT dsr.first_name || ' ' || dsr.last_name
                    FROM display_sales_rep DSR
                   WHERE A.NEW_VALUE = DSR.DISPLAY_SALES_REP_ID(+))
              WHEN LOWER (A.FIELD) = 'manufacturer_id'
              THEN
                 (SELECT MFG.NAME
                    FROM MANUFACTURER MFG
                   WHERE A.NEW_VALUE = MFG.MANUFACTURER_ID(+))
              WHEN LOWER (A.FIELD) = 'location_id'
              THEN
                 (SELECT LOC.DC_CODE
                    FROM LOCATION LOC
                   WHERE A.NEW_VALUE = LOC.LOCATION_ID(+))
              WHEN LOWER (A.FIELD) = 'customer_id'
              THEN
                 (SELECT CUST.CUSTOMER_NAME
                    FROM CUSTOMER CUST
                   WHERE A.NEW_VALUE = CUST.CUSTOMER_ID(+))
              WHEN LOWER (A.FIELD) = 'promo_period_id'
              THEN
                 (SELECT PPD.PROMO_PERIOD_NAME
                    FROM PROMO_PERIOD PPD
                   WHERE A.NEW_VALUE = PPD.PROMO_PERIOD_ID(+))
              WHEN LOWER (A.FIELD) = 'display_setup_id'
              THEN
                 (SELECT DSTP.DISPLAY_SETUP_DESC
                    FROM DISPLAY_SETUP DSTP
                   WHERE A.NEW_VALUE = DSTP.DISPLAY_SETUP_ID(+))
              WHEN LOWER (A.FIELD) = 'structure_id'
              THEN
                 (SELECT STRUCT.STRUCTURE_NAME
                    FROM STRUCTURE STRUCT
                   WHERE A.NEW_VALUE = STRUCT.STRUCTURE_ID(+))
              WHEN LOWER (A.FIELD) = 'display_dc_id'
              THEN
                 (SELECT LOCN.DC_CODE
                    FROM display_dc DDC, LOCATION LOCN
                   WHERE     DDC.LOCATION_ID = LOCN.LOCATION_ID
                         AND A.NEW_VALUE = DDC.DISPLAY_DC_ID)
              ELSE
                 NEW_VALUE
           END
              NEW_VALUE,
           DISPLAY_ID,
           CHANGE_TYPE,
           PRIMARY_KEY_ID,
           COMPONENT_SKU,
           TOP_LEVEL_SKU,
           DT_CHANGED_DATE
      FROM V_USER_CHANGE_LOG A
     WHERE LOWER (A.FIELD) IN
              ('sku',
               'sku_mix_final_flg',
               'qty_forecast',
               'location_id',
               'status_id',
               'manufacturer_id',
               'comments',
               'customer_id',
               'kit_version_code',
               'i2_forecast_flg',
               'qty_per_facing',
               'number_facings',
               'pbc_version',
               'plant_code',
               'planner_code',
               'inventory requirement',
               'ship_from_date',
               'product_due_date',
               'bu_desc'));
CREATE OR REPLACE FORCE VIEW V_PRIOR_WEEK
(
   START_DATE,
   END_DATE
)
AS
   (SELECT TRIM (
              TO_CHAR (TRUNC (NEXT_DAY (SYSDATE - 14, 'Sunday')),
                       'mm/dd/yyyy')),
           TRIM (
              TO_CHAR (TRUNC (NEXT_DAY (SYSDATE - 7, 'Saturday')),
                       'mm/dd/yyyy'))
      FROM DUAL);
CREATE OR REPLACE FORCE VIEW V_LOCATION
(
   DC_CODE,
   DC_NAME
)
AS
   (SELECT dc_code, dc_name
      FROM location
    UNION ALL
    SELECT 'All' dc_code, 'All' dc_name
      FROM DUAL);
CREATE OR REPLACE FORCE VIEW V_DMM_DISPLAY_COMPONENTS_DC
(
   DC_CODE,
   DC_NAME,
   DISPLAY_ID
)
AS
   SELECT DISTINCT a.dc_code, b.dc_name, a.display_id
     FROM v_dmm_components a, LOCATION b
    WHERE     a.display_id IS NOT NULL
          AND a.dc_code IS NOT NULL
          AND a.dc_code = b.dc_code;
CREATE OR REPLACE FORCE VIEW V_DISPLAY_MANUFACTURER_FILTER (NAME)
AS
   (SELECT DISTINCT NAME
      FROM V_DISPLAY_MANUFACTURER
    UNION ALL
    SELECT 'All' NAME
      FROM DUAL);
CREATE OR REPLACE FORCE VIEW V_STATUS
(
   STATUS_ID,
   STATUS_NAME,
   DESCRIPTION
)
AS
   (SELECT status_id, status_name, description
      FROM status
    UNION ALL
    SELECT 99 status_id, 'All' status_name, 'All'
      FROM DUAL);
CREATE OR REPLACE FORCE VIEW V_DMM_COMPONENTS
(
   KIT_COMPONENT_ID,
   DISPLAY_ID,
   SKU,
   PRODUCT_NAME,
   BU_DESC,
   BREAK_CASE,
   PROMO_FLG,
   I2_FORECAST_FLG,
   SOURCE_PIM_FLG,
   KIT_VERSION_CODE,
   PLANT_BREAK_CASE_FLG,
   PBC_VERSION,
   FINISHED_GOODS_SKU,
   FINISHED_GOODS_QTY,
   QTY_PER_FACING,
   NUMBER_FACINGS,
   REG_CASE_PACK,
   PLANT_CODE,
   PLANNER_CODE,
   LEAD_TIME,
   DC_CODE,
   DC_QUANTITY,
   SEQUENCE_NUM
)
AS
   SELECT kc.kit_component_id,
          kc.display_id,
          kc.sku,
          kc.product_name,
          kc.bu_desc,
          kc.break_case,
          kc.promo_flg,
          kc.i2_forecast_flg,
          kc.source_pim_flg,
          kc.kit_version_code,
          kc.plant_break_case_flg,
          kc.pbc_version,
          kc.finished_goods_sku,
          kc.finished_goods_qty,
          kc.qty_per_facing,
          kc.number_facings,
          kc.reg_case_pack,
          dp.plant_code,
          dp.planner_code,
          dp.lead_time,
          l.dc_code,
          ddc.quantity,
          kc.sequence_num
     FROM kit_component kc,
          display_planner dp,
          display_dc ddc,
          LOCATION l
    WHERE     kc.kit_component_id = dp.kit_component_id(+)
          AND dp.display_dc_id = ddc.display_dc_id(+)
          AND ddc.location_id = l.location_id(+);
CREATE OR REPLACE FORCE VIEW V_DMM_CORRUGATE_VENDOR
(
   DISPLAY_ID,
   MANUFACTURER_NAME,
   VENDOR_PART_NUM,
   GLOVIA_PART,
   DESCRIPTION,
   UNIT_COST,
   DELIVERY_DATE,
   QUANTITY
)
AS
   SELECT dm.display_id,
          m.NAME,
          dm.vendor_part_num,
          dm.glovia_part,
          dm.description,
          dm.unit_cost,
          dm.delivery_date,
          dm.quantity
     FROM manufacturer m, display_material dm
    WHERE m.manufacturer_id = dm.manufacturer_id;
CREATE OR REPLACE FORCE VIEW V_DMM_DISPLAY
(
   DISPLAY_ID,
   SKU,
   DESCRIPTION,
   QTY_FORECAST,
   PROMO_YEAR,
   SKU_MIX_FINAL_FLG,
   QSI_DOC_NUMBER,
   CUSTOMER_DISPLAY#,
   SENT_SAMPLE_DATE,
   FINAL_ARTWORK_DATE,
   STRUCTURE_APPROVED_FLG,
   PRICE_AVAILABILITY_DATE,
   PRICING_ADMIN_DATE,
   PIM_COMPLETED_DATE,
   PROJECT_LEVEL_PCT,
   COMMENTS,
   CUSTOMER_NAME,
   STATUS_NAME,
   STRUCTURE_NAME,
   PROMO_PERIOD_NAME,
   DISPLAY_SETUP_DESC,
   COST_CENTER,
   CM_PROJECT_NUM,
   SSCID_PROJECT_NUM,
   PROJECT_REP,
   SPECIAL_LABEL_RQT,
   DISPLAY_LENGTH,
   DISPLAY_WIDTH,
   DISPLAY_HEIGHT,
   DISPLAY_WEIGHT,
   DISPLAY_PER_LAYER,
   LAYERS_PER_PALLET,
   SERIAL_SHIP_FLG,
   PALLETS_PER_TRAILER,
   MANUFACTURER_NAME,
   CREATE_DATE,
   CUSTOMER_TYPE_ID,
   CUSTOMER_PARENT,
   GO_NO_GO_DATE,
   RYG_STATUS,
   COUNTRY_NAME
)
AS
   SELECT d.display_id,
          d.sku,
          d.description,
          d.actual_qty,
          d.promo_year,
          d.sku_mix_final_flg,
          d.qsi_doc_number,
          d.customer_display#,
          cm.sent_sample_date,
          cm.final_artwork_date,
          cm.structure_approved_flg,
          cm.price_availability_date,
          cm.pricing_admin_date,
          cm.pim_completed_date,
          cm.project_level_pct,
          cm.comments,
          c.customer_name,
          s.status_name,
          str.structure_name,
          pp.promo_period_name,
          ds.display_setup_desc,
          dc.cost_center,
          dc.cm_project_num,
          dc.sscid_project_num,
          dsr.first_name || ' ' || dsr.last_name,
          dl.special_label_rqt,
          dl.display_length,
          dl.display_width,
          dl.display_height,
          dl.display_weight,
          dl.display_per_layer,
          dl.layers_per_pallet,
          dl.serial_ship_flg,
          dl.pallets_per_trailer,
          m.NAME,
          d.dt_created,
          c.customer_type_id,
          c.customer_parent,
          d.dt_gonogo,
          d.ryg_status_flg ryg_status,
          cntry.country_name country_name
     FROM display d,
          customer_marketing cm,
          (SELECT A.*, B.customer_name customer_parent
             FROM customer A, customer B
            WHERE A.parent_id = B.customer_id(+)) c,
          status s,
          STRUCTURE str,
          promo_period pp,
          display_setup ds,
          display_cost dc,
          display_sales_rep dsr,
          display_logistics dl,
          manufacturer m,
          country cntry
    WHERE     d.display_id = cm.display_id(+)
          AND d.customer_id = c.customer_id(+)
          AND d.status_id = s.status_id(+)
          AND d.structure_id = str.structure_id(+)
          AND d.promo_period_id = pp.promo_period_id(+)
          AND d.display_setup_id = ds.display_setup_id(+)
          AND d.display_id = dc.display_id(+)
          AND d.display_sales_rep_id = dsr.display_sales_rep_id(+)
          AND d.display_id = dl.display_id(+)
          AND d.manufacturer_id = m.manufacturer_id(+)
          AND d.country_id = cntry.country_id(+);
CREATE OR REPLACE FORCE VIEW V_DMM_PACKOUT_DETAIL
(
   DISPLAY_ID,
   DC_CODE,
   DC_QUANTITY,
   MUST_ARRIVE_DATE,
   SHIP_FROM_DATE,
   PRODUCT_DUE_DATE,
   PACKOUT_QUANTITY,
   SHIP_WAVE_NUM,
   CORRUGATE_DUE_AT_PACKOUT,
   SHIP_FROM_LOCATION,
   SHIP_LOC_PRODUCT_DUE_DATE
)
AS
   SELECT ddc.display_id,
          l.dc_code,
          ddc.quantity,
          dsw.must_arrive_date,
          dpo.ship_from_date,
          dpo.product_due_date,
          dpo.quantity,
          dsw.ship_wave_num,
          dpo.corrugate_due_date,
          ls.dc_code,
          dpo.ship_loc_product_due_date
     FROM display_ship_wave dsw,
          display_packout dpo,
          display_dc ddc,
          LOCATION l,
          LOCATION ls
    WHERE     dpo.display_ship_wave_id = dsw.display_ship_wave_id(+)
          AND ddc.display_dc_id = dpo.display_dc_id(+)
          AND ddc.location_id = l.location_id(+)
          AND dpo.ship_from_location_id = ls.location_id(+);
CREATE OR REPLACE FORCE VIEW V_DMM_SHIP_WAVE
(
   DISPLAY_ID,
   MUST_ARRIVE_DATE,
   QUANTITY,
   DESTINATION,
   SHIP_WAVE_NUM
)
AS
   SELECT dsw.display_id,
          dsw.must_arrive_date,
          dsw.quantity,
          CASE destination_flg WHEN 'N' THEN 'Domestic' ELSE 'Over Seas' END,
          dsw.ship_wave_num
     FROM display_ship_wave dsw;
CREATE OR REPLACE FORCE VIEW V_DISPLAY_SALES_REP
(
   DISPLAY_SALES_REP_ID,
   FIRST_NAME,
   LAST_NAME,
   REP_CODE,
   PROJECT_REP
)
AS
   SELECT display_sales_rep_id,
          first_name,
          last_name,
          rep_code,
          first_name || ' ' || last_name
     FROM display_sales_rep;
CREATE OR REPLACE FORCE VIEW V_DISPLAY_DEMAND
(
   DISPLAY_ID,
   TL_SKU,
   TL_STATUS,
   TL_PROJECT_REP,
   TL_CUSTOMER,
   TL_DESCRIPTION,
   TL_QTY,
   VENDOR,
   CP_BU,
   CP_SKU,
   CP_DESCRIPTION,
   CP_QTY_PER_DISPLAY,
   CP_DISPLAY_SETUP_TYPE,
   VER_CODE_SETUP,
   CP_FG_CONSISTS_OF_SKU,
   CP_I2_FCST,
   CP_SKU_MIX_FINAL,
   CP_PBC_15_DIGIT,
   CP_PLANT,
   CP_PLANNER,
   CP_DC,
   PACKOUT_LOC,
   PRD_DUE_DATE,
   SHIP_FROM_LOCATION,
   PACKOUT_QTY,
   SHIP_QTY,
   SHIP_FROM_DATE,
   MUST_ARRIVE_BY_DATE,
   CP_FG_QTY
)
AS
   SELECT A.DISPLAY_ID,
          A.SKU AS TL_SKU,
          A.STATUS_NAME AS TL_STATUS,
          A.PROJECT_REP AS TL_PROJECT_REP,
          A.CUSTOMER_NAME AS TL_CUSTOMER,
          A.DESCRIPTION AS TL_DESCRIPTION,
          A.QTY_FORECAST AS TL_QTY,
          A.MANUFACTURER_NAME AS VENDOR,
          B.BU_DESC AS CP_BU,
          B.SKU AS CP_SKU,
          B.PRODUCT_NAME AS CP_DESCRIPTION,
          B.QTY_PER_FACING * B.NUMBER_FACINGS AS CP_QTY_PER_DISPLAY,
          A.DISPLAY_SETUP_DESC AS CP_DISPLAY_SETUP_TYPE,
          DECODE (BREAK_CASE,
                  'Production Version', 'PV',
                  'Plant Break Case', 'PBC',
                  'Break Case', 'BC',
                  Break_Case)
             AS VER_CODE_SETUP,
          B.FINISHED_GOODS_SKU AS CP_FG_CONSISTS_OF_SKU,
          B.I2_FORECAST_FLG AS CP_I2_FCST,
          A.SKU_MIX_FINAL_FLG AS CP_SKU_MIX_FINAL,
          CASE
             WHEN B.PBC_VERSION IS NULL THEN NULL
             ELSE SUBSTR (B.SKU, 1, 10) || TRIM (PBC_VERSION)
          END
             CP_PBC_15_DIGIT,
          B.PLANT_CODE AS CP_PLANT,
          B.PLANNER_CODE AS CP_PLANNER,
          B.DC_CODE AS CP_DC,
          C.DC_CODE AS PACKOUT_LOC,
          C.PRODUCT_DUE_DATE AS PRD_DUE_DATE,
          C.SHIP_FROM_LOCATION AS SHIP_FROM_LOCATION,
          C.DC_QUANTITY AS PACKOUT_QTY,
          C.PACKOUT_QUANTITY AS SHIP_QTY,
          C.SHIP_FROM_DATE AS SHIP_FROM_DATE,
          C.MUST_ARRIVE_DATE AS MUST_ARRIVE_BY_DATE,
          B.Finished_Goods_Qty AS CP_FG_QTY
     FROM V_DMM_DISPLAY A, V_DMM_COMPONENTS B, V_DMM_PACKOUT_DETAIL C
    WHERE     A.DISPLAY_ID = B.DISPLAY_ID(+)
          AND A.DISPLAY_ID = C.DISPLAY_ID(+)
          AND A.STATUS_NAME IN ('Approved', 'Proposed')
--a.display_id = 208
;
CREATE OR REPLACE FORCE VIEW V_LAST_7_DAYS
(
   START_DATE,
   END_DATE
)
AS
   SELECT TRIM (TO_CHAR (TRUNC (SYSDATE - 6), 'mm/dd/yyyy')) Start_date,
          TRIM (TO_CHAR (TRUNC (SYSDATE), 'mm/dd/yyyy')) End_date
     FROM DUAL;
CREATE OR REPLACE FORCE VIEW V_DMM_CUSTOMER_COUNTRY
(
   CUSTOMER_ID,
   CUSTOMER_NAME,
   ACTIVE_FLG,
   COUNTRY_ID,
   COUNTRY_NAME,
   ACTIVE_COUNTRY_FLG
)
AS
   SELECT a.customer_id,
          a.customer_name,
          a.active_flg,
          b.country_id,
          b.country_name,
          b.active_country_flg
     FROM customer a, country b, customer_country c
    WHERE a.customer_id = c.customer_id(+) AND b.country_id(+) = c.country_id;
CREATE OR REPLACE FORCE VIEW V_DMM_MANUFACTURER_COUNTRY
(
   MANUFACTURER_ID,
   NAME,
   ACTIVE_FLG,
   COUNTRY_ID,
   COUNTRY_NAME,
   ACTIVE_COUNTRY_FLG
)
AS
   SELECT a.manufacturer_id,
          a.NAME,
          a.active_flg,
          b.country_id,
          b.country_name,
          b.active_country_flg
     FROM manufacturer a, country b, manufacturer_country c
    WHERE     a.manufacturer_id = c.manufacturer_id(+)
          AND b.country_id(+) = c.country_id;
CREATE OR REPLACE FORCE VIEW V_DMM_PACKOUT_LOCATION_COUNTRY
(
   LOCATION_ID,
   DC_CODE,
   DC_NAME,
   ACTIVE_FLG,
   COUNTRY_ID,
   COUNTRY_NAME,
   ACTIVE_COUNTRY_FLG
)
AS
   SELECT a.location_id,
          a.dc_code,
          a.dc_name,
          a.active_flg,
          b.country_id,
          b.country_name,
          b.active_country_flg
     FROM LOCATION a, country b, location_country c
    WHERE a.location_id = c.location_id(+) AND b.country_id(+) = c.country_id;
CREATE OR REPLACE FORCE VIEW V_DMM_PROMO_PERIOD_COUNTRY
(
   PROMO_PERIOD_ID,
   PROMO_PERIOD_NAME,
   ACTIVE_FLG,
   COUNTRY_ID,
   COUNTRY_NAME,
   ACTIVE_COUNTRY_FLG
)
AS
   SELECT a.promo_period_id,
          a.promo_period_name,
          a.active_flg,
          b.country_id,
          b.country_name,
          b.active_country_flg
     FROM promo_period a, country b, promo_period_country c
    WHERE     a.promo_period_id = c.promo_period_id(+)
          AND b.country_id(+) = c.country_id;
CREATE OR REPLACE FORCE VIEW V_DMM_DISPLAY_EXCHANGE_RATE
(
   DISPLAY_ID,
   APPROVAL_DATE,
   COUNTRY_NAME,
   USD_EXCHANGE_RATE,
   CANADIAN_EXCHANGE_RATE
)
AS
   SELECT DISTINCT
          (a.display_id),
          NVL (b.approval_date, SYSDATE) AS approval_date,
          a.country_name,
          NVL (c.usd_exchange_rate, d.exchange_rate) AS usd_exchange_rate,
          ROUND (NVL (c.canadian_exchange_rate, (1 / d.exchange_rate * 1)),
                 6)
             AS canadian_exchange_rate
     FROM (SELECT DISTINCT (a.display_id), c.country_name
             FROM display a, country c
            WHERE a.country_id = c.country_id) a,
          (SELECT DISTINCT
                  (a.display_id),
                  (CASE b.scenario_status_id
                      WHEN 3 THEN NVL (b.dt_last_changed, SYSDATE)
                      ELSE SYSDATE
                   END)
                     AS approval_date
             FROM display a, display_scenario b, country c
            WHERE b.scenario_status_id = 3 AND a.display_id = b.display_id(+)
           ORDER BY 1 DESC) b,
          (SELECT DISTINCT
                  (a.display_id),
                  (SELECT exchange_rate
                     FROM exchange_rate
                    WHERE     fiscal_year =
                                 TO_NUMBER (
                                    TO_CHAR (
                                       (CASE b.scenario_status_id
                                           WHEN 3
                                           THEN
                                              NVL (b.dt_last_changed,
                                                   SYSDATE)
                                           ELSE
                                              SYSDATE
                                        END),
                                       'yyyy'))
                          AND fiscal_mon =
                                 TO_NUMBER (
                                    TO_CHAR (
                                       (CASE b.scenario_status_id
                                           WHEN 3
                                           THEN
                                              NVL (b.dt_last_changed,
                                                   SYSDATE)
                                           ELSE
                                              SYSDATE
                                        END),
                                       'mm'))
                          AND country_id = 235)
                     AS usd_exchange_rate,
                  (SELECT exchange_rate
                     FROM exchange_rate
                    WHERE     fiscal_year =
                                 TO_NUMBER (
                                    TO_CHAR (
                                       (CASE b.scenario_status_id
                                           WHEN 3
                                           THEN
                                              NVL (b.dt_last_changed,
                                                   SYSDATE)
                                           ELSE
                                              SYSDATE
                                        END),
                                       'yyyy'))
                          AND fiscal_mon =
                                 TO_NUMBER (
                                    TO_CHAR (
                                       (CASE b.scenario_status_id
                                           WHEN 3
                                           THEN
                                              NVL (b.dt_last_changed,
                                                   SYSDATE)
                                           ELSE
                                              SYSDATE
                                        END),
                                       'mm'))
                          AND country_id = 40)
                     AS canadian_exchange_rate
             FROM display a, display_scenario b
            WHERE a.display_id = b.display_id AND b.scenario_status_id = 3) c,
          (SELECT exchange_rate
             FROM exchange_rate
            WHERE     fiscal_year = TO_NUMBER (TO_CHAR (SYSDATE, 'yyyy'))
                  AND fiscal_mon = TO_NUMBER (TO_CHAR (SYSDATE, 'mm'))
                  AND country_id = 235) d
    WHERE a.display_id = b.display_id(+) AND a.display_id = c.display_id(+);
CREATE OR REPLACE FORCE VIEW V_DISPLAY_CANADA
(
   DISPLAY_ID,
   SKU,
   DESCRIPTION,
   CUSTOMER_ID,
   STATUS_ID,
   QTY_FORECAST,
   PROMO_PERIOD_ID,
   DISPLAY_SALES_REP_ID,
   DESTINATION,
   STRUCTURE_ID,
   QSI_DOC_NUMBER,
   SKU_MIX_FINAL_FLG,
   MANUFACTURER_ID,
   DISPLAY_SETUP_ID,
   PROMO_YEAR,
   CUSTOMER_DISPLAY#,
   DT_GONOGO,
   RYG_STATUS_FLG,
   PACKOUT_VENDOR_ID,
   ACTUAL_QTY,
   CM_PROJECT_NUM,
   USER_LAST_CHANGED,
   USER_CREATED,
   DT_LAST_CHANGED,
   DT_CREATED,
   COUNTRY_ID,
   COUNTRY_NAME
)
AS
   SELECT A."DISPLAY_ID",
          A."SKU",
          A."DESCRIPTION",
          A."CUSTOMER_ID",
          A."STATUS_ID",
          A."QTY_FORECAST",
          A."PROMO_PERIOD_ID",
          A."DISPLAY_SALES_REP_ID",
          A."DESTINATION",
          A."STRUCTURE_ID",
          A."QSI_DOC_NUMBER",
          A."SKU_MIX_FINAL_FLG",
          A."MANUFACTURER_ID",
          A."DISPLAY_SETUP_ID",
          A."PROMO_YEAR",
          A."CUSTOMER_DISPLAY#",
          A."DT_GONOGO",
          A."RYG_STATUS_FLG",
          A."PACKOUT_VENDOR_ID",
          A."ACTUAL_QTY",
          A."CM_PROJECT_NUM",
          A."USER_LAST_CHANGED",
          A."USER_CREATED",
          A."DT_LAST_CHANGED",
          A."DT_CREATED",
          A."COUNTRY_ID",
          B.Country_Name
     FROM display A, country B
    WHERE A.country_id = B.country_id AND A.country_id = 40;
CREATE OR REPLACE FORCE VIEW V_DMM_DISPLAY_QTRLY_RPTNG_CAN
(
   DISPLAY_ID,
   SKU,
   DESCRIPTION,
   QTY_FORECAST,
   PROMO_YEAR,
   SKU_MIX_FINAL_FLG,
   QSI_DOC_NUMBER,
   CUSTOMER_DISPLAY#,
   SENT_SAMPLE_DATE,
   FINAL_ARTWORK_DATE,
   STRUCTURE_APPROVED_FLG,
   PRICE_AVAILABILITY_DATE,
   PRICING_ADMIN_DATE,
   PIM_COMPLETED_DATE,
   PROJECT_LEVEL_PCT,
   COMMENTS,
   CUSTOMER_NAME,
   STATUS_NAME,
   STRUCTURE_NAME,
   PROMO_PERIOD_NAME,
   DISPLAY_SETUP_DESC,
   PROJECT_REP,
   SPECIAL_LABEL_RQT,
   DISPLAY_LENGTH,
   DISPLAY_WIDTH,
   DISPLAY_HEIGHT,
   DISPLAY_WEIGHT,
   DISPLAY_PER_LAYER,
   LAYERS_PER_PALLET,
   SERIAL_SHIP_FLG,
   PALLETS_PER_TRAILER,
   MANUFACTURER_NAME,
   CREATE_DATE,
   CUSTOMER_TYPE_ID,
   CUSTOMER_PARENT,
   GO_NO_GO_DATE,
   RYG_STATUS,
   DISPLAY_SCENARIO_ID
)
AS
   SELECT d.display_id,
          d.sku,
          d.description,
          d.actual_qty,
          d.promo_year,
          d.sku_mix_final_flg,
          d.qsi_doc_number,
          d.customer_display#,
          cm.sent_sample_date,
          cm.final_artwork_date,
          cm.structure_approved_flg,
          cm.price_availability_date,
          cm.pricing_admin_date,
          cm.pim_completed_date,
          cm.project_level_pct,
          cm.comments,
          c.customer_name,
          s.status_name,
          str.structure_name,
          pp.promo_period_name,
          ds.display_setup_desc,
          dsr.first_name || ' ' || dsr.last_name,
          dl.special_label_rqt,
          dl.display_length,
          dl.display_width,
          dl.display_height,
          dl.display_weight,
          dl.display_per_layer,
          dl.layers_per_pallet,
          dl.serial_ship_flg,
          dl.pallets_per_trailer,
          m.NAME,
          d.dt_created,
          c.customer_type_id,
          c.customer_parent,
          d.dt_gonogo,
          d.ryg_status_flg ryg_status,
          dsp_scnr.display_scenario_id
     FROM display d,
          customer_marketing cm,
          (SELECT A.*, B.customer_name customer_parent
             FROM customer A, customer B
            WHERE A.parent_id = B.customer_id(+)) c,
          status s,
          STRUCTURE str,
          promo_period pp,
          display_setup ds,
          (SELECT *
             FROM display_scenario
            WHERE scenario_approval_flg = 'Y') dsp_scnr,
          display_sales_rep dsr,
          display_logistics dl,
          manufacturer m,
          V_DISPLAY_canada dsp_can
    WHERE     d.display_id = cm.display_id(+)
          AND d.customer_id = c.customer_id(+)
          AND d.status_id = s.status_id(+)
          AND d.structure_id = str.structure_id(+)
          AND d.promo_period_id = pp.promo_period_id(+)
          AND d.display_setup_id = ds.display_setup_id(+)
          AND d.display_id = dsp_scnr.display_id(+)
          AND d.display_sales_rep_id = dsr.display_sales_rep_id(+)
          AND d.display_id = dl.display_id(+)
          AND d.manufacturer_id = m.manufacturer_id(+)
          AND d.display_id = dsp_can.display_id;
CREATE OR REPLACE FORCE VIEW V_DISPLAY_LOCATION_CANADA
(
   LOCATION_ID,
   DC_CODE,
   DC_NAME,
   ACTIVE_FLG
)
AS
   (SELECT DISTINCT A."LOCATION_ID",
                    A."DC_CODE",
                    A."DC_NAME",
                    A."ACTIVE_FLG"
      FROM location A, display_dc B, V_DISPLAY_CANADA C
     WHERE A.location_id = B.location_ID AND B.display_id = C.display_id);
CREATE OR REPLACE FORCE VIEW V_DMM_DISPLAY_CANADA
(
   DISPLAY_ID,
   SKU,
   DESCRIPTION,
   QTY_FORECAST,
   PROMO_YEAR,
   SKU_MIX_FINAL_FLG,
   QSI_DOC_NUMBER,
   CUSTOMER_DISPLAY#,
   SENT_SAMPLE_DATE,
   FINAL_ARTWORK_DATE,
   STRUCTURE_APPROVED_FLG,
   PRICE_AVAILABILITY_DATE,
   PRICING_ADMIN_DATE,
   PIM_COMPLETED_DATE,
   PROJECT_LEVEL_PCT,
   COMMENTS,
   CUSTOMER_NAME,
   STATUS_NAME,
   STRUCTURE_NAME,
   PROMO_PERIOD_NAME,
   DISPLAY_SETUP_DESC,
   COST_CENTER,
   CM_PROJECT_NUM,
   SSCID_PROJECT_NUM,
   PROJECT_REP,
   SPECIAL_LABEL_RQT,
   DISPLAY_LENGTH,
   DISPLAY_WIDTH,
   DISPLAY_HEIGHT,
   DISPLAY_WEIGHT,
   DISPLAY_PER_LAYER,
   LAYERS_PER_PALLET,
   SERIAL_SHIP_FLG,
   PALLETS_PER_TRAILER,
   MANUFACTURER_NAME,
   CREATE_DATE,
   CUSTOMER_TYPE_ID,
   CUSTOMER_PARENT,
   GO_NO_GO_DATE,
   RYG_STATUS,
   COUNTRY_NAME
)
AS
   SELECT d.display_id,
          d.sku,
          d.description,
          d.actual_qty,
          d.promo_year,
          d.sku_mix_final_flg,
          d.qsi_doc_number,
          d.customer_display#,
          cm.sent_sample_date,
          cm.final_artwork_date,
          cm.structure_approved_flg,
          cm.price_availability_date,
          cm.pricing_admin_date,
          cm.pim_completed_date,
          cm.project_level_pct,
          cm.comments,
          c.customer_name,
          s.status_name,
          str.structure_name,
          pp.promo_period_name,
          ds.display_setup_desc,
          dc.cost_center,
          dc.cm_project_num,
          dc.sscid_project_num,
          dsr.first_name || ' ' || dsr.last_name,
          dl.special_label_rqt,
          dl.display_length,
          dl.display_width,
          dl.display_height,
          dl.display_weight,
          dl.display_per_layer,
          dl.layers_per_pallet,
          dl.serial_ship_flg,
          dl.pallets_per_trailer,
          m.NAME,
          d.dt_created,
          c.customer_type_id,
          c.customer_parent,
          d.dt_gonogo,
          d.ryg_status_flg ryg_status,
          dsp_usa.country_name
     FROM display d,
          customer_marketing cm,
          (SELECT A.*, B.customer_name customer_parent
             FROM customer A, customer B
            WHERE A.parent_id = B.customer_id(+)) c,
          status s,
          STRUCTURE str,
          promo_period pp,
          display_setup ds,
          display_cost dc,
          display_sales_rep dsr,
          display_logistics dl,
          manufacturer m,
          V_DISPLAY_CANADA dsp_usa
    WHERE     d.display_id = cm.display_id(+)
          AND d.customer_id = c.customer_id(+)
          AND d.status_id = s.status_id(+)
          AND d.structure_id = str.structure_id(+)
          AND d.promo_period_id = pp.promo_period_id(+)
          AND d.display_setup_id = ds.display_setup_id(+)
          AND d.display_id = dc.display_id(+)
          AND d.display_sales_rep_id = dsr.display_sales_rep_id(+)
          AND d.display_id = dl.display_id(+)
          AND d.manufacturer_id = m.manufacturer_id(+)
          AND d.display_id = dsp_usa.display_id;
CREATE OR REPLACE FORCE VIEW V_CALCULATION_SMY_CAN_USA_CONV
(
   DISPLAY_ID,
   SUM_TOT_ACTUAL_INVOICE,
   SUM_CORRUGATE_COST,
   SUM_FULFILLMENT_COST,
   SUM_OTHER_COST,
   SUM_TOT_DISP_COST_NOSHIPPING,
   SUM_COGS,
   SUM_VM_PCT_BP,
   SUM_NET_SALES_BP,
   HURDLES,
   SUM_TRADE_SALES,
   VM_PCT_HURDLE_RED,
   VM_PCT_HURDLE_GREEN,
   VM_DOLLAR_HURDLE_RED,
   VM_DOLLAR_HURDLE_GREEN
)
AS
   SELECT A.DISPLAY_ID,
          A.SUM_TOT_ACTUAL_INVOICE / USD_EXCHANGE_RATE SUM_TOT_ACTUAL_INVOICE,
          A.SUM_CORRUGATE_COST / USD_EXCHANGE_RATE SUM_CORRUGATE_COST,
          A.SUM_FULFILLMENT_COST / USD_EXCHANGE_RATE SUM_FULFILLMENT_COST,
          A.SUM_OTHER_COST / USD_EXCHANGE_RATE SUM_OTHER_COST,
          A.SUM_TOT_DISP_COST_NOSHIPPING / USD_EXCHANGE_RATE
             SUM_TOT_DISP_COST_NOSHIPPING,
          A.SUM_COGS / USD_EXCHANGE_RATE SUM_COGS,
          A.SUM_VM_PCT_BP SUM_VM_PCT_BP,
          A.SUM_NET_SALES_BP / USD_EXCHANGE_RATE SUM_NET_SALES_BP,
          A.HURDLES HURDLES,
          A.SUM_TRADE_SALES / USD_EXCHANGE_RATE SUM_TRADE_SALES,
          A.VM_PCT_HURDLE_RED,
          A.VM_PCT_HURDLE_GREEN,
          A.VM_DOLLAR_HURDLE_RED / USD_EXCHANGE_RATE VM_DOLLAR_HURDLE_RED,
          A.VM_DOLLAR_HURDLE_GREEN / USD_EXCHANGE_RATE VM_DOLLAR_HURDLE_GREEN
     FROM CALCULATION_SUMMARY A,
          V_DISPLAY_CANADA B,
          V_DMM_DISPLAY_EXCHANGE_RATE C
    WHERE A.DISPLAY_ID = B.DISPLAY_ID AND A.DISPLAY_ID = C.DISPLAY_ID;
CREATE OR REPLACE FORCE VIEW V_PROMO_PERIOD_CANADA
(
   PROMO_PERIOD_ID,
   PROMO_PERIOD_NAME,
   PROMO_PERIOD_DESCRIPTION,
   PROMO_DATE,
   ACTIVE_FLG
)
AS
   (SELECT DISTINCT A."PROMO_PERIOD_ID",
                    A."PROMO_PERIOD_NAME",
                    A."PROMO_PERIOD_DESCRIPTION",
                    A."PROMO_DATE",
                    A."ACTIVE_FLG"
      FROM promo_period A, V_display_canada B
     WHERE A.promo_period_id = B.promo_period_id);
CREATE OR REPLACE FORCE VIEW V_USER_CHANGE_LOG_REPORT_CAN
(
   USER_CHANGE_LOG_ID,
   USERNAME,
   DT_CHANGED,
   TABLE_NAME,
   FIELD,
   FRIENDLY_NAME,
   OLD_VALUE,
   NEW_VALUE,
   DISPLAY_ID,
   CHANGE_TYPE,
   PRIMARY_KEY_ID,
   COMPONENT_SKU,
   TOP_LEVEL_SKU,
   DT_CHANGED_DATE
)
AS
   (SELECT USER_CHANGE_LOG_ID,
           USERNAME,
           DT_CHANGED,
           TABLE_NAME,
           FIELD,
           FRIENDLY_NAME,
           --OLD_VALUE,
           CASE
              WHEN LOWER (A.FIELD) = 'status_id'
              THEN
                 (SELECT B.STATUS_NAME
                    FROM STATUS B
                   WHERE A.OLD_VALUE = B.status_id(+))
              WHEN LOWER (A.FIELD) = 'display_sales_rep_id'
              THEN
                 (SELECT dsr.first_name || ' ' || dsr.last_name
                    FROM display_sales_rep DSR
                   WHERE A.OLD_VALUE = DSR.DISPLAY_SALES_REP_ID(+))
              WHEN LOWER (A.FIELD) = 'manufacturer_id'
              THEN
                 (SELECT MFG.NAME
                    FROM MANUFACTURER MFG
                   WHERE A.OLD_VALUE = MFG.MANUFACTURER_ID(+))
              WHEN LOWER (A.FIELD) = 'location_id'
              THEN
                 (SELECT LOC.DC_CODE
                    FROM LOCATION LOC
                   WHERE A.OLD_VALUE = LOC.LOCATION_ID(+))
              WHEN LOWER (A.FIELD) = 'customer_id'
              THEN
                 (SELECT CUST.CUSTOMER_NAME
                    FROM CUSTOMER CUST
                   WHERE A.OLD_VALUE = CUST.CUSTOMER_ID(+))
              WHEN LOWER (A.FIELD) = 'promo_period_id'
              THEN
                 (SELECT PPD.PROMO_PERIOD_NAME
                    FROM PROMO_PERIOD PPD
                   WHERE A.OLD_VALUE = PPD.PROMO_PERIOD_ID(+))
              WHEN LOWER (A.FIELD) = 'display_setup_id'
              THEN
                 (SELECT DSTP.DISPLAY_SETUP_DESC
                    FROM DISPLAY_SETUP DSTP
                   WHERE A.OLD_VALUE = DSTP.DISPLAY_SETUP_ID(+))
              WHEN LOWER (A.FIELD) = 'structure_id'
              THEN
                 (SELECT STRUCT.STRUCTURE_NAME
                    FROM STRUCTURE STRUCT
                   WHERE A.OLD_VALUE = STRUCT.STRUCTURE_ID(+))
              WHEN LOWER (A.FIELD) = 'display_dc_id'
              THEN
                 (SELECT LOCN.DC_CODE
                    FROM display_dc DDC, LOCATION LOCN
                   WHERE     DDC.LOCATION_ID = LOCN.LOCATION_ID
                         AND A.OLD_VALUE = DDC.DISPLAY_DC_ID)
              ELSE
                 OLD_VALUE
           END
              OLD_VALUE,
           --NEW_VALUE,
           CASE
              WHEN LOWER (A.FIELD) = 'status_id'
              THEN
                 (SELECT B.STATUS_NAME
                    FROM STATUS B
                   WHERE A.NEW_VALUE = B.status_id(+))
              WHEN LOWER (A.FIELD) = 'display_sales_rep_id'
              THEN
                 (SELECT dsr.first_name || ' ' || dsr.last_name
                    FROM display_sales_rep DSR
                   WHERE A.NEW_VALUE = DSR.DISPLAY_SALES_REP_ID(+))
              WHEN LOWER (A.FIELD) = 'manufacturer_id'
              THEN
                 (SELECT MFG.NAME
                    FROM MANUFACTURER MFG
                   WHERE A.NEW_VALUE = MFG.MANUFACTURER_ID(+))
              WHEN LOWER (A.FIELD) = 'location_id'
              THEN
                 (SELECT LOC.DC_CODE
                    FROM LOCATION LOC
                   WHERE A.NEW_VALUE = LOC.LOCATION_ID(+))
              WHEN LOWER (A.FIELD) = 'customer_id'
              THEN
                 (SELECT CUST.CUSTOMER_NAME
                    FROM CUSTOMER CUST
                   WHERE A.NEW_VALUE = CUST.CUSTOMER_ID(+))
              WHEN LOWER (A.FIELD) = 'promo_period_id'
              THEN
                 (SELECT PPD.PROMO_PERIOD_NAME
                    FROM PROMO_PERIOD PPD
                   WHERE A.NEW_VALUE = PPD.PROMO_PERIOD_ID(+))
              WHEN LOWER (A.FIELD) = 'display_setup_id'
              THEN
                 (SELECT DSTP.DISPLAY_SETUP_DESC
                    FROM DISPLAY_SETUP DSTP
                   WHERE A.NEW_VALUE = DSTP.DISPLAY_SETUP_ID(+))
              WHEN LOWER (A.FIELD) = 'structure_id'
              THEN
                 (SELECT STRUCT.STRUCTURE_NAME
                    FROM STRUCTURE STRUCT
                   WHERE A.NEW_VALUE = STRUCT.STRUCTURE_ID(+))
              WHEN LOWER (A.FIELD) = 'display_dc_id'
              THEN
                 (SELECT LOCN.DC_CODE
                    FROM display_dc DDC, LOCATION LOCN
                   WHERE     DDC.LOCATION_ID = LOCN.LOCATION_ID
                         AND A.NEW_VALUE = DDC.DISPLAY_DC_ID)
              ELSE
                 NEW_VALUE
           END
              NEW_VALUE,
           A.DISPLAY_ID,
           CHANGE_TYPE,
           PRIMARY_KEY_ID,
           COMPONENT_SKU,
           TOP_LEVEL_SKU,
           DT_CHANGED_DATE
      FROM V_USER_CHANGE_LOG A, V_DISPLAY_CANADA B
     WHERE     LOWER (A.FIELD) IN
                  ('sku',
                   'sku_mix_final_flg',
                   'qty_forecast',
                   'location_id',
                   'status_id',
                   'manufacturer_id',
                   'comments',
                   'customer_id',
                   'kit_version_code',
                   'i2_forecast_flg',
                   'qty_per_facing',
                   'number_facings',
                   'pbc_version',
                   'plant_code',
                   'planner_code',
                   'inventory requirement',
                   'ship_from_date',
                   'product_due_date',
                   'bu_desc')
           AND A.DISPLAY_ID = B.DISPLAY_ID);
CREATE OR REPLACE FORCE VIEW V_DISPLAY_USA
(
   DISPLAY_ID,
   SKU,
   DESCRIPTION,
   CUSTOMER_ID,
   STATUS_ID,
   QTY_FORECAST,
   PROMO_PERIOD_ID,
   DISPLAY_SALES_REP_ID,
   DESTINATION,
   STRUCTURE_ID,
   QSI_DOC_NUMBER,
   SKU_MIX_FINAL_FLG,
   MANUFACTURER_ID,
   DISPLAY_SETUP_ID,
   PROMO_YEAR,
   CUSTOMER_DISPLAY#,
   DT_GONOGO,
   RYG_STATUS_FLG,
   PACKOUT_VENDOR_ID,
   ACTUAL_QTY,
   CM_PROJECT_NUM,
   USER_LAST_CHANGED,
   USER_CREATED,
   DT_LAST_CHANGED,
   DT_CREATED,
   COUNTRY_ID,
   COUNTRY_NAME
)
AS
   SELECT A."DISPLAY_ID",
          A."SKU",
          A."DESCRIPTION",
          A."CUSTOMER_ID",
          A."STATUS_ID",
          A."QTY_FORECAST",
          A."PROMO_PERIOD_ID",
          A."DISPLAY_SALES_REP_ID",
          A."DESTINATION",
          A."STRUCTURE_ID",
          A."QSI_DOC_NUMBER",
          A."SKU_MIX_FINAL_FLG",
          A."MANUFACTURER_ID",
          A."DISPLAY_SETUP_ID",
          A."PROMO_YEAR",
          A."CUSTOMER_DISPLAY#",
          A."DT_GONOGO",
          A."RYG_STATUS_FLG",
          A."PACKOUT_VENDOR_ID",
          A."ACTUAL_QTY",
          A."CM_PROJECT_NUM",
          A."USER_LAST_CHANGED",
          A."USER_CREATED",
          A."DT_LAST_CHANGED",
          A."DT_CREATED",
          A."COUNTRY_ID",
          B.Country_Name
     FROM display A, country B
    WHERE A.country_id = B.country_id AND A.country_id = 235;
CREATE OR REPLACE FORCE VIEW V_DMM_DISPLAY_QTRLY_RPTNG_USA
(
   DISPLAY_ID,
   SKU,
   DESCRIPTION,
   QTY_FORECAST,
   PROMO_YEAR,
   SKU_MIX_FINAL_FLG,
   QSI_DOC_NUMBER,
   CUSTOMER_DISPLAY#,
   SENT_SAMPLE_DATE,
   FINAL_ARTWORK_DATE,
   STRUCTURE_APPROVED_FLG,
   PRICE_AVAILABILITY_DATE,
   PRICING_ADMIN_DATE,
   PIM_COMPLETED_DATE,
   PROJECT_LEVEL_PCT,
   COMMENTS,
   CUSTOMER_NAME,
   STATUS_NAME,
   STRUCTURE_NAME,
   PROMO_PERIOD_NAME,
   DISPLAY_SETUP_DESC,
   PROJECT_REP,
   SPECIAL_LABEL_RQT,
   DISPLAY_LENGTH,
   DISPLAY_WIDTH,
   DISPLAY_HEIGHT,
   DISPLAY_WEIGHT,
   DISPLAY_PER_LAYER,
   LAYERS_PER_PALLET,
   SERIAL_SHIP_FLG,
   PALLETS_PER_TRAILER,
   MANUFACTURER_NAME,
   CREATE_DATE,
   CUSTOMER_TYPE_ID,
   CUSTOMER_PARENT,
   GO_NO_GO_DATE,
   RYG_STATUS,
   DISPLAY_SCENARIO_ID
)
AS
   SELECT d.display_id,
          d.sku,
          d.description,
          d.actual_qty,
          d.promo_year,
          d.sku_mix_final_flg,
          d.qsi_doc_number,
          d.customer_display#,
          cm.sent_sample_date,
          cm.final_artwork_date,
          cm.structure_approved_flg,
          cm.price_availability_date,
          cm.pricing_admin_date,
          cm.pim_completed_date,
          cm.project_level_pct,
          cm.comments,
          c.customer_name,
          s.status_name,
          str.structure_name,
          pp.promo_period_name,
          ds.display_setup_desc,
          dsr.first_name || ' ' || dsr.last_name,
          dl.special_label_rqt,
          dl.display_length,
          dl.display_width,
          dl.display_height,
          dl.display_weight,
          dl.display_per_layer,
          dl.layers_per_pallet,
          dl.serial_ship_flg,
          dl.pallets_per_trailer,
          m.NAME,
          d.dt_created,
          c.customer_type_id,
          c.customer_parent,
          d.dt_gonogo,
          d.ryg_status_flg ryg_status,
          dsp_scnr.display_scenario_id
     FROM display d,
          customer_marketing cm,
          (SELECT A.*, B.customer_name customer_parent
             FROM customer A, customer B
            WHERE A.parent_id = B.customer_id(+)) c,
          status s,
          STRUCTURE str,
          promo_period pp,
          display_setup ds,
          (SELECT *
             FROM display_scenario
            WHERE scenario_approval_flg = 'Y') dsp_scnr,
          display_sales_rep dsr,
          display_logistics dl,
          manufacturer m,
          V_DISPLAY_USA dsp_usa
    WHERE     d.display_id = cm.display_id(+)
          AND d.customer_id = c.customer_id(+)
          AND d.status_id = s.status_id(+)
          AND d.structure_id = str.structure_id(+)
          AND d.promo_period_id = pp.promo_period_id(+)
          AND d.display_setup_id = ds.display_setup_id(+)
          AND d.display_id = dsp_scnr.display_id(+)
          AND d.display_sales_rep_id = dsr.display_sales_rep_id(+)
          AND d.display_id = dl.display_id(+)
          AND d.manufacturer_id = m.manufacturer_id(+)
          AND d.display_id = dsp_usa.display_id;
CREATE OR REPLACE FORCE VIEW V_DMM_DISPLAY_USA
(
   DISPLAY_ID,
   SKU,
   DESCRIPTION,
   QTY_FORECAST,
   PROMO_YEAR,
   SKU_MIX_FINAL_FLG,
   QSI_DOC_NUMBER,
   CUSTOMER_DISPLAY#,
   SENT_SAMPLE_DATE,
   FINAL_ARTWORK_DATE,
   STRUCTURE_APPROVED_FLG,
   PRICE_AVAILABILITY_DATE,
   PRICING_ADMIN_DATE,
   PIM_COMPLETED_DATE,
   PROJECT_LEVEL_PCT,
   COMMENTS,
   CUSTOMER_NAME,
   STATUS_NAME,
   STRUCTURE_NAME,
   PROMO_PERIOD_NAME,
   DISPLAY_SETUP_DESC,
   COST_CENTER,
   CM_PROJECT_NUM,
   SSCID_PROJECT_NUM,
   PROJECT_REP,
   SPECIAL_LABEL_RQT,
   DISPLAY_LENGTH,
   DISPLAY_WIDTH,
   DISPLAY_HEIGHT,
   DISPLAY_WEIGHT,
   DISPLAY_PER_LAYER,
   LAYERS_PER_PALLET,
   SERIAL_SHIP_FLG,
   PALLETS_PER_TRAILER,
   MANUFACTURER_NAME,
   CREATE_DATE,
   CUSTOMER_TYPE_ID,
   CUSTOMER_PARENT,
   GO_NO_GO_DATE,
   RYG_STATUS,
   COUNTRY_NAME
)
AS
   SELECT d.display_id,
          d.sku,
          d.description,
          d.actual_qty,
          d.promo_year,
          d.sku_mix_final_flg,
          d.qsi_doc_number,
          d.customer_display#,
          cm.sent_sample_date,
          cm.final_artwork_date,
          cm.structure_approved_flg,
          cm.price_availability_date,
          cm.pricing_admin_date,
          cm.pim_completed_date,
          cm.project_level_pct,
          cm.comments,
          c.customer_name,
          s.status_name,
          str.structure_name,
          pp.promo_period_name,
          ds.display_setup_desc,
          dc.cost_center,
          dc.cm_project_num,
          dc.sscid_project_num,
          dsr.first_name || ' ' || dsr.last_name,
          dl.special_label_rqt,
          dl.display_length,
          dl.display_width,
          dl.display_height,
          dl.display_weight,
          dl.display_per_layer,
          dl.layers_per_pallet,
          dl.serial_ship_flg,
          dl.pallets_per_trailer,
          m.NAME,
          d.dt_created,
          c.customer_type_id,
          c.customer_parent,
          d.dt_gonogo,
          d.ryg_status_flg ryg_status,
          dsp_usa.country_name
     FROM display d,
          customer_marketing cm,
          (SELECT A.*, B.customer_name customer_parent
             FROM customer A, customer B
            WHERE A.parent_id = B.customer_id(+)) c,
          status s,
          STRUCTURE str,
          promo_period pp,
          display_setup ds,
          display_cost dc,
          display_sales_rep dsr,
          display_logistics dl,
          manufacturer m,
          V_DISPLAY_USA dsp_usa
    WHERE     d.display_id = cm.display_id(+)
          AND d.customer_id = c.customer_id(+)
          AND d.status_id = s.status_id(+)
          AND d.structure_id = str.structure_id(+)
          AND d.promo_period_id = pp.promo_period_id(+)
          AND d.display_setup_id = ds.display_setup_id(+)
          AND d.display_id = dc.display_id(+)
          AND d.display_sales_rep_id = dsr.display_sales_rep_id(+)
          AND d.display_id = dl.display_id(+)
          AND d.manufacturer_id = m.manufacturer_id(+)
          AND d.display_id = dsp_usa.display_id;
CREATE OR REPLACE FORCE VIEW V_PROMO_PERIOD_USA
(
   PROMO_PERIOD_ID,
   PROMO_PERIOD_NAME,
   PROMO_PERIOD_DESCRIPTION,
   PROMO_DATE,
   ACTIVE_FLG
)
AS
   (SELECT DISTINCT A."PROMO_PERIOD_ID",
                    A."PROMO_PERIOD_NAME",
                    A."PROMO_PERIOD_DESCRIPTION",
                    A."PROMO_DATE",
                    A."ACTIVE_FLG"
      FROM promo_period A, V_display_usa B
     WHERE A.promo_period_id = B.promo_period_id);
CREATE OR REPLACE FORCE VIEW V_USER_CHANGE_LOG_REPORT_USA
(
   USER_CHANGE_LOG_ID,
   USERNAME,
   DT_CHANGED,
   TABLE_NAME,
   FIELD,
   FRIENDLY_NAME,
   OLD_VALUE,
   NEW_VALUE,
   DISPLAY_ID,
   CHANGE_TYPE,
   PRIMARY_KEY_ID,
   COMPONENT_SKU,
   TOP_LEVEL_SKU,
   DT_CHANGED_DATE
)
AS
   (SELECT USER_CHANGE_LOG_ID,
           USERNAME,
           DT_CHANGED,
           TABLE_NAME,
           FIELD,
           FRIENDLY_NAME,
           --OLD_VALUE,
           CASE
              WHEN LOWER (A.FIELD) = 'status_id'
              THEN
                 (SELECT B.STATUS_NAME
                    FROM STATUS B
                   WHERE A.OLD_VALUE = B.status_id(+))
              WHEN LOWER (A.FIELD) = 'display_sales_rep_id'
              THEN
                 (SELECT dsr.first_name || ' ' || dsr.last_name
                    FROM display_sales_rep DSR
                   WHERE A.OLD_VALUE = DSR.DISPLAY_SALES_REP_ID(+))
              WHEN LOWER (A.FIELD) = 'manufacturer_id'
              THEN
                 (SELECT MFG.NAME
                    FROM MANUFACTURER MFG
                   WHERE A.OLD_VALUE = MFG.MANUFACTURER_ID(+))
              WHEN LOWER (A.FIELD) = 'location_id'
              THEN
                 (SELECT LOC.DC_CODE
                    FROM LOCATION LOC
                   WHERE A.OLD_VALUE = LOC.LOCATION_ID(+))
              WHEN LOWER (A.FIELD) = 'customer_id'
              THEN
                 (SELECT CUST.CUSTOMER_NAME
                    FROM CUSTOMER CUST
                   WHERE A.OLD_VALUE = CUST.CUSTOMER_ID(+))
              WHEN LOWER (A.FIELD) = 'promo_period_id'
              THEN
                 (SELECT PPD.PROMO_PERIOD_NAME
                    FROM PROMO_PERIOD PPD
                   WHERE A.OLD_VALUE = PPD.PROMO_PERIOD_ID(+))
              WHEN LOWER (A.FIELD) = 'display_setup_id'
              THEN
                 (SELECT DSTP.DISPLAY_SETUP_DESC
                    FROM DISPLAY_SETUP DSTP
                   WHERE A.OLD_VALUE = DSTP.DISPLAY_SETUP_ID(+))
              WHEN LOWER (A.FIELD) = 'structure_id'
              THEN
                 (SELECT STRUCT.STRUCTURE_NAME
                    FROM STRUCTURE STRUCT
                   WHERE A.OLD_VALUE = STRUCT.STRUCTURE_ID(+))
              WHEN LOWER (A.FIELD) = 'display_dc_id'
              THEN
                 (SELECT LOCN.DC_CODE
                    FROM display_dc DDC, LOCATION LOCN
                   WHERE     DDC.LOCATION_ID = LOCN.LOCATION_ID
                         AND A.OLD_VALUE = DDC.DISPLAY_DC_ID)
              ELSE
                 OLD_VALUE
           END
              OLD_VALUE,
           --NEW_VALUE,
           CASE
              WHEN LOWER (A.FIELD) = 'status_id'
              THEN
                 (SELECT B.STATUS_NAME
                    FROM STATUS B
                   WHERE A.NEW_VALUE = B.status_id(+))
              WHEN LOWER (A.FIELD) = 'display_sales_rep_id'
              THEN
                 (SELECT dsr.first_name || ' ' || dsr.last_name
                    FROM display_sales_rep DSR
                   WHERE A.NEW_VALUE = DSR.DISPLAY_SALES_REP_ID(+))
              WHEN LOWER (A.FIELD) = 'manufacturer_id'
              THEN
                 (SELECT MFG.NAME
                    FROM MANUFACTURER MFG
                   WHERE A.NEW_VALUE = MFG.MANUFACTURER_ID(+))
              WHEN LOWER (A.FIELD) = 'location_id'
              THEN
                 (SELECT LOC.DC_CODE
                    FROM LOCATION LOC
                   WHERE A.NEW_VALUE = LOC.LOCATION_ID(+))
              WHEN LOWER (A.FIELD) = 'customer_id'
              THEN
                 (SELECT CUST.CUSTOMER_NAME
                    FROM CUSTOMER CUST
                   WHERE A.NEW_VALUE = CUST.CUSTOMER_ID(+))
              WHEN LOWER (A.FIELD) = 'promo_period_id'
              THEN
                 (SELECT PPD.PROMO_PERIOD_NAME
                    FROM PROMO_PERIOD PPD
                   WHERE A.NEW_VALUE = PPD.PROMO_PERIOD_ID(+))
              WHEN LOWER (A.FIELD) = 'display_setup_id'
              THEN
                 (SELECT DSTP.DISPLAY_SETUP_DESC
                    FROM DISPLAY_SETUP DSTP
                   WHERE A.NEW_VALUE = DSTP.DISPLAY_SETUP_ID(+))
              WHEN LOWER (A.FIELD) = 'structure_id'
              THEN
                 (SELECT STRUCT.STRUCTURE_NAME
                    FROM STRUCTURE STRUCT
                   WHERE A.NEW_VALUE = STRUCT.STRUCTURE_ID(+))
              WHEN LOWER (A.FIELD) = 'display_dc_id'
              THEN
                 (SELECT LOCN.DC_CODE
                    FROM display_dc DDC, LOCATION LOCN
                   WHERE     DDC.LOCATION_ID = LOCN.LOCATION_ID
                         AND A.NEW_VALUE = DDC.DISPLAY_DC_ID)
              ELSE
                 NEW_VALUE
           END
              NEW_VALUE,
           A.DISPLAY_ID,
           CHANGE_TYPE,
           PRIMARY_KEY_ID,
           COMPONENT_SKU,
           TOP_LEVEL_SKU,
           DT_CHANGED_DATE
      FROM V_USER_CHANGE_LOG A, V_DISPLAY_USA B
     WHERE     LOWER (A.FIELD) IN
                  ('sku',
                   'sku_mix_final_flg',
                   'qty_forecast',
                   'location_id',
                   'status_id',
                   'manufacturer_id',
                   'comments',
                   'customer_id',
                   'kit_version_code',
                   'i2_forecast_flg',
                   'qty_per_facing',
                   'number_facings',
                   'pbc_version',
                   'plant_code',
                   'planner_code',
                   'inventory requirement',
                   'ship_from_date',
                   'product_due_date',
                   'bu_desc')
           AND A.DISPLAY_ID = B.DISPLAY_ID);
CREATE OR REPLACE FORCE VIEW V_CUSTOMER_CANADA
(
   CUSTOMER_ID,
   CUSTOMER_NAME,
   CUSTOMER_DESCRIPTION,
   CUSTOMER_TYPE_ID,
   PARENT_ID,
   ACTIVE_FLG,
   COMPANY_CODE,
   CUSTOMER_CODE
)
AS
   (SELECT DISTINCT A."CUSTOMER_ID",
                    A."CUSTOMER_NAME",
                    A."CUSTOMER_DESCRIPTION",
                    A."CUSTOMER_TYPE_ID",
                    A."PARENT_ID",
                    A."ACTIVE_FLG",
                    A."COMPANY_CODE",
                    A."CUSTOMER_CODE"
      FROM CUSTOMER A, V_display_canada B
     WHERE A.CUSTOMER_ID = B.CUSTOMER_ID);
CREATE OR REPLACE FORCE VIEW V_DISP_QTRLY_MNGMNT_RPTNG_CAN
(
   DISPLAY_ID,
   TL_STATUS,
   TL_CUSTOMER,
   TL_PROMO_YEAR,
   TL_PROMO_PERIOD,
   TL_SKU,
   TL_DESCRIPTION,
   TL_STRUCTURE,
   CP_SKU,
   CP_DESCRIPTION,
   CP_QTY_PER_FACING,
   CP_#_FACINGS,
   CP_QTY_PER_DISPLAY,
   TL_QTY,
   SALES_CASE,
   CORRUGATE_COST,
   FULFILLMENT_COST,
   OTHER_COST,
   TTL_COSTS_WO_SHIP,
   COGS,
   VM_PCT_BEFORE_PROGRAM,
   NET_SALES_BEFORE_PROGRAM,
   HURDLES,
   TTL_SALES,
   VM_DOLLAR_HURDLE,
   CHAR_VM_DOLLAR_HURDLE,
   VM_DOLLAR_HURDLE_COLOR,
   VM_PCT_HURDLE,
   CHAR_VM_PCT_HURDLE,
   VM_PCT_HURDLE_COLOR
)
AS
   SELECT A.DISPLAY_ID DISPLAY_ID,
          A.STATUS_NAME TL_STATUS,
          A.CUSTOMER_NAME TL_CUSTOMER,
          A.PROMO_YEAR TL_PROMO_YEAR,
          A.PROMO_PERIOD_NAME TL_PROMO_PERIOD,
          A.SKU TL_SKU,
          A.DESCRIPTION TL_DESCRIPTION,
          A.STRUCTURE_NAME TL_STRUCTURE,
          B.SKU CP_SKU,
          B.PRODUCT_NAME CP_DESCRIPTION,
          B.QTY_PER_FACING CP_QTY_PER_FACING,
          B.NUMBER_FACINGS CP_#_FACINGS,
          B.QTY_PER_FACING * B.NUMBER_FACINGS CP_QTY_PER_DISPLAY,
          D.ACTUAL_QTY TL_QTY,
          C.SUM_TOT_ACTUAL_INVOICE SALES_CASE,
          C.SUM_CORRUGATE_COST CORRUGATE_COST,
          C.SUM_FULFILLMENT_COST FULFILLMENT_COST,
          C.SUM_OTHER_COST OTHER_COST,
          C.SUM_TOT_DISP_COST_NOSHIPPING TTL_COSTS_WO_SHIP,
          C.SUM_COGS COGS,
          C.SUM_VM_PCT_BP VM_PCT_BEFORE_PROGRAM,
          C.SUM_NET_SALES_BP NET_SALES_BEFORE_PROGRAM,
          C.HURDLES HURDLES,
          C.SUM_TRADE_SALES TTL_SALES,
          NVL (C.VM_DOLLAR_HURDLE_RED, C.VM_DOLLAR_HURDLE_GREEN)
             VM_DOLLAR_HURDLE,
          CASE
             WHEN C.VM_DOLLAR_HURDLE_RED IS NULL
             THEN
                TRIM (TO_CHAR (C.VM_DOLLAR_HURDLE_GREEN))
             ELSE
                ' ' || TRIM (TO_CHAR (C.VM_DOLLAR_HURDLE_RED))
          END,
          CASE
             WHEN C.VM_DOLLAR_HURDLE_RED IS NULL THEN 'GREEN'
             ELSE 'RED'
          END,
          NVL (C.VM_PCT_HURDLE_RED, C.VM_PCT_HURDLE_GREEN) VM_PCT_HURDLE,
          CASE
             WHEN C.VM_PCT_HURDLE_RED IS NULL
             THEN
                DECODE (
                   TRIM (TO_CHAR (C.VM_PCT_HURDLE_GREEN, '9999.99')),
                   NULL, NULL,
                   TRIM (TO_CHAR (C.VM_PCT_HURDLE_GREEN, '9999.99')) || '%')
             ELSE
                ' ' || TRIM (TO_CHAR (C.VM_PCT_HURDLE_RED, '9999.99')) || '%'
          END,
          --CASE WHEN C.VM_PCT_HURDLE_RED IS NULL THEN TRIM(TO_CHAR(C.VM_PCT_HURDLE_GREEN,'9999.99'))||'%' ELSE ' '||TRIM(TO_CHAR(C.VM_PCT_HURDLE_RED,'9999.99'))||'%' END,
          CASE WHEN C.VM_PCT_HURDLE_RED IS NULL THEN 'GREEN' ELSE 'RED' END
     FROM v_dmm_display_qtrly_rptng_can A,
          KIT_COMPONENT B,
          CALCULATION_SUMMARY C,
          (SELECT DISPLAY_ID, ACTUAL_QTY
             FROM DISPLAY_SCENARIO
            WHERE scenario_approval_flg = 'Y') D
    WHERE     A.DISPLAY_ID = B.DISPLAY_ID(+)
          AND A.DISPLAY_ID = C.DISPLAY_ID(+)
          AND A.DISPLAY_ID = D.DISPLAY_ID(+)
          AND A.DISPLAY_SCENARIO_ID = B.DISPLAY_SCENARIO_ID(+);
CREATE OR REPLACE FORCE VIEW V_DISPLAY_MANUFACTURER_CANADA
(
   MANUFACTURER_ID,
   NAME
)
AS
   SELECT DISTINCT manufacturer_id, name
     FROM Manufacturer A, v_dmm_display_canada B
    WHERE A.name = B.manufacturer_name
   ORDER BY name;
CREATE OR REPLACE FORCE VIEW V_DSP_QTRLY_MGMT_RPTG_CAN_CONV
(
   DISPLAY_ID,
   TL_STATUS,
   TL_CUSTOMER,
   TL_PROMO_YEAR,
   TL_PROMO_PERIOD,
   TL_SKU,
   TL_DESCRIPTION,
   TL_STRUCTURE,
   CP_SKU,
   CP_DESCRIPTION,
   CP_QTY_PER_FACING,
   CP_#_FACINGS,
   CP_QTY_PER_DISPLAY,
   TL_QTY,
   SALES_CASE,
   CORRUGATE_COST,
   FULFILLMENT_COST,
   OTHER_COST,
   TTL_COSTS_WO_SHIP,
   COGS,
   VM_PCT_BEFORE_PROGRAM,
   NET_SALES_BEFORE_PROGRAM,
   HURDLES,
   TTL_SALES,
   VM_DOLLAR_HURDLE,
   CHAR_VM_DOLLAR_HURDLE,
   VM_DOLLAR_HURDLE_COLOR,
   VM_PCT_HURDLE,
   CHAR_VM_PCT_HURDLE,
   VM_PCT_HURDLE_COLOR
)
AS
   SELECT A.DISPLAY_ID DISPLAY_ID,
          A.STATUS_NAME TL_STATUS,
          A.CUSTOMER_NAME TL_CUSTOMER,
          A.PROMO_YEAR TL_PROMO_YEAR,
          A.PROMO_PERIOD_NAME TL_PROMO_PERIOD,
          A.SKU TL_SKU,
          A.DESCRIPTION TL_DESCRIPTION,
          A.STRUCTURE_NAME TL_STRUCTURE,
          B.SKU CP_SKU,
          B.PRODUCT_NAME CP_DESCRIPTION,
          B.QTY_PER_FACING CP_QTY_PER_FACING,
          B.NUMBER_FACINGS CP_#_FACINGS,
          B.QTY_PER_FACING * B.NUMBER_FACINGS CP_QTY_PER_DISPLAY,
          D.ACTUAL_QTY TL_QTY,
          C.SUM_TOT_ACTUAL_INVOICE SALES_CASE,
          C.SUM_CORRUGATE_COST CORRUGATE_COST,
          C.SUM_FULFILLMENT_COST FULFILLMENT_COST,
          C.SUM_OTHER_COST OTHER_COST,
          C.SUM_TOT_DISP_COST_NOSHIPPING TTL_COSTS_WO_SHIP,
          C.SUM_COGS COGS,
          C.SUM_VM_PCT_BP VM_PCT_BEFORE_PROGRAM,
          C.SUM_NET_SALES_BP NET_SALES_BEFORE_PROGRAM,
          C.HURDLES HURDLES,
          C.SUM_TRADE_SALES TTL_SALES,
          NVL (C.VM_DOLLAR_HURDLE_RED, C.VM_DOLLAR_HURDLE_GREEN)
             VM_DOLLAR_HURDLE,
          CASE
             WHEN C.VM_DOLLAR_HURDLE_RED IS NULL
             THEN
                TRIM (TO_CHAR (C.VM_DOLLAR_HURDLE_GREEN, '99999999.99'))
             ELSE
                ' ' || TRIM (TO_CHAR (C.VM_DOLLAR_HURDLE_RED, '99999999.99'))
          END,
          CASE
             WHEN C.VM_DOLLAR_HURDLE_RED IS NULL THEN 'GREEN'
             ELSE 'RED'
          END,
          NVL (C.VM_PCT_HURDLE_RED, C.VM_PCT_HURDLE_GREEN) VM_PCT_HURDLE,
          CASE
             WHEN C.VM_PCT_HURDLE_RED IS NULL
             THEN
                DECODE (
                   TRIM (TO_CHAR (C.VM_PCT_HURDLE_GREEN, '9999.99')),
                   NULL, NULL,
                   TRIM (TO_CHAR (C.VM_PCT_HURDLE_GREEN, '9999.99')) || '%')
             ELSE
                ' ' || TRIM (TO_CHAR (C.VM_PCT_HURDLE_RED, '9999.99')) || '%'
          END,
          --CASE WHEN C.VM_PCT_HURDLE_RED IS NULL THEN TRIM(TO_CHAR(C.VM_PCT_HURDLE_GREEN,'9999.99'))||'%' ELSE ' '||TRIM(TO_CHAR(C.VM_PCT_HURDLE_RED,'9999.99'))||'%' END,
          CASE WHEN C.VM_PCT_HURDLE_RED IS NULL THEN 'GREEN' ELSE 'RED' END
     FROM v_dmm_display_qtrly_rptng_can A,
          KIT_COMPONENT B,
          V_CALCULATION_SMY_CAN_USA_CONV C,
          (SELECT DISPLAY_ID, ACTUAL_QTY
             FROM DISPLAY_SCENARIO
            WHERE scenario_approval_flg = 'Y') D
    WHERE     A.DISPLAY_ID = B.DISPLAY_ID(+)
          AND A.DISPLAY_ID = C.DISPLAY_ID(+)
          AND A.DISPLAY_ID = D.DISPLAY_ID(+)
          AND A.DISPLAY_SCENARIO_ID = B.DISPLAY_SCENARIO_ID(+);
CREATE OR REPLACE FORCE VIEW V_CUSTOMER_USA
(
   CUSTOMER_ID,
   CUSTOMER_NAME,
   CUSTOMER_DESCRIPTION,
   CUSTOMER_TYPE_ID,
   PARENT_ID,
   ACTIVE_FLG,
   COMPANY_CODE,
   CUSTOMER_CODE
)
AS
   (SELECT DISTINCT A."CUSTOMER_ID",
                    A."CUSTOMER_NAME",
                    A."CUSTOMER_DESCRIPTION",
                    A."CUSTOMER_TYPE_ID",
                    A."PARENT_ID",
                    A."ACTIVE_FLG",
                    A."COMPANY_CODE",
                    A."CUSTOMER_CODE"
      FROM CUSTOMER A, V_display_usa B
     WHERE A.CUSTOMER_ID = B.CUSTOMER_ID);
CREATE OR REPLACE FORCE VIEW V_DISP_QTRLY_MNGMNT_RPTNG_USA
(
   DISPLAY_ID,
   TL_STATUS,
   TL_CUSTOMER,
   TL_PROMO_YEAR,
   TL_PROMO_PERIOD,
   TL_SKU,
   TL_DESCRIPTION,
   TL_STRUCTURE,
   CP_SKU,
   CP_DESCRIPTION,
   CP_QTY_PER_FACING,
   CP_#_FACINGS,
   CP_QTY_PER_DISPLAY,
   TL_QTY,
   SALES_CASE,
   CORRUGATE_COST,
   FULFILLMENT_COST,
   OTHER_COST,
   TTL_COSTS_WO_SHIP,
   COGS,
   VM_PCT_BEFORE_PROGRAM,
   NET_SALES_BEFORE_PROGRAM,
   HURDLES,
   TTL_SALES,
   VM_DOLLAR_HURDLE,
   CHAR_VM_DOLLAR_HURDLE,
   VM_DOLLAR_HURDLE_COLOR,
   VM_PCT_HURDLE,
   CHAR_VM_PCT_HURDLE,
   VM_PCT_HURDLE_COLOR
)
AS
   SELECT A.DISPLAY_ID DISPLAY_ID,
          A.STATUS_NAME TL_STATUS,
          A.CUSTOMER_NAME TL_CUSTOMER,
          A.PROMO_YEAR TL_PROMO_YEAR,
          A.PROMO_PERIOD_NAME TL_PROMO_PERIOD,
          A.SKU TL_SKU,
          A.DESCRIPTION TL_DESCRIPTION,
          A.STRUCTURE_NAME TL_STRUCTURE,
          B.SKU CP_SKU,
          B.PRODUCT_NAME CP_DESCRIPTION,
          B.QTY_PER_FACING CP_QTY_PER_FACING,
          B.NUMBER_FACINGS CP_#_FACINGS,
          B.QTY_PER_FACING * B.NUMBER_FACINGS CP_QTY_PER_DISPLAY,
          D.ACTUAL_QTY TL_QTY,
          C.SUM_TOT_ACTUAL_INVOICE SALES_CASE,
          C.SUM_CORRUGATE_COST CORRUGATE_COST,
          C.SUM_FULFILLMENT_COST FULFILLMENT_COST,
          C.SUM_OTHER_COST OTHER_COST,
          C.SUM_TOT_DISP_COST_NOSHIPPING TTL_COSTS_WO_SHIP,
          C.SUM_COGS COGS,
          C.SUM_VM_PCT_BP VM_PCT_BEFORE_PROGRAM,
          C.SUM_NET_SALES_BP NET_SALES_BEFORE_PROGRAM,
          C.HURDLES HURDLES,
          C.SUM_TRADE_SALES TTL_SALES,
          NVL (C.VM_DOLLAR_HURDLE_RED, C.VM_DOLLAR_HURDLE_GREEN)
             VM_DOLLAR_HURDLE,
          CASE
             WHEN C.VM_DOLLAR_HURDLE_RED IS NULL
             THEN
                TRIM (TO_CHAR (C.VM_DOLLAR_HURDLE_GREEN))
             ELSE
                ' ' || TRIM (TO_CHAR (C.VM_DOLLAR_HURDLE_RED))
          END,
          CASE
             WHEN C.VM_DOLLAR_HURDLE_RED IS NULL THEN 'GREEN'
             ELSE 'RED'
          END,
          NVL (C.VM_PCT_HURDLE_RED, C.VM_PCT_HURDLE_GREEN) VM_PCT_HURDLE,
          CASE
             WHEN C.VM_PCT_HURDLE_RED IS NULL
             THEN
                DECODE (
                   TRIM (TO_CHAR (C.VM_PCT_HURDLE_GREEN, '9999.99')),
                   NULL, NULL,
                   TRIM (TO_CHAR (C.VM_PCT_HURDLE_GREEN, '9999.99')) || '%')
             ELSE
                ' ' || TRIM (TO_CHAR (C.VM_PCT_HURDLE_RED, '9999.99')) || '%'
          END,
          --CASE WHEN C.VM_PCT_HURDLE_RED IS NULL THEN TRIM(TO_CHAR(C.VM_PCT_HURDLE_GREEN,'9999.99'))||'%' ELSE ' '||TRIM(TO_CHAR(C.VM_PCT_HURDLE_RED,'9999.99'))||'%' END,
          CASE WHEN C.VM_PCT_HURDLE_RED IS NULL THEN 'GREEN' ELSE 'RED' END
     FROM v_dmm_display_qtrly_rptng_usa A,
          KIT_COMPONENT B,
          CALCULATION_SUMMARY C,
          (SELECT DISPLAY_ID, ACTUAL_QTY
             FROM DISPLAY_SCENARIO
            WHERE scenario_approval_flg = 'Y') D
    WHERE     A.DISPLAY_ID = B.DISPLAY_ID(+)
          AND A.DISPLAY_ID = C.DISPLAY_ID(+)
          AND A.DISPLAY_ID = D.DISPLAY_ID(+)
          AND A.DISPLAY_SCENARIO_ID = B.DISPLAY_SCENARIO_ID(+);
CREATE OR REPLACE FORCE VIEW V_DISPLAY_LOCATION_USA
(
   LOCATION_ID,
   DC_CODE,
   DC_NAME,
   ACTIVE_FLG
)
AS
   (SELECT DISTINCT A."LOCATION_ID",
                    A."DC_CODE",
                    A."DC_NAME",
                    A."ACTIVE_FLG"
      FROM location A, display_dc B, V_DISPLAY_USA C
     WHERE A.location_id = B.location_ID AND B.display_id = C.display_id);
CREATE OR REPLACE FORCE VIEW V_DISPLAY_MANUFACTURER_USA
(
   MANUFACTURER_ID,
   NAME
)
AS
   SELECT DISTINCT manufacturer_id, name
     FROM Manufacturer A, v_dmm_display_usa B
    WHERE A.name = B.manufacturer_name
   ORDER BY name;
CREATE OR REPLACE FORCE VIEW V_DISP_QTRLY_MNGMNT_RPTNG_CONS
(
   DISPLAY_ID,
   TL_STATUS,
   TL_CUSTOMER,
   TL_PROMO_YEAR,
   TL_PROMO_PERIOD,
   TL_SKU,
   TL_DESCRIPTION,
   TL_STRUCTURE,
   CP_SKU,
   CP_DESCRIPTION,
   CP_QTY_PER_FACING,
   CP_#_FACINGS,
   CP_QTY_PER_DISPLAY,
   TL_QTY,
   SALES_CASE,
   CORRUGATE_COST,
   FULFILLMENT_COST,
   OTHER_COST,
   TTL_COSTS_WO_SHIP,
   COGS,
   VM_PCT_BEFORE_PROGRAM,
   NET_SALES_BEFORE_PROGRAM,
   HURDLES,
   TTL_SALES,
   VM_DOLLAR_HURDLE,
   CHAR_VM_DOLLAR_HURDLE,
   VM_DOLLAR_HURDLE_COLOR,
   VM_PCT_HURDLE,
   CHAR_VM_PCT_HURDLE,
   VM_PCT_HURDLE_COLOR
)
AS
   (SELECT "DISPLAY_ID",
           "TL_STATUS",
           "TL_CUSTOMER",
           "TL_PROMO_YEAR",
           "TL_PROMO_PERIOD",
           "TL_SKU",
           "TL_DESCRIPTION",
           "TL_STRUCTURE",
           "CP_SKU",
           "CP_DESCRIPTION",
           "CP_QTY_PER_FACING",
           "CP_#_FACINGS",
           "CP_QTY_PER_DISPLAY",
           "TL_QTY",
           "SALES_CASE",
           "CORRUGATE_COST",
           "FULFILLMENT_COST",
           "OTHER_COST",
           "TTL_COSTS_WO_SHIP",
           "COGS",
           "VM_PCT_BEFORE_PROGRAM",
           "NET_SALES_BEFORE_PROGRAM",
           "HURDLES",
           "TTL_SALES",
           "VM_DOLLAR_HURDLE",
           "CHAR_VM_DOLLAR_HURDLE",
           "VM_DOLLAR_HURDLE_COLOR",
           "VM_PCT_HURDLE",
           "CHAR_VM_PCT_HURDLE",
           "VM_PCT_HURDLE_COLOR"
      FROM v_disp_qtrly_mngmnt_rptng_usa
    UNION ALL
    SELECT "DISPLAY_ID",
           "TL_STATUS",
           "TL_CUSTOMER",
           "TL_PROMO_YEAR",
           "TL_PROMO_PERIOD",
           "TL_SKU",
           "TL_DESCRIPTION",
           "TL_STRUCTURE",
           "CP_SKU",
           "CP_DESCRIPTION",
           "CP_QTY_PER_FACING",
           "CP_#_FACINGS",
           "CP_QTY_PER_DISPLAY",
           "TL_QTY",
           "SALES_CASE",
           "CORRUGATE_COST",
           "FULFILLMENT_COST",
           "OTHER_COST",
           "TTL_COSTS_WO_SHIP",
           "COGS",
           "VM_PCT_BEFORE_PROGRAM",
           "NET_SALES_BEFORE_PROGRAM",
           "HURDLES",
           "TTL_SALES",
           "VM_DOLLAR_HURDLE",
           "CHAR_VM_DOLLAR_HURDLE",
           "VM_DOLLAR_HURDLE_COLOR",
           "VM_PCT_HURDLE",
           "CHAR_VM_PCT_HURDLE",
           "VM_PCT_HURDLE_COLOR"
      FROM v_dsp_qtrly_mgmt_rptg_can_conv);
CREATE OR REPLACE FORCE VIEW V_DISPLAY_DEMAND_CANADA
(
   DISPLAY_ID,
   TL_SKU,
   TL_STATUS,
   TL_PROJECT_REP,
   TL_CUSTOMER,
   TL_DESCRIPTION,
   TL_QTY,
   VENDOR,
   CP_BU,
   CP_SKU,
   CP_DESCRIPTION,
   CP_QTY_PER_DISPLAY,
   CP_DISPLAY_SETUP_TYPE,
   VER_CODE_SETUP,
   CP_FG_CONSISTS_OF_SKU,
   CP_I2_FCST,
   CP_SKU_MIX_FINAL,
   CP_PBC_15_DIGIT,
   CP_PLANT,
   CP_PLANNER,
   CP_DC,
   PACKOUT_LOC,
   PRD_DUE_DATE,
   SHIP_FROM_LOCATION,
   PACKOUT_QTY,
   SHIP_QTY,
   SHIP_FROM_DATE,
   MUST_ARRIVE_BY_DATE,
   CP_FG_QTY
)
AS
   SELECT A.DISPLAY_ID,
          A.SKU AS TL_SKU,
          A.STATUS_NAME AS TL_STATUS,
          A.PROJECT_REP AS TL_PROJECT_REP,
          A.CUSTOMER_NAME AS TL_CUSTOMER,
          A.DESCRIPTION AS TL_DESCRIPTION,
          A.QTY_FORECAST AS TL_QTY,
          A.MANUFACTURER_NAME AS VENDOR,
          B.BU_DESC AS CP_BU,
          B.SKU AS CP_SKU,
          B.PRODUCT_NAME AS CP_DESCRIPTION,
          B.QTY_PER_FACING * B.NUMBER_FACINGS AS CP_QTY_PER_DISPLAY,
          A.DISPLAY_SETUP_DESC AS CP_DISPLAY_SETUP_TYPE,
          DECODE (BREAK_CASE,
                  'Production Version', 'PV',
                  'Plant Break Case', 'PBC',
                  'Break Case', 'BC',
                  Break_Case)
             AS VER_CODE_SETUP,
          B.FINISHED_GOODS_SKU AS CP_FG_CONSISTS_OF_SKU,
          B.I2_FORECAST_FLG AS CP_I2_FCST,
          A.SKU_MIX_FINAL_FLG AS CP_SKU_MIX_FINAL,
          CASE
             WHEN B.PBC_VERSION IS NULL THEN NULL
             ELSE SUBSTR (B.SKU, 1, 10) || TRIM (PBC_VERSION)
          END
             CP_PBC_15_DIGIT,
          B.PLANT_CODE AS CP_PLANT,
          B.PLANNER_CODE AS CP_PLANNER,
          B.DC_CODE AS CP_DC,
          C.DC_CODE AS PACKOUT_LOC,
          C.PRODUCT_DUE_DATE AS PRD_DUE_DATE,
          C.SHIP_FROM_LOCATION AS SHIP_FROM_LOCATION,
          C.DC_QUANTITY AS PACKOUT_QTY,
          C.PACKOUT_QUANTITY AS SHIP_QTY,
          C.SHIP_FROM_DATE AS SHIP_FROM_DATE,
          C.MUST_ARRIVE_DATE AS MUST_ARRIVE_BY_DATE,
          B.Finished_Goods_Qty AS CP_FG_QTY
     FROM V_DMM_DISPLAY_CANADA A, V_DMM_COMPONENTS B, V_DMM_PACKOUT_DETAIL C
    WHERE     A.DISPLAY_ID = B.DISPLAY_ID(+)
          AND A.DISPLAY_ID = C.DISPLAY_ID(+)
          AND A.STATUS_NAME IN ('Approved', 'Proposed')
--a.display_id = 208
;
CREATE OR REPLACE FORCE VIEW V_DISPLAY_DEMAND_USA
(
   DISPLAY_ID,
   TL_SKU,
   TL_STATUS,
   TL_PROJECT_REP,
   TL_CUSTOMER,
   TL_DESCRIPTION,
   TL_QTY,
   VENDOR,
   CP_BU,
   CP_SKU,
   CP_DESCRIPTION,
   CP_QTY_PER_DISPLAY,
   CP_DISPLAY_SETUP_TYPE,
   VER_CODE_SETUP,
   CP_FG_CONSISTS_OF_SKU,
   CP_I2_FCST,
   CP_SKU_MIX_FINAL,
   CP_PBC_15_DIGIT,
   CP_PLANT,
   CP_PLANNER,
   CP_DC,
   PACKOUT_LOC,
   PRD_DUE_DATE,
   SHIP_FROM_LOCATION,
   PACKOUT_QTY,
   SHIP_QTY,
   SHIP_FROM_DATE,
   MUST_ARRIVE_BY_DATE,
   CP_FG_QTY
)
AS
   SELECT A.DISPLAY_ID,
          A.SKU AS TL_SKU,
          A.STATUS_NAME AS TL_STATUS,
          A.PROJECT_REP AS TL_PROJECT_REP,
          A.CUSTOMER_NAME AS TL_CUSTOMER,
          A.DESCRIPTION AS TL_DESCRIPTION,
          A.QTY_FORECAST AS TL_QTY,
          A.MANUFACTURER_NAME AS VENDOR,
          B.BU_DESC AS CP_BU,
          B.SKU AS CP_SKU,
          B.PRODUCT_NAME AS CP_DESCRIPTION,
          B.QTY_PER_FACING * B.NUMBER_FACINGS AS CP_QTY_PER_DISPLAY,
          A.DISPLAY_SETUP_DESC AS CP_DISPLAY_SETUP_TYPE,
          DECODE (BREAK_CASE,
                  'Production Version', 'PV',
                  'Plant Break Case', 'PBC',
                  'Break Case', 'BC',
                  Break_Case)
             AS VER_CODE_SETUP,
          B.FINISHED_GOODS_SKU AS CP_FG_CONSISTS_OF_SKU,
          B.I2_FORECAST_FLG AS CP_I2_FCST,
          A.SKU_MIX_FINAL_FLG AS CP_SKU_MIX_FINAL,
          CASE
             WHEN B.PBC_VERSION IS NULL THEN NULL
             ELSE SUBSTR (B.SKU, 1, 10) || TRIM (PBC_VERSION)
          END
             CP_PBC_15_DIGIT,
          B.PLANT_CODE AS CP_PLANT,
          B.PLANNER_CODE AS CP_PLANNER,
          B.DC_CODE AS CP_DC,
          C.DC_CODE AS PACKOUT_LOC,
          C.PRODUCT_DUE_DATE AS PRD_DUE_DATE,
          C.SHIP_FROM_LOCATION AS SHIP_FROM_LOCATION,
          C.DC_QUANTITY AS PACKOUT_QTY,
          C.PACKOUT_QUANTITY AS SHIP_QTY,
          C.SHIP_FROM_DATE AS SHIP_FROM_DATE,
          C.MUST_ARRIVE_DATE AS MUST_ARRIVE_BY_DATE,
          B.Finished_Goods_Qty AS CP_FG_QTY
     FROM V_DMM_DISPLAY_USA A, V_DMM_COMPONENTS B, V_DMM_PACKOUT_DETAIL C
    WHERE     A.DISPLAY_ID = B.DISPLAY_ID(+)
          AND A.DISPLAY_ID = C.DISPLAY_ID(+)
          AND A.STATUS_NAME IN ('Approved', 'Proposed')
--a.display_id = 208
;
