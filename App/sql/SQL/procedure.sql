CREATE OR REPLACE PROCEDURE delete_display (
   in_display_id   IN   NUMBER,
   in_username     IN   VARCHAR2
)
IS
/**********************************************************
 Date: 01/23/2010
 Created By: Lupe Garcia
 Description: Deletes a display by specific display_id and
 all child records in all corresponding tables.

 Modified: 01/25/2010
 Description: Fixed an issue where multiple records  in the
 display_planner and display_packout table we're making the
 stored procedure fail.

 Modified: 01/26/2010
 Description: Added the ability to pass in the username of
 the person deleting the display.  The procedure will then
 add the username to the user_change_log table where the
 display_id passed is matched and the change type is 'Delete'

 Modified: 09/07/2010
 Description: Added deletes for the display_cost_comment table

 Modified: 02/01/2011
 Description: Added deleted for corrugate and corrugate_component

 Modified: 05/09/2011
 Description added delition for calculation_summary
**********************************************************/
   CURSOR k_cursor
   IS
      SELECT NVL (kit_component_id, 0) kit_component_id
        FROM kit_component
       WHERE display_id = in_display_id;

   CURSOR dsw_cursor
   IS
      SELECT NVL (display_ship_wave_id, 0) display_ship_wave_id
        FROM display_ship_wave
       WHERE display_id = in_display_id;

   CURSOR dc_cursor
   IS
      SELECT NVL (display_cost_id, 0) display_cost_id
        FROM display_cost
       WHERE display_id = in_display_id;

   CURSOR dcc_cursor
   IS
      SELECT NVL (display_scenario_id, 0) display_scenario_id
        FROM display_scenario
       WHERE display_id = in_display_id;
BEGIN
   FOR k_rec IN k_cursor
   LOOP
      DELETE FROM display_planner
            WHERE kit_component_id = k_rec.kit_component_id;
   END LOOP;

   FOR dsw_rec IN dsw_cursor
   LOOP
      DELETE FROM display_packout
            WHERE display_ship_wave_id = dsw_rec.display_ship_wave_id;
   END LOOP;

   FOR dc_rec IN dc_cursor
   LOOP
      DELETE FROM display_cost_comment
            WHERE display_cost_id = dc_rec.display_cost_id;
   END LOOP;

   FOR dcc_rec IN dcc_cursor
   LOOP
      DELETE FROM display_cost_comment
            WHERE display_scenario_id = dcc_rec.display_scenario_id;
   END LOOP;

--
   DELETE FROM kit_component
         WHERE display_id = in_display_id;

--
   DELETE FROM display_ship_wave
         WHERE display_id = in_display_id;

--
   DELETE FROM display_material
         WHERE display_id = in_display_id;

--
   DELETE FROM display_material
         WHERE display_id = in_display_id;

--
   DELETE FROM display_logistics
         WHERE display_id = in_display_id;

--
   DELETE FROM display_dc
         WHERE display_id = in_display_id;

--
   DELETE FROM image
         WHERE display_id = in_display_id;

--
   DELETE FROM display_cost
         WHERE display_id = in_display_id;

--
   DELETE FROM customer_marketing
         WHERE display_id = in_display_id;

--
   DELETE FROM corrugate
         WHERE display_id = in_display_id;

--
   DELETE FROM corrugate_component
         WHERE display_id = in_display_id;

--
   DELETE FROM calculation_summary
         WHERE display_id = in_display_id;

--
   DELETE FROM display_scenario
         WHERE display_id = in_display_id;

--
   DELETE FROM display
         WHERE display_id = in_display_id;

   UPDATE user_change_log
      SET username = in_username
    WHERE display_id = in_display_id AND change_type = 'Delete';

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (   DBMS_UTILITY.format_error_stack ()
                            || ' : '
                            || DBMS_UTILITY.format_error_backtrace ()
                           );
      ROLLBACK;
END delete_display;

/
CREATE OR REPLACE PROCEDURE         get_price_exception (
   in_component_id   IN   kit_component.kit_component_id%TYPE,
   in_date           IN   DATE
)
IS
/**********************************************************
  Date: 07/30/2010
  Created By: Lupe Garcia
  Overview: Gets a price exception for a given component.
  Requires the use of two tables in ILS;
  ilspdsi61.ixdps01p
  ilspdsi61.ixdps02p
  Requires the use of a database link;
  Dev and Test = pimilsdev (Public DB Link)
  Production = dwgatep2 (Private DB Link)

  Modified: 08/02/2010
  By: Lupe Garcia
  Overview: Added exception handling for any components
  where we don't have a customer code.

  Modified: 08/04/2010
  By: Lupe Garcia
  Removed the out variable and added an insert statement.
  The procedure will now write the price_exception directly
  to the kit_component table instead of passing it back.

  Modified: 08/26/2010
  By: Lupe Garcia
  Overview: Added in_date to receive a date from the app
  so that the price exception can be gathered on a specific
  date.  Also changed the select statement from the ILS
  response table so that it pulls by customer_code and sku.
**********************************************************/
   v_count             NUMBER;
   v_sku               VARCHAR2 (20);
   v_customer_id       NUMBER;
   v_company_code      VARCHAR2 (10);
   v_customer_code     VARCHAR2 (10);
   v_display_id        NUMBER;
   v_price_exception   NUMBER;
   no_customer_code    EXCEPTION;
   v_date              VARCHAR2 (8);
   not_now exception;
BEGIN

-- clean up the tables so we don't get any errors
   DELETE FROM price_exception;

-- check if the in_date is null and format accordingly
   IF in_date IS NULL
   THEN
      v_date := TO_CHAR (SYSDATE, 'yyyymmdd');
   ELSE
      v_date := TO_CHAR (in_date, 'yyyymmdd');
   END IF;

--  DELETE FROM price_exception_response;
   COMMIT;
   v_count := 0;

   INSERT INTO price_exception
      SELECT k.kit_component_id, k.sku, d.customer_id, c.company_code,
             c.customer_code
        FROM kit_component k, display d, customer c
       WHERE k.kit_component_id = in_component_id
         AND k.display_id = d.display_id
         AND d.customer_id = c.customer_id;

   COMMIT;

   SELECT company_code, customer_code, sku
     INTO v_company_code, v_customer_code, v_sku
     FROM price_exception
    WHERE kit_component_id = in_component_id;

-- check if we have a customer_code
   IF v_customer_code IS NULL
   THEN
      RAISE no_customer_code;
   END IF;

   INSERT INTO ilspdsi61.ixdps01p@dwgatep2
               (dpcomp, dpcust, dpitmn, dppodt
               )
        VALUES (v_company_code, v_customer_code, v_sku, v_date
               );

   COMMIT;

   WHILE v_count < 1
   LOOP
      SELECT COUNT (*)
        INTO v_count
        FROM ilspdsi61.ixdps02p@dwgatep2
       WHERE drpepr IS NOT NULL;

      IF v_count = 1
      THEN
         INSERT INTO price_exception_response
                     (kit_component_id, company_code, customer_code, sku,
                      price_exception, run_date)
            SELECT in_component_id, drcomp, TRIM (drcust), dritmn, drpepr,
                   SYSDATE
              FROM ilspdsi61.ixdps02p@dwgatep2
             WHERE TRIM (drcust) = TRIM (v_customer_code) AND dritmn = v_sku;

         SELECT TO_NUMBER (drpepr)
           INTO v_price_exception
           FROM ilspdsi61.ixdps02p@dwgatep2
          WHERE TRIM (drcust) = TRIM (v_customer_code) AND dritmn = v_sku;
      END IF;
   END LOOP;

   COMMIT;

   UPDATE kit_component
      SET price_exception = NVL (v_price_exception, 0)
    WHERE kit_component_id = in_component_id;

   COMMIT;

-- clean up the ILS tables
   DELETE FROM ilspdsi61.ixdps02p@dwgatep2;

   COMMIT;

EXCEPTION
when not_now
then
      UPDATE kit_component
         SET price_exception = 0
       WHERE kit_component_id = in_component_id;

      COMMIT;
   WHEN no_customer_code
   THEN
      UPDATE kit_component
         SET price_exception = 0
       WHERE kit_component_id = in_component_id;

      COMMIT;
      DBMS_OUTPUT.put_line (   'No Customer code found for kit_component_id '
                            || in_component_id
                           );
   WHEN NO_DATA_FOUND
   THEN
      UPDATE kit_component
         SET price_exception = 0
       WHERE kit_component_id = in_component_id;

      COMMIT;
      DBMS_OUTPUT.put_line ('No Data Found');
      DBMS_OUTPUT.put_line (   DBMS_UTILITY.format_error_stack ()
                            || ' : '
                            || DBMS_UTILITY.format_error_backtrace ()
                           );
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (   DBMS_UTILITY.format_error_stack ()
                            || ' : '
                            || DBMS_UTILITY.format_error_backtrace ()
                           );
      DBMS_OUTPUT.put_line ('Value of v_price_exception: '
                            || v_price_exception
                           );
END get_price_exception;

/
