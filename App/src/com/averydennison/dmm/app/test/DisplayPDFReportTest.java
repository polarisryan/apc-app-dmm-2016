package com.averydennison.dmm.app.test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.averydennison.dmm.app.server.managers.DisplayManager;
import com.averydennison.dmm.app.server.managers.PDFReportManager;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"file:war/WEB-INF/dispatcher-service.xml", "file:war/WEB-INF/dispatcher-data.xml"})
public class DisplayPDFReportTest  {
	@Autowired
	private PDFReportManager pdfReportManager;
    
	@Test
	public void testDisplayPDFReport(){
		try {
			Document d = pdfReportManager.createPDF("151",new FileOutputStream("output/dps-report.pdf"));
			d.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }

}
