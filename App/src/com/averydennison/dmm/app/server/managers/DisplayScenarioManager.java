package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.server.dao.DisplayScenarioDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.DisplayScenario;

public class DisplayScenarioManager {
	 private DisplayScenarioDAO displayScenarioDAO = getDisplayScenarioDAO();
	    public void setDisplayScenarioDAO(DisplayScenarioDAO displayScenarioDAO) {
			this.displayScenarioDAO = displayScenarioDAO;
		}
		public DisplayScenarioDAO getDisplayScenarioDAO() {
	        return displayScenarioDAO;
	    }
	
	    public DisplayScenarioDTO save(DisplayScenarioDTO displayScenarioDTO) {
	    	System.out.println("<<Debug>> : Current DisplayId =" + displayScenarioDTO.getDisplayId());
	    	System.out.println("<<Debug>> : Current DisplayScenarioId =" + displayScenarioDTO.getDisplayScenarioId());
	        DisplayScenario displayScenario = ConverterFactory.convert(displayScenarioDTO);
	        /*
			 *Commented By : Mari
			 *Date         : 01/05/2011
			 *Description  : To avoid duplicate scenario number
			 **/
	        int MAX_SCENARIO=5;
	        Long curScenarioNum= displayScenarioDTO.getScenarioNum();
	        Long curScenarioId= null;
	        List<DisplayScenario> loadedDisplayScenaris =displayScenarioDAO.getDisplayScenariosByScenarioNum(displayScenarioDTO.getDisplayId(),curScenarioNum);
	        if(displayScenarioDTO.getDisplayScenarioId()==null && loadedDisplayScenaris.size()>0 ){
	        	curScenarioNum=loadedDisplayScenaris.get(0).getScenarioNum();
	        	curScenarioId=loadedDisplayScenaris.get(0).getDisplayScenarioId();
	        	displayScenario.setScenarioNum(curScenarioNum);
	        	displayScenario.setDisplayScenarioId(curScenarioId);
	        }
	        if(curScenarioNum>MAX_SCENARIO){
	        	return null;
	        }
	        System.out.println("<<Debug>> : Current ScenarioNum =" + curScenarioNum);
	        displayScenarioDAO.attachDirty(displayScenario);
	        List<DisplayScenario> savedDisplayScenaris =displayScenarioDAO.getDisplayScenariosByScenarioNum(displayScenarioDTO.getDisplayId(),curScenarioNum);
	        DisplayScenarioDTO savedDisplayScenarioDTO=null;
	        if(savedDisplayScenaris!=null && savedDisplayScenaris.size()>0 ){
	        	savedDisplayScenarioDTO=ConverterFactory.convert(savedDisplayScenaris.get(0));
	        }else{
	        	savedDisplayScenarioDTO=null;
	        }
	        return savedDisplayScenarioDTO;
	    }
}
