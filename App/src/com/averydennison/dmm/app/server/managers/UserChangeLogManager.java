package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.CostAnalysisHistoryDTO;
import com.averydennison.dmm.app.server.dao.UserChangeLogDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 12:13:14 PM
 */
public class UserChangeLogManager {
    private UserChangeLogDAO userChangeLogDAO;

    public void setUserChangeLogDAO(UserChangeLogDAO userChangeLogDAO) {
		this.userChangeLogDAO = userChangeLogDAO;
	}

	public UserChangeLogDAO getUserChangeLogDAO() {
        return userChangeLogDAO;
    }
	
	public List<CostAnalysisHistoryDTO> getCostAnalysisHistoryLog(Long displayId, Long displayScenarioId, String tableName) {
		System.out.println("<<Debug>> UserChangeLogManager:getCostAnalysisHistoryLog starts");
		System.out.println("<<Debug>> UserChangeLogManager:getCostAnalysisHistoryLog displayId="+displayId + " tableName="+tableName);
        return ConverterFactory.convertUserChangeLog(userChangeLogDAO.getCostAnalysisHistoryLog(displayId,displayScenarioId, tableName));
    }
}
