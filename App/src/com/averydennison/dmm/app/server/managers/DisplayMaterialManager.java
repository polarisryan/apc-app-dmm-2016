package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.server.dao.DisplayMaterialDAO;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:34:16 AM
 */
public class DisplayMaterialManager {
    private DisplayMaterialDAO displayMaterialDAO;

    public DisplayMaterialDAO getDisplayMaterialDAO() {
        return displayMaterialDAO;
    }

	public void setDisplayMaterialDAO(DisplayMaterialDAO displayMaterialDAO) {
		this.displayMaterialDAO = displayMaterialDAO;
	}
}
