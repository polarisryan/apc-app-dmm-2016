package com.averydennison.dmm.app.server.managers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.server.dao.ManufacturerDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.Country;
import com.averydennison.dmm.app.server.persistence.Manufacturer;

/**
 * User: Spart Arguello 
 * Date: Jan 13, 2010
 */
public class ManufacturerManager {
    private ManufacturerDAO manufacturerDAO;

    public ManufacturerDAO getManufacturerDAO() {
        return manufacturerDAO;
    }

    public void setManufacturerDAO(ManufacturerDAO manufacturerDAO) {
        this.manufacturerDAO = manufacturerDAO;
    }

    public List<Manufacturer> getManufacturers() {
        return manufacturerDAO.getManufacturers();
    }


	public ManufacturerCountryDTO save(ManufacturerCountryDTO manufacturerCountryDTO) {
    	System.out.println("ManufacturerCountryManager:save starts");
    	Manufacturer mc = new Manufacturer();
    	mc.setActiveFlg(manufacturerCountryDTO.isActive());
    	mc.setName(manufacturerCountryDTO.getName());
    	mc.setManufacturerId(manufacturerCountryDTO.getManufacturerId());
    	mc.setCountries(buildCountries(manufacturerCountryDTO));
    	manufacturerDAO.attachDirty(mc);
    	manufacturerCountryDTO.setManufacturerId(mc.getManufacturerId());
        return manufacturerCountryDTO;
	}

	private Set<Country> buildCountries(ManufacturerCountryDTO manufacturerCountryDTO) {
		Set<Country> result = new HashSet<Country>();
		if(manufacturerCountryDTO.getCountryId() == null) return result;
		Country c = new Country();
		c.setCountryId(manufacturerCountryDTO.getCountryId());
		result.add(c);
		return result;
	}

	public void delete(ManufacturerCountryDTO manufacturerCountryDTO) {
		Manufacturer mc = manufacturerDAO.findById(manufacturerCountryDTO.getManufacturerId());	
		manufacturerDAO.delete(mc);
	}
}