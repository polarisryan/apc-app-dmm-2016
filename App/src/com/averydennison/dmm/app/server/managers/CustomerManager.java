package com.averydennison.dmm.app.server.managers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.CustomerTypeDTO;
import com.averydennison.dmm.app.server.dao.CustomerDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.Country;
import com.averydennison.dmm.app.server.persistence.Customer;
import com.averydennison.dmm.app.server.persistence.CustomerType;
import com.averydennison.dmm.app.server.persistence.DisplaySalesRep;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:19:37 AM
 */
public class CustomerManager {
    private CustomerDAO customerDAO = getCustomerDAO();

    public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	public CustomerDAO getCustomerDAO() {
        return customerDAO;
    }

    public List<Customer> getCustomers() {
        CustomerDAO dao = customerDAO;
        return dao.getCustomers();
    }

    public List<CustomerDTO> getCustomerDTOs() {
        return ConverterFactory.convertCustomers(customerDAO.getCustomers(), false);
    }

	public List<CustomerDTO> getAllCustomerDTOs() {
		return ConverterFactory.convertCustomers(customerDAO.getAllCustomers(), true);
	}

	public List<CustomerTypeDTO> getAllCustomerTypeDTOs() {
		return ConverterFactory.convertCustomerTypes(customerDAO.getAllCustomerTypes());
	}

	public CustomerDTO save(CustomerDTO customerDTO) {
		System.out.println("CustomerManager:save starts");
		Customer customer = new Customer();
    	customer.setActiveFlg(customerDTO.isActive());
    	customer.setCompanyCode(customerDTO.getCompanyCode());
    	customer.setCountries(buildCountries(customerDTO));
    	customer.setCustomerCode(customerDTO.getCustomerCode());
    	customer.setCustomerId(customerDTO.getCustomerId());
    	customer.setCustomerName(customerDTO.getCustomerName());
    	customer.setCustomerType(buildCustomerType(customerDTO));
    	customer.setParent(buildParent(customerDTO));
    	customer.setCma(buildCma(customerDTO));
    	customerDAO.attachDirty(customer);
        return ConverterFactory.convert(customer, true);
	}

	public void delete(CustomerDTO customerDTO) {
		Customer customer = customerDAO.findById(customerDTO.getCustomerId());	
		customerDAO.delete(customer);
	}
	
	private Customer buildParent(CustomerDTO customerDTO) {
		if (customerDTO.getCustomerParentId() == null) return null;
		Customer result = new Customer();
		result.setCustomerId(customerDTO.getCustomerParentId());
		result.setCustomerName(customerDTO.getCustomerParentName());
		return result;
	}

	private CustomerType buildCustomerType(CustomerDTO customerDTO) {
		if (customerDTO.getCustomerTypeId() == null) return null;
		CustomerType result = new CustomerType();
		result.setCustomerTypeId(customerDTO.getCustomerTypeId());
		result.setTypeName(customerDTO.getCustomerTypeName());
		return result;
	}
	
	private DisplaySalesRep buildCma(CustomerDTO customerDTO) {
		if (customerDTO.getCustomerCMAId() == null) return null;
		DisplaySalesRep result = new DisplaySalesRep();
		result.setDisplaySalesRepId(customerDTO.getCustomerCMAId());
		result.setFirstName(customerDTO.getCustomerCMAName());
		result.setLastName("");
		return result;
	}

	private Set<Country> buildCountries(CustomerDTO customerDTO) {
		Set<Country> result = new HashSet<Country>();
		if (customerDTO.getCustomerCountryId() == null) return result;
		Country c = new Country();
		c.setCountryId(customerDTO.getCustomerCountryId());
		c.setCountryName(customerDTO.getCustomerCountryName());
		result.add(c);
		return result;
	}

}
