package com.averydennison.dmm.app.server.managers;

import java.util.Date;
import java.util.List;

import com.averydennison.dmm.app.client.App;
import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.server.dao.DisplayCostDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.DisplayCost;
import com.averydennison.dmm.app.server.persistence.DisplayScenario;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:30:55 AM
 */
public class DisplayCostManager {
    private DisplayCostDAO displayCostDAO = getDisplayCostDAO();

    public void setDisplayCostDAO(DisplayCostDAO displayCostDAO) {
		this.displayCostDAO = displayCostDAO;
	}

	public DisplayCostDAO getDisplayCostDAO() {
        return displayCostDAO;
    }
	
	public DisplayCostDTO save(DisplayCostDTO displayCostDTO) {
		System.out.println("DisplayCostManager:save starts");
		System.out.println("DisplayCostManager:save displayCostDTO..getDisplayScenarioId()= "+displayCostDTO.getDisplayScenarioId());
		try{
        DisplayCost displayCostDirty = ConverterFactory.convert(displayCostDTO);
        Long curScenarioId= displayCostDirty.getDisplayScenarioId();
		System.out.println("DisplayCostManager:save curScenarioId= "+curScenarioId);

        Long curDisplayCostId=null;
        DisplayCost loadedDisplayCost =displayCostDAO.loadByDisplayScenarioId(curScenarioId);
        if(loadedDisplayCost!=null && loadedDisplayCost.getDisplayCostId()!=null  ){
        	curDisplayCostId=loadedDisplayCost.getDisplayCostId();
        	displayCostDirty.setDisplayCostId(curDisplayCostId);
        }
        displayCostDAO.attachDirty(displayCostDirty);
	    DisplayCost displayCost =displayCostDAO.loadByDisplayScenarioId(curScenarioId);
		System.out.println("DisplayCostManager:save NULL displayCost ends  displayCost.getDisplayCostId():"+displayCost.getDisplayCostId());
		displayCostDirty.setDisplayCostId(displayCost.getDisplayCostId());
		return ConverterFactory.convert(displayCost);
        }catch(Exception ex){
        	System.out.println("DisplayCostManager:save error "+ ex.toString());
        	return null;
        }
	}
}
