package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.client.models.DisplayLogisticsDTO;
import com.averydennison.dmm.app.server.dao.DisplayLogisticsDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.DisplayLogistics;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:32:17 AM
 */
public class DisplayLogisticsManager {
    private DisplayLogisticsDAO displayLogisticsDAO;

    public DisplayLogisticsDAO getDisplayLogisticsDAO() {
        return displayLogisticsDAO;
    }

    public void setDisplayLogisticsDAO(DisplayLogisticsDAO displayLogisticsDAO) {
		this.displayLogisticsDAO = displayLogisticsDAO;
	}

	public DisplayLogisticsDTO save(DisplayLogisticsDTO displayLogisticsInfoDTO) {
        DisplayLogistics dl = ConverterFactory.convert(displayLogisticsInfoDTO);
        displayLogisticsDAO.attachDirty(dl);
        return ConverterFactory.convert(dl);
    }

}
