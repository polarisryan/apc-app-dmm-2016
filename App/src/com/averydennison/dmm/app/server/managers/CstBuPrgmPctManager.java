package com.averydennison.dmm.app.server.managers;
	import java.util.List;
	import com.averydennison.dmm.app.client.models.CstBuPrgmPctDTO;
	import com.averydennison.dmm.app.server.dao.CstBuPrgmPctDAO;
	import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
	import com.averydennison.dmm.app.server.persistence.CstBuPrgmPct;
	import com.averydennison.dmm.app.server.persistence.Customer;
	import com.averydennison.dmm.app.server.persistence.ValidBu;
	public class CstBuPrgmPctManager {
		private CstBuPrgmPctDAO cstBuPrgmPctDAO = getCstBuPrgmPctDAO();
		private CstBuPrgmPctDAO getCstBuPrgmPctDAO() {
			return cstBuPrgmPctDAO;
		}
		
		public void setCstBuPrgmPctDAO(CstBuPrgmPctDAO cstBuPrgmPctDAO) {
			this.cstBuPrgmPctDAO = cstBuPrgmPctDAO;
		}

		public CstBuPrgmPctDTO save(CstBuPrgmPctDTO cstBuPrgmPctDTO) {
			CstBuPrgmPct cstBuPrgmPct = new CstBuPrgmPct();
			cstBuPrgmPct.setActiveFlg(cstBuPrgmPctDTO.isActiveFlg());
			cstBuPrgmPct.setBu(buildValidBu(cstBuPrgmPctDTO));
			cstBuPrgmPct.setCustomer(buildCustomer(cstBuPrgmPctDTO));
			cstBuPrgmPct.setFyear(cstBuPrgmPctDTO.getFyear());
			cstBuPrgmPct.setProgramPct(cstBuPrgmPctDTO.getProgramPct());
			cstBuPrgmPctDAO.attachDirty(cstBuPrgmPct);
			return ConverterFactory.convert(cstBuPrgmPct);
		}

		private ValidBu buildValidBu(CstBuPrgmPctDTO cstBuPrgmPctDTO) {
			if (cstBuPrgmPctDTO.getBuCode() == null) return null;
			List<ValidBu> result = cstBuPrgmPctDAO.findValidBuByBuCode(cstBuPrgmPctDTO.getBuCode());
			if(result.size()>0)
				return result.get(0);
			else
				return null;
		}

		private Customer buildCustomer(CstBuPrgmPctDTO cstBuPrgmPctDTO) {
			if (cstBuPrgmPctDTO.getCustomerId() == null) return null;
			List<Customer> result = cstBuPrgmPctDAO.findCustomerById(cstBuPrgmPctDTO.getCustomerId());
			if(result.size()>0)
				return result.get(0);
			else
				return null;
		}
		
		public List<ValidBu>  findValidBuByBuCode(String buCode) {
			return cstBuPrgmPctDAO.findValidBuByBuCode(buCode);
		} 
		
		public List<Customer>  findCustomerById(Long customerId) {
			return cstBuPrgmPctDAO.findCustomerById(customerId);
		} 
		
		public List<CstBuPrgmPct>  findCstBuPrgmPctByCustomerId(Long customerId) {
			return cstBuPrgmPctDAO.findCstBuPrgmPctByCustomerId(customerId);
		} 
}
