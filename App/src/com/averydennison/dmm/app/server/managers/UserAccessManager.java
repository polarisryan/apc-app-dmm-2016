package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.server.dao.UserAccessDAO;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:58:42 AM
 */
public class UserAccessManager {
    private UserAccessDAO userAccessDAO;
    
    public UserAccessDAO getUserAccessDAO() {
        return userAccessDAO;
    }

	public void setUserAccessDAO(UserAccessDAO userAccessDAO) {
		this.userAccessDAO = userAccessDAO;
	}

}
