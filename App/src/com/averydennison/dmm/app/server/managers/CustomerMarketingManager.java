package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.client.models.CustomerMarketingDTO;
import com.averydennison.dmm.app.server.dao.CustomerMarketingDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.CustomerMarketing;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:21:35 AM
 */
public class CustomerMarketingManager {
    private CustomerMarketingDAO customerMarketingDAO = getCustomerMarketingDAO();

    public CustomerMarketingDAO getCustomerMarketingDAO() {
        return customerMarketingDAO;
    }

    public void setCustomerMarketingDAO(CustomerMarketingDAO customerMarketingDAO) {
		this.customerMarketingDAO = customerMarketingDAO;
	}

	public CustomerMarketingDTO save(CustomerMarketingDTO customerMarketingDTO) {
    	System.out.println("CustomerMarketingManager:save starts");
        CustomerMarketing cm = ConverterFactory.convert(customerMarketingDTO);
    	System.out.println("CustomerMarketingManager:save ...1...");
        customerMarketingDAO.attachDirty(cm);
    	System.out.println("CustomerMarketingManager:save ...2...");
        return ConverterFactory.convert(cm);
    }
}
