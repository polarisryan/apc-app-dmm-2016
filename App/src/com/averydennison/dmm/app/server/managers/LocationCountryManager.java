package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.server.dao.LocationCountryDAO;
import com.averydennison.dmm.app.server.persistence.LocationCountry;

public class LocationCountryManager {
	    private LocationCountryDAO locationCountryDAO;

	    public void setLocationCountryDAO(LocationCountryDAO locationCountriesDAO) {
			this.locationCountryDAO = locationCountriesDAO;
		}

		public LocationCountryDAO getLocationsCountryDAO() {
	        return locationCountryDAO;
	    }

	    public List<LocationCountry> getAllActiveLocationsCountry() {
	        return locationCountryDAO.getAllActiveLocationsCountry();
	    }

		public List<LocationCountry> getAllLocationsCountry() {
			return locationCountryDAO.getAllLocationsCountry();		
		}
}