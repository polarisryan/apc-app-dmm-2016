package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.client.models.DisplayPlannerDTO;
import com.averydennison.dmm.app.server.dao.DisplayPlannerDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.DisplayPlanner;

/**
 * User: Alvin Wong
 * Date: Dec 20, 2009
 * Time: 11:43:09 AM
 */
public class DisplayPlannerManager {
    private DisplayPlannerDAO displayPlannerDAO;

    public void setDisplayPlannerDAO(DisplayPlannerDAO displayPlannerDAO) {
		this.displayPlannerDAO = displayPlannerDAO;
	}

	public DisplayPlannerDAO getDisplayPlannerDAO() {
        return displayPlannerDAO;
    }

    public DisplayPlannerDTO save(DisplayPlannerDTO displayPlannerDTO) {
        DisplayPlanner dl = ConverterFactory.convert(displayPlannerDTO);
        displayPlannerDAO.attachDirty(dl);
        return ConverterFactory.convert(dl);
    }

}
