package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayDcDTO;
import com.averydennison.dmm.app.client.models.DisplayPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.client.models.SlimPackoutDTO;
import com.averydennison.dmm.app.server.dao.DisplayPackoutDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayDc;
import com.averydennison.dmm.app.server.persistence.DisplayPackout;
import com.averydennison.dmm.app.server.persistence.DisplayShipWave;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:36:24 AM
 */
public class DisplayPackoutManager {
    private DisplayPackoutDAO displayPackoutDAO;

    public DisplayPackoutDAO getDisplayPackoutDAO() {
        return displayPackoutDAO;
    }

    public DisplayPackoutDTO save(DisplayPackoutDTO displayPackoutDTO) {
        DisplayPackout displayPackout = ConverterFactory.convert(displayPackoutDTO);
        displayPackoutDAO.attachDirty(displayPackout);
        return ConverterFactory.convert(displayPackout);
    }

    public void delete(DisplayPackoutDTO displayPackoutDTO) {
        DisplayPackout displayPackout = ConverterFactory.convert(displayPackoutDTO);
        displayPackoutDAO.delete(displayPackout);
	}

    public void setDisplayPackoutDAO(DisplayPackoutDAO displayPackoutDAO) {
		this.displayPackoutDAO = displayPackoutDAO;
	}

	public List<DisplayPackoutDTO> findDisplayPackoutsByDisplayDc(DisplayDcDTO displayDcDTO) {
        DisplayDc d = ConverterFactory.convert(displayDcDTO);
        return ConverterFactory.convertDisplayPackouts(displayPackoutDAO.findDisplayPackoutsByDisplayDc(d));
    }

	public List<DisplayPackoutDTO> findDisplayPackoutsByShipWave(DisplayShipWaveDTO displayShipWaveDTO) {
        DisplayShipWave d = ConverterFactory.convert(displayShipWaveDTO);
        return ConverterFactory.convertDisplayPackouts(displayPackoutDAO.findDisplayPackoutsByShipWave(d));
    }
	
    public List<SlimPackoutDTO> findDisplayPackoutsByDisplay(DisplayDTO displayDTO) {
        Display d = ConverterFactory.convert(displayDTO);
        return displayPackoutDAO.findDisplayPackoutsByDisplay(d);
    }
    
 
}
