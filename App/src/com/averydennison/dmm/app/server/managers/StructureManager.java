package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.StructureDTO;
import com.averydennison.dmm.app.server.dao.StructureDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.Customer;
import com.averydennison.dmm.app.server.persistence.Structure;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:55:49 AM
 */
public class StructureManager {
    private StructureDAO structureDAO;

    public void setStructureDAO(StructureDAO structureDAO) {
		this.structureDAO = structureDAO;
	}

	public StructureDAO getStructureDAO() {
        return structureDAO;
    }

    public List<Structure> getStructures() {
        return structureDAO.getStructures();
    }

    public List<StructureDTO> getStructureDTOs() {
        return ConverterFactory.convertStructures(structureDAO.getStructures());
    }

	public List<Structure> getAllStructures() {
		return structureDAO.getAllStructures();
	}

	public StructureDTO save(StructureDTO dto) {
		Structure structure = new Structure();
    	structure.setActiveFlg(dto.isActiveFlg());
    	structure.setStructureId(dto.getStructureId());
    	structure.setStructureName(dto.getStructureName());
    	structureDAO.attachDirty(structure);
        return ConverterFactory.convert(structure);
	}

	public void delete(StructureDTO model) {
		Structure structure = structureDAO.findById(model.getStructureId());	
		structureDAO.delete(structure);
	}
}
