package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.server.dao.CustomerCountryDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.CustomerCountry;

public class CustomerCountryManager {
    private CustomerCountryDAO customerCountryDAO = getCustomerCountryDAO();

    public void setCustomerCountryDAO(CustomerCountryDAO customerCountryDAO) {
		this.customerCountryDAO = customerCountryDAO;
	}

	public CustomerCountryDAO getCustomerCountryDAO() {
        return customerCountryDAO;
    }

    public List<CustomerCountry> getCustomers() {
        CustomerCountryDAO dao = customerCountryDAO;
        return dao.getCustomerCountries();
    }

    public List<CustomerCountryDTO> getCustomerCountryDTOs() {
        return ConverterFactory.convertCustomersCountry(customerCountryDAO.getCustomerCountries());
    }

	public CustomerCountry findByNameAndCountry(String customerName, Long customerCountryId) {
		return customerCountryDAO.findByNameAndCountry(customerName, customerCountryId);
	}
}
