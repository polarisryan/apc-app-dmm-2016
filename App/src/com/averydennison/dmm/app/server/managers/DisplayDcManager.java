package com.averydennison.dmm.app.server.managers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayDcAndPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayDcDTO;
import com.averydennison.dmm.app.server.dao.DisplayDcDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayDc;
import com.averydennison.dmm.app.server.persistence.DisplayDcAndPackout;

/**
 * User: Spart Arguello
 * Date: Dec 8, 2009
 * Time: 2:59:49 PM
 */
public class DisplayDcManager {
    private DisplayDcDAO displayDcDAO;
    
    public DisplayDcDAO getDisplayDcDAO() {
        return displayDcDAO;
    }

    public void setDisplayDcDAO(DisplayDcDAO displayDcDAO) {
		this.displayDcDAO = displayDcDAO;
	}
    
    public Map<Long, DisplayDc> findDisplayDcMapByDisplay(Display display) {
        return displayDcDAO.findDisplayDcMapByDisplay(display);
    }

	public List<DisplayDcAndPackoutDTO> findDisplayDcAndPackoutsByDisplay(DisplayDTO displayDTO) {
		List<DisplayDcAndPackoutDTO>dcpDTOList = new ArrayList<DisplayDcAndPackoutDTO>();
        Display d = ConverterFactory.convert(displayDTO);

		List<DisplayDcAndPackout> dcpList = displayDcDAO.findDisplayDcAndPackoutsByDisplay(d);
		
		// format for packout details tree grid
		String dcNode = "";
		int rowId = 0;
		Iterator<DisplayDcAndPackout>iter = dcpList.iterator();
		while (iter.hasNext()) {
			System.out.println("DisplayManager:findDisplayDcAndPackoutsByDisplay record found");
			DisplayDcAndPackout dap = (DisplayDcAndPackout)iter.next();
			DisplayDcAndPackoutDTO dapDTO = new DisplayDcAndPackoutDTO();
			if (!dcNode.equals(dap.getDcCode())) {
				DisplayDcAndPackoutDTO dc = new DisplayDcAndPackoutDTO();
				dc.setDisplayDcId(dap.getDisplayDcId());			
				dc.setDisplayId(dap.getDisplayId());
				dc.setLocationId(dap.getLocationId());
				dc.setDcCode(dap.getDcCode());
				dc.setDcQuantity(dap.getDcQuantity());
				dc.setDtCreated(dap.getDtCreated());
				dc.setCorrugateDueDate(dap.getCorrugateDueDate());
				dc.setDeliveryDate(dap.getDeliveryDate());
				dc.setShipLocProductDueDate(dap.getShipLocProductDueDate());
				if(dap.getShipFromLocationId()!=null && dap.getShipFromLocationId().toString().trim().length() > 0){
					dc.setShipFromDcCode(dap.getShipFromDcCode());
					dc.setShipFromLocationId(dap.getShipFromLocationId());
				}
				dc.setDtLastChanged(dap.getDtLastChanged());
				dc.setUserCreated(dap.getUserCreated());
				dc.setUserLastChanged(dap.getUserLastChanged());
				dc.setDeleteFlg("N");
				dc.setType("DC");
				dc.setRowId(String.valueOf((new Date()).getTime()) + rowId++);
				dcpDTOList.add(dc);
				dcNode = dap.getDcCode();
			}

			dapDTO.setDisplayDcId(dap.getDisplayDcId());			
			dapDTO.setDisplayId(dap.getDisplayId());
			dapDTO.setLocationId(dap.getLocationId());
			dapDTO.setDcCode(dap.getDcCode());
			dapDTO.setDcQuantity(dap.getDcQuantity());
			dapDTO.setDtCreated(dap.getDtCreated());
			dapDTO.setDtLastChanged(dap.getDtLastChanged());
			dapDTO.setUserCreated(dap.getUserCreated());
			dapDTO.setUserLastChanged(dap.getUserLastChanged());
			dapDTO.setShipWaveNum(dap.getShipWaveNum());
			dapDTO.setMustArriveDate(dap.getMustArriveDate());
			dapDTO.setDisplayPackoutId(dap.getDisplayPackoutId());
			dapDTO.setDisplayShipWaveId(dap.getDisplayShipWaveId());
			dapDTO.setShipFromDate(dap.getShipFromDate());
			dapDTO.setCorrugateDueDate(dap.getCorrugateDueDate());
			dapDTO.setDeliveryDate(dap.getDeliveryDate());
			dapDTO.setShipLocProductDueDate(dap.getShipLocProductDueDate());
			dapDTO.setProductDueDate(dap.getProductDueDate());
			dapDTO.setPoQuantity(dap.getPoQuantity());
			dapDTO.setPoDtCreated(dap.getPoDtCreated());
			dapDTO.setPoDtLastChanged(dap.getPoDtLastChanged());
			dapDTO.setPoUserCreated(dap.getPoUserCreated());
			dapDTO.setPoUserLastChanged(dap.getPoUserLastChanged());
			dapDTO.setDeleteFlg("N");
			dapDTO.setType("Packout");
			dapDTO.setRowId(String.valueOf((new Date()).getTime()) + rowId++);
			if(dap.getShipFromLocationId()!=null && dap.getShipFromLocationId().toString().trim().length() > 0){
				dapDTO.setShipFromLocationId(dap.getShipFromLocationId());
				dapDTO.setShipFromDcCode(dap.getShipFromDcCode());
			}
			if (dapDTO.getDisplayPackoutId() != null)
				dcpDTOList.add(dapDTO);
		}
		
        return dcpDTOList;
    }


	public void save(DisplayDc displayDc) {
		displayDcDAO.attachDirty(displayDc);
    }

    public DisplayDcDTO save(DisplayDcDTO displayDc) {
        DisplayDc dc = ConverterFactory.convert(displayDc);
        displayDcDAO.attachDirty(dc);
        return ConverterFactory.convert(dc);
    }

    public void delete(DisplayDcDTO displayDc) {
        DisplayDc dc = ConverterFactory.convert(displayDc);
        displayDcDAO.delete(dc);
    }

}
