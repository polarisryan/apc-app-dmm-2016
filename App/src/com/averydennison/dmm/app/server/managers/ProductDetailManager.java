package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.server.dao.ProductDetailDAO;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:49:38 AM
 */
public class ProductDetailManager {
    private ProductDetailDAO productDetailDAO;

    public ProductDetailDAO getProductDetailDAO() {
        return productDetailDAO;
    }

	public void setProductDetailDAO(ProductDetailDAO productDetailDAO) {
		this.productDetailDAO = productDetailDAO;
	}
}
