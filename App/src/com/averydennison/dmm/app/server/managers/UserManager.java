package com.averydennison.dmm.app.server.managers;

import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.security.userdetails.User;
import org.springframework.security.userdetails.ldap.LdapUserDetailsImpl;

import com.averydennison.dmm.app.client.models.UserDTO;
import com.averydennison.dmm.app.client.models.UserTypeDTO;
import com.averydennison.dmm.app.server.dao.UserDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;

/**
 * User: Spart Arguello
 * Date: Oct 16, 2009
 * Time: 9:14:19 AM
 */
public class UserManager {
	private static Logger logger = Logger.getLogger(UserManager.class);
	
	private Properties roles;

	private UserDAO userDAO;

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
    }
    
	private String[] getUserPermissions(String[] auth){
		String per = "";
		for(int i=0;i<auth.length;i++){
			if(roles.getProperty(auth[i])!=null){
				per += roles.getProperty(auth[i]) + "|";
			}
		}
		return per.split("\\|");
	}
	
	public UserDTO getCurrentUserInfo(){
		UserDTO uDTO = null;
		String[] authorities;
		User u = null;
		LdapUserDetailsImpl ludi = null;
		String username;
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if ( obj instanceof User ) {
			u = (User)obj;
			username=u.getUsername();
			uDTO = new UserDTO();
			uDTO.setUsername(u.getUsername());
			System.out.println("Login user: " + username);
			GrantedAuthority[] ga = u.getAuthorities();
			authorities = new String[ga.length];
			if(ga!=null){
				for (int i=0; i<ga.length; i++)
			    {
					GrantedAuthorityImpl gai = (GrantedAuthorityImpl)ga[i];
					authorities[i] = gai.getAuthority();	
					System.out.println("Authorities: " + gai.getAuthority());
			    }
				uDTO.setAuthorities(authorities);
				uDTO.setPermissions(getUserPermissions(authorities));
			}
		}else if(obj instanceof LdapUserDetailsImpl){
			ludi = (LdapUserDetailsImpl)obj;
			username=ludi.getUsername();
			uDTO = new UserDTO();
			uDTO.setUsername(ludi.getUsername());
			System.out.println("LDAP Login user: " + username);
			GrantedAuthority[] ga = ludi.getAuthorities();
			authorities = new String[ga.length];
			if(ga!=null){
				for (int i=0; i<ga.length; i++)
			    {
					GrantedAuthorityImpl gai = (GrantedAuthorityImpl)ga[i];
					authorities[i] = gai.getAuthority();
					
					System.out.println("Authorities: " + gai.getAuthority());
			    }
				uDTO.setAuthorities(authorities);
				uDTO.setPermissions(getUserPermissions(authorities));
			}
		}
		return uDTO;
		
	}
	
	public Properties getRoles() {
		return roles;
	}

	public void setRoles(Properties roles) {
		this.roles = roles;
	}

	public List<UserTypeDTO> getAllUserTypeDTOs() {
		return ConverterFactory.convertUserTypes(userDAO.getAllUserTypes());
	}
	
}
