package com.averydennison.dmm.app.server.managers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.server.dao.LocationDAO;
import com.averydennison.dmm.app.server.persistence.Country;
import com.averydennison.dmm.app.server.persistence.Location;
import com.averydennison.dmm.app.server.persistence.Manufacturer;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:48:37 AM
 */
public class LocationManager {
    private LocationDAO locationDAO;

    public void setLocationDAO(LocationDAO locationsDAO) {
		this.locationDAO = locationsDAO;
	}

	public LocationDAO getLocationsDAO() {
        return locationDAO;
    }

    public List<Location> getAllActiveLocations() {
        return locationDAO.getAllActiveLocations();
    }

	public LocationCountryDTO save(LocationCountryDTO locationCountryDTO) {
    	Location location = new Location();
    	location.setActiveFlg(locationCountryDTO.isActiveFlg());
    	location.setLocationName(locationCountryDTO.getLocationName());
    	location.setLocationCode(locationCountryDTO.getLocationCode());
    	location.setLocationId(locationCountryDTO.getLocationId());
    	location.setCountries(buildCountries(locationCountryDTO));
    	locationDAO.attachDirty(location);
    	locationCountryDTO.setLocationId(location.getLocationId());
        return locationCountryDTO;
	}

	private Set<Country> buildCountries(LocationCountryDTO locationCountryDTO) {
		Set<Country> result = new HashSet<Country>();
		if(locationCountryDTO.getCountryId() == null) return result;
		Country c = new Country();
		c.setCountryId(locationCountryDTO.getCountryId());
		result.add(c);
		return result;
	}

	public void delete(LocationCountryDTO locationDTO) {
		Location location = locationDAO.findById(locationDTO.getLocationId());	
		locationDAO.delete(location);		
	}
}
