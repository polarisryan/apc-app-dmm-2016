package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.client.models.CorrugateComponentDTO;
import com.averydennison.dmm.app.server.dao.CorrugateComponentDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.CorrugateComponent;

	/**
	 * User: Mari
	 * Date: Jan 04, 2011
	 * Time: 06:01:40 PM
	 */
public class CorrugateComponentManager {
	    private CorrugateComponentDAO corrugateComponentDAO;
	    public CorrugateComponentDAO getCorrugateComponentDAO() {
	        return corrugateComponentDAO;
	    }
	    
	    public void setCorrugateComponentDAO(CorrugateComponentDAO corrugateComponentDAO) {
			this.corrugateComponentDAO = corrugateComponentDAO;
		}
	    
		public CorrugateComponentDTO save(CorrugateComponentDTO corrugateComponentDTO) {
			CorrugateComponent dl = ConverterFactory.convert(corrugateComponentDTO);
	        corrugateComponentDAO.attachDirty(dl);
	        return ConverterFactory.convert(dl);
	    }

	}

