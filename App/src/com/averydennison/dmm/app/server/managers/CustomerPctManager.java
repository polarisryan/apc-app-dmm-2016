package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.server.dao.CustomerPctDAO;



public class CustomerPctManager {
	 private CustomerPctDAO customerPctDAO = getCustomerPctDAO();

	    public void setCustomerPctDAO(CustomerPctDAO customerPctDAO) {
			this.customerPctDAO = customerPctDAO;
		}

		public CustomerPctDAO getCustomerPctDAO() {
	        return customerPctDAO;
	    }
}
