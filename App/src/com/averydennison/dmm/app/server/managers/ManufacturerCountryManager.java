package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.server.dao.ManufacturerCountryDAO;
import com.averydennison.dmm.app.server.persistence.ManufacturerCountry;

public class ManufacturerCountryManager {
	 private ManufacturerCountryDAO manufacturerCountryDAO;

	    public ManufacturerCountryDAO getManufacturerCountryDAO() {
	        return manufacturerCountryDAO;
	    }

	    public void setManufacturerCountryDAO(ManufacturerCountryDAO manufacturerCountryDAO) {
	        this.manufacturerCountryDAO = manufacturerCountryDAO;
	    }

	    public List<ManufacturerCountry> getManufacturersCountry() {
	        return manufacturerCountryDAO.getManufacturersCountry();
	    }
	    
	    public List<ManufacturerCountry> getAllManufacturersCountry() {
	        return manufacturerCountryDAO.getAllManufacturersCountry();
	    }

		public ManufacturerCountry findByNameAndCountry(String name, Long countryId) {
			return manufacturerCountryDAO.findByNameAndCountry(name, countryId);
		}

	}
