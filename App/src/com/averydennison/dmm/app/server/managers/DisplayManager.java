package com.averydennison.dmm.app.server.managers;


import com.averydennison.dmm.app.client.models.BooleanDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.server.dao.DisplayDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.Display;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:23:22 AM
 */
public class DisplayManager {
    private DisplayDAO displayDAO;

    public DisplayDAO getDisplayDAO() {
        return displayDAO;
    }

    public DisplayDTO save(DisplayDTO displayDTO) {
    	System.out.println("<<Debug>> : Current DisplayId " + displayDTO.getDisplayId());
        Display display = ConverterFactory.convert(displayDTO);
        displayDAO.attachDirty(display);
        
        if (display.getDisplayId() != null) {
        	displayDAO.updateGoNoGo(display.getDisplayId());
        	display = displayDAO.findById(display.getDisplayId());
        }
        return ConverterFactory.convert(display, true);
    }

	public void setDisplayDAO(DisplayDAO displayDAO) {
		this.displayDAO = displayDAO;
	}

	public BooleanDTO isDuplicateSKU(String sku) {
		return displayDAO.isDuplicateSKU(sku);
	}
	
	public void callDisplayDeleteStoredProcedure(Long id, String username){
	    displayDAO.callDisplayDeleteStoredProcedure(id, username);
	}

}
