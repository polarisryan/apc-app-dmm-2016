package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.PromoPeriodDTO;
import com.averydennison.dmm.app.server.dao.PromoPeriodDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.PromoPeriod;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:53:45 AM
 */
public class PromoPeriodManager {
    private PromoPeriodDAO promoPeriodDAO;

    public PromoPeriodDAO getPromoPeriodDAO() {
        return promoPeriodDAO;
    }

    public List<PromoPeriod> getPromoPeriods() {
        return promoPeriodDAO.getPromoPeriods();
    }

    public void setPromoPeriodDAO(PromoPeriodDAO promoPeriodDAO) {
		this.promoPeriodDAO = promoPeriodDAO;
	}

	public List<PromoPeriodDTO> getPromoPeriodDTOs() {
        return ConverterFactory.convertPromoPeriods(promoPeriodDAO.getPromoPeriods());
    }
}
