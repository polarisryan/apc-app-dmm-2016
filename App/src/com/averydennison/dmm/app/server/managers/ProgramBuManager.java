package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.ProgramBuDTO;
import com.averydennison.dmm.app.server.dao.ProgramBuDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.ProgramBu;
import com.averydennison.dmm.app.server.persistence.ValidBu;

public class ProgramBuManager {
    
	private ProgramBuDAO programBuDAO = getProgramBuDAO();

	private ProgramBuDAO getProgramBuDAO() {
		return programBuDAO;
	}

	public void setProgramBuDAO(ProgramBuDAO programBuDAO) {
		this.programBuDAO = programBuDAO;
	}

	public ProgramBuDTO save(ProgramBuDTO programDTO) {
		ProgramBu program = new ProgramBu();
		program.setProgramBuId(programDTO.getProgramBuId());
		program.setDisplayId(programDTO.getDisplayId());
		program.setBu(buildBu(programDTO));
		program.setProgramPct(programDTO.getProgramPct());
		program.setDtCreated(programDTO.getDtCreated());
		program.setDtLastChanged(programDTO.getDtLastChanged());
		program.setUserCreated(programDTO.getUserCreated());
		program.setUserLastChanged(programDTO.getUserLastChanged());
		programBuDAO.attachDirty(program);
		return ConverterFactory.convert(program);
	}

	private ValidBu buildBu(ProgramBuDTO programDTO) {
		if (programDTO.getBuCode() == null) return null;
		ValidBu result = new ValidBu();
		result.setBuCode(programDTO.getBuCode());
		result.setBuDesc(programDTO.getBuDesc());
		return result;
	}

	public List<ProgramBu> findByDisplayId(Long displayId) {
		return programBuDAO.findProgramBuByDisplayId(displayId);
	}
}
