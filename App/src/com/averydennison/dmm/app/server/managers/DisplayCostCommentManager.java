package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.client.models.DisplayCostCommentDTO;
import com.averydennison.dmm.app.server.dao.DisplayCostCommentDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.DisplayCostComment;


public class DisplayCostCommentManager {
								   
	 private DisplayCostCommentDAO displayCostCommentDAO;

	    public DisplayCostCommentDAO getDisplayCostCommentDAO() {
	        return displayCostCommentDAO;
	    }
	    public DisplayCostCommentDTO save(DisplayCostCommentDTO displayCostCommentDTO) {
	    	System.out.println("DisplayCostCommentManager:save displayCostCommentDTO.getDisplayScenarioId =>>>"+displayCostCommentDTO.getDisplayScenarioId());
	        DisplayCostComment displayCostComment = ConverterFactory.convert(displayCostCommentDTO);
	    	System.out.println("DisplayCostCommentManager:save displayCostComment.getDisplayScenarioId =>>>"+displayCostComment.getDisplayScenarioId());
	        displayCostCommentDAO.attachDirty(displayCostComment);
	        /*if (displayCostComment.getDisplayCostId() != null) {
	        	displayCostComment = displayCostCommentDAO.findByDisplayCostId(displayCostComment.getDisplayCostId());
	        }*/
	        return ConverterFactory.convert(displayCostComment);
	    }

		public void setDisplayCostCommentDAO(DisplayCostCommentDAO displayCostCommentDAO) {
			this.displayCostCommentDAO = displayCostCommentDAO;
		}

}
