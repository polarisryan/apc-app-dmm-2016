package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.server.dao.DisplaySalesRepDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.Country;
import com.averydennison.dmm.app.server.persistence.DisplaySalesRep;
import com.averydennison.dmm.app.server.persistence.UserType;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:44:08 AM
 */
public class DisplaySalesRepManager {
    private DisplaySalesRepDAO displaySalesRepDAO;

    public DisplaySalesRepDAO getDisplaySalesRepDAO() {
        return displaySalesRepDAO;
    }

    public void setDisplaySalesRepDAO(DisplaySalesRepDAO displaySalesRepDAO) {
		this.displaySalesRepDAO = displaySalesRepDAO;
	}

	public List<DisplaySalesRep> getDisplaySalesReps() {
        return displaySalesRepDAO.getDisplaySalesReps();
    }

    public List<DisplaySalesRepDTO> getDisplaySalesRepDTOs() {
        return ConverterFactory.convertDisplaySalesReps(displaySalesRepDAO.getDisplaySalesReps(), false);
    }

	public List<DisplaySalesRep> getAllDisplaySalesReps() {
		return displaySalesRepDAO.getAllDisplaySalesReps();
	}

	public DisplaySalesRepDTO save(DisplaySalesRepDTO dto) {
		DisplaySalesRep entity = new DisplaySalesRep();
    	entity.setActiveFlg(dto.isActiveFlg());
    	entity.setDisplaySalesRepId(dto.getDisplaySalesRepId());
    	entity.setFirstName(dto.getFirstName());
    	entity.setLastName(dto.getLastName());
    	entity.setCountry(buildCountry(dto));
    	entity.setUserType(buildUserType(dto));
    	displaySalesRepDAO.attachDirty(entity);
        return ConverterFactory.convert(entity, true);
	}

	public void delete(DisplaySalesRepDTO model) {
		DisplaySalesRep entity = displaySalesRepDAO.findById(model.getDisplaySalesRepId());	
		displaySalesRepDAO.delete(entity);
	}
	
	private UserType buildUserType(DisplaySalesRepDTO dto) {
		if (dto.getUserTypeId() == null) return null;
		UserType result = new UserType();
		result.setUserTypeId(dto.getUserTypeId());
		result.setTypeName(dto.getUserTypeName());
		return result;
	}
	
	private Country buildCountry(DisplaySalesRepDTO dto) {
		if (dto.getCountryId() == null) return null;
		Country result = new Country();
		result.setCountryId(dto.getCountryId());
		result.setCountryName(dto.getCountryName());
		return result;
	}

	public List<DisplaySalesRepDTO> getApproverDTOs() {
		 return ConverterFactory.convertDisplaySalesReps(displaySalesRepDAO.getApprovers(), false);
	}
}
