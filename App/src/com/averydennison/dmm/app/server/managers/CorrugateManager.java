package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.client.models.CorrugateDTO;
import com.averydennison.dmm.app.server.dao.CorrugateDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.Corrugate;

public class CorrugateManager {
	 private CorrugateDAO corrugateDAO;
	    public CorrugateDAO getCorrugateDAO() {
	        return corrugateDAO;
	    }
	    
	    public void setCorrugateDAO(CorrugateDAO corrugateDAO) {
			this.corrugateDAO = corrugateDAO;
		}
	    
		public CorrugateDTO save(CorrugateDTO corrugateDTO) {
			System.out.println("CorrugateManager:save starts");
			Corrugate dirtyCorrugate = ConverterFactory.convert(corrugateDTO);
			Corrugate loadedCorrugate=null;
			Corrugate savedCorrugate=null;
			if(dirtyCorrugate.getDisplayId()!=null){
				loadedCorrugate =corrugateDAO.findByDisplayId(dirtyCorrugate.getDisplayId());
				if(loadedCorrugate!=null){
/*					loadedCorrugate.setCorrugateDueDate(dirtyCorrugate.getCorrugateDueDate());
*/					loadedCorrugate.setCreateDate(dirtyCorrugate.getCreateDate());
/*					loadedCorrugate.setDeliveryDate(dirtyCorrugate.getDeliveryDate());
*/					corrugateDAO.attachDirty(loadedCorrugate);
				}else{
					corrugateDAO.attachDirty(dirtyCorrugate);
				}
			}else{
				return null;
			}
			savedCorrugate =corrugateDAO.findByDisplayId(dirtyCorrugate.getDisplayId());
			CorrugateDTO savedCorrugateDTO=null;
	        if(savedCorrugate!=null  ){
	        	savedCorrugateDTO=ConverterFactory.convert(savedCorrugate);
	        }else{
	        	savedCorrugateDTO=null;
	        }
	        System.out.println("CorrugateManager:save ends");
	        return savedCorrugateDTO;
	    }
	}
