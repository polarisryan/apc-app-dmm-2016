package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.ValidPlannerDTO;
import com.averydennison.dmm.app.server.dao.ValidPlannerDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.ValidPlanner;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 11:55:49 AM
 */
public class ValidPlannerManager {
	public ValidPlannerDAO validPlannerDAO;
	
    public ValidPlannerDAO getValidPlannerDAO() {
		return validPlannerDAO;
	}

	public void setValidPlannerDAO(ValidPlannerDAO validPlannerDAO) {
		this.validPlannerDAO = validPlannerDAO;
	}

	public List<ValidPlanner> getPlanners() {
        return validPlannerDAO.getPlanners();
    }

    public List<ValidPlannerDTO> getValidPlannerDTOs() {
        return ConverterFactory.convertPlanners(validPlannerDAO.getPlanners());
    }
    
}
