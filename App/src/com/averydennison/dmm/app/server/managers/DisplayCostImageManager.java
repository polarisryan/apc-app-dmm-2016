package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayCostImageDTO;
import com.averydennison.dmm.app.server.dao.DisplayCostImageDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.DisplayCostImage;

public class DisplayCostImageManager {
	private DisplayCostImageDAO displayCostImageDAO = getDisplayCostImageDAO();

    public void setDisplayCostImageDAO(DisplayCostImageDAO displayCostImageDAO) {
		this.displayCostImageDAO = displayCostImageDAO;
	}

	public DisplayCostImageDAO getDisplayCostImageDAO() {
        return displayCostImageDAO;
    }
	
	public List<DisplayCostImageDTO> save(DisplayCostImageDTO displayCostImageDTO) {
		System.out.println("DisplayCostImageManager:save starts");
		System.out.println("DisplayCostImageManager:save ..1..scenarioId="+ displayCostImageDTO.getDisplayScenarioId());
        DisplayCostImage displayCostImageDirty = ConverterFactory.convert(displayCostImageDTO);
        System.out.println("DisplayCostImageManager:save ..1..scenarioId="+ displayCostImageDirty.getDisplayScenarioId());
		displayCostImageDAO.attachDirty(displayCostImageDirty);
		List <DisplayCostImage> displayCostImages=displayCostImageDAO.findByDisplayScenarioId(displayCostImageDirty.getDisplayScenarioId());
		return ConverterFactory.convertDisplayCostImage(displayCostImages);
	}
	
	public List<DisplayCostImageDTO> delete(DisplayCostImageDTO displayCostImageDTO) {
		System.out.println("DisplayCostImageManager:delete starts");
        DisplayCostImage displayCostImageDirty = ConverterFactory.convert(displayCostImageDTO);
		System.out.println("DisplayCostImageManager:delete ..1.."+ displayCostImageDTO.getUrl());
		displayCostImageDAO.delete(displayCostImageDirty);
		List <DisplayCostImage> displayCostImages=displayCostImageDAO.findByDisplayScenarioId(displayCostImageDirty.getDisplayScenarioId());
		return ConverterFactory.convertDisplayCostImage(displayCostImages);
	}
}
