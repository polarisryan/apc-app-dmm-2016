package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.server.dao.ProductPlannerDAO;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:52:49 AM
 */
public class ProductPlannerManager {
    private ProductPlannerDAO productPlannerDAO;

    public void setProductPlannerDAO(ProductPlannerDAO productPlannerDAO) {
		this.productPlannerDAO = productPlannerDAO;
	}

	public ProductPlannerDAO getProductPlannerDAO() {
        return productPlannerDAO;
    }
}
