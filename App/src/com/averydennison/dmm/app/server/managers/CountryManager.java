package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.server.dao.CountryDAO;
import com.averydennison.dmm.app.server.dao.LocationDAO;
import com.averydennison.dmm.app.server.persistence.Country;
import com.averydennison.dmm.app.server.persistence.Location;

/**
 * User: Mari
 * Date: Jan 07, 2011
 * Time: 4:47:02 PM
 */
public class CountryManager {
    private CountryDAO countryDAO;

    public void setCountryDAO(CountryDAO countryDAO) {
		this.countryDAO = countryDAO;
	}

	public CountryDAO getCountryDAO() {
        return countryDAO;
    }

    public List<Country> getAllActiveCountries() {
        return countryDAO.getAllActiveCountries();
    }
    
    public List<Country> getAllActiveCountryFlag() {
        return countryDAO.getAllActiveCountryFlag();
    }
}
