package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.ValidBuDTO;
import com.averydennison.dmm.app.server.dao.ValidBuDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.ValidBu;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 11:55:49 AM
 */
public class ValidBuManager {
	private ValidBuDAO validBuDAO;
	
    public List<ValidBu> getBus() {
        return validBuDAO.getBus();
    }

    public List<ValidBuDTO> getValidBuDTOs() {
        return ConverterFactory.convertBus(getBus());
    }
	public void setValidBuDAO(ValidBuDAO validBuDao) {
		this.validBuDAO = validBuDao;
	}
    
}
