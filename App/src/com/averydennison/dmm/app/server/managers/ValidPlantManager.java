package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.ValidPlantDTO;
import com.averydennison.dmm.app.server.dao.ValidPlantDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.ValidPlant;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 11:55:49 AM
 */
public class ValidPlantManager {
	private ValidPlantDAO validPlantDAO;
	
    public ValidPlantDAO getValidPlantDAO() {
		return validPlantDAO;
	}

	public void setValidPlantDAO(ValidPlantDAO validPlantDAO) {
		this.validPlantDAO = validPlantDAO;
	}

	public List<ValidPlant> getPlants() {
        return validPlantDAO.getPlants();
    }

    public List<ValidPlantDTO> getValidPlantDTOs() {
        return ConverterFactory.convertPlants(validPlantDAO.getPlants());
    }
}
