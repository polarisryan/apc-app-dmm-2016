package com.averydennison.dmm.app.server.managers;

import java.util.List;
import com.averydennison.dmm.app.client.models.ScenarioStatusDTO;
import com.averydennison.dmm.app.server.dao.ScenarioStatusDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.ScenarioStatus;

public class ScenarioStatusManager {
	 private ScenarioStatusDAO scenarioStatusDAO = getScenarioStatusDAO();

	    public void setScenarioStatusDAO(ScenarioStatusDAO scenarioStatusDAO) {
			this.scenarioStatusDAO = scenarioStatusDAO;
		}

		public ScenarioStatusDAO getScenarioStatusDAO() {
	        return scenarioStatusDAO;
	    }

	    public List<ScenarioStatus> getScenarioStatuses() {
	    	ScenarioStatusDAO dao = scenarioStatusDAO;
	        return dao.getScenarioStatuses();
	    }

	    public List<ScenarioStatusDTO> getScenarioStatusDTOs() {
	        return ConverterFactory.convertScenarioStatus(scenarioStatusDAO.getScenarioStatuses());
	    }
}
