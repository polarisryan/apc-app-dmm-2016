package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.averydennison.dmm.app.server.dao.KitComponentDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.KitComponent;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:47:03 AM
 */
public class KitComponentManager {
    private KitComponentDAO kitComponentDAO;

    public KitComponentDAO getKitComponentDAO() {
        return kitComponentDAO;
    }
    
    public void setKitComponentDAO(KitComponentDAO kitComponentDAO) {
		this.kitComponentDAO = kitComponentDAO;
	}

	public KitComponentDTO save(KitComponentDTO kitComponentDTO) {
        KitComponent dl = ConverterFactory.convert(kitComponentDTO);
        kitComponentDAO.attachDirty(dl);
        return ConverterFactory.convert(dl);
    }

}
