package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.server.dao.PromoPeriodCountryDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.PromoPeriodCountry;

public class PromoPeriodCountryManager {
    private PromoPeriodCountryDAO promoPeriodCountryDAO;

    public PromoPeriodCountryDAO getPromoPeriodCountryDAO() {
        return promoPeriodCountryDAO;
    }

    public List<PromoPeriodCountry> getPromoPeriodCountries() {
        return promoPeriodCountryDAO.getPromoPeriodsCountry();
    }

    public void setPromoPeriodCountryDAO(PromoPeriodCountryDAO promoPeriodCountryDAO) {
		this.promoPeriodCountryDAO = promoPeriodCountryDAO;
	}

	public List<PromoPeriodCountryDTO> getPromoPeriodCountryDTOs() {
        return ConverterFactory.convertPromoPeriodsCountry(promoPeriodCountryDAO.getPromoPeriodsCountry());
    }
}

