package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.server.dao.DisplayShipWaveDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayShipWave;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:45:15 AM
 */
public class DisplayShipWaveManager {
    private DisplayShipWaveDAO displayShipWaveDAO;

    public void setDisplayShipWaveDAO(DisplayShipWaveDAO displayShipWaveDAO) {
		this.displayShipWaveDAO = displayShipWaveDAO;
	}

	public DisplayShipWaveDAO getDisplayShipWaveDAO() {
        return displayShipWaveDAO;
    }

    public DisplayShipWaveDTO save(DisplayShipWaveDTO displayShipWaveDTO) {
        DisplayShipWave displayShipWave = ConverterFactory.convert(displayShipWaveDTO);
        displayShipWaveDAO.attachDirty(displayShipWave);
        return ConverterFactory.convert(displayShipWave);
    }

    public void delete(DisplayShipWaveDTO displayShipWaveDTO) {
        DisplayShipWave displayShipWave = ConverterFactory.convert(displayShipWaveDTO);
        displayShipWaveDAO.delete(displayShipWave);
    }

    public List<DisplayShipWaveDTO> findDisplayShipWavesByDisplay(DisplayDTO display) {
        Display d = ConverterFactory.convert(display);
        return ConverterFactory.convertDisplayShipWavesList(displayShipWaveDAO.findDisplayShipWavesByDisplay(d));
    }
}
