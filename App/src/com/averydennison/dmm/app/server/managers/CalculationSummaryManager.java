package com.averydennison.dmm.app.server.managers;

import com.averydennison.dmm.app.client.models.CalculationSummaryDTO;
import com.averydennison.dmm.app.client.models.CustomerMarketingDTO;
import com.averydennison.dmm.app.server.dao.CalculationSummaryDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.CalculationSummary;
import com.averydennison.dmm.app.server.persistence.CorrugateComponent;
import com.averydennison.dmm.app.server.persistence.CustomerMarketing;
import com.averydennison.dmm.app.server.persistence.DisplayCost;
import com.extjs.gxt.ui.client.widget.MessageBox;

public class CalculationSummaryManager {
	 private CalculationSummaryDAO calculationSummaryDAO;
	 
	    public CalculationSummaryDAO getCalculationSummaryDAO() {
	        return calculationSummaryDAO;
	    }
	    
	    public void setCalculationSummaryDAO(CalculationSummaryDAO calculationSummaryDAO) {
			this.calculationSummaryDAO = calculationSummaryDAO;
		}
	   
	    public CalculationSummaryDTO save(CalculationSummaryDTO calculationSummaryDTO) {
	    	System.out.println("CalculationSummaryManager:save starts");
			System.out.println("CalculationSummaryManager:save displayCostDTO..getDisplayScenarioId()= "+calculationSummaryDTO.getDisplayScenarioId());
			try{
			CalculationSummary calculationSummaryDirty = ConverterFactory.convert(calculationSummaryDTO);
	        Long curDisplayId= calculationSummaryDirty.getDisplayId();
			System.out.println("CalculationSummaryManager:save curDisplayId= "+curDisplayId);
	        Long curCalculationSummaryId=null;
	        CalculationSummary loadedCalculationSummary =calculationSummaryDAO.loadByDisplayId(curDisplayId);
	        if(loadedCalculationSummary!=null && loadedCalculationSummary.getCalculationSummaryId()!=null  ){
	        	curCalculationSummaryId=loadedCalculationSummary.getCalculationSummaryId();
	        	calculationSummaryDirty.setCalculationSummaryId(curCalculationSummaryId);
	        }
	        calculationSummaryDAO.attachDirty(calculationSummaryDirty);
	        CalculationSummary calculationSummary =calculationSummaryDAO.loadByDisplayId(curDisplayId);
			System.out.println("CalculationSummaryManager:save NULL displayCost ends  displayCost.getDisplayCostId():"+calculationSummary.getCalculationSummaryId());
			calculationSummaryDirty.setCalculationSummaryId(calculationSummary.getCalculationSummaryId());
			System.out.println("CalculationSummaryManager:save ends");
			return ConverterFactory.convert(calculationSummary);
			}catch(Exception ex){
	        	System.out.println("CalculationSummaryManager:save error "+ ex.toString());
	        	return null;
	        }
	    }
}
