package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.server.dao.StatusDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.Status;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:55:03 AM
 */
public class StatusManager {
	
    private StatusDAO statusDAO;
    
    public List<Status> getStatuses() {
        return statusDAO.getStatuses();
    }

    public List<StatusDTO> getStatusesDTOs() {
        return ConverterFactory.convertStatuses(statusDAO.getStatuses());
    }

    public void setStatusDAO(StatusDAO statusDAO) {
		this.statusDAO = statusDAO;
	}

	public StatusDAO getStatusDAO() {
        return statusDAO;
    }
}
