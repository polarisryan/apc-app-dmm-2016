package com.averydennison.dmm.app.server.managers;

import java.util.List;

import com.averydennison.dmm.app.client.models.ValidLocationDTO;
import com.averydennison.dmm.app.server.dao.ValidLocationDAO;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.persistence.ValidLocation;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 12:15:44 PM
 */
public class ValidLocationManager {
    private ValidLocationDAO validLocationDAO;
    
    public ValidLocationDAO getValidLocationDAO() {
		return validLocationDAO;
	}

	public void setValidLocationDAO(ValidLocationDAO validLocationDAO) {
		this.validLocationDAO = validLocationDAO;
	}

	public List<ValidLocation> getLocations() {
        return validLocationDAO.getLocations();
    }

    public List<ValidLocationDTO> getValidLocationDTOs() {
        return ConverterFactory.convertLocations(validLocationDAO.getLocations());
    }
}
