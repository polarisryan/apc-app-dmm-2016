package com.averydennison.dmm.app.server.managers;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.server.dao.DisplayDcDAO;
import com.averydennison.dmm.app.server.persistence.CustomerMarketing;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayCost;
import com.averydennison.dmm.app.server.persistence.DisplayDc;
import com.averydennison.dmm.app.server.persistence.DisplayLogistics;
import com.averydennison.dmm.app.server.persistence.DisplayPlanner;
import com.averydennison.dmm.app.server.persistence.DisplaySalesRep;
import com.averydennison.dmm.app.server.persistence.DisplayShipWave;
import com.averydennison.dmm.app.server.persistence.KitComponent;
import com.averydennison.dmm.app.server.persistence.Location;
import com.averydennison.dmm.app.server.persistence.Manufacturer;
import com.averydennison.dmm.app.server.persistence.PromoPeriod;
import com.averydennison.dmm.app.server.persistence.Structure;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel.Cell;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Header;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFReportManager {
	
	private DisplayManager displayManager;
	private StatusManager statusManager;
	private StructureManager structureManager;
	private PromoPeriodManager promoPeriodManager;
	private CustomerManager customerManager;
	private DisplaySalesRepManager displaySalesRepManager;
	private KitComponentManager kitComponentManager;
	private DisplayPlannerManager displayPlannerManager;
	private ValidPlannerManager validPlannerManager;
	private ValidPlantManager validPlantManager;
	private DisplayDcManager displayDcManager;
	private LocationManager locationManager;
	private CustomerMarketingManager customerMarketingManager;

	private DisplayCostManager displayCostManager;

	private DisplayLogisticsManager displayLogisticsManager;
	private DisplayShipWaveManager displayShipWaveManager;
	private ManufacturerManager manufacturerManager;

	private String dateFormat = "MM/dd/yyyy";
	private String filename ="output/dps-report.pdf";
	
	public PDFReportManager() throws FileNotFoundException, DocumentException{

	}
	
	private Font getFont(float size, int style){
		Font f = new Font();
		f.setSize(size);
		f.setStyle(style);
		return f;
	}
	
	public Document createPDF(String displayId, ByteArrayOutputStream boas) throws DocumentException{
		Document document = new Document(PageSize.LEGAL.rotate(),25,25,25,25);

        PdfWriter.getInstance(document,boas);
        document.open();
        document.add(getHeader());
        document.add(getMainTable(displayId));

        return document;
	}
	
	public Document createPDF(String displayId, FileOutputStream fos) throws FileNotFoundException, DocumentException{
		Document document = new Document(PageSize.LEGAL.rotate(),25,25,25,25);

        PdfWriter.getInstance(document,fos);
        document.open();
        document.add(getHeader());
        document.add(getMainTable(displayId));
        

        return document;
	}

	private PdfPTable getHeader() throws DocumentException{
		Font f = null;
		PdfPCell cell = null;
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date date = new Date();
		
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		table.setWidths(new int[]{33,33,33});
		f = getFont(8f, Font.NORMAL);
		cell = new PdfPCell(new Phrase("", f));
		cell.setBorder(PdfPCell.NO_BORDER);
		
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Display Production System", f));
		cell.setBorder(PdfPCell.NO_BORDER);
		cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		table.addCell(cell);
		
		f = getFont(5f, Font.NORMAL);
		cell = new PdfPCell(new Phrase("Run Date: " + dateFormat.format(date) , f));
		cell.setBorder(PdfPCell.NO_BORDER);
		cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("  ", f));
		cell.setBorder(PdfPCell.NO_BORDER);
		cell.setColspan(3);
		table.addCell(cell);
		table.completeRow();
		
		return table;
	}
	private PdfPTable getMainTable(String displayId) throws DocumentException{
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100f);
		table.setWidths(new int[]{70,30});
        table.addCell(this.getDisplayInfoTable(displayId));
        table.addCell(this.getCustomerMarketingTable(displayId));
        table.completeRow();
		return table;
	}
	
	private PdfPTable getDisplayInfoTable(String displayId) throws DocumentException{
		PdfPTable table = new PdfPTable(12);
		PdfPCell cell = null;
		Font f = null;
		table.setWidths(new int[]{10,10,10,10,10,10,5,5,10,10,10,10});
        table.setWidthPercentage(100f);
        
        f = getFont(5f,Font.BOLD);
        cell = new PdfPCell(new Phrase("Display Info", f));
        cell.setColspan(12);
        table.addCell(cell);
        table.completeRow();
        //Load Display Date
        Display display = displayManager.getDisplayDAO().findById(new Long(displayId).longValue());
        //Top Level SKU
        f = getFont(5f,Font.NORMAL);
        cell = new PdfPCell(new Phrase("Top Level SKU:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase(display.getSku(), f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        //Status
        cell = new PdfPCell(new Phrase("Status:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String status = statusManager.getStatusDAO().findById(display.getStatusId()).getStatusName();
        cell = new PdfPCell(new Phrase(status, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        //Structure
        cell = new PdfPCell(new Phrase("Structure:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        cell.setBorder(PdfPCell.NO_BORDER);
        String structure = "";
        Structure s = structureManager.getStructureDAO().findById(display.getStructureId());
        structure = s.getStructureName();
        cell = new PdfPCell(new Phrase(structure, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        //Qty
        cell = new PdfPCell(new Phrase("Qty:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(nullCheck(display.getQtyForecast()), f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        //Promo Period Name
        cell = new PdfPCell(new Phrase("Promo Period:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String promo = promoPeriodManager.getPromoPeriodDAO().findById(display.getPromoPeriodId()).getPromoPeriodName();
        cell = new PdfPCell(new Phrase(promo, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        //Go/No-Go
        cell = new PdfPCell(new Phrase("Go/No-Go:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        //String promoPeriod = formatDate(display.get);
        cell = new PdfPCell(new Phrase("", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        //Customer
        cell = new PdfPCell(new Phrase("Customer:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String customer = customerManager.getCustomerDAO().findById(display.getCustomerId()).getCustomerName();
        cell = new PdfPCell(new Phrase(customer, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        //Project Rep
        cell = new PdfPCell(new Phrase("Project Rep:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        DisplaySalesRep dsr = displaySalesRepManager.getDisplaySalesRepDAO().findById(display.getDisplaySalesRepId());
        String displayRep = dsr.getFirstName() + " " + dsr.getLastName();
        cell = new PdfPCell(new Phrase(displayRep, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        //Project Description
        cell = new PdfPCell(new Phrase("Desc:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(display.getDescription(), f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        //Blank Cell
        cell = new PdfPCell(new Phrase(" ", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        table.addCell(cell);
        
        //Promo year
        cell = new PdfPCell(new Phrase("Promo Yr:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String promoYear = "";
        if(display.getPromoYear()!=null) promoYear = display.getPromoYear().toString();
        cell = new PdfPCell(new Phrase(promoYear, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("", f));
        cell.setColspan(2);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        table.completeRow();
        cell = new PdfPCell(getComponentDetailInfo(displayId));
        cell.setColspan(12);
        table.addCell(cell);
        table.completeRow();
		return table;
	}

	private PdfPTable getCustomerMarketingTable(String displayId){
		PdfPTable table = new PdfPTable(4);
		Font f = null;
		table.setTotalWidth(30f);
        f = getFont(5f, Font.BOLD);
        PdfPCell cell = new PdfPCell(new Phrase("Customer Marketing & Cost Info", f));
        cell.setColspan(4);
        table.addCell(cell);
        
        f = getFont(5f, Font.NORMAL);
        
        CustomerMarketing c = customerMarketingManager.getCustomerMarketingDAO().findByDisplayId(new Long(displayId).longValue());
        Display d = displayManager.getDisplayDAO().findById(new Long(displayId).longValue());
        DisplayCost dc = displayCostManager.getDisplayCostDAO().findById(new Long(displayId).longValue());
        
        cell = new PdfPCell(new Phrase("Init Rend/Est Qt:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String renderFlg = "";
        if(c!=null) renderFlg = formatBoolean(c.isInitialRenderingFlg());
        cell = new PdfPCell(new Phrase(renderFlg, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("QSI#:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(nullCheck(d.getQsiDocNumber()), f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Cost Analysis Comp:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String priceAvailDate = "";
        if(c!=null)priceAvailDate = formatDate(c.getPriceAvailabilityDate());
        cell = new PdfPCell(new Phrase(priceAvailDate, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Struct Approve:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String structApprovedFlg = "";
        if(c!=null) structApprovedFlg = formatBoolean(c.isStructureApprovedFlg());
        cell = new PdfPCell(new Phrase(structApprovedFlg, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Sample to Cust:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String sampleDate = "";
        if(c!=null) sampleDate = formatDate(c.getSentSampleDate());
        cell = new PdfPCell(new Phrase(sampleDate, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Orders Received:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String orderFlg = "";
        if(c!=null) orderFlg = formatBoolean(c.isOrderInFlg());
        cell = new PdfPCell(new Phrase(orderFlg, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Price Admin:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String pricingAdminDate = "";
        if(c!=null) pricingAdminDate = formatDate(c.getPricingAdminDate());
        cell = new PdfPCell(new Phrase(pricingAdminDate, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase(" ", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(" ", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Artwork/Po Comp:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String finalArtworkDate = "";
        if(c!=null) finalArtworkDate = formatDate(c.getFinalArtworkDate());
        cell = new PdfPCell(new Phrase(finalArtworkDate, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Cost Ctr:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String costCenter = "";
        if(dc!=null) costCenter = dc.getCostCenter();
        cell = new PdfPCell(new Phrase(costCenter, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("PIM Complete:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String pimCompletedDate = "";
        if(c!=null) pimCompletedDate = formatDate(c.getPimCompletedDate());
        cell = new PdfPCell(new Phrase(pimCompletedDate, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("CM Project #:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String cmProjectNum = "";
/*        if(dc!=null) cmProjectNum = dc.getCmProjectNum();
*/        cell = new PdfPCell(new Phrase(cmProjectNum, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Conf Level:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String projectLevelPct = "";
        if(c!=null) projectLevelPct = nullCheck(c.getProjectLevelPct());
        cell = new PdfPCell(new Phrase(projectLevelPct, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("SSCID Project#:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String sscidProjectNum = "";
        if(dc!=null) sscidProjectNum = dc.getSscidProjectNum();
        cell = new PdfPCell(new Phrase(sscidProjectNum, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Comments:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String comments = "";
        if(c!=null) comments = c.getComments();
        cell = new PdfPCell(new Phrase(comments, f));
        cell.setColspan(3);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        table.completeRow();
        
        f = getFont(5f, Font.BOLD);
        cell = new PdfPCell(new Phrase("Display Logistics", f));
        cell.setColspan(4);
        table.addCell(cell);
        
        DisplayLogistics dl = displayLogisticsManager.getDisplayLogisticsDAO().findByDisplayId(new Long(displayId));
        
        f = getFont(5f, Font.NORMAL);
        cell = new PdfPCell(new Phrase("L x W x H (in):", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String dimen = "";
        String length = "";
        String width = "";
        String height = "";
        if(dl!=null){
        	if(dl.getDisplayLength()!=null) length = dl.getDisplayLength().toString();
        	if(dl.getDisplayWidth()!=null) width = dl.getDisplayWidth().toString();
        	if(dl.getDisplayHeight()!=null)height = dl.getDisplayHeight().toString();
        	dimen = length + " x " + width + " x " + height + " (in)";
        }

        cell = new PdfPCell(new Phrase(dimen, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Weight (lbs):", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String weight = "";
        if(dl!=null) weight = nullCheck(dl.getDisplayWidth());
        cell = new PdfPCell(new Phrase(weight, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Disp Per Layer:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String displayPerLayer = "";
        if(dl!=null) displayPerLayer = nullCheck(dl.getDisplayPerLayer());
        cell = new PdfPCell(new Phrase(displayPerLayer, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
 
        cell = new PdfPCell(new Phrase("# Pallets (Tot):", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String layerPerPallet = "";
        String totalPallet = "";
        Long displayPerPallet = null;
        Long total = null;
        if(dl!=null){ 
        	if(dl.getLayersPerPallet()!=null && dl.getDisplayPerLayer()!=null){
        		layerPerPallet = dl.getLayersPerPallet().toString();
        		displayPerPallet = dl.getLayersPerPallet() * dl.getDisplayPerLayer();
            	layerPerPallet = dl.getLayersPerPallet().toString();
            	if(d.getQtyForecast()!=null){
            		total = calcTotalPallet(displayPerPallet, d.getQtyForecast());
            	}
        	}
        }
        cell = new PdfPCell(new Phrase(nullCheck(total), f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
    
        cell = new PdfPCell(new Phrase("Layers per Pallet:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(nullCheck(layerPerPallet), f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Disp per Pallet:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(nullCheck(displayPerPallet), f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("# Trailers:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        String numTrailers = "";
        if(dl!=null){
        	if(dl.getPalletsPerTrailer()!=null||total!=null){
        		numTrailers = nullCheck(calcNumTrailers(dl.getPalletsPerTrailer(), total));
        	}	
        }
        cell = new PdfPCell(new Phrase(numTrailers, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
  
        cell = new PdfPCell(new Phrase("Serial Ship:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String serialShip = "";
        if(dl!=null) serialShip = formatBoolean(dl.isSerialShipFlg());
        cell = new PdfPCell(new Phrase(serialShip, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
 
        cell = new PdfPCell(new Phrase("Placecard/Spc Lbl:", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        String specialLabel = "";
        if(dl!=null) specialLabel = dl.getSpecialLabetRqt();
        cell = new PdfPCell(new Phrase(specialLabel, f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase(" ", f));
        cell.setColspan(4);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.completeRow();
        
        f = getFont(5f, Font.BOLD);
        cell = new PdfPCell(new Phrase("Shipping Wave", f));
        cell.setColspan(4);
        table.addCell(cell);
        
        f = getFont(5f, Font.BOLD);
        cell = new PdfPCell(new Phrase("#", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Must Arrive", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Qty", f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase(" ",f));
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        
        f = getFont(5f, Font.NORMAL);
        List<DisplayShipWave> shipWaves = displayShipWaveManager.getDisplayShipWaveDAO().findDisplayShipWaveByDisplayId(new Long(displayId));
        if(shipWaves!=null || shipWaves.size()!=0){
        	for(DisplayShipWave shipWave : shipWaves){
            	cell = new PdfPCell(new Phrase(nullCheck(shipWave.getShipWaveNum()), f));
            	cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(formatDate(shipWave.getMustArriveDate()), f));
                cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(nullCheck(shipWave.getQuantity()), f));
                cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase("", f));
                cell.setBorder(PdfPCell.NO_BORDER);
                table.addCell(cell);
                table.completeRow();
            	
            }
        }
        
        cell = new PdfPCell(new Phrase(" ",f));
        cell.setColspan(4);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        table.completeRow();
        
        
        Manufacturer m = null;
        String mfgName = "";
        if(d.getManufacturerId()!=null){
        	m = manufacturerManager.getManufacturerDAO().findById(d.getManufacturerId());
        }
        if(m!=null){
        	mfgName = nullCheck(m.getName());
        }
        f = getFont(5f, Font.BOLD);
        cell = new PdfPCell(new Phrase("Corrugate Mfg: " + mfgName , f));
        cell.setColspan(4);
        table.addCell(cell);
        table.completeRow();
        
        cell = new PdfPCell(new Phrase("", f));
        cell.setColspan(4);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        table.completeRow();
        
        return table;
	}
	private PdfPTable getDisplaySubDetail(String displayId) throws DocumentException{
		PdfPTable table = new PdfPTable(2);
		PdfPCell cell = new PdfPCell();
		Font f = null;
		f = getFont(5f,Font.NORMAL);
		table.setWidths(new int[]{30,70});
		
		Display d = displayManager.getDisplayDAO().findById(new Long(displayId).longValue());
		
		cell = new PdfPCell(new Phrase("Display Setup Type:", f));
		cell.setBorder(PdfPCell.NO_BORDER);
		table.addCell(cell);
		
		String setup = "";
		if (d.getDisplaySetupId().equals((Long.valueOf(1)))) setup="Kit";
        if (d.getDisplaySetupId().equals((Long.valueOf(2)))) setup="Finished Goods - New UPC";
        if (d.getDisplaySetupId().equals((Long.valueOf(3)))) setup="Finished Goods - New Production Version";
		cell = new PdfPCell(new Phrase("", f));
		cell.setBorder(PdfPCell.NO_BORDER);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Sku Mix Final:", f));
		cell.setBorder(PdfPCell.NO_BORDER);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(formatBoolean(d.isSkuMixFinalFlg()), f));
		cell.setBorder(PdfPCell.NO_BORDER);
		table.addCell(cell);
		
		return table;
		
	}
	private PdfPTable getComponentDetailInfo(String displayId) throws DocumentException{
		PdfPTable table = new PdfPTable(21);
		PdfPCell cell = new PdfPCell();
		Font f = null;
		
		table.setWidths(new int[]{10,40,50,20,5,5,5,10,10,10,10,30,20,10,10,10,10,10,10,10,10});
		table.setWidthPercentage(100f);
		
		f = getFont(5f,Font.BOLD);
        cell = new PdfPCell(new Phrase("Component Detail", f));
        cell.setColspan(21);
        table.addCell(cell);
        
        cell = new PdfPCell(getDisplaySubDetail(displayId));
        cell.setColspan(4);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Promo Ver", f));
    	cell.setRotation(90);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("i2 Forecast", f));
    	cell.setRotation(90);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("Source = PIM", f));
    	cell.setRotation(90);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("Ver Code Setup", f));
    	cell.setRotation(90);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("Kit Version Code", f));
    	cell.setRotation(90);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("PBC BOM", f));
    	cell.setRotation(90);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("PBC Version", f));
    	cell.setRotation(90);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("FG Consists of SKU", f));
    	cell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("FG Qty", f));
    	cell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("Ret Pk/In Ctn", f));
    	cell.setRowspan(2);
    	cell.setRotation(90);
    	cell.setHorizontalAlignment(PdfPCell.ALIGN_TOP);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("Qty Per Facing", f));
    	cell.setRowspan(2);
    	cell.setRotation(90);
    	cell.setHorizontalAlignment(PdfPCell.ALIGN_TOP);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("# Facings", f));
    	cell.setRowspan(2);
    	cell.setRotation(90);
    	cell.setHorizontalAlignment(PdfPCell.ALIGN_TOP);
    	cell.setRowspan(2);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("Qty Per Display", f));
    	cell.setRowspan(2);
    	cell.setRotation(90);
    	cell.setHorizontalAlignment(PdfPCell.ALIGN_TOP);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("Plant", f));
    	cell.setRowspan(2);
    	cell.setRotation(90);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("Planner", f));
    	cell.setRowspan(2);
    	cell.setRotation(90);
    	table.addCell(cell);   	
    	cell = new PdfPCell(new Phrase("Lead Time", f));
    	cell.setRowspan(2);
    	cell.setRotation(90);
    	cell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
    	table.addCell(cell); 	
    	cell = new PdfPCell(new Phrase("DC Inv Reqt", f));
    	cell.setRowspan(2);
    	cell.setRotation(90);
    	table.addCell(cell);
    	
        
        //Kit Componet Headers
    	cell = new PdfPCell(new Phrase("DC", f));
    	cell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("SKU", f));
    	cell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("Desc", f));
    	cell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);
    	table.addCell(cell);
    	cell = new PdfPCell(new Phrase("BU", f));
    	cell.setVerticalAlignment(PdfPCell.ALIGN_BOTTOM);
    	table.addCell(cell);
    	
        
    	f = getFont(5f,Font.NORMAL);
    	
    	//Kit Component Details
        List<KitComponent> kc = kitComponentManager.getKitComponentDAO().findByDisplayId(new Long(displayId).longValue());  
        List<DisplayDc> displayDcs = displayDcManager.getDisplayDcDAO().findDisplayDcByDisplayId(new Long(displayId).longValue());
        
        for(DisplayDc displayDc : displayDcs){
        	List<DisplayPlanner> displayPlanners = displayPlannerManager.getDisplayPlannerDAO().findByDcId(displayDc.getDisplayDcId());
        	if(displayPlanners!=null){
        		String dc = "";
                for(DisplayPlanner dp : displayPlanners){
                    KitComponent k = kitComponentManager.getKitComponentDAO().findById(dp.getKitComponentId());
                    Location l = locationManager.getLocationsDAO().findById(displayDc.getLocationId());
                    //DC
                    if(!l.getLocationCode().equals(dc)){
                    	dc = l.getLocationCode();
                    	cell = new PdfPCell(new Phrase(dc, f));
                    }else{
                    	cell = new PdfPCell(new Phrase(" ", f));
                    }
                    setCompBorder(cell);
                    table.addCell(cell);
                    //SKU
                    cell = new PdfPCell(new Phrase(k.getSku(), f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //Desc
                    cell = new PdfPCell(new Phrase(k.getProductName(), f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //BU
                    cell = new PdfPCell(new Phrase(k.getBuDesc(), f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //Promo Ver
                    cell = new PdfPCell(new Phrase(formatBoolean(k.isPromoFlg()), f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //i2 Forcast
                    cell = new PdfPCell(new Phrase(formatBoolean(k.isI2ForecastFlg()), f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //Source = PIM
                    cell = new PdfPCell(new Phrase(formatBoolean(k.isSourcePimFlg()), f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //Ver Code Setup
                    String verSetup = "";
                    if(k.getBreakCase()!=null) verSetup = k.getBreakCase();
                    String verCode = "";
                    if(verSetup.equals("Break Case")){
                        verCode = "BC";
                    }else if (verSetup.equals("Plant Break Case")){
                        verCode = "PBC";
                    }else if (verSetup.equals("Production Version")){
                        verCode = "PV";
                    }
                    cell = new PdfPCell(new Phrase(verCode, f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //Kit Version Code
                    cell = new PdfPCell(new Phrase(k.getVersionNo(), f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //PBC Bom
                    cell = new PdfPCell(new Phrase(formatBoolean(k.isPlantBreakCaseFlg()), f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //PBC Version
                    cell = new PdfPCell(new Phrase(k.getPbcVersion(), f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //FG Consists of SKU
                    cell = new PdfPCell(new Phrase(k.getFinishedGoodsSku(), f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //FG Qty
                    String fgQty = "";
                    if(k.getFinishedGoodsQty()!=null) fgQty = k.getFinishedGoodsQty().toString();
                    cell = new PdfPCell(new Phrase(fgQty, f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //Ret Pk/IN Ctn
                    String casePack = "";
                    if(k.getRegCasePack()!=null) casePack = k.getRegCasePack().toString();
                    cell = new PdfPCell(new Phrase(casePack, f));
                    setCompBorder(cell);
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                    table.addCell(cell);
                    //Qty Per Facing
                    Long qtyPerFacing = 0l;
                    if(k.getQtyPerFacing()!=null) qtyPerFacing = k.getQtyPerFacing();
                    cell = new PdfPCell(new Phrase(qtyPerFacing.toString(), f));
                    setCompBorder(cell);
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                    table.addCell(cell);
                    //# Facing
                    Long numFacing = 0l;
                    if(k.getNumberFacings()!=null) {
                        numFacing = k.getNumberFacings();
                    }
                    cell = new PdfPCell(new Phrase(numFacing.toString(), f));
                    setCompBorder(cell);
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                    table.addCell(cell);
                    //Qty Per Display (QtyPerFacing * # Facing)
                    Long qtyPerDisplay = (numFacing * qtyPerFacing);
                    cell = new PdfPCell(new Phrase(qtyPerDisplay.toString(), f));
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                    setCompBorder(cell);
                    table.addCell(cell);
                    //Plant
                    String plant = dp.getPlantCode();           
                    cell = new PdfPCell(new Phrase(plant, f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //Planner
                    String planner = dp.getPlannerCode();
                    cell = new PdfPCell(new Phrase(planner, f));
                    setCompBorder(cell);
                    table.addCell(cell);
                    //Lead Time (Display Planner)
                    cell = new PdfPCell(new Phrase(dp.getLeadTime(), f));
                    setCompBorder(cell);
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                    table.addCell(cell);
                    //DC Inv Reqt (Calc) DC qty * Qty Per Display 
                    Long invReq = displayDc.getQuantity() * qtyPerDisplay;
                    cell = new PdfPCell(new Phrase(invReq.toString(), f));
                    setCompBorder(cell);
                    cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                    table.addCell(cell);            
                }
        	}else{
        		 cell = new PdfPCell(new Phrase(" ", f));
                 setCompBorder(cell);
                 cell.setColspan(21);
                 table.addCell(cell);  
                 table.completeRow();
        	}
        }
        table.completeRow();
        
		return table;
	}
	private PdfPCell setCompBorder(PdfPCell cell){
		
		cell.setBorder(PdfPCell.LEFT);
		cell.setBorder(PdfPCell.RIGHT);
		return cell;
	}
	private String formatDate(Date date){
		String str = "";
		if(date != null){
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			str = formatter.format(date);
		}
        return str;
		
	}
	private String formatBoolean(Boolean b){
		String ret = "";
		if(b!=null){
			if(b==false){
				ret = "N";
			}else{
				ret = "Y";
			}
		}
		return ret;
	}
	private Long calcTotalPallet(Long dispPerPallet, Long qtyForecast){
		Long totPallets = null;
		if (dispPerPallet > 0 && qtyForecast != null) {
            double d = new Double(qtyForecast).doubleValue() / new Double(dispPerPallet).doubleValue();
            totPallets = new Double(d % 1 > 0 ? d + 1 : d).longValue();
        }
		return totPallets;
	}
	
	private Long calcNumTrailers(Long palletsPerTrailer, Long totPallets){
		Long numTrailers = null;
		String sNumTrailers = "";
		if (palletsPerTrailer != null && palletsPerTrailer > 0) {
            double d = new Double(totPallets).doubleValue() / new Double(palletsPerTrailer).doubleValue();
            numTrailers = new Double(d % 1 > 0 ? d + 1 : d).longValue();
        }
		return numTrailers;
	}
	private String nullCheck(Float f){
		String s = "";
		if(f!=null){
			s = f.toString();
		}
		return s;
	}
	private String nullCheck(Long l){
		String s = "";
		if(l!=null){
			s = l.toString();
		}
		return s;
	}
	private String nullCheck(Integer i){
		String s = "";
			if(i!=null){
				s = i.toString();
			}
		return s;
	}
	private String nullCheck(Short sh){
		String s = "";
		if(sh!=null){
			s = sh.toString();
		}
		return s;
	}
	private String nullCheck(String st){
		String s = "";
		if(st!=null){
			s = st.toString();
		}
		return s;
	}
	
	
	/*
	 * For readability 
	 */
	public void setDisplayManager(DisplayManager displayManager) {
		this.displayManager = displayManager;
	}
	public void setStatusManager(StatusManager statusManager) {
		this.statusManager = statusManager;
	}
	public void setStructureManager(StructureManager structureManager) {
		this.structureManager = structureManager;
	}
	public void setPromoPeriodManager(PromoPeriodManager promoPeriodManager) {
		this.promoPeriodManager = promoPeriodManager;
	}
	public void setCustomerManager(CustomerManager customerManager) {
		this.customerManager = customerManager;
	}	
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	public void setDisplaySalesRepManager(
			DisplaySalesRepManager displaySalesRepManager) {
		this.displaySalesRepManager = displaySalesRepManager;
	}
	public void setKitComponentManager(KitComponentManager kitComponentManager) {
		this.kitComponentManager = kitComponentManager;
	}
	public void setDisplayPlannerManager(DisplayPlannerManager displayPlannerManager) {
		this.displayPlannerManager = displayPlannerManager;
	}
	public void setValidPlannerManager(ValidPlannerManager validPlannerManager) {
		this.validPlannerManager = validPlannerManager;
	}
	public void setValidPlantManager(ValidPlantManager validPlantManager) {
		this.validPlantManager = validPlantManager;
	}
	public void setDisplayDcManager(DisplayDcManager displayDcManager) {
		this.displayDcManager = displayDcManager;
	}
	public void setLocationManager(LocationManager locationManager) {
		this.locationManager = locationManager;
	}
	public void setDisplayLogisticsManager(
			DisplayLogisticsManager displayLogisticsManager) {
		this.displayLogisticsManager = displayLogisticsManager;
	}
	public void setDisplayCostManager(DisplayCostManager displayCostManager) {
		this.displayCostManager = displayCostManager;
	}
	public void setCustomerMarketingManager(
			CustomerMarketingManager customerMarketingManager) {
		this.customerMarketingManager = customerMarketingManager;
	}
	public void setDisplayShipWaveManager(
			DisplayShipWaveManager displayShipWaveManager) {
		this.displayShipWaveManager = displayShipWaveManager;
	}
	public void setManufacturerManager(ManufacturerManager manufacturerManager) {
		this.manufacturerManager = manufacturerManager;
	}
}
