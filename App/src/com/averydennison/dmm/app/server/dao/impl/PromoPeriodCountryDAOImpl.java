package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.averydennison.dmm.app.server.dao.PromoPeriodCountryDAO;
import com.averydennison.dmm.app.server.persistence.PromoPeriodCountry;


public class PromoPeriodCountryDAOImpl extends HibernateDaoSupport implements PromoPeriodCountryDAO {
    private static final Log log = LogFactory.getLog(PromoPeriodCountryDAOImpl.class);
	@Override
	public PromoPeriodCountry findById(Long id) {
		log.debug("getting Customer instance with id: " + id);
        try {
        	PromoPeriodCountry instance = (PromoPeriodCountry) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.PromoPeriodCountry",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

	@Override
	public List<PromoPeriodCountry> getPromoPeriodsCountry() {
		log.debug("getting PromoPeriodCountry List");
        Session s = null;
        try {
            Query q = getSession().getNamedQuery("promoPeriodCountryDetail");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<PromoPeriodCountry>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

}
