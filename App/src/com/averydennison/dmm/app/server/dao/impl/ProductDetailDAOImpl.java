package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import com.averydennison.dmm.app.server.dao.ProductDetailDAO;
import com.averydennison.dmm.app.server.persistence.ProductDetail;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 9:33:07 AM
 */
public class ProductDetailDAOImpl extends HibernateDaoSupport implements ProductDetailDAO {
    private static final Log log = LogFactory.getLog(ProductDetailDAOImpl.class);

    public void persist(ProductDetail transientInstance) {
        log.debug("persisting ProductDetail instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(ProductDetail instance) {
        log.debug("attaching dirty ProductDetail instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(ProductDetail instance) {
        log.debug("attaching clean ProductDetail instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(ProductDetail persistentInstance) {
        log.debug("deleting ProductDetail instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public ProductDetail merge(ProductDetail detachedInstance) {
        log.debug("merging ProductDetail instance");
        try {
            ProductDetail result = (ProductDetail) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public ProductDetail findById(Long id) {
        log.debug("getting ProductDetail instance with id: " + id);
        if(id==null) return null;
        try {
            ProductDetail instance = (ProductDetail) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.ProductDetail",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<ProductDetail> findBySku(String sku) {
        if (sku == null) return null;
        log.debug("getting Product Detail instance by sku: " + sku);
        System.out.println("getting Product Detail instance by sku: " + sku);
        try {
        	sku  = sku.replaceAll("\\*", "%");
            Query q = getSession().getNamedQuery("productDetailBySku");
            q.setString(0, "%" + sku + "%");
            q.setMaxResults(200);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            if (q.list().size() == 0) return null;
            return (List<ProductDetail>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    /*
	int elementsPerBlock = 10;
	int page = 2;
	return  getSession().createQuery("from SomeItems order by id asc")
	            .setFirstResult(elementsPerBlock * (page-1) + 1 )
	            .setMaxResults(elementsPerBlock)
	            .list(); 

    *    
    */
}
