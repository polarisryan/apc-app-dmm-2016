package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.CountryDAO;
import com.averydennison.dmm.app.server.persistence.Country;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author Mari
 * @see com.averydennison.dmm.app.server.persistence.Vendor
 */
public class CountryDAOImpl extends HibernateDaoSupport implements CountryDAO {

    private static final Log log = LogFactory.getLog(CountryDAOImpl.class);

    public void persist(Country transientInstance) {
        log.debug("persisting Country instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(Country instance) {
        log.debug("attaching dirty Country instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(Country instance) {
        log.debug("attaching clean Country instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(Country persistentInstance) {
        log.debug("deleting Country instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public Country merge(Country detachedInstance) {
        log.debug("merging Country instance");
        try {
            Country result = (Country) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public Country findById(Long id) {
        log.debug("getting Country instance with id: " + id);
        try {
            Country instance = (Country) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.Country",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    /*public List<Country> getAllActiveCountries() {
        log.debug("getting all active Country");
        try {
            Query q = getSession().getNamedQuery("getAllActiveCountries");
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            return (List<Country>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }*/
    
    public List<Country> getAllActiveCountries() {
        log.debug("getting all active Country");
        try {
            Query q = getSession().getNamedQuery("getAllActiveCountries");
            if (q == null) {
                log.debug("get successful, no instaces found");
                return null;
            } else {
                log.debug("get successful, instaces found");
            }
            if(q.list().size()==0){
            	 log.debug("get successful, empty instaces found");
            	 return null;
            }
            return (List<Country>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<Country> getAllActiveCountryFlag() {
        log.debug("getting all getAllActiveCountryFlag ");
        try {
            Query q = getSession().getNamedQuery("getAllActiveCountryFlag");
            if (q == null) {
                log.debug("get successful, no instaces found");
                return null;
            } else {
                log.debug("get successful, instaces found");
            }
            if(q.list().size()==0){
            	 log.debug("get successful, empty instaces found");
            	 return null;
            }
            return (List<Country>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
