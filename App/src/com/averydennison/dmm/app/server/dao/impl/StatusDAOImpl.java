package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.StatusDAO;
import com.averydennison.dmm.app.server.persistence.Status;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.Status
 */
public class StatusDAOImpl extends HibernateDaoSupport implements StatusDAO {

    private static final Log log = LogFactory.getLog(StatusDAOImpl.class);

    public void persist(Status transientInstance) {
        log.debug("persisting Status instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(Status instance) {
        log.debug("attaching dirty Status instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(Status instance) {
        log.debug("attaching clean Status instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(Status persistentInstance) {
        log.debug("deleting Status instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public Status merge(Status detachedInstance) {
        log.debug("merging Status instance");
        try {
            Status result = (Status) getHibernateTemplate().merge(
                    detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public Status findById(Long id) {
        log.debug("getting Status instance with id: " + id);
        try {
            Status instance = (Status) getHibernateTemplate().get(
                    "com.averydennison.dmm.app.server.persistence.Status", id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<Status> getStatuses() {
        log.debug("getting Statuses");
        Session s = null;
        try {
            Query q = getSession().getNamedQuery("statuses");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<Status>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
