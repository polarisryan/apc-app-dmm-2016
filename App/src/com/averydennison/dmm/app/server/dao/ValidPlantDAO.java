package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.ValidPlant;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 9:31:19 AM
 */
public interface ValidPlantDAO {
    public void persist(ValidPlant transientInstance);

    public void attachDirty(ValidPlant instance);

    public void attachClean(ValidPlant instance);

    public void delete(ValidPlant persistentInstance);

    public ValidPlant merge(ValidPlant detachedInstance);

    public ValidPlant findById(Long id);
    
    public List<ValidPlant> getPlants();
}
