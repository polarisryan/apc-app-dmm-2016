package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.ValidBu;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 9:31:19 AM
 */
public interface ValidBuDAO {
    public void persist(ValidBu transientInstance);

    public void attachDirty(ValidBu instance);

    public void attachClean(ValidBu instance);

    public void delete(ValidBu persistentInstance);

    public ValidBu merge(ValidBu detachedInstance);

    public ValidBu findById(Long id);
    
    public List<ValidBu> getBus();
}
