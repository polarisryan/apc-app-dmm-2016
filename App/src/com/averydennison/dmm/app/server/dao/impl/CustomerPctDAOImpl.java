package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.averydennison.dmm.app.server.dao.CustomerPctDAO;
import com.averydennison.dmm.app.server.persistence.CustomerPct;



public class CustomerPctDAOImpl extends HibernateDaoSupport implements CustomerPctDAO {

	 private static final Log log = LogFactory.getLog(CustomerPctDAOImpl.class);

	    public void persist(CustomerPct transientInstance) {
	        log.debug("persisting CustomerPct instance");
	        try {
	            getHibernateTemplate().persist(transientInstance);
	            log.debug("persist successful");
	        } catch (RuntimeException re) {
	            log.error("persist failed", re);
	            throw re;
	        }
	    }

	    public void attachDirty(CustomerPct instance) {
	        log.debug("attaching dirty CustomerPct instance getCustomerPctId : " + instance.getCustomerId());
	        try {
	            getHibernateTemplate().saveOrUpdate(instance);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	    }

	    public void attachClean(CustomerPct instance) {
	        log.debug("attaching clean CustomerPct instance");
	        try {
	            getHibernateTemplate().lock(instance, LockMode.NONE);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	    }

	    public void delete(CustomerPct persistentInstance) {
	        log.debug("deleting CustomerPct instance");
	        try {
	            getHibernateTemplate().delete(persistentInstance);
	            log.debug("delete successful");
	        } catch (RuntimeException re) {
	            log.error("delete failed", re);
	            throw re;
	        }
	    }

	    public CustomerPct merge(CustomerPct detachedInstance) {
	        log.debug("merging CustomerPct instance");
	        try {
	        	CustomerPct result = (CustomerPct) getHibernateTemplate().merge(detachedInstance);
	            log.debug("merge successful");
	            return result;
	        } catch (RuntimeException re) {
	            log.error("merge failed", re);
	            throw re;
	        }
	    }

	    public CustomerPct findById(Long id) {
	        log.debug("getting CustomerPct instance with id: " + id);
	        if (id == null) return null;
	        try {
	        	CustomerPct instance = (CustomerPct) getHibernateTemplate()
	                    .get(
	                            "com.averydennison.dmm.app.server.persistence.CustomerPct",
	                            id);
	            if (instance == null) {
	                log.debug("<<debug>>get successful, no instance found");
	            } else {
	                log.debug("<<debug>>get successful, instance found");
	            }
	            return instance;
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }
	    
	    public List<CustomerPct> findByCustomerId(Long customerId) {
	    	log.debug("getting CustomerPct instance by customerId id: " + customerId);
	        if (customerId == null) return null;
	        try {
	            Query q = getSession().getNamedQuery("customerPctByCustomerId");
	            log.debug("CustomerPct get query used :" + q.getQueryString());
	            q.setLong(0, customerId);
	            if (q == null) {
	                log.debug("CustomerPct get successful, no instances found");
	            } else {
	                log.debug("CustomerPct get successful, instances found");
	            }
	            return (List<CustomerPct>) q.list();
	        } catch (RuntimeException re) {
	            log.error("CustomerPct get failed", re);
	            throw re;
	        }
	    }
	    
	    
	  
	}

