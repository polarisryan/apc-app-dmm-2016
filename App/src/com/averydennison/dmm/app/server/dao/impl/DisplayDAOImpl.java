package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.averydennison.dmm.app.client.models.BooleanDTO;
import com.averydennison.dmm.app.server.dao.DisplayDAO;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayForMassUpdate;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:26:57 AM
 */
public class DisplayDAOImpl extends HibernateDaoSupport implements DisplayDAO {
    private static final Log log = LogFactory.getLog(DisplayDAOImpl.class);

    public void persist(Display transientInstance) {
        log.debug("persisting Display instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(Display instance) {
        log.debug("attaching dirty Display instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
            
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(Display instance) {
        log.debug("attaching clean Display instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(Display persistentInstance) {
        log.debug("deleting Display instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public Display merge(Display detachedInstance) {
        log.debug("merging Display instance");
        try {
            Display result = (Display) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public Display findById(Long id) {
        log.debug("getting Display instance with id: " + id);
        try {
            Display instance = (Display) getSession().get("com.averydennison.dmm.app.server.persistence.Display", id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    
    
    public List<DisplayForMassUpdate> getSelectedDisplaysForMassUpdate(List<Long> parameters) {
        log.debug("getting selected Displays only mass update");
        try {
           Query q = getSession().getNamedQuery("selectedDisplaysForMassUpdate");
           q.setResultTransformer(Transformers.aliasToBean(DisplayForMassUpdate.class));
           q.setParameterList("displaySelectedIds", parameters,Hibernate.LONG);
        	if (q == null) {
                log.debug("get selected displays for mass update only, successful but no instances found");
            } else {
                log.debug("get selected displays for mass update only,  successful, instances found");
            }
           return (  q.list()) ;
        } catch (RuntimeException re) {
            log.error("get selected display for mass update only failed", re);
            throw re;
        }
    }
    
    public List<Display> getAllDisplays() {
        log.debug("getting Displays");
        try {
            Query q = getSession().getNamedQuery("displays");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<Display>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

	public BooleanDTO isDuplicateSKU(String sku) {
		 if (sku == null) return null;
		 log.debug("looking for duplicate SKU " + sku);
	        try {
	            SQLQuery q = (SQLQuery) getSession().getNamedQuery("isDuplicateSKU");
	            q.setResultTransformer(Transformers.aliasToBean(BooleanDTO.class));
	            q.setString(0, sku);
	            if (q == null) {
	                log.debug("get successful, no instaces found");
	            } else {
	                log.debug("get successful, instances found");
	            }
	            
	            return (BooleanDTO) q.list().get(0);
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	}

    public void callDisplayDeleteStoredProcedure(Long displayId, String username) {
        if( displayId == null) return;
        log.debug("call delete_display(" + displayId + ", " + username + ")");
        Session s = this.getSession();//wrap with transactions here or on the method itself
        String sql = "call delete_display(?, ?)";
        SQLQuery sqlQuery = s.createSQLQuery(sql);
        sqlQuery.setLong(0, displayId);
        sqlQuery.setString(1, username);
        sqlQuery.executeUpdate();
        s.close();
    }
    
	public void updateGoNoGo(Long displayId) {
		if (displayId == null) return;
       log.debug("call update go no go (" + displayId + ")");
        try {
            SQLQuery q = (SQLQuery) getSession().getNamedQuery("updateGoNoGo");
            q.setLong(0, displayId);
            q.executeUpdate();
            
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}
}
