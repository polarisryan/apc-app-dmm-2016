package com.averydennison.dmm.app.server.dao.impl;


import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.averydennison.dmm.app.server.dao.CalculationSummaryDAO;
import com.averydennison.dmm.app.server.persistence.CalculationSummary;


public class CalculationSummaryDAOImpl  extends HibernateDaoSupport implements CalculationSummaryDAO  {
	 private static final Log log = LogFactory.getLog(CalculationSummaryDAOImpl.class);

	@Override
	public void attachClean(CalculationSummary instance) {
		log.debug("attaching clean CorrugateComponent instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }		
	}

	@Override
	public void attachDirty(CalculationSummary instance) {
		 try {
	            getHibernateTemplate().saveOrUpdate(instance);
	          //  log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attachDirty(CorrugateComponent instance) attach failed", re);
	            throw re;
	        }
		
	}

	@Override
	public void delete(CalculationSummary instance) {
		 log.debug("deleting CorrugateComponent instance");
	        try {
	            getHibernateTemplate().delete(instance);
	            log.debug("delete successful");
	        } catch (RuntimeException re) {
	            log.error("CorrugateComponent instance delete failed", re);
	            throw re;
	        }
	}

	/*@Override
	public CalculationSummary findById(Long displayId) {
		 if (displayId == null) return null;
	        log.debug("getting CorrugateComponent instance by display id: " + displayId);
	     
	        try {
	            CalculationSummary calculationSummary = (CalculationSummary)getSession().load(CalculationSummary.class, displayId);
	            if (calculationSummary == null) {
	                log.debug("get successful, no instances found");
	            } else {
	                log.debug("get successful, instances found");
	            }
	            return calculationSummary;
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	}*/
	
	public CalculationSummary loadByDisplayId(Long displayId) {
        if (displayId == null) return null;
        log.debug("loading DisplayCost instance by displayScenarioId id: " + displayId);
        try {
            Query q = getSession().getNamedQuery("calculationSummaryByDisplayId");
            q.setLong(0, displayId);
            if (q == null) {
                log.debug("loading DisplayCost get successful, no instances found");
            } else {
                log.debug("loading DisplayCost get successful, instances found");
            }
            if(q.list()!=null){
            	if(q.list().size()>0){
            		log.debug("loading DisplayCost get successful, instances returned");
            		log.debug("loading DisplayCost get successful, instances id " + ((CalculationSummary) q.list().get(0)).getCalculationSummaryId());

            		return (CalculationSummary) q.list().get(0);
            	}
            }
    		log.debug("loading DisplayCost null instance returned");
            return null;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
	
	@Override
	public CalculationSummary merge(CalculationSummary detachedInstance) {
		 log.debug("merging CorrugateComponent instance");
	        try {
	        	CalculationSummary result = (CalculationSummary) getHibernateTemplate().merge(detachedInstance);
	            log.debug("merge successful");
	            return result;
	        } catch (RuntimeException re) {
	            log.error("merge failed", re);
	            throw re;
	        }
	}

	@Override
	public void persist(CalculationSummary transientInstance) {
		 try {
	            getHibernateTemplate().persist(transientInstance);
	            log.debug("persist successful");
	        } catch (RuntimeException re) {
	            log.error("persist(CorrugateComponent transientInstance) persist failed", re);
	            throw re;
	        }
	}
}
