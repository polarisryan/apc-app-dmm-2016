package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.averydennison.dmm.app.server.dao.DisplayCostCommentDAO;
import com.averydennison.dmm.app.server.persistence.DisplayCostComment;

public class DisplayCostCommentDAOImpl extends HibernateDaoSupport implements DisplayCostCommentDAO{
	private static final Log log = LogFactory.getLog(DisplayCostCommentDAOImpl.class);
	@Override
	public void attachClean(DisplayCostComment instance) {
		  log.debug("attaching clean DisplayCostComment instance");
	        try {
	            getHibernateTemplate().lock(instance, LockMode.NONE);
	            log.debug("attach DisplayCostComment successful");
	        } catch (RuntimeException re) {
	            log.error("attach DisplayCostComment failed", re);
	            throw re;
	        }
	}

	@Override
	public void attachDirty(DisplayCostComment instance) {
		  log.debug("attaching dirty DisplayCostComment instance scenarioId="+ instance.getDisplayScenarioId());
	        try {
	            getHibernateTemplate().saveOrUpdate(instance);
	            log.debug("attach DisplayCostComment successful");
	        } catch (RuntimeException re) {
	            log.error("attach DisplayCostComment failed", re);
	            throw re;
	        }
	}

	@Override
	public void delete(DisplayCostComment persistentInstance) {
		   log.debug("deleting DisplayCostComment instance");
	        try {
	            getHibernateTemplate().delete(persistentInstance);
	            log.debug("delete DisplayCostComment successful");
	        } catch (RuntimeException re) {
	            log.error("delete DisplayCostComment failed", re);
	            throw re;
	        }		
	}

		@Override
	public List<DisplayCostComment> findByDisplayScenarioId(Long displayScenarioId) {
		log.debug("getting DisplayCostComment instance by findByDisplayScenarioId(displayScenarioId)="+ displayScenarioId);
        if (displayScenarioId == null) return null;
        try {
            Query q = getSession().getNamedQuery("displayCostComment");
            q.setLong(0, displayScenarioId);
            log.debug("get DisplayCostComment =" +q.getQueryString());
            log.debug("get displayScenarioId =" +displayScenarioId);
            if (q == null) {
                log.debug("get DisplayCostComment successful, no instances found");
            } else {
                log.debug("get DisplayCostComment successful, instances found");
            }
            return (List<DisplayCostComment>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

	
	@Override
	public DisplayCostComment findById(Long id) {
		log.debug("getting DisplayCost instance with id: " + id);
        try {
        	DisplayCostComment instance = (DisplayCostComment) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.DisplayCostComment",
                            id);
            if (instance == null) {
                log.debug("get DisplayCostComment successful, no instance found");
            } else {
                log.debug("get DisplayCostComment successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get DisplayCostComment failed", re);
            throw re;
        }
	}

	
	@Override
	public DisplayCostComment merge(DisplayCostComment detachedInstance) {
		log.debug("merging DisplayCostComment instance");
        try {
        	DisplayCostComment result = (DisplayCostComment) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge DisplayCostComment successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge DisplayCostComment failed", re);
            throw re;
        }
	}

	@Override
	public void persist(DisplayCostComment transientInstance) {
		log.debug("persisting DisplayCostComment instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist DisplayCostComment successful");
        } catch (RuntimeException re) {
            log.error("persist DisplayCostComment failed", re);
            throw re;
        }
	}
}
