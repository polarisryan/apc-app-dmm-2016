package com.averydennison.dmm.app.server.dao.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 9:49:05 AM
 */
public enum DataType {
    StringType("S", "java.lang.String", java.sql.Types.VARCHAR),
    IntegerType("I", "java.lang.Integer", java.sql.Types.INTEGER),
    LongType("L", "java.lang.Long", java.sql.Types.BIGINT),
    DoubleType("D", "java.lang.Double", java.sql.Types.DOUBLE),
    FloatType("F", "java.lang.Float", java.sql.Types.DOUBLE),
    TimeStampType("TS", "java.sql.Timestamp", java.sql.Types.TIMESTAMP, "MM-dd-yyyy"),
    DateType("TD", "java.util.Date", java.sql.Types.DATE, "MM-dd-yyyy"),
    ObjectType("O", "java.lang.Object", java.sql.Types.JAVA_OBJECT),
    BooleanType("B", "java.lang.Boolean", java.sql.Types.BOOLEAN);


    private String name;
    private String defaultFormat = null;
    private String javaType;
    private int javaSqlType;

    private DataType(String name, String javaType, int sqlType) {
        setName(name);
        setJavaType(javaType);
        setJavaSqlType(sqlType);

    }

    private DataType(String name, String javaType, int sqlType, String defaultFormat) {
        setName(name);
        setJavaType(javaType);
        setJavaSqlType(sqlType);
        setDefaultFormat(defaultFormat);
    }

    /**
     * get value of attribute javaSqlType
     *
     * @return Returns the javaSqlType.
     */
    public int getJavaSqlType() {
        return javaSqlType;
    }

    /**
     * Set value for attribute javaSqlType
     *
     * @param javaSqlType The javaSqlType to set.
     */
    public void setJavaSqlType(int javaSqlType) {
        this.javaSqlType = javaSqlType;
    }

    /**
     * get $param.name$
     *
     * @return
     */
    public String getDefaultFormat() {
        return defaultFormat;
    }

    private void setDefaultFormat(String defaultFormat) {
        this.defaultFormat = defaultFormat;
    }

    /**
     * get $param.name$
     *
     * @return
     */
    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    /**
     * get $param.name$
     *
     * @return
     */
    public String getJavaType() {
        return javaType;
    }

    /**
     * set value for attribute
     *
     * @param javaType
     */
    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    /**
     * get $param.name$
     *
     * @param name
     * @return
     */
    public static DataType getDataTypeByName(String name) {
        if (name == null) {
            return null;
        }
        if (name.equals(StringType.getName())) {
            return StringType;
        } else if (name.equals(FloatType.getName())) {
            return FloatType;
        } else if (name.equals(IntegerType.getName())) {
            return IntegerType;
        } else if (name.equals(LongType.getName())) {
            return LongType;
        } else if (name.equals(DoubleType.getName())) {
            return DoubleType;
        } else if (name.equals(TimeStampType.getName())) {
            return TimeStampType;
        } else if (name.equals(DateType.getName())) {
            return DateType;
        } else if (name.equals(ObjectType.getName())) {
            return ObjectType;
        } else if (name.equals(BooleanType.getName())) {
            return BooleanType;
        }

        return ObjectType;
    }

    public static DataType getDataTypeEnum(String javaType) {
        if (StringType.javaType.endsWith(javaType)) {

            return StringType;
        } else if (FloatType.javaType.endsWith(javaType)) {
            return FloatType;
        } else if (IntegerType.javaType.endsWith(javaType)) {
            return IntegerType;
        } else if (LongType.javaType.endsWith(javaType)) {
            return LongType;
        } else if (DoubleType.javaType.endsWith(javaType)) {
            return DoubleType;
        } else if (TimeStampType.javaType.endsWith(javaType)) {
            return TimeStampType;
        } else if (DateType.javaType.endsWith(javaType)) {
            return DateType;
        } else if (BooleanType.javaType.endsWith(javaType)) {
            return BooleanType;
        }

        return ObjectType;
    }


    /**
     * Method: DOCUMENT ME!
     *
     * @param dataType
     * @param strValue
     * @return
     * @throws ParseException Exception when error
     */
    public static Object parseStringValue(String dataType, String strValue)
            throws ParseException {
        if (dataType.equals(FloatType.getName())) {
            return Float.parseFloat(strValue);
        } else if (dataType.equals(IntegerType.getName())) {
            return Integer.parseInt(strValue);
        } else if (dataType.equals(LongType.getName())) {
            return Long.parseLong(strValue);
        } /*else if (dataType.equals(CalendarType.getName())) {
            Date dt = Utility.stringDateToDate(strValue, CalendarType.getDefaultFormat());
            long time = dt.getTime(); Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(time);

           return cal;

          }*/
        else if (dataType.equals(TimeStampType.getName())) {
            Date dt = Utility.stringDateToDate(strValue,
                    TimeStampType.getDefaultFormat());
            long time = dt.getTime();
            Timestamp ts = new Timestamp(time);

            return ts;
        } else if (dataType.equals(DateType.getName())) {
            return Utility.stringDateToDate(strValue, DateType.getDefaultFormat());
        }

        return strValue;

    }

    /**
     * Method: DOCUMENT ME!
     *
     * @param dataType
     * @param objValue
     * @return
     */
    @SuppressWarnings("unused")
    private static String toStringValue(String dataType, Object objValue) {
        if ((dataType.equals(FloatType.getName())) ||
                (dataType.equals(DoubleType.getName())) ||
                (dataType.equals(IntegerType.getName())) ||
                (dataType.equals(LongType.getName()))) {
            return objValue.toString();
        } /*else if (dataType.equals(CalendarType.getName())) {
            Timestamp ts = new Timestamp(((Calendar) objValue).getTimeInMillis()); String
            dateStr = Utility.dateToStringFormat(ts, TimeStampType.getDefaultFormat());

           return dateStr;}*/
        else if (dataType.equals(TimeStampType.getName())) {
            Timestamp ts = (Timestamp) objValue;
            String dateStr = Utility.dateToStringFormat(ts,
                    TimeStampType.getDefaultFormat());

            return dateStr;
        } else if (dataType.equals(DateType.getName())) {
            Timestamp ts = new Timestamp(((Date) objValue).getTime());
            String dateStr = Utility.dateToStringFormat(ts, DateType.getDefaultFormat());

            return dateStr;
        }
        return objValue.toString();

    }

}
