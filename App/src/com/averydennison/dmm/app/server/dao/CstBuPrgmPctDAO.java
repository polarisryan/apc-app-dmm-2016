package com.averydennison.dmm.app.server.dao;
import com.averydennison.dmm.app.server.persistence.CstBuPrgmPct;
import com.averydennison.dmm.app.server.persistence.Customer;
import com.averydennison.dmm.app.server.persistence.ValidBu;

import java.util.List;

public interface CstBuPrgmPctDAO {

    public void persist(CstBuPrgmPct transientInstance);

    public void attachDirty(CstBuPrgmPct instance);

    public void attachClean(CstBuPrgmPct instance);

    public void delete(CstBuPrgmPct persistentInstance);

    public CstBuPrgmPct merge(CstBuPrgmPct detachedInstance);

    public CstBuPrgmPct findById(Long id);
    
	public List<ValidBu> findValidBuByBuCode(String buCode);
	
	public List<Customer> findCustomerById(Long customerId);
	
	public List<CstBuPrgmPct> findCstBuPrgmPctByCustomerId(Long customerId);

}