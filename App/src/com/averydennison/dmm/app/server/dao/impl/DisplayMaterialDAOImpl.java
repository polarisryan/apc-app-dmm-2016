package com.averydennison.dmm.app.server.dao.impl;


import java.util.List;

import com.averydennison.dmm.app.server.dao.DisplayMaterialDAO;
import com.averydennison.dmm.app.server.persistence.DisplayMaterial;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.DisplayMaterial
 */
public class DisplayMaterialDAOImpl extends HibernateDaoSupport implements DisplayMaterialDAO {

    private static final Log log = LogFactory.getLog(DisplayMaterialDAOImpl.class);

    public void persist(DisplayMaterial transientInstance) {
        log.debug("persisting DisplayMaterial instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(DisplayMaterial instance) {
        log.debug("attaching dirty DisplayMaterial instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(DisplayMaterial instance) {
        log.debug("attaching clean DisplayMaterial instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(DisplayMaterial persistentInstance) {
        log.debug("deleting DisplayMaterial instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public DisplayMaterial merge(DisplayMaterial detachedInstance) {
        log.debug("merging DisplayMaterial instance");
        try {
            DisplayMaterial result = (DisplayMaterial) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public DisplayMaterial findById(Long id) {
        log.debug("getting DisplayMaterial instance with id: " + id);
        try {
            DisplayMaterial instance = (DisplayMaterial) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.DisplayMaterial",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<DisplayMaterial> findByDisplayId(Long displayId) {
        if (displayId == null) return null;
        log.debug("getting Display Material instance by display id: " + displayId);
        try {
            Query q = getSession().getNamedQuery("displayMaterialByDisplayId");
            q.setLong(0, displayId);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<DisplayMaterial>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
