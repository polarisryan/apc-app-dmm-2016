package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.DisplayCost;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:38:04 PM
 */
public interface DisplayCostDAO {
    public void persist(DisplayCost transientInstance);

    public void attachDirty(DisplayCost instance);

    public void attachClean(DisplayCost instance);

    public void delete(DisplayCost persistentInstance);

    public DisplayCost merge(DisplayCost detachedInstance);

    public DisplayCost findById(Long id);
    
    public List<DisplayCost> findByDisplayId(Long displayId);
    
    public DisplayCost loadByDisplayId(Long displayId);
    public DisplayCost loadByDisplayScenarioId(Long displayScenarioId);
}
