package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import com.averydennison.dmm.app.server.dao.ValidLocationDAO;
import com.averydennison.dmm.app.server.persistence.ValidLocation;
import com.averydennison.dmm.app.server.persistence.ValidPlanner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 9:47:15 AM
 */
public class ValidLocationDAOImpl extends HibernateDaoSupport implements ValidLocationDAO {
    private static final Log log = LogFactory.getLog(ValidLocationDAOImpl.class);

    public void persist(ValidLocation transientInstance) {
        log.debug("persisting ValidLocation instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(ValidLocation instance) {
        log.debug("attaching dirty ValidLocation instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(ValidLocation instance) {
        log.debug("attaching clean ValidLocation instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(ValidLocation persistentInstance) {
        log.debug("deleting ValidLocation instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public ValidLocation merge(ValidLocation detachedInstance) {
        log.debug("merging ValidLocation instance");
        try {
            ValidLocation result = (ValidLocation) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public ValidLocation findById(Long id) {
        log.debug("getting ValidLocation instance with id: " + id);
        try {
            ValidLocation instance = (ValidLocation) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.ValidLocation",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<ValidLocation> getLocations() {
        log.debug("getting Locations");
        try {
            Query q = getSession().getNamedQuery("locations");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<ValidLocation>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
}
