package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.StructureDAO;
import com.averydennison.dmm.app.server.persistence.Structure;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.Structure
 */
public class StructureDAOImpl extends HibernateDaoSupport implements StructureDAO {

    private static final Log log = LogFactory.getLog(StructureDAOImpl.class);

    public void persist(Structure transientInstance) {
        log.debug("persisting Structure instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(Structure instance) {
        log.debug("attaching dirty Structure instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(Structure instance) {
        log.debug("attaching clean Structure instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(Structure persistentInstance) {
        log.debug("deleting Structure instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public Structure merge(Structure detachedInstance) {
        log.debug("merging Structure instance");
        try {
            Structure result = (Structure) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public Structure findById(Long id) {
        log.debug("getting Structure instance with id: " + id);
        try {
            Structure instance = (Structure) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.Structure",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<Structure> getStructures() {
        log.debug("getting Structures");
        try {
            Query q = getSession().getNamedQuery("structures");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<Structure>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

	@Override
	public List<Structure> getAllStructures() {
        try {
            Query q = getSession().getNamedQuery("structuresAll");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<Structure>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}
}
