package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.Corrugate;

public interface CorrugateDAO {
	  	public void persist(Corrugate transientInstance);
	    public void attachDirty(Corrugate instance);
	    public void attachClean(Corrugate instance);
	    public void delete(Corrugate instance);
	    public Corrugate merge(Corrugate detachedInstance);
	    public Corrugate findById(Long id);
	    public Corrugate findByDisplayId(Long displayId);
}


