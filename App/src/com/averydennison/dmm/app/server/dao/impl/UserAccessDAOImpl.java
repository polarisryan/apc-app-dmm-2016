package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.UserAccessDAO;
import com.averydennison.dmm.app.server.persistence.UserAccess;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.UserAccess
 */
public class UserAccessDAOImpl extends HibernateDaoSupport implements UserAccessDAO {

    private static final Log log = LogFactory.getLog(UserAccessDAOImpl.class);

    public void persist(UserAccess transientInstance) {
        log.debug("persisting UserAccess instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(UserAccess instance) {
        log.debug("attaching dirty UserAccess instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(UserAccess instance) {
        log.debug("attaching clean UserAccess instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(UserAccess persistentInstance) {
        log.debug("deleting UserAccess instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public UserAccess merge(UserAccess detachedInstance) {
        log.debug("merging UserAccess instance");
        try {
            UserAccess result = (UserAccess) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public UserAccess findById(Long id) {
        log.debug("getting UserAccess instance with id: " + id);
        try {
            UserAccess instance = (UserAccess) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.UserAccess",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
