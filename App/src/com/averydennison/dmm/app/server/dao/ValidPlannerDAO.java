package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.ValidPlanner;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 9:31:19 AM
 */
public interface ValidPlannerDAO {
    public void persist(ValidPlanner transientInstance);

    public void attachDirty(ValidPlanner instance);

    public void attachClean(ValidPlanner instance);

    public void delete(ValidPlanner persistentInstance);

    public ValidPlanner merge(ValidPlanner detachedInstance);

    public ValidPlanner findById(Long id);

    public List<ValidPlanner> getPlanners();

}
