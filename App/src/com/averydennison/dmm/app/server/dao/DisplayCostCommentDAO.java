package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.DisplayCostComment;


public interface DisplayCostCommentDAO {
	 public void persist(DisplayCostComment transientInstance);

	    public void attachDirty(DisplayCostComment instance);

	    public void attachClean(DisplayCostComment instance);

	    public void delete(DisplayCostComment persistentInstance);

	    public DisplayCostComment merge(DisplayCostComment detachedInstance);

	    public DisplayCostComment findById(Long id);
	    
		List<DisplayCostComment> findByDisplayScenarioId(Long displayScenarioId);
}
