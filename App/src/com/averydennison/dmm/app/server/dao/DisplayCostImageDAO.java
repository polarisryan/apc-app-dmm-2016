package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.DisplayCostImage;

public interface DisplayCostImageDAO {
	  	public void persist(DisplayCostImage transientInstance);

	    public void attachDirty(DisplayCostImage instance);

	    public void attachClean(DisplayCostImage instance);

	    public void delete(DisplayCostImage persistentInstance);

	    public DisplayCostImage merge(DisplayCostImage detachedInstance);

	    public DisplayCostImage findById(Long id);
	    
	    public List<DisplayCostImage> findByDisplayScenarioId(Long scenarioId);
	    
}
