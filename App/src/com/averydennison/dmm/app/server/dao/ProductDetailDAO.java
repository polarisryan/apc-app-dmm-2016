package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.ProductDetail;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 9:15:10 AM
 */
public interface ProductDetailDAO {

    public void persist(ProductDetail transientInstance);

    public void attachDirty(ProductDetail instance);

    public void attachClean(ProductDetail instance);

    public void delete(ProductDetail persistentInstance);

    public ProductDetail merge(ProductDetail detachedInstance);

    public ProductDetail findById(Long id);
    
    public List<ProductDetail> findBySku(String sku);

}
