package com.averydennison.dmm.app.server.dao.impl;

import com.averydennison.dmm.app.server.dao.DisplayDcDAO;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayDc;
import com.averydennison.dmm.app.server.persistence.DisplayDcAndPackout;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: Spart Arguello
 * Date: Dec 8, 2009
 * Time: 3:16:16 PM
 */
public class DisplayDcDAOImpl extends HibernateDaoSupport implements DisplayDcDAO {
    private static final Log log = LogFactory.getLog(DisplayDcDAOImpl.class);

    public void persist(DisplayDc transientInstance) {
        log.debug("persisting DisplayDc instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(DisplayDc instance) {
        log.debug("attaching dirty DisplayDc instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(DisplayDc instance) {
        log.debug("attaching clean DisplayDc instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(DisplayDc persistentInstance) {
        log.debug("deleting DisplayDc instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public DisplayDc merge(DisplayDc detachedInstance) {
        log.debug("merging DisplayDc instance");
        try {
            DisplayDc result = (DisplayDc) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public DisplayDc findById(Long id) {
        log.debug("getting DisplayDc instance with id: " + id);
        try {
            DisplayDc instance = (DisplayDc) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.DisplayDc",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    public List<DisplayDc> findDisplayDcByDisplayId(long displayId){
    	log.debug("getting display dcs for display id " + displayId);
        try {
            Query q = getSession().getNamedQuery("findDisplayDcByDisplayId");
            q.setLong(0, displayId);
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            return (List<DisplayDc>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    public Map<Long, DisplayDc> findDisplayDcMapByDisplay(Display display) {
        Map<Long, DisplayDc> dcMap = new HashMap<Long, DisplayDc>();

        log.debug("getting display dc map for display id " + display.getDisplayId());
        List<DisplayDc> list = findDisplayDcListByDisplay(display);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                dcMap.put(list.get(i).getDisplayDcId(), list.get(i));
            }
            return dcMap;
        } else {
            return dcMap;
        }
    }

    public List<DisplayDc> findDisplayDcListByDisplay(Display display) {
        if (display == null || display.getDisplayId() == null) return null;
        log.debug("getting display dcs for display id " + display.getDisplayId());
        try {
            Query q = getSession().getNamedQuery("findDisplayDcByDisplayId");
            q.setLong(0, display.getDisplayId());
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            return (List<DisplayDc>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<DisplayDcAndPackout> findDisplayDcAndPackoutsByDisplay(Display display) {
        if (display == null ) return null;
        if (display.getDisplayId()==null) return null;
        log.debug("getting display dc and packouts for display id " + display.getDisplayId());
        try {
            SQLQuery q = (SQLQuery) getSession().getNamedQuery("findDisplayDcAndPackoutsByDisplay");
            q.setResultTransformer(Transformers.aliasToBean(DisplayDcAndPackout.class));
            q.setLong(0, display.getDisplayId());
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            List<DisplayDcAndPackout> list = (List<DisplayDcAndPackout>) q.list();
            return list;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }


}
