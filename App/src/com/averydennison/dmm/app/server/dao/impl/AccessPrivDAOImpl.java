package com.averydennison.dmm.app.server.dao.impl;

import com.averydennison.dmm.app.server.dao.AccessPrivDAO;
import com.averydennison.dmm.app.server.persistence.AccessPriv;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.AccessPriv
 */
public class AccessPrivDAOImpl extends HibernateDaoSupport implements AccessPrivDAO {

    private static final Log log = LogFactory.getLog(AccessPrivDAOImpl.class);

    public void persist(AccessPriv transientInstance) {
        log.debug("persisting AccessPriv instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(AccessPriv instance) {
        log.debug("attaching dirty AccessPriv instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(AccessPriv instance) {
        log.debug("attaching clean AccessPriv instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(AccessPriv persistentInstance) {
        log.debug("deleting AccessPriv instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public AccessPriv merge(AccessPriv detachedInstance) {
        log.debug("merging AccessPriv instance");
        try {
            AccessPriv result = (AccessPriv) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public AccessPriv findById(Long id) {
        log.debug("getting AccessPriv instance with id: " + id);
        try {
            AccessPriv instance = (AccessPriv) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.AccessPriv",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
