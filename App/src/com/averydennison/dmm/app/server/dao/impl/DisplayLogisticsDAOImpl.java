package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.DisplayLogisticsDAO;
import com.averydennison.dmm.app.server.persistence.DisplayLogistics;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.DisplayLogistics
 */
public class DisplayLogisticsDAOImpl extends HibernateDaoSupport implements DisplayLogisticsDAO {

    private static final Log log = LogFactory
            .getLog(DisplayLogisticsDAOImpl.class);

    public void persist(DisplayLogistics transientInstance) {
        log.debug("persisting DisplayLogistics instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(DisplayLogistics instance) {
        log.debug("attaching dirty DisplayLogistics instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(DisplayLogistics instance) {
        log.debug("attaching clean DisplayLogistics instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(DisplayLogistics persistentInstance) {
        log.debug("deleting DisplayLogistics instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public DisplayLogistics merge(DisplayLogistics detachedInstance) {
        log.debug("merging DisplayLogistics instance");
        try {
            DisplayLogistics result = (DisplayLogistics)
                    getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public DisplayLogistics findByDisplayId(Long id) {
        if (id == null) return null;
        log.debug("getting DisplayLogistics instance by display id: " + id);
        try {
            Query q = getSession().getNamedQuery("displayLogisticsByDisplayId");
            q.setLong(0, id);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            if (q.list().size() == 0) return null;
            return (DisplayLogistics) q.list().get(0);
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }


    public DisplayLogistics findById(Long id) {
        log.debug("getting DisplayLogistics instance with id: " + id);
        try {
            DisplayLogistics instance = (DisplayLogistics)
                    getHibernateTemplate()
                            .get(
                                    "com.averydennison.dmm.app.server.persistence.DisplayLogistics",
                                    id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
