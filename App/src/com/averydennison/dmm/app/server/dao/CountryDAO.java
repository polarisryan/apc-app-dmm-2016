package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.Country;

import java.util.List;

/**
 * User: Mari
 * Date: Jan 7, 2011
 * Time: 05:10:14 PM
 */
public interface CountryDAO {
    public void persist(Country transientInstance);

    public void attachDirty(Country instance);

    public void attachClean(Country instance);

    public void delete(Country persistentInstance);

    public Country merge(Country detachedInstance);

    public Country findById(Long id);

    public List<Country> getAllActiveCountries();
    
    public List<Country> getAllActiveCountryFlag();
}
