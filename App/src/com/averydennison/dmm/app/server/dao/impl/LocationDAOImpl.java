package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.LocationDAO;
import com.averydennison.dmm.app.server.persistence.Location;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.Location
 */
public class LocationDAOImpl extends HibernateDaoSupport implements LocationDAO {

    private static final Log log = LogFactory.getLog(LocationDAOImpl.class);

    public void persist(Location transientInstance) {
        log.debug("persisting Location instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(Location instance) {
        log.debug("attaching dirty Location instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(Location instance) {
        log.debug("attaching clean Location instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(Location persistentInstance) {
        log.debug("deleting Location instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public Location merge(Location detachedInstance) {
        log.debug("merging Location instance");
        try {
            Location result = (Location) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public Location findById(Long id) {
        log.debug("getting Location instance with id: " + id);
        try {
            Location instance = (Location) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.Location",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<Location> getAllActiveLocations() {
        log.debug("getting all active locations");
        try {
            Query q = getSession().getNamedQuery("getAllActiveLocations");
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            return (List<Location>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
