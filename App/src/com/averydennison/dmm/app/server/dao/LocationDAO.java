package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.Location;

import java.util.List;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:48:34 PM
 */
public interface LocationDAO {
    public void persist(Location transientInstance);

    public void attachDirty(Location instance);

    public void attachClean(Location instance);

    public void delete(Location persistentInstance);

    public Location merge(Location detachedInstance);

    public Location findById(Long id);

    public List<Location> getAllActiveLocations();
}
