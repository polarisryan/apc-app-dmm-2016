package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import com.averydennison.dmm.app.server.dao.ProductPlannerDAO;
import com.averydennison.dmm.app.server.persistence.ProductPlanner;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 9:39:41 AM
 */
public class ProductPlannerDAOImpl extends HibernateDaoSupport implements ProductPlannerDAO {
    private static final Log log = LogFactory.getLog(ProductPlannerDAOImpl.class);

    public void persist(ProductPlanner transientInstance) {
        log.debug("persisting ProductPlanner instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(ProductPlanner instance) {
        log.debug("attaching dirty ProductPlanner instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(ProductPlanner instance) {
        log.debug("attaching clean ProductPlanner instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(ProductPlanner persistentInstance) {
        log.debug("deleting ProductPlanner instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public ProductPlanner merge(ProductPlanner detachedInstance) {
        log.debug("merging ProductPlanner instance");
        try {
            ProductPlanner result = (ProductPlanner) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public ProductPlanner findById(Long id) {
        log.debug("getting ProductPlanner instance with id: " + id);
        try {
            ProductPlanner instance = (ProductPlanner) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.ProductPlanner",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<ProductPlanner> findBySkuId(Long skuId) {
        if (skuId == null) return null;
        log.debug("getting Product Planner instance by skuId: " + skuId);
        System.out.println("getting Product Planner instance by skuId: " + skuId);
        try {
            Query q = getSession().getNamedQuery("productPlannerBySkuId");
            q.setLong(0, skuId);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            if (q.list().size() == 0) return null;
            return (List<ProductPlanner>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<ProductPlanner> findBySkuIdDc(Long skuId, String dcCode) {
        if (skuId == null) return null;
        log.debug("getting Product Planner instance by skuId: " + skuId);
        System.out.println("getting Product Planner instance by skuId: " + skuId + ", dc=" + dcCode);
        try {
            Query q = getSession().getNamedQuery("productPlannerBySkuIdDc");
            q.setLong(0, skuId);
            q.setString(1, dcCode);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            if (q.list().size() == 0) return null;
            return (List<ProductPlanner>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

}
