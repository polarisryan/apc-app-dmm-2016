package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.client.models.BooleanDTO;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayForMassUpdate;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 11:25:13 AM
 */
public interface DisplayDAO {
    public void persist(Display transientInstance);

    public void attachDirty(Display instance);

    public void attachClean(Display instance);

    public void delete(Display persistentInstance);
    
    public void callDisplayDeleteStoredProcedure(Long displayId, String username);

    public Display merge(Display detachedInstance);

    public Display findById(Long id);

    public List<Display> getAllDisplays();
    
    public List<DisplayForMassUpdate> getSelectedDisplaysForMassUpdate(List<Long> parameters);
    
    public BooleanDTO isDuplicateSKU(String sku);
    
	public void updateGoNoGo(Long displayId);
	
}
