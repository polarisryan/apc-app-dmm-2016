package com.averydennison.dmm.app.server.dao.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.averydennison.dmm.app.client.models.BuCostCenterSplitDTO;
import com.averydennison.dmm.app.client.models.CalculationSummaryDTO;
import com.averydennison.dmm.app.client.models.CorrugateComponentDTO;
import com.averydennison.dmm.app.client.models.CorrugateComponentsDetailDTO;
import com.averydennison.dmm.app.client.models.CorrugateDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisHistoryDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.CstBuPrgmPctDTO;
import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.CustomerMarketingDTO;
import com.averydennison.dmm.app.client.models.CustomerPctDTO;
import com.averydennison.dmm.app.client.models.CustomerTypeDTO;
import com.averydennison.dmm.app.client.models.DisplayCostCommentDTO;
import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.client.models.DisplayCostImageDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayDcDTO;
import com.averydennison.dmm.app.client.models.DisplayForMassUpdateDTO;
import com.averydennison.dmm.app.client.models.DisplayLogisticsDTO;
import com.averydennison.dmm.app.client.models.DisplayPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayPlannerDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.LocationDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerDTO;
import com.averydennison.dmm.app.client.models.ProductDetailDTO;
import com.averydennison.dmm.app.client.models.ProgramBuDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodDTO;
import com.averydennison.dmm.app.client.models.ScenarioStatusDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.averydennison.dmm.app.client.models.UserDTO;
import com.averydennison.dmm.app.client.models.UserTypeDTO;
import com.averydennison.dmm.app.client.models.ValidBuDTO;
import com.averydennison.dmm.app.client.models.ValidLocationDTO;
import com.averydennison.dmm.app.client.models.ValidPlannerDTO;
import com.averydennison.dmm.app.client.models.ValidPlantDTO;
import com.averydennison.dmm.app.server.persistence.CalculationSummary;
import com.averydennison.dmm.app.server.persistence.Corrugate;
import com.averydennison.dmm.app.server.persistence.CorrugateComponent;
import com.averydennison.dmm.app.server.persistence.CorrugateComponentsDetail;
import com.averydennison.dmm.app.server.persistence.CostAnalysisKitComponent;
import com.averydennison.dmm.app.server.persistence.Country;
import com.averydennison.dmm.app.server.persistence.CstBuPrgmPct;
import com.averydennison.dmm.app.server.persistence.Customer;
import com.averydennison.dmm.app.server.persistence.CustomerCountry;
import com.averydennison.dmm.app.server.persistence.CustomerMarketing;
import com.averydennison.dmm.app.server.persistence.CustomerPct;
import com.averydennison.dmm.app.server.persistence.CustomerType;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayCost;
import com.averydennison.dmm.app.server.persistence.DisplayCostComment;
import com.averydennison.dmm.app.server.persistence.DisplayCostImage;
import com.averydennison.dmm.app.server.persistence.DisplayDc;
import com.averydennison.dmm.app.server.persistence.DisplayForMassUpdate;
import com.averydennison.dmm.app.server.persistence.DisplayLogistics;
import com.averydennison.dmm.app.server.persistence.DisplayPackout;
import com.averydennison.dmm.app.server.persistence.DisplayPlanner;
import com.averydennison.dmm.app.server.persistence.DisplaySalesRep;
import com.averydennison.dmm.app.server.persistence.DisplayScenario;
import com.averydennison.dmm.app.server.persistence.DisplayShipWave;
import com.averydennison.dmm.app.server.persistence.KitComponent;
import com.averydennison.dmm.app.server.persistence.Location;
import com.averydennison.dmm.app.server.persistence.LocationCountry;
import com.averydennison.dmm.app.server.persistence.Manufacturer;
import com.averydennison.dmm.app.server.persistence.ManufacturerCountry;
import com.averydennison.dmm.app.server.persistence.ProductDetail;
import com.averydennison.dmm.app.server.persistence.ProgramBu;
import com.averydennison.dmm.app.server.persistence.PromoPeriod;
import com.averydennison.dmm.app.server.persistence.PromoPeriodCountry;
import com.averydennison.dmm.app.server.persistence.ScenarioStatus;
import com.averydennison.dmm.app.server.persistence.Status;
import com.averydennison.dmm.app.server.persistence.Structure;
import com.averydennison.dmm.app.server.persistence.User;
import com.averydennison.dmm.app.server.persistence.UserChangeLog;
import com.averydennison.dmm.app.server.persistence.UserType;
import com.averydennison.dmm.app.server.persistence.ValidBu;
import com.averydennison.dmm.app.server.persistence.ValidLocation;
import com.averydennison.dmm.app.server.persistence.ValidPlanner;
import com.averydennison.dmm.app.server.persistence.ValidPlant;

/**
 * User: Spart Arguello
 * Date: Oct 19, 2009
 * Time: 2:02:26 PM
 */
public class ConverterFactory {
	public static UserDTO convert(User user) {
		if (user == null) return null;
		UserDTO newInstance = new UserDTO();
		newInstance.setId(user.getUserId());
		newInstance.setUsername(user.getUsername());
		newInstance.setPassword(user.getPassword());
		return newInstance;
	}

	public static StatusDTO convert(Status status) {
		if (status == null) return null;
		StatusDTO newInstance = new StatusDTO();
		newInstance.setDescription(status.getDescription());
		newInstance.setStatusId(status.getStatusId());
		newInstance.setStatusName(status.getStatusName());
		return newInstance;
	}

	public static List<StatusDTO> convertStatuses(List<Status> statuses) {
		if (statuses == null) return null;
		List<StatusDTO> statusDTOs = new ArrayList<StatusDTO>();
		for (int i = 0; i < statuses.size(); i++) {
			statusDTOs.add(convert(statuses.get(i)));
		}
		return statusDTOs;
	}

	public static DisplayScenarioDTO convert(DisplayScenario displayScenario) {
		if (displayScenario == null) return null;
		DisplayScenarioDTO displayScenarioDTO = new DisplayScenarioDTO();
		displayScenarioDTO.setDisplayScenarioId(displayScenario.getDisplayScenarioId());
		displayScenarioDTO.setDisplayId(displayScenario.getDisplayId());
		displayScenarioDTO.setDisplaySalesRepId(displayScenario.getDisplaySalesRepId());
		displayScenarioDTO.setManufacturerId(displayScenario.getManufacturerId());
		displayScenarioDTO.setPackoutVendorId(displayScenario.getPackoutVendorId());
		displayScenarioDTO.setPromoPeriodId(displayScenario.getPromoPeriodId());
		displayScenarioDTO.setPromoYear(displayScenario.getPromoYear());
		displayScenarioDTO.setScenarioNum(displayScenario.getScenarioNum());
		displayScenarioDTO.setScenariodStatusId(displayScenario.getScenariodStatusId());
		displayScenarioDTO.setSku(displayScenario.getSku());
		displayScenarioDTO.setScenarioApprovalFlg(displayScenario.getScenarioApprovalFlg());
		displayScenarioDTO.setActualQty(displayScenario.getActualQty());
		displayScenarioDTO.setQtyForecast(displayScenario.getQtyForecast());
		displayScenarioDTO.setUserCreated(displayScenario.getUserCreated());
		displayScenarioDTO.setUserLastChanged(displayScenario.getUserLastChanged());
		displayScenarioDTO.setDtCreated(displayScenario.getDtCreated());
		displayScenarioDTO.setDtLastChanged(displayScenario.getDtLastChanged());
		return displayScenarioDTO;
	}

	public static DisplayScenario convert(DisplayScenarioDTO displayScenarioDTO) {
		if (displayScenarioDTO == null) return null;
		DisplayScenario displayScenario = new DisplayScenario();
		displayScenario.setDisplayScenarioId(displayScenarioDTO.getDisplayScenarioId());
		displayScenario.setDisplayId(displayScenarioDTO.getDisplayId());
		displayScenario.setDisplaySalesRepId(displayScenarioDTO.getDisplaySalesRepId());
		displayScenario.setManufacturerId(displayScenarioDTO.getManufacturerId());
		displayScenario.setPackoutVendorId(displayScenarioDTO.getPackoutVendorId());
		displayScenario.setPromoPeriodId(displayScenarioDTO.getPromoPeriodId());
		displayScenario.setPromoYear(displayScenarioDTO.getPromoYear());
		displayScenario.setScenarioNum(displayScenarioDTO.getScenarioNum());
		displayScenario.setScenariodStatusId(displayScenarioDTO.getScenariodStatusId());
		displayScenario.setSku(displayScenarioDTO.getSku());
		displayScenario.setScenarioApprovalFlg(displayScenarioDTO.getScenarioApprovalFlg());
		displayScenario.setActualQty(displayScenarioDTO.getActualQty());
		displayScenario.setQtyForecast(displayScenarioDTO.getQtyForecast());
		displayScenario.setUserCreated(displayScenarioDTO.getUserCreated());
		displayScenario.setUserLastChanged(displayScenarioDTO.getUserLastChanged());
		displayScenario.setDtCreated(displayScenarioDTO.getDtCreated());
		displayScenario.setDtLastChanged(displayScenarioDTO.getDtLastChanged());
		return displayScenario;
	}
	
	public static List<DisplayScenarioDTO> convertDisplayScenario(List<DisplayScenario> displayScenarios) {
		if (displayScenarios == null) return null;
		List<DisplayScenarioDTO> displayScenarioDTOs = new ArrayList<DisplayScenarioDTO>();
		for (int i = 0; i < displayScenarios.size(); i++) {
			if(displayScenarios.get(i).getDisplayScenarioId()!=null)
				displayScenarioDTOs.add(convert(displayScenarios.get(i)));
		}
		return displayScenarioDTOs;
	}

	public static List<DisplayScenario> convertDisplayScenarioDTO(List<DisplayScenarioDTO> displayScenariosDTO) {
		if (displayScenariosDTO == null) return null;
		List<DisplayScenario> displayScenarios = new ArrayList<DisplayScenario>();
		for (int i = 0; i < displayScenariosDTO.size(); i++) {
			displayScenarios.add(convert(displayScenariosDTO.get(i)));
		}
		return displayScenarios;
	}
	
	public static ScenarioStatusDTO convert(ScenarioStatus scenarioStatus) {
		if (scenarioStatus == null) return null;
		ScenarioStatusDTO scenarioStatusDTO = new ScenarioStatusDTO();
		scenarioStatusDTO.setScenarioStatusId(scenarioStatus.getScenarioStatusId());
		scenarioStatusDTO.setName(scenarioStatus.getName());
		scenarioStatusDTO.setDescription(scenarioStatus.getDescription());
		scenarioStatusDTO.setActiveFlg(scenarioStatus.getActiveFlg());
		return scenarioStatusDTO;
	}

	public static List<ScenarioStatusDTO> convertScenarioStatus(List<ScenarioStatus> scenarioStatuses) {
		if (scenarioStatuses == null) return null;
		List<ScenarioStatusDTO> scenarioStatusDTOs = new ArrayList<ScenarioStatusDTO>();
		for (int i = 0; i < scenarioStatuses.size(); i++) {
			scenarioStatusDTOs.add(convert(scenarioStatuses.get(i)));
		}
		return scenarioStatusDTOs;
	}

	public static CustomerDTO convert(Customer customer, boolean isFull) {
		if (customer == null) return null;
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setCustomerId(customer.getCustomerId());
		customerDTO.setCustomerName(customer.getCustomerName());
		customerDTO.setCustomerDescription(customer.getCustomerDescription());
		if(isFull){
			customerDTO.setCustomerCode(customer.getCustomerCode());
			customerDTO.setCompanyCode(customer.getCompanyCode());
			customerDTO.setCustomerTypeId(customer.getCustomerType() != null ? customer.getCustomerType().getCustomerTypeId() : null);
			customerDTO.setCustomerTypeName(customer.getCustomerType() != null ? customer.getCustomerType().getTypeName() : null);
			customerDTO.setCustomerParentId(customer.getParent() != null ? customer.getParent().getCustomerId() : null);
			customerDTO.setCustomerParentName(customer.getParent() != null ? customer.getParent().getCustomerName() : null);
			customerDTO.setCustomerCMAId(customer.getCma() != null ? customer.getCma().getDisplaySalesRepId() : null);
			customerDTO.setCustomerCMAName(customer.getCma() != null ? customer.getCma().getFirstName() + " " + customer.getCma().getLastName() : null);			
			customerDTO.setActive(customer.isActiveFlg());
			for(Iterator<Country> i = customer.getCountries().iterator(); i.hasNext();){
				Country country = i.next();
				customerDTO.setCustomerCountryId(country.getCountryId());
				customerDTO.setCustomerCountryName(country.getCountryName());
			}
		}
		return customerDTO;
	}

	public static List<CustomerDTO> convertCustomers(List<Customer> customers, boolean isFull) {
		if (customers == null) return null;
		List<CustomerDTO> customerDTOs = new ArrayList<CustomerDTO>();
		for (int i = 0; i < customers.size(); i++) {
			customerDTOs.add(convert(customers.get(i), isFull));
		}
		return customerDTOs;
	}
	
	public static List<CustomerTypeDTO> convertCustomerTypes(List<CustomerType> customerTypes) {
		if (customerTypes == null) return null;
		List<CustomerTypeDTO> result = new ArrayList<CustomerTypeDTO>();
		for (int i = 0; i < customerTypes.size(); i++) {
			result.add(convert(customerTypes.get(i)));
		}
		return result;
	}
	
	private static CustomerTypeDTO convert(CustomerType customerType) {
		if (customerType == null) return null;
		CustomerTypeDTO result = new CustomerTypeDTO();
		result.setCustomerTypeId(customerType.getCustomerTypeId());
		result.setCustomerTypeName(customerType.getTypeName());
		result.setActive(customerType.isActiveFlg());
		return result;
	}

	public static CustomerCountryDTO convert(CustomerCountry customerCountry) {
		if (customerCountry == null) return null;
		CustomerCountryDTO customerCountryDTO = new CustomerCountryDTO();
		customerCountryDTO.setCustomerId(customerCountry.getCustomerId());
		customerCountryDTO.setCustomerName(customerCountry.getCustomerName());
		customerCountryDTO.setCountryId(customerCountry.getCountryId());
		customerCountryDTO.setCountryName(customerCountry.getCountryName());
		return customerCountryDTO;
	}

	public static List<CustomerCountryDTO> convertCustomersCountry(List<CustomerCountry> customersCountry) {
		if (customersCountry == null) return null;
		List<CustomerCountryDTO> customerDTOs = new ArrayList<CustomerCountryDTO>();
		for (int i = 0; i < customersCountry.size(); i++) {
			customerDTOs.add(convert(customersCountry.get(i)));
		}
		return customerDTOs;
	}
	
	public static StructureDTO convert(Structure structure) {
		if (structure == null) return null;
		StructureDTO structureDTO = new StructureDTO();
		structureDTO.setStructureId(structure.getStructureId());
		structureDTO.setStructureName(structure.getStructureName());
		structureDTO.setStructureDesc(structure.getStructureDesc());
		structureDTO.setActiveFlg(structure.isActiveFlg());
		return structureDTO;
	}

	public static List<StructureDTO> convertStructures(List<Structure> structures) {
		if (structures == null) return null;
		List<StructureDTO> structureDTOs = new ArrayList<StructureDTO>();
		for (int i = 0; i < structures.size(); i++) {
			structureDTOs.add(convert(structures.get(i)));
		}
		return structureDTOs;
	}

	public static DisplaySalesRepDTO convert(DisplaySalesRep displaySalesRep, boolean isFull) {
		if (displaySalesRep == null) return null;
		DisplaySalesRepDTO displaySalesRepDTO = new DisplaySalesRepDTO();
		displaySalesRepDTO.setDisplaySalesRepId(displaySalesRep.getDisplaySalesRepId());
		displaySalesRepDTO.setFirstName(displaySalesRep.getFirstName());
		displaySalesRepDTO.setLastName(displaySalesRep.getLastName());
		displaySalesRepDTO.setRepCode(displaySalesRep.getRepCode());
		displaySalesRepDTO.setFullName(displaySalesRep.getFirstName() + " " + displaySalesRep.getLastName());
		displaySalesRepDTO.setActiveFlg(displaySalesRep.isActiveFlg());
		displaySalesRepDTO.setCountryId(displaySalesRep.getCountry() != null ? displaySalesRep.getCountry().getCountryId() : null);
		displaySalesRepDTO.setCountryName(displaySalesRep.getCountry() != null ? displaySalesRep.getCountry().getCountryName() : null);
		if(isFull){
			displaySalesRepDTO.setUserTypeId(displaySalesRep.getUserType() != null ? displaySalesRep.getUserType().getUserTypeId() : null);
			displaySalesRepDTO.setUserTypeName(displaySalesRep.getUserType() != null ? displaySalesRep.getUserType().getTypeName() : null);
		}
		return displaySalesRepDTO;
	}

	public static List<DisplaySalesRepDTO> convertDisplaySalesReps(List<DisplaySalesRep> displaySalesReps, boolean isFull) {
		if (displaySalesReps == null) return null;
		List<DisplaySalesRepDTO> displaySalesRepDTOs = new ArrayList<DisplaySalesRepDTO>();
		for (int i = 0; i < displaySalesReps.size(); i++) {
			displaySalesRepDTOs.add(convert(displaySalesReps.get(i), isFull));
		}
		return displaySalesRepDTOs;
	}

	public static PromoPeriodDTO convert(PromoPeriod promoPeriod) {
		if (promoPeriod == null) return null;
		PromoPeriodDTO promoPeriodDTO = new PromoPeriodDTO();
		promoPeriodDTO.setPromoPeriodId(promoPeriod.getPromoPeriodId());
		promoPeriodDTO.setPromoPeriodName(promoPeriod.getPromoPeriodName());
		promoPeriodDTO.setPromoPeriodDescription(promoPeriod.getPromoPeriodDescription());
		promoPeriodDTO.setPromoDate(promoPeriod.getPromoDate());
		return promoPeriodDTO;
	}

	public static List<PromoPeriodDTO> convertPromoPeriods(List<PromoPeriod> promoPeriods) {
		if (promoPeriods == null) return null;
		List<PromoPeriodDTO> promoPeriodDTOs = new ArrayList<PromoPeriodDTO>();
		for (int i = 0; i < promoPeriods.size(); i++) {
			promoPeriodDTOs.add(convert(promoPeriods.get(i)));
		}
		return promoPeriodDTOs;
	}

	public static PromoPeriodCountryDTO convert(PromoPeriodCountry promoPeriodCountry) {
		if (promoPeriodCountry == null) return null;
		PromoPeriodCountryDTO promoPeriodCountryDTO = new PromoPeriodCountryDTO();
		promoPeriodCountryDTO.setPromoPeriodId(promoPeriodCountry.getpromoPeriodId());
		promoPeriodCountryDTO.setPromoPeriodName(promoPeriodCountry.getpromoPeriodName());
		promoPeriodCountryDTO.setCountryId(promoPeriodCountry.getCountryId());
		promoPeriodCountryDTO.setCountryName(promoPeriodCountry.getCountryName());
		return promoPeriodCountryDTO;
	}

	public static List<PromoPeriodCountryDTO> convertPromoPeriodsCountry(List<PromoPeriodCountry> promoPeriodsCountry) {
		if (promoPeriodsCountry == null) return null;
		List<PromoPeriodCountryDTO> promoPeriodCountryDTOs = new ArrayList<PromoPeriodCountryDTO>();
		for (int i = 0; i < promoPeriodsCountry.size(); i++) {
			promoPeriodCountryDTOs.add(convert(promoPeriodsCountry.get(i)));
		}
		return promoPeriodCountryDTOs;
	}
	
	public static Display convert(DisplayDTO displayDTO) {
		if (displayDTO == null) return null;
		Display display = new Display();
		display.setDisplayId(displayDTO.getDisplayId());
		display.setCustomerId(displayDTO.getCustomerId());
		display.setDescription(displayDTO.getDescription());
		display.setCustomerDisplayNumber(displayDTO.getCustomerDisplayNumber());
		display.setCmProjectNumber(displayDTO.getCmProjectNumber());
		display.setDestination(displayDTO.getDestination());
		display.setDisplaySalesRepId(displayDTO.getDisplaySalesRepId());
		display.setDtCreated(displayDTO.getDtCreated());
		display.setDtLastChanged(displayDTO.getDtLastChanged());
		display.setManufacturerId(displayDTO.getManufacturerId());
		display.setPromoPeriodId(displayDTO.getPromoPeriodId());
		display.setPromoYear(displayDTO.getPromoYear());
		display.setQtyForecast(displayDTO.getQtyForecast());
		display.setQsiDocNumber(displayDTO.getQsiDocNumber());
		display.setSku(displayDTO.getSku());
		display.setStatusId(displayDTO.getStatusId());
		display.setSkuMixFinalFlg(displayDTO.isSkuMixFinalFlg());
		display.setStructureId(displayDTO.getStructureId());
		display.setUserCreated(displayDTO.getUserCreated());
		display.setUserLastChanged(displayDTO.getUserLastChanged());
		display.setDisplaySetupId(displayDTO.getDisplaySetupId());
		display.setDtGoNoGo(displayDTO.getDtGoNoGo());
		display.setRygStatusFlg(displayDTO.getRygStatusFlg());
		display.setPackoutVendorId(displayDTO.getPackoutVendorId());
		display.setActualQty(displayDTO.getActualQty());
		display.setCountryId(displayDTO.getCountryId());
		display.setApprovers(convertTo(displayDTO.getApproverIds()));
		return display;
	}

	private static Set<DisplaySalesRep> convertTo(Set<Long> approverIds) {
		HashSet<DisplaySalesRep> result = new HashSet<DisplaySalesRep>();
		for(Long appId: approverIds){
			DisplaySalesRep dsr = new DisplaySalesRep();
			dsr.setDisplaySalesRepId(appId);
			result.add(dsr);
		}
		return result;
	}

	public static DisplayDTO convert(Display display, boolean isFull) {
		if (display == null) return null;
		DisplayDTO displayDTO = new DisplayDTO();
		displayDTO.setDisplayId(display.getDisplayId());
		displayDTO.setCustomerId(display.getCustomerId());
		displayDTO.setDescription(display.getDescription());
		displayDTO.setCustomerDisplayNumber(display.getCustomerDisplayNumber());
		displayDTO.setCmProjectNumber(display.getCmProjectNumber());
		displayDTO.setDestination(display.getDestination());
		displayDTO.setDisplaySalesRepId(display.getDisplaySalesRepId());
		displayDTO.setDtCreated(display.getDtCreated());
		displayDTO.setDtLastChanged(display.getDtLastChanged());
		displayDTO.setManufacturerId(display.getManufacturerId());
		displayDTO.setPromoPeriodId(display.getPromoPeriodId());
		displayDTO.setPromoYear(display.getPromoYear());
		displayDTO.setQtyForecast(display.getQtyForecast());
		displayDTO.setQsiDocNumber(display.getQsiDocNumber());
		displayDTO.setSku(display.getSku());
		displayDTO.setStatusId(display.getStatusId());
		displayDTO.setSkuMixFinalFlg(display.isSkuMixFinalFlg());
		displayDTO.setStructureId(display.getStructureId());
		displayDTO.setUserCreated(display.getUserCreated());
		displayDTO.setUserLastChanged(display.getUserLastChanged());
		displayDTO.setDisplaySetupId(display.getDisplaySetupId());
		displayDTO.setDisplaySalesRepByDisplaySalesRepId(convert(display.getDisplaySalesRepByDisplaySalesRepId(), false));
		displayDTO.setStatusByStatusId(convert(display.getStatusByStatusId()));
		displayDTO.setCustomerByCustomerId(convert(display.getCustomerByCustomerId(), false));
		displayDTO.setStructureByStructureId(convert(display.getStructureByStructureId()));
		displayDTO.setPromoPeriodByPromoPeriodId(convert(display.getPromoPeriodByPromoPeriodId()));
		displayDTO.setDtGoNoGo(display.getDtGoNoGo()); 
		displayDTO.setRygStatusFlg(display.getRygStatusFlg());
		displayDTO.setPackoutVendorId(display.getPackoutVendorId());
		displayDTO.setActualQty(display.getActualQty());
		displayDTO.setCountryId(display.getCountryId());
		displayDTO.setCountryByCountryId(convert(display.getCountryByCountryId()));
		if(isFull){
			displayDTO.setApproverIds(convert(display.getApprovers()));
		}
		return displayDTO;
	}

	private static Set<Long> convert(Set<DisplaySalesRep> approvers) {
		HashSet<Long> result = new HashSet<Long>();
		for(DisplaySalesRep app: approvers){
			result.add(app.getDisplaySalesRepId());
		}
		return result;
	}

	public static DisplayForMassUpdate convert(DisplayForMassUpdateDTO displayMassUpdateDTO) {
		if (displayMassUpdateDTO == null) return null;
		DisplayForMassUpdate displayForMassUpdate = new DisplayForMassUpdate();
		displayForMassUpdate.setDisplayId(displayMassUpdateDTO.getDisplayId());
		displayForMassUpdate.setSku(displayMassUpdateDTO.getSku());
		displayForMassUpdate.setDescription(displayMassUpdateDTO.getDescription());
		displayForMassUpdate.setCustomerId(displayMassUpdateDTO.getCustomerId());
		displayForMassUpdate.setStatusId(displayMassUpdateDTO.getStatusId());
		displayForMassUpdate.setPromoPeriodId(displayMassUpdateDTO.getPromoPeriodId());
		displayForMassUpdate.setPromoYear(displayMassUpdateDTO.getPromoYear());
		displayForMassUpdate.setDisplaySalesRepId(displayMassUpdateDTO.getDisplaySalesRepId());
		displayForMassUpdate.setSkuMixFinalFlg(displayMassUpdateDTO.isSkuMixFinalFlg());
		displayForMassUpdate.setStructureId(displayMassUpdateDTO.getStructureId());
		displayForMassUpdate.setDtLastChanged(displayMassUpdateDTO.getDtLastChanged());
		displayForMassUpdate.setPriceAvailabilityDate(displayMassUpdateDTO.getPriceAvailabilityDate());
		displayForMassUpdate.setSentSampleDate(displayMassUpdateDTO.getSentSampleDate());
		displayForMassUpdate.setFinalArtworkDate(displayMassUpdateDTO.getFinalArtworkDate());
		displayForMassUpdate.setPimCompletedDate(displayMassUpdateDTO.getPimCompletedDate());
		displayForMassUpdate.setPricingAdminDate(displayMassUpdateDTO.getPricingAdminDate());
		displayForMassUpdate.setInitialRenderingFlg(displayMassUpdateDTO.getInitialRendering());
		displayForMassUpdate.setStructureApprovedFlg(displayMassUpdateDTO.getStructureApproved() );
		displayForMassUpdate.setOrderInFlg(displayMassUpdateDTO.getOrderIn());
		displayForMassUpdate.setComments(displayMassUpdateDTO.getComments());
		displayForMassUpdate.setProjectLevel(displayMassUpdateDTO.getProjectLevel());
		displayForMassUpdate.setCustomerName(displayMassUpdateDTO.getCustomerName());
		displayForMassUpdate.setStatusName(displayMassUpdateDTO.getStatusName());
		displayForMassUpdate.setPromoPeriodName(displayMassUpdateDTO.getPromoPeriodName());
		displayForMassUpdate.setFullName(displayMassUpdateDTO.getFullName());
		displayForMassUpdate.setStructureName(displayMassUpdateDTO.getStructureName());
		displayForMassUpdate.setCorrugateId(displayMassUpdateDTO.getCorrugateId());
		displayForMassUpdate.setCorrugateName(displayMassUpdateDTO.getCorrugateName());
		return displayForMassUpdate;
	}

	public static DisplayForMassUpdateDTO convert(DisplayForMassUpdate displayMassUpdate) {
		if (displayMassUpdate == null) return null;
		DisplayForMassUpdateDTO displayForMassUpdateDTO = new DisplayForMassUpdateDTO();
		displayForMassUpdateDTO.setDisplayId(displayMassUpdate.getDisplayId());
		displayForMassUpdateDTO.setSku(displayMassUpdate.getSku());
		displayForMassUpdateDTO.setDescription(displayMassUpdate.getDescription());
		displayForMassUpdateDTO.setCustomerId(displayMassUpdate.getCustomerId());
		displayForMassUpdateDTO.setStatusId(displayMassUpdate.getStatusId());
		displayForMassUpdateDTO.setDisplaySalesRepId(displayMassUpdate.getDisplaySalesRepId());
		displayForMassUpdateDTO.setPromoPeriodId(displayMassUpdate.getPromoPeriodId());
		displayForMassUpdateDTO.setPromoYear(displayMassUpdate.getPromoYear());
		displayForMassUpdateDTO.setSkuMixFinalFlg(displayMassUpdate.getSkuMixFinalFlg());
		displayForMassUpdateDTO.setStructureId(displayMassUpdate.getStructureId());
		displayForMassUpdateDTO.setDtLastChanged(displayMassUpdate.getDtLastChanged());
		displayForMassUpdateDTO.setPriceAvailabilityDate(displayMassUpdate.getPriceAvailabilityDate());
		displayForMassUpdateDTO.setSentSampleDate(displayMassUpdate.getSentSampleDate());
		displayForMassUpdateDTO.setFinalArtworkDate(displayMassUpdate.getFinalArtworkDate());
		displayForMassUpdateDTO.setPimCompletedDate(displayMassUpdate.getPimCompletedDate());
		displayForMassUpdateDTO.setPricingAdminDate(displayMassUpdate.getPricingAdminDate());
		displayForMassUpdateDTO.setIntialRendering(displayMassUpdate.getInitialRenderingFlg());
		displayForMassUpdateDTO.setStructureApproved(displayMassUpdate.getStructureApprovedFlg() );
		displayForMassUpdateDTO.setOrderIn(displayMassUpdate.getOrderInFlg());
		displayForMassUpdateDTO.setComments(displayMassUpdate.getComments());
		displayForMassUpdateDTO.setProjectLevel(displayMassUpdate.getProjectLevel());
		displayForMassUpdateDTO.setCustomerName(displayMassUpdate.getCustomerName());
		displayForMassUpdateDTO.setStatusName(displayMassUpdate.getStatusName());
		displayForMassUpdateDTO.setPromoPeriodName(displayMassUpdate.getPromoPeriodName());
		displayForMassUpdateDTO.setFullName(displayMassUpdate.getFullName());
		displayForMassUpdateDTO.setStructureName(displayMassUpdate.getStructureName());
		displayForMassUpdateDTO.setCorrugateId(displayMassUpdate.getCorrugateId());
		displayForMassUpdateDTO.setCorrugateName(displayMassUpdate.getCorrugateName());
		return displayForMassUpdateDTO;
	}


	public static List<DisplayDTO> convertDisplays(List<Display> displays) {
		if (displays == null) return null;
		List<DisplayDTO> displayDTOs = new ArrayList<DisplayDTO>();
		for (int i = 0; i < displays.size(); i++) {
			displayDTOs.add(convert(displays.get(i), false));
		}
		return displayDTOs;
	}

	public static List<DisplayForMassUpdateDTO> convertDisplayForMassUpdates(List<DisplayForMassUpdate> displays) {
		if (displays == null) return null;
		List<DisplayForMassUpdateDTO> displayForMassUpdateDTOs = new ArrayList<DisplayForMassUpdateDTO>();
		for (int i = 0; i < displays.size(); i++) {
			displayForMassUpdateDTOs.add(convert(displays.get(i)));
		}
		return displayForMassUpdateDTOs;
	}

	public static Collection<DisplayShipWaveDTO> convertDisplayShipWaves(Collection<DisplayShipWave> displayShipWavesByDisplayId) {
		if (displayShipWavesByDisplayId == null) return null;
		Collection<DisplayShipWaveDTO> displayShipWaveDTOs = new HashSet<DisplayShipWaveDTO>();
		Iterator i = displayShipWavesByDisplayId.iterator();
		while (i.hasNext()) {
			displayShipWaveDTOs.add(convert((DisplayShipWave) i.next()));
		}
		return displayShipWaveDTOs;
	}

	public static DisplayShipWaveDTO convert(DisplayShipWave displayShipWave) {
		if (displayShipWave == null) return null;
		DisplayShipWaveDTO displayShipWaveDTO = new DisplayShipWaveDTO();
		displayShipWaveDTO.setDisplayId(displayShipWave.getDisplayId());
		displayShipWaveDTO.setDisplayShipWaveId(displayShipWave.getDisplayShipWaveId());
		displayShipWaveDTO.setDtCreated(displayShipWave.getDtCreated());
		displayShipWaveDTO.setDtLastChanged(displayShipWave.getDtLastChanged());
		displayShipWaveDTO.setShipWave(displayShipWave.getShipWaveNum());
		displayShipWaveDTO.setDestinationFlg(displayShipWave.isDestinationFlg());
		displayShipWaveDTO.setMustArriveDate(displayShipWave.getMustArriveDate());
		displayShipWaveDTO.setQuantity(displayShipWave.getQuantity());
		displayShipWaveDTO.setUserCreated(displayShipWave.getUserCreated());
		displayShipWaveDTO.setUserLastChanged(displayShipWave.getUserLastChanged());
		return displayShipWaveDTO;
	}

	public static DisplayLogisticsDTO convert(DisplayLogistics dl) {
		if (dl == null) return null;
		DisplayLogisticsDTO dlDTO = new DisplayLogisticsDTO();
		dlDTO.setDisplayLogisticsId(dl.getDisplayLogisticsId());
		dlDTO.setDisplayId(dl.getDisplayId());
		dlDTO.setDisplayHeight(dl.getDisplayHeight());
		dlDTO.setDisplayLength(dl.getDisplayLength());
		dlDTO.setDisplayWidth(dl.getDisplayWidth());
		dlDTO.setDisplayWeight(dl.getDisplayWeight());
		dlDTO.setDisplayPerLayer(dl.getDisplayPerLayer());
		dlDTO.setLayersPerPallet(dl.getLayersPerPallet());
		dlDTO.setPalletsPerTrailer(dl.getPalletsPerTrailer());
		dlDTO.setDoubleStackFlg(dl.isDoubleStackFlg());
		dlDTO.setSpecialLabetRqt(dl.getSpecialLabetRqt());
		dlDTO.setSerialShipFlg(dl.isSerialShipFlg());
		dlDTO.setDtCreated(dl.getDtCreated());
		dlDTO.setDtLastChanged(dl.getDtLastChanged());
		dlDTO.setUserCreated(dl.getUserCreated());
		dlDTO.setUserLastChanged(dl.getUserLastChanged());
		return dlDTO;
	}

	public static DisplayLogistics convert(DisplayLogisticsDTO dlDTO) {
		if (dlDTO == null) return null;
		DisplayLogistics dl = new DisplayLogistics();
		dl.setDisplayLogisticsId(dlDTO.getDisplayLogisticsId());
		dl.setDisplayId(dlDTO.getDisplayId());
		dl.setDisplayHeight(dlDTO.getDisplayHeight());
		dl.setDisplayLength(dlDTO.getDisplayLength());
		dl.setDisplayWidth(dlDTO.getDisplayWidth());
		dl.setDisplayWeight(dlDTO.getDisplayWeight());
		dl.setDisplayPerLayer(dlDTO.getDisplayPerLayer());
		dl.setLayersPerPallet(dlDTO.getLayersPerPallet());
		dl.setPalletsPerTrailer(dlDTO.getPalletsPerTrailer());
		dl.setDoubleStackFlg(dlDTO.isDoubleStackFlg());
		dl.setSpecialLabetRqt(dlDTO.getSpecialLabetRqt());
		dl.setSerialShipFlg(dlDTO.isSerialShipFlg());
		dl.setDtCreated(dlDTO.getDtCreated());
		dl.setDtLastChanged(dlDTO.getDtLastChanged());
		dl.setUserCreated(dlDTO.getUserCreated());
		dl.setUserLastChanged(dlDTO.getUserLastChanged());
		return dl;
	}

	public static CustomerMarketingDTO convert(CustomerMarketing customerMarketing) {
		if (customerMarketing == null) return null;
		CustomerMarketingDTO customerMarketingDTO = new CustomerMarketingDTO();
		customerMarketingDTO.setDisplayId(customerMarketing.getDisplayId());
		customerMarketingDTO.setCustomerMarketingId(customerMarketing.getCustomerMarketingId());
		customerMarketingDTO.setPriceAvailabilityDate(customerMarketing.getPriceAvailabilityDate());
		customerMarketingDTO.setSentSampleDate(customerMarketing.getSentSampleDate());
		customerMarketingDTO.setFinalArtworkDate(customerMarketing.getFinalArtworkDate());
		customerMarketingDTO.setPimCompletedDate(customerMarketing.getPimCompletedDate());
		customerMarketingDTO.setPricingAdminDate(customerMarketing.getPricingAdminDate());
		customerMarketingDTO.setIntialRendering(customerMarketing.isInitialRenderingFlg());
		customerMarketingDTO.setStructureApproved(customerMarketing.isStructureApprovedFlg());
		customerMarketingDTO.setOrderIn(customerMarketing.isOrderInFlg());
		customerMarketingDTO.setProjectLevelPct(customerMarketing.getProjectLevelPct());
		customerMarketingDTO.setComments(customerMarketing.getComments());
		customerMarketingDTO.setDtCreated(customerMarketing.getDtCreated());
		customerMarketingDTO.setDtLastChanged(customerMarketing.getDtLastChanged());
		customerMarketingDTO.setUserCreated(customerMarketing.getUserCreated());
		customerMarketingDTO.setUserLastChanged(customerMarketing.getUserLastChanged());
		return customerMarketingDTO;
	}

	public static CustomerMarketing convert(CustomerMarketingDTO customerMarketingDTO) {
		if (customerMarketingDTO == null) return null;
		CustomerMarketing customerMarketing = new CustomerMarketing();
		customerMarketing.setDisplayId(customerMarketingDTO.getDisplayId());
		customerMarketing.setCustomerMarketingId(customerMarketingDTO.getCustomerMarketingId());
		customerMarketing.setPriceAvailabilityDate(customerMarketingDTO.getPriceAvailabilityDate());
		customerMarketing.setSentSampleDate(customerMarketingDTO.getSentSampleDate());
		customerMarketing.setFinalArtworkDate(customerMarketingDTO.getFinalArtworkDate());
		customerMarketing.setPimCompletedDate(customerMarketingDTO.getPimCompletedDate());
		customerMarketing.setPricingAdminDate(customerMarketingDTO.getPricingAdminDate());
		customerMarketing.setInitialRenderingFlg(customerMarketingDTO.getInitialRendering());
		customerMarketing.setStructureApprovedFlg(customerMarketingDTO.getStructureApproved());
		customerMarketing.setOrderInFlg(customerMarketingDTO.getOrderIn());
		customerMarketing.setProjectLevelPct(customerMarketingDTO.getProjectLevelPct());
		customerMarketing.setComments(customerMarketingDTO.getComments());
		customerMarketing.setDtCreated(customerMarketingDTO.getDtCreated());
		customerMarketing.setDtLastChanged(customerMarketingDTO.getDtLastChanged());
		customerMarketing.setUserCreated(customerMarketingDTO.getUserCreated());
		customerMarketing.setUserLastChanged(customerMarketingDTO.getUserLastChanged());
		return customerMarketing;
	}

	public static KitComponentDTO convert(KitComponent kc) {
		if (kc == null) return null;
		KitComponentDTO kcDTO = new KitComponentDTO();
		try{
			kcDTO.setKitComponentId(kc.getKitComponentId());
			kcDTO.setDisplayId(kc.getDisplayId());
			kcDTO.setSkuId(kc.getSkuId());
			kcDTO.setQtyPerFacing(kc.getQtyPerFacing());
			kcDTO.setNumberFacings(kc.getNumberFacings());
			kcDTO.setPlantBreakCaseFlg(kc.isPlantBreakCaseFlg());
			kcDTO.setI2ForecastFlg(kc.isI2ForecastFlg());
			kcDTO.setRegCasePack(kc.getRegCasePack());
			kcDTO.setInvoiceUnitPrice(kc.getInvoiceUnitPrice());
			kcDTO.setCogsUnitPrice(kc.getCogsUnitPrice());
			kcDTO.setSequenceNum(kc.getSequenceNum());
			kcDTO.setSourcePimFlg(kc.isSourcePimFlg());
			kcDTO.setSku(kc.getSku());
			kcDTO.setProductName(kc.getProductName());
			kcDTO.setBuDesc(kc.getBuDesc());
			kcDTO.setVersionNo(kc.getVersionNo());
			kcDTO.setPromoFlg(kc.isPromoFlg());
			kcDTO.setBreakCase(kc.getBreakCase());
			kcDTO.setKitVersionCode(kc.getKitVersionCode());
			kcDTO.setPbcVersion(kc.getPbcVersion());
			kcDTO.setFinishedGoodsSku(kc.getFinishedGoodsSku());
			kcDTO.setFinishedGoodsQty(kc.getFinishedGoodsQty());
			kcDTO.setDtCreated(kc.getDtCreated());
			kcDTO.setDtLastChanged(kc.getDtLastChanged());
			kcDTO.setUserCreated(kc.getUserCreated());
			kcDTO.setUserLastChanged(kc.getUserLastChanged());
			kcDTO.setPriceException(kc.getPriceException());
			kcDTO.setBenchMarkPerUnit(kc.getBenchMarkPerUnit());
			kcDTO.setExcessInvtDollar(kc.getExcessInvtDollar());
			kcDTO.setExcessInvtPct(kc.getExcessInvtPct());
			kcDTO.setInPogYn(kc.getInPogYn());
			kcDTO.setPeReCalcDate(kc.getPeReCalcDate()==null?new Date():kc.getPeReCalcDate());
			kcDTO.setDisplayScenarioId(kc.getDisplayScenarioId());
			kcDTO.setOverrideExceptionPct(kc.getOverrideExceptionPct());
			kcDTO.setProgPrcnt(kc.getProgPrcnt());
		}catch(Exception ex){
			System.out.println("ConverterFactory:convert(KitComponent kc) error "+ ex.toString());
		}
		return kcDTO;
	}

	public static KitComponent convert(KitComponentDTO kcDTO) {
		if (kcDTO == null) return null;
		KitComponent kc = new KitComponent();
		try{
			kc.setKitComponentId(kcDTO.getKitComponentId());
			kc.setDisplayId(kcDTO.getDisplayId());
			kc.setSkuId(kcDTO.getSkuId());
			kc.setQtyPerFacing(kcDTO.getQtyPerFacing());
			kc.setNumberFacings(kcDTO.getNumberFacings());
			kc.setPlantBreakCaseFlg(kcDTO.isPlantBreakCaseFlg());
			kc.setI2ForecastFlg(kcDTO.isI2ForecastFlg());
			kc.setRegCasePack(kcDTO.getRegCasePack());
			kc.setInvoiceUnitPrice(kcDTO.getInvoiceUnitPrice());
			kc.setCogsUnitPrice(kcDTO.getCogsUnitPrice());
			kc.setSequenceNum(kcDTO.getSequenceNum());
			kc.setSourcePimFlg(kcDTO.isSourcePimFlg());
			kc.setSku(kcDTO.getSku());
			kc.setProductName(kcDTO.getProductName());
			kc.setBuDesc(kcDTO.getBuDesc());
			kc.setVersionNo(kcDTO.getVersionNo());
			kc.setPromoFlg(kcDTO.isPromoFlg());
			kc.setBreakCase(kcDTO.getBreakCase());
			kc.setKitVersionCode(kcDTO.getKitVersionCode());
			kc.setPbcVersion(kcDTO.getPbcVersion());
			kc.setFinishedGoodsSku(kcDTO.getFinishedGoodsSku());
			kc.setFinishedGoodsQty(kcDTO.getFinishedGoodsQty());
			kc.setDtCreated(kcDTO.getDtCreated());
			kc.setDtLastChanged(kcDTO.getDtLastChanged());
			kc.setUserCreated(kcDTO.getUserCreated());
			kc.setUserLastChanged(kcDTO.getUserLastChanged());
			kc.setPriceException(kcDTO.getPriceException());
			kc.setBenchMarkPerUnit(kcDTO.getBenchMarkPerUnit());
			kc.setExcessInvtDollar(kcDTO.getExcessInvtDollar());
			kc.setExcessInvtPct(kcDTO.getExcessInvtPct());
			kc.setInPogYn(kcDTO.getInPogYn());
			kc.setPeReCalcDate(kcDTO.getPeReCalcDate()==null?new Date():kcDTO.getPeReCalcDate());
			kc.setDisplayScenarioId(kcDTO.getDisplayScenarioId());
			kc.setOverrideExceptionPct(kcDTO.getOverrideExceptionPct());
			kc.setProgPrcnt(kcDTO.getProgPrcnt());
		}catch(Exception ex){
			System.out.println("ConverterFactory:convert(KitComponentDTO kcDTO) error "+ ex.toString());
		}
		return kc;
	}

	public static List<KitComponentDTO> convertKitComponents(List<KitComponent> kitComponents) {
		if (kitComponents == null) return null;
		List<KitComponentDTO> kitComponentDTOs = new ArrayList<KitComponentDTO>();
		for (int i = 0; i < kitComponents.size(); i++) {
			kitComponentDTOs.add(convert(kitComponents.get(i)));
		}
		return kitComponentDTOs;
	}

	public static DisplayPlannerDTO convert(DisplayPlanner dp) {
		if (dp == null) return null;
		DisplayPlannerDTO dpDTO = new DisplayPlannerDTO();
		try{
			dpDTO.setDisplayPlannerId(dp.getDisplayPlannerId());
			dpDTO.setKitComponentId(dp.getKitComponentId());
			dpDTO.setFirstName(dp.getFirstName());
			dpDTO.setLastName(dp.getLastName());
			dpDTO.setInitials(dp.getInitials());
			dpDTO.setPlannerCode(dp.getPlannerCode());
			dpDTO.setPlantCode(dp.getPlantCode());
			dpDTO.setLeadTime(dp.getLeadTime());
			dpDTO.setPlantOverrideFlg(dp.isPlantOverrideFlg());
			dpDTO.setDisplayDcId(dp.getDisplayDcId());
			dpDTO.setDtCreated(dp.getDtCreated());
			dpDTO.setDtLastChanged(dp.getDtLastChanged());
			dpDTO.setUserCreated(dp.getUserCreated());
			dpDTO.setUserLastChanged(dp.getUserLastChanged());
		}catch(Exception ex){
			System.out.println("ConverterFactory:convert(DisplayPlanner dp) error "+ ex.toString());
		}
		return dpDTO;
	}

	public static DisplayPlanner convert(DisplayPlannerDTO dpDTO) {
		if (dpDTO == null) return null;
		DisplayPlanner dp = new DisplayPlanner();
		try{
			dp.setDisplayPlannerId(dpDTO.getDisplayPlannerId());
			dp.setKitComponentId(dpDTO.getKitComponentId());
			dp.setFirstName(dpDTO.getFirstName());
			dp.setLastName(dpDTO.getLastName());
			dp.setInitials(dpDTO.getInitials());
			dp.setPlannerCode(dpDTO.getPlannerCode());
			dp.setPlantCode(dpDTO.getPlantCode());
			dp.setLeadTime(dpDTO.getLeadTime());
			dp.setPlantOverrideFlg(dpDTO.isPlantOverrideFlg());
			dp.setDisplayDcId(dpDTO.getDisplayDcId());
			dp.setDtCreated(dpDTO.getDtCreated());
			dp.setDtLastChanged(dpDTO.getDtLastChanged());
			dp.setUserCreated(dpDTO.getUserCreated());
			dp.setUserLastChanged(dpDTO.getUserLastChanged());
		}catch(Exception ex){
			System.out.println("ConverterFactory:convert(DisplayPlannerDTO dpDTO) error "+ ex.toString());
		}
		return dp;
	}

	public static List<DisplayPlannerDTO> convertDisplayPlanners(List<DisplayPlanner> displayPlanners) {
		if (displayPlanners == null) return null;
		List<DisplayPlannerDTO> displayPlannerDTOs = new ArrayList<DisplayPlannerDTO>();
		for (int i = 0; i < displayPlanners.size(); i++) {
			displayPlannerDTOs.add(convert(displayPlanners.get(i)));
		}
		return displayPlannerDTOs;
	}

	public static ProductDetailDTO convert(ProductDetail pd) {
		if (pd == null) return null;
		ProductDetailDTO pdDTO = new ProductDetailDTO();
		pdDTO.setSkuId(pd.getSkuId());
		pdDTO.setSku(pd.getSku());
		pdDTO.setProductName(pd.getProductName());
		pdDTO.setBuDesc(pd.getBuDesc());
		pdDTO.setVersionNo(pd.getVersionNo());
		pdDTO.setPromoFlg(pd.isPromoFlg());
		pdDTO.setBreakCase(pd.getBreakCase());
		pdDTO.setBenchMarkPrice(pd.getBenchMarkPrice());
		pdDTO.setListPrice(pd.getListPrice());
		pdDTO.setFullBurdenCost(pd.getMaterialCost() + pd.getLaborCost());
		pdDTO.setInnerpackQty(pd.getInnerpackQty());
		pdDTO.setIlsSkuDesc(pd.getIlsSkuDesc());
		pdDTO.setCanBenchMarkPrice(pd.getCanBenchMarkPrice());
		pdDTO.setCanListPrice(pd.getCanListPrice());
		pdDTO.setCanFullBurdenCost(pd.getCanMaterialCost() + pd.getCanLaborCost());
		return pdDTO;
	}

	public static List<ProductDetailDTO> convertProductDetails(List<ProductDetail> productDetails) {
		if (productDetails == null) return null;
		List<ProductDetailDTO> productDetailDTOs = new ArrayList<ProductDetailDTO>();
		for (int i = 0; i < productDetails.size(); i++) {
			productDetailDTOs.add(convert(productDetails.get(i)));
		}
		return productDetailDTOs;
	}

	public static LocationDTO convert(Location a) {
		if (a == null) return null;
		LocationDTO b = new LocationDTO();
		b.setActiveFlg(a.isActiveFlg());
		b.setLocationCode(a.getLocationCode());
		b.setLocationId(a.getLocationId());
		b.setLocationName(a.getLocationName());
		return b;
	}

	public static List<LocationDTO> convertLocationList(List<Location> locationList) {
		if (locationList == null) return null;
		List<LocationDTO> locationDTOList = new ArrayList<LocationDTO>();
		for (int i = 0; i < locationList.size(); i++) {
			locationDTOList.add(convert(locationList.get(i)));
		}
		return locationDTOList;
	}

	public static LocationCountryDTO convert(LocationCountry a) {
		if (a == null) return null;
		LocationCountryDTO b = new LocationCountryDTO();
		b.setActiveFlg(a.isActiveFlg());
		b.setLocationCode(a.getLocationCode());
		b.setLocationId(a.getLocationId());
		b.setLocationName(a.getLocationName());
		b.setCountryId(a.getCountryId());
		b.setCountryName(a.getCountryName());
		return b;
	}
	
	public static List<LocationCountryDTO> convertLocationCountryList(List<LocationCountry> locationCountryList) {
		if (locationCountryList == null) return null;
		List<LocationCountryDTO> locationCountryDTOList = new ArrayList<LocationCountryDTO>();
		for (int i = 0; i < locationCountryList.size(); i++) {
			locationCountryDTOList.add(convert(locationCountryList.get(i)));
		}
		return locationCountryDTOList;
	}
	
	public static Map<Long, DisplayDcDTO> convertDcMap(Map<Long, DisplayDc> displayDcMap) {
		if (displayDcMap == null) return null;
		Map<Long, DisplayDcDTO> dtoMap = new HashMap<Long, DisplayDcDTO>();
		Iterator<Long> ki = displayDcMap.keySet().iterator();
		while (ki.hasNext()) {
			Long key = ki.next();
			dtoMap.put(key, ConverterFactory.convert(displayDcMap.get(key)));
		}
		return dtoMap;
	}

	public static DisplayDcDTO convert(DisplayDc displayDc) {
		DisplayDcDTO displayDcDTO = new DisplayDcDTO();
		displayDcDTO.setDisplayDcId(displayDc.getDisplayDcId());
		displayDcDTO.setLocationId(displayDc.getLocationId());
		displayDcDTO.setDtCreated(displayDc.getDtCreated());
		displayDcDTO.setDisplayId(displayDc.getDisplayId());
		displayDcDTO.setDtLastChanged(displayDc.getDtLastChanged());
		displayDcDTO.setQuantity(displayDc.getQuantity());
		displayDcDTO.setUserCreated(displayDc.getUserCreated());
		displayDcDTO.setUserLastChanged(displayDcDTO.getUserLastChanged());
		return displayDcDTO;
	}

	public static DisplayDc convert(DisplayDcDTO displayDcDTO) {
		DisplayDc displayDc = new DisplayDc();
		displayDc.setDisplayId(displayDcDTO.getDisplayId());
		displayDc.setDisplayDcId(displayDcDTO.getDisplayDcId());
		displayDc.setLocationId(displayDcDTO.getLocationId());
		displayDc.setQuantity(displayDcDTO.getQuantity());
		displayDc.setDtCreated(displayDcDTO.getDtCreated());
		displayDc.setDtLastChanged(displayDcDTO.getDtLastChanged());
		displayDc.setUserCreated(displayDcDTO.getUserCreated());
		displayDc.setUserLastChanged(displayDcDTO.getUserLastChanged());
		return displayDc;
	}

	public static DisplayShipWave convert(DisplayShipWaveDTO displayShipWaveDTO) {
		if (displayShipWaveDTO == null) return null;
		DisplayShipWave displayShipWave = new DisplayShipWave();
		displayShipWave.setDisplayId(displayShipWaveDTO.getDisplayId());
		displayShipWave.setDisplayShipWaveId(displayShipWaveDTO.getDisplayShipWaveId());
		displayShipWave.setDtCreated(displayShipWaveDTO.getDtCreated());
		displayShipWave.setDtLastChanged(displayShipWaveDTO.getDtLastChanged());
		displayShipWave.setMustArriveDate(displayShipWaveDTO.getMustArriveDate());
		displayShipWave.setQuantity(displayShipWaveDTO.getQuantity());
		displayShipWave.setShipWaveNum(displayShipWaveDTO.getShipWaveNum());
		displayShipWave.setDestinationFlg(displayShipWaveDTO.isDestinationFlg());
		displayShipWave.setUserCreated(displayShipWaveDTO.getUserCreated());
		displayShipWave.setUserLastChanged(displayShipWaveDTO.getUserLastChanged());
		return displayShipWave;
	}

	public static List<DisplayShipWaveDTO> convertDisplayShipWavesList(List<DisplayShipWave> displayShipWaveList) {
		if (displayShipWaveList == null) return null;
		List<DisplayShipWaveDTO> displayShipWaveDTOList = new ArrayList<DisplayShipWaveDTO>();
		for (int i = 0; i < displayShipWaveList.size(); i++) {
			displayShipWaveDTOList.add(convert((displayShipWaveList.get(i))));
		}
		return displayShipWaveDTOList;

	}

	public static DisplayPackout convert(DisplayPackoutDTO displayPackoutDTO) {
		if (displayPackoutDTO == null) return null;
		DisplayPackout displayPackout = new DisplayPackout();
		displayPackout.setDisplayPackoutId(displayPackoutDTO.getDisplayPackoutId());
		displayPackout.setDisplayDcId(displayPackoutDTO.getDisplayDcId());
		displayPackout.setDisplayShipWaveId(displayPackoutDTO.getDisplayShipWaveId());
		displayPackout.setShipFromDate(displayPackoutDTO.getShipFromDate());
		displayPackout.setCorrugateDueDate(displayPackoutDTO.getCorrugateDueDate());
		displayPackout.setDeliveryDate(displayPackoutDTO.getDeliveryDate());
		displayPackout.setShipLocProductDueDate(displayPackoutDTO.getShipLocProductDueDate());
		displayPackout.setProductDueDate(displayPackoutDTO.getProductDueDate());
		displayPackout.setQuantity(displayPackoutDTO.getQuantity());
		displayPackout.setDtCreated(displayPackoutDTO.getDtCreated());
		displayPackout.setDtLastChanged(displayPackoutDTO.getDtLastChanged());
		displayPackout.setShipFromLocationId(displayPackoutDTO.getShipFromLocationId());
		displayPackout.setUserCreated(displayPackoutDTO.getUserCreated());
		displayPackout.setUserLastChanged(displayPackoutDTO.getUserLastChanged());
		return displayPackout;
	}

	public static DisplayPackoutDTO convert(DisplayPackout displayPackout) {
		if (displayPackout == null) return null;
		DisplayPackoutDTO displayPackoutDTO = new DisplayPackoutDTO();
		displayPackoutDTO.setDisplayPackoutId(displayPackout.getDisplayPackoutId());
		displayPackoutDTO.setDisplayDcId(displayPackout.getDisplayDcId());
		displayPackoutDTO.setDisplayShipWaveId(displayPackout.getDisplayShipWaveId());
		displayPackoutDTO.setShipFromDate(displayPackout.getShipFromDate());
		displayPackoutDTO.setCorrugateDueDate(displayPackout.getCorrugateDueDate());
		displayPackoutDTO.setDeliveryDate(displayPackout.getDeliveryDate());
		displayPackoutDTO.setShipLocProductDueDate(displayPackout.getShipLocProductDueDate());
		displayPackoutDTO.setProductDueDate(displayPackout.getProductDueDate());
		displayPackoutDTO.setQuantity(displayPackout.getQuantity());
		displayPackoutDTO.setDtCreated(displayPackout.getDtCreated());
		displayPackoutDTO.setDtLastChanged(displayPackout.getDtLastChanged());
		displayPackoutDTO.setUserCreated(displayPackout.getUserCreated());
		displayPackoutDTO.setUserLastChanged(displayPackout.getUserLastChanged());
		displayPackoutDTO.setShipFromLocationId(displayPackout.getShipFromLocationId());
		if (displayPackout.getDisplayShipWaveByDisplayShipWaveId() == null) return displayPackoutDTO;
		displayPackoutDTO.setMustArriveDate(displayPackout.getDisplayShipWaveByDisplayShipWaveId().getMustArriveDate());
		displayPackoutDTO.setShipWaveNum(displayPackout.getDisplayShipWaveByDisplayShipWaveId().getShipWaveNum());
		return displayPackoutDTO;
	}

	public static List<DisplayPackoutDTO> convertDisplayPackouts(List<DisplayPackout> displayPackoutList) {
		List<DisplayPackoutDTO> displayPackoutDTOList = new ArrayList<DisplayPackoutDTO>();
		for (int i = 0; i < displayPackoutList.size(); i++) {
			displayPackoutDTOList.add(convert(displayPackoutList.get(i)));
		}
		return displayPackoutDTOList;
	}


	public static ValidPlantDTO convert(ValidPlant plant) {
		if (plant == null) return null;
		ValidPlantDTO validPlantDTO = new ValidPlantDTO();
		validPlantDTO.setPlantCode(plant.getPlantCode());
		validPlantDTO.setPlantDesc(plant.getPlantDesc());
		validPlantDTO.setActiveFlg(plant.isActiveFlg());
		validPlantDTO.setActiveOnFinanceFlg(plant.isActiveOnFinanceFlg());
		return validPlantDTO;
	}

	public static List<ValidPlantDTO> convertPlants(List<ValidPlant> plants) {
		if (plants == null) return null;
		List<ValidPlantDTO> plantDTOs = new ArrayList<ValidPlantDTO>();
		for (int i = 0; i < plants.size(); i++) {
			plantDTOs.add(convert(plants.get(i)));
		}
		return plantDTOs;
	}

	public static ValidPlannerDTO convert(ValidPlanner planner) {
		if (planner == null) return null;
		ValidPlannerDTO validPlannerDTO = new ValidPlannerDTO();
		validPlannerDTO.setPlannerCode(planner.getPlannerCode());
		validPlannerDTO.setPlannerDesc(planner.getPlannerDesc());
		validPlannerDTO.setActiveFlg(planner.isActiveFlg());
		return validPlannerDTO;
	}

	public static List<ValidPlannerDTO> convertPlanners(List<ValidPlanner> planners) {
		if (planners == null) return null;
		List<ValidPlannerDTO> plannerDTOs = new ArrayList<ValidPlannerDTO>();
		for (int i = 0; i < planners.size(); i++) {
			plannerDTOs.add(convert(planners.get(i)));
		}
		return plannerDTOs;
	}

	public static ValidLocationDTO convert(ValidLocation location) {
		if (location == null) return null;
		ValidLocationDTO validLocationDTO = new ValidLocationDTO();
		validLocationDTO.setDcCode(location.getDcCode());
		validLocationDTO.setDcName(location.getDcName());
		validLocationDTO.setActiveFlg(location.isActiveFlg());
		return validLocationDTO;
	}

	public static List<ValidLocationDTO> convertLocations(List<ValidLocation> locations) {
		if (locations == null) return null;
		List<ValidLocationDTO> locationDTOs = new ArrayList<ValidLocationDTO>();
		for (int i = 0; i < locations.size(); i++) {
			locationDTOs.add(convert(locations.get(i)));
		}
		return locationDTOs;
	}

	public static ValidBuDTO convert(ValidBu bu) {
		if (bu == null) return null;
		ValidBuDTO validBuDTO = new ValidBuDTO();
		validBuDTO.setBuCode(bu.getBuCode());
		validBuDTO.setBuDesc(bu.getBuDesc());
		validBuDTO.setActiveFlg(bu.isActiveFlg());
		validBuDTO.setProductCategoryCode(bu.getProductCategoryCode());
		return validBuDTO;
	}

	public static List<ValidBuDTO> convertBus(List<ValidBu> bus) {
		if (bus == null) return null;
		List<ValidBuDTO> buDTOs = new ArrayList<ValidBuDTO>();
		for (int i = 0; i < bus.size(); i++) {
			buDTOs.add(convert(bus.get(i)));
		}
		return buDTOs;
	}

	public static List<ManufacturerDTO> convertManufacturerList(List<Manufacturer> manufacturers) {
		if(manufacturers==null) return null;
		List<ManufacturerDTO> manufacturerDTOs = new ArrayList<ManufacturerDTO>();
		for(int i =0; i < manufacturers.size(); i++){
			manufacturerDTOs.add(convert(manufacturers.get(i)));
		}
		return manufacturerDTOs;
	}

	public static ManufacturerDTO convert(Manufacturer manufacturer) {
		ManufacturerDTO manufacturerDTO = new ManufacturerDTO();
		manufacturerDTO.setManufacturerId(manufacturer.getManufacturerId());
		manufacturerDTO.setName(manufacturer.getName());
		return manufacturerDTO;
	}

	public static Manufacturer convert(ManufacturerDTO manufacturerDTO) {
		Manufacturer manufacturer = new Manufacturer();
		manufacturer.setManufacturerId(manufacturerDTO.getManufacturerId());
		manufacturer.setName(manufacturerDTO.getName());
		return manufacturer;
	}

	
	public static List<ManufacturerCountryDTO> convertManufacturerCountryList(List<ManufacturerCountry> manufacturersCountry) {
		if(manufacturersCountry==null) return null;
		List<ManufacturerCountryDTO> manufacturerCountryDTOs = new ArrayList<ManufacturerCountryDTO>();
		for(int i =0; i < manufacturersCountry.size(); i++){
			manufacturerCountryDTOs.add(convert(manufacturersCountry.get(i)));
		}
		return manufacturerCountryDTOs;
	}

	public static ManufacturerCountryDTO convert(ManufacturerCountry manufacturerCountry) {
		ManufacturerCountryDTO manufacturerCountryDTO = new ManufacturerCountryDTO();
		manufacturerCountryDTO.setManufacturerId(manufacturerCountry.getManufacturerId());
		manufacturerCountryDTO.setName(manufacturerCountry.getName());
		manufacturerCountryDTO.setCountryId(manufacturerCountry.getCountryId());
		manufacturerCountryDTO.setCountryName(manufacturerCountry.getCountryName());
		manufacturerCountryDTO.setActive(manufacturerCountry.isActiveFlg());
		return manufacturerCountryDTO;
	}

	public static ManufacturerCountry convert(ManufacturerCountryDTO manufacturerCountryDTO) {
		ManufacturerCountry manufacturerCountry = new ManufacturerCountry();
		manufacturerCountry.setManufacturerId(manufacturerCountryDTO.getManufacturerId());
		manufacturerCountry.setName(manufacturerCountryDTO.getName());
		manufacturerCountry.setCountryId(manufacturerCountryDTO.getCountryId());
		manufacturerCountry.setCountryName(manufacturerCountryDTO.getCountryName());
		manufacturerCountry.setActiveCountryFlg(manufacturerCountryDTO.isActive());
		return manufacturerCountry;
	}

	public static List<CostAnalysisKitComponentDTO> convertCostAnalysisKitComponents(List<CostAnalysisKitComponent> costAnalysisKitComponents) {
		if (costAnalysisKitComponents == null) return null;
		List<CostAnalysisKitComponentDTO> costAnalysisKitComponentDTOs = new ArrayList<CostAnalysisKitComponentDTO>();
		try{
			for (int i = 0; i < costAnalysisKitComponents.size(); i++) {
				costAnalysisKitComponentDTOs.add(convert(costAnalysisKitComponents.get(i)));
			}
		}catch(Exception ex){
			System.out.println("ConvererFactory:convertCostAnalysisKitComponents error "+ ex.toString());
		}
		return costAnalysisKitComponentDTOs;
	}

	public static List<CostAnalysisKitComponentDTO> updateCostAnalysisKitComponentDTOInfo(List<CostAnalysisKitComponentDTO> costAnalysisDTOList,Long qtyForeCase, double customerProgramPct) {
		if (costAnalysisDTOList == null) {
			return null;
		}
		List<CostAnalysisKitComponentDTO> calculatedCostAnalysisKitComponents = new ArrayList<CostAnalysisKitComponentDTO>();
		try{
			for (int i = 0; i < costAnalysisDTOList.size(); i++) {
				costAnalysisDTOList.get(i).updateCalculatedFields(qtyForeCase, customerProgramPct);
				calculatedCostAnalysisKitComponents.add(costAnalysisDTOList.get(i));
			}
		}catch(Exception ex){
			System.out.println("ConvererFactory:updateCostAnalysisKitComponentDTOInfo error "+ ex.toString());
		}
		return calculatedCostAnalysisKitComponents;
	}

	public static List<CorrugateComponentsDetailDTO> updateCorrugateComponentDTOInfo(List<CorrugateComponentsDetailDTO> corrugateCompDTOList, DisplayDTO displayDTO) {
		if (corrugateCompDTOList == null) {
			return null;
		}
		List<CorrugateComponentsDetailDTO> calculatedCorrugateComponents = new ArrayList<CorrugateComponentsDetailDTO>();
		try{
			for (int i = 0; i < corrugateCompDTOList.size(); i++) {
				corrugateCompDTOList.get(i).updateCalculatedFields(displayDTO.getActualQty());
				calculatedCorrugateComponents.add(corrugateCompDTOList.get(i));
			}
		}catch(Exception ex){
			System.out.println("ConvererFactory:updateCostAnalysisKitComponentDTOInfo error "+ ex.toString());
		}
		return calculatedCorrugateComponents;
	}
	
	public static List<CostAnalysisKitComponent> convertCostAnalysisKitComponentDTOs(List<CostAnalysisKitComponentDTO> costAnalysisKitComponentDTOs) {
		if (costAnalysisKitComponentDTOs == null) return null;
		List<CostAnalysisKitComponent> costAnalysisKitComponents = new ArrayList<CostAnalysisKitComponent>();
		try{
			for (int i = 0; i < costAnalysisKitComponentDTOs.size(); i++) {
				costAnalysisKitComponents.add(convert(costAnalysisKitComponentDTOs.get(i)));
			}
		}catch(Exception ex){
			System.out.println("ConvererFactory:convertCostAnalysisKitComponentDTOs error " + ex.toString());
		}
		return costAnalysisKitComponents;
	}


	public static CostAnalysisKitComponentDTO convert(CostAnalysisKitComponent costAnalysisKc) {
		if (costAnalysisKc == null) return null;
		CostAnalysisKitComponentDTO costAnalysisKcDTO=null;
		Double pctFixedValue=100.00d;
		try{
			costAnalysisKcDTO = new CostAnalysisKitComponentDTO();
			costAnalysisKcDTO.setKitComponentId(costAnalysisKc.getKitComponentId());
			costAnalysisKcDTO.setDisplayId(costAnalysisKc.getDisplayId());
			costAnalysisKcDTO.setSequenceNum(costAnalysisKc.getSequenceNum());
			costAnalysisKcDTO.setBuDesc(costAnalysisKc.getBuDesc());
			costAnalysisKcDTO.setBuCode(costAnalysisKc.getBuCode());
			costAnalysisKcDTO.setRegCasePack(costAnalysisKc.getRegCasePack());
			costAnalysisKcDTO.setSourcePimFlg(costAnalysisKc.getSourcePimFlg().equalsIgnoreCase("Y")?true:false);
			costAnalysisKcDTO.setPromoFlg(costAnalysisKc.getPromoFlg().equalsIgnoreCase("Y")?true:false);
			costAnalysisKcDTO.setI2ForecastFlg(costAnalysisKc.getI2ForecastFlg().equalsIgnoreCase("Y")?true:false);
			costAnalysisKcDTO.setPlantBreakCaseFlg(costAnalysisKc.getPlantBreakCaseFlg().equalsIgnoreCase("Y")?true:false);
			costAnalysisKcDTO.setPbcVersion(costAnalysisKc.getPbcVersion());
			costAnalysisKcDTO.setQtyPerFacing(costAnalysisKc.getQtyPerFacing());
			costAnalysisKcDTO.setNumberFacings(costAnalysisKc.getNumberFacings());
			costAnalysisKcDTO.setQtyPerDisplay(costAnalysisKc.getQtyPerFacing());
			costAnalysisKcDTO.setInventoryReq(costAnalysisKc.getInventoryReq());
			costAnalysisKcDTO.setPriceException(costAnalysisKc.getPriceException());
			costAnalysisKcDTO.setActualInvoicePerEa(costAnalysisKc.getActualInvoicePerEa());
			Double totalActualInvoiceVal=costAnalysisKc.getTotalActualInvoice();
			if(totalActualInvoiceVal==null){
				totalActualInvoiceVal=0.0;
			}else{
				totalActualInvoiceVal=costAnalysisKc.getTotalActualInvoice();
			}
			Double totalActualInvoiceWprgmVal=costAnalysisKc.getTotalActualInvoiceWpgrm();
			if(totalActualInvoiceVal==null){
				totalActualInvoiceVal=0.0;
			}else{
				totalActualInvoiceWprgmVal=costAnalysisKc.getTotalActualInvoiceWpgrm();
			}
			costAnalysisKcDTO.setTotalActualInvoice(totalActualInvoiceVal);
			costAnalysisKcDTO.setTotalActualInvoiceWpgrm(totalActualInvoiceWprgmVal);
			costAnalysisKcDTO.setCogsUnitPrice(costAnalysisKc.getCogsUnitPrice());
			costAnalysisKcDTO.setTotalCogsUnitPrice(costAnalysisKc.getTotalCogsUnitPrice());
			costAnalysisKcDTO.setNetVmAmount(costAnalysisKc.getNetVmAmount());
			costAnalysisKcDTO.setNetVmPct(costAnalysisKc.getNetVmPct());
			costAnalysisKcDTO.setBenchMarkPerUnit(costAnalysisKc.getBenchMarkPerUnit());
			costAnalysisKcDTO.setTotalBenchMark(costAnalysisKc.getTotalBenchMark());
			costAnalysisKcDTO.setInvoiceUnitPrice(costAnalysisKc.getInvoiceUnitPrice());
			costAnalysisKcDTO.setTotalInvoiceUnitPrice(costAnalysisKc.getTotalInvoiceUnitPrice());
			costAnalysisKcDTO.setSku(costAnalysisKc.getSku());
			costAnalysisKcDTO.setProductName(costAnalysisKc.getProductName());
			costAnalysisKcDTO.setChkDgt(costAnalysisKc.getChkDgt());
			costAnalysisKcDTO.setPeReCalcDate(costAnalysisKc.getPeReCalcDate());
			Double excessInvtPctVal=0.0;
			if(costAnalysisKc.getExcessInvtPct()==null){
				excessInvtPctVal=0.0;
			}else{
				excessInvtPctVal=costAnalysisKc.getExcessInvtPct();
			}
			costAnalysisKcDTO.setExcessInvtPct(excessInvtPctVal);
			Double excessInvtDollarVal=(pctFixedValue-excessInvtPctVal/pctFixedValue)*totalActualInvoiceVal;
			costAnalysisKcDTO.setExcessInvtDollar( excessInvtDollarVal);
			if(costAnalysisKc.getInPogYn()!=null){
				costAnalysisKcDTO.setInPogYn(costAnalysisKc.getInPogYn().equalsIgnoreCase("Y")?true:false);
			}else{
				costAnalysisKcDTO.setInPogYn(false);
			}
			costAnalysisKcDTO.setEstimatedProgramPct(costAnalysisKc.getEstimatedProgramPct());
			costAnalysisKcDTO.setOverrideExcPct(costAnalysisKc.getOverrideExcPct());
			costAnalysisKcDTO.setInvoiceWithProgram(costAnalysisKc.getInvoiceWithProgram());
			costAnalysisKcDTO.setNetVmAmountBp(costAnalysisKc.getNetVmAmountBp());
			costAnalysisKcDTO.setNetVmPctBp(costAnalysisKc.getNetVmPctBp());
			costAnalysisKcDTO.setNetVmAmount(costAnalysisKc.getNetVmAmount());
			costAnalysisKcDTO.setNetVmPct(costAnalysisKc.getNetVmPct());
		}catch(Exception ex){
		}
		return costAnalysisKcDTO;
	}

	public static CostAnalysisKitComponent convert(CostAnalysisKitComponentDTO costAnalysisKcDTO) {
		if (costAnalysisKcDTO == null) return null;
		CostAnalysisKitComponent costAnalysisKc =null;
		Double pctFixedValue=100.00d;
		try{
			costAnalysisKc = new CostAnalysisKitComponent();
			costAnalysisKc.setKitComponentId(costAnalysisKcDTO.getKitComponentId());
			costAnalysisKc.setDisplayId(costAnalysisKcDTO.getDisplayId());
			costAnalysisKc.setSequenceNum(costAnalysisKcDTO.getSequenceNum());
			costAnalysisKc.setBuDesc(costAnalysisKcDTO.getBuDesc());
			costAnalysisKc.setBuCode(costAnalysisKcDTO.getBuCode());
			costAnalysisKc.setRegCasePack(costAnalysisKcDTO.getRegCasePack());
			costAnalysisKc.setSourcePimFlg(costAnalysisKcDTO.getSourcePimFlg()?"Y":"N");
			costAnalysisKc.setPromoFlg(costAnalysisKcDTO.getPromoFlg()?"Y":"N");
			costAnalysisKc.setI2ForecastFlg(costAnalysisKcDTO.getI2ForecastFlg()?"Y":"N");
			costAnalysisKc.setPlantBreakCaseFlg(costAnalysisKcDTO.getPlantBreakCaseFlg()?"Y":"N");
			costAnalysisKc.setPbcVersion(costAnalysisKcDTO.getPbcVersion());
			costAnalysisKc.setQtyPerFacing(costAnalysisKcDTO.getQtyPerFacing());
			costAnalysisKc.setNumberFacings(costAnalysisKcDTO.getNumberFacings());
			costAnalysisKc.setQtyPerDisplay(costAnalysisKcDTO.getQtyPerFacing());
			costAnalysisKc.setInventoryReq(costAnalysisKcDTO.getInventoryReq());
			costAnalysisKc.setPriceException(costAnalysisKcDTO.getPriceException());
			costAnalysisKc.setActualInvoicePerEa(costAnalysisKcDTO.getActualInvoicePerEa());
			Double totalActualInvoiceVal=costAnalysisKcDTO.getTotalActualInvoice();
			if(totalActualInvoiceVal==null){
				totalActualInvoiceVal=0.0;
			}else{
				totalActualInvoiceVal=costAnalysisKcDTO.getTotalActualInvoice();
			}
			Double totalActualInvoiceWPgrmVal=costAnalysisKcDTO.getTotalActualInvoiceWpgrm();
			if(totalActualInvoiceWPgrmVal==null){
				totalActualInvoiceWPgrmVal=0.0;
			}else{
				totalActualInvoiceWPgrmVal=costAnalysisKcDTO.getTotalActualInvoiceWpgrm();
			}
			costAnalysisKc.setTotalActualInvoice(totalActualInvoiceVal);
			costAnalysisKc.setTotalActualInvoiceWpgrm(totalActualInvoiceWPgrmVal);
			costAnalysisKc.setCogsUnitPrice(costAnalysisKcDTO.getCogsUnitPrice());
			costAnalysisKc.setTotalCogsUnitPrice(costAnalysisKcDTO.getTotalCogsUnitPrice());
			costAnalysisKc.setNetVmAmount(costAnalysisKcDTO.getNetVmAmount());
			costAnalysisKc.setNetVmPct(costAnalysisKcDTO.getNetVmPct());
			costAnalysisKc.setBenchMarkPerUnit(costAnalysisKcDTO.getBenchMarkPerUnit());
			costAnalysisKc.setTotalBenchMark(costAnalysisKcDTO.getTotalBenchMark());
			costAnalysisKc.setInvoiceUnitPrice(costAnalysisKcDTO.getInvoiceUnitPrice());
			costAnalysisKc.setTotalInvoiceUnitPrice(costAnalysisKcDTO.getTotalInvoiceUnitPrice());
			costAnalysisKc.setSku(costAnalysisKcDTO.getSku());
			costAnalysisKc.setProductName(costAnalysisKcDTO.getProductName());
			costAnalysisKc.setChkDgt(costAnalysisKcDTO.getChkDgt());
			costAnalysisKc.setPeReCalcDate(costAnalysisKcDTO.getPeReCalcDate());
			
			costAnalysisKc.setNetVmAmountBp(costAnalysisKcDTO.getNetVmAmountBp());
			costAnalysisKc.setNetVmPctBp(costAnalysisKcDTO.getNetVmPctBp());
			
			costAnalysisKc.setNetVmAmount(costAnalysisKcDTO.getNetVmAmount());
			costAnalysisKc.setNetVmPct(costAnalysisKcDTO.getNetVmPct());
			
			
			Double excessInvtPctVal=0.0;
			if(costAnalysisKcDTO.getExcessInvtPct()==null){
				excessInvtPctVal=0.0;
			}else{
				excessInvtPctVal=costAnalysisKcDTO.getExcessInvtPct();
			}
			costAnalysisKc.setExcessInvtPct(excessInvtPctVal);
			costAnalysisKc.setExcessInvtDollar((pctFixedValue-excessInvtPctVal/pctFixedValue)*totalActualInvoiceVal);
			if(costAnalysisKcDTO.getInPogYn()!=null){
				costAnalysisKc.setInPogYn(costAnalysisKcDTO.getInPogYn()?"Y":"N");
			}else{
				costAnalysisKc.setInPogYn("N");
			}
			costAnalysisKc.setEstimatedProgramPct(costAnalysisKcDTO.getEstimatedProgramPct());
			costAnalysisKc.setOverrideExcPct(costAnalysisKcDTO.getOverrideExcPct());
			costAnalysisKc.setInvoiceWithProgram(costAnalysisKcDTO.getInvoiceWithProgram());
		}catch(Exception ex){
			System.out.println("ConvererFactory:convert(CostAnalysisKitComponentDTO costAnalysisKcDTO) error " + ex.toString());
		}
		return costAnalysisKc;
	}

	public static List<CorrugateComponentsDetail> convertCorrComponentDTOs(List<CorrugateComponentsDetailDTO> corrugateDTOList) {
		if (corrugateDTOList == null) return null;
		List<CorrugateComponentsDetail> corrugateComponents = new ArrayList<CorrugateComponentsDetail>();
		try{
			for (int i = 0; i < corrugateDTOList.size(); i++) {
				corrugateComponents.add(convert(corrugateDTOList.get(i)));
			}
		}catch(Exception ex){
			//System.out.println("ConvererFactory:convertCorrComponentDTOs error " + ex.toString());
		}
		return corrugateComponents;
	}
	
	public static DisplayCostDTO convert(DisplayCost displayCost) {
		DisplayCostDTO displayCostDTO=null;
		try{
			if(displayCost==null) return null;
			displayCostDTO = new DisplayCostDTO();
			if(displayCost.getDisplayCostId()!=null)
				displayCostDTO.setDisplayCostId(displayCost.getDisplayCostId());
			if(displayCost.getDisplayId()!=null)
				displayCostDTO.setDisplayId(displayCost.getDisplayId());
			if(displayCost.getDisplayScenarioId()!=null)
				displayCostDTO.setDisplayScenarioId(displayCost.getDisplayScenarioId());
			if(displayCost.getDisplayMaterialCost()!=null)
				displayCostDTO.setDisplayMaterialCost(displayCost.getDisplayMaterialCost());
			if(displayCost.getFullfilmentCost()!=null)
				displayCostDTO.setFullfilmentCost(displayCost.getFullfilmentCost());
			if(displayCost.getCtpCost()!=null)
				displayCostDTO.setCtpCost(displayCost.getCtpCost());
			if(displayCost.getDieCuttingCost()!=null)
				displayCostDTO.setDieCuttingCost(displayCost.getDieCuttingCost());
			if(displayCost.getPrintingPlateCost()!=null)
				displayCostDTO.setPrintingPlateCost(displayCost.getPrintingPlateCost());
			if(displayCost.getTotalArtworkCost()!=null)
				displayCostDTO.setTotalArtworkCost(displayCost.getTotalArtworkCost());
			if(displayCost.getShipTestCost()!=null)
				displayCostDTO.setShipTestCost(displayCost.getShipTestCost());
			if(displayCost.getPalletCost()!=null)
				displayCostDTO.setPalletCost(displayCost.getPalletCost());
			if(displayCost.getMiscCost()!=null)
				displayCostDTO.setMiscCost(displayCost.getMiscCost());
			if(displayCost.getCorrugateCost()!=null)
				displayCostDTO.setCorrugateCost(displayCost.getCorrugateCost());
			if(displayCost.getOverageScrapPct()!=null)
				displayCostDTO.setOverageScrapPct(displayCost.getOverageScrapPct());
			if(displayCost.getCustomerProgramPct()!=null)
				displayCostDTO.setCustomerProgramPct(displayCost.getCustomerProgramPct());
			if(displayCost.getSscidProjectNum()!=null)
				displayCostDTO.setSscidProjectNum(displayCost.getSscidProjectNum());
			if(displayCost.getCostCenter()!=null)
				displayCostDTO.setCostCenter(displayCost.getCostCenter());
			if(displayCost.getDtCreated()!=null)
				displayCostDTO.setDtCreated(displayCost.getDtCreated());
			if(displayCost.getDtLastChanged()!=null)
				displayCostDTO.setDtLastChanged(displayCost.getDtLastChanged());
			if(displayCost.getUserCreated()!=null)
				displayCostDTO.setUserCreated(displayCost.getUserCreated());
			if(displayCost.getUserLastChanged()!=null)
				displayCostDTO.setUserLastChanged(displayCost.getUserLastChanged());
			if(displayCost.getExpeditedFreightCost()!=null)
				displayCostDTO.setExpeditedFreightCost(displayCost.getExpeditedFreightCost());
			if(displayCost.getFines()!=null)
				displayCostDTO.setFines(displayCost.getFines());
			if(displayCost.getMdfPct()!=null)
				displayCostDTO.setMdfPct(displayCost.getMdfPct());
			if(displayCost.getDiscountPct()!=null)
				displayCostDTO.setDiscountPct(displayCost.getDiscountPct());
			
			if(displayCost.getTradeSalesChanged() != null)
				displayCostDTO.setTradeSales(displayCost.getTradeSalesChanged());
			
			if(displayCost.getCaDate()!=null)
				displayCostDTO.setCaDate(displayCost.getCaDate());
		}catch(Exception ex){
			System.out.println("ConvererFactory:convert(DisplayCostDTOdisplayCost) error " + ex.toString());
		}
		return displayCostDTO;
	}

	public static DisplayCost convert(DisplayCostDTO displayCostDTO) {
		DisplayCost displayCost=null;
		if(displayCostDTO==null) return null;
		try{
			displayCost = new DisplayCost();
			if(displayCostDTO.getDisplayCostId()!=null)displayCost.setDisplayCostId(displayCostDTO.getDisplayCostId());
			if(displayCostDTO.getDisplayId()!=null)displayCost.setDisplayId(displayCostDTO.getDisplayId());
			if(displayCostDTO.getDisplayScenarioId()!=null)displayCost.setDisplayScenarioId(displayCostDTO.getDisplayScenarioId());
			if(displayCostDTO.getDisplayMaterialCost()!=null)displayCost.setDisplayMaterialCost(displayCostDTO.getDisplayMaterialCost());
			if(displayCostDTO.getFullfilmentCost()!=null)displayCost.setFullfilmentCost(displayCostDTO.getFullfilmentCost());
			if(displayCostDTO.getCtpCost()!=null)displayCost.setCtpCost(displayCostDTO.getCtpCost());
			if(displayCostDTO.getDieCuttingCost()!=null)displayCost.setDieCuttingCost(displayCostDTO.getDieCuttingCost());
			if(displayCostDTO.getPrintingPlateCost()!=null)displayCost.setPrintingPlateCost(displayCostDTO.getPrintingPlateCost());
			if(displayCostDTO.getTotalArtworkCost()!=null)displayCost.setTotalArtworkCost(displayCostDTO.getTotalArtworkCost());
			if(displayCostDTO.getShipTestCost()!=null)displayCost.setShipTestCost(displayCostDTO.getShipTestCost());
			if(displayCostDTO.getPalletCost()!=null)displayCost.setPalletCost(displayCostDTO.getPalletCost());
			if(displayCostDTO.getMiscCost()!=null) displayCost.setMiscCost(displayCostDTO.getMiscCost());
			if(displayCostDTO.getCorrugateCost()!=null)displayCost.setCorrugateCost(displayCostDTO.getCorrugateCost());
			if(displayCostDTO.getOverageScrapPct()!=null)displayCost.setOverageScrapPct(displayCostDTO.getOverageScrapPct());
			if(displayCostDTO.getCustomerProgramPct()!=null)displayCost.setCustomerProgramPct(displayCostDTO.getCustomerProgramPct());
			if(displayCostDTO.getSscidProjectNum()!=null)displayCost.setSscidProjectNum(displayCostDTO.getSscidProjectNum());
			if(displayCostDTO.getCostCenter()!=null)displayCost.setCostCenter(displayCostDTO.getCostCenter());
			if(displayCostDTO.getDtCreated()!=null)displayCost.setDtCreated(displayCostDTO.getDtCreated());
			if(displayCostDTO.getDtLastChanged()!=null)displayCost.setDtLastChanged(displayCostDTO.getDtLastChanged());
			if(displayCostDTO.getUserCreated()!=null)displayCost.setUserCreated(displayCostDTO.getUserCreated());
			if(displayCostDTO.getUserLastChanged()!=null)displayCost.setUserLastChanged(displayCostDTO.getUserLastChanged());
			if(displayCostDTO.getExpeditedFreightCost()!=null)displayCost.setExpeditedFreightCost(displayCostDTO.getExpeditedFreightCost());
			if(displayCostDTO.getFines()!=null)displayCost.setFines(displayCostDTO.getFines());
			if(displayCostDTO.getMdfPct()!=null)displayCost.setMdfPct(displayCostDTO.getMdfPct());
			if(displayCostDTO.getDiscountPct()!=null)displayCost.setDiscountPct(displayCostDTO.getDiscountPct());
			if(displayCostDTO.getTradeSales() != null)displayCost.setTradeSalesChanged(displayCostDTO.getTradeSales());
			if(displayCostDTO.getCaDate()!=null)displayCost.setCaDate(displayCostDTO.getCaDate());
		}catch(Exception ex){
			System.out.println("ConverterFactory:convert DisplayCostDTO error " + ex.toString());
		}

		return displayCost;
	}

	public static List<DisplayCostDTO> convertDisplayCosts(List<DisplayCost> displayCosts) {
		if (displayCosts == null) return null;
		List<DisplayCostDTO> displayCostsDTO = new ArrayList<DisplayCostDTO>();
		try{
			for (int i = 0; i < displayCosts.size(); i++) {
				displayCostsDTO.add(convert(displayCosts.get(i)));
			}
		}catch(Exception ex){
			System.out.println("ConvererFactory:convertDisplayCostComments error " + ex.toString());
		}
		return displayCostsDTO;
	}
	
	public static DisplayCostComment convert(DisplayCostCommentDTO displayCostCommentDTO) {
		DisplayCostComment displayCostComment=null;
		if(displayCostCommentDTO==null) return null;
		try{
			displayCostComment = new DisplayCostComment();
			displayCostComment.setDisplayCostCommentId(displayCostCommentDTO.getDisplayCostCommentId());
			displayCostComment.setDisplayScenarioId(displayCostCommentDTO.getDisplayScenarioId());
			displayCostComment.setCostComment(displayCostCommentDTO.getCostComment().length()<500?displayCostCommentDTO.getCostComment():displayCostCommentDTO.getCostComment().substring(0, 499));
			displayCostComment.setCreateDate(new Date());
			displayCostComment.setDisplayCostId(displayCostCommentDTO.getDisplayCostId());
			displayCostComment.setUserName(displayCostCommentDTO.getUserName());
		}catch(Exception ex){
			System.out.println("ConverterFactory:convert DisplayCostComment error " + ex.toString());
		}
		return displayCostComment;
	}

	public static DisplayCostCommentDTO convert(DisplayCostComment displayCostComment) {
		DisplayCostCommentDTO displayCostCommentDTO=null;
		if(displayCostComment==null) return null;
		try{
			displayCostCommentDTO = new DisplayCostCommentDTO();
			displayCostCommentDTO.setDisplayCostCommentId(displayCostComment.getDisplayCostCommentId());
			displayCostCommentDTO.setCostComment(displayCostComment.getCostComment());
			displayCostCommentDTO.setCreateDate(Utility.dateToStringFormat(displayCostComment.getCreateDate(),""));
			displayCostCommentDTO.setDisplayCostId(displayCostComment.getDisplayCostId());
			displayCostCommentDTO.setUserName(displayCostComment.getUserName());
			displayCostCommentDTO.setDisplayScenarioId(displayCostComment.getDisplayScenarioId());
		}catch(Exception ex){
			System.out.println("ConverterFactory:convert DisplayCostCommentDTO error " + ex.toString());
		}
		return displayCostCommentDTO;
	}

	public static List<DisplayCostCommentDTO> convertDisplayCostComments(List<DisplayCostComment> displayCostComments) {
		if (displayCostComments == null) return null;
		List<DisplayCostCommentDTO> displayCostCommentsDTO = new ArrayList<DisplayCostCommentDTO>();
		try{
			for (int i = 0; i < displayCostComments.size(); i++) {
				displayCostCommentsDTO.add(convert(displayCostComments.get(i)));
			}
		}catch(Exception ex){
			System.out.println("ConvererFactory:convertDisplayCostComments error " + ex.toString());
		}
		return displayCostCommentsDTO;
	}

	public static List<CostAnalysisHistoryDTO> convertUserChangeLog(List<UserChangeLog> userChangeLogs) {
		if (userChangeLogs == null) return null;
		List<CostAnalysisHistoryDTO> costAnalysisHistoryDTOs = new ArrayList<CostAnalysisHistoryDTO>();
		for (int i = 0; i < userChangeLogs.size(); i++) {
			costAnalysisHistoryDTOs.add(convert(userChangeLogs.get(i)));
		}
		return costAnalysisHistoryDTOs;
	}

	public static CostAnalysisHistoryDTO convert(UserChangeLog userChangeLog) {
		if (userChangeLog == null) return null;
		try{
			String sUserChangeLog="";
			if(userChangeLog.getField().equalsIgnoreCase("scenario_approval_flg")){
				/*
				 * Quick fix: For Approval Flag default it to N instead of empty
				 * Date     : 01-14-2011, 10.43 am
				 * By       : Mari  
				 */
				
				sUserChangeLog=  Utility.dateToStringFormat(userChangeLog.getDtChanged(),"") +" " + userChangeLog.getUserName() +
				" changed "+ userChangeLog.getField() + " from " + 
				(userChangeLog.getOldValue()==null?"N":userChangeLog.getOldValue()) + " to "+  
				(userChangeLog.getNewValue()==null?"":userChangeLog.getNewValue());
			}else{
				sUserChangeLog=  Utility.dateToStringFormat(userChangeLog.getDtChanged(),"") +" " + userChangeLog.getUserName() +
				" changed "+ userChangeLog.getField() + " from " + 
				(userChangeLog.getOldValue()==null?"":userChangeLog.getOldValue()) + " to "+  
				(userChangeLog.getNewValue()==null?"":userChangeLog.getNewValue());
			}
			CostAnalysisHistoryDTO costAnalysisHistoryDTO = new CostAnalysisHistoryDTO();
			costAnalysisHistoryDTO.setHistory(sUserChangeLog);
			return costAnalysisHistoryDTO;
		}catch(Exception ex){
			System.out.println("ConverterFactory:convert(UserChangeLog userChangeLog) error "+ ex.toString());
			return null;
		}
	}

	/*public static List<BuCostCenterSplitDTO> convertBuCostCenterSplit(Double mdfPctVal,Double discountPctVal,
			List<ProgramBuDTO> programBuDTOs,
			Long actualQtyVal, Double totalDisplaysOrdered,
			Double totalActualInvoiceTotCostVal,
			Double customerProgramPercentageVal,Double totalDisplayCostsVal,
			List<CostAnalysisKitComponentDTO> costAnalysisKitComponentDTOs) {
		List<BuCostCenterSplitDTO> buCostCenterSplitDTOs = new ArrayList<BuCostCenterSplitDTO>();
		BuCostCenterSplitDTO buCostCenterSplitDTO=null;
		double percentMaxValue=100.00;
		double netSales=0.00;
		double sumNetSales=0.00;
		BuCostCenterSplitDTO buRow;
		double pctOfCost=0.00;
		double vmPct=0.00;
		double dOverrideExcPctVal=0.00;
		double dEstimatedProgramPctVal=0.00;
		try{
			if(costAnalysisKitComponentDTOs==null || costAnalysisKitComponentDTOs.size()==0) return null;
			Iterator<BuCostCenterSplitDTO> iter=null;
			boolean buRowExists=false;
			Iterator<BuCostCenterSplitDTO> iterSumCostCenterSplitDTOs=null;
			BuCostCenterSplitDTO buSumRow=null;
			Iterator<BuCostCenterSplitDTO> iterCostCenterSplitDTOs = null;;
			BuCostCenterSplitDTO buResultRow;
			for (CostAnalysisKitComponentDTO costAnalysisKitComponentDTO: costAnalysisKitComponentDTOs){
				dOverrideExcPctVal=costAnalysisKitComponentDTO.getOverrideExcPct()==null?0.00:costAnalysisKitComponentDTO.getOverrideExcPct();
				dEstimatedProgramPctVal=costAnalysisKitComponentDTO.getEstimatedProgramPct()==null?0.00:costAnalysisKitComponentDTO.getEstimatedProgramPct();
				customerProgramPercentageVal= dOverrideExcPctVal > 0.0 ? dOverrideExcPctVal:dEstimatedProgramPctVal;
				if(buCostCenterSplitDTOs!=null && buCostCenterSplitDTOs.size() >0){
					iter = buCostCenterSplitDTOs.iterator();
					buRow=null;
					while (iter.hasNext()) {
						buRow = (BuCostCenterSplitDTO) iter.next();
						buRowExists=false;
						if(buRow.getBuDesc().trim().equalsIgnoreCase(costAnalysisKitComponentDTO.getBuDesc())){
							buRowExists=true;
							buRow.setTotalActualInvoicePerBu(buRow.getTotalActualInvoicePerBu()+(costAnalysisKitComponentDTO.getInvoiceWithProgram() *  costAnalysisKitComponentDTO.getQtyPerDisplay()));
							buRow.setTotalNetVmAmountPerBu(buRow.getTotalNetVmAmountPerBu()+costAnalysisKitComponentDTO.getNetVmAmount());
							break;	
						}
					}
					if(buRowExists){//Already Exists
						if(buRow!=null){
							 
							 * Date: 01/03/2013
							 * Description: apply actualQty instead of forecastQty
							 * netSales=buRow.getTotalActualInvoicePerBu() * foreCastQtyVal ;//--1--
							 
							netSales=buRow.getTotalActualInvoicePerBu() * actualQtyVal ;
							buRow.setNetSales(roundToDecimals(netSales,2));
						}
					}else{//SUbsequant BUs Exist 
						buCostCenterSplitDTO=new BuCostCenterSplitDTO();
						buCostCenterSplitDTO.setBuCode(costAnalysisKitComponentDTO.getBuCode());
						buCostCenterSplitDTO.setBuDesc(costAnalysisKitComponentDTO.getBuDesc());
						buCostCenterSplitDTO.setTotalActualInvoicePerBu(costAnalysisKitComponentDTO.getInvoiceWithProgram() *  costAnalysisKitComponentDTO.getQtyPerDisplay());
						buCostCenterSplitDTO.setTotalNetVmAmountPerBu(costAnalysisKitComponentDTO.getNetVmAmount());
						
						 * Date: 01/03/2013
						 * Description: apply actualQty instead of forecastQty
						 * netSales=buCostCenterSplitDTO.getTotalActualInvoicePerBu() * foreCastQtyVal ;//--3--
						 
						netSales=buCostCenterSplitDTO.getTotalActualInvoicePerBu() * actualQtyVal ;//--3--
						buCostCenterSplitDTO.setNetSales(roundToDecimals(netSales,2));
						buCostCenterSplitDTOs.add(buCostCenterSplitDTO);
					}
				}else{
					//First Cost Center BU
					buCostCenterSplitDTO=new BuCostCenterSplitDTO();
					buCostCenterSplitDTO.setBuCode(costAnalysisKitComponentDTO.getBuCode());
					buCostCenterSplitDTO.setBuDesc(costAnalysisKitComponentDTO.getBuDesc());
					buCostCenterSplitDTO.setTotalActualInvoicePerBu(roundToDecimals(costAnalysisKitComponentDTO.getInvoiceWithProgram() *  costAnalysisKitComponentDTO.getQtyPerDisplay(),2));
					buCostCenterSplitDTO.setTotalNetVmAmountPerBu(roundToDecimals(costAnalysisKitComponentDTO.getNetVmAmount(),2));
					
					 * Date: 01/03/2013
					 * Description: apply actualQty instead of forecastQty
					 * netSales=buCostCenterSplitDTO.getTotalActualInvoicePerBu() * foreCastQtyVal ;//--5--
					 
					netSales=buCostCenterSplitDTO.getTotalActualInvoicePerBu() * actualQtyVal ;//--5--
					buCostCenterSplitDTO.setNetSales(roundToDecimals(netSales,2));
					buCostCenterSplitDTOs.add(buCostCenterSplitDTO);
				}
			}
			iterSumCostCenterSplitDTOs = buCostCenterSplitDTOs.iterator();
			sumNetSales=0.00;
			while(iterSumCostCenterSplitDTOs.hasNext()){
				buSumRow=(BuCostCenterSplitDTO) iterSumCostCenterSplitDTOs.next();
				sumNetSales+=buSumRow.getNetSales();
			}
			iterCostCenterSplitDTOs = buCostCenterSplitDTOs.iterator();
			while(iterCostCenterSplitDTOs.hasNext()){
				buResultRow=(BuCostCenterSplitDTO) iterCostCenterSplitDTOs.next();
				pctOfCost=(buResultRow.getNetSales()/sumNetSales) * totalDisplayCostsVal;
				buResultRow.setPctOfCost(roundToDecimals(pctOfCost,2));
				buResultRow.setBuSplit(sumNetSales > 0 ? buResultRow.getNetSales()/sumNetSales*100 : 0);
				customerProgramPercentageVal=getTotalDisplayOrderedDedPct(buResultRow.getBuCode(),programBuDTOs);
				vmPct=(((buResultRow.getTotalNetVmAmountPerBu() * totalDisplaysOrdered) - pctOfCost)/
						(buResultRow.getTotalActualInvoicePerBu() * totalDisplaysOrdered )) * percentMaxValue;
				buResultRow.setVmPct(roundToDecimals(vmPct,2));
			}
		}catch(Exception ex){
			System.out.println("ConvererFactory:convertBuCostCenterSplit error " + ex.toString());
		}
		System.out.println("<<Debug>>ConverterFactory:convertBuCostCenterSplit() ends");
		return buCostCenterSplitDTOs;
	}*/
	
	
	public static List<BuCostCenterSplitDTO> convertBuCostCenterSplit(Double mdfPctVal,Double discountPctVal,
			List<ProgramBuDTO> programBuDTOs,
			Long actualQtyVal, Double totalDisplaysOrdered,
			Double totalActualInvoiceTotCostVal,
			Double customerProgramPercentageVal,Double totalDisplayCostsVal,
			List<CostAnalysisKitComponentDTO> costAnalysisKitComponentDTOs) {
		List<BuCostCenterSplitDTO> buCostCenterSplitDTOs = new ArrayList<BuCostCenterSplitDTO>();
		BuCostCenterSplitDTO buCostCenterSplitDTO=null;
		double percentMaxValue=100.00;
		double netSales=0.00;
		double sumNetSales=0.00;
		double dsumTotNetSales=0.00;
		BuCostCenterSplitDTO buRow;
		double pctOfCost=0.00;
		double vmPct=0.00;
		double dOverrideExcPctVal=0.00;
		double dEstimatedProgramPctVal=0.00;
		double discountTradeSales;
		double mdfTradeSales;
		double netSalesInterim;
		try{
			if(costAnalysisKitComponentDTOs==null || costAnalysisKitComponentDTOs.size()==0) return null;
			Iterator<BuCostCenterSplitDTO> iter=null;
			boolean buRowExists=false;
			Iterator<BuCostCenterSplitDTO> iterSumCostCenterSplitDTOs=null;
			BuCostCenterSplitDTO buSumRow=null;
			Iterator<BuCostCenterSplitDTO> iterCostCenterSplitDTOs = null;;
			BuCostCenterSplitDTO buResultRow;
			for (CostAnalysisKitComponentDTO costAnalysisKitComponentDTO: costAnalysisKitComponentDTOs){
				dOverrideExcPctVal=costAnalysisKitComponentDTO.getOverrideExcPct()==null?0.00:costAnalysisKitComponentDTO.getOverrideExcPct();
				dEstimatedProgramPctVal=costAnalysisKitComponentDTO.getEstimatedProgramPct()==null?0.00:costAnalysisKitComponentDTO.getEstimatedProgramPct();
				customerProgramPercentageVal= dOverrideExcPctVal > 0.0 ? dOverrideExcPctVal:dEstimatedProgramPctVal;
				if(buCostCenterSplitDTOs!=null && buCostCenterSplitDTOs.size() >0){
					iter = buCostCenterSplitDTOs.iterator();
					buRow=null;
					while (iter.hasNext()) {
						buRow = (BuCostCenterSplitDTO) iter.next();
						buRowExists=false;
						if(buRow.getBuDesc().trim().equalsIgnoreCase(costAnalysisKitComponentDTO.getBuDesc())){
							buRowExists=true;
							buRow.setTotalActualInvoicePerBu(buRow.getTotalActualInvoicePerBu()+(costAnalysisKitComponentDTO.getActualInvoicePerEa() *  costAnalysisKitComponentDTO.getQtyPerDisplay()));
							buRow.setTotalActualInvoiceWithProgram(buRow.getTotalActualInvoiceWithProgram()+(costAnalysisKitComponentDTO.getTotalActualInvoiceWpgrm() ));
							buRow.setTotalNetVmAmountPerBu(buRow.getTotalNetVmAmountPerBu()+costAnalysisKitComponentDTO.getNetVmAmount());
							break;	
						}
					}
					if(buRowExists){//Already Exists
						if(buRow!=null){
							/* 
							 * Date: 01/03/2013
							 * Description: apply actualQty instead of forecastQty
							 * netSales=buRow.getTotalActualInvoicePerBu() * foreCastQtyVal ;//--1--
							 */
							/*
							 * Date : 08/19/2014
							 * Description Net Sales = Trade Sales - Trade Program Disc - (Trade Sales * MDF %) - (Trade Sales Discount %)
							 * By : Marianandan Arockiasamy
							 */
							
							netSales=buRow.getTotalActualInvoicePerBu() * actualQtyVal ;
							//discountTradeSales= netSales * discountPctVal;
							//mdfTradeSales= netSales * mdfPctVal;
							buRow.setNetSales(roundToDecimals(netSales,2));
						}
					}else{//SUbsequant BUs Exist 
						buCostCenterSplitDTO=new BuCostCenterSplitDTO();
						buCostCenterSplitDTO.setBuCode(costAnalysisKitComponentDTO.getBuCode());
						buCostCenterSplitDTO.setBuDesc(costAnalysisKitComponentDTO.getBuDesc());
						buCostCenterSplitDTO.setTotalActualInvoicePerBu((costAnalysisKitComponentDTO.getActualInvoicePerEa()) *  costAnalysisKitComponentDTO.getQtyPerDisplay());
						buCostCenterSplitDTO.setTotalActualInvoiceWithProgram(costAnalysisKitComponentDTO.getTotalActualInvoiceWpgrm() );
						buCostCenterSplitDTO.setTotalNetVmAmountPerBu(costAnalysisKitComponentDTO.getNetVmAmount());
						
						netSales=buCostCenterSplitDTO.getTotalActualInvoicePerBu() * actualQtyVal ;//--3--
					//	discountTradeSales= netSales * discountPctVal;
					//	mdfTradeSales= netSales * mdfPctVal;
						buCostCenterSplitDTO.setNetSales(roundToDecimals(netSales,2));
						buCostCenterSplitDTOs.add(buCostCenterSplitDTO);
					}
				}else{
					//First Cost Center BU
					buCostCenterSplitDTO=new BuCostCenterSplitDTO();
					buCostCenterSplitDTO.setBuCode(costAnalysisKitComponentDTO.getBuCode());
					buCostCenterSplitDTO.setBuDesc(costAnalysisKitComponentDTO.getBuDesc());
					buCostCenterSplitDTO.setTotalActualInvoicePerBu(roundToDecimals((costAnalysisKitComponentDTO.getActualInvoicePerEa()) *  costAnalysisKitComponentDTO.getQtyPerDisplay(),2));
					buCostCenterSplitDTO.setTotalActualInvoiceWithProgram(roundToDecimals((costAnalysisKitComponentDTO.getTotalActualInvoiceWpgrm()) ,2));
					buCostCenterSplitDTO.setTotalNetVmAmountPerBu(roundToDecimals(costAnalysisKitComponentDTO.getNetVmAmount(),2));
					/*
					 * Date: 01/03/2013
					 * Description: apply actualQty instead of forecastQty
					 * netSales=buCostCenterSplitDTO.getTotalActualInvoicePerBu() * foreCastQtyVal ;//--5--
					 */
					netSales=buCostCenterSplitDTO.getTotalActualInvoicePerBu() * actualQtyVal ;//--5--
					buCostCenterSplitDTO.setNetSales(roundToDecimals(netSales,2));
					buCostCenterSplitDTOs.add(buCostCenterSplitDTO);
				}
			}
			iterSumCostCenterSplitDTOs = buCostCenterSplitDTOs.iterator();
			sumNetSales=0.00;
			dsumTotNetSales=0.00;
			while(iterSumCostCenterSplitDTOs.hasNext()){
				buSumRow=(BuCostCenterSplitDTO) iterSumCostCenterSplitDTOs.next();
				/*
				 * Date: 01/03/2013
				 * Description: apply actualQty instead of forecastQty
				 * netSales=buCostCenterSplitDTO.getTotalActualInvoicePerBu() * foreCastQtyVal ;//--3--
				 */
				netSalesInterim=buSumRow.getNetSales() ;
				discountTradeSales= netSalesInterim * discountPctVal;
				mdfTradeSales= netSalesInterim * mdfPctVal;
				dsumTotNetSales=roundToDecimals((netSalesInterim-(  (netSalesInterim-(buSumRow.getTotalActualInvoiceWithProgram() * actualQtyVal)) +  
						discountTradeSales+mdfTradeSales)),2);
				buSumRow.setNetSales(dsumTotNetSales);
				sumNetSales+=dsumTotNetSales;
			}
			iterCostCenterSplitDTOs = buCostCenterSplitDTOs.iterator();
			while(iterCostCenterSplitDTOs.hasNext()){
				buResultRow=(BuCostCenterSplitDTO) iterCostCenterSplitDTOs.next();
				pctOfCost=(buResultRow.getNetSales()/sumNetSales) * totalDisplayCostsVal;
				buResultRow.setPctOfCost(roundToDecimals(pctOfCost,2));
				buResultRow.setBuSplit(sumNetSales > 0 ? ((buResultRow.getNetSales()/sumNetSales)*100) : 0);
				customerProgramPercentageVal=getTotalDisplayOrderedDedPct(buResultRow.getBuCode(),programBuDTOs);
				vmPct=(((buResultRow.getTotalNetVmAmountPerBu() * totalDisplaysOrdered) - pctOfCost)/
						(buResultRow.getTotalActualInvoicePerBu() * totalDisplaysOrdered )) * percentMaxValue;
				buResultRow.setVmPct(roundToDecimals(vmPct,2));
			}
		}catch(Exception ex){
			System.out.println("ConvererFactory:convertBuCostCenterSplit error " + ex.toString());
		}
		System.out.println("<<Debug>>ConverterFactory:convertBuCostCenterSplit() ends");
		return buCostCenterSplitDTOs;
	}


	public static DisplayCostImage convert(DisplayCostImageDTO displayCostImageDTO) {
		DisplayCostImage displayCostImage=null;
		if(displayCostImageDTO==null) return null;
		try{
			displayCostImage = new DisplayCostImage();
			displayCostImage.setCreateDate(displayCostImageDTO.getCreateDate());
			displayCostImage.setDescription(displayCostImageDTO.getDescription());
			displayCostImage.setDisplayScenarioId(displayCostImageDTO.getDisplayScenarioId());
			displayCostImage.setImageId(displayCostImageDTO.getImageId());
			displayCostImage.setDisplayCostId(displayCostImageDTO.getDisplayCostId());
			displayCostImage.setImageName(displayCostImageDTO.getImageName());
			displayCostImage.setUrl(displayCostImageDTO.getUrl());
			displayCostImage.setDisplayId(displayCostImageDTO.getDisplayId());
		}catch(Exception ex){
			System.out.println("ConverterFactory:convert displayCostImageDTO error " + ex.toString());
		}
		return displayCostImage;
	}

	public static DisplayCostImageDTO convert(DisplayCostImage displayCostImage) {
		DisplayCostImageDTO displayCostImageDTO=null;
		if(displayCostImage==null) return null;
		try{
			displayCostImageDTO = new DisplayCostImageDTO();
			displayCostImageDTO.setCreateDate(displayCostImage.getCreateDate());
			displayCostImageDTO.setDescription(displayCostImage.getDescription());
			displayCostImageDTO.setDisplayScenarioId(displayCostImage.getDisplayScenarioId());
			displayCostImageDTO.setImageId(displayCostImage.getImageId());
			displayCostImageDTO.setDisplayCostId(displayCostImage.getDisplayCostId());
			displayCostImageDTO.setImageName(displayCostImage.getImageName());
			displayCostImageDTO.setUrl(displayCostImage.getUrl());
			displayCostImageDTO.setDisplayId(displayCostImage.getDisplayId());
		}catch(Exception ex){
			System.out.println("ConverterFactory:convert displayCostImageDTO error " + ex.toString());
		}
		return displayCostImageDTO;
	}

	public static List<DisplayCostImageDTO> convertDisplayCostImage(List<DisplayCostImage> displayCostImages) {
		if (displayCostImages == null) return null;
		List<DisplayCostImageDTO> displayCostImageDTO = new ArrayList<DisplayCostImageDTO>();
		try{
			for (int i = 0; i < displayCostImages.size(); i++) {
				displayCostImageDTO.add(convert(displayCostImages.get(i)));
			}
		}catch(Exception ex){
			System.out.println("ConvererFactory:convertDisplayCostImages error " + ex.toString());
		}
		return displayCostImageDTO;
	}


	public static CustomerPctDTO convert(CustomerPct customerPct) {
		if (customerPct == null) return null;
		CustomerPctDTO customerPctDTO = new CustomerPctDTO();
		customerPctDTO.setCustomerId(customerPct.getCustomerId());
		customerPctDTO.setBinders( customerPct.getBinders());
		customerPctDTO.setCardsPaper(customerPct.getCardsPaper());
		customerPctDTO.setDividers(customerPct.getDividers());
		customerPctDTO.setLabels(customerPct.getLabels());
		customerPctDTO.setoAndP(customerPct.getoAndP());
		customerPctDTO.setWritingInstruments(customerPct.getWritingInstruments());
		return customerPctDTO;
	}

	public static List<CustomerPctDTO> convertCustomerPcts(List<CustomerPct> customerPcts) {
		if (customerPcts == null) return null;
		List<CustomerPctDTO> customerPctDTOs = new ArrayList<CustomerPctDTO>();
		for (int i = 0; i < customerPcts.size(); i++) {
			customerPctDTOs.add(convert(customerPcts.get(i)));
		}
		return customerPctDTOs;
	}


	public static Double roundToDecimals(Double d, int c) {
			Double temp=(Double)((d*Math.pow(10,c)));
			temp= (Math.round(temp)/Math.pow(10,c));
			return temp;
	}

	public static double getTotalDisplayOrderedDedPct(String sBuCode, List<ProgramBuDTO> programBuDTOs){
		double totalDisplayOrderedDedPct=0.0;
		for(ProgramBuDTO objProgramBuDTO : programBuDTOs){
			if(objProgramBuDTO.getBuCode().equals(sBuCode)){
				totalDisplayOrderedDedPct=objProgramBuDTO.getProgramPct();
				return totalDisplayOrderedDedPct;
			}
		}
		return totalDisplayOrderedDedPct;
	}
	public static CorrugateComponentDTO convert(CorrugateComponent oldInstance) {
		if (oldInstance == null) return null;
		CorrugateComponentDTO newInstance = new CorrugateComponentDTO();
		newInstance.setCorrugateComponentId(oldInstance.getCorrugateComponentId());
		newInstance.setDisplayId(oldInstance.getDisplayId());
		newInstance.setDisplayScenarioId(oldInstance.getDisplayScenarioId());
		newInstance.setCountryId(oldInstance.getCountryId());
		newInstance.setManufacturerId(oldInstance.getManufacturerId());
		newInstance.setVendorPartNum(oldInstance.getVendorPartNum()); 
		newInstance.setGloviaPartNum(oldInstance.getGloviaPartNum());
		newInstance.setDescription(oldInstance.getDescription());
		newInstance.setBoardTest(oldInstance.getBoardTest());
		newInstance.setMaterial(oldInstance.getMaterial());
		newInstance.setQtyPerDisplay(oldInstance.getQtyPerDisplay()); 
		newInstance.setPiecePrice(oldInstance.getPiecePrice());
		newInstance.setTotalCost(oldInstance.getTotalCost());
		newInstance.setTotalQty(oldInstance.getTotalQty());
		newInstance.setDtCreated(oldInstance.getDtCreated());
		newInstance.setDtLastChanged(oldInstance.getDtLastChanged());
		newInstance.setUserCreated(oldInstance.getUserCreated());
		newInstance.setUserLastChanged(oldInstance.getUserLastChanged());
		return newInstance;
	}
	public static CorrugateComponent convert(CorrugateComponentDTO oldInstance) {
		if (oldInstance == null) return null;
		CorrugateComponent newInstance = new CorrugateComponent();
		newInstance.setCorrugateComponentId(oldInstance.getCorrugateComponentId());
		newInstance.setDisplayId(oldInstance.getDisplayId());
		newInstance.setDisplayScenarioId(oldInstance.getDisplayScenarioId());
		newInstance.setCountryId(oldInstance.getCountryId());
		newInstance.setManufacturerId(oldInstance.getManufacturerId());
		newInstance.setVendorPartNum(oldInstance.getVendorPartNum()); 
		newInstance.setGloviaPartNum(oldInstance.getGloviaPartNum());
		newInstance.setDescription(oldInstance.getDescription());
		newInstance.setBoardTest(oldInstance.getBoardTest());
		newInstance.setMaterial(oldInstance.getMaterial());
		newInstance.setQtyPerDisplay(oldInstance.getQtyPerDisplay()); 
		newInstance.setPiecePrice(oldInstance.getPiecePrice());
		newInstance.setTotalCost(oldInstance.getTotalCost());
		newInstance.setTotalQty(oldInstance.getTotalQty());
		newInstance.setDtCreated(oldInstance.getDtCreated());
		newInstance.setDtLastChanged(oldInstance.getDtLastChanged());
		newInstance.setUserCreated(oldInstance.getUserCreated());
		newInstance.setUserLastChanged(oldInstance.getUserLastChanged());
		return newInstance;
	}
	
	public static CorrugateComponentsDetailDTO convert(CorrugateComponentsDetail oldInstance) {
		if (oldInstance == null) return null;
		CorrugateComponentsDetailDTO newInstance = new CorrugateComponentsDetailDTO();
		newInstance.setCorrugateComponentId(oldInstance.getCorrugateComponentId());
		newInstance.setDisplayId(oldInstance.getDisplayId());
		newInstance.setCountryId(oldInstance.getCountryId());
		newInstance.setManufacturerId(oldInstance.getManufacturerId());
		newInstance.setVendorPartNum(oldInstance.getVendorPartNum()); 
		newInstance.setGloviaPartNum(oldInstance.getGloviaPartNum());
		newInstance.setDescription(oldInstance.getDescription());
		newInstance.setBoardTest(oldInstance.getBoardTest());
		newInstance.setMaterial(oldInstance.getMaterial());
		newInstance.setQtyPerDisplay(oldInstance.getQtyPerDisplay()); 
		newInstance.setPiecePrice(oldInstance.getPiecePrice());
		newInstance.setTotalCost(oldInstance.getTotalCost());
		newInstance.setTotalQty(oldInstance.getTotalQty());
		newInstance.setDtCreated(oldInstance.getDtCreated());
		newInstance.setDtLastChanged(oldInstance.getDtLastChanged());
		newInstance.setUserCreated(oldInstance.getUserCreated());
		newInstance.setUserLastChanged(oldInstance.getUserLastChanged());
		newInstance.setCountryName(oldInstance.getCountryName());
		newInstance.setName(oldInstance.getName());
		return newInstance;
	}
	public static CorrugateComponentsDetail convert(CorrugateComponentsDetailDTO oldInstance) {
		if (oldInstance == null) return null;
		CorrugateComponentsDetail newInstance = new CorrugateComponentsDetail();
		newInstance.setCorrugateComponentId(oldInstance.getCorrugateComponentId());
		newInstance.setDisplayId(oldInstance.getDisplayId());
		newInstance.setCountryId(oldInstance.getCountryId());
		newInstance.setManufacturerId(oldInstance.getManufacturerId());
		newInstance.setVendorPartNum(oldInstance.getVendorPartNum()); 
		newInstance.setGloviaPartNum(oldInstance.getGloviaPartNum());
		newInstance.setDescription(oldInstance.getDescription());
		newInstance.setBoardTest(oldInstance.getBoardTest());
		newInstance.setMaterial(oldInstance.getMaterial());
		newInstance.setQtyPerDisplay(oldInstance.getQtyPerDisplay()); 
		newInstance.setPiecePrice(oldInstance.getPiecePrice());
		newInstance.setTotalCost(oldInstance.getTotalCost());
		newInstance.setTotalQty(oldInstance.getTotalQty());
		newInstance.setDtCreated(oldInstance.getDtCreated());
		newInstance.setDtLastChanged(oldInstance.getDtLastChanged());
		newInstance.setUserCreated(oldInstance.getUserCreated());
		newInstance.setUserLastChanged(oldInstance.getUserLastChanged());
		newInstance.setCountryName(oldInstance.getCountryName());
		newInstance.setName(oldInstance.getName());
		return newInstance;
	}
	public static List<CorrugateComponentsDetailDTO> convertCorrugateComponents(List<CorrugateComponentsDetail> corrugateComponents) {
		if (corrugateComponents == null) return null;
		List<CorrugateComponentsDetailDTO> corrugateComponentsDetailDTOs = new ArrayList<CorrugateComponentsDetailDTO>();
		for (int i = 0; i < corrugateComponents.size(); i++) {
			corrugateComponentsDetailDTOs.add(convert(corrugateComponents.get(i)));
		}
		return corrugateComponentsDetailDTOs;
	}
	public static CorrugateDTO convert(Corrugate oldInstance) {
		if (oldInstance == null) return null;
		CorrugateDTO newInstance = new CorrugateDTO();
		newInstance.setDisplayId(oldInstance.getDisplayId());
		newInstance.setCorrugateId(oldInstance.getCorrugateId());
		newInstance.setCreateDate(oldInstance.getCreateDate());
		newInstance.setCorrugateCost(oldInstance.getCorrugateCost());
		return newInstance;
	}

	public static Corrugate convert(CorrugateDTO oldInstance) {
		if (oldInstance == null) return null;
		Corrugate newInstance = new Corrugate();
		newInstance.setDisplayId(oldInstance.getDisplayId());
		newInstance.setCorrugateId(oldInstance.getCorrugateId());
		newInstance.setCreateDate(oldInstance.getCreateDate());
		newInstance.setCorrugateCost(oldInstance.getCorrugateCost());
		return newInstance;
	}
	
	public static List<CountryDTO> convertCountryList(List<Country> countryList) {
		if (countryList == null) return null;
		List<CountryDTO> countryDTOList = new ArrayList<CountryDTO>();
		for (int i = 0; i < countryList.size(); i++) {
			countryDTOList.add(convert(countryList.get(i)));
		}
		return countryDTOList;
	}
	
	public static CountryDTO convert(Country a) {
		if (a == null) return null;
		CountryDTO b = new CountryDTO();
		b.setActiveFlg(a.isActiveFlg());
		b.setCountryCode(a.getCountryCode());
		b.setCountryId(a.getCountryId());
		b.setCountryName(a.getCountryName());
		b.setActiveCountryFlg(a.isActiveCountryFlg());
		return b;
	}
	
	public static CalculationSummaryDTO convert(CalculationSummary calculationSummary) {
		if (calculationSummary == null) return null;
		CalculationSummaryDTO calculationSummaryDTO = new CalculationSummaryDTO();
		calculationSummaryDTO.setCalculationSummaryId(calculationSummary.getCalculationSummaryId());
		calculationSummaryDTO.setDisplayId(calculationSummary.getDisplayId());
		calculationSummaryDTO.setDisplayScenarioId(calculationSummary.getDisplayScenarioId());
		calculationSummaryDTO.setActualQty(calculationSummary.getActualQty());
		calculationSummaryDTO.setAverySku(calculationSummary.getAverySku());
		calculationSummaryDTO.setCustomerId(calculationSummary.getCustomerId());
		calculationSummaryDTO.setCustProgDisc(calculationSummary.getCustProgDisc());
		calculationSummaryDTO.setDescription(calculationSummary.getDescription());
		calculationSummaryDTO.setForeCastQty(calculationSummary.getForeCastQty());
		calculationSummaryDTO.setHurdles(calculationSummary.getHurdles());
		calculationSummaryDTO.setNumberFacing(calculationSummary.getNumberFacing());
		calculationSummaryDTO.setPromoPeriodId(calculationSummary.getPromoPeriodId());
		calculationSummaryDTO.setQtyPerDisplay(calculationSummary.getQtyPerDisplay());
		calculationSummaryDTO.setQtyPerFacing(calculationSummary.getQtyPerFacing());
		calculationSummaryDTO.setShippingCost(calculationSummary.getShippingCost());
		calculationSummaryDTO.setSku(calculationSummary.getSku());
		calculationSummaryDTO.setStatusId(calculationSummary.getStatusId());
		calculationSummaryDTO.setSumCogs(calculationSummary.getSumCogs());
		calculationSummaryDTO.setSumCorrugateCost(calculationSummary.getSumCorrugateCost());
		calculationSummaryDTO.setSumDisplayCost(calculationSummary.getSumDisplayCost());
		calculationSummaryDTO.setSumVmDollarBp(calculationSummary.getSumVmDollarBp());
		calculationSummaryDTO.setSumVmPctBp(calculationSummary.getSumVmPctBp());
		calculationSummaryDTO.setSumFulFillmentCost(calculationSummary.getSumFulFillmentCost());
		calculationSummaryDTO.setSumNetSalesAp(calculationSummary.getSumNetSalesAp());
		calculationSummaryDTO.setSumNetSalesBp(calculationSummary.getSumNetSalesBp());
		calculationSummaryDTO.setVmDollarHurdleGreen(calculationSummary.getVmDollarHurdleGreen());
		calculationSummaryDTO.setSumOtherCost(calculationSummary.getSumOtherCost());
		calculationSummaryDTO.setSumProdVarMargin(calculationSummary.getSumProdVarMargin());
		calculationSummaryDTO.setSumTotActualInvoice(calculationSummary.getSumTotActualInvoice());
		calculationSummaryDTO.setSumTotDispCostNoShipping(calculationSummary.getSumTotDispCostNoShipping());
		calculationSummaryDTO.setSumTotDisplayCostShipping(calculationSummary.getSumTotDisplayCostShipping());
		calculationSummaryDTO.setVmDollarHurdleGreen(calculationSummary.getVmDollarHurdleGreen());
		calculationSummaryDTO.setVmDollarHurdleRed(calculationSummary.getVmDollarHurdleRed());
		calculationSummaryDTO.setVmPctHurdleGreen(calculationSummary.getVmPctHurdleGreen());
		calculationSummaryDTO.setVmPctHurdleRed(calculationSummary.getVmPctHurdleRed());
		calculationSummaryDTO.setHurdles(calculationSummary.getHurdles());
		calculationSummaryDTO.setSumTradeSales(calculationSummary.getSumTradeSales());
		calculationSummaryDTO.setPromoPeriodId(calculationSummary.getPromoPeriodId());
		calculationSummaryDTO.setExcessInvtVmPct(calculationSummary.getExcessInvtVmPct());
		calculationSummaryDTO.setExcessInvtVmDollar(calculationSummary.getExcessInvtVmDollar());
		calculationSummaryDTO.setCorrugateCostPerUnit(calculationSummary.getCorrugateCostPerUnit());
		calculationSummaryDTO.setLaborCostPerUnit(calculationSummary.getLaborCostPerUnit());
		calculationSummaryDTO.setSumVmDollarAp(calculationSummary.getSumVmDollarAp());
		calculationSummaryDTO.setSumVmPctAp(calculationSummary.getSumVmPctAp());
		calculationSummaryDTO.setMdfPct(calculationSummary.getMdfPct());
		calculationSummaryDTO.setDiscountPct(calculationSummary.getDiscountPct());
		return calculationSummaryDTO;
	}

	public static CalculationSummary convert(CalculationSummaryDTO calculationSummaryDTO) {
		if (calculationSummaryDTO == null) return null;
		CalculationSummary calculationSummary = new CalculationSummary();
		calculationSummary.setCalculationSummaryId(calculationSummaryDTO.getCalculationSummaryId());
		calculationSummary.setDisplayId(calculationSummaryDTO.getDisplayId()); 
		calculationSummary.setDisplayScenarioId(calculationSummaryDTO.getDisplayScenarioId());
		calculationSummary.setActualQty(calculationSummaryDTO.getActualQty());
		calculationSummary.setAverySku(calculationSummaryDTO.getAverySku());
		calculationSummary.setCustomerId(calculationSummaryDTO.getCustomerId());
		calculationSummary.setCustProgDisc(calculationSummaryDTO.getCustProgDisc());
		calculationSummary.setDescription(calculationSummaryDTO.getDescription());
		calculationSummary.setForeCastQty(calculationSummaryDTO.getForeCastQty());
		calculationSummary.setHurdles(calculationSummaryDTO.getHurdles());
		calculationSummary.setNumberFacing(calculationSummaryDTO.getNumberFacing());
		calculationSummary.setPromoPeriodId(calculationSummaryDTO.getPromoPeriodId());
		calculationSummary.setQtyPerDisplay(calculationSummaryDTO.getQtyPerDisplay());
		calculationSummary.setQtyPerFacing(calculationSummaryDTO.getQtyPerFacing());
		calculationSummary.setShippingCost(calculationSummaryDTO.getShippingCost());
		calculationSummary.setSku(calculationSummaryDTO.getSku());
		calculationSummary.setStatusId(calculationSummaryDTO.getStatusId());
		calculationSummary.setSumCogs(calculationSummaryDTO.getSumCogs());
		calculationSummary.setSumCorrugateCost(calculationSummaryDTO.getSumCorrugateCost());
		calculationSummary.setSumDisplayCost(calculationSummaryDTO.getSumDisplayCost());
		calculationSummary.setSumFulFillmentCost(calculationSummaryDTO.getSumFulFillmentCost());
		calculationSummary.setSumVmDollarBp(calculationSummaryDTO.getSumVmDollarBp());
		calculationSummary.setSumVmPctBp(calculationSummaryDTO.getSumVmPctBp());
		calculationSummary.setSumNetSalesAp(calculationSummaryDTO.getSumNetSalesAp());
		calculationSummary.setSumNetSalesBp(calculationSummaryDTO.getSumNetSalesBp());
		calculationSummary.setVmDollarHurdleGreen(calculationSummaryDTO.getVmDollarHurdleGreen());
		calculationSummary.setSumOtherCost(calculationSummaryDTO.getSumOtherCost());
		calculationSummary.setSumProdVarMargin(calculationSummaryDTO.getSumProdVarMargin());
		calculationSummary.setSumTotActualInvoice(calculationSummaryDTO.getSumTotActualInvoice());
		calculationSummary.setSumTotDispCostNoShipping(calculationSummaryDTO.getSumTotDispCostNoShipping());
		calculationSummary.setSumTotDisplayCostShipping(calculationSummaryDTO.getSumTotDisplayCostShipping());
		calculationSummary.setVmDollarHurdleGreen(calculationSummaryDTO.getVmDollarHurdleGreen());
		calculationSummary.setVmDollarHurdleRed(calculationSummaryDTO.getVmDollarHurdleRed());
		calculationSummary.setVmPctHurdleGreen(calculationSummaryDTO.getVmPctHurdleGreen());
		calculationSummary.setVmPctHurdleRed(calculationSummaryDTO.getVmPctHurdleRed());
		calculationSummary.setHurdles(calculationSummaryDTO.getHurdles());
		calculationSummary.setSumTradeSales(calculationSummaryDTO.getSumTradeSales());
		calculationSummary.setStatusId(calculationSummaryDTO.getStatusId());
		calculationSummary.setPromoPeriodId(calculationSummaryDTO.getPromoPeriodId());
		calculationSummary.setExcessInvtVmPct(calculationSummaryDTO.getExcessInvtVmPct());
		calculationSummary.setExcessInvtVmDollar(calculationSummaryDTO.getExcessInvtVmDollar());
		calculationSummary.setCorrugateCostPerUnit(calculationSummaryDTO.getCorrugateCostPerUnit());
		calculationSummary.setLaborCostPerUnit(calculationSummaryDTO.getLaborCostPerUnit());
		calculationSummary.setSumVmDollarAp(calculationSummaryDTO.getSumVmDollarAp());
		calculationSummary.setSumVmPctAp(calculationSummaryDTO.getSumVmPctAp());
		calculationSummary.setMdfPct(calculationSummaryDTO.getMdfPct());
		calculationSummary.setDiscountPct(calculationSummaryDTO.getDiscountPct());
		return calculationSummary;
	}

	public static List<UserTypeDTO> convertUserTypes(List<UserType> userTypes) {
		if (userTypes == null) return null;
		List<UserTypeDTO> result = new ArrayList<UserTypeDTO>();
		for (int i = 0; i < userTypes.size(); i++) {
			result.add(convert(userTypes.get(i)));
		}
		return result;
	}

	private static UserTypeDTO convert(UserType userType) {
		if (userType == null) return null;
		UserTypeDTO result = new UserTypeDTO();
		result.setUserTypeId(userType.getUserTypeId());
		result.setUserTypeName(userType.getTypeName());
		result.setActive(userType.isActiveFlg());
		return result;
	}

	public static ProgramBuDTO convert(ProgramBu program) {
		if (program == null) return null;
		ProgramBuDTO result = new ProgramBuDTO();
		result.setBuCode(program.getBu().getBuCode());
		result.setBuDesc(program.getBu().getBuDesc());
		result.setDisplayId(program.getDisplayId());
		result.setDtCreated(program.getDtCreated());
		result.setDtLastChanged(program.getDtLastChanged());
		result.setProgramBuId(program.getProgramBuId());
		result.setProgramPct(program.getProgramPct());
		result.setUserCreated(program.getUserCreated());
		result.setUserLastChanged(program.getUserLastChanged());
		return result;		
	}

	public static List<ProgramBuDTO> convertProgramBus(List<ProgramBu> programs) {
		if (programs == null) return null;
		List<ProgramBuDTO> result = new ArrayList<ProgramBuDTO>();
		for (int i = 0; i < programs.size(); i++) {
			result.add(convert(programs.get(i)));
		}
		return result;
	}
	
	
	public static CstBuPrgmPctDTO convert(CstBuPrgmPct cstBuPrgmPct) {
		if (cstBuPrgmPct == null) return null;
		CstBuPrgmPctDTO result = new CstBuPrgmPctDTO();
		result.setActiveFlg(cstBuPrgmPct.getActiveFlg());
		result.setBuCode(cstBuPrgmPct.getBu().getBuCode());
		result.setCstBuPrgmPctId(cstBuPrgmPct.getCstBuPrgmPctId());
		result.setCustomerId(cstBuPrgmPct.getCustomer().getCustomerId());
		result.setFyear(cstBuPrgmPct.getFyear());
		result.setProgramPct(cstBuPrgmPct.getProgramPct());
		return result;		
	}

	public static List<CstBuPrgmPctDTO> convertCstBuPrgmPcts(List<CstBuPrgmPct> cstBuPrgmPcts) {
		if (cstBuPrgmPcts == null) return null;
		List<CstBuPrgmPctDTO> result = new ArrayList<CstBuPrgmPctDTO>();
		for (int i = 0; i < cstBuPrgmPcts.size(); i++) {
			result.add(convert(cstBuPrgmPcts.get(i)));
		}
		return result;
	}

	

}
