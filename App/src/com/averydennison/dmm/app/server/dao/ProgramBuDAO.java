package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.ProgramBu;

import java.util.List;

public interface ProgramBuDAO {

    public void persist(ProgramBu transientInstance);

    public void attachDirty(ProgramBu instance);

    public void attachClean(ProgramBu instance);

    public void delete(ProgramBu persistentInstance);

    public ProgramBu merge(ProgramBu detachedInstance);

    public ProgramBu findById(Long id);
    
    public List<ProgramBu> findProgramBuByDisplayId(long displayId);

}
