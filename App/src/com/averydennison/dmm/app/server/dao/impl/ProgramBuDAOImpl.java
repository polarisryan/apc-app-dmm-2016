package com.averydennison.dmm.app.server.dao.impl;

import com.averydennison.dmm.app.server.dao.ProgramBuDAO;
import com.averydennison.dmm.app.server.persistence.ProgramBu;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import java.util.List;

/**
 * User: Spart Arguello
 * Date: Dec 8, 2009
 * Time: 3:16:16 PM
 */
public class ProgramBuDAOImpl extends HibernateDaoSupport implements ProgramBuDAO {
    private static final Log log = LogFactory.getLog(ProgramBuDAOImpl.class);

    public void persist(ProgramBu transientInstance) {
        log.debug("persisting ProgramBu instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(ProgramBu instance) {
        log.debug("attaching dirty ProgramBu instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(ProgramBu instance) {
        log.debug("attaching clean ProgramBu instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(ProgramBu persistentInstance) {
        log.debug("deleting ProgramBu instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public ProgramBu merge(ProgramBu detachedInstance) {
        log.debug("merging ProgramBu instance");
        try {
        	ProgramBu result = (ProgramBu) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public ProgramBu findById(Long id) {
        log.debug("getting ProgramBu instance with id: " + id);
        try {
        	ProgramBu instance = (ProgramBu) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.ProgramBu",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    public List<ProgramBu> findProgramBuByDisplayId(long displayId){
    	log.debug("getting ProgramBus for display id " + displayId);
        try {
        	Query q = getSession().getNamedQuery("programBuByDisplayId");
            q.setLong(0, displayId);
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            return (List<ProgramBu>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

}
