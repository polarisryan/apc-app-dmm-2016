package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.DisplaySetupDAO;
import com.averydennison.dmm.app.server.persistence.DisplaySetup;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.Status
 */
public class DisplaySetupDAOImpl extends HibernateDaoSupport implements DisplaySetupDAO {

    private static final Log log = LogFactory.getLog(DisplaySetupDAOImpl.class);

    public void persist(DisplaySetup transientInstance) {
        log.debug("persisting DisplaySetup instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(DisplaySetup instance) {
        log.debug("attaching dirty DisplaySetup instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(DisplaySetup instance) {
        log.debug("attaching clean DisplaySetup instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(DisplaySetup persistentInstance) {
        log.debug("deleting DisplaySetup instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public DisplaySetup merge(DisplaySetup detachedInstance) {
        log.debug("merging DisplaySetup instance");
        try {
            DisplaySetup result = (DisplaySetup) getHibernateTemplate().merge(
                    detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public DisplaySetup findById(Long id) {
        log.debug("getting DisplaySetup instance with id: " + id);
        try {
            DisplaySetup instance = (DisplaySetup) getHibernateTemplate().get(
                    "com.averydennison.dmm.app.server.persistence.DisplaySetup", id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<DisplaySetup> getDisplaySetups() {
        log.debug("getting DisplaySetup");
        Session s = null;
        try {
            Query q = getSession().getNamedQuery("displaysetups");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<DisplaySetup>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
