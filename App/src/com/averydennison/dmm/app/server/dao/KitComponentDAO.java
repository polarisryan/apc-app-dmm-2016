package com.averydennison.dmm.app.server.dao;

import java.util.Date;
import java.util.List;

import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.averydennison.dmm.app.server.persistence.CostAnalysisKitComponent;
import com.averydennison.dmm.app.server.persistence.KitComponent;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:47:47 PM
 */
public interface KitComponentDAO {
    public void persist(KitComponent transientInstance);

    public void attachDirty(KitComponent instance);

    public void attachClean(KitComponent instance);

    public void delete(KitComponent persistentInstance);

    public KitComponent merge(KitComponent detachedInstance);

    public KitComponent findById(Long id);
    
    public List<KitComponent> findByDisplayId(Long displayId);
    public List<KitComponent> findByDisplayScenarioId(Long displayScenarioId);
    
    public List<CostAnalysisKitComponent> findCostAnalysisKitComponentByDisplayId(Long displayId, Long displayScenarioId);
    
    public void callILSPriceExceptionForKitComponent(Long kitComponentId,Date reCalcDate) ;
    
    public void updateReCalcDate(Long displayId,Date reCalcDate) ;


}
