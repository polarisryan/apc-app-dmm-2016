package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.LocationCountry;

public interface LocationCountryDAO {

    public LocationCountry findById(Long id);

    public List<LocationCountry> getAllActiveLocationsCountry();

	public List<LocationCountry> getAllLocationsCountry();
}