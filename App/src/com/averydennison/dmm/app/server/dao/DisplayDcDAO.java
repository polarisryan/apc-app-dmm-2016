package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayDc;
import com.averydennison.dmm.app.server.persistence.DisplayDcAndPackout;

import java.util.List;
import java.util.Map;

/**
 * User: Spart Arguello
 * Date: Dec 8, 2009
 * Time: 3:14:17 PM
 */
public interface DisplayDcDAO {

    public void persist(DisplayDc transientInstance);

    public void attachDirty(DisplayDc instance);

    public void attachClean(DisplayDc instance);

    public void delete(DisplayDc persistentInstance);

    public DisplayDc merge(DisplayDc detachedInstance);

    public DisplayDc findById(Long id);
    
    public List<DisplayDc> findDisplayDcByDisplayId(long displayId);

    public Map<Long, DisplayDc> findDisplayDcMapByDisplay(Display display);
    
    public List<DisplayDcAndPackout> findDisplayDcAndPackoutsByDisplay(Display display);
}
