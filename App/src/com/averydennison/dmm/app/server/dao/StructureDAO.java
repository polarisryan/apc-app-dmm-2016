package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.Structure;

import java.util.List;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:51:19 PM
 */
public interface StructureDAO {
    public void persist(Structure transientInstance);

    public void attachDirty(Structure instance);

    public void attachClean(Structure instance);

    public void delete(Structure persistentInstance);

    public Structure merge(Structure detachedInstance);

    public Structure findById(Long id);

    public List<Structure> getStructures();

	public List<Structure> getAllStructures();
}
