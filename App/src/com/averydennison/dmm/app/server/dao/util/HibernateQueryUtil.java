package com.averydennison.dmm.app.server.dao.util;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.math.BigDecimal;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Utilize the HQL query support functions for executing rule and solving Hibernate query
 * problems.
 * <p/>
 * ser: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 9:24:53 AM
 */
public class HibernateQueryUtil {
    public static final int MAX_IN = 900;
    public static final String IN_OP = "IN";
    public static final String NOTIN_OP = "NOT IN";
    public static final String TRUE_QRY = "1=1";
    public static final String FALSE_QRY = "1=0";
    //private static final Logger log = Logger.getLogger(HibernateQueryUtil.class);

    /**
     * It used for split a big in statement to a few sub statements conjuncted by "OR".
     *
     * @param inStatement such as: account in (a1, a2, ...a1001);
     * @return split statement, such as: account in (a1,..a499) OR...OR account in
     *         (a501,..a1000) OR account in (a1001)
     */
    public static String splitBigInStatement(String inStatement) {
        List<String> pList = parseHQLStatmentForMultParam(inStatement);
        List<String> paramList = pList.subList(1, pList.size());
        if (paramList.size() <= MAX_IN) {
            return inStatement;
        }

        List<String> newParams = new ArrayList<String>();
        int count = 0;
        StringBuffer sb = new StringBuffer();
        for (String param : paramList) {
            if (count >= MAX_IN) {
                newParams.add(sb.toString());
                count = 0;
                sb = new StringBuffer();
            } else {
                if (count > 0) {
                    sb.append(",");
                }
            }

            sb.append(param);
            count++;
        }

        if (sb.length() > 0) {
            newParams.add(sb.toString());
        }

        sb = new StringBuffer();
        for (String s : newParams) {
            if ((s == null) || (s.trim().length() == 0)) {
                continue;
            }

            if (sb.length() > 0) {
                sb.append(" OR ");
            }

            sb.append("( ");
            sb.append(pList.get(0));
            sb.append(" ");
            sb.append(IN_OP);
            sb.append(" (");
            sb.append(s);
            sb.append(" ) ) ");
        }

        return "( " + sb.toString() + " )";

    }

    /**
     * Initialize lazy loaded object in hibernate session. This function call should be in
     * an active hibernate session, not any where. (Should this method be here properly? )
     *
     * @param o DOCUMENT ME!
     * @return DOCUMENT ME!
     */
    public static Object initializeHibernateObject(Object o) {
        Hibernate.initialize(o);

        return o;
    }


    /**
     * Split large parameter collection to serve JDBC preparedStatement.
     *
     * @param params       given collection of parameters
     * @param inStatements empty not null list
     * @return DOCUMENT ME!
     */
    public static <T> Map<String, List<T>> splitParamsForJDBC(Collection<T> params, List<String> inStatements) {
        Map<String, List<T>> result = new HashMap<String, List<T>>();
        if ((params == null) || params.isEmpty()) {
            return null;
        }

        StringBuffer keySb = new StringBuffer();
        List<T> paramOut = new ArrayList<T>();
        int count = 0;
        int round = 0;
        for (T param : params) {
            paramOut.add(param);
            if (count == 0) {
                keySb = new StringBuffer();
                keySb.append("?");
            } else if (count > 0) {
                keySb.append(",?");
            }

            if (count > MAX_IN) {
                inStatements.add(keySb.toString());
                result.put(String.valueOf(round), paramOut);
                keySb = new StringBuffer();
                paramOut = new ArrayList<T>();
                round++;
            }

            count++;
        }

        return result;
    }

    /**
     * DOCUMENT ME!
     *
     * @param cs     DOCUMENT ME!
     * @param params DOCUMENT ME!
     * @throws SQLException DOCUMENT ME!
     */
    public static void setParamsForCallableStatement(CallableStatement cs, List<Object> params)
            throws SQLException {
        if ((params == null) || params.isEmpty()) {
            return;
        }

        int index = 1;
        for (Object param : params) {
            if (param instanceof Long) {
                cs.setLong(index, (Long) param);
            } else if (param instanceof Integer) {
                cs.setInt(index, (Integer) param);
            } else if (param instanceof java.sql.Date) {
                cs.setDate(index, (java.sql.Date) param);
            } else if (param instanceof String) {
                cs.setString(index, (String) param);
            } else {
                cs.setObject(index, param);
            }

            index++;
        }
    }

    /**
     * It used for split a big in statement to a few sub statements conjuncted by "OR".
     * Usage: 1) re-compose the HQL query string: original:<code>sb.append(" and  ac IN
     * (:accounts) ");</code> Change: <code>Map<String, List> resultParamMap = new
     * HashMap<String, List>(); sb.append(" and  "); String inStatement = "ac IN
     * (:accounts) "; String resultHql =HibernateQueryUtil.splitBigInStatement(accounts,
     * inStatement, resultParamMap); sb.append(resultHql);</code> 2) Set up the
     * parameterLists for the query: original:<code>query.setParameterList("accounts",
     * accounts)</code> Change: <code>if (resultParamMap!=null &&
     * !resultParamMap.isEmpty()){ for (String param: resultParamMap.keySet()){
     * query.setParameterList(param, resultParamMap.get(param));}}</code>
     *
     * @param params         DOCUMENT ME! list value for the paramId.
     * @param inStatement    DOCUMENT ME! format like "ac IN (:paramId)" or "ac NOT IN
     *                       (:paramId)"
     * @param resultParamMap require not null Map as return reference map. By pass in
     *                       an empty map, return a parameter map: key-- paramId,
     *                       value-- list HQL sql string
     * @return DOCUMENT ME!
     */
    @SuppressWarnings("unchecked")
    public static String splitBigInStatement(List params, String inStatement, Map<String, List> resultParamMap) {
        if ((params == null) || params.isEmpty()) {
            return null;
        }

        String op = IN_OP;
        String conjunctionOp = ConjunctionOperator.OR.name();
        if (inStatement.toUpperCase().indexOf(NOTIN_OP) > 0) {
            op = NOTIN_OP;
            conjunctionOp = ConjunctionOperator.AND.name();
        }

        String[] paramIds = parseHQLStatmentForSingleParam(inStatement, op);
        if ((paramIds == null) || (paramIds.length == 0)) {
            return null;
        }

        // format alia name to be paramPrefix
        String paramPrefix = formatParam(paramIds[0]);
        String baseParamId = "A0" + paramPrefix + "00" + paramIds[1];
        String qry1 = Utility.replaceAll(inStatement, ":" + paramIds[1], ":" + baseParamId);
        if (params.size() <= MAX_IN) {
            resultParamMap.put(baseParamId, params);

            return qry1;
        }

        StringBuffer sb = new StringBuffer();
        sb.append("(" + qry1 + " ) ");
        int total = params.size();
        int rest = total;
        int fromIndex = 0;
        int toIndex = 0;
        List subList = null;
        int count = 0;
        for (int i = 0; rest > MAX_IN; i++) {
            fromIndex = MAX_IN * i;
            if (rest > MAX_IN) {
                toIndex = fromIndex + MAX_IN;
            } else {
                toIndex = total;
            }

            subList = params.subList(fromIndex, toIndex);
            if (i == 0) {
                resultParamMap.put(baseParamId, subList);
            } else {
                String newParamId = "B" + i + baseParamId + i;
                String qry = Utility.replaceAll(inStatement, ":" + paramIds[1], ":" + newParamId);
                resultParamMap.put(newParamId, subList);
                sb.append(" ").append(conjunctionOp).append(" (");
                sb.append(qry);
                sb.append(" ) ");
                //log.info("Split big IN statement: " + sb.toString());
            }

            rest = rest - MAX_IN;
            count++;
        }

        if ((rest > 0) && (rest <= MAX_IN)) {

            // lets take care of the rest of the list
            fromIndex = toIndex;
            toIndex = rest + toIndex;
            subList = params.subList(fromIndex, toIndex);
            String newParamId = "C" + count + baseParamId + count;
            String qry = Utility.replaceAll(inStatement, ":" + paramIds[1], ":" + newParamId);
            resultParamMap.put(newParamId, subList);
            sb.append(" ").append(conjunctionOp).append(" (");
            sb.append(qry);
            sb.append(" ) ");
        }

        return " (" + sb.toString() + ") ";
    }


    /**
     * DOCUMENT ME!
     *
     * @param preparedStatement DOCUMENT ME!
     * @param index             DOCUMENT ME!
     * @param param             DOCUMENT ME!
     * @throws SQLException DOCUMENT ME!
     */
    public static void setParamForPreparedStatement(PreparedStatement preparedStatement, int index, Object param)
            throws SQLException {

        // log.info("index:"+ index+"==>" + param);
        if (param instanceof Long) {
            preparedStatement.setLong(index, (Long) param);
        } else if (param instanceof Integer) {

            // log.info("index:"+ index+"==>" + param);
            preparedStatement.setInt(index, (Integer) param);
        } else if (param instanceof java.sql.Date) {
            preparedStatement.setDate(index, (java.sql.Date) param);
        } else if (param instanceof Timestamp) {
            preparedStatement.setTimestamp(index, (Timestamp) param);
        } else if (param instanceof String) {
            preparedStatement.setString(index, (String) param);
        } else {
            preparedStatement.setObject(index, param);
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param conn          DOCUMENT ME!
     * @param procedureName DOCUMENT ME!
     * @param paramNum      DOCUMENT ME!
     * @return DOCUMENT ME!
     * @throws SQLException DOCUMENT ME!
     */
    public static CallableStatement createCallableStatement(Connection conn, String procedureName, int paramNum)
            throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append(" call ").append(procedureName).append("(");
        if (paramNum > 0) {
            for (int i = 0; i < paramNum; i++) {
                if (i > 0) {
                    sb.append(",");
                }

                sb.append("?");
            }
        }

        sb.append(")");
        CallableStatement cs = conn.prepareCall(sb.toString());

        return cs;
    }

    /**
     * DOCUMENT ME!
     *
     * @param whereClause      DOCUMENT ME!
     * @param bindVariablesMap DOCUMENT ME!
     * @param hqlStatement     DOCUMENT ME!
     * @param session          DOCUMENT ME!
     * @return DOCUMENT ME!
     */
    @SuppressWarnings("unchecked")
    public static List executeHibernateQueryByValueHolder(String whereClause, Map<DataType, Map<String, Object>> bindVariablesMap,
                                                          StringBuffer hqlStatement, Session session) {
        if ((whereClause != null) && (whereClause.length() > 0)) {
            if (hqlStatement.toString().toUpperCase().indexOf(" WHERE ") < 0) {
                hqlStatement.append(" where ");
            } else {
                hqlStatement.append(" AND ");
            }

            hqlStatement.append(whereClause);
        }
//        if (log.isDebugEnabled()) {
//            log.debug("%%%%%hqlStatement: " + hqlStatement.toString());
//        }
        org.hibernate.Query query = session.createQuery(hqlStatement.toString());
        if (bindVariablesMap != null) {
            for (DataType d : bindVariablesMap.keySet()) {
                Map<String, Object> map1 = bindVariablesMap.get(d);
                for (String s : map1.keySet()) {
                    switch (d) {

                        case StringType: {
                            query.setString(s, (String) map1.get(s));

                            break;
                        }

                        case LongType: {
                            query.setLong(s, (Long) map1.get(s));

                            break;
                        }

                        case IntegerType: {
                            query.setInteger(s, (Integer) map1.get(s));

                            break;
                        }

                        case DoubleType: {
                            query.setDouble(s, (Double) map1.get(s));

                            break;
                        }

                        case FloatType: {
                            query.setFloat(s, (Float) map1.get(s));

                            break;
                        }

                        case DateType: {
                            query.setDate(s, (Date) map1.get(s));

                            break;
                        }

                        case TimeStampType: {
                            query.setTimestamp(s, (Timestamp) map1.get(s));

                            break;
                        }

                        default: {
                            query.setEntity(s, map1.get(s));

                            break;
                        }
                    }
                }
            }
        }

        List result = null;
        try {
            result = query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            //log.error("Excute Rule error:", e);
        }

        return result;
    }

    /**
     * Utility Method to Run a Native SQL query and give back the Appropriate Domain
     * Objects using the Passed in DAO Useful when there is a need to retrieve Domain
     * Objects but cannot use Certain SQL features in HQL
     *
     * @param session          Hibernate Session (SHort or Long)
     * @param sql              The Native SQL Query Strign with bind Variables (Not
     *                         HQL)
     * @param bindVariablesMap Bind Variables Map
     * @param dao              The CallBack DAO to use for retreiving objects from
     *                         objectIds
     * @param tClass           Class of the Object
     * @return List of Domain Objects
     */
    @SuppressWarnings("unchecked")
    public static <T extends Object> List<T> getResultsThroughNativeSQL(Session session, String sql,
                                                                        Map<DataType, Map<String, Object>> bindVariablesMap, AbstractHibernateDAO dao, Class<T> tClass) {

        final List<Long> objectIds = new ArrayList<Long>();

        SQLQuery query = session.createSQLQuery(sql);

        if (bindVariablesMap != null) {
            for (DataType d : bindVariablesMap.keySet()) {
                Map<String, Object> map1 = bindVariablesMap.get(d);

                for (String s : map1.keySet()) {
                    switch (d) {

                        case StringType: {
                            query.setString(s, (String) map1.get(s));

                            break;
                        }

                        case LongType: {
                            query.setLong(s, (Long) map1.get(s));

                            break;
                        }

                        case IntegerType: {
                            query.setInteger(s, (Integer) map1.get(s));

                            break;
                        }

                        case DoubleType: {
                            query.setDouble(s, (Double) map1.get(s));

                            break;
                        }

                        case FloatType: {
                            query.setFloat(s, (Float) map1.get(s));

                            break;
                        }

                        case DateType: {
                            query.setDate(s, (Date) map1.get(s));

                            break;
                        }

                        case TimeStampType: {
                            query.setTimestamp(s, (Timestamp) map1.get(s));

                            break;
                        }
                    } // switch
                } // for
            } // for

            List<Object[]> l1 = query.list();

            if ((l1 != null) && !l1.isEmpty()) {

                for (Object[] obj : l1) {
                    BigDecimal bd = (BigDecimal) obj[0];
                    objectIds.add(bd.longValue());
                }

                return dao.loadByIds(objectIds);
            }

        } // bindVariableMap != null

        return new ArrayList<T>();
    }


    /**
     * Utility Method to Split Big In Lists when using JDBC PreparedStatements THis method
     * should be used when directly constructing your Individual bind variable parameters
     * as opposed to using List Bind Variable Parameters This method will split the In
     * List into Individual Bind Variable Parameters, Not Lists, For Example it will
     * convert ISV.ACCOUNT_ID IN (?) to ISV.ACCOUNT_ID IN (?,?,?,....... 500 times...??)
     * The Caller is responsible for Tracking the Bind Variable Indexes when calling into
     * this by passing the right Start Index.
     *
     * @param params                   Collection of Objects
     * @param inListStatement          In Statement, Has to be of the Form:
     *                                 ISV.ENTITY_ID IN (?)
     * @param processedInListStatement This will hold the Transformed In Statement
     * @param parameterMap             Map of Bind Variable Index to Object
     * @param startIndex               The Start Index for the Bind Variable Position
     * @return DOCUMENT ME!
     */
    public static <T> long splitParamsForJDBC(Collection<T> params, String inListStatement, StringBuffer processedInListStatement,
                                              LinkedHashMap<Long, T> parameterMap, long startIndex) {
        if ((params == null) || params.isEmpty()) {
            return startIndex;
        }

        if (startIndex <= 0) {
            return startIndex;
        }

        StringBuffer returnSB = new StringBuffer();
        StringBuffer keySb = new StringBuffer();
        int count = 0;
        int round = 0;
        boolean maxOut = false;
        for (T param : params) {
            maxOut = false;
            parameterMap.put(startIndex, param);
            startIndex++;
            if (count == 0) {
                keySb = new StringBuffer();

                keySb.append(" ? ");

            } else if (count > 0) {

                keySb.append(",? ");

            }

            if (count > MAX_IN) {
                count = -1;
                maxOut = true;
                if (round > 0) {
                    returnSB.append(" OR ");
                }

                returnSB.append(StringUtils.replace(inListStatement, "?", keySb.toString()));
                keySb = new StringBuffer();
                round++;
            }

            count++;
        }

        if (!maxOut) {
            if (round > 0) {
                returnSB.append(" OR ");
            }

            returnSB.append(StringUtils.replace(inListStatement, "?", keySb.toString()));
        }

        processedInListStatement.append(returnSB);

        return startIndex;

    }

    /*
     * Check given HQL query string inStatement by operator and return the parameter name.
     *
     * @param inStatement: such as: acc In (:paramId1, :paramId2...) @param operator: HQL query operator, such as "IN" @return [0]
     * alia name, [1..n]paramenter names in the query statement
     */
    private static List<String> parseHQLStatmentForMultParam(String inStatement) {
        if ((inStatement == null) || (inStatement.trim().length() == 0)) {
            return null;
        }

        String tmp = inStatement.trim();
        List<String> resultList = new ArrayList<String>();
        while (tmp.startsWith("(")) {
            tmp = tmp.substring(1).trim();
        }

        List<String> strs = parseStringByLinkToken(tmp, " ");
        String aliaName = strs.get(0);
        resultList.add(aliaName);
        int startIndex = tmp.indexOf("( ");
        int endIndex = tmp.indexOf(")");
        tmp = tmp.substring(startIndex + 1, endIndex).trim();
        List<String> paramList = parseStringByLinkToken(tmp, ",");
        resultList.addAll(paramList);

        return resultList;
    }

    /**
     * Method: parse a given string by the token LINK_TOKEN
     *
     * @param name
     * @return
     */
    public static List<String> parseStringByLinkToken(String name, String delimiter) {
        String temp = Utility.replaceAll(name, delimiter, "~");
        StringTokenizer st = new StringTokenizer(temp, "~");
        List<String> result = new ArrayList<String>();
        while (st.hasMoreTokens()) {
            result.add(st.nextToken());
        }

        return result;

    }


    /*
    * Format given string to return a quality parameter string based on HQL parameter syntax. @param originStr @return
    */
    private static String formatParam(String originStr) {
        String[] escapeChars = {
                ".",
                "*",
                "-",
                "_"
        };
        String result = originStr;
        for (String s : escapeChars) {
            if (result.indexOf(s) >= 0) {
                result = Utility.replaceAll(result, s, "0");
            }
        }

        return result;
    }

    ///////////////////////////////////////////////////////////////////////////
    // ====================Start of Private method Section====================//
    // please add public method above Private method section                 ///
    ///////////////////////////////////////////////////////////////////////////
    /*
     * Check given HQL query string inStatement by operator and return the parameter name.
     *
     * @param inStatement: such as: acc In (:paramName) @param operator: HQL query operator, such as "IN" @return [0] alia name,
     * [1]paramenter name in the query statement
     */
    private static String[] parseHQLStatmentForSingleParam(String inStatement, String operator) {
        if ((inStatement == null) || (inStatement.trim().length() == 0)) {
            return null;
        }

        String workStr = inStatement.trim();
        String[] resultArray = new String[2];
        int opIndex = inStatement.trim().toUpperCase().indexOf(" " + operator.toUpperCase());
        String tmp = workStr;
        while (tmp.startsWith("(")) {
            tmp = tmp.substring(1).trim();
        }

        List<String> strs = parseStringByLinkToken(tmp, " ");
        resultArray[0] = strs.get(0);

        if (opIndex >= 0) {
            for (String s : strs) {
                if (s.indexOf(":") >= 0) {
                    int lastIndex = s.indexOf(")");
                    if (lastIndex > 0) {
                        resultArray[1] = s.substring(s.indexOf(":") + 1, lastIndex).trim();
                    } else {
                        resultArray[1] = s.substring(s.indexOf(":") + 1).trim();
                    }
                }
            }
        }

        return resultArray;
    }

    private static void collectBindVariablesMap(Map<DataType, Map<String, Object>> bindVariablesMap,
                                                Map<DataType, Map<String, Object>> bindVarMap) {
        Map<String, Object> subMap = null;
        Map<String, Object> subTotalMap = null;
        for (Map.Entry<DataType, Map<String, Object>> entry1 : bindVarMap.entrySet()) {
            DataType key = entry1.getKey();
            subMap = entry1.getValue();
            if ((subMap == null) || subMap.isEmpty()) {
                continue;
            }

            subTotalMap = bindVariablesMap.get(key);
            if (subTotalMap == null) {
                subTotalMap = new HashMap<String, Object>();
            }

            subTotalMap.putAll(subMap);
            bindVariablesMap.put(key, subTotalMap);
        }
    }

    public static String trimQueryEmptyBlock(String qry) {
        if (qry == null || qry.length() == 0) {
            return qry;
        }
        String workSQL = qry;
        String[] emptyTrails = {"and  (  )", "or  (  )", "AND  (  )", "OR  (  )", "and  ( )", "or  ( )", "AND  ( )", "OR  ( )", "and ( )", "or ( )", "AND ( )", "OR ( )"};
        String[] emptyTrailsSolution = {"and  (1=1)",
                "or  (1=0)",
                "AND  (1=1)", "OR  (1=0)", "and  (1=1)", "or  (1=0)", "AND  (1=1)", "OR  (1=0)", "and  (1=1)", "or  (1=0)", "AND  (1=1)", "OR  (1=0)"};
        for (int idx = 0; idx < emptyTrails.length; idx++) {
            workSQL = Utility.replaceAll(workSQL, emptyTrails[idx], emptyTrailsSolution[idx]);
        }
        return workSQL;

    }
}
