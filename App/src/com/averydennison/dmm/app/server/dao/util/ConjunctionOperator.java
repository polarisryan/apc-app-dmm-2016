package com.averydennison.dmm.app.server.dao.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Define conjunction operation for linking UserRuleExpressions.
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 9:35:31 AM
 */
public enum ConjunctionOperator {

    OR("OR"), AND("AND"), ANDNOT("AND NOT"), ORNOT("OR NOT");


    private String name;

    private List<ConjunctionOperator> allOps = new ArrayList<ConjunctionOperator>();


    private ConjunctionOperator(String name) {

        setName(name);

    }


    /**
     * get $param.name$
     *
     * @return
     */
    public String getName() {

        return name;

    }


    /**
     * set value for attribute
     *
     * @param name
     */
    public void setName(String name) {

        this.name = name;

    }


    /**
     * Method: DOCUMENT ME!
     *
     * @return
     */
    public List<ConjunctionOperator> listAllOperators() {

        return allOps;

    }


    /**
     * get Operator object by given name
     *
     * @param name
     * @return
     */
    public ConjunctionOperator getOperator(String name) {

        if (name.equals(AND.getName())) {

            return AND;

        } else if (name.equals(ANDNOT.getName())) {

            return ANDNOT;

        } else if (name.equals(OR.getName())) {

            return OR;

        } else if (name.equals(ORNOT.getName())) {

            return ORNOT;

        }

        return null;
    }
}
