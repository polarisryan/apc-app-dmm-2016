package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.averydennison.dmm.app.server.dao.CstBuPrgmPctDAO;
import com.averydennison.dmm.app.server.persistence.CstBuPrgmPct;
import com.averydennison.dmm.app.server.persistence.Customer;
import com.averydennison.dmm.app.server.persistence.ValidBu;

public class CstBuPrgmPctDAOImpl extends HibernateDaoSupport implements CstBuPrgmPctDAO {
	private static final Log log = LogFactory.getLog(CstBuPrgmPctDAOImpl.class);
	@Override
	public void attachClean(CstBuPrgmPct instance) {
		  log.debug("attaching clean CstBuPrgmPct instance");
	        try {
	            getHibernateTemplate().lock(instance, LockMode.NONE);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	}

	@Override
	public void attachDirty(CstBuPrgmPct instance) {
		   log.debug("attaching dirty CstBuPrgmPct instance");
	        try {
	            getHibernateTemplate().saveOrUpdate(instance);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	}

	@Override
	public void delete(CstBuPrgmPct persistentInstance) {
		   log.debug("deleting CstBuPrgmPct instance");
	        try {
	            getHibernateTemplate().delete(persistentInstance);
	            log.debug("delete successful");
	        } catch (RuntimeException re) {
	            log.error("delete failed", re);
	            throw re;
	        }
	}

	@Override
	public CstBuPrgmPct findById(Long id) {
		  log.debug("getting CstBuPrgmPct instance with id: " + id);
	        try {
	        	CstBuPrgmPct instance = (CstBuPrgmPct) getHibernateTemplate()
	                    .get(
	                            "com.averydennison.dmm.app.server.persistence.CstBuPrgmPct",
	                            id);
	            if (instance == null) {
	                log.debug("get successful, no instance found");
	            } else {
	                log.debug("get successful, instance found");
	            }
	            return instance;
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	}

	@Override
	public CstBuPrgmPct merge(CstBuPrgmPct detachedInstance) {
	    log.debug("merging CstBuPrgmPct instance");
        try {
        	CstBuPrgmPct result = (CstBuPrgmPct) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
	}

	@Override
	public void persist(CstBuPrgmPct transientInstance) {
		  log.debug("persisting CstBuPrgmPct instance");
	        try {
	            getHibernateTemplate().persist(transientInstance);
	            log.debug("persist successful");
	        } catch (RuntimeException re) {
	            log.error("persist failed", re);
	            throw re;
	        }

	}
	
	   public List<ValidBu> findValidBuByBuCode(String buCode){
	    	log.debug("getting ProgramBus for display id " + buCode);
	        try {
	        	Query q = getSession().getNamedQuery("findValidBuByBuCode");
	            q.setString(0, buCode);
	            if (q == null) {
	                log.debug("get successful, no instaces found");
	            } else {
	                log.debug("get successful, instaces found");
	            }
	            return (List<ValidBu>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	   }
	   
	   public List<Customer> findCustomerById(Long customerId){
	    	log.debug("getting ProgramBus for display id " + customerId);
	        try {
	        	Query q = getSession().getNamedQuery("findCustomerById");
	            q.setLong(0, customerId);
	            if (q == null) {
	                log.debug("get successful, no instaces found");
	            } else {
	                log.debug("get successful, instaces found");
	            }
	            return (List<Customer>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	   }
	   
	   
	   public List<CstBuPrgmPct> findCstBuPrgmPctByCustomerId(Long customerId){
	    	log.debug("getting findCstBuPrgmPctByCustomerId for display id " + customerId);
	        try {
	        	Query q = getSession().getNamedQuery("findCstBuPrgmPctByCustomerId");
	        	log.debug("findCstBuPrgmPctByCustomerId: query used: "+ q.getQueryString());
	        	log.debug("findCstBuPrgmPctByCustomerId: customerId: "+ customerId);
	            q.setLong(0, customerId);
	            if (q == null) {
	                log.debug("findCstBuPrgmPctByCustomerId: get successful, no instaces found");
	            } else {
	                log.debug("findCstBuPrgmPctByCustomerId: get successful, instaces found");
	            }
	            return (List<CstBuPrgmPct>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	   }
}
