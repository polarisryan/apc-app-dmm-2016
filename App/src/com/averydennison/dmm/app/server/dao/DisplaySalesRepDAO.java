package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.DisplaySalesRep;

import java.util.List;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:45:36 PM
 */
public interface DisplaySalesRepDAO {
    public void persist(DisplaySalesRep transientInstance);

    public void attachDirty(DisplaySalesRep instance);

    public void attachClean(DisplaySalesRep instance);

    public void delete(DisplaySalesRep persistentInstance);

    public DisplaySalesRep merge(DisplaySalesRep detachedInstance);

    public DisplaySalesRep findById(Long id);

    public List<DisplaySalesRep> getDisplaySalesReps();

	public List<DisplaySalesRep> getAllDisplaySalesReps();

	public List<DisplaySalesRep> getApprovers();
}
