package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.CustomerCountry;

public interface CustomerCountryDAO {

	    public CustomerCountry findById(Long id);

	    public List<CustomerCountry> getCustomerCountries();

		public CustomerCountry findByNameAndCountry(String customerName, Long customerCountryId);
	}

