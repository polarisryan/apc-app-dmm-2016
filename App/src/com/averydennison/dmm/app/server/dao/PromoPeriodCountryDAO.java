package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.PromoPeriodCountry;

public interface PromoPeriodCountryDAO {
	    public PromoPeriodCountry findById(Long id);
	    public List<PromoPeriodCountry> getPromoPeriodsCountry();
	}