package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.Status;

import java.util.List;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:50:02 PM
 */
public interface StatusDAO {
    public void persist(Status transientInstance);

    public void attachDirty(Status instance);

    public void attachClean(Status instance);

    public void delete(Status persistentInstance);

    public Status merge(Status detachedInstance);

    public Status findById(Long id);

    public List<Status> getStatuses();
}
