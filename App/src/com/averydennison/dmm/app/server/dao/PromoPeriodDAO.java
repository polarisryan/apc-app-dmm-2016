package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.PromoPeriod;

import java.util.List;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:49:19 PM
 */
public interface PromoPeriodDAO {
    public void persist(PromoPeriod transientInstance);

    public void attachDirty(PromoPeriod instance);

    public void attachClean(PromoPeriod instance);

    public void delete(PromoPeriod persistentInstance);

    public PromoPeriod merge(PromoPeriod detachedInstance);

    public PromoPeriod findById(Long id);

    List<PromoPeriod> getPromoPeriods();
}
