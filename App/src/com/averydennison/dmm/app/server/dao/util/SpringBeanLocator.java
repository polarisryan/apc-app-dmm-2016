package com.averydennison.dmm.app.server.dao.util;

/**
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 10:02:57 AM
 */

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.net.URL;

public class SpringBeanLocator {

    private static SpringBeanLocator INSTANCE = null;
    private static SpringBeanLocator JUNIT_INSTANCE = null;
    private static ApplicationContext context = null;
    private static ApplicationContext junitContext = null;

    private SpringBeanLocator(boolean isTestCase) {


        URL resourceURL = this.getClass().getResource("/applicationContext.xml");

        String fileName = resourceURL.toExternalForm();

        /*if(resourceURL != null){
            //EJB in EAR case
            fileName = resourceURL.toExternalForm();
        }else{
            //WAR case
            fileName = "classpath*:/applicationContext.xml";
        } */

        if (!isTestCase) {
            context = new ClassPathXmlApplicationContext(fileName);
        } else {
            junitContext = new ClassPathXmlApplicationContext("junit-ApplicationContext.xml");
        }
    }

    public static SpringBeanLocator getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SpringBeanLocator(false);
        }
        return INSTANCE;
    }

    public static SpringBeanLocator getJunitInstance() {
        if (JUNIT_INSTANCE == null) {
            JUNIT_INSTANCE = new SpringBeanLocator(true);
        }
        return JUNIT_INSTANCE;
    }

    private ApplicationContext getContext() {
        return context;
    }

    private ApplicationContext getJunitContext() {
        return junitContext;
    }


    public static ApplicationContext getApplicationContext() {
        return getInstance().getContext();
    }

    public static ApplicationContext getJunitApplicationContext() {
        return getJunitInstance().getJunitContext();
    }

    public static Object getBean(String beanName) {

        try {
            if (context != null) {
                return getInstance().getApplicationContext().getBean(beanName);
            } else {
                return getJunitInstance().getJunitContext().getBean(beanName);
            }
        } catch (Exception e) {
            return getJunitInstance().getJunitContext().getBean(beanName);
        }
    }
}
