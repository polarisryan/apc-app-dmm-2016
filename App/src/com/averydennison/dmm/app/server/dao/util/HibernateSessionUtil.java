package com.averydennison.dmm.app.server.dao.util;

/**
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 10:01:21 AM
 */

import org.hibernate.SessionFactory;


public class HibernateSessionUtil {


    public HibernateSessionUtil() {
    }

    public static SessionFactory getSessionFactory() {
        return (SessionFactory) SpringBeanLocator.getBean("ssf");
    }
//    public static void closeSession(Session session) {
//
//        if (session != null) {
//            if (session.isOpen())
//                session.close();
//        }
//    }
//
//    public static Session openNewSession(String adsUserName, String adsPassword) {
//
//        // Set the current ADS users credentials on the ThreadLocalCredentialSourceFactory
//        setUserCredentialsOnThreadLocal(adsUserName, adsPassword);
//
//        Session session = getSessionFactory().openSession();
//        // beginTransaction will call getConnection() in the ConfigurableConnectionProvider which will have the
//        // ADS user credentials provided above.
//        session.beginTransaction();
//        return session;
//    }
//
//    /**
//     * Used by DataSteam for Paging
//     * @return
//     */
//    public static void resetSession() {
//
//        Session session = HibernateSessionUtil.getSessionFromTransSyncManager();
//
//        HibernateSessionUtil.closeAndUnBindSession(session);
//        //HibernateSessionUtil.bindNewSessionUsingCurrentThreadCredentials();
//    }
//
//    public static void closeAndUnBindSession(Session session) {
//
//        Session hibSession = getSessionFromTransSyncManager();
//
//        if (hibSession != null) {
//            HibernateSessionUtil.closeSession(hibSession);
//            HibernateSessionUtil.unBindSession(session);
//        }
//
//    }
//
//    private static Session getSessionFromTransSyncManager() {
//        SessionHolder sessionHolder =
//                (SessionHolder) TransactionSynchronizationManager.getResource(getSessionFactory());
//        Session hibSession = sessionHolder.getSession();
//        return hibSession;
//    }
//
//    private static void unBindSession(Session session) {
//        if (TransactionSynchronizationManager.hasResource(session.getSessionFactory())) {
//            TransactionSynchronizationManager.unbindResourceIfPossible(session.getSessionFactory());
//        }
//    }
//
//    public static Session openAndBindNewSession(String adsUserName, String adsPassword) {
//
//        // Set the current ADS users credentials on the ThreadLocalCredentialSourceFactory
//        setUserCredentialsOnThreadLocal(adsUserName, adsPassword);
//
//        // get the session factory from spring. The datasource is not bound in spring anymore.
//        SessionFactory sf = HibernateSessionUtil.getSessionFactory();
//        Session session = sf.openSession();
//        // The Spring session wrapper used for the Spring HibernateTemplate
//        SessionHolder sessionHolder = new SessionHolder(session);
//        // beginTransaction will call getConnection() in the ConfigurableConnectionProvider which will have the
//        // ADS user credentials provided above.
//        Transaction txn = session.beginTransaction();
//        sessionHolder.setTransaction(txn);
//
//        if (!TransactionSynchronizationManager.hasResource(sf)) {
//            TransactionSynchronizationManager.bindResource(sf, sessionHolder);
//        }
//
//        return session;
//    }

//    private static void setUserCredentialsOnThreadLocal(String adsUserName, String adsPassword) {
//        UserCredentials adsUserCredentials = new UserCredentials(adsUserName, adsPassword);
//        IUserCredentialSourceFactory credentialsSourceFactory = new ThreadLocalCredentialSourceFactory();
//        IUserCredentialsSource credentialsSource = credentialsSourceFactory.createCredentialSource();
//        credentialsSource.configureUserCredentials(adsUserCredentials);
//    }
//
//    private static void bindNewSessionUsingCurrentThreadCredentials() {
//
//        // Set the current ADS users credentials on the ThreadLocalCredentialSourceFactory
//        IUserCredentialSourceFactory credentialsSourceFactory = new ThreadLocalCredentialSourceFactory();
//        IUserCredentialsSource credentialsSource = credentialsSourceFactory.createCredentialSource();
//        UserCredentials adsCurrentUserCredentials = credentialsSource.currentUserCredentials();
//        credentialsSource.configureUserCredentials(adsCurrentUserCredentials);
//
//        // get the session factory from spring. The datasource is not bound in spring anymore.
//        SessionFactory sf = HibernateSessionUtil.getSessionFactory();
//        Session session = sf.openSession();
//        // The Spring session wrapper used for the Spring HibernateTemplate
//        SessionHolder sessionHolder = new SessionHolder(session);
//        // beginTransaction will call getConnection() in the ConfigurableConnectionProvider which will have the
//        // ADS user credentials provided above.
//        Transaction txn = session.beginTransaction();
//        sessionHolder.setTransaction(txn);
//
//        if (!TransactionSynchronizationManager.hasResource(sf)) {
//            TransactionSynchronizationManager.bindResource(sf, sessionHolder);
//        }
//    }
}

