package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.ValidLocation;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 9:31:19 AM
 */
public interface ValidLocationDAO {
    public void persist(ValidLocation transientInstance);

    public void attachDirty(ValidLocation instance);

    public void attachClean(ValidLocation instance);

    public void delete(ValidLocation persistentInstance);

    public ValidLocation merge(ValidLocation detachedInstance);

    public ValidLocation findById(Long id);
    
    public List<ValidLocation> getLocations();

}
