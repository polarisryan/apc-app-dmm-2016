package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.DisplayPlanner;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:44:34 PM
 */
public interface DisplayPlannerDAO {
    public void persist(DisplayPlanner transientInstance);

    public void attachDirty(DisplayPlanner instance);

    public void attachClean(DisplayPlanner instance);

    public void delete(DisplayPlanner persistentInstance);

    public DisplayPlanner merge(DisplayPlanner detachedInstance);

    public DisplayPlanner findById(Long id);
    
    public List<DisplayPlanner> findByKitComponentId(Long id);
    
    public List<DisplayPlanner> findByDcId(Long id);
    
    public void deleteByKitComponentId(Long id);

    public void deleteByDisplayDcId(Long id);
}
