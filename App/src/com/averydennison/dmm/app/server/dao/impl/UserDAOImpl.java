package com.averydennison.dmm.app.server.dao.impl;

import com.averydennison.dmm.app.server.dao.UserDAO;
import com.averydennison.dmm.app.server.persistence.CustomerType;
import com.averydennison.dmm.app.server.persistence.User;
import com.averydennison.dmm.app.server.persistence.UserType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

public class UserDAOImpl extends HibernateDaoSupport implements UserDAO {

    private static final Log log = LogFactory.getLog(UserDAOImpl.class);

    public void persist(User transientInstance) {
        log.debug("persisting User instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(User instance) {
        log.debug("attaching dirty User instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(User instance) {
        log.debug("attaching clean User instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(User persistentInstance) {
        log.debug("deleting User instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public User merge(User detachedInstance) {
        log.debug("merging User instance");
        try {
            User result = (User) getHibernateTemplate().merge(
                    detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public User findById(Long id) {
        log.debug("getting User instance with id: " + id);
        try {
            User instance = (User) getHibernateTemplate().get(
                    "com.averydennison.dmm.app.server.persistence.User", id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public User findByName(String username) {
        log.debug("getting User instance with username: " + username);
        List l = null;
        try {
            l = getHibernateTemplate().find("from User u where u.username = '" + username + "'");
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return l.size() > 0 ? (User) l.get(0) : null;
    }

	public List<UserType> getAllUserTypes() {
		log.debug("getting All User Types");
        Session s = null;
        try {
            Query q = getSession().getNamedQuery("userTypes");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<UserType>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}
}
