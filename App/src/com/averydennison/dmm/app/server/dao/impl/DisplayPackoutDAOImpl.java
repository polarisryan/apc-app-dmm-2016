package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.client.models.SlimPackoutDTO;
import com.averydennison.dmm.app.server.dao.DisplayPackoutDAO;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayDc;
import com.averydennison.dmm.app.server.persistence.DisplayPackout;
import com.averydennison.dmm.app.server.persistence.DisplayShipWave;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.DisplayPackout
 */
public class DisplayPackoutDAOImpl extends HibernateDaoSupport implements DisplayPackoutDAO {

    private static final Log log = LogFactory.getLog(DisplayPackoutDAOImpl.class);

    public void persist(DisplayPackout transientInstance) {
        log.debug("persisting DisplayPackout instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(DisplayPackout instance) {
        log.debug("attaching dirty DisplayPackout instance");
        try {
            if (instance.getDisplayShipWaveId() == null) {
                System.out.println("display packout doesn't have shipwave id" + instance.toString());
                throw new RuntimeException("display packout doesn't have shipwave id");
            }
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(DisplayPackout instance) {
        log.debug("attaching clean DisplayPackout instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(DisplayPackout persistentInstance) {
        log.debug("deleting DisplayPackout instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public DisplayPackout merge(DisplayPackout detachedInstance) {
        log.debug("merging DisplayPackout instance");
        try {
            DisplayPackout result = (DisplayPackout) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public DisplayPackout findById(Long id) {
        log.debug("getting DisplayPackout instance with id: " + id);
        try {
            DisplayPackout instance = (DisplayPackout) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.DisplayPackout",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<DisplayPackout> findDisplayPackoutsByDisplayDc(DisplayDc displayDc) {
        if (displayDc == null) return null;
        log.debug("getting display packouts for display dc id " + displayDc.getDisplayDcId());
        try {
            Query q = getSession().getNamedQuery("findDisplayPackoutsByDisplayDc");
            q.setLong(0, displayDc.getDisplayDcId());
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            List<DisplayPackout> list = (List<DisplayPackout>) q.list();
            return list;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<DisplayPackout> findDisplayPackoutsByShipWave(DisplayShipWave displayShipWave) {
        if (displayShipWave == null) return null;
        log.debug("getting display packouts for display shipwave id ::>> " + displayShipWave.getDisplayShipWaveId());
        try {
            Query q = getSession().getNamedQuery("findDisplayPackoutsByShipWave");
            q.setLong(0, displayShipWave.getDisplayShipWaveId());
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            List<DisplayPackout> list = (List<DisplayPackout>) q.list();
            return list;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<SlimPackoutDTO> findDisplayPackoutsByDisplay(Display display) {
        if (display == null) return null;

        log.debug("getting display packouts for display id " + display.getDisplayId());
        try {
            SQLQuery q = (SQLQuery) getSession().getNamedQuery("findDisplayPackoutsByDisplay");
            q.setResultTransformer(Transformers.aliasToBean(SlimPackoutDTO.class));
            q.setLong(0, display.getDisplayId());
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instances found");
            }
            List<SlimPackoutDTO> list = (List<SlimPackoutDTO>) q.list();
            return list;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
   
}
