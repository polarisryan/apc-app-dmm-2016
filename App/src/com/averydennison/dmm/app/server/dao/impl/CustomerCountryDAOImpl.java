package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.averydennison.dmm.app.server.dao.CustomerCountryDAO;
import com.averydennison.dmm.app.server.persistence.CustomerCountry;

public class CustomerCountryDAOImpl  extends HibernateDaoSupport implements CustomerCountryDAO {

	
	  private static final Log log = LogFactory.getLog(CustomerCountryDAOImpl.class);
	@Override
	public CustomerCountry findById(Long id) {
		log.debug("getting Customer instance with id: " + id);
        try {
        	CustomerCountry instance = (CustomerCountry) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.CustomerCountry",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

	@Override
	public List<CustomerCountry> getCustomerCountries() {
		  log.debug("getting CustomerCountry List");
	        Session s = null;
	        try {
	            Query q = getSession().getNamedQuery("customerCountryDetail");
	            if (q == null) {
	                log.debug("get successful, no instances found");
	            } else {
	                log.debug("get successful, instances found");
	            }
	            return (List<CustomerCountry>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	}

	@Override
	public CustomerCountry findByNameAndCountry(String customerName, Long customerCountryId) {
		try {
            Query q = getSession().getNamedQuery("findCustomerByNameAndCountry")
            	.setParameter("name", customerName).setParameter("countryId", customerCountryId);
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            return (CustomerCountry)q.setMaxResults(1).uniqueResult();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

	

}
