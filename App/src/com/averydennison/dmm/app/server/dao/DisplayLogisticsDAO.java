package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.DisplayLogistics;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:39:45 PM
 */
public interface DisplayLogisticsDAO {
    public void persist(DisplayLogistics transientInstance);

    public void attachDirty(DisplayLogistics instance);

    public void attachClean(DisplayLogistics instance);

    public void delete(DisplayLogistics persistentInstance);

    public DisplayLogistics merge(DisplayLogistics detachedInstance);

    public DisplayLogistics findByDisplayId(Long id);

    public DisplayLogistics findById(Long id);

}
