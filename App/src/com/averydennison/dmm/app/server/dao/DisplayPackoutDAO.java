package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.client.models.SlimPackoutDTO;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayDc;
import com.averydennison.dmm.app.server.persistence.DisplayPackout;
import com.averydennison.dmm.app.server.persistence.DisplayShipWave;

import java.util.List;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:42:17 PM
 */
public interface DisplayPackoutDAO {
    public void persist(DisplayPackout transientInstance);

    public void attachDirty(DisplayPackout instance);

    public void attachClean(DisplayPackout instance);

    public void delete(DisplayPackout persistentInstance);

    public DisplayPackout merge(DisplayPackout detachedInstance);

    public DisplayPackout findById(Long id);

    public List<DisplayPackout> findDisplayPackoutsByDisplayDc(DisplayDc displayDc);

    public List<SlimPackoutDTO> findDisplayPackoutsByDisplay(Display display);
    
    public List<DisplayPackout> findDisplayPackoutsByShipWave(DisplayShipWave displayShipWave) ;
    

}
