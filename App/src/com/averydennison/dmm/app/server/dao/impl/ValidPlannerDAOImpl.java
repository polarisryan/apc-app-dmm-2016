package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import com.averydennison.dmm.app.server.dao.ValidPlannerDAO;
import com.averydennison.dmm.app.server.persistence.ValidPlanner;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 9:47:15 AM
 */
public class ValidPlannerDAOImpl extends HibernateDaoSupport implements ValidPlannerDAO {
    private static final Log log = LogFactory.getLog(ValidPlannerDAOImpl.class);

    public void persist(ValidPlanner transientInstance) {
        log.debug("persisting ValidPlanner instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(ValidPlanner instance) {
        log.debug("attaching dirty ValidPlanner instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(ValidPlanner instance) {
        log.debug("attaching clean ValidPlanner instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(ValidPlanner persistentInstance) {
        log.debug("deleting ValidPlanner instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public ValidPlanner merge(ValidPlanner detachedInstance) {
        log.debug("merging ValidPlanner instance");
        try {
        	ValidPlanner result = (ValidPlanner) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public ValidPlanner findById(Long id) {
        log.debug("getting ValidPlanner instance with id: " + id);
        try {
        	ValidPlanner instance = (ValidPlanner) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.ValidPlanner",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<ValidPlanner> getPlanners() {
        log.debug("getting Planners");
        try {
            Query q = getSession().getNamedQuery("planners");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<ValidPlanner>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
 
}
