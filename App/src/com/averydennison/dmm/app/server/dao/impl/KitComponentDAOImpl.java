package com.averydennison.dmm.app.server.dao.impl;



import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.averydennison.dmm.app.client.models.PriceExceptionDTO;
import com.averydennison.dmm.app.server.dao.KitComponentDAO;
import com.averydennison.dmm.app.server.persistence.CostAnalysisKitComponent;

import com.averydennison.dmm.app.server.persistence.KitComponent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.KitComponent
 */
public class KitComponentDAOImpl extends HibernateDaoSupport implements KitComponentDAO {

    private static final Log log = LogFactory.getLog(KitComponentDAOImpl.class);

    public void persist(KitComponent transientInstance) {
        //log.debug("persisting KitComponent instance");
        try {
            getHibernateTemplate().persist(transientInstance);
          //  log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist(KitComponent transientInstance) persist failed", re);
            throw re;
        }
    }

    public void attachDirty(KitComponent instance) {
        //log.debug("attaching dirty KitComponent instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
          //  log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attachDirty(KitComponent instance) attach failed", re);
            throw re;
        }
    }

    public void attachClean(KitComponent instance) {
        log.debug("attaching clean KitComponent instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(KitComponent persistentInstance) {
        log.debug("deleting KitComponent instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public KitComponent merge(KitComponent detachedInstance) {
        log.debug("merging KitComponent instance");
        try {
            KitComponent result = (KitComponent) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public KitComponent findById(Long id) {
        log.debug("getting KitComponent instance with id: " + id);
        try {
            KitComponent instance = (KitComponent) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.KitComponent",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<KitComponent> findByDisplayId(Long displayId) {
        if (displayId == null) return null;
        log.debug("getting KitComponents instance by display id: " + displayId);
        try {
            Query q = getSession().getNamedQuery("kitComponentsByDisplayId");
            q.setLong(0, displayId);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<KitComponent>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<KitComponent> findByDisplayScenarioId(Long displayScenarioId) {
        if (displayScenarioId == null) return null;
        log.debug("getting KitComponents instance by display id: " + displayScenarioId);
        try {
            Query q = getSession().getNamedQuery("kitComponentsByDisplayScenarioId");
            q.setLong(0, displayScenarioId);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<KitComponent>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<CostAnalysisKitComponent> findCostAnalysisKitComponentByDisplayId(Long displayId, Long displayScenarioId) {
    	//log.debug("get successful, no instances found");
    	log.debug("KitComponentDAOImpl:findCostAnalysisKitComponentByDisplayId get successful, no instances [displayId,displayScenarioId]=["
    			+displayScenarioId +","+ displayScenarioId +"]");
        if (displayId == null || displayScenarioId==null ) return null;
        try {
            Query q = getSession().getNamedQuery("costAnalysisKitComponents");
          // System.out.println("++++query used =" + q.getQueryString());
            q.setResultTransformer(Transformers.aliasToBean(CostAnalysisKitComponent.class));
            q.setLong(0, displayId);
            q.setLong(1, displayId);
            q.setLong(2, displayScenarioId);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<CostAnalysisKitComponent>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public void callILSPriceExceptionForKitComponent(Long kitComponentId, Date reCalcDate) {
    	 if( kitComponentId == null) return ;
        // System.out.println("####KitComponentDAOImpl:callILSPriceExceptionForKitComponent input params=" + kitComponentId+ " started at "+new java.sql.Date(reCalcDate.getTime()));
    	 Session s = this.getSession();//wrap with transactions here or on the method itself
         String sql = "call get_price_exception(?,?)";
         SQLQuery sqlQuery = s.createSQLQuery(sql);
         sqlQuery.setLong(0, kitComponentId);
         sqlQuery.setDate(1,   new java.sql.Date(reCalcDate.getTime()));
         sqlQuery.executeUpdate();
         s.close();
    	 return;
    }
    
	public void updateReCalcDate(Long displayId, Date reCalcDate) {
		if (displayId == null) return;

       try {
            SQLQuery q = (SQLQuery) getSession().getNamedQuery("updateReCalcDate");
            log.debug("KitComponentDAOImpl:updateReCalcDate  passed data (" +  new java.sql.Date(reCalcDate.getTime()) + "," + displayId+  ")");
            q.setDate(0,   new java.sql.Date(reCalcDate.getTime()));
            q.setLong(1, displayId);
            q.executeUpdate();
            
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}
}
