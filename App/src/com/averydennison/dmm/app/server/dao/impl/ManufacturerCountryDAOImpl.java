package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.averydennison.dmm.app.server.dao.ManufacturerCountryDAO;
import com.averydennison.dmm.app.server.persistence.ManufacturerCountry;

public class ManufacturerCountryDAOImpl extends HibernateDaoSupport implements
		ManufacturerCountryDAO {
	 private static final Log log = LogFactory.getLog(ManufacturerCountryDAOImpl.class);

	  public ManufacturerCountry findById(Long id) {
	        log.debug("getting Manufacturer instance with id: " + id);
	        try {
	            ManufacturerCountry instance = (ManufacturerCountry) getHibernateTemplate()
	                    .get(
	                            "com.averydennison.dmm.app.server.persistence.ManufacturerCountry",
	                            id);
	            if (instance == null) {
	                log.debug("get successful, no instance found");
	            } else {
	                log.debug("get successful, instance found");
	            }
	            return instance;
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }

	    public List<ManufacturerCountry> getManufacturersCountry() {
	        log.debug("getting all ManufacturerCountries");
	        try {
	            Query q = getSession().getNamedQuery("manufacturerCountryDetail");
	            if (q == null) {
	                log.debug("get successful, no instaces found");
	            } else {
	                log.debug("get successful, instaces found");
	            }
	            return (List<ManufacturerCountry>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }

	    public List<ManufacturerCountry> getAllManufacturersCountry() {
	        log.debug("getting all ManufacturerCountry, even inactive");
	        try {
	            Query q = getSession().getNamedQuery("manufacturerCountryAll");
	            if (q == null) {
	                log.debug("get successful, no instaces found");
	            } else {
	                log.debug("get successful, instaces found");
	            }
	            return (List<ManufacturerCountry>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }
	    

		@Override
		public ManufacturerCountry findByNameAndCountry(String name, Long countryId) {
			try {
	            Query q = getSession().getNamedQuery("findManufacturerByNameAndCountry")
	            	.setParameter("name", name).setParameter("countryId", countryId);
	            if (q == null) {
	                log.debug("get successful, no instaces found");
	            } else {
	                log.debug("get successful, instaces found");
	            }
	            return (ManufacturerCountry)q.setMaxResults(1).uniqueResult();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
		}

}
