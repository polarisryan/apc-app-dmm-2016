package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.CustomerPct;


public interface CustomerPctDAO {
	public void persist(CustomerPct transientInstance);

    public void attachDirty(CustomerPct instance);

    public void attachClean(CustomerPct instance);

    public void delete(CustomerPct persistentInstance);

    public CustomerPct merge(CustomerPct detachedInstance);

    public CustomerPct findById(Long id);
    
    public List<CustomerPct> findByCustomerId(Long customerId);
    
}
