package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.CustomerMarketing;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:36:08 PM
 */
public interface CustomerMarketingDAO {
    public void persist(CustomerMarketing transientInstance);

    public void attachDirty(CustomerMarketing instance);

    public void attachClean(CustomerMarketing instance);

    public void delete(CustomerMarketing persistentInstance);

    public CustomerMarketing merge(CustomerMarketing detachedInstance);

    public CustomerMarketing findById(Long id);

    public CustomerMarketing findByDisplayId(Long id);
}
