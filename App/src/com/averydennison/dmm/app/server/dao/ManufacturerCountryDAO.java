package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.ManufacturerCountry;

public interface ManufacturerCountryDAO {
	
	public ManufacturerCountry findById(Long id);

	public List<ManufacturerCountry> getManufacturersCountry();

	public List<ManufacturerCountry> getAllManufacturersCountry();

	public ManufacturerCountry findByNameAndCountry(String name, Long countryId);
}
