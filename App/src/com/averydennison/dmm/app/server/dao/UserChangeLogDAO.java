package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.UserChangeLog;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:53:28 PM
 */
public interface UserChangeLogDAO {
    public void persist(UserChangeLog transientInstance);

    public void attachDirty(UserChangeLog instance);

    public void attachClean(UserChangeLog instance);

    public void delete(UserChangeLog persistentInstance);

    public UserChangeLog merge(UserChangeLog detachedInstance);

    public UserChangeLog findById(Long id);
    
    public List<UserChangeLog> getCostAnalysisHistoryLog(Long displayId, Long displayScenarioId, String tableName);
    
}
