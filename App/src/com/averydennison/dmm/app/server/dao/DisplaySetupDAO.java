package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.DisplaySetup;

import java.util.List;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:50:02 PM
 */
public interface DisplaySetupDAO {
    public void persist(DisplaySetup transientInstance);

    public void attachDirty(DisplaySetup instance);

    public void attachClean(DisplaySetup instance);

    public void delete(DisplaySetup persistentInstance);

    public DisplaySetup merge(DisplaySetup detachedInstance);

    public DisplaySetup findById(Long id);

    public List<DisplaySetup> getDisplaySetups();
}
