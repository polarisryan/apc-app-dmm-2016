package com.averydennison.dmm.app.server.dao.impl;


import java.util.List;

import com.averydennison.dmm.app.client.models.BooleanDTO;
import com.averydennison.dmm.app.server.dao.UserChangeLogDAO;
import com.averydennison.dmm.app.server.persistence.DisplayCost;
import com.averydennison.dmm.app.server.persistence.UserChangeLog;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.UserChangeLog
 */
public class UserChangeLogDAOImpl extends HibernateDaoSupport implements UserChangeLogDAO {

    private static final Log log = LogFactory.getLog(UserChangeLogDAOImpl.class);

    public void persist(UserChangeLog transientInstance) {
        log.debug("persisting UserChangeLog instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(UserChangeLog instance) {
        log.debug("attaching dirty UserChangeLog instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(UserChangeLog instance) {
        log.debug("attaching clean UserChangeLog instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(UserChangeLog persistentInstance) {
        log.debug("deleting UserChangeLog instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public UserChangeLog merge(UserChangeLog detachedInstance) {
        log.debug("merging UserChangeLog instance");
        try {
            UserChangeLog result = (UserChangeLog) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public UserChangeLog findById(Long id) {
        log.debug("getting UserChangeLog instance with id: " + id);
        try {
            UserChangeLog instance = (UserChangeLog) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.UserChangeLog",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

	@Override
	public List<UserChangeLog> getCostAnalysisHistoryLog(Long displayId, Long displayScenarioId,  String tableName) {
		log.debug("getting UserChangeLog instance by display id: " + displayId);
        if (displayId == null || displayScenarioId==null) return null;
        try {
            Query q = getSession().getNamedQuery("changelogs");
            q.setResultTransformer(Transformers.aliasToBean(UserChangeLog.class));
            q.setLong(0, displayId);
            q.setLong(1, displayScenarioId);
            log.debug("get UserChangeLog query used displayId="+displayId + " tableName=" + tableName + " displayScenarioId="+displayScenarioId);
            //log.debug("get UserChangeLog query used "+ q.getQueryString());
            if (q == null) {
                log.debug("get UserChangeLog successful, no instances found");
            } else {
                log.debug("get UserChangeLog successful, instances found");
            }
            return (List<UserChangeLog>) q.list();
        } catch (RuntimeException re) {
            log.error("get UserChangeLog failed", re);
            throw re;
        }
	}
}
