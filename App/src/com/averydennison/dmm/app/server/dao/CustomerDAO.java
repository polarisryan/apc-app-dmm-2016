package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.Customer;
import com.averydennison.dmm.app.server.persistence.CustomerType;

import java.util.List;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:31:00 PM
 */
public interface CustomerDAO {
    public void persist(Customer transientInstance);

    public void attachDirty(Customer instance);

    public void attachClean(Customer instance);

    public void delete(Customer persistentInstance);

    public Customer merge(Customer detachedInstance);

    public Customer findById(Long id);

    public List<Customer> getCustomers();

	public List<Customer> getAllCustomers();

	public List<CustomerType> getAllCustomerTypes();
}
