package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.averydennison.dmm.app.server.dao.CorrugateDAO;
import com.averydennison.dmm.app.server.persistence.Corrugate;

public class CorrugateDAOImpl extends HibernateDaoSupport implements CorrugateDAO {
	 private static final Log log = LogFactory.getLog(CorrugateDAOImpl.class);
		@Override
		public void attachClean(Corrugate instance) {
			 log.debug("attaching clean Corrugate instance");
		        try {
		            getHibernateTemplate().lock(instance, LockMode.NONE);
		            log.debug("attach successful");
		        } catch (RuntimeException re) {
		            log.error("attach failed", re);
		            throw re;
		        }
		}
		@Override
		public void attachDirty(Corrugate instance) {
			  try {
		            getHibernateTemplate().saveOrUpdate(instance);
		          //  log.debug("attach successful");
		        } catch (RuntimeException re) {
		            log.error("attachDirty(CorrugateComponent instance) attach failed", re);
		            throw re;
		        }
			
		}
		@Override
		public void delete(Corrugate persistentInstance) {
			 log.debug("deleting Corrugate instance");
		        try {
		            getHibernateTemplate().delete(persistentInstance);
		            log.debug("delete successful");
		        } catch (RuntimeException re) {
		            log.error("delete failed", re);
		            throw re;
		        }
		}
		@Override
		public Corrugate findByDisplayId(Long displayId) {
			 if (displayId == null) return null;
		        log.debug("getting Corrugate instance by display id: " + displayId);
		        try {
		            Query q = getSession().getNamedQuery("getCorrugateByDisplayId");
		            q.setLong(0, displayId);
		            if (q == null) {
		                log.debug("get successful, no instances found");
		            } else {
		                log.debug("get successful, instances found");
		            }
		            if(q.list().size()>0){
		            	return (Corrugate) q.list().get(0);
		            }else{
		            	return null;
		            }
		        } catch (RuntimeException re) {
		            log.error("get failed", re);
		            throw re;
		        }
		}
		@Override
		public Corrugate findById(Long id) {
			  log.debug("getting CorrugateComponent instance with id: " + id);
		        try {
		        	Corrugate instance = (Corrugate) getHibernateTemplate()
		                    .get(
		                            "com.averydennison.dmm.app.server.persistence.Corrugate",
		                            id);
		            if (instance == null) {
		                log.debug("get successful, no instance found");
		            } else {
		                log.debug("get successful, instance found");
		            }
		            return instance;
		        } catch (RuntimeException re) {
		            log.error("get failed", re);
		            throw re;
		        }
		}
		@Override
		public Corrugate merge(Corrugate detachedInstance) {
			 log.debug("merging CorrugateComponent instance");
		        try {
		        	Corrugate result = (Corrugate) getHibernateTemplate().merge(detachedInstance);
		            log.debug("merge successful");
		            return result;
		        } catch (RuntimeException re) {
		            log.error("merge failed", re);
		            throw re;
		        }
		}
		@Override
		public void persist(Corrugate transientInstance) {
			  try {
		            getHibernateTemplate().persist(transientInstance);
		            log.debug("persist successful");
		        } catch (RuntimeException re) {
		            log.error("persist(CorrugateComponent transientInstance) persist failed", re);
		            throw re;
		        }
		}
}


