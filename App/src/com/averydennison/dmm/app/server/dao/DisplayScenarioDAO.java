package com.averydennison.dmm.app.server.dao;

import java.util.List;
import com.averydennison.dmm.app.server.persistence.DisplayScenario;
public interface DisplayScenarioDAO {
	public void persist(DisplayScenario transientInstance);
    public void attachDirty(DisplayScenario instance);
    public void attachClean(DisplayScenario instance);
    public void delete(DisplayScenario persistentInstance);
    public DisplayScenario merge(DisplayScenario detachedInstance);
    public DisplayScenario findById(Long id);
    public List<DisplayScenario> getDisplayScenarios(Long displayId);
    public List<DisplayScenario> getDisplayScenariosByScenarioNum(Long displayId, Long scenarioNum); 
	public List<DisplayScenario> findApprovedScenario(Long displayId);
		
	
}
