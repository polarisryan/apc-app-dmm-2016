package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.ScenarioStatus;

public interface ScenarioStatusDAO {
	public void persist(ScenarioStatus transientInstance);
    public void attachDirty(ScenarioStatus instance);
    public void attachClean(ScenarioStatus instance);
    public void delete(ScenarioStatus persistentInstance);
    public ScenarioStatus merge(ScenarioStatus detachedInstance);
    public ScenarioStatus findById(Long id);
    public List<ScenarioStatus> getScenarioStatuses();
}
