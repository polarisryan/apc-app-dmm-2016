package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.averydennison.dmm.app.server.dao.ManufacturerDAO;
import com.averydennison.dmm.app.server.persistence.Manufacturer;
/**
 * User: Spart Arguello
 * Date: Jan 13, 2010
 */
public class ManufacturerDAOImpl extends HibernateDaoSupport implements
        ManufacturerDAO {
    private static final Log log = LogFactory.getLog(ManufacturerDAOImpl.class);

    public void persist(Manufacturer transientInstance) {
        log.debug("persisting Manufacturer instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(Manufacturer instance) {
        log.debug("attaching dirty manufacturer instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(Manufacturer instance) {
        log.debug("attaching clean Manufacturer instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(Manufacturer persistentInstance) {
        log.debug("deleting Manufacturer instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public Manufacturer merge(Manufacturer detachedInstance) {
        log.debug("merging Manufacturer instance");
        try {
            Manufacturer result = (Manufacturer) getHibernateTemplate().merge(
                    detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public Manufacturer findById(Long id) {
        log.debug("getting Manufacturer instance with id: " + id);
        try {
            Manufacturer instance = (Manufacturer) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.Manufacturer",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<Manufacturer> getManufacturers() {
        log.debug("getting all Manufacturers");
        try {
            Query q = getSession().getNamedQuery("getAllManufacturers");
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            return (List<Manufacturer>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

}
