package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.CalculationSummary;


public interface CalculationSummaryDAO {
 	public void persist(CalculationSummary transientInstance);
    public void attachDirty(CalculationSummary instance);
    public void attachClean(CalculationSummary instance);
    public void delete(CalculationSummary instance);
    public CalculationSummary merge(CalculationSummary detachedInstance);
    public CalculationSummary loadByDisplayId(Long displayId);
    }
