package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import com.averydennison.dmm.app.server.dao.ValidBuDAO;
import com.averydennison.dmm.app.server.persistence.ValidBu;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 9:47:15 AM
 */
public class ValidBuDAOImpl extends HibernateDaoSupport implements ValidBuDAO {
    private static final Log log = LogFactory.getLog(ValidBuDAOImpl.class);

    public void persist(ValidBu transientInstance) {
        log.debug("persisting ValidBu instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(ValidBu instance) {
        log.debug("attaching dirty ValidBu instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(ValidBu instance) {
        log.debug("attaching clean ValidBu instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(ValidBu persistentInstance) {
        log.debug("deleting ValidBu instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public ValidBu merge(ValidBu detachedInstance) {
        log.debug("merging ValidBu instance");
        try {
        	ValidBu result = (ValidBu) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public ValidBu findById(Long id) {
        log.debug("getting ValidBu instance with id: " + id);
        try {
        	ValidBu instance = (ValidBu) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.ValidBu",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<ValidBu> getBus() {
        log.debug("getting Bus");
        try {
            Query q = getSession().getNamedQuery("validBus");
            log.debug("Query Used :" + q.getQueryString());
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<ValidBu>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
}
