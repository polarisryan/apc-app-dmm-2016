package com.averydennison.dmm.app.server.dao.util;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Utility class collects common functions.
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 9:46:00 AM
 */
public class Utility {
    public static final String LINK_TOKEN = "$_$";
    public static final String LINK_CHAR = "_";
    public static final String LINK_DOT = ".";
    public static final String LINK_LT_QUOTE = "(";
    public static final String LINK_RT_QUOTE = ")";
    public static final String SQL_DELIMITER = ",";
    public static final String SQL_DEFAULT_ALIA = "zz";
    public static final String SQL_SUM = "SUM";
    public static final String defaultDateFormat = "MM-dd-yyyy hh:mm";
    public static final String GEN_TOKEN = "() =+-*/><,%!~\t\r\n|#@&$";
    public static final String SPACE_TOKEN = " \t\r\n";


    /**
     * Converts a Date to String with the proper format.
     *
     * @param incomingDate  input timestamp
     * @param patternString date format pattern string
     * @return date string
     */
    public static String dateToStringFormat(Timestamp incomingDate,
                                            String patternString) {

        SimpleDateFormat dateformat;
        if (incomingDate == null) {
            return null;
        }

        // Create the supplied pattern string if one is supplied.
        if ((patternString != null) && !patternString.equals("")) {
            try {
                Field formatField = Utility.class.getField(patternString);
                String parseValue = (String) formatField.get(new Utility());

                dateformat = new SimpleDateFormat(parseValue);

            } catch (NoSuchFieldException nsfe) {
                dateformat = new SimpleDateFormat(patternString);
            } catch (IllegalAccessException iae) {
                dateformat = new SimpleDateFormat(patternString);
            }

        } else {
            dateformat = (SimpleDateFormat) SimpleDateFormat.getDateInstance(
                    SimpleDateFormat.DEFAULT);
        }

        return dateformat.format(incomingDate);
    }

    public static String dateToStringFormat(Date incomingDate,
                                            String patternString) {

        SimpleDateFormat dateformat;
        if (incomingDate == null) {
            return null;
        }

        // Create the supplied pattern string if one is supplied.
        if ((patternString != null) && !patternString.equals("")) {
            try {
                Field formatField = Utility.class.getField(patternString);
                String parseValue = (String) formatField.get(new Utility());

                dateformat = new SimpleDateFormat(parseValue);

            } catch (NoSuchFieldException nsfe) {
                dateformat = new SimpleDateFormat(patternString);
            } catch (IllegalAccessException iae) {
                dateformat = new SimpleDateFormat(patternString);
            }

        } else {
            dateformat = (SimpleDateFormat) SimpleDateFormat.getDateInstance(
                    SimpleDateFormat.DEFAULT);
        }

        return dateformat.format(incomingDate);
    }

    /**
     * Converts a Date to String with the proper format.
     *
     * @param incomingDate  input timestamp
     * @param patternString date format pattern string
     * @return date string
     * @throws ParseException Exception when error
     */
    public static Date stringDateToDate(String incomingDate, String patternString)
            throws ParseException {

        SimpleDateFormat dateformat;
        if (incomingDate == null) {
            return null;
        }

        // Create the supplied pattern string if one is supplied.
        if ((patternString != null) && !patternString.equals("")) {

            dateformat = new SimpleDateFormat(patternString);

        } else {
            dateformat = new SimpleDateFormat(defaultDateFormat);
        }

        return dateformat.parse(incomingDate);

    }

    /**
     * Utility method used to replace special characters with an escaped equivilant
     * character for inclusion into quoted strings in HTML.
     *
     * @param str input string
     * @return html format string
     */
    public static String formatForHTML(String str) {
        if (str == null) {
            return "";
        }

        // -- replace ' character with html equiv
        StringBuffer bufferedString = new StringBuffer();
        char[] string = str.toCharArray();
        for (int i = 0; i < string.length; i++) {
            if (string[i] == 39) { // -- ' single quote
                bufferedString.append("&#39;");
            } else if (string[i] == 34) { // -- " double quote
                bufferedString.append("&quot;");
            } else if (string[i] == 92) { // -- \ backslash
                bufferedString.append("\\");
            } else if (string[i] == 38) { // -- & ampersand
                bufferedString.append("&amp;");
            } else if (string[i] == 60) { // -- < left angle bracket
                bufferedString.append("&lt;");
            } else if (string[i] == 62) { // -- > right angle bracket
                bufferedString.append("&gt;");
            } else {
                bufferedString.append(string[i]);
            }
        }

        return bufferedString.toString();
    }

    public static String formatForNumber(String str) {
        if (str == null) {
            return "";
        }

        // -- replace ' character with html equiv
        StringBuffer bufferedString = new StringBuffer();
        char[] string = str.toCharArray();
        int startIndex = 0;
        if (string[0] == 46) {
            // handle format ".0555"
            bufferedString.append("0.");
            startIndex = 1;
        } else if (string[0] == 45 && string[1] == 46) {
            // handle format "-.55"
            bufferedString.append("-0.");
            startIndex = 2;
        }
        for (int i = startIndex; i < string.length; i++) {
            if ((string[i] >= 48 && string[i] <= 57) || string[i] == 43
                    || string[i] == 45 || string[i] == 46 || string[i] == 69 || string[i] == 101) {
                bufferedString.append(string[i]);
            }
        }

        return bufferedString.toString();
    }

    /**
     * Utility method used to replace special characters with an escaped equivilant
     * character for inclusion into quoted strings in Javascript.
     *
     * @param str input string
     * @return Javascript format string
     */
    public static String formatForJS(String str) {
        if (str == null) {
            return "";
        }

        // -- replace ' character with html equiv
        StringBuffer bufferedString = new StringBuffer();
        char[] string = str.toCharArray();
        for (int i = 0; i < string.length; i++) {
            if (string[i] == 39) { // -- ' single quote
                bufferedString.append("\\\'");
            } else if (string[i] == 34) { // -- " double quote
                bufferedString.append("\\\"");
            } else if (string[i] == 92) { // -- \ backslash
                bufferedString.append("\\\\");
            } else {
                bufferedString.append(string[i]);
            }
        }

        return bufferedString.toString();
    }

    /**
     * Returns the current date and time.
     *
     * @return current Timestamp
     */
    public static Timestamp getCurrentDateTime() {

        Calendar c = Calendar.getInstance();
        Timestamp datetime = new Timestamp(c.getTime().getTime());

        return datetime;
    }

    /**
     * Converts a String to Date with the proper format.
     *
     * @param str input date string
     * @param fmt input date string format
     * @return date object
     */
    public static java.util.Date stringToDate(String str, String fmt) {

        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat formatter = new SimpleDateFormat(fmt);
        java.util.Date d = formatter.parse(str, pos);

        return d;
    }

    /**
     * Method: replace all intermediat spaces to "_" for a given string.
     *
     * @param name
     * @return
     */
    public static String formateStringName(String name) {
        return name.trim().replaceAll(" ", LINK_CHAR);
    }

    /**
     * Method: replace all intermediat spaces to a given delimiter for a given string.
     *
     * @param name
     * @param delimiter
     * @return
     */
    public static String formateStringName(String name, String delimiter) {
        return name.trim().replaceAll(" ", delimiter);
    }

    /**
     * get last substring for a given string by token LINK_TOKEN
     *
     * @param name
     * @return
     */
    public static String getLastStringByLinkToken(String name) {
        int index = name.lastIndexOf(LINK_TOKEN);
        if (index < 0) {
            return null;
        }

        String result = name.substring(index + LINK_TOKEN.length());

        return result;
    }

    /**
     * Method: parse a given string by the token LINK_TOKEN
     *
     * @param name
     * @return
     */
    public static List<String> parseStringByLinkToken(String name) {
        String temp = Utility.replaceAll(name, LINK_TOKEN, "~");
        StringTokenizer st = new StringTokenizer(temp, "~");
        List<String> result = new ArrayList<String>();
        while (st.hasMoreTokens()) {
            result.add(st.nextToken());
        }

        return result;

    }

    /**
     * Method: parse a given string by the token LINK_TOKEN
     *
     * @param name
     * @return
     */
    public static List<String> parseStringByLinkToken(String name, String delimiter) {
        String temp = Utility.replaceAll(name, delimiter, "~");
        StringTokenizer st = new StringTokenizer(temp, "~");
        List<String> result = new ArrayList<String>();
        while (st.hasMoreTokens()) {
            result.add(st.nextToken());
        }

        return result;

    }

    /**
     * Replaces String from with String to in String s.
     *
     * @param s
     * @param from
     * @param to
     * @return String
     * @throws IllegalArgumentException Exception when error
     */

    public static String replaceAll(String s, String from, String to) {
        int fromLength = from.length();

        if (fromLength == 0) {
            throw new IllegalArgumentException(
                    "String to be replaced must not be empty.");
        }

        int start = s.indexOf(from);
        if (start == -1) {
            return s;
        }

        boolean greaterLength = (to.length() >= fromLength);

        StringBuffer buffer;

        // If the "to" parameter is longer than (or
        // as long as) "from", the final length will
        // be at least as large
        if (greaterLength) {
            if (from.equals(to)) {
                return s;
            }

            buffer = new StringBuffer(s.length());
        } else {
            buffer = new StringBuffer();
        }

        char[] origChars = s.toCharArray();

        int copyFrom = 0;
        while (start != -1) {
            buffer.append(origChars, copyFrom, start - copyFrom);
            buffer.append(to);
            copyFrom = start + fromLength;
            start = s.indexOf(from, copyFrom);
        }

        buffer.append(origChars, copyFrom, origChars.length - copyFrom);

        return buffer.toString();
    }

    /**
     * Method: DOCUMENT ME!
     *
     * @param source
     * @param delimiter
     * @return
     */
    public static <T> String formatCollectionToString(Collection<T> source,
                                                      String delimiter) {
        if ((source == null) || source.isEmpty()) {
            return "";
        }

        StringBuffer sb = new StringBuffer();

        for (T str : source) {
            if (sb.length() > 0) {
                sb.append(delimiter);
            }
            sb.append(str.toString());
        }

        return sb.toString();
    }

    @SuppressWarnings("unchecked")
    public static String composeSQLString(String[] elements, String aliaName, Map sumMap) {
        if (elements == null || elements.length == 0) {
            return null;
        }
        boolean noAlia = false;
        if (aliaName == null) {
            noAlia = true;
        }
        StringBuffer sb = new StringBuffer();
        boolean isSum = false;
        for (String element : elements) {
            if (element == null || element.trim().length() == 0) {
                continue;
            }
            isSum = false;
            if (sumMap != null && sumMap.containsKey(element)) {
                isSum = true;
            }
            if (sb.length() > 0) {
                sb.append(SQL_DELIMITER);
            }
            if (!isSum) {
                if (!noAlia) {
                    sb.append(aliaName);
                    sb.append(LINK_DOT);
                }
                sb.append(element);
            } else {
                sb.append(SQL_SUM);
                sb.append(LINK_LT_QUOTE);
                if (!noAlia) {
                    sb.append(aliaName);
                    sb.append(LINK_DOT);
                }
                sb.append(element);
                sb.append(LINK_RT_QUOTE);
                sb.append(" as ");
                sb.append(element);
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    public static Calendar convertCalendar(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal;
    }

    @SuppressWarnings("unchecked")
    public static <T, S> List<T> convertClassTypeForCollection(List<S> dimValues, final Class<T> tClass) {
        if (dimValues == null) {
            return new ArrayList<T>();
        }
        List<T> dims = (List<T>) CollectionUtils.collect(dimValues.iterator(),
                new Transformer() {
                    public Object transform(Object arg0) {
                        return tClass.cast(arg0);
                    }
                });
        return dims;
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T>[] splitListForPaging(List<T> inList, int maxSize) {
        int size = Double.valueOf(Math.floor(inList.size() + 0.00d) / maxSize + 1.00d).intValue();
        List[] result = new List[size];
        if (inList.size() <= maxSize) {
            result[0] = inList;
        } else {
            int rest = size * maxSize - inList.size();
            int start = 0;
            for (int idx = 0; idx < size; idx++) {
                int end = start + maxSize;
                if (end > inList.size()) {
                    end = inList.size();
                }
                result[idx] = inList.subList(start, end);
                start += maxSize;
            }
        }
        return (List<T>[]) result;
    }

    public static int[] getSplitStartIndexForPaging(int total, int maxSize) {
        int size = Double.valueOf(Math.floor(total + 0.00d) / maxSize + 1.00d).intValue();
        int[] result = new int[size];
        if (total <= maxSize) {
            result[0] = total;
        } else {
            int start = 0;
            for (int idx = 0; idx < size; idx++) {
                result[idx] = start;
                start += maxSize;
            }
        }
        return result;
    }

    public static String changeInitialCase(String inStr, boolean toLowerCase) {
        if (inStr == null || inStr.trim().length() == 0) {
            return "";
        }
        String initial = inStr.substring(0, 1);
        return (toLowerCase) ? (initial.toLowerCase() + inStr.substring(1)) : (initial.toUpperCase() + inStr.substring(1));
    }

    public static List<String> parseString(String workString, String tokenChars) {
        if (workString == null || workString.trim().length() == 0) {
            return null;
        }
        StringTokenizer st = new StringTokenizer(workString, tokenChars);
        List<String> tokenList = new ArrayList<String>();
        while (st.hasMoreElements()) {
            String token = st.nextToken();
            tokenList.add(token);
        }
        return tokenList;
    }

    /**
     * Checks to see if the source date is between the start date and end date.
     *
     * @param sourceDate: The date we want to check.
     * @param startDate:  The begining of the date range
     * @param endDate:    The end of the date range
     * @return - True: if in range.
     *         - False: if not in range.
     */
    public static boolean isDateInRange(Date sourceDate, Date startDate, Date endDate) {
        if (sourceDate == null || startDate == null || endDate == null) {
            throw new IllegalArgumentException("Source Date, Start Date and End Date are all required.");
        }

        if (startDate.getTime() <= sourceDate.getTime() && endDate.getTime() >= sourceDate.getTime()) {
            return true;
        }
        return false;
    }

    public static String trimParethese(String workStr, boolean trimBegin, boolean trimEnd, boolean isRecursive) {
        String result = trimHeadSpace(workStr.trim());
        if (trimBegin && (result.trim().startsWith("("))) {
            if (isRecursive) {
                return trimParethese(result.substring(result.indexOf("(") + 1), true, false, true);
            } else {
                result = result.substring(result.indexOf("(") + 1);
            }

        }
        if (trimEnd && (result.endsWith(")"))) {
            if (isRecursive) {
                return trimParethese(result.substring(0, result.lastIndexOf(")")), false, true, true);
            } else {
                result = result.substring(0, result.lastIndexOf(")"));
            }
        }

        return result;


    }

    public static String trimSingleQuot(String workStr) {
        String result = trimHeadSpace(workStr.trim());
        if ((result.trim().startsWith("\'"))) {
            result = result.substring(1);

        }
        if (result.endsWith("\'")) {
            result = result.substring(0, result.lastIndexOf("\'"));

        }

        return result;


    }

    public static String trimDoubleQuot(String workStr) {
        String result = trimHeadSpace(workStr.trim());
        if ((result.trim().startsWith("\""))) {
            result = result.substring(1);

        }
        if (result.endsWith("\"")) {
            result = result.substring(0, result.lastIndexOf("\""));

        }

        return result;


    }

    public static String trimHeadSpace(String inStr) {
        String workStr = inStr.trim();
        if (workStr != null && workStr.length() > 0) {
            if (workStr.startsWith(" ")) {
                return trimHeadSpace(workStr.substring(1));
            }
        }
        return workStr;
    }

    public static void logMapValue(Map inMap, Logger log) {
        for (Object key : inMap.keySet()) {
            Object val = inMap.get(key);
            log.info(key.toString() + "==>" + ((val == null) ? "null" : val.toString()));
        }
    }
    
   

}

