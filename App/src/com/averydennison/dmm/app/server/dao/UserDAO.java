package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.User;
import com.averydennison.dmm.app.server.persistence.UserType;

public interface UserDAO {

    public void persist(User transientInstance);

    public void attachDirty(User instance);

    public void attachClean(User instance);

    public void delete(User persistentInstance);

    public User merge(User detachedInstance);

    public User findById(Long id);

    public User findByName(String username);

	public List<UserType> getAllUserTypes();
}
