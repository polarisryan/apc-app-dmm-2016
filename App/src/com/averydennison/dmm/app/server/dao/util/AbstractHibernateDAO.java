package com.averydennison.dmm.app.server.dao.util;

import org.hibernate.HibernateException;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.engine.EntityKey;
import org.hibernate.engine.PersistenceContext;
import org.hibernate.event.EventSource;
import org.hibernate.impl.SessionImpl;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.util.IdentityMap;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;


/**
 * AbstractHibernateDao is a generic implementation of the BaseDao interface that uses
 * Hibernate for its persistence mechanism. While extending AbstractHibernateDao is not
 * required, doing so will simplify the implementation of Hibernate-based DAO classes.
 * <p/>
 * <p>This implementation uses Spring's HibernateDaoSupport. Methods in that class are
 * made available to subclasses and should be used to generate the Hibernate calls.</p>
 * <p/>
 * <p>Subclasses should not need to override any of the BaseDao methods under normal
 * circumstances (though they are welcome to if necessary). They should, however, provide
 * additional load and find methods specific to the domain class they are managing. See
 * BaseDao for more information.</p>
 * <p/>
 * <p>Implementation note: We use the term "load" to retrieve a single object from the
 * database or to return null if none exists. Hibernate uses "load" to retrieve a single
 * object but throw an exception if it doesn't exist. Hibernate uses "get" to mimic the
 * functionality we want for "load". Also note that Spring does not provide a method to
 * return a single object from a find-like query. The DaoUtils class can be used for that
 * functionality.</p>
 * <p/>
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 9:23:04 AM
 */
public abstract class AbstractHibernateDAO extends HibernateDaoSupport {


    public List findUsingCompositeKeyObject(String hql, String param, Object key) {
        return this.getHibernateTemplate().findByNamedParam(hql, param, key);
    }

    @SuppressWarnings("unchecked")
    public List findAllSecured(List<String> recipients) {
        return this.findAll();
    }


    private static final Logger log = Logger.getLogger(AbstractHibernateDAO.class.getName());

    /**
     * Returns the class that this DAO manages. This should be the class that is returned
     * by the loadById() method.
     *
     * @return the class.
     */
    @SuppressWarnings("unchecked")
    public abstract Class getDomainClass();

    /**
     * Return Count of All Objects of this type
     *
     * @return DOCUMENT ME!
     */
    @SuppressWarnings("unchecked")
    public Long countAll() {
        return this.countAll(this.getDomainClass());
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List findAll() {
        return getHibernateTemplate().loadAll(getDomainClass());
    }

    /**
     * {@inheritDoc}
     */
    public void flush() {
        getHibernateTemplate().flush();
    }


    /**
     * Flush the statements in the long session. Allows the global staffing strategy to
     * flush the sql statements after a pages
     */
    public void longSessionFlush() {
        getSession().flush();
    }

    /*
     * (non-Javadoc)
     * @see com.avega.dao.BaseDao#loadByIds(java.util.List)
     */
    @SuppressWarnings("unchecked")
    public List loadByIds(final List<Long> ids) {

        return this.getHibernateTemplate().executeFind(new HibernateCallback() {

            public Object doInHibernate(Session session)
                    throws HibernateException, SQLException {

                StringBuffer sb = new StringBuffer();

                sb.append(" from ");
                sb.append(getDomainClass().getName());
                sb.append(" cl where ");

                Map<String, List> resultParamMap = new HashMap<String, List>();

                String inStatement = null;
                String resultHql = null;

                inStatement = " cl.objectId IN  (:objectIds) ";
                resultHql = HibernateQueryUtil.splitBigInStatement(ids, inStatement, resultParamMap);
                sb.append(resultHql);

                Query query = session.createQuery(sb.toString());

                for (String param : resultParamMap.keySet()) {
                    query.setParameterList(param, resultParamMap.get(param));
                }

                return query.list();

            }

        });

    }

    protected List<Long> loadObjectIds(final String className) {

        return this.getHibernateTemplate().executeFind(new HibernateCallback() {

            public Object doInHibernate(Session session)
                    throws HibernateException, SQLException {

                StringBuffer sb = new StringBuffer();

                sb.append(" select objectId from ");
                sb.append(className);

                Query query = session.createQuery(sb.toString());

                return query.list();

            }

        });

    }

    protected Long loadMinObjectId(final String className) {
        return (Long) this.getHibernateTemplate().execute(new HibernateCallback() {

            public Object doInHibernate(Session session)
                    throws HibernateException, SQLException {

                StringBuffer sb = new StringBuffer();

                sb.append(" select min(objectId) from ");
                sb.append(className);

                Query query = session.createQuery(sb.toString());

                return query.uniqueResult();

            }

        });

    }


    /**
     * {@inheritDoc}
     */
    public void clearSessionAndDelete(Object obj) {
        getHibernateTemplate().clear();
        getHibernateTemplate().delete(obj);
    }

    /**
     * DOCUMENT ME!
     *
     * @param obj DOCUMENT ME!
     */
    public void delete(Object obj) {
        getHibernateTemplate().delete(obj);
    }

    /**
     * (non-Javadoc)
     *
     * @param tClass DOCUMENT ME!
     * @return DOCUMENT ME!
     *         =-
     */
    @SuppressWarnings({
            "unchecked",
            "cast"
    })
    public <T> List<T> findAll(final Class<T> tClass) {

        return (List<T>) this.getHibernateTemplate().executeFind(new HibernateCallback() {

            public Object doInHibernate(Session session)
                    throws HibernateException, SQLException {
                StringBuffer sb = new StringBuffer();

                sb.append(" from ");
                sb.append(tClass.getName());

                Query query = session.createQuery(sb.toString());

                return query.list();
            }
        });

    }

    /**
     * {@inheritDoc}
     */
    public Object loadById(Long id) {
        return (Object) getHibernateTemplate().get(getDomainClass(), id);
    }

    public Object loadByStringId(String id) {
        return (Object) getHibernateTemplate().get(getDomainClass(), id);
    }

    /**
     * {@inheritDoc}
     */
    public Object loadExistingById(Long id) {
        return (Object) getHibernateTemplate().load(getDomainClass(), id);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * <p>Implementation note: This method is intended to load all the lazy references
     * that are held by the Object that the DAO manages. There is apparently no
     * Hibernate call that will scan through an object and find and load all the lazy
     * references, though in theory such a method could be written using the Hibernate
     * metadata. Therefore, each DAO that manages a Object object with lazy
     * references must override this method and explicitly call
     * getHibernateTemplate().initialize(reference) on each lazy reference or Collection.
     * </p>
     */
    public void loadLazyReferences(Object Object) {
        this.getHibernateTemplate().refresh(Object);
    }

    @SuppressWarnings("unchecked")
    protected void loadLazyCollection(Collection collection) {
        this.getHibernateTemplate().refresh(collection);
    }

    /*
     * (non-Javadoc)
     *
     */
    public void store(Object obj) {
        getHibernateTemplate().saveOrUpdate(obj);
    }

    /**
     * DOCUMENT ME!
     *
     * @param objects DOCUMENT ME!
     */
    public void store(List<? extends Object> objects) {

        for (Object obj : objects) {
            store(obj);
        }
    }

    /**
     * Evicts the object from the Hibernate session
     *
     * @param obj The object to be evicted
     */
    public void evict(Object obj) {
        this.getSession().evict(obj);

    }

    /**
     * used for debug only. this will not filter by package name
     */
    public void dumpLongSession(boolean includeCollections) {

        dumpLongSession(null, includeCollections);
    }

    /**
     * used for debug only
     *
     * @param packageName DOCUMENT ME!
     */
    public void dumpLongSession(String packageName, boolean includeCollections) {

        log.info("##################################[Debug Long Session]#################################");

        Session longSession = this.getSession();
        SessionImpl test = (SessionImpl) longSession;
        PersistenceContext pc = test.getPersistenceContext();

        IdentityMap collectionMap = (IdentityMap) pc.getCollectionEntries();
        IdentityMap elementMap = (IdentityMap) pc.getEntityEntries();
        log.info("#######################################################################################");
        log.info("###################################[Element-start   ]##################################");
        log.info("#######################################################################################");
        String key = null;
        for (Object o : elementMap.entryList()) {
            IdentityMap.IdentityMapEntry ime = (IdentityMap.IdentityMapEntry) o;
            key = ime.getKey().toString();

            if (packageName != null) {
                if ((key != null) && key.startsWith(packageName)) {
                    log.info("key=" + key + " val=" + ime.getValue().toString());
                }
            } else {
                log.info("key=" + key + " val=" + ime.getValue().toString());
            }

        }

        log.info("####################################[Element-End     ]#######################################");
        if (includeCollections) {
            log.info("####################################[Collection-start]#######################################");
            for (Object o : collectionMap.entryList()) {
                IdentityMap.IdentityMapEntry ime = (IdentityMap.IdentityMapEntry) o;
                key = ime.getKey().toString();

                if (packageName != null) {
                    if ((key != null) && key.startsWith(packageName)) {
                        log.info("key=" + key + " val=" + ime.getValue().toString());
                    }

                } else {
                    log.info("key=" + key + " val=" + ime.getValue().toString());
                }

            }

            log.info("###################################[Collection-End]####################################");
            log.info("#######################################################################################");
            log.info("#######################################################################################");
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param entityname DOCUMENT ME!
     * @param Object     DOCUMENT ME!
     */
    public final void storeByEntity(String entityname, Object Object) {
        this.getSession().saveOrUpdate(entityname, Object);
    }

    /**
     * DOCUMENT ME!
     *
     * @param entityname DOCUMENT ME!
     * @param Object     DOCUMENT ME!
     */
    public final void deleteByEntity(String entityname, Object Object) {
        this.getSession().delete(entityname, Object);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Session getNewSession() {
        Session s;
        s = SessionFactoryUtils.getNewSession(getSessionFactory());

        return s;
    }


    /**
     * Return Count of All objects of a Particular Type
     *
     * @param tClass DOCUMENT ME!
     * @return DOCUMENT ME!
     */

    protected <T> Long countAll(final Class<T> tClass) {
        return (Long) this.getHibernateTemplate().execute(new HibernateCallback() {

            public Object doInHibernate(Session session)
                    throws HibernateException, SQLException {
                StringBuffer sb = new StringBuffer();

                sb.append(" select count(*) from ");
                sb.append(tClass.getName());
                sb.append(" cl ");

                Query query = session.createQuery(sb.toString());

                return query.uniqueResult();
            }
        });
    }

    @SuppressWarnings("unchecked")
    protected boolean isFilterable(Collection c) {
        if (c == null) {
            return false;
        }
        Set set = new HashSet(c);
        if ((set != null) && (set.size() > 0)) {
            return true;
        }

        return false;
    }


    /**
     * DOCUMENT ME!
     *
     * @param description DOCUMENT ME!
     * @param objectId    DOCUMENT ME!
     * @return DOCUMENT ME!
     */
    protected Long countDescriptionsSkippingUs(final String description, final Long objectId) {

        return (Long) this.getHibernateTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session)
                    throws HibernateException, SQLException {

                // Don't need thread locking so use StringBuilder
                StringBuilder sb = new StringBuilder();
                sb.append(" select count(*) ");
                sb.append(" from ");
                sb.append(getDomainClass().getName());
                sb.append(" target ");
                sb.append(" where target.description = :description ");

                if (objectId != null) {
                    sb.append(" and target.objectId != :objectId");
                }

                Query query = session.createQuery(sb.toString());
                query.setString("description", description);

                if (objectId != null) {
                    query.setLong("objectId", objectId);
                }

                return query.uniqueResult();
            }
        });
    }

    /**
     * DOCUMENT ME!
     *
     * @param entityName DOCUMENT ME!
     * @param obj        DOCUMENT ME!
     */
    protected void saveAndEvictExisting(String entityName, Object obj) {
        try {
            this.getSession().saveOrUpdate(entityName, obj);
            log.info("######" + entityName + "==>[save only]=>" + obj.toString());
        } catch (NonUniqueObjectException e) {
            EventSource source = (EventSource) this.getSession();
            final EntityPersister persister = source.getEntityPersister(entityName, obj);
            final PersistenceContext persistenceContext = source.getPersistenceContext();
            Object entity = persistenceContext.unproxyAndReassociate(obj);

            final Serializable id = persister.getIdentifier(entity, source.getEntityMode());
            if (id == null) {
            }

            EntityKey key = new EntityKey(id, persister, source.getEntityMode());
            Object existingOne = persistenceContext.getEntity(key);
            //log.warn("######" + entityName + "==>[" + id + "]==>[ObjectId]=>" + obj.getObjectId());
            if (existingOne != entity) {
                //log.warn("######" + entityName + "==>[" + id + "]==>[Evict existing]=>" + existingOne.toString());
                this.getSession().evict(existingOne);

                // sync the reference
                existingOne = obj;
            }

            this.getSession().saveOrUpdate(entityName, obj);
        }

    }

    @SuppressWarnings("unchecked")
    public <T> List<T> findAllSecured(Class<T> tClass, List<String> recipients) {
        return this.findAllSecured(recipients);
    }

    public <T> List<T> getList(T obj) {
        List<T> result = new ArrayList<T>();
        result.add(obj);
        return result;
    }

    /**
     * This is used to check necessary conditions before executing query.
     *
     * @param checkList
     * @return boolean
     */
    protected boolean isNullable(Collection... checkList) {
        for (Collection element : checkList) {
            if (element == null || !isFilterable(element)) {
                //log.warn("Passed list is null or empty.");
                return true;
            }
        }
        return false;
    }

    protected void nullOut(Object[] objList) {
        for (Object element : objList) {
            element = null;
        }
    }

    protected List findByJdbcQuery(final String selectQuery) {
        return this.getHibernateTemplate().executeFind(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                return session.createSQLQuery(selectQuery).list();
            }
        });
    }

    public List executeHQL(final String hql, final Map<String, Object> namedParameters) {
        return this.getHibernateTemplate().executeFind(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Query query = session.createQuery(hql);
                log.info("hql:" + hql);
                if (namedParameters != null) {
                    for (Iterator<String> it = namedParameters.keySet().iterator(); it.hasNext();) {
                        String key = it.next();
                        Object value = namedParameters.get(key);
                        log.info("setting key:" + key + " value:" + value);
                        if (value instanceof List) {
                            query.setParameterList(key, (List) value);
                        } else {
                            query.setParameter(key, value);
                        }
                    }
                }
                return query.list();
            }
        });
    }

    public List executeSQL(final String sql, final Map<String, Object> namedParameters) {
        return this.getHibernateTemplate().executeFind(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Query query = session.createSQLQuery(sql);
                log.info("sql:" + sql);
                if (namedParameters != null) {
                    for (Iterator<String> it = namedParameters.keySet().iterator(); it.hasNext();) {
                        String key = it.next();
                        Object value = namedParameters.get(key);
                        log.info("setting key:" + key + " value:" + value);
                        if (value instanceof List) {
                            query.setParameterList(key, (List) value);
                        } else {
                            query.setParameter(key, value);
                        }
                    }
                }
                return query.list();
            }
        });
    }

}
