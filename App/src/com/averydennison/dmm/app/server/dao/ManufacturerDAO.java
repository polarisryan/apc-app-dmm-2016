package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.Manufacturer;

public interface ManufacturerDAO {
	public void persist(Manufacturer transientInstance);

	public void attachDirty(Manufacturer instance);

	public void attachClean(Manufacturer instance);

	public void delete(Manufacturer persistentInstance);

	public Manufacturer merge(Manufacturer detachedInstance);

	public Manufacturer findById(Long id);

	public List<Manufacturer> getManufacturers();

}
