package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.PromoPeriodDAO;
import com.averydennison.dmm.app.server.persistence.PromoPeriod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.PromoPeriod
 */
public class PromoPeriodDAOImpl extends HibernateDaoSupport implements PromoPeriodDAO {

    private static final Log log = LogFactory.getLog(PromoPeriodDAOImpl.class);

    public void persist(PromoPeriod transientInstance) {
        log.debug("persisting PromoPeriod instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(PromoPeriod instance) {
        log.debug("attaching dirty PromoPeriod instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(PromoPeriod instance) {
        log.debug("attaching clean PromoPeriod instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(PromoPeriod persistentInstance) {
        log.debug("deleting PromoPeriod instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public PromoPeriod merge(PromoPeriod detachedInstance) {
        log.debug("merging PromoPeriod instance");
        try {
            PromoPeriod result = (PromoPeriod) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public PromoPeriod findById(Long id) {
        log.debug("getting PromoPeriod instance with id: " + id);
        try {
            PromoPeriod instance = (PromoPeriod) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.PromoPeriod",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<PromoPeriod> getPromoPeriods() {
        log.debug("getting Display Sales Reps");
        try {
            Query q = getSession().getNamedQuery("promoPeriods");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<PromoPeriod>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
