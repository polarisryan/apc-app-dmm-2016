package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.DisplayShipWaveDAO;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayShipWave;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.DisplayShipWave
 */
public class DisplayShipWaveDAOImpl extends HibernateDaoSupport implements DisplayShipWaveDAO {

    private static final Log log = LogFactory.getLog(DisplayShipWaveDAOImpl.class);

    public void persist(DisplayShipWave transientInstance) {
        log.debug("persisting DisplayShipWave instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(DisplayShipWave instance) {
        log.debug("attaching dirty DisplayShipWave instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(DisplayShipWave instance) {
        log.debug("attaching clean DisplayShipWave instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(DisplayShipWave persistentInstance) {
        log.debug("deleting DisplayShipWave instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public DisplayShipWave merge(DisplayShipWave detachedInstance) {
        log.debug("merging DisplayShipWave instance");
        try {
            DisplayShipWave result = (DisplayShipWave) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public DisplayShipWave findById(Long id) {
        log.debug("getting DisplayShipWave instance with id: " + id);
        try {
            DisplayShipWave instance = (DisplayShipWave) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.DisplayShipWave",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<DisplayShipWave> findDisplayShipWavesByDisplay(Display display) {
        if (display == null || display.getDisplayId() == null) return null;
        log.debug("getting display dcs for display id " + display.getDisplayId());
        try {
            Query q = getSession().getNamedQuery("findDisplayShipWavesByDisplay");
            q.setLong(0, display.getDisplayId());
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            return (List<DisplayShipWave>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    public List<DisplayShipWave> findDisplayShipWaveByDisplayId(Long displayId){
    	if (displayId == null) return null;
        log.debug("getting display dcs for display id " + displayId.toString());
        try {
            Query q = getSession().getNamedQuery("findDisplayShipWavesByDisplay");
            q.setLong(0, displayId);
            if (q == null) {
                log.debug("get successful, no instaces found");
            } else {
                log.debug("get successful, instaces found");
            }
            return (List<DisplayShipWave>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
