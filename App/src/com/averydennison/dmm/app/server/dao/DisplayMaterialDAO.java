package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.DisplayMaterial;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:41:02 PM
 */
public interface DisplayMaterialDAO {
    public void persist(DisplayMaterial transientInstance);

    public void attachDirty(DisplayMaterial instance);

    public void attachClean(DisplayMaterial instance);

    public void delete(DisplayMaterial persistentInstance);

    public DisplayMaterial merge(DisplayMaterial detachedInstance);

    public DisplayMaterial findById(Long id);
    
    public List<DisplayMaterial> findByDisplayId(Long displayId);
}
