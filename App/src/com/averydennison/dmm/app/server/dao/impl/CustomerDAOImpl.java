package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.CustomerDAO;
import com.averydennison.dmm.app.server.persistence.Customer;
import com.averydennison.dmm.app.server.persistence.CustomerType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.Customer
 */
public class CustomerDAOImpl extends HibernateDaoSupport implements CustomerDAO {

    private static final Log log = LogFactory.getLog(CustomerDAOImpl.class);

    public void persist(Customer transientInstance) {
        log.debug("persisting Customer instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(Customer instance) {
        log.debug("attaching dirty Customer instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(Customer instance) {
        log.debug("attaching clean Customer instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(Customer persistentInstance) {
        log.debug("deleting Customer instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public Customer merge(Customer detachedInstance) {
        log.debug("merging Customer instance");
        try {
            Customer result = (Customer) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public Customer findById(Long id) {
        log.debug("getting Customer instance with id: " + id);
        try {
            Customer instance = (Customer) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.Customer",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<Customer> getCustomers() {
        log.debug("getting Customers");
        Session s = null;
        try {
            Query q = getSession().getNamedQuery("customers");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<Customer>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

	@Override
	public List<Customer> getAllCustomers() {
		log.debug("getting All Customers");
        Session s = null;
        try {
            Query q = getSession().getNamedQuery("customersAll");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<Customer>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

	@Override
	public List<CustomerType> getAllCustomerTypes() {
		log.debug("getting All Customer Types");
        Session s = null;
        try {
            Query q = getSession().getNamedQuery("customerTypes");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<CustomerType>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}
}
