package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.UserAccess;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:52:29 PM
 */
public interface UserAccessDAO {
    public void persist(UserAccess transientInstance);

    public void attachDirty(UserAccess instance);

    public void attachClean(UserAccess instance);

    public void delete(UserAccess persistentInstance);

    public UserAccess merge(UserAccess detachedInstance);

    public UserAccess findById(Long id);
}
