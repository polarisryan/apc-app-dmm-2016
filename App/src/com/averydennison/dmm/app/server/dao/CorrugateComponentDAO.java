package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.CorrugateComponentsDetail;
import com.averydennison.dmm.app.server.persistence.CorrugateComponent;

public interface CorrugateComponentDAO {
	  	public void persist(CorrugateComponent transientInstance);
	    public void attachDirty(CorrugateComponent instance);
	    public void attachClean(CorrugateComponent instance);
	    public void delete(CorrugateComponent persistentInstance);
	    public CorrugateComponent merge(CorrugateComponent detachedInstance);
	    public CorrugateComponent findById(Long id);
	    public List<CorrugateComponent> findByDisplayId(Long displayId);
	    public List<CorrugateComponent> findByDisplayScenarioId(Long displayScenarioId);
	    public List<CorrugateComponentsDetail> findCorrugateComponentByDisplayId(Long displayId);
}
