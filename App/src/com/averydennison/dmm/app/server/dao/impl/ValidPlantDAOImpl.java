package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import com.averydennison.dmm.app.server.dao.ValidPlantDAO;
import com.averydennison.dmm.app.server.persistence.ValidPlant;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 9:47:15 AM
 */
public class ValidPlantDAOImpl extends HibernateDaoSupport implements ValidPlantDAO {
    private static final Log log = LogFactory.getLog(ValidPlantDAOImpl.class);

    public void persist(ValidPlant transientInstance) {
        log.debug("persisting ValidPlant instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(ValidPlant instance) {
        log.debug("attaching dirty ValidPlant instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(ValidPlant instance) {
        log.debug("attaching clean ValidPlant instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(ValidPlant persistentInstance) {
        log.debug("deleting ValidPlant instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public ValidPlant merge(ValidPlant detachedInstance) {
        log.debug("merging ValidPlant instance");
        try {
        	ValidPlant result = (ValidPlant) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public ValidPlant findById(Long id) {
        log.debug("getting ValidPlant instance with id: " + id);
        try {
        	ValidPlant instance = (ValidPlant) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.ValidPlant",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<ValidPlant> getPlants() {
        log.debug("getting Plants");
        try {
            Query q = getSession().getNamedQuery("plants");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<ValidPlant>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
}
