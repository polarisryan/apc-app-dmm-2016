package com.averydennison.dmm.app.server.dao.impl;

/**
 * @author Mari
 * @see com.averydennison.dmm.app.server.persistence.DisplayScenario
 */
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.averydennison.dmm.app.server.dao.DisplayScenarioDAO;
import com.averydennison.dmm.app.server.persistence.DisplayScenario;

public class DisplayScenarioDAOImpl extends HibernateDaoSupport implements DisplayScenarioDAO {

	 private static final Log log = LogFactory.getLog(DisplayScenarioDAOImpl.class);

	    public void persist(DisplayScenario transientInstance) {
	        log.debug("persisting DisplayScenario instance");
	        try {
	            getHibernateTemplate().persist(transientInstance);
	            log.debug("persist successful");
	        } catch (RuntimeException re) {
	            log.error("persist failed", re);
	            throw re;
	        }
	    }

	    public void attachDirty(DisplayScenario instance) {
	        log.debug("attaching dirty DisplayScenario instance");
	        try {
	            getHibernateTemplate().saveOrUpdate(instance);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	    }

	    public void attachClean(DisplayScenario instance) {
	        log.debug("attaching clean DisplayScenario instance");
	        try {
	            getHibernateTemplate().lock(instance, LockMode.NONE);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	    }

	    public void delete(DisplayScenario persistentInstance) {
	        log.debug("deleting DisplayScenario instance");
	        try {
	            getHibernateTemplate().delete(persistentInstance);
	            log.debug("delete successful");
	        } catch (RuntimeException re) {
	            log.error("delete failed", re);
	            throw re;
	        }
	    }

	    public DisplayScenario merge(DisplayScenario detachedInstance) {
	        log.debug("merging DisplayScenario instance");
	        try {
	        	DisplayScenario result = (DisplayScenario) getHibernateTemplate()
	                    .merge(detachedInstance);
	            log.debug("merge successful");
	            return result;
	        } catch (RuntimeException re) {
	            log.error("merge failed", re);
	            throw re;
	        }
	    }

	    public DisplayScenario findById(Long id) {
	        log.debug("getting Customer instance with id: " + id);
	        try {
	        	DisplayScenario instance = (DisplayScenario) getHibernateTemplate()
	                    .get(
	                            "com.averydennison.dmm.app.server.persistence.DisplayScenario",
	                            id);
	            if (instance == null) {
	                log.debug("get successful, no instance found");
	            } else {
	                log.debug("get successful, instance found");
	            }
	            return instance;
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }

	    public List<DisplayScenario> getDisplayScenarios(Long displayId) {
	        log.debug("getting Scenarios");
	        Session s = null;
	        try {
	            Query q = getSession().getNamedQuery("displayScenarios");
	            log.debug("getting query used="+ q.getQueryString() + " displayId="+displayId);
	            q.setLong(0, displayId);
	            if (q == null) {
	                log.debug("get successful, no instances found");
	            } else {
	                log.debug("get successful, instances found");
	            }
	            return (List<DisplayScenario>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }

	    public List<DisplayScenario> getDisplayScenariosByScenarioNum(Long displayId, Long scenarioNum) {
	        log.debug("getting Scenarios");
	        Session s = null;
	        try {
	            Query q = getSession().getNamedQuery("displayScenariosBYScenarioNum");
	            log.debug("getting query used="+ q.getQueryString() + " displayId="+displayId + " scenarioNum="+scenarioNum);
	            q.setLong(0, displayId);
	            q.setLong(1, scenarioNum);
	            if (q == null) {
	                log.debug("get successful, no instances found");
	            } else {
	                log.debug("get successful, instances found");
	            }
	            return (List<DisplayScenario>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }
	    
	    public List<DisplayScenario> findApprovedScenario(Long displayId) {
	        log.debug("getting Scenarios");
	        Session s = null;
	        try {
	            Query q = getSession().getNamedQuery("aprrovedScenarios");
	            log.debug("getting query used="+ q.getQueryString() + " displayId="+displayId);
	            q.setLong(0, displayId);
	            if (q == null) {
	                log.debug("get successful, no instances found");
	            } else {
	                log.debug("get successful, instances found");
	            }
	            return (List<DisplayScenario>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }
	}
