package com.averydennison.dmm.app.server.dao.impl;


import java.util.List;

import com.averydennison.dmm.app.server.dao.DisplayPlannerDAO;
import com.averydennison.dmm.app.server.persistence.DisplayLogistics;
import com.averydennison.dmm.app.server.persistence.DisplayPlanner;
import com.averydennison.dmm.app.server.persistence.KitComponent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.DisplayPlanner
 */
public class DisplayPlannerDAOImpl extends HibernateDaoSupport implements DisplayPlannerDAO {

    private static final Log log = LogFactory.getLog(DisplayPlannerDAOImpl.class);

    public void persist(DisplayPlanner transientInstance) {
        log.debug("persisting DisplayPlanner instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(DisplayPlanner instance) {
        log.debug("attaching dirty DisplayPlanner instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(DisplayPlanner instance) {
        log.debug("attaching clean DisplayPlanner instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(DisplayPlanner persistentInstance) {
        log.debug("deleting DisplayPlanner instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public DisplayPlanner merge(DisplayPlanner detachedInstance) {
        log.debug("merging DisplayPlanner instance");
        try {
            DisplayPlanner result = (DisplayPlanner) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public DisplayPlanner findById(Long id) {
        log.debug("getting DisplayPlanner instance with id: " + id);
        try {
            DisplayPlanner instance = (DisplayPlanner) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.DisplayPlanner",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<DisplayPlanner> findByKitComponentId(Long id) {
        if (id == null) return null;
        log.debug("getting DisplayPlanner instance by kit component id: " + id);
        try {
            Query q = getSession().getNamedQuery("displayPlannersByKitComponentId");
            q.setLong(0, id);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            if (q.list().size() == 0) return null;
            return (List<DisplayPlanner>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public void deleteByDisplayDcId(Long id) {
        if (id == null) return;
        log.debug("deleting DisplayPlanner instance by display dc id: " + id);
        try {
        	//getSession().beginTransaction();
            Query q = getSession().createQuery("delete from DisplayPlanner dp where dp.displayDcId=:id");
            q.setLong("id", id);
            q.executeUpdate();
            
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public void deleteByKitComponentId(Long id) {
        if (id == null) return;
        log.debug("getting DisplayPlanner instance by kit component id: " + id);
        try {
        	//getSession().beginTransaction();
            Query q = getSession().createQuery("delete from DisplayPlanner dp where dp.kitComponentId=:id");
            q.setLong("id", id);
            q.executeUpdate();
            //getSession().getTransaction().commit();
            
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

	@Override
	public List<DisplayPlanner> findByDcId(Long id) {
		 if (id == null) return null;
	        log.debug("getting DisplayPlanner instance by display dc id: " + id);
	        try {
	            Query q = getSession().getNamedQuery("displayPlannersByDisplayDcId");
	            q.setLong(0, id);
	            if (q == null) {
	                log.debug("get successful, no instances found");
	            } else {
	                log.debug("get successful, instances found");
	            }
	            if (q.list().size() == 0) return null;
	            return (List<DisplayPlanner>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	}


}
