package com.averydennison.dmm.app.server.dao.impl;
	import java.util.List;
	import com.averydennison.dmm.app.server.dao.DisplayCostImageDAO;
	import com.averydennison.dmm.app.server.persistence.DisplayCostImage;
	import org.apache.commons.logging.Log;
	import org.apache.commons.logging.LogFactory;
	import org.hibernate.LockMode;
	import org.hibernate.Query;
	import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
	/**
	 * @author Mari
	 * @see com.averydennison.dmm.app.server.persistence.DisplayCostImage
	 */
	public class DisplayCostImageDAOImpl extends HibernateDaoSupport implements DisplayCostImageDAO {
	    private static final Log log = LogFactory.getLog(DisplayCostImageDAOImpl.class);
	    public void persist(DisplayCostImage transientInstance) {
	        log.debug("persisting DisplayCostImage instance");
	        try {
	            getHibernateTemplate().persist(transientInstance);
	            log.debug("persist successful");
	        } catch (RuntimeException re) {
	            log.error("persist failed", re);
	            throw re;
	        }
	    }
	    public void attachDirty(DisplayCostImage instance) {
	        log.debug("attaching dirty DisplayCostImage instance");
	        try {
	            getHibernateTemplate().saveOrUpdate(instance);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	    }
	    public void attachClean(DisplayCostImage instance) {
	        log.debug("attaching clean DisplayCostImage instance");
	        try {
	            getHibernateTemplate().lock(instance, LockMode.NONE);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	    }
	    public void delete(DisplayCostImage persistentInstance) {
	        log.debug("deleting DisplayCostImage instance");
	        try {
	            getHibernateTemplate().delete(persistentInstance);
	            log.debug("delete successful");
	        } catch (RuntimeException re) {
	            log.error("delete failed", re);
	            throw re;
	        }
	    }
	    public DisplayCostImage merge(DisplayCostImage detachedInstance) {
	        log.debug("merging DisplayCostImage instance");
	        try {
	            DisplayCostImage result = (DisplayCostImage) getHibernateTemplate().merge(detachedInstance);
	            log.debug("merge successful");
	            return result;
	        } catch (RuntimeException re) {
	            log.error("merge failed", re);
	            throw re;
	        }
	    }
	    public DisplayCostImage findById(Long id) {
	        log.debug("getting DisplayCostImage instance with id: " + id);
	        try {
	            DisplayCostImage instance = (DisplayCostImage) getHibernateTemplate()
	                    .get(
	                            "com.averydennison.dmm.app.server.persistence.DisplayCostImage",
	                            id);
	            if (instance == null) {
	                log.debug("get successful, no instance found");
	            } else {
	                log.debug("get successful, instance found");
	            }
	            return instance;
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }
	    public List<DisplayCostImage> findByDisplayScenarioId(Long displayScenarioId) {
	    	log.debug("<<debug>>getting DisplayCostImageDAO.findByDisplayScenarioId instance by display id...1....: displayScenarioId="+displayScenarioId);
	        if (displayScenarioId==null ) return null;
	        log.debug("<<debug>>getting DisplayCostImage instance by display id...2....: ");
	        try {
	            Query q = getSession().getNamedQuery("displayCostImageByDisplayId");
	            q.setLong(0, displayScenarioId);
	            if (q == null) {
	                log.debug("<<debug>>get successful, no instances found");
	            } else {
	                log.debug("get successful, instances found");
	                log.debug("<<debug>>get successful, instances found, image size : "+ q.list().size());

	            }
	            return (List<DisplayCostImage>) q.list();
	        } catch (RuntimeException re) {
	            log.error("<<debug>>get failed", re);
	            throw re;
	        }
	    }
	   
}

	

