package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.CustomerMarketingDAO;
import com.averydennison.dmm.app.server.persistence.CustomerMarketing;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.CustomerMarketing
 */
public class CustomerMarketingDAOImpl extends HibernateDaoSupport implements CustomerMarketingDAO {

    private static final Log log = LogFactory
            .getLog(CustomerMarketingDAOImpl.class);

    public void persist(CustomerMarketing transientInstance) {
        log.debug("persisting CustomerMarketing instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(CustomerMarketing instance) {
        log.debug("attaching dirty CustomerMarketing instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(CustomerMarketing instance) {
        log.debug("attaching clean CustomerMarketing instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(CustomerMarketing persistentInstance) {
        log.debug("deleting CustomerMarketing instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public CustomerMarketing merge(CustomerMarketing detachedInstance) {
        log.debug("merging CustomerMarketing instance");
        try {
            CustomerMarketing result = (CustomerMarketing) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public CustomerMarketing findByDisplayId(Long id) {
        if (id == null) return null;
        log.debug("getting CustomerMarketing instance by display id: " + id);
        try {
            Query q = getSession().getNamedQuery("customerMarketingByDisplayId");
            q.setLong(0, id);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            if (q.list().size() == 0) return null;
            return (CustomerMarketing) q.list().get(0);
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public CustomerMarketing findById(Long id) {
        log.debug("getting CustomerMarketing instance with id: " + id);
        try {
            CustomerMarketing instance = (CustomerMarketing) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.CustomerMarketing",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
}
