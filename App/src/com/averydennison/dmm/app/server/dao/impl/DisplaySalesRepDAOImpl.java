package com.averydennison.dmm.app.server.dao.impl;


import com.averydennison.dmm.app.server.dao.DisplaySalesRepDAO;
import com.averydennison.dmm.app.server.persistence.DisplaySalesRep;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.DisplaySalesRep
 */
public class DisplaySalesRepDAOImpl extends HibernateDaoSupport implements DisplaySalesRepDAO {

    private static final Log log = LogFactory.getLog(DisplaySalesRepDAOImpl.class);

    public void persist(DisplaySalesRep transientInstance) {
        log.debug("persisting DisplaySalesRep instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(DisplaySalesRep instance) {
        log.debug("attaching dirty DisplaySalesRep instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(DisplaySalesRep instance) {
        log.debug("attaching clean DisplaySalesRep instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(DisplaySalesRep persistentInstance) {
        log.debug("deleting DisplaySalesRep instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public DisplaySalesRep merge(DisplaySalesRep detachedInstance) {
        log.debug("merging DisplaySalesRep instance");
        try {
            DisplaySalesRep result = (DisplaySalesRep) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public DisplaySalesRep findById(Long id) {
        log.debug("getting DisplaySalesRep instance with id: " + id);
        try {
            DisplaySalesRep instance = (DisplaySalesRep) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.DisplaySalesRep",
                            id);
            
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

    public List<DisplaySalesRep> getDisplaySalesReps() {
        log.debug("getting Display Sales Reps");
        try {
            Query q = getSession().getNamedQuery("displaySalesReps");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<DisplaySalesRep>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

	@Override
	public List<DisplaySalesRep> getAllDisplaySalesReps() {
        try {
            Query q = getSession().getNamedQuery("displaySalesRepsAll");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<DisplaySalesRep>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

	@Override
	public List<DisplaySalesRep> getApprovers() {
        try {
            Query q = getSession().getNamedQuery("approvers");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<DisplaySalesRep>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}
}
