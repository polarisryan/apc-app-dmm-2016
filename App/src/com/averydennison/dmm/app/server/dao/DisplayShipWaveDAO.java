package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayShipWave;

import java.util.List;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:46:39 PM
 */
public interface DisplayShipWaveDAO {
    public void persist(DisplayShipWave transientInstance);

    public void attachDirty(DisplayShipWave instance);

    public void attachClean(DisplayShipWave instance);

    public void delete(DisplayShipWave persistentInstance);

    public DisplayShipWave merge(DisplayShipWave detachedInstance);

    public DisplayShipWave findById(Long id);

    public List<DisplayShipWave> findDisplayShipWavesByDisplay(Display display);
    
    public List<DisplayShipWave> findDisplayShipWaveByDisplayId(Long displayId);
}
