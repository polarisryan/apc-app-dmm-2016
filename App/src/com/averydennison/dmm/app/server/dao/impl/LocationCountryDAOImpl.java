package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.averydennison.dmm.app.server.dao.LocationCountryDAO;
import com.averydennison.dmm.app.server.persistence.LocationCountry;

public class LocationCountryDAOImpl extends HibernateDaoSupport implements
		LocationCountryDAO {
	 private static final Log log = LogFactory.getLog(LocationCountryDAOImpl.class);
	@Override
	public LocationCountry findById(Long id) {
		log.debug("getting LocationCountry instance with id: " + id);
        try {
        	LocationCountry instance = (LocationCountry) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.LocationCountry",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

	@Override
	public List<LocationCountry> getAllActiveLocationsCountry() {
		log.debug("getting LocationCountry List");
        Session s = null;
        try {
            Query q = getSession().getNamedQuery("packoutLocationCountryDetail");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<LocationCountry>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

	@Override
	public List<LocationCountry> getAllLocationsCountry() {
        Session s = null;
        try {
            Query q = getSession().getNamedQuery("packoutLocationCountryAll");
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<LocationCountry>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

}
