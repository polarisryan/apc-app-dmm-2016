package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.averydennison.dmm.app.server.dao.CorrugateComponentDAO;
import com.averydennison.dmm.app.server.persistence.CorrugateComponent;
import com.averydennison.dmm.app.server.persistence.CorrugateComponentsDetail;

public class CorrugateComponentDAOImpl extends HibernateDaoSupport implements CorrugateComponentDAO {
	 private static final Log log = LogFactory.getLog(CorrugateComponentDAOImpl.class);
	@Override
	public void attachClean(CorrugateComponent instance) {
		 log.debug("attaching clean CorrugateComponent instance");
	        try {
	            getHibernateTemplate().lock(instance, LockMode.NONE);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	}

	@Override
	public void attachDirty(CorrugateComponent instance) {
		  try {
	            getHibernateTemplate().saveOrUpdate(instance);
	          //  log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attachDirty(CorrugateComponent instance) attach failed", re);
	            throw re;
	        }
		
	}

	@Override
	public void delete(CorrugateComponent persistentInstance) {
		 log.debug("deleting CorrugateComponent instance");
	        try {
	            getHibernateTemplate().delete(persistentInstance);
	            log.debug("delete successful");
	        } catch (RuntimeException re) {
	            log.error("CorrugateComponent instance delete failed", re);
	            throw re;
	        }
	}

	@Override
	public List<CorrugateComponent> findByDisplayId(Long displayId) {
		 if (displayId == null) return null;
	        log.debug("getting CorrugateComponent instance by display id: " + displayId);
	        try {
	            Query q = getSession().getNamedQuery("getAllCorrugates");
	            q.setLong(0, displayId);
	            if (q == null) {
	                log.debug("get successful, no instances found");
	            } else {
	                log.debug("get successful, instances found");
	            }
	            return (List<CorrugateComponent>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	}

	@Override
	public List<CorrugateComponent> findByDisplayScenarioId(
			Long displayScenarioId) {
		 if (displayScenarioId == null) return null;
	        log.debug("getting CorrugateComponents instance by display id: " + displayScenarioId);
	        try {
	            Query q = getSession().getNamedQuery("corrugateComponentsDetail");
	            q.setLong(0, displayScenarioId);
	            if (q == null) {
	                log.debug("get successful, no instances found");
	            } else {
	                log.debug("get successful, instances found");
	            }
	            return (List<CorrugateComponent>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	}

	@Override
	public CorrugateComponent findById(Long id) {
		  log.debug("getting CorrugateComponent instance with id: " + id);
		  if(id==null){
			  return null;
		  }
        try {
        	CorrugateComponent instance = (CorrugateComponent) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.CorrugateComponent",
                            id);
            if (instance == null) {
                log.debug("get successful, no instance found");
            } else {
                log.debug("get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

	@Override
	public List<CorrugateComponentsDetail> findCorrugateComponentByDisplayId(
			Long displayId) {
		//log.debug("get successful, no instances found");
    	//log.debug("get successful, no instances displayId="+displayId);
        if (displayId == null  ) return null;
        try {
            Query q = getSession().getNamedQuery("corrugateComponentsDetail");
          // System.out.println("++++query used =" + q.getQueryString());
            q.setResultTransformer(Transformers.aliasToBean(CorrugateComponentsDetail.class));
            q.setLong(0, displayId);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successful, instances found");
            }
            return (List<CorrugateComponentsDetail>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
	}

	@Override
	public CorrugateComponent merge(CorrugateComponent detachedInstance) {
		 log.debug("merging CorrugateComponent instance");
	        try {
	        	CorrugateComponent result = (CorrugateComponent) getHibernateTemplate().merge(detachedInstance);
	            log.debug("merge successful");
	            return result;
	        } catch (RuntimeException re) {
	            log.error("merge failed", re);
	            throw re;
	        }
	}

	@Override
	public void persist(CorrugateComponent transientInstance) {
		  try {
	            getHibernateTemplate().persist(transientInstance);
	            log.debug("persist successful");
	        } catch (RuntimeException re) {
	            log.error("persist(CorrugateComponent transientInstance) persist failed", re);
	            throw re;
	        }
	}
}
