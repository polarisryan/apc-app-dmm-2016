package com.averydennison.dmm.app.server.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import com.averydennison.dmm.app.server.dao.ScenarioStatusDAO;
import com.averydennison.dmm.app.server.persistence.ScenarioStatus;

public class ScenarioStatusDAOImpl extends HibernateDaoSupport implements
		ScenarioStatusDAO {

	 private static final Log log = LogFactory.getLog(ScenarioStatusDAOImpl.class);

	    public void persist(ScenarioStatus transientInstance) {
	        log.debug("persisting ScenarioStatus instance");
	        try {
	            getHibernateTemplate().persist(transientInstance);
	            log.debug("persist successful");
	        } catch (RuntimeException re) {
	            log.error("persist failed", re);
	            throw re;
	        }
	    }

	    public void attachDirty(ScenarioStatus instance) {
	        log.debug("attaching dirty ScenarioStatus instance");
	        try {
	            getHibernateTemplate().saveOrUpdate(instance);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	    }

	    public void attachClean(ScenarioStatus instance) {
	        log.debug("attaching clean Customer instance");
	        try {
	            getHibernateTemplate().lock(instance, LockMode.NONE);
	            log.debug("attach successful");
	        } catch (RuntimeException re) {
	            log.error("attach failed", re);
	            throw re;
	        }
	    }

	    public void delete(ScenarioStatus persistentInstance) {
	        log.debug("deleting Customer instance");
	        try {
	            getHibernateTemplate().delete(persistentInstance);
	            log.debug("delete successful");
	        } catch (RuntimeException re) {
	            log.error("delete failed", re);
	            throw re;
	        }
	    }

	    public ScenarioStatus merge(ScenarioStatus detachedInstance) {
	        log.debug("merging Customer instance");
	        try {
	        	ScenarioStatus result = (ScenarioStatus) getHibernateTemplate()
	                    .merge(detachedInstance);
	            log.debug("merge successful");
	            return result;
	        } catch (RuntimeException re) {
	            log.error("merge failed", re);
	            throw re;
	        }
	    }

	    public ScenarioStatus findById(Long id) {
	        log.debug("getting ScenarioStatus instance with id: " + id);
	        try {
	        	ScenarioStatus instance = (ScenarioStatus) getHibernateTemplate()
	                    .get(
	                            "com.averydennison.dmm.app.server.persistence.ScenarioStatus",
	                            id);
	            if (instance == null) {
	                log.debug("get successful, no instance found");
	            } else {
	                log.debug("get successful, instance found");
	            }
	            return instance;
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }

	    public List<ScenarioStatus> getScenarioStatuses() {
	        log.debug("getting Customers");
	        Session s = null;
	        try {
	            Query q = getSession().getNamedQuery("scenarioStatuses");
	            if (q == null) {
	                log.debug("get successful, no instances found");
	            } else {
	                log.debug("get successful, instances found");
	            }
	            return (List<ScenarioStatus>) q.list();
	        } catch (RuntimeException re) {
	            log.error("get failed", re);
	            throw re;
	        }
	    }
	}
