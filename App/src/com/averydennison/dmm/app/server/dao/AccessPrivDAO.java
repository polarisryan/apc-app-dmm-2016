package com.averydennison.dmm.app.server.dao;

import com.averydennison.dmm.app.server.persistence.AccessPriv;

/**
 * User: Spart Arguello
 * Date: Oct 28, 2009
 * Time: 2:19:21 PM
 */
public interface AccessPrivDAO {

    public void persist(AccessPriv transientInstance);

    public void attachDirty(AccessPriv instance);

    public void attachClean(AccessPriv instance);

    public void delete(AccessPriv persistentInstance);

    public AccessPriv merge(AccessPriv detachedInstance);

    public AccessPriv findById(Long id);
}
