package com.averydennison.dmm.app.server.dao.impl;


import java.util.List;

import com.averydennison.dmm.app.server.dao.DisplayCostDAO;
import com.averydennison.dmm.app.server.persistence.DisplayCost;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Spart Arguello
 * @see com.averydennison.dmm.app.server.persistence.DisplayCost
 */
public class DisplayCostDAOImpl extends HibernateDaoSupport implements DisplayCostDAO {

    private static final Log log = LogFactory.getLog(DisplayCostDAOImpl.class);

    public void persist(DisplayCost transientInstance) {
        log.debug("persisting DisplayCost instance");
        try {
            getHibernateTemplate().persist(transientInstance);
            log.debug("persist successful");
        } catch (RuntimeException re) {
            log.error("persist failed", re);
            throw re;
        }
    }

    public void attachDirty(DisplayCost instance) {
        log.debug("attaching dirty DisplayCost instance getDisplayCostId : " + instance.getDisplayCostId());
        log.debug("attaching dirty DisplayCost instance getDisplayId : " + instance.getDisplayId());
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void attachClean(DisplayCost instance) {
        log.debug("attaching clean DisplayCost instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

    public void delete(DisplayCost persistentInstance) {
        log.debug("deleting DisplayCost instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }

    public DisplayCost merge(DisplayCost detachedInstance) {
        log.debug("merging DisplayCost instance");
        try {
            DisplayCost result = (DisplayCost) getHibernateTemplate().merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public DisplayCost findById(Long id) {
        log.debug("getting DisplayCost instance with id: " + id);
        if (id == null) return null;
        try {
            DisplayCost instance = (DisplayCost) getHibernateTemplate()
                    .get(
                            "com.averydennison.dmm.app.server.persistence.DisplayCost",
                            id);
            if (instance == null) {
                log.debug("<<debug>>get successful, no instance found");
            } else {
                log.debug("<<debug>>get successful, instance found");
            }
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public List<DisplayCost> findByDisplayId(Long displayId) {
    	log.debug("getting DisplayCost instance by display id: " + displayId);
        if (displayId == null) return null;
        try {
            Query q = getSession().getNamedQuery("displayCostByDisplayId");
            System.out.println(" DisplayCostDAOImpl:findByDisplayId Query Used "+q.getQueryString());
            q.setLong(0, displayId);
            if (q == null) {
                log.debug("get successful, no instances found");
            } else {
                log.debug("get successfu;l, instances found");
            }
            return (List<DisplayCost>) q.list();
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public DisplayCost loadByDisplayId(Long displayId) {
        if (displayId == null) return null;
        log.debug("loading DisplayCost instance by display id: " + displayId);
        try {
            Query q = getSession().getNamedQuery("displayCostByDisplayId");
            q.setLong(0, displayId);
            if (q == null) {
                log.debug("loading DisplayCost get successful, no instances found");
            } else {
                log.debug("loading DisplayCost get successful, instances found");
            }
            if(q.list()!=null){
            	if(q.list().size()>0){
            		log.debug("loading DisplayCost get successful, instances returned");
            		log.debug("loading DisplayCost get successful, instances id " + ((DisplayCost) q.list().get(0)).getDisplayCostId());

            		return (DisplayCost) q.list().get(0);
            	}
            }
    		log.debug("loading DisplayCost null instance returned");
            return null;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    public DisplayCost loadByDisplayScenarioId(Long displayScenarioId) {
        if (displayScenarioId == null) return null;
        log.debug("loading DisplayCost instance by displayScenarioId id: " + displayScenarioId);
        try {
            Query q = getSession().getNamedQuery("displayCostByDisplayScenarioId");
            q.setLong(0, displayScenarioId);
            if (q == null) {
                log.debug("loading DisplayCost get successful, no instances found");
            } else {
                log.debug("loading DisplayCost get successful, instances found");
            }
            if(q.list()!=null){
            	if(q.list().size()>0){
            		log.debug("loading DisplayCost get successful, instances returned");
            		log.debug("loading DisplayCost get successful, instances id " + ((DisplayCost) q.list().get(0)).getDisplayCostId());

            		return (DisplayCost) q.list().get(0);
            	}
            }
    		log.debug("loading DisplayCost null instance returned");
            return null;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }

}
