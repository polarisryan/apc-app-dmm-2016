package com.averydennison.dmm.app.server.dao;

import java.util.List;

import com.averydennison.dmm.app.server.persistence.ProductPlanner;

/**
 * User: Spart Arguello
 * Date: Oct 29, 2009
 * Time: 9:28:53 AM
 */
public interface ProductPlannerDAO {
    public void persist(ProductPlanner transientInstance);

    public void attachDirty(ProductPlanner instance);

    public void attachClean(ProductPlanner instance);

    public void delete(ProductPlanner persistentInstance);

    public ProductPlanner merge(ProductPlanner detachedInstance);

    public ProductPlanner findById(Long id);
    
    public List<ProductPlanner> findBySkuId(Long skuId);

    public List<ProductPlanner> findBySkuIdDc(Long skuId, String dcCode);
}
