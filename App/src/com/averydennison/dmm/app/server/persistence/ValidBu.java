package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 11:54:07 AM
 */
public class ValidBu implements Serializable {
    private String buCode;

    public String getBuCode() {
        return buCode;
    }

    public void setBuCode(String buCode) {
        this.buCode = buCode;
    }

    private String buDesc;

    public String getBuDesc() {
        return buDesc;
    }

    public void setBuDesc(String buDesc) {
        this.buDesc = buDesc;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }

    private String productCategoryCode;

    public String getProductCategoryCode() {
        return productCategoryCode;
    }

    public void setProductCategoryCode(String productCategoryCode) {
        this.productCategoryCode = productCategoryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValidBu that = (ValidBu) o;

        if (activeFlg != null ? !activeFlg.equals(that.activeFlg) : that.activeFlg != null) return false;
        if (productCategoryCode != null ? !productCategoryCode.equals(that.productCategoryCode) : that.productCategoryCode != null) return false;
        if (buCode != null ? !buCode.equals(that.buCode) : that.buCode != null) return false;
        if (buDesc != null ? !buDesc.equals(that.buDesc) : that.buDesc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = buCode != null ? buCode.hashCode() : 0;
        result = 31 * result + (buDesc != null ? buDesc.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        result = 31 * result + (productCategoryCode != null ? productCategoryCode.hashCode() : 0);
        return result;
    }
}
