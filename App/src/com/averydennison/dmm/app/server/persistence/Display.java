package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:34 AM
 */
public class Display implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3241567615976936185L;
	private Long displayId;

    public Long getDisplayId() {
        return displayId;
    }

    public void setDisplayId(Long displayId) {
        this.displayId = displayId;
    }

    private String sku;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    private String cmProjectNumber;
    
    public String getCmProjectNumber() {
		return cmProjectNumber;
	}

	public void setCmProjectNumber(String cmProjectNumber) {
		this.cmProjectNumber = cmProjectNumber;
	}

	private String customerDisplayNumber;
    
    public String getCustomerDisplayNumber() {
        return customerDisplayNumber;
    }

    public void setCustomerDisplayNumber(String customerDisplayNumber) {
        this.customerDisplayNumber = customerDisplayNumber;
    }

    private Long customerId;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    private Long countryId;

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }
    
    private Long statusId;

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    private Long qtyForecast;

    public Long getQtyForecast() {
        return qtyForecast;
    }

    public void setQtyForecast(Long qtyForecast) {
        this.qtyForecast = qtyForecast;
    }

    private Long promoPeriodId;

    public Long getPromoPeriodId() {
        return promoPeriodId;
    }

    public void setPromoPeriodId(Long promoPeriodId) {
        this.promoPeriodId = promoPeriodId;
    }
    
    private Long promoYear;

	public Long getPromoYear() {
		return promoYear;
	}

	public void setPromoYear(Long promoYear) {
		this.promoYear = promoYear;
	}

	private Long displaySalesRepId;

    public Long getDisplaySalesRepId() {
        return displaySalesRepId;
    }

    public void setDisplaySalesRepId(Long displaySalesRepId) {
        this.displaySalesRepId = displaySalesRepId;
    }

    private String destination;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    private Long structureId;

    public Long getStructureId() {
        return structureId;
    }
    public void setStructureId(Long structureId) {
        this.structureId = structureId;
    }

    private String qsiDocNumber;

    public String getQsiDocNumber() {
        return qsiDocNumber;
    }

    public void setQsiDocNumber(String qsiDocNumber) {
        this.qsiDocNumber = qsiDocNumber;
    }

    private Boolean skuMixFinalFlg;

    public Boolean isSkuMixFinalFlg() {
        return skuMixFinalFlg;
    }

    public void setSkuMixFinalFlg(Boolean skuMixFinalFlg) {
        this.skuMixFinalFlg = skuMixFinalFlg;
    }

    private Long manufacturerId;

    public Long getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    private Long displaySetupId;

    public Long getDisplaySetupId() {
        return displaySetupId;
    }

    public void setDisplaySetupId(Long displaySetupId) {
        this.displaySetupId = displaySetupId;
    }

    private Date dtCreated;

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    private Date dtLastChanged;

    public Date getDtLastChanged() {
        return dtLastChanged;
    }

    public void setDtLastChanged(Date dtLastChanged) {
        this.dtLastChanged = dtLastChanged;
    }

    private String userCreated;

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    private String userLastChanged;

    public String getUserLastChanged() {
        return userLastChanged;
    }

    public void setUserLastChanged(String userLastChanged) {
        this.userLastChanged = userLastChanged;
    }

    private Date dtGoNoGo;

    public Date getDtGoNoGo() {
        return dtGoNoGo;
    }

    public void setDtGoNoGo(Date dtGoNoGo) {
        this.dtGoNoGo = dtGoNoGo;
    }

    private String rygStatusFlg;
    
    public String getRygStatusFlg() {
		return rygStatusFlg;
	}

	public void setRygStatusFlg(String rygStatusFlg) {
		this.rygStatusFlg = rygStatusFlg;
	}
	
	private Long packoutVendorId;

    public Long getPackoutVendorId() {
        return packoutVendorId;
    }

    public void setPackoutVendorId(Long packoutVendorId) {
        this.packoutVendorId = packoutVendorId;
    }
    
    private Long actualQty;
    public Long getActualQty() {
        return actualQty;
    }

    public void setActualQty(Long actualQty) {
        this.actualQty = actualQty;
    }
	
	

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((customerByCustomerId == null) ? 0 : customerByCustomerId
                        .hashCode());
        result = prime
                * result
                + ((customerDisplayNumber == null) ? 0 : customerDisplayNumber
                        .hashCode());
        result = prime * result
                + ((customerId == null) ? 0 : customerId.hashCode());
        
        result = prime * result
        + ((countryId == null) ? 0 : countryId.hashCode());
        
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result
                + ((destination == null) ? 0 : destination.hashCode());
        result = prime
                * result
                + ((displayDcsByDisplayId == null) ? 0 : displayDcsByDisplayId
                        .hashCode());
        result = prime * result
                + ((displayId == null) ? 0 : displayId.hashCode());
        result = prime
                * result
                + ((displaySalesRepByDisplaySalesRepId == null) ? 0
                        : displaySalesRepByDisplaySalesRepId.hashCode());
        result = prime
                * result
                + ((displaySalesRepId == null) ? 0 : displaySalesRepId
                        .hashCode());
        result = prime
                * result
                + ((displaySetupByDisplaySetupId == null) ? 0
                        : displaySetupByDisplaySetupId.hashCode());
        result = prime * result
                + ((displaySetupId == null) ? 0 : displaySetupId.hashCode());
        result = prime * result
                + ((dtCreated == null) ? 0 : dtCreated.hashCode());
        result = prime * result
                + ((dtLastChanged == null) ? 0 : dtLastChanged.hashCode());
        result = prime
                * result
                + ((manufacturerByManufacturerId == null) ? 0
                        : manufacturerByManufacturerId.hashCode());
        result = prime * result
                + ((manufacturerId == null) ? 0 : manufacturerId.hashCode());
        result = prime
                * result
                + ((promoPeriodByPromoPeriodId == null) ? 0
                        : promoPeriodByPromoPeriodId.hashCode());
        result = prime
        * result
        + ((countryByCountryId == null) ? 0
                : countryByCountryId.hashCode());
        result = prime * result
                + ((countryByCountryId == null) ? 0 : countryByCountryId.hashCode());
        result = prime * result
                + ((promoYear == null) ? 0 : promoYear.hashCode());
        result = prime * result
                + ((qsiDocNumber == null) ? 0 : qsiDocNumber.hashCode());
        result = prime * result
                + ((qtyForecast == null) ? 0 : qtyForecast.hashCode());
        result = prime * result + ((sku == null) ? 0 : sku.hashCode());
        result = prime * result
                + ((skuMixFinalFlg == null) ? 0 : skuMixFinalFlg.hashCode());
        result = prime
                * result
                + ((statusByStatusId == null) ? 0 : statusByStatusId.hashCode());
        result = prime * result
                + ((statusId == null) ? 0 : statusId.hashCode());
        result = prime
                * result
                + ((structureByStructureId == null) ? 0
                        : structureByStructureId.hashCode());
        result = prime * result
                + ((structureId == null) ? 0 : structureId.hashCode());
        result = prime * result
                + ((userCreated == null) ? 0 : userCreated.hashCode());
        result = prime * result
                + ((userLastChanged == null) ? 0 : userLastChanged.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Display other = (Display) obj;
        if (customerByCustomerId == null) {
            if (other.customerByCustomerId != null)
                return false;
        } else if (!customerByCustomerId.equals(other.customerByCustomerId))
            return false;
        if (customerDisplayNumber == null) {
            if (other.customerDisplayNumber != null)
                return false;
        } else if (!customerDisplayNumber.equals(other.customerDisplayNumber))
            return false;
        /*if (cmProjectNumber == null) {
            if (other.cmProjectNumber != null)
                return false;
        } else if (!cmProjectNumber.equals(other.cmProjectNumber))
            return false;*/
        if (customerId == null) {
            if (other.customerId != null)
                return false;
        } else if (!customerId.equals(other.customerId))
            return false;
        if (countryId == null) {
            if (other.countryId != null)
                return false;
        } else if (!countryId.equals(other.countryId))
            return false;
        
        if (countryByCountryId == null) {
            if (other.countryByCountryId != null)
                return false;
        } else if (!countryByCountryId
                .equals(other.countryByCountryId))
            return false;
        
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (destination == null) {
            if (other.destination != null)
                return false;
        } else if (!destination.equals(other.destination))
            return false;
        if (displayDcsByDisplayId == null) {
            if (other.displayDcsByDisplayId != null)
                return false;
        } else if (!displayDcsByDisplayId.equals(other.displayDcsByDisplayId))
            return false;
        if (displayId == null) {
            if (other.displayId != null)
                return false;
        } else if (!displayId.equals(other.displayId))
            return false;
        if (displaySalesRepByDisplaySalesRepId == null) {
            if (other.displaySalesRepByDisplaySalesRepId != null)
                return false;
        } else if (!displaySalesRepByDisplaySalesRepId
                .equals(other.displaySalesRepByDisplaySalesRepId))
            return false;
        if (displaySalesRepId == null) {
            if (other.displaySalesRepId != null)
                return false;
        } else if (!displaySalesRepId.equals(other.displaySalesRepId))
            return false;
        if (displaySetupByDisplaySetupId == null) {
            if (other.displaySetupByDisplaySetupId != null)
                return false;
        } else if (!displaySetupByDisplaySetupId
                .equals(other.displaySetupByDisplaySetupId))
            return false;
        if (displaySetupId == null) {
            if (other.displaySetupId != null)
                return false;
        } else if (!displaySetupId.equals(other.displaySetupId))
            return false;
        if (displayShipWavesByDisplayId == null) {
            if (other.displayShipWavesByDisplayId != null)
                return false;
        } else if (!displayShipWavesByDisplayId
                .equals(other.displayShipWavesByDisplayId))
            return false;
        if (dtCreated == null) {
            if (other.dtCreated != null)
                return false;
        } else if (!dtCreated.equals(other.dtCreated))
            return false;
        if (dtLastChanged == null) {
            if (other.dtLastChanged != null)
                return false;
        } else if (!dtLastChanged.equals(other.dtLastChanged))
            return false;
        if (kitComponentsByDisplayId == null) {
            if (other.kitComponentsByDisplayId != null)
                return false;
        } else if (!kitComponentsByDisplayId
                .equals(other.kitComponentsByDisplayId))
            return false;
        if (manufacturerByManufacturerId == null) {
            if (other.manufacturerByManufacturerId != null)
                return false;
        } else if (!manufacturerByManufacturerId
                .equals(other.manufacturerByManufacturerId))
            return false;
        if (manufacturerId == null) {
            if (other.manufacturerId != null)
                return false;
        } else if (!manufacturerId.equals(other.manufacturerId))
            return false;
        if (promoPeriodByPromoPeriodId == null) {
            if (other.promoPeriodByPromoPeriodId != null)
                return false;
        } else if (!promoPeriodByPromoPeriodId
                .equals(other.promoPeriodByPromoPeriodId))
            return false;
        if (promoPeriodId == null) {
            if (other.promoPeriodId != null)
                return false;
        } else if (!promoPeriodId.equals(other.promoPeriodId))
            return false;
        if (promoYear == null) {
            if (other.promoYear != null)
                return false;
        } else if (!promoYear.equals(other.promoYear))
            return false;
        if (qsiDocNumber == null) {
            if (other.qsiDocNumber != null)
                return false;
        } else if (!qsiDocNumber.equals(other.qsiDocNumber))
            return false;
        if (qtyForecast == null) {
            if (other.qtyForecast != null)
                return false;
        } else if (!qtyForecast.equals(other.qtyForecast))
            return false;
        if (sku == null) {
            if (other.sku != null)
                return false;
        } else if (!sku.equals(other.sku))
            return false;
        if (skuMixFinalFlg == null) {
            if (other.skuMixFinalFlg != null)
                return false;
        } else if (!skuMixFinalFlg.equals(other.skuMixFinalFlg))
            return false;
        if (statusByStatusId == null) {
            if (other.statusByStatusId != null)
                return false;
        } else if (!statusByStatusId.equals(other.statusByStatusId))
            return false;
        if (statusId == null) {
            if (other.statusId != null)
                return false;
        } else if (!statusId.equals(other.statusId))
            return false;
        if (structureByStructureId == null) {
            if (other.structureByStructureId != null)
                return false;
        } else if (!structureByStructureId.equals(other.structureByStructureId))
            return false;
        if (structureId == null) {
            if (other.structureId != null)
                return false;
        } else if (!structureId.equals(other.structureId))
            return false;
        if (userCreated == null) {
            if (other.userCreated != null)
                return false;
        } else if (!userCreated.equals(other.userCreated))
            return false;
        if (userLastChanged == null) {
            if (other.userLastChanged != null)
                return false;
        } else if (!userLastChanged.equals(other.userLastChanged))
            return false;
        return true;
    }

    private DisplaySalesRep displaySalesRepByDisplaySalesRepId;

    public DisplaySalesRep getDisplaySalesRepByDisplaySalesRepId() {
        return displaySalesRepByDisplaySalesRepId;
    }

    public void setDisplaySalesRepByDisplaySalesRepId(DisplaySalesRep displaySalesRepByDisplaySalesRepId) {
        this.displaySalesRepByDisplaySalesRepId = displaySalesRepByDisplaySalesRepId;
    }

    private Status statusByStatusId;

    public Status getStatusByStatusId() {
        return statusByStatusId;
    }

    public void setStatusByStatusId(Status statusByStatusId) {
        this.statusByStatusId = statusByStatusId;
    }

    private DisplaySetup displaySetupByDisplaySetupId;

    public DisplaySetup getDisplaySetupByDisplaySetupId() {
        return displaySetupByDisplaySetupId;
    }

    public void setDisplaySetupByDisplaySetupId(DisplaySetup displaySetupByDisplaySetupId) {
        this.displaySetupByDisplaySetupId = displaySetupByDisplaySetupId;
    }

    private Manufacturer manufacturerByManufacturerId;

    public Manufacturer getManufacturerByManufacturerId() {
        return manufacturerByManufacturerId;
    }

    public void setManufacturerByManufacturerId(Manufacturer manufacturerByManufacturerId) {
        this.manufacturerByManufacturerId = manufacturerByManufacturerId;
    }

    private Customer customerByCustomerId;

    public Customer getCustomerByCustomerId() {
        return customerByCustomerId;
    }

    public void setCustomerByCustomerId(Customer customerByCustomerId) {
        this.customerByCustomerId = customerByCustomerId;
    }

    private PromoPeriod promoPeriodByPromoPeriodId;

    public PromoPeriod getPromoPeriodByPromoPeriodId() {
        return promoPeriodByPromoPeriodId;
    }

    public void setPromoPeriodByPromoPeriodId(PromoPeriod promoPeriodByPromoPeriodId) {
        this.promoPeriodByPromoPeriodId = promoPeriodByPromoPeriodId;
    }

    private Country countryByCountryId;

    public Country getCountryByCountryId() {
        return countryByCountryId;
    }

    public void setCountryByCountryId(Country countryByCountryId) {
        this.countryByCountryId = countryByCountryId;
    }
    
    private Structure structureByStructureId;

    public Structure getStructureByStructureId() {
        return structureByStructureId;
    }

    public void setStructureByStructureId(Structure structureByStructureId) {
        this.structureByStructureId = structureByStructureId;
    }

    private Collection<DisplayDc> displayDcsByDisplayId;

    public Collection<DisplayDc> getDisplayDcsByDisplayId() {
        return displayDcsByDisplayId;
    }

    public void setDisplayDcsByDisplayId(Collection<DisplayDc> displayDcsByDisplayId) {
        this.displayDcsByDisplayId = displayDcsByDisplayId;
    }

    private Collection<DisplayMaterial> displayMaterialsByDisplayId;

    public Collection<DisplayMaterial> getDisplayMaterialsByDisplayId() {
        return displayMaterialsByDisplayId;
    }

    public void setDisplayMaterialsByDisplayId(Collection<DisplayMaterial> displayMaterialsByDisplayId) {
        this.displayMaterialsByDisplayId = displayMaterialsByDisplayId;
    }

    private Collection<DisplayShipWave> displayShipWavesByDisplayId;

    public Collection<DisplayShipWave> getDisplayShipWavesByDisplayId() {
        return displayShipWavesByDisplayId;
    }

    public void setDisplayShipWavesByDisplayId(Collection<DisplayShipWave> displayShipWavesByDisplayId) {
        this.displayShipWavesByDisplayId = displayShipWavesByDisplayId;
    }

    private Collection<KitComponent> kitComponentsByDisplayId;

    public Collection<KitComponent> getKitComponentsByDisplayId() {
        return kitComponentsByDisplayId;
    }

    public void setKitComponentsByDisplayId(Collection<KitComponent> kitComponentsByDisplayId) {
        this.kitComponentsByDisplayId = kitComponentsByDisplayId;
    }

   private Set<DisplaySalesRep> approvers;

    public Set<DisplaySalesRep> getApprovers() {
        return approvers;
    }

    public void setApprovers(Set<DisplaySalesRep> approvers) {
        this.approvers = approvers;
    }
    
    @Override
    public String toString() {
        return "Display{" +
                "displayId=" + displayId +
                ", sku='" + sku + '\'' +
                ", description='" + description + '\'' +
                ", customerId=" + customerId +
                ", statusId=" + statusId +
                ", countryId=" + countryId +
                ", qtyForecast=" + qtyForecast +
                ", promoPeriodId=" + promoPeriodId +
                ", displaySalesRepId=" + displaySalesRepId +
                ", destination='" + destination + '\'' +
                ", structureId=" + structureId +
                ", qsiDocNumber='" + qsiDocNumber + '\'' +
                ", skuMixFinalFlg=" + skuMixFinalFlg +
                ", manufacturerId=" + manufacturerId +
                ", dtCreated=" + dtCreated +
                ", dtLastChanged=" + dtLastChanged +
                ", userCreated='" + userCreated + '\'' +
                ", userLastChanged='" + userLastChanged + '\'' +
                '}';
    }
}
