package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

public class ProgramBu implements Serializable {
    
	private static final long serialVersionUID = 2825450309015197662L;
	
	private Long programBuId;
	private Double programPct;
	private ValidBu bu;
	private Long displayId;
	
	private Date dtCreated;
	private Date dtLastChanged;
	private String userCreated;
	private String userLastChanged;
	
	public Long getProgramBuId() {
		return programBuId;
	}
	public void setProgramBuId(Long programBuId) {
		this.programBuId = programBuId;
	}
	public Double getProgramPct() {
		return programPct;
	}
	public void setProgramPct(Double programPct) {
		this.programPct = programPct;
	}
	public ValidBu getBu() {
		return bu;
	}
	public void setBu(ValidBu bu) {
		this.bu = bu;
	}
	public Date getDtCreated() {
		return dtCreated;
	}
	public void setDtCreated(Date dtCreated) {
		this.dtCreated = dtCreated;
	}
	public Date getDtLastChanged() {
		return dtLastChanged;
	}
	public void setDtLastChanged(Date dtLastChanged) {
		this.dtLastChanged = dtLastChanged;
	}
	public String getUserCreated() {
		return userCreated;
	}
	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}
	public String getUserLastChanged() {
		return userLastChanged;
	}
	public void setUserLastChanged(String userLastChanged) {
		this.userLastChanged = userLastChanged;
	}
	public void setDisplayId(Long displayId) {
		this.displayId = displayId;
	}
	public Long getDisplayId() {
		return displayId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((displayId == null) ? 0 : displayId.hashCode());
		result = prime * result
				+ ((dtCreated == null) ? 0 : dtCreated.hashCode());
		result = prime * result
				+ ((dtLastChanged == null) ? 0 : dtLastChanged.hashCode());
		result = prime * result
				+ ((programBuId == null) ? 0 : programBuId.hashCode());
		result = prime * result
				+ ((programPct == null) ? 0 : programPct.hashCode());
		result = prime * result
				+ ((userCreated == null) ? 0 : userCreated.hashCode());
		result = prime * result
				+ ((userLastChanged == null) ? 0 : userLastChanged.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProgramBu other = (ProgramBu) obj;
		if (displayId == null) {
			if (other.displayId != null)
				return false;
		} else if (!displayId.equals(other.displayId))
			return false;
		if (dtCreated == null) {
			if (other.dtCreated != null)
				return false;
		} else if (!dtCreated.equals(other.dtCreated))
			return false;
		if (dtLastChanged == null) {
			if (other.dtLastChanged != null)
				return false;
		} else if (!dtLastChanged.equals(other.dtLastChanged))
			return false;
		if (programBuId == null) {
			if (other.programBuId != null)
				return false;
		} else if (!programBuId.equals(other.programBuId))
			return false;
		if (programPct == null) {
			if (other.programPct != null)
				return false;
		} else if (!programPct.equals(other.programPct))
			return false;
		if (userCreated == null) {
			if (other.userCreated != null)
				return false;
		} else if (!userCreated.equals(other.userCreated))
			return false;
		if (userLastChanged == null) {
			if (other.userLastChanged != null)
				return false;
		} else if (!userLastChanged.equals(other.userLastChanged))
			return false;
		return true;
	}
	

}
