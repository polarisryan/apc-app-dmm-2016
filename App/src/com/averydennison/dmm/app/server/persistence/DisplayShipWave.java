package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Nov 30, 2009
 * Time: 5:14:48 PM
 */
public class DisplayShipWave implements Serializable {
    private Long displayShipWaveId;

    public Long getDisplayShipWaveId() {
        return displayShipWaveId;
    }

    public void setDisplayShipWaveId(Long displayShipWaveId) {
        this.displayShipWaveId = displayShipWaveId;
    }

    private Long displayId;

    public Long getDisplayId() {
        return displayId;
    }

    public void setDisplayId(Long displayId) {
        this.displayId = displayId;
    }

    private Date mustArriveDate;

    public Date getMustArriveDate() {
        return mustArriveDate;
    }

    public void setMustArriveDate(Date mustArriveDate) {
        this.mustArriveDate = mustArriveDate;
    }

    private Long quantity;

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    private Integer shipWaveNum;

    public Integer getShipWaveNum() {
        return shipWaveNum;
    }

    public void setShipWaveNum(Integer shipWaveNum) {
        this.shipWaveNum = shipWaveNum;
    }

    private Boolean destinationFlg;

    public Boolean isDestinationFlg() {
        return destinationFlg;
    }

    public void setDestinationFlg(Boolean destinationFlg) {
        this.destinationFlg = destinationFlg;
    }

    private Date dtCreated;

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    private Date dtLastChanged;

    public Date getDtLastChanged() {
        return dtLastChanged;
    }

    public void setDtLastChanged(Date dtLastChanged) {
        this.dtLastChanged = dtLastChanged;
    }

    private String userCreated;

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    private String userLastChanged;

    public String getUserLastChanged() {
        return userLastChanged;
    }

    public void setUserLastChanged(String userLastChanged) {
        this.userLastChanged = userLastChanged;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisplayShipWave that = (DisplayShipWave) o;

        if (displayId != null ? !displayId.equals(that.displayId) : that.displayId != null) return false;
        if (displayShipWaveId != null ? !displayShipWaveId.equals(that.displayShipWaveId) : that.displayShipWaveId != null)
            return false;
        if (dtCreated != null ? !dtCreated.equals(that.dtCreated) : that.dtCreated != null) return false;
        if (dtLastChanged != null ? !dtLastChanged.equals(that.dtLastChanged) : that.dtLastChanged != null)
            return false;
        if (mustArriveDate != null ? !mustArriveDate.equals(that.mustArriveDate) : that.mustArriveDate != null)
            return false;
        if (quantity != null ? !quantity.equals(that.quantity) : that.quantity != null) return false;
        if (shipWaveNum != null ? !shipWaveNum.equals(that.shipWaveNum) : that.shipWaveNum != null) return false;
        if (userCreated != null ? !userCreated.equals(that.userCreated) : that.userCreated != null) return false;
        if (userLastChanged != null ? !userLastChanged.equals(that.userLastChanged) : that.userLastChanged != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = displayShipWaveId != null ? displayShipWaveId.hashCode() : 0;
        result = 31 * result + (displayId != null ? displayId.hashCode() : 0);
        result = 31 * result + (mustArriveDate != null ? mustArriveDate.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        result = 31 * result + (shipWaveNum != null ? shipWaveNum.hashCode() : 0);
        result = 31 * result + (dtCreated != null ? dtCreated.hashCode() : 0);
        result = 31 * result + (dtLastChanged != null ? dtLastChanged.hashCode() : 0);
        result = 31 * result + (userCreated != null ? userCreated.hashCode() : 0);
        result = 31 * result + (userLastChanged != null ? userLastChanged.hashCode() : 0);
        return result;
    }

    private Collection<DisplayPackout> displayPackoutsByDisplayShipWaveId;

    public Collection<DisplayPackout> getDisplayPackoutsByDisplayShipWaveId() {
        return displayPackoutsByDisplayShipWaveId;
    }

    public void setDisplayPackoutsByDisplayShipWaveId(Collection<DisplayPackout> displayPackoutsByDisplayShipWaveId) {
        this.displayPackoutsByDisplayShipWaveId = displayPackoutsByDisplayShipWaveId;
    }

    private Display displayByDisplayId;

    public Display getDisplayByDisplayId() {
        return displayByDisplayId;
    }

    public void setDisplayByDisplayId(Display displayByDisplayId) {
        this.displayByDisplayId = displayByDisplayId;
    }

    @Override
    public String toString() {
        return "DisplayShipWave{" +
                "displayShipWaveId=" + displayShipWaveId +
                ", displayId=" + displayId +
                ", mustArriveDate=" + mustArriveDate +
                ", quantity=" + quantity +
                ", shipWaveNum=" + shipWaveNum +
                ", dtCreated=" + dtCreated +
                ", dtLastChanged=" + dtLastChanged +
                ", userCreated='" + userCreated + '\'' +
                ", userLastChanged='" + userLastChanged + '\'' +
                ", displayPackoutsByDisplayShipWaveId=" + displayPackoutsByDisplayShipWaveId +
                ", displayByDisplayId=" + displayByDisplayId +
                '}';
    }
}
