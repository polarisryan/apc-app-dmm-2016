package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:54:07 AM
 */
public class ValidLocation implements Serializable {
    private String dcCode;

    public String getDcCode() {
        return dcCode;
    }

    public void setDcCode(String dcCode) {
        this.dcCode = dcCode;
    }

    private String dcName;

    public String getDcName() {
        return dcName;
    }

    public void setDcName(String dcName) {
        this.dcName = dcName;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValidLocation that = (ValidLocation) o;

        if (activeFlg != null ? !activeFlg.equals(that.activeFlg) : that.activeFlg != null) return false;
        if (dcCode != null ? !dcCode.equals(that.dcCode) : that.dcCode != null) return false;
        if (dcName != null ? !dcName.equals(that.dcName) : that.dcName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dcCode != null ? dcCode.hashCode() : 0;
        result = 31 * result + (dcName != null ? dcName.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }
}
