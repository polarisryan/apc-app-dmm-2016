package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:50 AM
 */
public class Structure implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4614807248535790997L;
	private Long structureId;

    public Long getStructureId() {
        return structureId;
    }

    public void setStructureId(Long structureId) {
        this.structureId = structureId;
    }

    private String structureName;

    public String getStructureName() {
        return structureName;
    }

    public void setStructureName(String structureName) {
        this.structureName = structureName;
    }

    private String structureDesc;

    public String getStructureDesc() {
        return structureDesc;
    }

    public void setStructureDesc(String structureDesc) {
        this.structureDesc = structureDesc;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Structure structure = (Structure) o;

        if (structureDesc != null ? !structureDesc.equals(structure.structureDesc) : structure.structureDesc != null)
            return false;
        if (structureId != null ? !structureId.equals(structure.structureId) : structure.structureId != null)
            return false;
        if (structureName != null ? !structureName.equals(structure.structureName) : structure.structureName != null)
            return false;
        if (activeFlg != null ? !activeFlg.equals(structure.activeFlg) : structure.activeFlg != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = structureId != null ? structureId.hashCode() : 0;
        result = 31 * result + (structureName != null ? structureName.hashCode() : 0);
        result = 31 * result + (structureDesc != null ? structureDesc.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }

    private Collection<Display> displaysByStructureId;

    public Collection<Display> getDisplaysByStructureId() {
        return displaysByStructureId;
    }

    public void setDisplaysByStructureId(Collection<Display> displaysByStructureId) {
        this.displaysByStructureId = displaysByStructureId;
    }
}
