package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

public class ScenarioStatus implements Serializable {
	 private Long scenarioStatusId;
	 private String name;
	 private String description;
	 private Boolean activeFlg;
	 public Long getScenarioStatusId() {
		return scenarioStatusId;
	 }
	 public void setScenarioStatusId(Long scenarioStatusId) {
		this.scenarioStatusId = scenarioStatusId;
	 }
	 public String getName() {
		return name;
	 }
	 public void setName(String name) {
		this.name = name;
	 }
	 public String getDescription() {
		return description;
	 }
	 public void setDescription(String description) {
		this.description = description;
	 }
	 public Boolean getActiveFlg() {
		return activeFlg;
	 }
	public void setActiveFlg(Boolean activeFlg) {
		this.activeFlg = activeFlg;
	}
	@Override
	public String toString() {
		return "ScenarioStatus [activeFlg=" + activeFlg + ", description="
				+ description + ", name=" + name + ", scenarioStatusId="
				+ scenarioStatusId + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((activeFlg == null) ? 0 : activeFlg.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime
				* result
				+ ((scenarioStatusId == null) ? 0 : scenarioStatusId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScenarioStatus other = (ScenarioStatus) obj;
		if (activeFlg == null) {
			if (other.activeFlg != null)
				return false;
		} else if (!activeFlg.equals(other.activeFlg))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (scenarioStatusId == null) {
			if (other.scenarioStatusId != null)
				return false;
		} else if (!scenarioStatusId.equals(other.scenarioStatusId))
			return false;
		return true;
	}
}
