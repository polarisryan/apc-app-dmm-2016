package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;

public class LocationCountry implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = -6993373867289106698L;
	private Long locationId;

	    public Long getLocationId() {
	        return locationId;
	    }

	    public void setLocationId(Long locationId) {
	        this.locationId = locationId;
	    }
	    private String locationCode;
	    public String getLocationCode() {
	        return locationCode;
	    }

	    public void setLocationCode(String locationCode) {
	        this.locationCode = locationCode;
	    }
	    
	    private String locationName;

	    public String getLocationName() {
	        return locationName;
	    }

	    public void setlocationName(String locationName) {
	        this.locationName = locationName;
	    }

	    private Boolean activeFlg;

	    public Boolean isActiveFlg() {
	        return activeFlg;
	    }

	    public void setActiveFlg(Boolean activeFlg) {
	        this.activeFlg = activeFlg;
	    }

	    Long countryId;
	    public Long getCountryId() {
		     return countryId;
		 }

		 public void setCountryId(Long countryId) {
		        this.countryId=countryId;
		 }

		 String countryName;
		 public String getCountryName() {
		     return countryName;
		 }

		 public void setCountryName(String countryName) {
		        this.countryName= countryName;
		 }
		 
		 private Boolean activeCountryFlg;
		 public Boolean isActiveCountryFlg() {
		      return activeCountryFlg;
		 }

		 public void setActiveCountryFlg(Boolean activeCountryFlg) {
		     this.activeCountryFlg = activeCountryFlg;
		 }
		    
	    @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;

	        LocationCountry locationCountry = (LocationCountry) o;
	        if (locationId != null ? !locationId.equals(locationCountry.locationId) : locationCountry.locationId != null) return false;
	        if (locationName != null ? !locationName.equals(locationCountry.locationName) : locationCountry.locationName != null)
	            return false;
	        if (activeFlg != null ? !activeFlg.equals(locationCountry.activeFlg) : locationCountry.activeFlg != null) return false;

	        return true;
	    }

	    @Override
	    public int hashCode() {
	        int result = locationId != null ? locationId.hashCode() : 0;
	        result = 31 * result + (locationName != null ? locationName.hashCode() : 0);
	        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
	        return result;
	    }

	    private Collection<Display> displaysByLocationId;

	    public Collection<Display> getDisplaysByLocationId() {
	        return displaysByLocationId;
	    }

	    public void setDisplaysByLocationId(Collection<Display> displaysByLocationId) {
	        this.displaysByLocationId = displaysByLocationId;
	    }

	    @Override
	    public String toString() {
	        return "LocationCountry{" +
	                "locationId=" + locationId +
	                ", locationName='" + locationName + '\'' +
	                ", displaysByLocationId=" + displaysByLocationId +
	                ", countryId=" + countryId +
	                ", countryName='" + countryName + '\'' +
	                '}';
	    }
	}

