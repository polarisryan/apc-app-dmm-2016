package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:48 AM
 */
public class Location implements Serializable {
    private Long locationId;
    private String locationCode;

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    private String locationName;

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (activeFlg != null ? !activeFlg.equals(location.activeFlg) : location.activeFlg != null) return false;
        if (locationId != null ? !locationId.equals(location.locationId) : location.locationId != null) return false;
        if (locationName != null ? !locationName.equals(location.locationName) : location.locationName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = locationId != null ? locationId.hashCode() : 0;
        result = 31 * result + (locationName != null ? locationName.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }

    private Collection<DisplayPackout> displayPackoutsByShipFromLocationId;

    public Collection<DisplayPackout> getDisplayPackoutsByShipFromLocationId() {
        return displayPackoutsByShipFromLocationId;
    }

    public void setDisplayPackoutsByShipFromLocationId(Collection<DisplayPackout> displayPackoutsByShipFromLocationId) {
        this.displayPackoutsByShipFromLocationId = displayPackoutsByShipFromLocationId;
    }

    private Collection<DisplayPackout> displayPackoutsByLocationId;

    public Collection<DisplayPackout> getDisplayPackoutsByLocationId() {
        return displayPackoutsByLocationId;
    }

    public void setDisplayPackoutsByLocationId(Collection<DisplayPackout> displayPackoutsByLocationId) {
        this.displayPackoutsByLocationId = displayPackoutsByLocationId;
    }
    
	private Set<Country> countries = new HashSet<Country>(0);

	public void setCountries(Set<Country> countries) {
		this.countries = countries;
	}
	
	public Set<Country> getCountries() {
		return countries;
	}

    @Override
    public String toString() {
        return "Location{" +
                "locationId=" + locationId +
                ", locationCode='" + locationCode + '\'' +
                ", locationName='" + locationName + '\'' +
                ", activeFlg=" + activeFlg +
                ", displayPackoutsByShipFromLocationId=" + displayPackoutsByShipFromLocationId +
                ", displayPackoutsByLocationId=" + displayPackoutsByLocationId +
                '}';
    }
}
