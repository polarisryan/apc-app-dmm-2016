package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

public class CstBuPrgmPct implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long CstBuPrgmPctId;
	private Double programPct;
	private ValidBu bu;
	private Customer customer;
	private Boolean activeFlg;
	private Long fYear;
	
	public Long getCstBuPrgmPctId() {
		return CstBuPrgmPctId;
	}
	public void setCstBuPrgmPctId(Long cstBuPrgmPctId) {
		CstBuPrgmPctId = cstBuPrgmPctId;
	}
	public Double getProgramPct() {
		return programPct;
	}
	public void setProgramPct(Double programPct) {
		this.programPct = programPct;
	}
	public ValidBu getBu() {
		return bu;
	}
	public void setBu(ValidBu bu) {
		this.bu = bu;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Boolean getActiveFlg() {
		return activeFlg;
	}
	public void setActiveFlg(Boolean activeFlg) {
		this.activeFlg = activeFlg;
	}
	public Long getFyear() {
		return fYear;
	}
	public void setFyear(Long fYear) {
		this.fYear = fYear;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((CstBuPrgmPctId == null) ? 0 : CstBuPrgmPctId.hashCode());
		result = prime * result
				+ ((activeFlg == null) ? 0 : activeFlg.hashCode());
		result = prime * result + ((bu == null) ? 0 : bu.hashCode());
		result = prime * result
				+ ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + ((fYear == null) ? 0 : fYear.hashCode());
		result = prime * result
				+ ((programPct == null) ? 0 : programPct.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstBuPrgmPct other = (CstBuPrgmPct) obj;
		if (CstBuPrgmPctId == null) {
			if (other.CstBuPrgmPctId != null)
				return false;
		} else if (!CstBuPrgmPctId.equals(other.CstBuPrgmPctId))
			return false;
		if (activeFlg == null) {
			if (other.activeFlg != null)
				return false;
		} else if (!activeFlg.equals(other.activeFlg))
			return false;
		if (bu == null) {
			if (other.bu != null)
				return false;
		} else if (!bu.equals(other.bu))
			return false;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.equals(other.customer))
			return false;
		if (fYear == null) {
			if (other.fYear != null)
				return false;
		} else if (!fYear.equals(other.fYear))
			return false;
		if (programPct == null) {
			if (other.programPct != null)
				return false;
		} else if (!programPct.equals(other.programPct))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CstBuPrgmPct [CstBuPrgmPctId=" + CstBuPrgmPctId
				+ ", activeFlg=" + activeFlg + ", bu=" + bu + ", customer="
				+ customer + ", fYear=" + fYear + ", programPct=" + programPct
				+ "]";
	}
}
