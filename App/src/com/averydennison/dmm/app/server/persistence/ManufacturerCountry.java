package com.averydennison.dmm.app.server.persistence;

import java.util.Collection;

public class ManufacturerCountry {
	private Long manufacturerId;

    public Long getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }

    Long countryId;
    public Long getCountryId() {
	     return countryId;
	 }

	 public void setCountryId(Long countryId) {
	        this.countryId=countryId;
	 }

	 String countryName;
	 public String getCountryName() {
	     return countryName;
	 }

	 public void setCountryName(String countryName) {
	        this.countryName= countryName;
	 }
	 
	 private Boolean activeCountryFlg;
	 public Boolean isActiveCountryFlg() {
	      return activeCountryFlg;
	 }

	 public void setActiveCountryFlg(Boolean activeCountryFlg) {
	     this.activeCountryFlg = activeCountryFlg;
	 }
	    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ManufacturerCountry manufacturerCountry = (ManufacturerCountry) o;
        if (manufacturerId != null ? !manufacturerId.equals(manufacturerCountry.manufacturerId) : manufacturerCountry.manufacturerId != null) return false;
        if (name != null ? !name.equals(manufacturerCountry.name) : manufacturerCountry.name != null)
            return false;
        if (activeFlg != null ? !activeFlg.equals(manufacturerCountry.activeFlg) : manufacturerCountry.activeFlg != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = manufacturerId != null ? manufacturerId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }

    private Collection<Display> displaysByLocationId;

    public Collection<Display> getDisplaysByLocationId() {
        return displaysByLocationId;
    }

    public void setDisplaysByLocationId(Collection<Display> displaysByLocationId) {
        this.displaysByLocationId = displaysByLocationId;
    }

    @Override
    public String toString() {
        return "ManufacturerCountry{" +
                "manufacturerId=" + manufacturerId +
                ", manufacturerName='" + name + '\'' +
                ", displaysByLocationId=" + displaysByLocationId +
                ", countryId=" + countryId +
                ", countryName='" + countryName + '\'' +
                '}';
    }
}


