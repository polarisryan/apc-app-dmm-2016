package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

/**
 * User: Mari
 * Date: Aug 01, 2010
 * Time: 11:21:36 AM
 */


public class DisplayCostComment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long displayCostCommentId;
	private Long displayCostId;
	private Date createDate;
	private String userName;
	private String costComment;
	private Long displayScenarioId;
	
	public Long getDisplayCostCommentId() {
		return displayCostCommentId;
	}
	public void setDisplayCostCommentId(Long displayCostCommentId) {
		this.displayCostCommentId = displayCostCommentId;
	}
	public Long getDisplayCostId() {
		return displayCostId;
	}
	public void setDisplayCostId(Long displayCostId) {
		this.displayCostId = displayCostId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCostComment() {
		return costComment;
	}
	public void setCostComment(String costComment) {
		this.costComment = costComment;
	}
	public Long getDisplayScenarioId() {
		return displayScenarioId;
	}
	public void setDisplayScenarioId(Long displayScenarioId) {
		this.displayScenarioId = displayScenarioId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((costComment == null) ? 0 : costComment.hashCode());
		result = prime * result
				+ ((createDate == null) ? 0 : createDate.hashCode());
		result = prime
				* result
				+ ((displayCostCommentId == null) ? 0 : displayCostCommentId
						.hashCode());
		result = prime * result
				+ ((displayCostId == null) ? 0 : displayCostId.hashCode());
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		result = prime * result
				+ ((displayScenarioId == null) ? 0 : displayScenarioId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DisplayCostComment other = (DisplayCostComment) obj;
		if (costComment == null) {
			if (other.costComment != null)
				return false;
		} else if (!costComment.equals(other.costComment))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (displayCostCommentId == null) {
			if (other.displayCostCommentId != null)
				return false;
		} else if (!displayCostCommentId.equals(other.displayCostCommentId))
			return false;
		if (displayCostId == null) {
			if (other.displayCostId != null)
				return false;
		} else if (!displayCostId.equals(other.displayCostId))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		if (displayScenarioId == null) {
			if (other.displayScenarioId != null)
				return false;
		} else if (!displayScenarioId.equals(other.displayScenarioId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "DisplayCostComment [costComment=" + costComment
				+ ", createDate=" + createDate + ", displayCostCommentId="
				+ displayCostCommentId + ", displayCostId=" + displayCostId
				+ ", displayScenarioId=" + displayScenarioId
				+ ", userName=" + userName + "]";
	}
	
	 private DisplayCost displayCostByDisplayCostId;

	    public DisplayCost getDisplayCostByDisplayCostId() {
	        return displayCostByDisplayCostId;
	    }

	    public void setDisplayCostByDisplayCostId(DisplayCost displayCostByDisplayCostId) {
	        this.displayCostByDisplayCostId = displayCostByDisplayCostId;
	    }
	
}
