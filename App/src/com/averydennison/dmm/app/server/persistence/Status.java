package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:50 AM
 */
public class Status implements Serializable {
    private Long statusId;

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    private String statusName;

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Status status = (Status) o;

        if (description != null ? !description.equals(status.description) : status.description != null) return false;
        if (statusId != null ? !statusId.equals(status.statusId) : status.statusId != null) return false;
        if (statusName != null ? !statusName.equals(status.statusName) : status.statusName != null) return false;
        if (activeFlg != null ? !activeFlg.equals(status.activeFlg) : status.activeFlg != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = statusId != null ? statusId.hashCode() : 0;
        result = 31 * result + (statusName != null ? statusName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }

    private Collection<Display> displaysByStatusId;

    public Collection<Display> getDisplaysByStatusId() {
        return displaysByStatusId;
    }

    public void setDisplaysByStatusId(Collection<Display> displaysByStatusId) {
        this.displaysByStatusId = displaysByStatusId;
    }

    @Override
    public String toString() {
        return "Status{" +
                "statusId=" + statusId +
                ", statusName='" + statusName + '\'' +
                ", description='" + description + '\'' +
                ", displaysByStatusId=" + displaysByStatusId +
                '}';
    }
}
