package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

public class DisplayForMassUpdate implements Serializable {
	
	
		
	 private Long displayId ; 
	 private String sku;
	 private String description;
	 private Long customerId;
	 private Long statusId;
	 private Long promoPeriodId;
	 private Long promoYear;
	 private Long displaySalesRepId;
	 private Long structureId;
	 private String skuMixFinalFlg;
	 
	 private Date dtLastChanged;
	 private Date  priceAvailabilityDate;
	 private Date  sentSampleDate;
	 private Date  finalArtworkDate;
	 private Date  pimCompletedDate;
	 private Date   pricingAdminDate;
	 private String   initialRenderingFlg;
	 private String  structureApprovedFlg;
	 private String  orderInFlg;
	 private String   comments;
	 
	 private String customerName;
	 private String statusName;
	 private String promoPeriodName;
	 private String fullName;
	 private String structureName;
	 private Short projectLevel;
	 private Long corrugateId;
	 private String corrugateName;


	public Long getDisplayId() {
		return displayId;
	}
	public void setDisplayId(Long displayId) {
		this.displayId = displayId;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public Long getPromoPeriodId() {
		return promoPeriodId;
	}
	public void setPromoPeriodId(Long promoPeriodId) {
		this.promoPeriodId = promoPeriodId;
	}
	public Long getPromoYear() {
		return promoYear;
	}
	public void setPromoYear(Long promoYear) {
		this.promoYear = promoYear;
	}
	public Long getDisplaySalesRepId() {
		return displaySalesRepId;
	}
	public void setDisplaySalesRepId(Long displaySalesRepId) {
		this.displaySalesRepId = displaySalesRepId;
	}
	public Long getStructureId() {
		return structureId;
	}
	public void setStructureId(Long structureId) {
		this.structureId = structureId;
	}
	public String getSkuMixFinalFlg() {
		return skuMixFinalFlg;
	}
	public void setSkuMixFinalFlg(String skuMixFinalFlg) {
		this.skuMixFinalFlg = skuMixFinalFlg;
	}
	public Date getDtLastChanged() {
		return dtLastChanged;
	}
	public void setDtLastChanged(Date dtLastChanged) {
		this.dtLastChanged = dtLastChanged;
	}
	public Date getPriceAvailabilityDate() {
		return priceAvailabilityDate;
	}
	public void setPriceAvailabilityDate(Date priceAvailabilityDate) {
		this.priceAvailabilityDate = priceAvailabilityDate;
	}
	public Date getSentSampleDate() {
		return sentSampleDate;
	}
	public void setSentSampleDate(Date sentSampleDate) {
		this.sentSampleDate = sentSampleDate;
	}
	public Date getFinalArtworkDate() {
		return finalArtworkDate;
	}
	public void setFinalArtworkDate(Date finalArtworkDate) {
		this.finalArtworkDate = finalArtworkDate;
	}
	public Date getPimCompletedDate() {
		return pimCompletedDate;
	}
	public void setPimCompletedDate(Date pimCompletedDate) {
		this.pimCompletedDate = pimCompletedDate;
	}
	public Date getPricingAdminDate() {
		return pricingAdminDate;
	}
	public void setPricingAdminDate(Date pricingAdminDate) {
		this.pricingAdminDate = pricingAdminDate;
	}
	public String getInitialRenderingFlg() {
		return initialRenderingFlg;
	}
	public void setInitialRenderingFlg(String initialRenderingFlg) {
		this.initialRenderingFlg = initialRenderingFlg;
	}
	public String getStructureApprovedFlg() {
		return structureApprovedFlg;
	}
	public void setStructureApprovedFlg(String structureApprovedFlg) {
		this.structureApprovedFlg = structureApprovedFlg;
	}
	public String getOrderInFlg() {
		return orderInFlg;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getPromoPeriodName() {
		return promoPeriodName;
	}
	public void setPromoPeriodName(String promoPeriodName) {
		this.promoPeriodName = promoPeriodName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getStructureName() {
		return structureName;
	}
	public void setStructureName(String structureName) {
		this.structureName = structureName;
	}
	public Long getCorrugateId() {
		return corrugateId;
	}
	public void setCorrugateId(Long corrugateId) {
		this.corrugateId = corrugateId;
	}
	public String getCorrugateName() {
		return corrugateName;
	}
	public void setCorrugateName(String corrugateName) {
		this.corrugateName = corrugateName;
	}
	@Override
	public String toString() {
		return "DisplayForMassUpdate [comments=" + comments + ", customerId="
				+ customerId + ", description=" + description + ", displayId="
				+ displayId + ", displaySalesRepId=" + displaySalesRepId
				+ ", dtLastChanged=" + dtLastChanged + ", finalArtworkDate="
				+ finalArtworkDate + ", initialRenderingFlg="
				+ initialRenderingFlg + ", orderInFlg=" + orderInFlg
				+ ", pimCompletedDate=" + pimCompletedDate
				+ ", priceAvailabilityDate=" + priceAvailabilityDate
				+ ", pricingAdminDate=" + pricingAdminDate + ", promoPeriodId="
				+ promoPeriodId + ", promoYear=" + promoYear
				+ ", sentSampleDate=" + sentSampleDate + ", sku=" + sku
				+ ", skuMixFinalFlg=" + skuMixFinalFlg + ", statusId="
				+ statusId + ", structureApprovedFlg=" + structureApprovedFlg
				+ ", structureId=" + structureId + "]";
	}
	public void setOrderInFlg(String orderInFlg) {
		this.orderInFlg = orderInFlg;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Short getProjectLevel() {
		return projectLevel;
	}
	public void setProjectLevel(Short projectLevel) {
		this.projectLevel = projectLevel;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result
				+ ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((displayId == null) ? 0 : displayId.hashCode());
		result = prime
				* result
				+ ((displaySalesRepId == null) ? 0 : displaySalesRepId
						.hashCode());
		result = prime * result
				+ ((dtLastChanged == null) ? 0 : dtLastChanged.hashCode());
		result = prime
				* result
				+ ((finalArtworkDate == null) ? 0 : finalArtworkDate.hashCode());
		result = prime
				* result
				+ ((initialRenderingFlg == null) ? 0 : initialRenderingFlg
						.hashCode());
		result = prime * result
				+ ((orderInFlg == null) ? 0 : orderInFlg.hashCode());
		result = prime
				* result
				+ ((pimCompletedDate == null) ? 0 : pimCompletedDate.hashCode());
		result = prime
				* result
				+ ((priceAvailabilityDate == null) ? 0 : priceAvailabilityDate
						.hashCode());
		result = prime
				* result
				+ ((pricingAdminDate == null) ? 0 : pricingAdminDate.hashCode());
		result = prime * result
				+ ((promoPeriodId == null) ? 0 : promoPeriodId.hashCode());
		result = prime * result
				+ ((promoYear == null) ? 0 : promoYear.hashCode());
		result = prime * result
				+ ((sentSampleDate == null) ? 0 : sentSampleDate.hashCode());
		result = prime * result + ((sku == null) ? 0 : sku.hashCode());
		result = prime * result
				+ ((skuMixFinalFlg == null) ? 0 : skuMixFinalFlg.hashCode());
		result = prime * result
				+ ((statusId == null) ? 0 : statusId.hashCode());
		result = prime
				* result
				+ ((structureApprovedFlg == null) ? 0 : structureApprovedFlg
						.hashCode());
		result = prime * result
				+ ((structureId == null) ? 0 : structureId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DisplayForMassUpdate other = (DisplayForMassUpdate) obj;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (displayId == null) {
			if (other.displayId != null)
				return false;
		} else if (!displayId.equals(other.displayId))
			return false;
		if (displaySalesRepId == null) {
			if (other.displaySalesRepId != null)
				return false;
		} else if (!displaySalesRepId.equals(other.displaySalesRepId))
			return false;
		if (dtLastChanged == null) {
			if (other.dtLastChanged != null)
				return false;
		} else if (!dtLastChanged.equals(other.dtLastChanged))
			return false;
		if (finalArtworkDate == null) {
			if (other.finalArtworkDate != null)
				return false;
		} else if (!finalArtworkDate.equals(other.finalArtworkDate))
			return false;
		if (initialRenderingFlg == null) {
			if (other.initialRenderingFlg != null)
				return false;
		} else if (!initialRenderingFlg.equals(other.initialRenderingFlg))
			return false;
		if (orderInFlg == null) {
			if (other.orderInFlg != null)
				return false;
		} else if (!orderInFlg.equals(other.orderInFlg))
			return false;
		if (pimCompletedDate == null) {
			if (other.pimCompletedDate != null)
				return false;
		} else if (!pimCompletedDate.equals(other.pimCompletedDate))
			return false;
		if (priceAvailabilityDate == null) {
			if (other.priceAvailabilityDate != null)
				return false;
		} else if (!priceAvailabilityDate.equals(other.priceAvailabilityDate))
			return false;
		if (pricingAdminDate == null) {
			if (other.pricingAdminDate != null)
				return false;
		} else if (!pricingAdminDate.equals(other.pricingAdminDate))
			return false;
		if (promoPeriodId == null) {
			if (other.promoPeriodId != null)
				return false;
		} else if (!promoPeriodId.equals(other.promoPeriodId))
			return false;
		if (promoYear == null) {
			if (other.promoYear != null)
				return false;
		} else if (!promoYear.equals(other.promoYear))
			return false;
		if (sentSampleDate == null) {
			if (other.sentSampleDate != null)
				return false;
		} else if (!sentSampleDate.equals(other.sentSampleDate))
			return false;
		if (sku == null) {
			if (other.sku != null)
				return false;
		} else if (!sku.equals(other.sku))
			return false;
		if (skuMixFinalFlg == null) {
			if (other.skuMixFinalFlg != null)
				return false;
		} else if (!skuMixFinalFlg.equals(other.skuMixFinalFlg))
			return false;
		if (statusId == null) {
			if (other.statusId != null)
				return false;
		} else if (!statusId.equals(other.statusId))
			return false;
		if (structureApprovedFlg == null) {
			if (other.structureApprovedFlg != null)
				return false;
		} else if (!structureApprovedFlg.equals(other.structureApprovedFlg))
			return false;
		if (structureId == null) {
			if (other.structureId != null)
				return false;
		} else if (!structureId.equals(other.structureId))
			return false;
		return true;
	}
	 
	 
	}
