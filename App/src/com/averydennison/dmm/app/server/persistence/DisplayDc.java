package com.averydennison.dmm.app.server.persistence;

import java.util.Collection;
import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Nov 30, 2009
 * Time: 5:14:40 PM
 */
public class DisplayDc {
    private Long displayDcId;

    public Long getDisplayDcId() {
        return displayDcId;
    }

    public void setDisplayDcId(Long displayDcId) {
        this.displayDcId = displayDcId;
    }

    private Long displayId;

    public Long getDisplayId() {
        return displayId;
    }

    public void setDisplayId(Long displayId) {
        this.displayId = displayId;
    }

    private Long locationId;

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    private Long quantity;

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    private Date dtCreated;

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    private Date dtLastChanged;

    public Date getDtLastChanged() {
        return dtLastChanged;
    }

    public void setDtLastChanged(Date dtLastChanged) {
        this.dtLastChanged = dtLastChanged;
    }

    private String userCreated;

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    private String userLastChanged;

    public String getUserLastChanged() {
        return userLastChanged;
    }

    public void setUserLastChanged(String userLastChanged) {
        this.userLastChanged = userLastChanged;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisplayDc displayDc = (DisplayDc) o;

        if (displayDcId != null ? !displayDcId.equals(displayDc.displayDcId) : displayDc.displayDcId != null)
            return false;
        if (displayId != null ? !displayId.equals(displayDc.displayId) : displayDc.displayId != null) return false;
        if (dtCreated != null ? !dtCreated.equals(displayDc.dtCreated) : displayDc.dtCreated != null) return false;
        if (dtLastChanged != null ? !dtLastChanged.equals(displayDc.dtLastChanged) : displayDc.dtLastChanged != null)
            return false;
        if (locationId != null ? !locationId.equals(displayDc.locationId) : displayDc.locationId != null) return false;
        if (quantity != null ? !quantity.equals(displayDc.quantity) : displayDc.quantity != null) return false;
        if (userCreated != null ? !userCreated.equals(displayDc.userCreated) : displayDc.userCreated != null)
            return false;
        if (userLastChanged != null ? !userLastChanged.equals(displayDc.userLastChanged) : displayDc.userLastChanged != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = displayDcId != null ? displayDcId.hashCode() : 0;
        result = 31 * result + (displayId != null ? displayId.hashCode() : 0);
        result = 31 * result + (locationId != null ? locationId.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        result = 31 * result + (dtCreated != null ? dtCreated.hashCode() : 0);
        result = 31 * result + (dtLastChanged != null ? dtLastChanged.hashCode() : 0);
        result = 31 * result + (userCreated != null ? userCreated.hashCode() : 0);
        result = 31 * result + (userLastChanged != null ? userLastChanged.hashCode() : 0);
        return result;
    }

    private Location locationByLocationId;

    public Location getLocationByLocationId() {
        return locationByLocationId;
    }

    public void setLocationByLocationId(Location locationByLocationId) {
        this.locationByLocationId = locationByLocationId;
    }

    private Display displayByDisplayId;

    public Display getDisplayByDisplayId() {
        return displayByDisplayId;
    }

    public void setDisplayByDisplayId(Display displayByDisplayId) {
        this.displayByDisplayId = displayByDisplayId;
    }

    private Collection<DisplayPackout> displayPackoutsByDisplayDcId;

    public Collection<DisplayPackout> getDisplayPackoutsByDisplayDcId() {
        return displayPackoutsByDisplayDcId;
    }

    public void setDisplayPackoutsByDisplayDcId(Collection<DisplayPackout> displayPackoutsByDisplayDcId) {
        this.displayPackoutsByDisplayDcId = displayPackoutsByDisplayDcId;
    }

    @Override
    public String toString() {
        return "DisplayDc{" +
                "displayDcId=" + displayDcId +
                ", displayId=" + displayId +
                ", locationId=" + locationId +
                ", quantity=" + quantity +
                ", dtCreated=" + dtCreated +
                ", dtLastChanged=" + dtLastChanged +
                ", userCreated='" + userCreated + '\'' +
                ", userLastChanged='" + userLastChanged + '\'' +
                ", locationByLocationId=" + locationByLocationId +
                ", displayByDisplayId=" + displayByDisplayId +
                ", displayPackoutsByDisplayDcId=" + displayPackoutsByDisplayDcId +
                '}';
    }
}