package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

public class CorrugateComponentsDetail implements Serializable {
	 private Long displayId;
	 private Long  corrugateComponentId;
	 private Long  countryId;
	 private Long  manufacturerId;
	 private String  vendorPartNum;
	 private String  gloviaPartNum;
	 private String  description;
	 private String  boardTest;
	 private String  material;
	 private Long  qtyPerDisplay;
	 private Double  piecePrice;
	 private Double  totalCost;
	 private Long  totalQty;
	 private Date  dtCreated;
	 private Date  dtLastChanged;
	 private String  userCreated;
	 private String  userLastChanged;
	 private String  countryName;
	 private String  name;
	public Long getDisplayId() {
		return displayId;
	}
	public void setDisplayId(Long displayId) {
		this.displayId = displayId;
	}
	public Long getCorrugateComponentId() {
		return corrugateComponentId;
	}
	public void setCorrugateComponentId(Long corrugateComponentId) {
		this.corrugateComponentId = corrugateComponentId;
	}
	public Long getCountryId() {
		return countryId;
	}
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}
	public Long getManufacturerId() {
		return manufacturerId;
	}
	public void setManufacturerId(Long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	public String getVendorPartNum() {
		return vendorPartNum;
	}
	public void setVendorPartNum(String vendorPartNum) {
		this.vendorPartNum = vendorPartNum;
	}
	public String getGloviaPartNum() {
		return gloviaPartNum;
	}
	public void setGloviaPartNum(String gloviaPartNum) {
		this.gloviaPartNum = gloviaPartNum;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBoardTest() {
		return boardTest;
	}
	public void setBoardTest(String boardTest) {
		this.boardTest = boardTest;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public Long getQtyPerDisplay() {
		return qtyPerDisplay;
	}
	public void setQtyPerDisplay(Long qtyPerDisplay) {
		this.qtyPerDisplay = qtyPerDisplay;
	}
	public Double getPiecePrice() {
		return piecePrice;
	}
	public void setPiecePrice(Double piecePrice) {
		this.piecePrice = piecePrice;
	}
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public Long getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(Long totalQty) {
		this.totalQty = totalQty;
	}
	public Date getDtCreated() {
		return dtCreated;
	}
	public void setDtCreated(Date dtCreated) {
		this.dtCreated = dtCreated;
	}
	public Date getDtLastChanged() {
		return dtLastChanged;
	}
	public void setDtLastChanged(Date dtLastChanged) {
		this.dtLastChanged = dtLastChanged;
	}
	public String getUserCreated() {
		return userCreated;
	}
	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}
	public String getUserLastChanged() {
		return userLastChanged;
	}
	public void setUserLastChanged(String userLastChanged) {
		this.userLastChanged = userLastChanged;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((boardTest == null) ? 0 : boardTest.hashCode());
		result = prime
				* result
				+ ((corrugateComponentId == null) ? 0 : corrugateComponentId
						.hashCode());
		result = prime * result
				+ ((countryId == null) ? 0 : countryId.hashCode());
		result = prime * result
				+ ((countryName == null) ? 0 : countryName.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((displayId == null) ? 0 : displayId.hashCode());
		result = prime * result
				+ ((dtCreated == null) ? 0 : dtCreated.hashCode());
		result = prime * result
				+ ((dtLastChanged == null) ? 0 : dtLastChanged.hashCode());
		result = prime * result
				+ ((gloviaPartNum == null) ? 0 : gloviaPartNum.hashCode());
		result = prime * result
				+ ((material == null) ? 0 : material.hashCode());
		result = prime * result
				+ ((piecePrice == null) ? 0 : piecePrice.hashCode());
		result = prime * result
				+ ((qtyPerDisplay == null) ? 0 : qtyPerDisplay.hashCode());
		result = prime * result
				+ ((totalCost == null) ? 0 : totalCost.hashCode());
		result = prime * result
				+ ((totalQty == null) ? 0 : totalQty.hashCode());
		result = prime * result
				+ ((userCreated == null) ? 0 : userCreated.hashCode());
		result = prime * result
				+ ((userLastChanged == null) ? 0 : userLastChanged.hashCode());
		result = prime * result
				+ ((manufacturerId == null) ? 0 : manufacturerId.hashCode());
		result = prime * result
				+ ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((vendorPartNum == null) ? 0 : vendorPartNum.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CorrugateComponentsDetail other = (CorrugateComponentsDetail) obj;
		if (boardTest == null) {
			if (other.boardTest != null)
				return false;
		} else if (!boardTest.equals(other.boardTest))
			return false;
		if (corrugateComponentId == null) {
			if (other.corrugateComponentId != null)
				return false;
		} else if (!corrugateComponentId.equals(other.corrugateComponentId))
			return false;
		if (countryId == null) {
			if (other.countryId != null)
				return false;
		} else if (!countryId.equals(other.countryId))
			return false;
		if (countryName == null) {
			if (other.countryName != null)
				return false;
		} else if (!countryName.equals(other.countryName))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (displayId == null) {
			if (other.displayId != null)
				return false;
		} else if (!displayId.equals(other.displayId))
			return false;
		if (dtCreated == null) {
			if (other.dtCreated != null)
				return false;
		} else if (!dtCreated.equals(other.dtCreated))
			return false;
		if (dtLastChanged == null) {
			if (other.dtLastChanged != null)
				return false;
		} else if (!dtLastChanged.equals(other.dtLastChanged))
			return false;
		if (gloviaPartNum == null) {
			if (other.gloviaPartNum != null)
				return false;
		} else if (!gloviaPartNum.equals(other.gloviaPartNum))
			return false;
		if (material == null) {
			if (other.material != null)
				return false;
		} else if (!material.equals(other.material))
			return false;
		if (piecePrice == null) {
			if (other.piecePrice != null)
				return false;
		} else if (!piecePrice.equals(other.piecePrice))
			return false;
		if (qtyPerDisplay == null) {
			if (other.qtyPerDisplay != null)
				return false;
		} else if (!qtyPerDisplay.equals(other.qtyPerDisplay))
			return false;
		if (totalCost == null) {
			if (other.totalCost != null)
				return false;
		} else if (!totalCost.equals(other.totalCost))
			return false;
		if (totalQty == null) {
			if (other.totalQty != null)
				return false;
		} else if (!totalQty.equals(other.totalQty))
			return false;
		if (userCreated == null) {
			if (other.userCreated != null)
				return false;
		} else if (!userCreated.equals(other.userCreated))
			return false;
		if (userLastChanged == null) {
			if (other.userLastChanged != null)
				return false;
		} else if (!userLastChanged.equals(other.userLastChanged))
			return false;
		if (manufacturerId == null) {
			if (other.manufacturerId != null)
				return false;
		} else if (!manufacturerId.equals(other.manufacturerId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (vendorPartNum == null) {
			if (other.vendorPartNum != null)
				return false;
		} else if (!vendorPartNum.equals(other.vendorPartNum))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CorrugateComponentsDetail [boardTest=" + boardTest
				+ ", corrugateComponentId=" + corrugateComponentId
				+ ", countryId=" + countryId + ", countryName=" + countryName
				+ ", description=" + description + ", displayId=" + displayId
				+ ", dtCreated="
				+ dtCreated + ", dtLastChanged=" + dtLastChanged
				+ ", gloviaPartNum=" + gloviaPartNum + ", material=" + material
				+ ", piecePrice=" + piecePrice + ", qtyPerDisplay="
				+ qtyPerDisplay + ", totalCost=" + totalCost + ", totalQty="
				+ totalQty + ", userCreated=" + userCreated
				+ ", userLastChanged=" + userLastChanged + ", manufacturerId="
				+ manufacturerId + ", name=" + name + ", vendorPartNum="
				+ vendorPartNum + "]";
	}
}
