package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

public class CustomerPct implements Serializable {
	 private Long customerId;
	 private Float binders;
	 private Float cardsPaper;
	 private Float dividers;
	 private Float labels;
	 private Float oAndP;
	 private Float writingInstruments;
	 
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Float getBinders() {
		return binders;
	}
	public void setBinders(Float binders) {
		this.binders = binders;
	}
	public Float getCardsPaper() {
		return cardsPaper;
	}
	public void setCardsPaper(Float cardsPaper) {
		this.cardsPaper = cardsPaper;
	}
	public Float getDividers() {
		return dividers;
	}
	public void setDividers(Float dividers) {
		this.dividers = dividers;
	}
	public Float getLabels() {
		return labels;
	}
	public void setLabels(Float labels) {
		this.labels = labels;
	}
	public Float getoAndP() {
		return oAndP;
	}
	public void setoAndP(Float oAndP) {
		this.oAndP = oAndP;
	}
	public Float getWritingInstruments() {
		return writingInstruments;
	}
	public void setWritingInstruments(Float writingInstruments) {
		this.writingInstruments = writingInstruments;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((binders == null) ? 0 : binders.hashCode());
		result = prime * result
				+ ((cardsPaper == null) ? 0 : cardsPaper.hashCode());
		result = prime * result
				+ ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result
				+ ((dividers == null) ? 0 : dividers.hashCode());
		result = prime * result + ((labels == null) ? 0 : labels.hashCode());
		result = prime * result + ((oAndP == null) ? 0 : oAndP.hashCode());
		result = prime
				* result
				+ ((writingInstruments == null) ? 0 : writingInstruments
						.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerPct other = (CustomerPct) obj;
		if (binders == null) {
			if (other.binders != null)
				return false;
		} else if (!binders.equals(other.binders))
			return false;
		if (cardsPaper == null) {
			if (other.cardsPaper != null)
				return false;
		} else if (!cardsPaper.equals(other.cardsPaper))
			return false;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (dividers == null) {
			if (other.dividers != null)
				return false;
		} else if (!dividers.equals(other.dividers))
			return false;
		if (labels == null) {
			if (other.labels != null)
				return false;
		} else if (!labels.equals(other.labels))
			return false;
		if (oAndP == null) {
			if (other.oAndP != null)
				return false;
		} else if (!oAndP.equals(other.oAndP))
			return false;
		if (writingInstruments == null) {
			if (other.writingInstruments != null)
				return false;
		} else if (!writingInstruments.equals(other.writingInstruments))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CustomerPct [binders=" + binders + ", cardsPaper=" + cardsPaper
				+ ", customerId=" + customerId + ", dividers=" + dividers
				+ ", labels=" + labels + ", oAndP=" + oAndP
				+ ", writingInstruments=" + writingInstruments + "]";
	}
	 
	
}
