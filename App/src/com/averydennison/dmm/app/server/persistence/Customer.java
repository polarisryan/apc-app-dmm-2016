package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * User: Spart Arguello Date: Nov 3, 2009 Time: 11:21:32 AM
 */
public class Customer implements Serializable {
	
	private Long customerId;
	private String customerName;
	private String customerDescription;
	private Boolean activeFlg;
	private Collection<Display> displaysByCustomerId;
	private String customerCode;
	private String companyCode;
	private Customer parent;
	private CustomerType customerType;
	private DisplaySalesRep cma;
	private Set<Country> countries = new HashSet<Country>(0);

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerDescription() {
		return customerDescription;
	}

	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}

	public Boolean isActiveFlg() {
		return activeFlg;
	}

	public void setActiveFlg(Boolean activeFlg) {
		this.activeFlg = activeFlg;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyCode() {
		return companyCode;
	}
	
	public void setParent(Customer parent) {
		this.parent = parent;
	}

	public Customer getParent() {
		return parent;
	}

	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}

	public CustomerType getCustomerType() {
		return customerType;
	}
	
	public Set<Country> getCountries() {
		return this.countries;
	}
	
	public void setCountries(Set<Country> countries) {
		this.countries = countries;
	}
	
	public void setCma(DisplaySalesRep cma) {
		this.cma = cma;
	}

	public DisplaySalesRep getCma() {
		return cma;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Customer customer = (Customer) o;

		if (customerDescription != null ? !customerDescription
				.equals(customer.customerDescription)
				: customer.customerDescription != null)
			return false;
		if (customerId != null ? !customerId.equals(customer.customerId)
				: customer.customerId != null)
			return false;
		if (customerName != null ? !customerName.equals(customer.customerName)
				: customer.customerName != null)
			return false;
		if (activeFlg != null ? !activeFlg.equals(customer.activeFlg)
				: customer.activeFlg != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = customerId != null ? customerId.hashCode() : 0;
		result = 31 * result
				+ (customerName != null ? customerName.hashCode() : 0);
		result = 31
				* result
				+ (customerDescription != null ? customerDescription.hashCode()
						: 0);
		result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
		return result;
	}

	public Collection<Display> getDisplaysByCustomerId() {
		return displaysByCustomerId;
	}

	public void setDisplaysByCustomerId(Collection<Display> displaysByCustomerId) {
		this.displaysByCustomerId = displaysByCustomerId;
	}

	@Override
	public String toString() {
		return "Customer{" + "customerId=" + customerId + ", customerName='"
				+ customerName + '\'' + ", customerDescription='"
				+ customerDescription + '\'' + ", displaysByCustomerId="
				+ displaysByCustomerId + '}';
	}
}
