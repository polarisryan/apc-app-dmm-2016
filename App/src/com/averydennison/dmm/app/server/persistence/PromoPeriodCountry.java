package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;

public class PromoPeriodCountry implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6992435813407345138L;
	private Long promoPeriodId;

    public Long getpromoPeriodId() {
        return promoPeriodId;
    }

    public void setpromoPeriodId(Long promoPeriodId) {
        this.promoPeriodId = promoPeriodId;
    }

    private String promoPeriodName;

    public String getpromoPeriodName() {
        return promoPeriodName;
    }

    public void setpromoPeriodName(String promoPeriodName) {
        this.promoPeriodName = promoPeriodName;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }

    Long countryId;
    public Long getCountryId() {
	     return countryId;
	 }

	 public void setCountryId(Long countryId) {
	        this.countryId=countryId;
	 }

	 String countryName;
	 public String getCountryName() {
	     return countryName;
	 }

	 public void setCountryName(String countryName) {
	        this.countryName= countryName;
	 }
	 
	 private Boolean activeCountryFlg;
	 public Boolean isActiveCountryFlg() {
	      return activeCountryFlg;
	 }

	 public void setActiveCountryFlg(Boolean activeCountryFlg) {
	     this.activeCountryFlg = activeCountryFlg;
	 }
	    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromoPeriodCountry promoPeriodCountry = (PromoPeriodCountry) o;
        if (promoPeriodId != null ? !promoPeriodId.equals(promoPeriodCountry.promoPeriodId) : promoPeriodCountry.promoPeriodId != null) return false;
        if (promoPeriodName != null ? !promoPeriodName.equals(promoPeriodCountry.promoPeriodName) : promoPeriodCountry.promoPeriodName != null)
            return false;
        if (activeFlg != null ? !activeFlg.equals(promoPeriodCountry.activeFlg) : promoPeriodCountry.activeFlg != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = promoPeriodId != null ? promoPeriodId.hashCode() : 0;
        result = 31 * result + (promoPeriodName != null ? promoPeriodName.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }

    private Collection<Display> displaysByLocationId;

    public Collection<Display> getDisplaysByLocationId() {
        return displaysByLocationId;
    }

    public void setDisplaysByLocationId(Collection<Display> displaysByLocationId) {
        this.displaysByLocationId = displaysByLocationId;
    }

    @Override
    public String toString() {
        return "PromoPeriodCountry{" +
                "promoPeriodId=" + promoPeriodId +
                ", promoPeriodName='" + promoPeriodName + '\'' +
                ", displaysByLocationId=" + displaysByLocationId +
                ", countryId=" + countryId +
                ", countryName='" + countryName + '\'' +
                '}';
    }
}



