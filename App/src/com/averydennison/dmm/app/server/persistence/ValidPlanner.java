package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 11:54:07 AM
 */
public class ValidPlanner implements Serializable {
    private String plannerCode;

    public String getPlannerCode() {
        return plannerCode;
    }

    public void setPlannerCode(String plannerCode) {
        this.plannerCode = plannerCode;
    }

    private String plannerDesc;

    public String getPlannerDesc() {
        return plannerDesc;
    }

    public void setPlannerDesc(String plannerDesc) {
        this.plannerDesc = plannerDesc;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValidPlanner that = (ValidPlanner) o;

        if (activeFlg != null ? !activeFlg.equals(that.activeFlg) : that.activeFlg != null) return false;
        if (plannerCode != null ? !plannerCode.equals(that.plannerCode) : that.plannerCode != null) return false;
        if (plannerDesc != null ? !plannerDesc.equals(that.plannerDesc) : that.plannerDesc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = plannerCode != null ? plannerCode.hashCode() : 0;
        result = 31 * result + (plannerDesc != null ? plannerDesc.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }
}
