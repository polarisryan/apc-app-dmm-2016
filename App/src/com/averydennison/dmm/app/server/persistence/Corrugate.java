package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

public class Corrugate implements Serializable {
	private Long corrugateId;
	private Long displayId;
	private Date createDate;
/*	private Date corrugateDueDate;
	private Date deliveryDate;*/
	private Float corrugateCost;
	
	public Long getCorrugateId() {
		return corrugateId;
	}
	public void setCorrugateId(Long corrugateId) {
		this.corrugateId = corrugateId;
	}
	public Long getDisplayId() {
		return displayId;
	}
	public void setDisplayId(Long displayId) {
		this.displayId = displayId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/*public Date getCorrugateDueDate() {
		return corrugateDueDate;
	}
	public void setCorrugateDueDate(Date corrugateDueDate) {
		this.corrugateDueDate = corrugateDueDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}*/
	
	public Float getCorrugateCost() {
		return corrugateCost;
	}
	public void setCorrugateCost(Float corrugateCost) {
		this.corrugateCost = corrugateCost;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		/*result = prime
				* result
				+ ((corrugateDueDate == null) ? 0 : corrugateDueDate.hashCode());*/
		result = prime
				* result
				+ ((displayId == null) ? 0 : displayId.hashCode());
		result = prime * result
				+ ((corrugateId == null) ? 0 : corrugateId.hashCode());
		result = prime * result
				+ ((createDate == null) ? 0 : createDate.hashCode());
		/*result = prime * result
				+ ((deliveryDate == null) ? 0 : deliveryDate.hashCode());*/
		result = prime * result
				+ ((corrugateCost == null) ? 0 : corrugateCost.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Corrugate other = (Corrugate) obj;
		/*if (corrugateDueDate == null) {
			if (other.corrugateDueDate != null)
				return false;
		} else if (!corrugateDueDate.equals(other.corrugateDueDate))
			return false;*/
		if (corrugateId == null) {
			if (other.corrugateId != null)
				return false;
		} else if (!corrugateId.equals(other.corrugateId))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		/*if (deliveryDate == null) {
			if (other.deliveryDate != null)
				return false;
		} else if (!deliveryDate.equals(other.deliveryDate))
			return false;*/
		if (corrugateCost == null) {
			if (other.corrugateCost != null)
				return false;
		} else if (!corrugateCost.equals(other.corrugateCost))
			return false;
		if (displayId == null) {
			if (other.displayId != null)
				return false;
		} else if (!displayId.equals(other.displayId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		/*return "Corrugate [corrugateDueDate=" + corrugateDueDate
				+ ", corrugateId=" + corrugateId + ", createDate=" + createDate
				+ ", corrugateCost=" + corrugateCost + ", displayId=" + displayId
				+ ", deliveryDate=" + deliveryDate + "]";*/
		return "Corrugate [" 
		+ " corrugateId=" + corrugateId + ", createDate=" + createDate
		+ ", corrugateCost=" + corrugateCost + ", displayId=" + displayId
		+ "]";
	}
	
	

}
