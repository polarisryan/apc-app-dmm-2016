package com.averydennison.dmm.app.server.persistence;

import java.util.Comparator;

public class DisplayShipWaveComparator implements Comparator<DisplayShipWave> {

	@Override
	public int compare(DisplayShipWave sw1, DisplayShipWave sw2) {
		return sw2.getMustArriveDate().compareTo(sw1.getMustArriveDate());
	}

}
