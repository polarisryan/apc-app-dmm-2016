package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

/**
 * User: Mari
 * Date: Aug 10, 2010
 * Time: 01:41:48 AM
 */
/*
 * IMAGE_ID	NUMBER(10,0)
DISPLAY_COST_ID	NUMBER(10,0)
DISPLAY_ID	NUMBER(10,0)
CREATE_DATE	DATE
URL	VARCHAR2(100 BYTE)
IMAGE_NAME	VARCHAR2(50 BYTE)
DESCRIPTION	VARCHAR2(200 BYTE)
 */
public class DisplayCostImage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long imageId;
	private Long displayId;
	private Long DisplayCostId ;
	private Date createDate;
	private String url;
	private String imageName;
	private String description;
	private Long displayScenarioId;
	public Long getImageId() {
		return imageId;
	}
	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}
	public Long getDisplayId() {
		return displayId;
	}
	public void setDisplayId(Long displayId) {
		this.displayId = displayId;
	}
	public Long getDisplayCostId() {
		return DisplayCostId;
	}
	public void setDisplayCostId(Long displayCostId) {
		DisplayCostId = displayCostId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getDisplayScenarioId() {
		return displayScenarioId;
	}
	public void setDisplayScenarioId(Long displayScenarioId) {
		this.displayScenarioId = displayScenarioId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
		+ ((DisplayCostId == null) ? 0 : DisplayCostId.hashCode());
		result = prime * result
		+ ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result
		+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((displayId == null) ? 0 : displayId.hashCode());
		result = prime * result + ((imageId == null) ? 0 : imageId.hashCode());
		result = prime * result + ((imageName == null) ? 0 : imageName.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		result = prime * result + ((displayScenarioId == null) ? 0 : displayScenarioId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DisplayCostImage other = (DisplayCostImage) obj;
		if (DisplayCostId == null) {
			if (other.DisplayCostId != null)
				return false;
		} else if (!DisplayCostId.equals(other.DisplayCostId))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (displayId == null) {
			if (other.displayId != null)
				return false;
		} else if (!displayId.equals(other.displayId))
			return false;
		if (imageId == null) {
			if (other.imageId != null)
				return false;
		} else if (!imageId.equals(other.imageId))
			return false;
		if (imageName == null) {
			if (other.imageName != null)
				return false;
		} else if (!imageName.equals(other.imageName))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		if (displayScenarioId == null) {
			if (other.displayScenarioId != null)
				return false;
		} else if (!displayScenarioId.equals(other.displayScenarioId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "DisplayCostImage [DisplayCostId=" + DisplayCostId + ", createDate="
		+ createDate + ", description=" + description + ", displayId="
		+ displayId + ", imageId=" + imageId + ", imageName=" + imageName
		+ ", displayScenarioId=" + displayScenarioId
		+ ", url=" + url + "]";
	}



}
