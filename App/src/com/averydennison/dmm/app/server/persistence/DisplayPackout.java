package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Nov 30, 2009
 * Time: 5:14:45 PM
 */
public class DisplayPackout implements Serializable {
    private Long displayPackoutId;

    public Long getDisplayPackoutId() {
        return displayPackoutId;
    }

    public void setDisplayPackoutId(Long displayPackoutId) {
        this.displayPackoutId = displayPackoutId;
    }

    private Long displayShipWaveId;

    public Long getDisplayShipWaveId() {
        return displayShipWaveId;
    }

    public void setDisplayShipWaveId(Long displayShipWaveId) {
        this.displayShipWaveId = displayShipWaveId;
    }

    private Long displayDcId;

    public Long getDisplayDcId() {
        return displayDcId;
    }

    public void setDisplayDcId(Long displayDcId) {
        this.displayDcId = displayDcId;
    }

    private Date productDueDate;

    public Date getProductDueDate() {
        return productDueDate;
    }

    public void setProductDueDate(Date productDueDate) {
        this.productDueDate = productDueDate;
    }

    private Date shipLocProductDueDate;

    public Date getShipLocProductDueDate() {
        return shipLocProductDueDate;
    }

    public void setShipLocProductDueDate(Date shipLocProductDueDate) {
        this.shipLocProductDueDate = shipLocProductDueDate;
    }
    
    private Date corrugateDueDate;

    public Date getCorrugateDueDate() {
        return corrugateDueDate;
    }

    public void setCorrugateDueDate(Date corrugateDueDate) {
        this.corrugateDueDate = corrugateDueDate;
    }

    private Date deliveryDate;

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
    
    private Long quantity;

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    private Date dtCreated;

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    private Date dtLastChanged;

    public Date getDtLastChanged() {
        return dtLastChanged;
    }

    public void setDtLastChanged(Date dtLastChanged) {
        this.dtLastChanged = dtLastChanged;
    }

    private String userCreated;

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    private String userLastChanged;

    public String getUserLastChanged() {
        return userLastChanged;
    }

    public void setUserLastChanged(String userLastChanged) {
        this.userLastChanged = userLastChanged;
    }

    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisplayPackout that = (DisplayPackout) o;

        if (displayDcId != null ? !displayDcId.equals(that.displayDcId) : that.displayDcId != null) return false;
        if (displayPackoutId != null ? !displayPackoutId.equals(that.displayPackoutId) : that.displayPackoutId != null)
            return false;
        if (displayShipWaveId != null ? !displayShipWaveId.equals(that.displayShipWaveId) : that.displayShipWaveId != null)
            return false;
        if (dtCreated != null ? !dtCreated.equals(that.dtCreated) : that.dtCreated != null) return false;
        if (dtLastChanged != null ? !dtLastChanged.equals(that.dtLastChanged) : that.dtLastChanged != null)
            return false;
        if (productDueDate != null ? !productDueDate.equals(that.productDueDate) : that.productDueDate != null)
            return false;
        if (shipLocProductDueDate != null ? !shipLocProductDueDate.equals(that.shipLocProductDueDate) : that.shipLocProductDueDate != null)
            return false;
        if (quantity != null ? !quantity.equals(that.quantity) : that.quantity != null) return false;
        if (userCreated != null ? !userCreated.equals(that.userCreated) : that.userCreated != null) return false;
        if (userLastChanged != null ? !userLastChanged.equals(that.userLastChanged) : that.userLastChanged != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = displayPackoutId != null ? displayPackoutId.hashCode() : 0;
        result = 31 * result + (displayShipWaveId != null ? displayShipWaveId.hashCode() : 0);
        result = 31 * result + (displayDcId != null ? displayDcId.hashCode() : 0);
        result = 31 * result + (productDueDate != null ? productDueDate.hashCode() : 0);
        result = 31 * result + (shipLocProductDueDate != null ? shipLocProductDueDate.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        result = 31 * result + (dtCreated != null ? dtCreated.hashCode() : 0);
        result = 31 * result + (dtLastChanged != null ? dtLastChanged.hashCode() : 0);
        result = 31 * result + (userCreated != null ? userCreated.hashCode() : 0);
        result = 31 * result + (userLastChanged != null ? userLastChanged.hashCode() : 0);
        return result;
    }

    private DisplayShipWave displayShipWaveByDisplayShipWaveId;

    public DisplayShipWave getDisplayShipWaveByDisplayShipWaveId() {
        return displayShipWaveByDisplayShipWaveId;
    }

    public void setDisplayShipWaveByDisplayShipWaveId(DisplayShipWave displayShipWaveByDisplayShipWaveId) {
        this.displayShipWaveByDisplayShipWaveId = displayShipWaveByDisplayShipWaveId;
    }

    private DisplayDc displayDcByDisplayDcId;

    public DisplayDc getDisplayDcByDisplayDcId() {
        return displayDcByDisplayDcId;
    }

    public void setDisplayDcByDisplayDcId(DisplayDc displayDcByDisplayDcId) {
        this.displayDcByDisplayDcId = displayDcByDisplayDcId;
    }
    private Date shipFromDate;

    public Date getShipFromDate() {
        return shipFromDate;
    }

    public void setShipFromDate(Date shipFromDate) {
        this.shipFromDate = shipFromDate;
    }
    
    private Long shipFromLocationId;
    
    public Long getShipFromLocationId() {
		return shipFromLocationId;
	}

	public void setShipFromLocationId(Long shipFromLocationId) {
		this.shipFromLocationId = shipFromLocationId;
	}

	private Location shipFromLocationByLocationId;

	public Location getShipFromLocationByLocationId() {
		return shipFromLocationByLocationId;
	}

	public void setShipFromLocationByLocationId(
			Location shipFromLocationByLocationId) {
		this.shipFromLocationByLocationId = shipFromLocationByLocationId;
	}

	@Override
    public String toString() {
        return "DisplayPackout{" +
                "displayPackoutId=" + displayPackoutId +
                ", displayShipWaveId=" + displayShipWaveId +
                ", displayDcId=" + displayDcId +
                ", productDueDate=" + productDueDate +
                ", quantity=" + quantity +
                ", dtCreated=" + dtCreated +
                ", dtLastChanged=" + dtLastChanged +
                ", userCreated='" + userCreated + '\'' +
                ", userLastChanged='" + userLastChanged + '\'' +
                ", displayShipWaveByDisplayShipWaveId=" + displayShipWaveByDisplayShipWaveId +
                ", displayDcByDisplayDcId=" + displayDcByDisplayDcId +
                ", shipFromDate=" + shipFromDate +
                ", shipFromLocationId=" + shipFromLocationId +
                '}';
    }
}
