package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:31 AM
 */
public class AccessPriv implements Serializable {
    private Long accessId;

    public Long getAccessId() {
        return accessId;
    }

    public void setAccessId(Long accessId) {
        this.accessId = accessId;
    }

    private String accessDescription;

    public String getAccessDescription() {
        return accessDescription;
    }

    public void setAccessDescription(String accessDescription) {
        this.accessDescription = accessDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessPriv that = (AccessPriv) o;

        if (accessDescription != null ? !accessDescription.equals(that.accessDescription) : that.accessDescription != null)
            return false;
        if (accessId != null ? !accessId.equals(that.accessId) : that.accessId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = accessId != null ? accessId.hashCode() : 0;
        result = 31 * result + (accessDescription != null ? accessDescription.hashCode() : 0);
        return result;
    }

    private Collection<UserAccess> userAccessesByAccessId;

    public Collection<UserAccess> getUserAccessesByAccessId() {
        return userAccessesByAccessId;
    }

    public void setUserAccessesByAccessId(Collection<UserAccess> userAccessesByAccessId) {
        this.userAccessesByAccessId = userAccessesByAccessId;
    }
}
