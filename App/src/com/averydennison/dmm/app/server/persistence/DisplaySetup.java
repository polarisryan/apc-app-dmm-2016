package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:50 AM
 */
public class DisplaySetup implements Serializable {
    private Long displaySetupId;

    public Long getDisplaySetupId() {
        return displaySetupId;
    }

    public void setDisplaySetupId(Long displaySetupId) {
        this.displaySetupId = displaySetupId;
    }

    private String displaySetupDesc;

    public String getDisplaySetupDesc() {
        return displaySetupDesc;
    }

    public void setDisplaySetupDesc(String displaySetupDesc) {
        this.displaySetupDesc = displaySetupDesc;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisplaySetup displaySetup = (DisplaySetup) o;

        if (displaySetupDesc != null ? !displaySetupDesc.equals(displaySetup.displaySetupDesc) : displaySetup.displaySetupDesc != null)
            return false;
        if (displaySetupId != null ? !displaySetupId.equals(displaySetup.displaySetupId) : displaySetup.displaySetupId != null)
            return false;
        if (activeFlg != null ? !activeFlg.equals(displaySetup.activeFlg) : displaySetup.activeFlg != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = displaySetupId != null ? displaySetupId.hashCode() : 0;
        result = 31 * result + (displaySetupDesc != null ? displaySetupDesc.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }

    private Collection<Display> displaysByDisplaySetupId;

    public Collection<Display> getDisplaysByDisplaySetupId() {
        return displaysByDisplaySetupId;
    }

    public void setDisplaysByDisplaySetupId(Collection<Display> displaysByDisplaySetupId) {
        this.displaysByDisplaySetupId = displaysByDisplaySetupId;
    }

    @Override
    public String toString() {
        return "DisplaySetup{" +
                "displaySetupId=" + displaySetupId +
                ", displaySetupDesc='" + displaySetupDesc + '\'' +
                '}';
    }
}
