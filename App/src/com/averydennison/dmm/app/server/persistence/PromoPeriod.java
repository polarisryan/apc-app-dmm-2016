package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:49 AM
 */
public class PromoPeriod implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7221265409142428969L;
	private Long promoPeriodId;

    public Long getPromoPeriodId() {
        return promoPeriodId;
    }

    public void setPromoPeriodId(Long promoPeriodId) {
        this.promoPeriodId = promoPeriodId;
    }

    private String promoPeriodName;

    public String getPromoPeriodName() {
        return promoPeriodName;
    }

    public void setPromoPeriodName(String promoPeriodName) {
        this.promoPeriodName = promoPeriodName;
    }

    private String promoPeriodDescription;

    public String getPromoPeriodDescription() {
        return promoPeriodDescription;
    }

    public void setPromoPeriodDescription(String promoPeriodDescription) {
        this.promoPeriodDescription = promoPeriodDescription;
    }

    private Date promoDate;

    public Date getPromoDate() {
        return promoDate;
    }

    public void setPromoDate(Date promoDate) {
        this.promoDate = promoDate;
    }
    
    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromoPeriod that = (PromoPeriod) o;

        if (promoDate != null ? !promoDate.equals(that.promoDate) : that.promoDate != null) return false;
        if (promoPeriodDescription != null ? !promoPeriodDescription.equals(that.promoPeriodDescription) : that.promoPeriodDescription != null)
            return false;
        if (promoPeriodId != null ? !promoPeriodId.equals(that.promoPeriodId) : that.promoPeriodId != null)
            return false;
        if (promoPeriodName != null ? !promoPeriodName.equals(that.promoPeriodName) : that.promoPeriodName != null)
            return false;
        if (activeFlg != null ? !activeFlg.equals(that.activeFlg) : that.activeFlg != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = promoPeriodId != null ? promoPeriodId.hashCode() : 0;
        result = 31 * result + (promoPeriodName != null ? promoPeriodName.hashCode() : 0);
        result = 31 * result + (promoPeriodDescription != null ? promoPeriodDescription.hashCode() : 0);
        result = 31 * result + (promoDate != null ? promoDate.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }

    private Collection<Display> displaysByPromoPeriodId;

    public Collection<Display> getDisplaysByPromoPeriodId() {
        return displaysByPromoPeriodId;
    }

    public void setDisplaysByPromoPeriodId(Collection<Display> displaysByPromoPeriodId) {
        this.displaysByPromoPeriodId = displaysByPromoPeriodId;
    }
}
