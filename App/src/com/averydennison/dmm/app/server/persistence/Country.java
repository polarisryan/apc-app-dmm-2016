package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

/**
 * User: Mari
 * Date: Jan 7, 2011
 * Time: 04:59:23 PM
 */
public class Country implements Serializable {
    private Long countryId;
    private String countryCode;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    private String countryName;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }
    
    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }

    private Boolean activeCountryFlg;

   public Boolean isActiveCountryFlg() {
		return activeCountryFlg;
	}

	public void setActiveCountryFlg(Boolean activeCountryFlg) {
		this.activeCountryFlg = activeCountryFlg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((activeFlg == null) ? 0 : activeFlg.hashCode());
		result = prime * result
		+ ((activeCountryFlg == null) ? 0 : activeCountryFlg.hashCode());
		result = prime * result
				+ ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result
				+ ((countryId == null) ? 0 : countryId.hashCode());
		result = prime * result
				+ ((countryName == null) ? 0 : countryName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (activeFlg == null) {
			if (other.activeFlg != null)
				return false;
		} else if (!activeFlg.equals(other.activeFlg))
			return false;
		if (activeCountryFlg == null) {
			if (other.activeCountryFlg != null)
				return false;
		} else if (!activeCountryFlg.equals(other.activeCountryFlg))
			return false;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (countryId == null) {
			if (other.countryId != null)
				return false;
		} else if (!countryId.equals(other.countryId))
			return false;
		if (countryName == null) {
			if (other.countryName != null)
				return false;
		} else if (!countryName.equals(other.countryName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Country [activeFlg=" + activeFlg + ", countryCode="
				+ countryCode + ", countryId=" + countryId + ", countryName="
				+ countryName +  ", activeCountryFlg=" +activeCountryFlg +"]";
	}
}

   