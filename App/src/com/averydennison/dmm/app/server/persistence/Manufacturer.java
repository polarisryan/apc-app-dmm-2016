package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:49 AM
 */
public class Manufacturer implements Serializable {
    private Long manufacturerId;

    public Long getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Manufacturer that = (Manufacturer) o;

        if (manufacturerId != null ? !manufacturerId.equals(that.manufacturerId) : that.manufacturerId != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (activeFlg != null ? !activeFlg.equals(that.activeFlg) : that.activeFlg != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = manufacturerId != null ? manufacturerId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }

    private Collection<Display> displaysByManufacturerId;

    public Collection<Display> getDisplaysByManufacturerId() {
        return displaysByManufacturerId;
    }

    public void setDisplaysByManufacturerId(Collection<Display> displaysByManufacturerId) {
        this.displaysByManufacturerId = displaysByManufacturerId;
    }

    private Collection<DisplayMaterial> displayMaterialsByManufacturerId;

    public Collection<DisplayMaterial> getDisplayMaterialsByManufacturerId() {
        return displayMaterialsByManufacturerId;
    }

    public void setDisplayMaterialsByManufacturerId(Collection<DisplayMaterial> displayMaterialsByManufacturerId) {
        this.displayMaterialsByManufacturerId = displayMaterialsByManufacturerId;
    }
    
	private Set<Country> countries = new HashSet<Country>(0);

	public void setCountries(Set<Country> countries) {
		this.countries = countries;
	}
	
	public Set<Country> getCountries() {
		return countries;
	}
	
}
