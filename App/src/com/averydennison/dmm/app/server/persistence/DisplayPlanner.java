package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:43 AM
 */
public class DisplayPlanner implements Serializable {
    private Long displayPlannerId;

    public Long getDisplayPlannerId() {
        return displayPlannerId;
    }

    public void setDisplayPlannerId(Long displayPlannerId) {
        this.displayPlannerId = displayPlannerId;
    }

    private Long kitComponentId;

    public Long getKitComponentId() {
        return kitComponentId;
    }

    public void setKitComponentId(Long kitComponentId) {
        this.kitComponentId = kitComponentId;
    }

    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String initials;

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    private String plannerCode;

    public String getPlannerCode() {
        return plannerCode;
    }

    public void setPlannerCode(String plannerCode) {
        this.plannerCode = plannerCode;
    }

    private String plantCode;

    public String getPlantCode() {
        return plantCode;
    }

    public void setPlantCode(String plantCode) {
        this.plantCode = plantCode;
    }

    private String leadTime;

    public String getLeadTime() {
        return leadTime;
    }

    public void setLeadTime(String leadTime) {
        this.leadTime = leadTime;
    }

    private Boolean plantOverrideFlg;

    public Boolean isPlantOverrideFlg() {
        return plantOverrideFlg;
    }

    public void setPlantOverrideFlg(Boolean plantOverrideFlg) {
        this.plantOverrideFlg = plantOverrideFlg;
    }

    private Long displayDcId;

    public Long getDisplayDcId() {
        return displayDcId;
    }

    public void setDisplayDcId(Long displayDcId) {
        this.displayDcId = displayDcId;
    }

    private Date dtCreated;

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    private Date dtLastChanged;

    public Date getDtLastChanged() {
        return dtLastChanged;
    }

    public void setDtLastChanged(Date dtLastChanged) {
        this.dtLastChanged = dtLastChanged;
    }

    private String userCreated;

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    private String userLastChanged;

    public String getUserLastChanged() {
        return userLastChanged;
    }

    public void setUserLastChanged(String userLastChanged) {
        this.userLastChanged = userLastChanged;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisplayPlanner that = (DisplayPlanner) o;

        if (displayPlannerId != null ? !displayPlannerId.equals(that.displayPlannerId) : that.displayPlannerId != null)
            return false;
        if (kitComponentId != null ? !kitComponentId.equals(that.kitComponentId) : that.kitComponentId != null)
            return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (initials != null ? !initials.equals(that.initials) : that.initials != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (plannerCode != null ? !plannerCode.equals(that.plannerCode) : that.plannerCode != null) return false;
        if (plantCode != null ? !plantCode.equals(that.plantCode) : that.plantCode != null) return false;
        if (leadTime != null ? !leadTime.equals(that.leadTime) : that.leadTime != null) return false;
        if (plantOverrideFlg != null ? !plantOverrideFlg.equals(that.plantOverrideFlg) : that.plantOverrideFlg != null) return false;
        if (displayDcId != null ? !displayDcId.equals(that.displayDcId) : that.displayDcId != null)
            return false;
        if (dtCreated != null ? !dtCreated.equals(that.dtCreated) : that.dtCreated != null) return false;
        if (dtLastChanged != null ? !dtLastChanged.equals(that.dtLastChanged) : that.dtLastChanged != null)
            return false;
        if (userCreated != null ? !userCreated.equals(that.userCreated) : that.userCreated != null) return false;
        if (userLastChanged != null ? !userLastChanged.equals(that.userLastChanged) : that.userLastChanged != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = displayPlannerId != null ? displayPlannerId.hashCode() : 0;
        result = 31 * result + (kitComponentId != null ? kitComponentId.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (initials != null ? initials.hashCode() : 0);
        result = 31 * result + (plannerCode != null ? plannerCode.hashCode() : 0);
        result = 31 * result + (plantCode != null ? plantCode.hashCode() : 0);
        result = 31 * result + (leadTime != null ? leadTime.hashCode() : 0);
        result = 31 * result + (plantOverrideFlg != null ? plantOverrideFlg.hashCode() : 0);
        result = 31 * result + (displayDcId != null ? displayDcId.hashCode() : 0);        
        result = 31 * result + (dtCreated != null ? dtCreated.hashCode() : 0);
        result = 31 * result + (dtLastChanged != null ? dtLastChanged.hashCode() : 0);
        result = 31 * result + (userCreated != null ? userCreated.hashCode() : 0);
        result = 31 * result + (userLastChanged != null ? userLastChanged.hashCode() : 0);
        return result;
    }

    private Collection<KitComponent> kitComponentsByDisplayPlannerId;

    public Collection<KitComponent> getKitComponentsByDisplayPlannerId() {
        return kitComponentsByDisplayPlannerId;
    }

    public void setKitComponentsByDisplayPlannerId(Collection<KitComponent> kitComponentsByDisplayPlannerId) {
        this.kitComponentsByDisplayPlannerId = kitComponentsByDisplayPlannerId;
    }
}
