package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:36 AM
 */
public class DisplayCost implements Serializable {
    /**
	 * 
	 */

	private Long displayCostId;

    public Long getDisplayCostId() {
        return displayCostId;
    }

    public void setDisplayCostId(Long displayCostId) {
        this.displayCostId = displayCostId;
    }

    private Long displayScenarioId;

    public Long getDisplayScenarioId() {
        return displayScenarioId;
    }

    public void setDisplayScenarioId(Long displayScenarioId) {
        this.displayScenarioId = displayScenarioId;
    }

    
    private Long displayId;

    public Long getDisplayId() {
        return displayId;
    }

    public void setDisplayId(Long displayId) {
        this.displayId = displayId;
    }

    private Float displayMaterialCost;

    public Float getDisplayMaterialCost() {
        return displayMaterialCost;
    }

    public void setDisplayMaterialCost(Float displayMaterialCost) {
        this.displayMaterialCost = displayMaterialCost;
    }

    private Float fullfilmentCost;

    public Float getFullfilmentCost() {
        return fullfilmentCost;
    }

    public void setFullfilmentCost(Float fullfilmentCost) {
        this.fullfilmentCost = fullfilmentCost;
    }

    private Float ctpCost;

    public Float getCtpCost() {
        return ctpCost;
    }

    public void setCtpCost(Float ctpCost) {
        this.ctpCost = ctpCost;
    }

    private Float dieCuttingCost;

    public Float getDieCuttingCost() {
        return dieCuttingCost;
    }

    public void setDieCuttingCost(Float dieCuttingCost) {
        this.dieCuttingCost = dieCuttingCost;
    }

    private Float printingPlateCost;

    public Float getPrintingPlateCost() {
        return printingPlateCost;
    }

    public void setPrintingPlateCost(Float printingPlateCost) {
        this.printingPlateCost = printingPlateCost;
    }

    private Float totalArtworkCost;

    public Float getTotalArtworkCost() {
        return totalArtworkCost;
    }

    public void setTotalArtworkCost(Float totalArtworkCost) {
        this.totalArtworkCost = totalArtworkCost;
    }

    private Float shipTestCost;

    public Float getShipTestCost() {
        return shipTestCost;
    }

    public void setShipTestCost(Float shipTestCost) {
        this.shipTestCost = shipTestCost;
    }

    private Float palletCost;

    public Float getPalletCost() {
        return palletCost;
    }

    public void setPalletCost(Float palletCost) {
        this.palletCost = palletCost;
    }

    private Float miscCost;

    public Float getMiscCost() {
        return miscCost;
    }

    public void setMiscCost(Float miscCost) {
        this.miscCost = miscCost;
    }

    private Float corrugateCost;

    public Float getCorrugateCost() {
        return corrugateCost;
    }

    public void setCorrugateCost(Float corrugateCost) {
        this.corrugateCost = corrugateCost;
    }

    private Long overageScrapPct;

    public Long getOverageScrapPct() {
        return overageScrapPct;
    }

    public void setOverageScrapPct(Long overageScrapPct) {
        this.overageScrapPct = overageScrapPct;
    }

    private Float customerProgramPct;

    public Float getCustomerProgramPct() {
        return customerProgramPct;
    }

    public void setCustomerProgramPct(Float customerProgramPct) {
        this.customerProgramPct = customerProgramPct;
    }
    private String sscidProjectNum;

    public String getSscidProjectNum() {
        return sscidProjectNum;
    }

    public void setSscidProjectNum(String sscidProjectNum) {
        this.sscidProjectNum = sscidProjectNum;
    }

    private String costCenter;

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    private Date dtCreated;

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    private Date dtLastChanged;

    public Date getDtLastChanged() {
        return dtLastChanged;
    }

    public void setDtLastChanged(Date dtLastChanged) {
        this.dtLastChanged = dtLastChanged;
    }

    private String userCreated;

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    private String userLastChanged;

    public String getUserLastChanged() {
        return userLastChanged;
    }

    public void setUserLastChanged(String userLastChanged) {
        this.userLastChanged = userLastChanged;
    }

    private Float expeditedFreightCost;
    			  
    public Float getExpeditedFreightCost() {
        return expeditedFreightCost;
    }

    public void setExpeditedFreightCost(Float expeditedFreightCost) {
        this.expeditedFreightCost = expeditedFreightCost;
    }
    
    private Float fines;
    
    public Float getFines() {
        return fines;
    }

    public void setFines(Float fines) {
        this.fines = fines;
    }
    
    private Date caDate;
    
    public Date getCaDate() {
        return caDate;
    }

    public void setCaDate(Date caDate) {
        this.caDate = caDate;
    }
    
    private Float discountPct;
    private Float mdfPct;
    
    
    public Float getDiscountPct() {
		return discountPct;
	}

	public void setDiscountPct(Float discountPct) {
		this.discountPct = discountPct;
	}

	public Float getMdfPct() {
		return mdfPct;
	}

	public void setMdfPct(Float mdfPct) {
		this.mdfPct = mdfPct;
	}
	
	private Float tradeSalesChanged;
	public Float getTradeSalesChanged() {
		return tradeSalesChanged;
	}

	public void setTradeSalesChanged(Float tradeSalesChanged) {
		this.tradeSalesChanged = tradeSalesChanged;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DisplayCost that = (DisplayCost) o;
        if (corrugateCost != null ? !corrugateCost.equals(that.corrugateCost) : that.corrugateCost != null) return false;
        if (costCenter != null ? !costCenter.equals(that.costCenter) : that.costCenter != null) return false;
        if (ctpCost != null ? !ctpCost.equals(that.ctpCost) : that.ctpCost != null) return false;
        if (customerProgramPct != null ? !customerProgramPct.equals(that.customerProgramPct) : that.customerProgramPct != null) return false;
        if (dieCuttingCost != null ? !dieCuttingCost.equals(that.dieCuttingCost) : that.dieCuttingCost != null)
            return false;
        if (displayCostId != null ? !displayCostId.equals(that.displayCostId) : that.displayCostId != null)
            return false;
        if (displayId != null ? !displayId.equals(that.displayId) : that.displayId != null) return false;
        if (displayMaterialCost != null ? !displayMaterialCost.equals(that.displayMaterialCost) : that.displayMaterialCost != null)
            return false;
        if (dtCreated != null ? !dtCreated.equals(that.dtCreated) : that.dtCreated != null) return false;
        if (dtLastChanged != null ? !dtLastChanged.equals(that.dtLastChanged) : that.dtLastChanged != null)
            return false;
        if (fullfilmentCost != null ? !fullfilmentCost.equals(that.fullfilmentCost) : that.fullfilmentCost != null)
            return false;
        if (miscCost != null ? !miscCost.equals(that.miscCost) : that.miscCost != null) return false;
        if (overageScrapPct != null ? !overageScrapPct.equals(that.overageScrapPct) : that.overageScrapPct != null)
            return false;
        if (palletCost != null ? !palletCost.equals(that.palletCost) : that.palletCost != null) return false;
        if (printingPlateCost != null ? !printingPlateCost.equals(that.printingPlateCost) : that.printingPlateCost != null)
            return false;
        if (shipTestCost != null ? !shipTestCost.equals(that.shipTestCost) : that.shipTestCost != null) return false;
        if (sscidProjectNum != null ? !sscidProjectNum.equals(that.sscidProjectNum) : that.sscidProjectNum != null)
            return false;
        if (totalArtworkCost != null ? !totalArtworkCost.equals(that.totalArtworkCost) : that.totalArtworkCost != null)
            return false;
        if (userCreated != null ? !userCreated.equals(that.userCreated) : that.userCreated != null) return false;
        if (userLastChanged != null ? !userLastChanged.equals(that.userLastChanged) : that.userLastChanged != null) return false;
        if (mdfPct != null ? !mdfPct.equals(that.mdfPct) : that.mdfPct != null) return false;
        if (discountPct != null ? !discountPct.equals(that.discountPct) : that.discountPct != null) return false;
        if (tradeSalesChanged != null ? !tradeSalesChanged.equals(that.tradeSalesChanged) : that.tradeSalesChanged != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = displayCostId != null ? displayCostId.hashCode() : 0;
        result = 31 * result + (displayId != null ? displayId.hashCode() : 0);
        result = 31 * result + (displayMaterialCost != null ? displayMaterialCost.hashCode() : 0);
        result = 31 * result + (fullfilmentCost != null ? fullfilmentCost.hashCode() : 0);
        result = 31 * result + (ctpCost != null ? ctpCost.hashCode() : 0);
        result = 31 * result + (dieCuttingCost != null ? dieCuttingCost.hashCode() : 0);
        result = 31 * result + (printingPlateCost != null ? printingPlateCost.hashCode() : 0);
        result = 31 * result + (totalArtworkCost != null ? totalArtworkCost.hashCode() : 0);
        result = 31 * result + (shipTestCost != null ? shipTestCost.hashCode() : 0);
        result = 31 * result + (palletCost != null ? palletCost.hashCode() : 0);
        result = 31 * result + (miscCost != null ? miscCost.hashCode() : 0);
        result = 31 * result + (corrugateCost != null ? corrugateCost.hashCode() : 0);
        result = 31 * result + (overageScrapPct != null ? overageScrapPct.hashCode() : 0);
        result = 31 * result + (customerProgramPct != null ? customerProgramPct.hashCode() : 0);
        result = 31 * result + (sscidProjectNum != null ? sscidProjectNum.hashCode() : 0);
        result = 31 * result + (costCenter != null ? costCenter.hashCode() : 0);
        result = 31 * result + (dtCreated != null ? dtCreated.hashCode() : 0);
        result = 31 * result + (dtLastChanged != null ? dtLastChanged.hashCode() : 0);
        result = 31 * result + (userCreated != null ? userCreated.hashCode() : 0);
        result = 31 * result + (userLastChanged != null ? userLastChanged.hashCode() : 0);
        result = 31 * result + (mdfPct != null ? mdfPct.hashCode() : 0);
        result = 31 * result + (discountPct != null ? discountPct.hashCode() : 0);
        result = 31 * result + (tradeSalesChanged != null ? tradeSalesChanged.hashCode() : 0);
        return result;
    }

    private Display displayByDisplayId;

    public Display getDisplayByDisplayId() {
        return displayByDisplayId;
    }

    public void setDisplayByDisplayId(Display displayByDisplayId) {
        this.displayByDisplayId = displayByDisplayId;
    }
}
