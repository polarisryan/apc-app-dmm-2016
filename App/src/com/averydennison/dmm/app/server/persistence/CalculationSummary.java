package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
/**
 * User: Mari
 * Date: Apr 29, 2011
 * Time: 10:51:43 AM
 */
public class CalculationSummary implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Long calculationSummaryId;
	Long displayId;
	Long displayScenarioId;
	Long foreCastQty;			
	Long actualQty;			
	Float sumCorrugateCost;			
	Float sumFulFillmentCost;			
	Float sumOtherCost;			
	Float sumTotDispCostNoShipping;			
	Float sumTotDisplayCostShipping;			
	Float shippingCost;			
	Float sumTradeSales;			
	Float custProgDisc;			
	Float sumNetSalesBp;			
	Float sumNetSalesAp;			
	Float sumCogs;			
	Float sumProdVarMargin;			
	Float sumDisplayCost;			
	Float sumVmDollarBp;			
	Float sumVmDollarAp;			
	Float sumVmPctBp; 			
	Float sumVmPctAp;			
	Float vmDollarHurdleRed;			
	Float vmDollarHurdleGreen;			
	Float vmPctHurdleRed;			
	Float vmPctHurdleGreen;			
	Long statusId;		
	Long promoPeriodId;			
	Long customerId;			
	String sku;			
	String description;			
	String averySku;			
	Long qtyPerFacing;			
	Long numberFacing;			
	Long qtyPerDisplay;			
	Float sumTotActualInvoice;			
	String hurdles;
	Float excessInvtVmDollar;		
	Float excessInvtVmPct;	
	Float corrugateCostPerUnit;		
	Float laborCostPerUnit;	
	Float mdfPct;		
	Float discountPct;	
	
	
	public Float getExcessInvtVmDollar() {
		return excessInvtVmDollar;
	}
	public void setExcessInvtVmDollar(Float excessInvtVmDollar) {
		this.excessInvtVmDollar = excessInvtVmDollar;
	}
	public Float getExcessInvtVmPct() {
		return excessInvtVmPct;
	}
	public void setExcessInvtVmPct(Float excessInvtVmPct) {
		this.excessInvtVmPct = excessInvtVmPct;
	}
	public Long getCalculationSummaryId() {
		return calculationSummaryId;
	}
	public void setCalculationSummaryId(Long calculationSummaryId) {
		this.calculationSummaryId = calculationSummaryId;
	}
	public Long getDisplayId() {
		return displayId;
	}
	public void setDisplayId(Long displayId) {
		this.displayId = displayId;
	}
	public Long getDisplayScenarioId() {
		return displayScenarioId;
	}
	public void setDisplayScenarioId(Long displayScenarioId) {
		this.displayScenarioId = displayScenarioId;
	}
	public Long getForeCastQty() {
		return foreCastQty;
	}
	public void setForeCastQty(Long foreCastQty) {
		this.foreCastQty = foreCastQty;
	}
	public Long getActualQty() {
		return actualQty;
	}
	public void setActualQty(Long actualQty) {
		this.actualQty = actualQty;
	}
	public Float getSumCorrugateCost() {
		return sumCorrugateCost;
	}
	public void setSumCorrugateCost(Float sumCorrugateCost) {
		this.sumCorrugateCost = sumCorrugateCost;
	}
	public Float getSumFulFillmentCost() {
		return sumFulFillmentCost;
	}
	public void setSumFulFillmentCost(Float sumFulFillmentCost) {
		this.sumFulFillmentCost = sumFulFillmentCost;
	}
	public Float getSumOtherCost() {
		return sumOtherCost;
	}
	public void setSumOtherCost(Float sumOtherCost) {
		this.sumOtherCost = sumOtherCost;
	}
	public Float getSumTotDispCostNoShipping() {
		return sumTotDispCostNoShipping;
	}
	public void setSumTotDispCostNoShipping(Float sumTotDispCostNoShipping) {
		this.sumTotDispCostNoShipping = sumTotDispCostNoShipping;
	}
	public Float getSumTotDisplayCostShipping() {
		return sumTotDisplayCostShipping;
	}
	public void setSumTotDisplayCostShipping(Float sumTotDisplayCostShipping) {
		this.sumTotDisplayCostShipping = sumTotDisplayCostShipping;
	}
	public Float getShippingCost() {
		return shippingCost;
	}
	public void setShippingCost(Float shippingCost) {
		this.shippingCost = shippingCost;
	}
	public Float getSumTradeSales() {
		return sumTradeSales;
	}
	public void setSumTradeSales(Float sumTradeSales) {
		this.sumTradeSales = sumTradeSales;
	}
	public Float getCustProgDisc() {
		return custProgDisc;
	}
	public void setCustProgDisc(Float custProgDisc) {
		this.custProgDisc = custProgDisc;
	}
	public Float getSumNetSalesBp() {
		return sumNetSalesBp;
	}
	public void setSumNetSalesBp(Float sumNetSalesBp) {
		this.sumNetSalesBp = sumNetSalesBp;
	}
	public Float getSumNetSalesAp() {
		return sumNetSalesAp;
	}
	public void setSumNetSalesAp(Float sumNetSalesAp) {
		this.sumNetSalesAp = sumNetSalesAp;
	}
	public Float getSumCogs() {
		return sumCogs;
	}
	public void setSumCogs(Float sumCogs) {
		this.sumCogs = sumCogs;
	}
	public Float getSumProdVarMargin() {
		return sumProdVarMargin;
	}
	public void setSumProdVarMargin(Float sumProdVarMargin) {
		this.sumProdVarMargin = sumProdVarMargin;
	}
	public Float getSumDisplayCost() {
		return sumDisplayCost;
	}
	public void setSumDisplayCost(Float sumDisplayCost) {
		this.sumDisplayCost = sumDisplayCost;
	}
	public Float getSumVmDollarBp() {
		return sumVmDollarBp;
	}
	public void setSumVmDollarBp(Float sumVmDollarBp) {
		this.sumVmDollarBp = sumVmDollarBp;
	}
	public Float getSumVmDollarAp() {
		return sumVmDollarAp;
	}
	public void setSumVmDollarAp(Float sumVmDollarAp) {
		this.sumVmDollarAp = sumVmDollarAp;
	}
	public Float getSumVmPctBp() {
		return sumVmPctBp;
	}
	public void setSumVmPctBp(Float sumVmPctBp) {
		this.sumVmPctBp = sumVmPctBp;
	}
	public Float getSumVmPctAp() {
		return sumVmPctAp;
	}
	public void setSumVmPctAp(Float sumVmPctAp) {
		this.sumVmPctAp = sumVmPctAp;
	}
	public Float getVmDollarHurdleRed() {
		return vmDollarHurdleRed;
	}
	public void setVmDollarHurdleRed(Float vmDollarHurdleRed) {
		this.vmDollarHurdleRed = vmDollarHurdleRed;
	}
	public Float getVmDollarHurdleGreen() {
		return vmDollarHurdleGreen;
	}
	public void setVmDollarHurdleGreen(Float vmDollarHurdleGreen) {
		this.vmDollarHurdleGreen = vmDollarHurdleGreen;
	}
	public Float getVmPctHurdleRed() {
		return vmPctHurdleRed;
	}
	public void setVmPctHurdleRed(Float vmPctHurdleRed) {
		this.vmPctHurdleRed = vmPctHurdleRed;
	}
	public Float getVmPctHurdleGreen() {
		return vmPctHurdleGreen;
	}
	public void setVmPctHurdleGreen(Float vmPctHurdleGreen) {
		this.vmPctHurdleGreen = vmPctHurdleGreen;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public Long getPromoPeriodId() {
		return promoPeriodId;
	}
	public void setPromoPeriodId(Long promoPeriodId) {
		this.promoPeriodId = promoPeriodId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAverySku() {
		return averySku;
	}
	public void setAverySku(String averySku) {
		this.averySku = averySku;
	}
	public Long getQtyPerFacing() {
		return qtyPerFacing;
	}
	public void setQtyPerFacing(Long qtyPerFacing) {
		this.qtyPerFacing = qtyPerFacing;
	}
	public Long getNumberFacing() {
		return numberFacing;
	}
	public void setNumberFacing(Long numberFacing) {
		this.numberFacing = numberFacing;
	}
	public Long getQtyPerDisplay() {
		return qtyPerDisplay;
	}
	public void setQtyPerDisplay(Long qtyPerDisplay) {
		this.qtyPerDisplay = qtyPerDisplay;
	}
	public Float getSumTotActualInvoice() {
		return sumTotActualInvoice;
	}
	public void setSumTotActualInvoice(Float sumTotActualInvoice) {
		this.sumTotActualInvoice = sumTotActualInvoice;
	}
	public String getHurdles() {
		return hurdles;
	}
	public void setHurdles(String hurdles) {
		this.hurdles = hurdles;
	}
	public Float getCorrugateCostPerUnit() {
		return corrugateCostPerUnit;
	}
	public void setCorrugateCostPerUnit(Float corrugateCostPerUnit) {
		this.corrugateCostPerUnit = corrugateCostPerUnit;
	}
	public Float getLaborCostPerUnit() {
		return laborCostPerUnit;
	}
	public void setLaborCostPerUnit(Float laborCostPerUnit) {
		this.laborCostPerUnit = laborCostPerUnit;
	}
	
	public Float getMdfPct() {
		return mdfPct;
	}
	public void setMdfPct(Float mdfPct) {
		this.mdfPct = mdfPct;
	}
	public Float getDiscountPct() {
		return discountPct;
	}
	public void setDiscountPct(Float discountPct) {
		this.discountPct = discountPct;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((actualQty == null) ? 0 : actualQty.hashCode());
		result = prime * result
				+ ((averySku == null) ? 0 : averySku.hashCode());
		result = prime
				* result
				+ ((calculationSummaryId == null) ? 0 : calculationSummaryId
						.hashCode());
		result = prime * result
				+ ((custProgDisc == null) ? 0 : custProgDisc.hashCode());
		result = prime * result
				+ ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((displayId == null) ? 0 : displayId.hashCode());
		result = prime
				* result
				+ ((displayScenarioId == null) ? 0 : displayScenarioId
						.hashCode());
		result = prime * result
				+ ((foreCastQty == null) ? 0 : foreCastQty.hashCode());
		result = prime * result + ((hurdles == null) ? 0 : hurdles.hashCode());
		result = prime * result
				+ ((numberFacing == null) ? 0 : numberFacing.hashCode());
		result = prime * result
				+ ((promoPeriodId == null) ? 0 : promoPeriodId.hashCode());
		result = prime * result
				+ ((qtyPerDisplay == null) ? 0 : qtyPerDisplay.hashCode());
		result = prime * result
				+ ((qtyPerFacing == null) ? 0 : qtyPerFacing.hashCode());
		result = prime * result
				+ ((shippingCost == null) ? 0 : shippingCost.hashCode());
		result = prime * result + ((sku == null) ? 0 : sku.hashCode());
		result = prime * result
				+ ((statusId == null) ? 0 : statusId.hashCode());
		result = prime * result + ((sumCogs == null) ? 0 : sumCogs.hashCode());
		result = prime
				* result
				+ ((sumCorrugateCost == null) ? 0 : sumCorrugateCost.hashCode());
		result = prime * result
				+ ((sumDisplayCost == null) ? 0 : sumDisplayCost.hashCode());
		result = prime
				* result
				+ ((sumFulFillmentCost == null) ? 0 : sumFulFillmentCost
						.hashCode());
		result = prime * result
				+ ((sumNetSalesAp == null) ? 0 : sumNetSalesAp.hashCode());
		result = prime * result
				+ ((sumNetSalesBp == null) ? 0 : sumNetSalesBp.hashCode());
		result = prime * result
				+ ((sumOtherCost == null) ? 0 : sumOtherCost.hashCode());
		result = prime
				* result
				+ ((sumProdVarMargin == null) ? 0 : sumProdVarMargin.hashCode());
		result = prime
				* result
				+ ((sumTotActualInvoice == null) ? 0 : sumTotActualInvoice
						.hashCode());
		result = prime
				* result
				+ ((sumTotDispCostNoShipping == null) ? 0
						: sumTotDispCostNoShipping.hashCode());
		result = prime
				* result
				+ ((sumTotDisplayCostShipping == null) ? 0
						: sumTotDisplayCostShipping.hashCode());
		result = prime * result
				+ ((sumTradeSales == null) ? 0 : sumTradeSales.hashCode());
		result = prime * result
				+ ((sumVmDollarAp == null) ? 0 : sumVmDollarAp.hashCode());
		result = prime * result
				+ ((sumVmDollarBp == null) ? 0 : sumVmDollarBp.hashCode());
		result = prime * result
				+ ((sumVmPctAp == null) ? 0 : sumVmPctAp.hashCode());
		result = prime * result
				+ ((sumVmPctBp == null) ? 0 : sumVmPctBp.hashCode());
		result = prime
				* result
				+ ((vmDollarHurdleGreen == null) ? 0 : vmDollarHurdleGreen
						.hashCode());
		result = prime
				* result
				+ ((vmDollarHurdleRed == null) ? 0 : vmDollarHurdleRed
						.hashCode());
		result = prime * result
				+ ((vmPctHurdleGreen == null) ? 0 : vmPctHurdleGreen.hashCode());
		result = prime * result
				+ ((vmPctHurdleRed == null) ? 0 : vmPctHurdleRed.hashCode());
		result = prime * result
		+ ((excessInvtVmDollar == null) ? 0 : excessInvtVmDollar.hashCode());
		result = prime * result
		+ ((excessInvtVmPct == null) ? 0 : excessInvtVmPct.hashCode());
		result = prime * result
		+ ((corrugateCostPerUnit == null) ? 0 : corrugateCostPerUnit.hashCode());
		result = prime * result
		+ ((laborCostPerUnit == null) ? 0 : laborCostPerUnit.hashCode());
		result = prime * result
				+ ((mdfPct == null) ? 0 : mdfPct.hashCode());
		result = prime * result
				+ ((discountPct == null) ? 0 : discountPct.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CalculationSummary other = (CalculationSummary) obj;
		if (actualQty == null) {
			if (other.actualQty != null)
				return false;
		} else if (!actualQty.equals(other.actualQty))
			return false;
		if (averySku == null) {
			if (other.averySku != null)
				return false;
		} else if (!averySku.equals(other.averySku))
			return false;
		if (calculationSummaryId == null) {
			if (other.calculationSummaryId != null)
				return false;
		} else if (!calculationSummaryId.equals(other.calculationSummaryId))
			return false;
		if (custProgDisc == null) {
			if (other.custProgDisc != null)
				return false;
		} else if (!custProgDisc.equals(other.custProgDisc))
			return false;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (displayId == null) {
			if (other.displayId != null)
				return false;
		} else if (!displayId.equals(other.displayId))
			return false;
		if (displayScenarioId == null) {
			if (other.displayScenarioId != null)
				return false;
		} else if (!displayScenarioId.equals(other.displayScenarioId))
			return false;
		if (foreCastQty == null) {
			if (other.foreCastQty != null)
				return false;
		} else if (!foreCastQty.equals(other.foreCastQty))
			return false;
		if (hurdles == null) {
			if (other.hurdles != null)
				return false;
		} else if (!hurdles.equals(other.hurdles))
			return false;
		if (numberFacing == null) {
			if (other.numberFacing != null)
				return false;
		} else if (!numberFacing.equals(other.numberFacing))
			return false;
		if (promoPeriodId == null) {
			if (other.promoPeriodId != null)
				return false;
		} else if (!promoPeriodId.equals(other.promoPeriodId))
			return false;
		if (qtyPerDisplay == null) {
			if (other.qtyPerDisplay != null)
				return false;
		} else if (!qtyPerDisplay.equals(other.qtyPerDisplay))
			return false;
		if (qtyPerFacing == null) {
			if (other.qtyPerFacing != null)
				return false;
		} else if (!qtyPerFacing.equals(other.qtyPerFacing))
			return false;
		if (shippingCost == null) {
			if (other.shippingCost != null)
				return false;
		} else if (!shippingCost.equals(other.shippingCost))
			return false;
		if (sku == null) {
			if (other.sku != null)
				return false;
		} else if (!sku.equals(other.sku))
			return false;
		if (statusId == null) {
			if (other.statusId != null)
				return false;
		} else if (!statusId.equals(other.statusId))
			return false;
		if (sumCogs == null) {
			if (other.sumCogs != null)
				return false;
		} else if (!sumCogs.equals(other.sumCogs))
			return false;
		if (sumCorrugateCost == null) {
			if (other.sumCorrugateCost != null)
				return false;
		} else if (!sumCorrugateCost.equals(other.sumCorrugateCost))
			return false;
		if (sumDisplayCost == null) {
			if (other.sumDisplayCost != null)
				return false;
		} else if (!sumDisplayCost.equals(other.sumDisplayCost))
			return false;
		if (sumFulFillmentCost == null) {
			if (other.sumFulFillmentCost != null)
				return false;
		} else if (!sumFulFillmentCost.equals(other.sumFulFillmentCost))
			return false;
		if (sumNetSalesAp == null) {
			if (other.sumNetSalesAp != null)
				return false;
		} else if (!sumNetSalesAp.equals(other.sumNetSalesAp))
			return false;
		if (sumNetSalesBp == null) {
			if (other.sumNetSalesBp != null)
				return false;
		} else if (!sumNetSalesBp.equals(other.sumNetSalesBp))
			return false;
		if (sumOtherCost == null) {
			if (other.sumOtherCost != null)
				return false;
		} else if (!sumOtherCost.equals(other.sumOtherCost))
			return false;
		if (sumProdVarMargin == null) {
			if (other.sumProdVarMargin != null)
				return false;
		} else if (!sumProdVarMargin.equals(other.sumProdVarMargin))
			return false;
		if (sumTotActualInvoice == null) {
			if (other.sumTotActualInvoice != null)
				return false;
		} else if (!sumTotActualInvoice.equals(other.sumTotActualInvoice))
			return false;
		if (sumTotDispCostNoShipping == null) {
			if (other.sumTotDispCostNoShipping != null)
				return false;
		} else if (!sumTotDispCostNoShipping
				.equals(other.sumTotDispCostNoShipping))
			return false;
		if (sumTotDisplayCostShipping == null) {
			if (other.sumTotDisplayCostShipping != null)
				return false;
		} else if (!sumTotDisplayCostShipping
				.equals(other.sumTotDisplayCostShipping))
			return false;
		if (sumTradeSales == null) {
			if (other.sumTradeSales != null)
				return false;
		} else if (!sumTradeSales.equals(other.sumTradeSales))
			return false;
		if (sumVmDollarAp == null) {
			if (other.sumVmDollarAp != null)
				return false;
		} else if (!sumVmDollarAp.equals(other.sumVmDollarAp))
			return false;
		if (sumVmDollarBp == null) {
			if (other.sumVmDollarBp != null)
				return false;
		} else if (!sumVmDollarBp.equals(other.sumVmDollarBp))
			return false;
		if (sumVmPctAp == null) {
			if (other.sumVmPctAp != null)
				return false;
		} else if (!sumVmPctAp.equals(other.sumVmPctAp))
			return false;
		if (sumVmPctBp == null) {
			if (other.sumVmPctBp != null)
				return false;
		} else if (!sumVmPctBp.equals(other.sumVmPctBp))
			return false;
		if (vmDollarHurdleGreen == null) {
			if (other.vmDollarHurdleGreen != null)
				return false;
		} else if (!vmDollarHurdleGreen.equals(other.vmDollarHurdleGreen))
			return false;
		if (vmDollarHurdleRed == null) {
			if (other.vmDollarHurdleRed != null)
				return false;
		} else if (!vmDollarHurdleRed.equals(other.vmDollarHurdleRed))
			return false;
		if (vmPctHurdleGreen == null) {
			if (other.vmPctHurdleGreen != null)
				return false;
		} else if (!vmPctHurdleGreen.equals(other.vmPctHurdleGreen))
			return false;
		if (vmPctHurdleRed == null) {
			if (other.vmPctHurdleRed != null)
				return false;
		} else if (!vmPctHurdleRed.equals(other.vmPctHurdleRed))
			return false;
		if (excessInvtVmDollar == null) {
			if (other.excessInvtVmDollar != null)
				return false;
		} else if (!excessInvtVmDollar.equals(other.excessInvtVmDollar))
			return false;
		if (excessInvtVmPct == null) {
			if (other.excessInvtVmPct != null)
				return false;
		} else if (!excessInvtVmPct.equals(other.excessInvtVmPct))
			return false;
		if (corrugateCostPerUnit == null) {
			if (other.corrugateCostPerUnit != null)
				return false;
		} else if (!corrugateCostPerUnit.equals(other.corrugateCostPerUnit))
			return false;
		if (laborCostPerUnit == null) {
			if (other.laborCostPerUnit != null)
				return false;
		} else if (!laborCostPerUnit.equals(other.laborCostPerUnit))
			return false;
		if (mdfPct == null) {
			if (other.mdfPct != null)
				return false;
		} else if (!mdfPct.equals(other.mdfPct))
			return false;
		if (discountPct == null) {
			if (other.discountPct != null)
				return false;
		} else if (!discountPct.equals(other.discountPct))
			return false;
		
		return true;
	}

	
	@Override
	public String toString() {
		return "CalculationSummary [actualQty=" + actualQty + ", averySku="
				+ averySku + ", calculationSummaryId=" + calculationSummaryId
				+ ", custProgDisc=" + custProgDisc + ", customerId="
				+ customerId + ", description=" + description + ", displayId="
				+ displayId + ", displayScenarioId=" + displayScenarioId
				+ ", foreCastQty=" + foreCastQty + ", hurdles=" + hurdles
				+ ", numberFacing=" + numberFacing + ", promoPeriodId="
				+ promoPeriodId + ", qtyPerDisplay=" + qtyPerDisplay
				+ ", qtyPerFacing=" + qtyPerFacing + ", shippingCost="
				+ shippingCost + ", sku=" + sku + ", statusId=" + statusId
				+ ", sumCogs=" + sumCogs + ", sumCorrugateCost="
				+ sumCorrugateCost + ", sumDisplayCost=" + sumDisplayCost
				+ ", sumFulFillmentCost=" + sumFulFillmentCost
				+ ", sumNetSalesAp=" + sumNetSalesAp + ", sumNetSalesBp="
				+ sumNetSalesBp + ", sumOtherCost=" + sumOtherCost
				+ ", sumProdVarMargin=" + sumProdVarMargin
				+ ", sumTotActualInvoice=" + sumTotActualInvoice
				+ ", sumTotDispCostNoShipping=" + sumTotDispCostNoShipping
				+ ", sumTotDisplayCostShipping=" + sumTotDisplayCostShipping
				+ ", sumTradeSales=" + sumTradeSales + ", sumVmDollarAp="
				+ sumVmDollarAp + ", sumVmDollarBp=" + sumVmDollarBp
				+ ", sumVmPctAp=" + sumVmPctAp + ", sumVmPctBp=" + sumVmPctBp
				+ ", vmDollarHurdleGreen=" + vmDollarHurdleGreen
				+ ", vmDollarHurdleRed=" + vmDollarHurdleRed
				+ ", excessInvtVmDollar=" + excessInvtVmDollar
				+ ", excessInvtVmPct=" + excessInvtVmPct
				+ ", corrugateCostPerUnit=" + corrugateCostPerUnit
				+ ", laborCostPerUnit=" + laborCostPerUnit
				+ ", mdfPct=" + mdfPct
				+ ", discountPct=" + discountPct
				+ ", vmPctHurdleGreen=" + vmPctHurdleGreen + ", vmPctHurdleRed="
				+ vmPctHurdleRed + "]";
	}
}
