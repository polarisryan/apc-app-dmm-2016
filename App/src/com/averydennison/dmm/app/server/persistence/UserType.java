package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

public class UserType implements Serializable {

	private static final long serialVersionUID = -5793022069770568841L;
    
	private Boolean activeFlg;

	private Long userTypeId;

	private String typeName;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }
    
    public Long getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Long userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((activeFlg == null) ? 0 : activeFlg.hashCode());
		result = prime * result
				+ ((userTypeId == null) ? 0 : userTypeId.hashCode());
		result = prime * result
				+ ((typeName == null) ? 0 : typeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserType other = (UserType) obj;
		if (activeFlg == null) {
			if (other.activeFlg != null)
				return false;
		} else if (!activeFlg.equals(other.activeFlg))
			return false;
		if (userTypeId == null) {
			if (other.userTypeId != null)
				return false;
		} else if (!userTypeId.equals(other.userTypeId))
			return false;
		if (typeName == null) {
			if (other.typeName != null)
				return false;
		} else if (!typeName.equals(other.typeName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserType [activeFlg=" + activeFlg + ", userTypeId="
				+ userTypeId + ", typeName=" + typeName + "]";
	}

}
