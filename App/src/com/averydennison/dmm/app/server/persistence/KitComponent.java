package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:46 AM
 */
public class KitComponent implements Serializable {
    private Long kitComponentId;

    public Long getKitComponentId() {
        return kitComponentId;
    }

    public void setKitComponentId(Long kitComponentId) {
        this.kitComponentId = kitComponentId;
    }

    private Long displayId;

    public Long getDisplayId() {
        return displayId;
    }

    private Long displayScenarioId;

    
    public Long getDisplayScenarioId() {
		return displayScenarioId;
	}

	public void setDisplayScenarioId(Long displayScenarioId) {
		this.displayScenarioId = displayScenarioId;
	}

	public void setDisplayId(Long displayId) {
        this.displayId = displayId;
    }

	private Long skuId;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    private Long qtyPerFacing;

    public Long getQtyPerFacing() {
        return qtyPerFacing;
    }

    public void setQtyPerFacing(Long qtyPerFacing) {
        this.qtyPerFacing = qtyPerFacing;
    }

    private Long numberFacings;

    public Long getNumberFacings() {
        return numberFacings;
    }

    public void setNumberFacings(Long numberFacings) {
        this.numberFacings = numberFacings;
    }

    private Boolean plantBreakCaseFlg;

    public Boolean isPlantBreakCaseFlg() {
        return plantBreakCaseFlg;
    }

    public void setPlantBreakCaseFlg(Boolean plantBreakCaseFlg) {
        this.plantBreakCaseFlg = plantBreakCaseFlg;
    }

    private Boolean i2ForecastFlg;

    public Boolean isI2ForecastFlg() {
        return i2ForecastFlg;
    }

    public void setI2ForecastFlg(Boolean i2ForecastFlg) {
        this.i2ForecastFlg = i2ForecastFlg;
    }

    private Long regCasePack;

    public Long getRegCasePack() {
        return regCasePack;
    }

    public void setRegCasePack(Long regCasePack) {
        this.regCasePack = regCasePack;
    }

    private Float invoiceUnitPrice;

    public Float getInvoiceUnitPrice() {
        return invoiceUnitPrice;
    }

    public void setInvoiceUnitPrice(Float invoiceUnitPrice) {
        this.invoiceUnitPrice = invoiceUnitPrice;
    }

    private Float cogsUnitPrice;

    public Float getCogsUnitPrice() {
        return cogsUnitPrice;
    }

    public void setCogsUnitPrice(Float cogsUnitPrice) {
        this.cogsUnitPrice = cogsUnitPrice;
    }

    private Float benchMarkPerUnit;
    
    
    public Float getBenchMarkPerUnit() {
		return benchMarkPerUnit;
	}

	public void setBenchMarkPerUnit(Float benchMarkPerUnit) {
		this.benchMarkPerUnit = benchMarkPerUnit;
	}

	private Float priceException;

    public Float getPriceException() {
        return priceException;
    }

    public void setPriceException(Float priceException) {
        this.priceException = priceException;
    }

    
    private Long sequenceNum;

    public Long getSequenceNum() {
        return sequenceNum;
    }

    public void setSequenceNum(Long sequenceNum) {
        this.sequenceNum = sequenceNum;
    }

    private Boolean sourcePimFlg;

    public Boolean isSourcePimFlg() {
        return sourcePimFlg;
    }

    public void setSourcePimFlg(Boolean sourcePimFlg) {
        this.sourcePimFlg = sourcePimFlg;
    }

    private String sku;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    private String buDesc;

    public String getBuDesc() {
        return buDesc;
    }

    public void setBuDesc(String buDesc) {
        this.buDesc = buDesc;
    }

    private String versionNo;

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }
    
    private Boolean promoFlg;

    public Boolean isPromoFlg() {
        return promoFlg;
    }

    public void setPromoFlg(Boolean promoFlg) {
        this.promoFlg = promoFlg;
    }

    private String breakCase;

    public String getBreakCase() {
        return breakCase;
    }

    public void setBreakCase(String breakCase) {
        this.breakCase = breakCase;
    }

    private String kitVersionCode;

    public String getKitVersionCode() {
        return kitVersionCode;
    }

    public void setKitVersionCode(String kitVersionCode) {
        this.kitVersionCode = kitVersionCode;
    }

    private String pbcVersion;

    public String getPbcVersion() {
        return pbcVersion;
    }

    public void setPbcVersion(String pbcVersion) {
        this.pbcVersion = pbcVersion;
    }

    private String finishedGoodsSku;

    public String getFinishedGoodsSku() {
        return finishedGoodsSku;
    }

    public void setFinishedGoodsSku(String finishedGoodsSku) {
        this.finishedGoodsSku = finishedGoodsSku;
    }

    private Long finishedGoodsQty;

    public Long getFinishedGoodsQty() {
        return finishedGoodsQty;
    }

    public void setFinishedGoodsQty(Long finishedGoodsQty) {
        this.finishedGoodsQty = finishedGoodsQty;
    }

    private Date dtCreated;

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    private Date dtLastChanged;

    public Date getDtLastChanged() {
        return dtLastChanged;
    }

    public void setDtLastChanged(Date dtLastChanged) {
        this.dtLastChanged = dtLastChanged;
    }

    private String userCreated;

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    private String userLastChanged;

    public String getUserLastChanged() {
        return userLastChanged;
    }

    public void setUserLastChanged(String userLastChanged) {
        this.userLastChanged = userLastChanged;
    }
    
    private Date peReCalcDate;
       
    public Date getPeReCalcDate() {
		return peReCalcDate;
	}

	public void setPeReCalcDate(Date peReCalcDate) {
		this.peReCalcDate = peReCalcDate;
	}
	private Float excessInvtDollar;
	private Float excessInvtPct;
	private Boolean inPogYn;
	
	public Float getExcessInvtDollar() {
		return excessInvtDollar;
	}

	public void setExcessInvtDollar(Float excessInvtDollar) {
		this.excessInvtDollar = excessInvtDollar;
	}

	public Float getExcessInvtPct() {
		return excessInvtPct;
	}

	public void setExcessInvtPct(Float excessInvtPct) {
		this.excessInvtPct = excessInvtPct;
	}

	public Boolean getInPogYn() {
		return inPogYn;
	}

	public void setInPogYn(Boolean inPogYn) {
		this.inPogYn = inPogYn;
	}
	private Long excessInventoryTypeId;
	private String excessInvtTypeName;
	public Long getExcessInventoryTypeId() {
		return excessInventoryTypeId;
	}

	public void setExcessInventoryTypeId(Long excessInventoryTypeId) {
		this.excessInventoryTypeId = excessInventoryTypeId;
	}

	public String getExcessInvtTypeName() {
		return excessInvtTypeName;
	}

	public void setExcessInvtTypeName(String excessInvtTypeName) {
		this.excessInvtTypeName = excessInvtTypeName;
	}
	
	private Long excessInvtTypeId;
	

	public Long getExcessInvtTypeId() {
		return excessInvtTypeId;
	}

	public void setExcessInvtTypeId(Long excessInvtTypeId) {
		this.excessInvtTypeId = excessInvtTypeId;
	}
	
	private Float overrideExceptionPct;
	
	public Float getOverrideExceptionPct() {
		return overrideExceptionPct;
	}

	public void setOverrideExceptionPct(Float overrideExceptionPct) {
		this.overrideExceptionPct = overrideExceptionPct;
	}

	
	private Float progPrcnt;
	
	public Float getProgPrcnt() {
		return progPrcnt;
	}

	public void setProgPrcnt(Float progPrcnt) {
		this.progPrcnt = progPrcnt;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KitComponent that = (KitComponent) o;
        if (cogsUnitPrice != null ? !cogsUnitPrice.equals(that.cogsUnitPrice) : that.cogsUnitPrice != null)
            return false;
        if (displayId != null ? !displayId.equals(that.displayId) : that.displayId != null) return false;
        if (dtCreated != null ? !dtCreated.equals(that.dtCreated) : that.dtCreated != null) return false;
        if (dtLastChanged != null ? !dtLastChanged.equals(that.dtLastChanged) : that.dtLastChanged != null)
            return false;
        if (i2ForecastFlg != null ? !i2ForecastFlg.equals(that.i2ForecastFlg) : that.i2ForecastFlg != null)
            return false;
        if (invoiceUnitPrice != null ? !invoiceUnitPrice.equals(that.invoiceUnitPrice) : that.invoiceUnitPrice != null)
            return false;
        if (kitComponentId != null ? !kitComponentId.equals(that.kitComponentId) : that.kitComponentId != null)
            return false;
        if (numberFacings != null ? !numberFacings.equals(that.numberFacings) : that.numberFacings != null)
            return false;
        if (plantBreakCaseFlg != null ? !plantBreakCaseFlg.equals(that.plantBreakCaseFlg) : that.plantBreakCaseFlg != null)
            return false;
        if (qtyPerFacing != null ? !qtyPerFacing.equals(that.qtyPerFacing) : that.qtyPerFacing != null) return false;
        if (regCasePack != null ? !regCasePack.equals(that.regCasePack) : that.regCasePack != null) return false;
        if (sequenceNum != null ? !sequenceNum.equals(that.sequenceNum) : that.sequenceNum != null) return false;
        if (sourcePimFlg != null ? !sourcePimFlg.equals(that.sourcePimFlg) : that.sourcePimFlg != null) return false;
        if (sku != null ? !sku.equals(that.sku) : that.sku != null) return false;
        if (productName != null ? !productName.equals(that.productName) : that.productName != null) return false;
        if (buDesc != null ? !buDesc.equals(that.buDesc) : that.buDesc != null) return false;
        if (versionNo != null ? !versionNo.equals(that.versionNo) : that.versionNo != null) return false;
        if (promoFlg != null ? !promoFlg.equals(that.promoFlg) : that.promoFlg != null) return false;
        if (breakCase != null ? !breakCase.equals(that.breakCase) : that.breakCase != null) return false;
        if (kitVersionCode != null ? !kitVersionCode.equals(that.kitVersionCode) : that.kitVersionCode != null)
            return false;
        if (pbcVersion != null ? !pbcVersion.equals(that.pbcVersion) : that.pbcVersion != null) return false;
        if (finishedGoodsSku != null ? !finishedGoodsSku.equals(that.finishedGoodsSku) : that.finishedGoodsSku != null)
            return false;
        if (finishedGoodsQty != null ? !finishedGoodsQty.equals(that.finishedGoodsQty) : that.finishedGoodsQty != null)
            return false;
        if (skuId != null ? !skuId.equals(that.skuId) : that.skuId != null) return false;
        if (userCreated != null ? !userCreated.equals(that.userCreated) : that.userCreated != null) return false;
        if (userLastChanged != null ? !userLastChanged.equals(that.userLastChanged) : that.userLastChanged != null)
            return false;
        if (displayScenarioId != null ? !displayScenarioId.equals(that.displayScenarioId) : that.displayScenarioId != null) return false;
        if (excessInvtDollar != null ? !excessInvtDollar.equals(that.excessInvtDollar) : that.excessInvtDollar != null)
            return false;
        if (excessInvtPct != null ? !excessInvtPct.equals(that.excessInvtPct) : that.excessInvtPct != null)
            return false;
        if (inPogYn != null ? !inPogYn.equals(that.inPogYn) : that.inPogYn != null)
            return false;
        if (excessInvtTypeName != null ? !excessInvtTypeName.equals(that.excessInvtTypeName) : that.excessInvtTypeName != null)
            return false;
        if (excessInventoryTypeId != null ? !excessInventoryTypeId.equals(that.excessInventoryTypeId) : that.excessInventoryTypeId != null)
            return false;
        if (overrideExceptionPct != null ? !overrideExceptionPct.equals(that.overrideExceptionPct) : that.overrideExceptionPct != null)
            return false;
        if (progPrcnt != null ? !progPrcnt.equals(that.progPrcnt) : that.progPrcnt != null)
            return false;
        return true;
    }
	
	@Override
    public int hashCode() {
        int result = kitComponentId != null ? kitComponentId.hashCode() : 0;
        result = 31 * result + (displayId != null ? displayId.hashCode() : 0);
        result = 31 * result + (skuId != null ? skuId.hashCode() : 0);
        result = 31 * result + (qtyPerFacing != null ? qtyPerFacing.hashCode() : 0);
        result = 31 * result + (numberFacings != null ? numberFacings.hashCode() : 0);
        result = 31 * result + (plantBreakCaseFlg != null ? plantBreakCaseFlg.hashCode() : 0);
        result = 31 * result + (i2ForecastFlg != null ? i2ForecastFlg.hashCode() : 0);
        result = 31 * result + (regCasePack != null ? regCasePack.hashCode() : 0);
        result = 31 * result + (invoiceUnitPrice != null ? invoiceUnitPrice.hashCode() : 0);
        result = 31 * result + (cogsUnitPrice != null ? cogsUnitPrice.hashCode() : 0);
        result = 31 * result + (sequenceNum != null ? sequenceNum.hashCode() : 0);
        result = 31 * result + (sourcePimFlg != null ? sourcePimFlg.hashCode() : 0);
        result = 31 * result + (sku != null ? sku.hashCode() : 0);
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (buDesc != null ? buDesc.hashCode() : 0);
        result = 31 * result + (versionNo != null ? versionNo.hashCode() : 0);
        result = 31 * result + (promoFlg != null ? promoFlg.hashCode() : 0);
        result = 31 * result + (breakCase != null ? breakCase.hashCode() : 0);
        result = 31 * result + (kitVersionCode != null ? kitVersionCode.hashCode() : 0);
        result = 31 * result + (pbcVersion != null ? pbcVersion.hashCode() : 0);
        result = 31 * result + (finishedGoodsSku != null ? finishedGoodsSku.hashCode() : 0);
        result = 31 * result + (finishedGoodsQty != null ? finishedGoodsQty.hashCode() : 0);
        result = 31 * result + (dtCreated != null ? dtCreated.hashCode() : 0);
        result = 31 * result + (dtLastChanged != null ? dtLastChanged.hashCode() : 0);
        result = 31 * result + (userCreated != null ? userCreated.hashCode() : 0);
        result = 31 * result + (userLastChanged != null ? userLastChanged.hashCode() : 0);
        result = 31 * result + (displayScenarioId != null ? displayScenarioId.hashCode() : 0);
        result = 31 * result + (excessInvtDollar != null ? excessInvtDollar.hashCode() : 0);
        result = 31 * result + (excessInvtPct != null ? excessInvtPct.hashCode() : 0);
        result = 31 * result + (overrideExceptionPct != null ? overrideExceptionPct.hashCode() : 0);
        result = 31 * result + (inPogYn != null ? inPogYn.hashCode() : 0);
        result = 31 * result + (excessInvtTypeName != null ? excessInvtTypeName.hashCode() : 0);
        result = 31 * result + (excessInventoryTypeId != null ? excessInventoryTypeId.hashCode() : 0);
        result = 31 * result + (progPrcnt != null ? progPrcnt.hashCode() : 0);
        
        return result;
    }

    private Display displayByDisplayId;

    public Display getDisplayByDisplayId() {
        return displayByDisplayId;
    }

    public void setDisplayByDisplayId(Display displayByDisplayId) {
        this.displayByDisplayId = displayByDisplayId;
    }

}
