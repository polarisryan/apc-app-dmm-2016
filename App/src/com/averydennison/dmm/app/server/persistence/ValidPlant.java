package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 11:54:07 AM
 */
public class ValidPlant implements Serializable {
    private String plantCode;

    public String getPlantCode() {
        return plantCode;
    }

    public void setPlantCode(String plantCode) {
        this.plantCode = plantCode;
    }

    private String plantDesc;

    public String getPlantDesc() {
        return plantDesc;
    }

    public void setPlantDesc(String plantDesc) {
        this.plantDesc = plantDesc;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }

    private Boolean activeOnFinanceFlg;

    public Boolean isActiveOnFinanceFlg() {
        return activeOnFinanceFlg;
    }

    public void setActiveOnFinanceFlg(Boolean activeOnFinanceFlg) {
        this.activeOnFinanceFlg = activeOnFinanceFlg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValidPlant that = (ValidPlant) o;

        if (activeFlg != null ? !activeFlg.equals(that.activeFlg) : that.activeFlg != null) return false;
        if (activeOnFinanceFlg != null ? !activeOnFinanceFlg.equals(that.activeOnFinanceFlg) : that.activeOnFinanceFlg != null) return false;
        if (plantCode != null ? !plantCode.equals(that.plantCode) : that.plantCode != null) return false;
        if (plantDesc != null ? !plantDesc.equals(that.plantDesc) : that.plantDesc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = plantCode != null ? plantCode.hashCode() : 0;
        result = 31 * result + (plantDesc != null ? plantDesc.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        result = 31 * result + (activeOnFinanceFlg != null ? activeOnFinanceFlg.hashCode() : 0);
        return result;
    }
}
