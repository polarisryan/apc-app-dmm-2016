package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

public class DisplayScenario implements Serializable {
	private Long displayScenarioId;
	private Long displayId;
	private Long scenariodStatusId;
	private Long scenarioNum;
	private Long displaySalesRepId;
	private Long promoYear;
	private Long promoPeriodId;
	private Long packoutVendorId;
	private Long manufacturerId;
	private String sku;
	private String scenarioApprovalFlg;
	private Date dtCreated;
	private Date dtLastChanged;
	private String userCreated;
	private String userLastChanged;
	private Long qtyForecast;
	private Long actualQty;
	
	
	public String getScenarioApprovalFlg() {
		return scenarioApprovalFlg;
	}
	public void setScenarioApprovalFlg(String scenarioApprovalFlg) {
		this.scenarioApprovalFlg = scenarioApprovalFlg;
	}
	public String getSku(){
		return sku;
	}
	public void setSku(String sku){
		this.sku=sku;
	}
	public Long getDisplayScenarioId() {
		return displayScenarioId;
	}
	public void setDisplayScenarioId(Long displayScenarioId) {
		this.displayScenarioId = displayScenarioId;
	}
	public Long getDisplayId() {
		return displayId;
	}
	public void setDisplayId(Long displayId) {
		this.displayId = displayId;
	}
	public Long getScenariodStatusId() {
		return scenariodStatusId;
	}
	public void setScenariodStatusId(Long scenariodStatusId) {
		this.scenariodStatusId = scenariodStatusId;
	}
	public Long getScenarioNum() {
		return scenarioNum;
	}
	public void setScenarioNum(Long scenarioNum) {
		this.scenarioNum = scenarioNum;
	}
	public Long getDisplaySalesRepId() {
		return displaySalesRepId;
	}
	public void setDisplaySalesRepId(Long displaySalesRepId) {
		this.displaySalesRepId = displaySalesRepId;
	}
	public Long getPromoYear() {
		return promoYear;
	}
	public void setPromoYear(Long promoYear) {
		this.promoYear = promoYear;
	}
	public Long getPromoPeriodId() {
		return promoPeriodId;
	}
	public void setPromoPeriodId(Long promoPeriodId) {
		this.promoPeriodId = promoPeriodId;
	}
	public Long getPackoutVendorId() {
		return packoutVendorId;
	}
	public void setPackoutVendorId(Long packoutVendorId) {
		this.packoutVendorId = packoutVendorId;
	}
	public Long getManufacturerId() {
		return manufacturerId;
	}
	public void setManufacturerId(Long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	public Long getQtyForecast() {
		return qtyForecast;
	}
	public void setQtyForecast(Long qtyForecast) {
		this.qtyForecast = qtyForecast;
	}
	public Long getActualQty() {
		return actualQty;
	}
	public void setActualQty(Long actualQty) {
		this.actualQty = actualQty;
	}
	public Date getDtCreated() {
		return dtCreated;
	}
	public void setDtCreated(Date dtCreated) {
		this.dtCreated = dtCreated;
	}
	public Date getDtLastChanged() {
		return dtLastChanged;
	}
	public void setDtLastChanged(Date dtLastChanged) {
		this.dtLastChanged = dtLastChanged;
	}
	public String getUserCreated() {
		return userCreated;
	}
	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}
	public String getUserLastChanged() {
		return userLastChanged;
	}
	public void setUserLastChanged(String userLastChanged) {
		this.userLastChanged = userLastChanged;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((displayId == null) ? 0 : displayId.hashCode());
		result = prime
				* result
				+ ((displaySalesRepId == null) ? 0 : displaySalesRepId
						.hashCode());
		result = prime
				* result
				+ ((displayScenarioId == null) ? 0 : displayScenarioId
						.hashCode());
		result = prime
				* result
				+ ((actualQty == null) ? 0 : actualQty
						.hashCode());
		result = prime
			* result
			+ ((qtyForecast == null) ? 0 : qtyForecast
						.hashCode());
		result = prime
				* result
				+ ((sku == null) ? 0 : sku.hashCode());
		result = prime
				* result
				+ ((scenarioApprovalFlg == null) ? 0 : scenarioApprovalFlg.hashCode());
		result = prime * result
				+ ((dtCreated == null) ? 0 : dtCreated.hashCode());
		result = prime * result
				+ ((dtLastChanged == null) ? 0 : dtLastChanged.hashCode());
		result = prime * result
				+ ((manufacturerId == null) ? 0 : manufacturerId.hashCode());
		result = prime * result
				+ ((packoutVendorId == null) ? 0 : packoutVendorId.hashCode());
		result = prime * result
				+ ((promoPeriodId == null) ? 0 : promoPeriodId.hashCode());
		result = prime * result
				+ ((promoYear == null) ? 0 : promoYear.hashCode());
		result = prime * result
				+ ((scenarioNum == null) ? 0 : scenarioNum.hashCode());
		result = prime
				* result
				+ ((scenariodStatusId == null) ? 0 : scenariodStatusId
						.hashCode());
		result = prime * result
				+ ((userCreated == null) ? 0 : userCreated.hashCode());
		result = prime * result
				+ ((userLastChanged == null) ? 0 : userLastChanged.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DisplayScenario other = (DisplayScenario) obj;
		if (displayId == null) {
			if (other.displayId != null)
				return false;
		} else if (!displayId.equals(other.displayId))
			return false;
		if (displaySalesRepId == null) {
			if (other.displaySalesRepId != null)
				return false;
		} else if (!displaySalesRepId.equals(other.displaySalesRepId))
			return false;
		if (displayScenarioId == null) {
			if (other.displayScenarioId != null)
				return false;
		} else if (!displayScenarioId.equals(other.displayScenarioId))
			return false;
		if (dtCreated == null) {
			if (other.dtCreated != null)
				return false;
		} else if (!dtCreated.equals(other.dtCreated))
			return false;
		if (dtLastChanged == null) {
			if (other.dtLastChanged != null)
				return false;
		} else if (!dtLastChanged.equals(other.dtLastChanged))
			return false;
		if (manufacturerId == null) {
			if (other.manufacturerId != null)
				return false;
		} else if (!manufacturerId.equals(other.manufacturerId))
			return false;
		if (packoutVendorId == null) {
			if (other.packoutVendorId != null)
				return false;
		} else if (!packoutVendorId.equals(other.packoutVendorId))
			return false;
		if (promoPeriodId == null) {
			if (other.promoPeriodId != null)
				return false;
		} else if (!promoPeriodId.equals(other.promoPeriodId))
			return false;
		if (promoYear == null) {
			if (other.promoYear != null)
				return false;
		} else if (!promoYear.equals(other.promoYear))
			return false;
		if (scenarioNum == null) {
			if (other.scenarioNum != null)
				return false;
		} else if (!scenarioNum.equals(other.scenarioNum))
			return false;
		if (scenariodStatusId == null) {
			if (other.scenariodStatusId != null)
				return false;
		} else if (!scenariodStatusId.equals(other.scenariodStatusId))
			return false;
		if (sku == null) {
			if (other.sku != null)
				return false;
		} else if (!sku.equals(other.sku))
				return false;
		if (scenarioApprovalFlg == null) {
			if (other.scenarioApprovalFlg != null)
				return false;
		} else if (!scenarioApprovalFlg.equals(other.scenarioApprovalFlg))
				return false;
		if (actualQty == null) {
			if (other.actualQty != null)
				return false;
		} else if (!actualQty.equals(other.actualQty))
				return false;
		if (qtyForecast == null) {
			if (other.qtyForecast != null)
				return false;
		} else if (!qtyForecast.equals(other.qtyForecast))
				return false;
		if (userCreated == null) {
			if (other.userCreated != null)
				return false;
		} else if (!userCreated.equals(other.userCreated))
			return false;
		if (userLastChanged == null) {
			if (other.userLastChanged != null)
				return false;
		} else if (!userLastChanged.equals(other.userLastChanged))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "DisplayScenario [displayId=" + displayId
				+ ", displaySalesRepId=" + displaySalesRepId
				+ ", displayScenarioId=" + displayScenarioId + ", dtCreated="
				+ dtCreated + ", dtLastChanged=" + dtLastChanged
				+ ", sku="+sku
				+ ", scenarioApprovalFlg="+scenarioApprovalFlg
				+ ", actualQty="+actualQty
				+ ", qtyForecast="+qtyForecast
				+ ", manufacturerId=" + manufacturerId + ", packoutVendorId="
				+ packoutVendorId + ", promoPeriodId=" + promoPeriodId
				+ ", promoYear=" + promoYear + ", scenarioNum=" + scenarioNum
				+ ", scenariodStatusId=" + scenariodStatusId + ", userCreated="
				+ userCreated + ", userLastChanged=" + userLastChanged + "]";
	}
}
