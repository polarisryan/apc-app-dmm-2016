package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:32 AM
 */
public class CustomerMarketing implements Serializable {
    private Long customerMarketingId;

    public Long getCustomerMarketingId() {
        return customerMarketingId;
    }

    public void setCustomerMarketingId(Long customerMarketingId) {
        this.customerMarketingId = customerMarketingId;
    }

    private Long displayId;

    public Long getDisplayId() {
        return displayId;
    }

    public void setDisplayId(Long displayId) {
        this.displayId = displayId;
    }

    private Date priceAvailabilityDate;

    public Date getPriceAvailabilityDate() {
        return priceAvailabilityDate;
    }

    public void setPriceAvailabilityDate(Date priceAvailabilityDate) {
        this.priceAvailabilityDate = priceAvailabilityDate;
    }

    private Date sentSampleDate;

    public Date getSentSampleDate() {
        return sentSampleDate;
    }

    public void setSentSampleDate(Date sentSampleDate) {
        this.sentSampleDate = sentSampleDate;
    }

    private Date finalArtworkDate;

    public Date getFinalArtworkDate() {
        return finalArtworkDate;
    }

    public void setFinalArtworkDate(Date finalArtworkDate) {
        this.finalArtworkDate = finalArtworkDate;
    }

    private Date pimCompletedDate;

    public Date getPimCompletedDate() {
        return pimCompletedDate;
    }

    public void setPimCompletedDate(Date pimCompletedDate) {
        this.pimCompletedDate = pimCompletedDate;
    }

    private Date pricingAdminDate;

    public Date getPricingAdminDate() {
        return pricingAdminDate;
    }

    public void setPricingAdminDate(Date pricingAdminDate) {
        this.pricingAdminDate = pricingAdminDate;
    }

    private Boolean initialRenderingFlg;

    public Boolean isInitialRenderingFlg() {
        return initialRenderingFlg;
    }

    public void setInitialRenderingFlg(Boolean initialRenderingFlg) {
        this.initialRenderingFlg = initialRenderingFlg;
    }

    private Boolean structureApprovedFlg;

    public Boolean isStructureApprovedFlg() {
        return structureApprovedFlg;
    }

    public void setStructureApprovedFlg(Boolean structureApprovedFlg) {
        this.structureApprovedFlg = structureApprovedFlg;
    }

    private Boolean orderInFlg;

    public Boolean isOrderInFlg() {
        return orderInFlg;
    }

    public void setOrderInFlg(Boolean orderInFlg) {
        this.orderInFlg = orderInFlg;
    }

    private Short projectLevelPct;

    public Short getProjectLevelPct() {
        return projectLevelPct;
    }

    public void setProjectLevelPct(Short projectLevelPct) {
        this.projectLevelPct = projectLevelPct;
    }

    private String comments;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    private Date dtCreated;

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    private Date dtLastChanged;

    public Date getDtLastChanged() {
        return dtLastChanged;
    }

    public void setDtLastChanged(Date dtLastChanged) {
        this.dtLastChanged = dtLastChanged;
    }

    private String userCreated;

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    private String userLastChanged;

    public String getUserLastChanged() {
        return userLastChanged;
    }

    public void setUserLastChanged(String userLastChanged) {
        this.userLastChanged = userLastChanged;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerMarketing that = (CustomerMarketing) o;

        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;
        if (customerMarketingId != null ? !customerMarketingId.equals(that.customerMarketingId) : that.customerMarketingId != null)
            return false;
        if (displayId != null ? !displayId.equals(that.displayId) : that.displayId != null) return false;
        if (dtCreated != null ? !dtCreated.equals(that.dtCreated) : that.dtCreated != null) return false;
        if (dtLastChanged != null ? !dtLastChanged.equals(that.dtLastChanged) : that.dtLastChanged != null)
            return false;
        if (finalArtworkDate != null ? !finalArtworkDate.equals(that.finalArtworkDate) : that.finalArtworkDate != null)
            return false;
        if (initialRenderingFlg != null ? !initialRenderingFlg.equals(that.initialRenderingFlg) : that.initialRenderingFlg != null)
            return false;
        if (orderInFlg != null ? !orderInFlg.equals(that.orderInFlg) : that.orderInFlg != null) return false;
        if (pimCompletedDate != null ? !pimCompletedDate.equals(that.pimCompletedDate) : that.pimCompletedDate != null)
            return false;
        if (priceAvailabilityDate != null ? !priceAvailabilityDate.equals(that.priceAvailabilityDate) : that.priceAvailabilityDate != null)
            return false;
        if (pricingAdminDate != null ? !pricingAdminDate.equals(that.pricingAdminDate) : that.pricingAdminDate != null)
            return false;
        if (projectLevelPct != null ? !projectLevelPct.equals(that.projectLevelPct) : that.projectLevelPct != null)
            return false;
        if (sentSampleDate != null ? !sentSampleDate.equals(that.sentSampleDate) : that.sentSampleDate != null)
            return false;
        if (structureApprovedFlg != null ? !structureApprovedFlg.equals(that.structureApprovedFlg) : that.structureApprovedFlg != null)
            return false;
        if (userCreated != null ? !userCreated.equals(that.userCreated) : that.userCreated != null) return false;
        if (userLastChanged != null ? !userLastChanged.equals(that.userLastChanged) : that.userLastChanged != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = customerMarketingId != null ? customerMarketingId.hashCode() : 0;
        result = 31 * result + (displayId != null ? displayId.hashCode() : 0);
        result = 31 * result + (priceAvailabilityDate != null ? priceAvailabilityDate.hashCode() : 0);
        result = 31 * result + (sentSampleDate != null ? sentSampleDate.hashCode() : 0);
        result = 31 * result + (finalArtworkDate != null ? finalArtworkDate.hashCode() : 0);
        result = 31 * result + (pimCompletedDate != null ? pimCompletedDate.hashCode() : 0);
        result = 31 * result + (pricingAdminDate != null ? pricingAdminDate.hashCode() : 0);
        result = 31 * result + (initialRenderingFlg != null ? initialRenderingFlg.hashCode() : 0);
        result = 31 * result + (structureApprovedFlg != null ? structureApprovedFlg.hashCode() : 0);
        result = 31 * result + (orderInFlg != null ? orderInFlg.hashCode() : 0);
        result = 31 * result + (projectLevelPct != null ? projectLevelPct.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (dtCreated != null ? dtCreated.hashCode() : 0);
        result = 31 * result + (dtLastChanged != null ? dtLastChanged.hashCode() : 0);
        result = 31 * result + (userCreated != null ? userCreated.hashCode() : 0);
        result = 31 * result + (userLastChanged != null ? userLastChanged.hashCode() : 0);
        return result;
    }

    public String toString() {
        String out = new String();
        out += "comments=" + comments;
        out += "customerMarketingId=" + customerMarketingId;
        out += "displayId=" + displayId;
        out += "dtCreated=" + dtCreated;
        out += "dtLastChanged=" + dtLastChanged;
        out += "finalArtworkDate=" + finalArtworkDate;
        out += "initialRenderingFlg=" + initialRenderingFlg;
        out += "orderInFlg=" + orderInFlg;
        out += "pimCompletedDate=" + pimCompletedDate;
        out += "priceAvailabilityDate=" + priceAvailabilityDate;
        out += "priceAdminDate=" + pricingAdminDate;
        out += "projectLevelPct=" + projectLevelPct;
        out += "sentSampleDate=" + sentSampleDate;
        out += "structureApprovedFlg=" + structureApprovedFlg;
        out += "userCreated=" + userCreated;
        out += "userLastChanged=" + userLastChanged;

        return out;
    }

    private Display displayByDisplayId;

    public Display getDisplayByDisplayId() {
        return displayByDisplayId;
    }

    public void setDisplayByDisplayId(Display displayByDisplayId) {
        this.displayByDisplayId = displayByDisplayId;
    }
}
