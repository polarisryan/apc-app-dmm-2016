package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Date;



public class CostAnalysisKitComponent implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long kitComponentId;
	 private Long displayId;
	 private String sku;
	 private Long displayScenarioId;
	 private String buDesc;
	 private String buCode;
	 private String productName;
	 private Long regCasePack;
	 private String sourcePimFlg;
	 private String promoFlg;
	 private String i2ForecastFlg;
	 private String plantBreakCaseFlg;
	 private String  pbcVersion;
	 private Long  qtyPerFacing;
	 private Long  numberFacings;
	 private Long  qtyPerDisplay;
	 private Long  inventoryReq;
	 private Double priceException;
	 private Double actualInvoicePerEa;
	 private Double totalActualInvoice;
	 private Double totalActualInvoiceWpgrm;
	 private Double cogsUnitPrice;
	 private Double totalCogsUnitPrice;
	 private Double netVmAmount;
	 private Double netVmPct;
	 private Double benchMarkPerUnit;
	 private Double totalBenchMark;
	 private Double invoiceUnitPrice;
	 private Double totalInvoiceUnitPrice;
	 private String chkDgt;
	 private Date peReCalcDate;
	 private Long sequenceNum;
     private Double excessInvtDollar;
     private Double  excessInvtPct;
     private String  inPogYn;
     private Double overrideExcPct;
     private Double estimatedProgramPct;
     private Double invoiceWithProgram;
     private Double netVmAmountBp;
	 private Double netVmPctBp;
	 private Double progPrcnt;
	
   
   
   public Double getTotalActualInvoiceWpgrm() {
		return totalActualInvoiceWpgrm;
	}

	public void setTotalActualInvoiceWpgrm(Double totalActualInvoiceWpgrm) {
		this.totalActualInvoiceWpgrm = totalActualInvoiceWpgrm;
	}

	public Double getOverrideExcPct() {
		return overrideExcPct;
	}
	
	public Double getProgPrcnt() {
		return progPrcnt;
	}
	
   public Double getNetVmAmountBp() {
		return netVmAmountBp;
	}

	public void setNetVmAmountBp(Double netVmAmountBp) {
		this.netVmAmountBp = netVmAmountBp;
	}

	public Double getNetVmPctBp() {
		return netVmPctBp;
	}

	public void setNetVmPctBp(Double netVmPctBp) {
		this.netVmPctBp = netVmPctBp;
	}

	public Double getInvoiceWithProgram() {
		return invoiceWithProgram;
	}

	public void setInvoiceWithProgram(Double invoiceWithProgram) {
		this.invoiceWithProgram = invoiceWithProgram;
	}

	public Double getEstimatedProgramPct() {
		return estimatedProgramPct;
	}

	public void setEstimatedProgramPct(Double estimatedProgramPct) {
		this.estimatedProgramPct = estimatedProgramPct;
	}

	public void setOverrideExcPct(Double overrideExcPct) {
		this.overrideExcPct = overrideExcPct;
	}

	public Long getDisplayId() {
		return displayId;
	}

	public void setDisplayId(Long displayId) {
		this.displayId = displayId;
	}
	
	public Long getSequenceNum() {
		return sequenceNum;
	}

	public void setSequenceNum(Long sequenceNum) {
		this.sequenceNum = sequenceNum;
	}
	public Long getKitComponentId() {
		return kitComponentId;
	}
	public void setKitComponentId(Long kitComponentId) {
		this.kitComponentId = kitComponentId;
	}
	public String getBuDesc() {
		return buDesc;
	}
	public void setBuDesc(String buDesc) {
		this.buDesc = buDesc;
	}
	
	public String getBuCode() {
		return buCode;
	}
	public void setBuCode(String buCode) {
		this.buCode = buCode;
	}
	
	public Long getRegCasePack() {
		return regCasePack;
	}
	public void setRegCasePack(Long regCasePack) {
		this.regCasePack = regCasePack;
	}
	public String getSourcePimFlg() {
		return sourcePimFlg;
	}
	public void setSourcePimFlg(String sourcePimFlg) {
		this.sourcePimFlg = sourcePimFlg;
	}
	public String getPromoFlg() {
		return promoFlg;
	}
	public void setPromoFlg(String promoFlg) {
		this.promoFlg = promoFlg;
	}
	public String getI2ForecastFlg() {
		return i2ForecastFlg;
	}
	public void setI2ForecastFlg(String i2ForecastFlg) {
		this.i2ForecastFlg = i2ForecastFlg;
	}
	public String getPlantBreakCaseFlg() {
		return plantBreakCaseFlg;
	}
	public void setPlantBreakCaseFlg(String plantBreakCaseFlg) {
		this.plantBreakCaseFlg = plantBreakCaseFlg;
	}
	public String getPbcVersion() {
		return pbcVersion;
	}
	public void setPbcVersion(String pbcVersion) {
		this.pbcVersion = pbcVersion;
	}
	public Long getQtyPerFacing() {
		return qtyPerFacing;
	}
	public void setQtyPerFacing(Long qtyPerFacing) {
		this.qtyPerFacing = qtyPerFacing;
	}
	public Long getNumberFacings() {
		return numberFacings;
	}
	public void setNumberFacings(Long numberFacings) {
		if (numberFacings!=null)
			this.numberFacings = numberFacings;
		else
			this.numberFacings = new Long(0);
	}
	public Long getQtyPerDisplay() {
		return qtyPerDisplay;
	}
	public void setQtyPerDisplay(Long qtyPerDisplay) {
		if(qtyPerDisplay==0) qtyPerDisplay= new Long(0);
		this.qtyPerDisplay = qtyPerDisplay;
	}
	public Long getInventoryReq() {
		return inventoryReq;
	}
	public void setInventoryReq(Long inventoryReq) {
		if(inventoryReq==0) inventoryReq= new Long(0);
		this.inventoryReq = inventoryReq;
	}
	public Double getPriceException() {
		return priceException;
	}
	public void setPriceException(Double priceException) {
		this.priceException = priceException;
	}
	
	public void setPeReCalcDate(Date peReCalcDate){
		this.peReCalcDate = peReCalcDate;
	}
	public Date getPeReCalcDate() {
		return peReCalcDate;
	}
	public Double getActualInvoicePerEa() {
		return actualInvoicePerEa;
	}
	public void setActualInvoicePerEa(Double actualInvoicePerEa) {
		if(actualInvoicePerEa==0.0) actualInvoicePerEa= new Double(0.0);
		this.actualInvoicePerEa = actualInvoicePerEa;
	}
	public Double getTotalActualInvoice() {
		return totalActualInvoice;
	}
	public void setTotalActualInvoice(Double totalActualInvoice) {
		if(totalActualInvoice==0.0) totalActualInvoice= new Double(0.0);
		this.totalActualInvoice = totalActualInvoice;
	}
	public Double getCogsUnitPrice() {
		return cogsUnitPrice;
	}
	public void setCogsUnitPrice(Double cogsUnitPrice) {
		if(cogsUnitPrice==null ) cogsUnitPrice=new Double(0.0);
		this.cogsUnitPrice = cogsUnitPrice;
	}
	public Double getTotalCogsUnitPrice() {
		return totalCogsUnitPrice;
	}
	public void setTotalCogsUnitPrice(Double totalCogsUnitPrice) {
		if(totalCogsUnitPrice==0.0) totalCogsUnitPrice= new Double(0.0);
		this.totalCogsUnitPrice = totalCogsUnitPrice;
	}
	public Double getNetVmAmount() {
		return netVmAmount;
	}
	public void setNetVmAmount(Double netVmAmount) {
		if(netVmAmount==0.0) netVmAmount= new Double(0.0);
		this.netVmAmount = netVmAmount;
	}
	public Double getNetVmPct() {
		return netVmPct;
	}
	public void setNetVmPct(Double netVmPct) {
		if(netVmPct==0.0) netVmPct= new Double(0.0);
		this.netVmPct = netVmPct;
	}
	public Double getBenchMarkPerUnit() {
		return benchMarkPerUnit;
	}
	public void setBenchMarkPerUnit(Double benchMarkPerUnit) {
		if(benchMarkPerUnit==null) benchMarkPerUnit = new Double(0.0);
		this.benchMarkPerUnit = benchMarkPerUnit;
	}
	public Double getTotalBenchMark() {
		return totalBenchMark;
	}
	public void setTotalBenchMark(Double totalBenchMark) {
		if(totalBenchMark==0.0) totalBenchMark = new Double(0.0);
		this.totalBenchMark = totalBenchMark;
	}
	public Double getInvoiceUnitPrice() {
		return invoiceUnitPrice;
	}
	public void setInvoiceUnitPrice(Double invoiceUnitPrice) {
		if(invoiceUnitPrice==null) invoiceUnitPrice= new Double(0.0);
		this.invoiceUnitPrice = invoiceUnitPrice;
	}
	public Double getTotalInvoiceUnitPrice() {
		return totalInvoiceUnitPrice;
	}
	public void setTotalInvoiceUnitPrice(Double totalInvoiceUnitPrice) {
		if(totalInvoiceUnitPrice==0.0) totalInvoiceUnitPrice = new Double(0.0);
		this.totalInvoiceUnitPrice = totalInvoiceUnitPrice;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getChkDgt() {
		return chkDgt;
	}
	public void setChkDgt(String chkDgt) {
		this.chkDgt = chkDgt;
	}
	public Long getDisplayScenarioId() {
		return displayScenarioId;
	}

	public void setDisplayScenarioId(Long displayScenarioId) {
		this.displayScenarioId = displayScenarioId;
	}
	
	public Double getExcessInvtDollar() {
		return excessInvtDollar;
	}
				
	public void setExcessInvtDollar(Double excessInvtDollar) {
		if(excessInvtDollar==null) excessInvtDollar= new Double(0.0);
		this.excessInvtDollar = excessInvtDollar;
	}

	public Double getExcessInvtPct() {
		return excessInvtPct;
	}

	public void setExcessInvtPct(Double excessInvtPct) {
		if(excessInvtPct==null) excessInvtPct= new Double(0.0);
		this.excessInvtPct = excessInvtPct;
	}

	public String getInPogYn() {
		return inPogYn;
	}

	public void setInPogYn(String inPogYn) {
		if(inPogYn==null) inPogYn= "N";
		this.inPogYn = inPogYn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
		* result
		+ ((overrideExcPct == null) ? 0 : overrideExcPct
				.hashCode());
		result = prime
		* result
		+ ((estimatedProgramPct == null) ? 0 : estimatedProgramPct
				.hashCode());
		result = prime
		* result
		+ ((invoiceWithProgram == null) ? 0 : invoiceWithProgram
				.hashCode());
		result = prime
				* result
				+ ((actualInvoicePerEa == null) ? 0 : actualInvoicePerEa
						.hashCode());
		result = prime
				* result
				+ ((benchMarkPerUnit == null) ? 0 : benchMarkPerUnit.hashCode());
		result = prime * result + ((buDesc == null) ? 0 : buDesc.hashCode());
		result = prime * result + ((buCode == null) ? 0 : buCode.hashCode());
		result = prime * result + ((chkDgt == null) ? 0 : chkDgt.hashCode());
		result = prime * result
				+ ((cogsUnitPrice == null) ? 0 : cogsUnitPrice.hashCode());
		result = prime * result
				+ ((i2ForecastFlg == null) ? 0 : i2ForecastFlg.hashCode());
		result = prime * result
				+ ((inventoryReq == null) ? 0 : inventoryReq.hashCode());
		result = prime
				* result
				+ ((invoiceUnitPrice == null) ? 0 : invoiceUnitPrice.hashCode());
		result = prime * result
				+ ((kitComponentId == null) ? 0 : kitComponentId.hashCode());
		result = prime * result
				+ ((netVmAmount == null) ? 0 : netVmAmount.hashCode());
		result = prime * result
				+ ((netVmPct == null) ? 0 : netVmPct.hashCode());
		result = prime * result
				+ ((numberFacings == null) ? 0 : numberFacings.hashCode());
		result = prime * result
				+ ((pbcVersion == null) ? 0 : pbcVersion.hashCode());
		result = prime
				* result
				+ ((plantBreakCaseFlg == null) ? 0 : plantBreakCaseFlg
						.hashCode());
		result = prime * result
				+ ((priceException == null) ? 0 : priceException.hashCode());
		result = prime * result
				+ ((productName == null) ? 0 : productName.hashCode());
		result = prime * result
				+ ((promoFlg == null) ? 0 : promoFlg.hashCode());
		result = prime * result
				+ ((qtyPerDisplay == null) ? 0 : qtyPerDisplay.hashCode());
		result = prime * result
				+ ((qtyPerFacing == null) ? 0 : qtyPerFacing.hashCode());
		result = prime * result
				+ ((regCasePack == null) ? 0 : regCasePack.hashCode());
		result = prime * result + ((sku == null) ? 0 : sku.hashCode());
		result = prime * result
				+ ((sourcePimFlg == null) ? 0 : sourcePimFlg.hashCode());
		result = prime
				* result
				+ ((totalActualInvoice == null) ? 0 : totalActualInvoice
						.hashCode());
		result = prime
		* result
		+ ((totalActualInvoiceWpgrm == null) ? 0 : totalActualInvoiceWpgrm
				.hashCode());
		result = prime * result
				+ ((totalBenchMark == null) ? 0 : totalBenchMark.hashCode());
		result = prime
				* result
				+ ((totalCogsUnitPrice == null) ? 0 : totalCogsUnitPrice
						.hashCode());
		result = prime
				* result
				+ ((totalInvoiceUnitPrice == null) ? 0 : totalInvoiceUnitPrice
						.hashCode());
		result = prime
		* result
		+ ((displayScenarioId == null) ? 0 : displayScenarioId
				.hashCode());
		result = prime
		* result
		+ ((excessInvtDollar == null) ? 0 : excessInvtDollar
				.hashCode());
		result = prime
		* result
		+ ((excessInvtPct == null) ? 0 : excessInvtPct
				.hashCode());
		result = prime
		* result
		+ ((inPogYn == null) ? 0 : inPogYn
				.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CostAnalysisKitComponent other = (CostAnalysisKitComponent) obj;
		if (actualInvoicePerEa == null) {
			if (other.actualInvoicePerEa != null)
				return false;
		} else if (!actualInvoicePerEa.equals(other.actualInvoicePerEa))
			return false;
		if (overrideExcPct == null) {
			if (other.overrideExcPct != null)
				return false;
		} else if (!overrideExcPct.equals(other.overrideExcPct))
			return false;
		if (benchMarkPerUnit == null) {
			if (other.benchMarkPerUnit != null)
				return false;
		} else if (!benchMarkPerUnit.equals(other.benchMarkPerUnit))
			return false;
		if (buDesc == null) {
			if (other.buDesc != null)
				return false;
		} else if (!buDesc.equals(other.buDesc))
			return false;
		if (buCode == null) {
			if (other.buCode != null)
				return false;
		} else if (!buCode.equals(other.buCode))
			return false;
		if (chkDgt == null) {
			if (other.chkDgt != null)
				return false;
		} else if (!chkDgt.equals(other.chkDgt))
			return false;
		if (cogsUnitPrice == null) {
			if (other.cogsUnitPrice != null)
				return false;
		} else if (!cogsUnitPrice.equals(other.cogsUnitPrice))
			return false;
		if (i2ForecastFlg == null) {
			if (other.i2ForecastFlg != null)
				return false;
		} else if (!i2ForecastFlg.equals(other.i2ForecastFlg))
			return false;
		if (inventoryReq == null) {
			if (other.inventoryReq != null)
				return false;
		} else if (!inventoryReq.equals(other.inventoryReq))
			return false;
		if (invoiceUnitPrice == null) {
			if (other.invoiceUnitPrice != null)
				return false;
		} else if (!invoiceUnitPrice.equals(other.invoiceUnitPrice))
			return false;
		if (kitComponentId == null) {
			if (other.kitComponentId != null)
				return false;
		} else if (!kitComponentId.equals(other.kitComponentId))
			return false;
		if (netVmAmount == null) {
			if (other.netVmAmount != null)
				return false;
		} else if (!netVmAmount.equals(other.netVmAmount))
			return false;
		if (netVmPct == null) {
			if (other.netVmPct != null)
				return false;
		} else if (!netVmPct.equals(other.netVmPct))
			return false;
		if (numberFacings == null) {
			if (other.numberFacings != null)
				return false;
		} else if (!numberFacings.equals(other.numberFacings))
			return false;
		if (pbcVersion == null) {
			if (other.pbcVersion != null)
				return false;
		} else if (!pbcVersion.equals(other.pbcVersion))
			return false;
		if (plantBreakCaseFlg == null) {
			if (other.plantBreakCaseFlg != null)
				return false;
		} else if (!plantBreakCaseFlg.equals(other.plantBreakCaseFlg))
			return false;
		if (priceException == null) {
			if (other.priceException != null)
				return false;
		} else if (!priceException.equals(other.priceException))
			return false;
		if (productName == null) {
			if (other.productName != null)
				return false;
		} else if (!productName.equals(other.productName))
			return false;
		if (promoFlg == null) {
			if (other.promoFlg != null)
				return false;
		} else if (!promoFlg.equals(other.promoFlg))
			return false;
		if (qtyPerDisplay == null) {
			if (other.qtyPerDisplay != null)
				return false;
		} else if (!qtyPerDisplay.equals(other.qtyPerDisplay))
			return false;
		if (qtyPerFacing == null) {
			if (other.qtyPerFacing != null)
				return false;
		} else if (!qtyPerFacing.equals(other.qtyPerFacing))
			return false;
		if (regCasePack == null) {
			if (other.regCasePack != null)
				return false;
		} else if (!regCasePack.equals(other.regCasePack))
			return false;
		if (sku == null) {
			if (other.sku != null)
				return false;
		} else if (!sku.equals(other.sku))
			return false;
		if (sourcePimFlg == null) {
			if (other.sourcePimFlg != null)
				return false;
		} else if (!sourcePimFlg.equals(other.sourcePimFlg))
			return false;
		if (totalActualInvoice == null) {
			if (other.totalActualInvoice != null)
				return false;
		} else if (!totalActualInvoice.equals(other.totalActualInvoice))
			return false;
		if (totalActualInvoiceWpgrm == null) {
			if (other.totalActualInvoiceWpgrm != null)
				return false;
		} else if (!totalActualInvoiceWpgrm.equals(other.totalActualInvoiceWpgrm))
			return false;
		if (totalBenchMark == null) {
			if (other.totalBenchMark != null)
				return false;
		} else if (!totalBenchMark.equals(other.totalBenchMark))
			return false;
		if (totalCogsUnitPrice == null) {
			if (other.totalCogsUnitPrice != null)
				return false;
		} else if (!totalCogsUnitPrice.equals(other.totalCogsUnitPrice))
			return false;
		if (totalInvoiceUnitPrice == null) {
			if (other.totalInvoiceUnitPrice != null)
				return false;
		} else if (!totalInvoiceUnitPrice.equals(other.totalInvoiceUnitPrice))
			return false;
		if (displayScenarioId == null) {
			if (other.displayScenarioId != null)
				return false;
		} else if (!displayScenarioId.equals(other.displayScenarioId))
			return false;
		if (excessInvtDollar == null) {
			if (other.excessInvtDollar != null)
				return false;
		} else if (!excessInvtDollar.equals(other.excessInvtDollar))
			return false;
		if (excessInvtPct == null) {
			if (other.excessInvtPct != null)
				return false;
		} else if (!excessInvtPct.equals(other.excessInvtPct))
			return false;
		if (inPogYn == null) {
			if (other.inPogYn != null)
				return false;
		} else if (!inPogYn.equals(other.inPogYn))
			return false;
		
		if (invoiceWithProgram == null) {
			if (other.invoiceWithProgram != null)
				return false;
		} else if (!invoiceWithProgram.equals(other.invoiceWithProgram))
			return false;
		
		if (estimatedProgramPct == null) {
			if (other.estimatedProgramPct != null)
				return false;
		} else if (!estimatedProgramPct.equals(other.estimatedProgramPct))
			return false;
		
		return true;
	}
	@Override
	public String toString() {
		return "CostAnalysisKitComponent [actualInvoicePerEa="
				+ actualInvoicePerEa + ", benchMarkPerUnit=" + benchMarkPerUnit
				+ ", buDesc=" + buDesc + ", chkDgt=" + chkDgt
				+ ", cogsUnitPrice=" + cogsUnitPrice + ", i2ForecastFlg="
				+ i2ForecastFlg + ", inventoryReq=" + inventoryReq
				+ ", invoiceUnitPrice=" + invoiceUnitPrice
				+ ", kitComponentId=" + kitComponentId + ", netVmAmount="
				+ netVmAmount + ", netVmPct=" + netVmPct + ", numberFacings="
				+ numberFacings + ", pbcVersion=" + pbcVersion
				+ ", plantBreakCaseFlg=" + plantBreakCaseFlg
				+ ", priceException=" + priceException + ", productName="
				+ productName + ", promoFlg=" + promoFlg + ", qtyPerDisplay="
				+ qtyPerDisplay + ", qtyPerFacing=" + qtyPerFacing
				+ ", regCasePack=" + regCasePack + ", sku=" + sku
				+ ", sourcePimFlg=" + sourcePimFlg 
				+ ", totalActualInvoice=" + totalActualInvoice +
				", totalActualInvoiceWpgrm=" + totalActualInvoiceWpgrm +
				", totalBenchMark=" + totalBenchMark
				+ ", totalCogsUnitPrice=" + totalCogsUnitPrice
				+ ", displayScenarioId=" + displayScenarioId
				+ ", excessInvtDollar=" + excessInvtDollar
				+ ", excessInvtPct=" + excessInvtPct
				+ ", overrideExceptionPct=" + overrideExcPct
				+ ", invoiceWithProgram=" + invoiceWithProgram
				+ ", estimatedProgramPct=" + estimatedProgramPct
				+ ", inPogYn=" + inPogYn
				+ ", totalInvoiceUnitPrice=" + totalInvoiceUnitPrice + "]";
	}
	
}
