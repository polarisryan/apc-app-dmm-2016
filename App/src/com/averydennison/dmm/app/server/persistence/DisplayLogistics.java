package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:38 AM
 */
public class DisplayLogistics implements Serializable {
    private Long displayLogisticsId;

    public Long getDisplayLogisticsId() {
        return displayLogisticsId;
    }

    public void setDisplayLogisticsId(Long displayLogisticsId) {
        this.displayLogisticsId = displayLogisticsId;
    }

    private Long displayId;

    public Long getDisplayId() {
        return displayId;
    }

    public void setDisplayId(Long displayId) {
        this.displayId = displayId;
    }

    private Float displayLength;

    public Float getDisplayLength() {
        return displayLength;
    }

    public void setDisplayLength(Float displayLength) {
        this.displayLength = displayLength;
    }

    private Float displayWidth;

    public Float getDisplayWidth() {
        return displayWidth;
    }

    public void setDisplayWidth(Float displayWidth) {
        this.displayWidth = displayWidth;
    }

    private Float displayHeight;

    public Float getDisplayHeight() {
        return displayHeight;
    }

    public void setDisplayHeight(Float displayHeight) {
        this.displayHeight = displayHeight;
    }

    private Float displayWeight;

    public Float getDisplayWeight() {
        return displayWeight;
    }

    public void setDisplayWeight(Float displayWeight) {
        this.displayWeight = displayWeight;
    }

    private Long displayPerLayer;

    public Long getDisplayPerLayer() {
        return displayPerLayer;
    }

    public void setDisplayPerLayer(Long displayPerLayer) {
        this.displayPerLayer = displayPerLayer;
    }

    private Long layersPerPallet;

    public Long getLayersPerPallet() {
        return layersPerPallet;
    }

    public void setLayersPerPallet(Long layersPerPallet) {
        this.layersPerPallet = layersPerPallet;
    }

    private Long palletsPerTrailer;

    public Long getPalletsPerTrailer() {
        return palletsPerTrailer;
    }

    public void setPalletsPerTrailer(Long palletsPerTrailer) {
        this.palletsPerTrailer = palletsPerTrailer;
    }

    private Boolean doubleStackFlg;

    public Boolean isDoubleStackFlg() {
        return doubleStackFlg;
    }

    public void setDoubleStackFlg(Boolean doubleStackFlg) {
        this.doubleStackFlg = doubleStackFlg;
    }

    private String specialLabetRqt;

    public String getSpecialLabetRqt() {
        return specialLabetRqt;
    }

    public void setSpecialLabetRqt(String specialLabetRqt) {
        this.specialLabetRqt = specialLabetRqt;
    }

    private Boolean serialShipFlg;

    public Boolean isSerialShipFlg() {
        return serialShipFlg;
    }

    public void setSerialShipFlg(Boolean serialShipFlg) {
        this.serialShipFlg = serialShipFlg;
    }

    private String units;

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    private Date dtCreated;

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    private Date dtLastChanged;

    public Date getDtLastChanged() {
        return dtLastChanged;
    }

    public void setDtLastChanged(Date dtLastChanged) {
        this.dtLastChanged = dtLastChanged;
    }

    private String userCreated;

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    private String userLastChanged;

    public String getUserLastChanged() {
        return userLastChanged;
    }

    public void setUserLastChanged(String userLastChanged) {
        this.userLastChanged = userLastChanged;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisplayLogistics that = (DisplayLogistics) o;
        if (displayId != null ? !displayId.equals(that.displayId) : that.displayId != null) return false;
        if (displayHeight != null ? !displayHeight.equals(that.displayHeight) : that.displayHeight != null)
            return false;
        if (displayLength != null ? !displayLength.equals(that.displayLength) : that.displayLength != null)
            return false;
        if (displayLogisticsId != null ? !displayLogisticsId.equals(that.displayLogisticsId) : that.displayLogisticsId != null)
            return false;
        if (displayPerLayer != null ? !displayPerLayer.equals(that.displayPerLayer) : that.displayPerLayer != null)
            return false;
        if (displayWeight != null ? !displayWeight.equals(that.displayWeight) : that.displayWeight != null)
            return false;
        if (displayWidth != null ? !displayWidth.equals(that.displayWidth) : that.displayWidth != null) return false;
        if (doubleStackFlg != null ? !doubleStackFlg.equals(that.doubleStackFlg) : that.doubleStackFlg != null)
            return false;
        if (dtCreated != null ? !dtCreated.equals(that.dtCreated) : that.dtCreated != null) return false;
        if (dtLastChanged != null ? !dtLastChanged.equals(that.dtLastChanged) : that.dtLastChanged != null)
            return false;
        if (layersPerPallet != null ? !layersPerPallet.equals(that.layersPerPallet) : that.layersPerPallet != null)
            return false;
        if (palletsPerTrailer != null ? !palletsPerTrailer.equals(that.palletsPerTrailer) : that.palletsPerTrailer != null)
            return false;
        if (serialShipFlg != null ? !serialShipFlg.equals(that.serialShipFlg) : that.serialShipFlg != null)
            return false;
        if (specialLabetRqt != null ? !specialLabetRqt.equals(that.specialLabetRqt) : that.specialLabetRqt != null)
            return false;
        if (units != null ? !units.equals(that.units) : that.units != null) return false;
        if (userCreated != null ? !userCreated.equals(that.userCreated) : that.userCreated != null) return false;
        if (userLastChanged != null ? !userLastChanged.equals(that.userLastChanged) : that.userLastChanged != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = displayLogisticsId != null ? displayLogisticsId.hashCode() : 0;
        result = 31 * result + (displayId != null ? displayId.hashCode() : 0);
        result = 31 * result + (displayLength != null ? displayLength.hashCode() : 0);
        result = 31 * result + (displayWidth != null ? displayWidth.hashCode() : 0);
        result = 31 * result + (displayHeight != null ? displayHeight.hashCode() : 0);
        result = 31 * result + (displayWeight != null ? displayWeight.hashCode() : 0);
        result = 31 * result + (displayPerLayer != null ? displayPerLayer.hashCode() : 0);
        result = 31 * result + (layersPerPallet != null ? layersPerPallet.hashCode() : 0);
        result = 31 * result + (palletsPerTrailer != null ? palletsPerTrailer.hashCode() : 0);
        result = 31 * result + (doubleStackFlg != null ? doubleStackFlg.hashCode() : 0);
        result = 31 * result + (specialLabetRqt != null ? specialLabetRqt.hashCode() : 0);
        result = 31 * result + (serialShipFlg != null ? serialShipFlg.hashCode() : 0);
        result = 31 * result + (units != null ? units.hashCode() : 0);
        result = 31 * result + (dtCreated != null ? dtCreated.hashCode() : 0);
        result = 31 * result + (dtLastChanged != null ? dtLastChanged.hashCode() : 0);
        result = 31 * result + (userCreated != null ? userCreated.hashCode() : 0);
        result = 31 * result + (userLastChanged != null ? userLastChanged.hashCode() : 0);
        return result;
    }

    public String toString() {
        String out = new String();
        out += "displayId =" + displayId;
        out += "displayLength =" + displayLength;
        out += "displayWidth=" + displayWidth;
        out += "displayHeight=" + displayHeight;
        out += "displayWeight=" + displayWeight;
        out += "displayPerLayer=" + displayPerLayer;
        out += "layersPerPallet=" + layersPerPallet;
        out += "palletsPerTrailer=" + palletsPerTrailer;
        out += "doubleStackFlg=" + doubleStackFlg;
        out += "specialLabetRqt=" + specialLabetRqt;
        out += "serialShipFlg=" + serialShipFlg;
        out += "dtCreated=" + dtCreated;
        out += "dtLastChanged=" + dtLastChanged;
        out += "userCreated=" + userCreated;
        out += "userLastChanged=" + userLastChanged;
        return out;
    }

    private Collection<Display> displaysByDisplayLogisticsId;

    public Collection<Display> getDisplaysByDisplayLogisticsId() {
        return displaysByDisplayLogisticsId;
    }

    public void setDisplaysByDisplayLogisticsId(Collection<Display> displaysByDisplayLogisticsId) {
        this.displaysByDisplayLogisticsId = displaysByDisplayLogisticsId;
    }
}
