package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:53 AM
 */
public class UserAccessPK implements Serializable {
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    private Long accessId;

    public Long getAccessId() {
        return accessId;
    }

    public void setAccessId(Long accessId) {
        this.accessId = accessId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAccessPK that = (UserAccessPK) o;

        if (accessId != null ? !accessId.equals(that.accessId) : that.accessId != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (accessId != null ? accessId.hashCode() : 0);
        return result;
    }
}
