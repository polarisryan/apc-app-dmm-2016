package com.averydennison.dmm.app.server.persistence;

import java.util.Date;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:53 AM
 */
public class UserChangeLog {
    private Long userChangeLogId;

    public Long getUserChangeLogId() {
        return userChangeLogId;
    }

    public void setUserChangeLogId(Long userChangeLogId) {
        this.userChangeLogId = userChangeLogId;
    }

  public String friendlyName;
    public String getFriendlyName() {
	return friendlyName;
}

public void setFriendlyName(String friendlyName) {
	this.friendlyName = friendlyName;
}

public String shortName;
public String getShortName() {
	return shortName;
}

public void setShortName(String shortName) {
	this.shortName = shortName;
}

	private Date dtChanged;

    public Date getDtChanged() {
        return dtChanged;
    }

    public void setDtChanged(Date dtChanged) {
        this.dtChanged = dtChanged;
    }

    private String tableName;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    private String field;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    private String oldValue;

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    private String newValue;

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    private Long displayId;

    public Long getDisplayId() {
        return displayId;
    }

    public void setDisplayId(Long displayId) {
        this.displayId = displayId;
    }

    private String changeType;

    public String getChangeType() {
        return changeType;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

    private Long primaryKeyId;

    public Long getPrimaryKeyId() {
        return primaryKeyId;
    }

    public void setPrimaryKeyId(Long primaryKeyId) {
        this.primaryKeyId = primaryKeyId;
    }

    private String userName;
    
    
    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	private Long  displayScenarioId;

	public Long getDisplayScenarioId() {
		return displayScenarioId;
	}

	public void setDisplayScenarioId(Long displayScenarioId) {
		this.displayScenarioId = displayScenarioId;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserChangeLog that = (UserChangeLog) o;
        if (changeType != null ? !changeType.equals(that.changeType) : that.changeType != null) return false;
        if (displayId != null ? !displayId.equals(that.displayId) : that.displayId != null) return false;
        if (dtChanged != null ? !dtChanged.equals(that.dtChanged) : that.dtChanged != null) return false;
        if (field != null ? !field.equals(that.field) : that.field != null) return false;
        if (newValue != null ? !newValue.equals(that.newValue) : that.newValue != null) return false;
        if (oldValue != null ? !oldValue.equals(that.oldValue) : that.oldValue != null) return false;
        if (tableName != null ? !tableName.equals(that.tableName) : that.tableName != null) return false;
        if (userChangeLogId != null ? !userChangeLogId.equals(that.userChangeLogId) : that.userChangeLogId != null) return false;
        if (primaryKeyId != null ? !primaryKeyId.equals(that.primaryKeyId) : that.primaryKeyId != null) return false;
        if (displayScenarioId != null ? !displayScenarioId.equals(that.displayScenarioId) : that.displayScenarioId != null) return false;
        return true;
    }
	
    @Override
    public int hashCode() {
        int result = userChangeLogId != null ? userChangeLogId.hashCode() : 0;
        result = 31 * result + (dtChanged != null ? dtChanged.hashCode() : 0);
        result = 31 * result + (tableName != null ? tableName.hashCode() : 0);
        result = 31 * result + (field != null ? field.hashCode() : 0);
        result = 31 * result + (oldValue != null ? oldValue.hashCode() : 0);
        result = 31 * result + (newValue != null ? newValue.hashCode() : 0);
        result = 31 * result + (displayId != null ? displayId.hashCode() : 0);
        result = 31 * result + (changeType != null ? changeType.hashCode() : 0);
        result = 31 * result + (primaryKeyId != null ? primaryKeyId.hashCode() : 0);
        result = 31 * result + (displayScenarioId != null ? primaryKeyId.hashCode() : 0);
        return result;
    }

    private User userByUserId;
    public User getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(User userByUserId) {
        this.userByUserId = userByUserId;
    }
    
    
    
}
