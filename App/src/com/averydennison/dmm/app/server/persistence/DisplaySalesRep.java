package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:21:44 AM
 */
public class DisplaySalesRep implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -830908323583328536L;
	private Long displaySalesRepId;
    private UserType userType;
    private Country country;

    public Long getDisplaySalesRepId() {
        return displaySalesRepId;
    }

    public void setDisplaySalesRepId(Long displaySalesRepId) {
        this.displaySalesRepId = displaySalesRepId;
    }

    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String repCode;

    public String getRepCode() {
        return repCode;
    }

    public void setRepCode(String repCode) {
        this.repCode = repCode;
    }

    private Boolean activeFlg;

    public Boolean isActiveFlg() {
        return activeFlg;
    }

    public void setActiveFlg(Boolean activeFlg) {
        this.activeFlg = activeFlg;
    }
    
    public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Country getCountry() {
		return country;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisplaySalesRep that = (DisplaySalesRep) o;

        if (displaySalesRepId != null ? !displaySalesRepId.equals(that.displaySalesRepId) : that.displaySalesRepId != null)
            return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (repCode != null ? !repCode.equals(that.repCode) : that.repCode != null) return false;
        if (activeFlg != null ? !activeFlg.equals(that.activeFlg) : that.activeFlg != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = displaySalesRepId != null ? displaySalesRepId.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (repCode != null ? repCode.hashCode() : 0);
        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
        return result;
    }

    private Collection<Display> displaysByDisplaySalesRepId;

    public Collection<Display> getDisplaysByDisplaySalesRepId() {
        return displaysByDisplaySalesRepId;
    }

    public void setDisplaysByDisplaySalesRepId(Collection<Display> displaysByDisplaySalesRepId) {
        this.displaysByDisplaySalesRepId = displaysByDisplaySalesRepId;
    }

    @Override
    public String toString() {
        return "DisplaySalesRep{" +
                "displaySalesRepId=" + displaySalesRepId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", repCode='" + repCode + '\'' +
                ", displaysByDisplaySalesRepId=" + displaysByDisplaySalesRepId +
                '}';
    }
}
