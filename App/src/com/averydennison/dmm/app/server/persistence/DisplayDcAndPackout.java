package com.averydennison.dmm.app.server.persistence;

import java.util.Date;

/**
 * User: Alvin Wong
 * Date: Jan 18, 2010
 * Time: 02:50:40 PM
 */
public class DisplayDcAndPackout {
	private Long displayDcId;
	private Long displayId;
	private Long locationId;
	private String dcCode;
	private Long dcQuantity;
	private Date dtCreated;
	private Date dtLastChanged;
	private String userCreated;
	private String userLastChanged;
	private Long shipWaveNum;
	private Date mustArriveDate;
	private Long displayPackoutId;
	private Long displayShipWaveId;
	private Date shipFromDate;
	private Date corrugateDueDate;
	private Date deliveryDate;
	private Date shipLocProductDueDate;
	private Date productDueDate;
	private Long poQuantity;
	private Date poDtCreated;
	private Date poDtLastChanged;
	private String poUserCreated;
	private String poUserLastChanged;
	private Long shipFromLocationId;
	private String shipFromDcCode;
	
    public Long getDisplayDcId() {
		return displayDcId;
	}

	public void setDisplayDcId(Long displayDcId) {
		this.displayDcId = displayDcId;
	}

	public Long getDisplayId() {
		return displayId;
	}

	public void setDisplayId(Long displayId) {
		this.displayId = displayId;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getDcCode() {
		return dcCode;
	}

	public void setDcCode(String dcCode) {
		this.dcCode = dcCode;
	}

	public Long getDcQuantity() {
		return dcQuantity;
	}

	public void setDcQuantity(Long dcQuantity) {
		this.dcQuantity = dcQuantity;
	}

	public Date getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(Date dtCreated) {
		this.dtCreated = dtCreated;
	}

	public Date getDtLastChanged() {
		return dtLastChanged;
	}

	public void setDtLastChanged(Date dtLastChanged) {
		this.dtLastChanged = dtLastChanged;
	}

	public String getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	public String getUserLastChanged() {
		return userLastChanged;
	}

	public void setUserLastChanged(String userLastChanged) {
		this.userLastChanged = userLastChanged;
	}

	public Long getShipWaveNum() {
		return shipWaveNum;
	}

	public void setShipWaveNum(Long shipWaveNum) {
		this.shipWaveNum = shipWaveNum;
	}

	public Date getMustArriveDate() {
		return mustArriveDate;
	}

	public void setMustArriveDate(Date mustArriveDate) {
		this.mustArriveDate = mustArriveDate;
	}

	public Long getDisplayPackoutId() {
		return displayPackoutId;
	}

	public void setDisplayPackoutId(Long displayPackoutId) {
		this.displayPackoutId = displayPackoutId;
	}

	public Long getDisplayShipWaveId() {
		return displayShipWaveId;
	}

	public void setDisplayShipWaveId(Long displayShipWaveId) {
		this.displayShipWaveId = displayShipWaveId;
	}

	public Date getShipFromDate() {
		return shipFromDate;
	}

	public void setShipFromDate(Date shipFromDate) {
		this.shipFromDate = shipFromDate;
	}

	public Date getCorrugateDueDate() {
		return corrugateDueDate;
	}

	public void setCorrugateDueDate(Date corrugateDueDate) {
		this.corrugateDueDate = corrugateDueDate;
	}
	
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Date getShipLocProductDueDate() {
		return shipLocProductDueDate;
	}

	public void setShipLocProductDueDate(Date shipLocProductDueDate) {
		this.shipLocProductDueDate = shipLocProductDueDate;
	}
	
	public Date getProductDueDate() {
		return productDueDate;
	}

	public void setProductDueDate(Date productDueDate) {
		this.productDueDate = productDueDate;
	}

	public Long getPoQuantity() {
		return poQuantity;
	}

	public void setPoQuantity(Long poQuantity) {
		this.poQuantity = poQuantity;
	}

	public Date getPoDtCreated() {
		return poDtCreated;
	}

	public void setPoDtCreated(Date poDtCreated) {
		this.poDtCreated = poDtCreated;
	}

	public Date getPoDtLastChanged() {
		return poDtLastChanged;
	}

	public void setPoDtLastChanged(Date poDtLastChanged) {
		this.poDtLastChanged = poDtLastChanged;
	}

	public String getPoUserCreated() {
		return poUserCreated;
	}

	public void setPoUserCreated(String poUserCreated) {
		this.poUserCreated = poUserCreated;
	}

	public String getPoUserLastChanged() {
		return poUserLastChanged;
	}

	public void setPoUserLastChanged(String poUserLastChanged) {
		this.poUserLastChanged = poUserLastChanged;
	}
	
	public Long getShipFromLocationId() {
		return shipFromLocationId;
	}

	public void setShipFromLocationId(Long shipFromLocationId) {
		this.shipFromLocationId = shipFromLocationId;
	}
	 
	public String getShipFromDcCode() {
		return shipFromDcCode;
	}

	public void setShipFromDcCode(String shipFromDcCode) {
		this.shipFromDcCode = shipFromDcCode;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisplayDcAndPackout displayDc = (DisplayDcAndPackout) o;

        if (displayDcId != null ? !displayDcId.equals(displayDc.displayDcId) : displayDc.displayDcId != null)
            return false;
        if (displayId != null ? !displayId.equals(displayDc.displayId) : displayDc.displayId != null) return false;
        if (locationId != null ? !locationId.equals(displayDc.locationId) : displayDc.locationId != null) return false;
        if (dcCode != null ? !dcCode.equals(displayDc.dcCode) : displayDc.dcCode != null) return false;
        if (dcQuantity != null ? !dcQuantity.equals(displayDc.dcQuantity) : displayDc.dcQuantity != null) return false;
        if (dtCreated != null ? !dtCreated.equals(displayDc.dtCreated) : displayDc.dtCreated != null) return false;
        if (dtLastChanged != null ? !dtLastChanged.equals(displayDc.dtLastChanged) : displayDc.dtLastChanged != null)
            return false;
        if (userCreated != null ? !userCreated.equals(displayDc.userCreated) : displayDc.userCreated != null)
            return false;
        if (userLastChanged != null ? !userLastChanged.equals(displayDc.userLastChanged) : displayDc.userLastChanged != null)
            return false;
        if (shipWaveNum != null ? !shipWaveNum.equals(displayDc.shipWaveNum) : displayDc.shipWaveNum != null) return false;
        if (mustArriveDate != null ? !mustArriveDate.equals(displayDc.mustArriveDate) : displayDc.mustArriveDate != null) return false;
        if (displayPackoutId != null ? !displayPackoutId.equals(displayDc.displayPackoutId) : displayDc.displayPackoutId != null) return false;
        if (displayShipWaveId != null ? !displayShipWaveId.equals(displayDc.displayShipWaveId) : displayDc.displayShipWaveId != null) return false;
        if (shipFromDate != null ? !shipFromDate.equals(displayDc.shipFromDate) : displayDc.shipFromDate != null) return false;
        if (productDueDate != null ? !productDueDate.equals(displayDc.productDueDate) : displayDc.productDueDate != null) return false;
        if (poQuantity != null ? !poQuantity.equals(displayDc.poQuantity) : displayDc.poQuantity != null) return false;        
        if (poDtCreated != null ? !poDtCreated.equals(displayDc.poDtCreated) : displayDc.poDtCreated != null) return false;
        if (poDtLastChanged != null ? !poDtLastChanged.equals(displayDc.poDtLastChanged) : displayDc.poDtLastChanged != null)
            return false;
        if (poUserCreated != null ? !poUserCreated.equals(displayDc.poUserCreated) : displayDc.poUserCreated != null)
            return false;
        if (poUserLastChanged != null ? !poUserLastChanged.equals(displayDc.poUserLastChanged) : displayDc.poUserLastChanged != null)
            return false;
        if (shipFromLocationId != null ? !shipFromLocationId.equals(displayDc.shipFromLocationId) : displayDc.shipFromLocationId != null) return false;
        if (shipFromDcCode != null ? !shipFromDcCode.equals(displayDc.shipFromDcCode) : displayDc.shipFromDcCode != null) return false;
        if (corrugateDueDate != null ? !corrugateDueDate.equals(displayDc.corrugateDueDate) : displayDc.corrugateDueDate != null) return false;
        if (shipLocProductDueDate != null ? !shipLocProductDueDate.equals(displayDc.shipLocProductDueDate) : displayDc.shipLocProductDueDate != null) return false;

        return true;
    }
	
    @Override
    public int hashCode() {
        int result = displayDcId != null ? displayDcId.hashCode() : 0;
        result = 31 * result + (displayId != null ? displayId.hashCode() : 0);
        result = 31 * result + (locationId != null ? locationId.hashCode() : 0);
        result = 31 * result + (dcCode != null ? dcCode.hashCode() : 0);
        result = 31 * result + (shipFromLocationId != null ? shipFromLocationId.hashCode() : 0);
        result = 31 * result + (shipFromDcCode != null ? shipFromDcCode.hashCode() : 0);
        result = 31 * result + (dcQuantity != null ? dcQuantity.hashCode() : 0);
        result = 31 * result + (dtCreated != null ? dtCreated.hashCode() : 0);
        result = 31 * result + (dtLastChanged != null ? dtLastChanged.hashCode() : 0);
        result = 31 * result + (userCreated != null ? userCreated.hashCode() : 0);
        result = 31 * result + (userLastChanged != null ? userLastChanged.hashCode() : 0);
        result = 31 * result + (shipWaveNum != null ? shipWaveNum.hashCode() : 0);
        result = 31 * result + (mustArriveDate != null ? mustArriveDate.hashCode() : 0);
        result = 31 * result + (displayPackoutId != null ? displayPackoutId.hashCode() : 0);
        result = 31 * result + (displayShipWaveId != null ? displayShipWaveId.hashCode() : 0);
        result = 31 * result + (shipFromDate != null ? shipFromDate.hashCode() : 0);
        result = 31 * result + (corrugateDueDate != null ? corrugateDueDate.hashCode() : 0);
        result = 31 * result + (shipLocProductDueDate != null ? shipLocProductDueDate.hashCode() : 0);
        result = 31 * result + (productDueDate != null ? productDueDate.hashCode() : 0);
        result = 31 * result + (poQuantity != null ? poQuantity.hashCode() : 0);
        result = 31 * result + (poDtCreated != null ? poDtCreated.hashCode() : 0);
        result = 31 * result + (poDtLastChanged != null ? poDtLastChanged.hashCode() : 0);
        result = 31 * result + (poUserCreated != null ? poUserCreated.hashCode() : 0);
        result = 31 * result + (poUserLastChanged != null ? poUserLastChanged.hashCode() : 0);
        return result;
    }
	

    @Override
    public String toString() {
        return "DisplayDc{" +
                "displayDcId=" + displayDcId +
                ", displayId=" + displayId +
                ", locationId=" + locationId +
                ", dcCode=" + dcCode +
                ", shipFromDcCode=" + shipFromDcCode +
                ", dcQuantity=" + dcQuantity +
                ", dtCreated=" + dtCreated +
                ", dtLastChanged=" + dtLastChanged +
                ", userCreated='" + userCreated + '\'' +
                ", userLastChanged='" + userLastChanged + '\'' +
                ", shipWaveNum=" + shipWaveNum +
                ", mustArriveDate=" + mustArriveDate +
                ", displayPackoutId=" + displayPackoutId +
                ", displayShipWaveId=" + displayShipWaveId +
                ", shipFromDate=" + shipFromDate +
                ", corrugateDueDate=" + corrugateDueDate +
                ", shipFromLocationId=" + shipFromLocationId +
                ", shipLocProductDueDate=" + shipLocProductDueDate +
                ", productDueDate=" + productDueDate +
                ", poQuantity=" + poQuantity +
                ", poDtCreated=" + poDtCreated +
                ", poDtLastChanged=" + poDtLastChanged +
                ", poUserCreated='" + poUserCreated + '\'' +
                ", poUserLastChanged='" + poUserLastChanged + '\'' +
                '}';
    }
}