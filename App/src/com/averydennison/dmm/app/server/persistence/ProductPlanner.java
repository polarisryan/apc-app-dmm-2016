package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:54:06 AM
 */
public class ProductPlanner implements Serializable {
    private Long skuId;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    private String plantCode;

    public String getPlantCode() {
        return plantCode;
    }

    public void setPlantCode(String plantCode) {
        this.plantCode = plantCode;
    }

    private String dcCode;

    public String getDcCode() {
        return dcCode;
    }

    public void setDcCode(String dcCode) {
        this.dcCode = dcCode;
    }

    private String plannerCode;

    public String getPlannerCode() {
        return plannerCode;
    }

    public void setPlannerCode(String plannerCode) {
        this.plannerCode = plannerCode;
    }

    private String plantDesc;

    public String getPlantDesc() {
        return plantDesc;
    }

    public void setPlantDesc(String plantDesc) {
        this.plantDesc = plantDesc;
    }

    private String dcName;

    public String getDcName() {
        return dcName;
    }

    public void setDcName(String dcName) {
        this.dcName = dcName;
    }

    private String plannerDesc;

    public String getPlannerDesc() {
        return plannerDesc;
    }

    public void setPlannerDesc(String plannerDesc) {
        this.plannerDesc = plannerDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductPlanner that = (ProductPlanner) o;

        if (dcCode != null ? !dcCode.equals(that.dcCode) : that.dcCode != null) return false;
        if (dcName != null ? !dcName.equals(that.dcName) : that.dcName != null) return false;
        if (plannerCode != null ? !plannerCode.equals(that.plannerCode) : that.plannerCode != null) return false;
        if (plannerDesc != null ? !plannerDesc.equals(that.plannerDesc) : that.plannerDesc != null) return false;
        if (plantCode != null ? !plantCode.equals(that.plantCode) : that.plantCode != null) return false;
        if (plantDesc != null ? !plantDesc.equals(that.plantDesc) : that.plantDesc != null) return false;
        if (skuId != null ? !skuId.equals(that.skuId) : that.skuId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = skuId != null ? skuId.hashCode() : 0;
        result = 31 * result + (plantCode != null ? plantCode.hashCode() : 0);
        result = 31 * result + (dcCode != null ? dcCode.hashCode() : 0);
        result = 31 * result + (plannerCode != null ? plannerCode.hashCode() : 0);
        result = 31 * result + (plantDesc != null ? plantDesc.hashCode() : 0);
        result = 31 * result + (dcName != null ? dcName.hashCode() : 0);
        result = 31 * result + (plannerDesc != null ? plannerDesc.hashCode() : 0);
        return result;
    }
}
