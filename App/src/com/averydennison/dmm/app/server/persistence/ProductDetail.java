package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;

/**
 * User: Spart Arguello
 * Date: Nov 3, 2009
 * Time: 11:54:05 AM
 */

/**
 * User: Marianandan Arockiasamy
 * Date: Jul 8, 2014
 * Time: 04:29:32 PM
 */

public class ProductDetail implements Serializable {
    private Long skuId;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    private String sku;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    private String buDesc;

    public String getBuDesc() {
        return buDesc;
    }

    public void setBuDesc(String buDesc) {
        this.buDesc = buDesc;
    }

    private String versionNo;

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    private Boolean promoFlg;

    public Boolean isPromoFlg() {
        return promoFlg;
    }

    public void setPromoFlg(Boolean promoFlg) {
        this.promoFlg = promoFlg;
    }

    private String breakCase;

    public String getBreakCase() {
        return breakCase;
    }

    public void setBreakCase(String breakCase) {
        this.breakCase = breakCase;
    }

    private Float benchMarkPrice;

    public Float getBenchMarkPrice() {
        return benchMarkPrice;
    }

    public void setBenchMarkPrice(Float benchMarkPrice) {
        this.benchMarkPrice = benchMarkPrice;
    }

    private Float listPrice;

    public Float getListPrice() {
        return listPrice;
    }

    public void setListPrice(Float listPrice) {
        this.listPrice = listPrice;
    }

    private Float fullBurdenCost;

    public Float getFullBurdenCost() {
        return fullBurdenCost;
    }

    public void setFullBurdenCost(Float fullBurdenCost) {
        this.fullBurdenCost = fullBurdenCost;
    }
    
    private Long innerpackQty;

    public Long getInnerpackQty() {
        return innerpackQty;
    }

    public void setInnerpackQty(Long innerpackQty) {
        this.innerpackQty = innerpackQty;
    }

    private String ilsSkuDesc;

    public String getIlsSkuDesc() {
        return ilsSkuDesc;
    }

    public void setIlsSkuDesc(String ilsSkuDesc) {
        this.ilsSkuDesc = ilsSkuDesc;
    }
    private Float canBenchMarkPrice;

    public Float getCanBenchMarkPrice() {
        return canBenchMarkPrice;
    }

    public void setCanBenchMarkPrice(Float canBenchMarkPrice) {
        this.canBenchMarkPrice = canBenchMarkPrice;
    }

    private Float canListPrice;

    public Float getCanListPrice() {
        return canListPrice;
    }

    public void setCanListPrice(Float canListPrice) {
        this.canListPrice = canListPrice;
    }

    private Float canFullBurdenCost;

    public Float getCanFullBurdenCost() {
        return canFullBurdenCost;
    }

    public void setCanFullBurdenCost(Float canFullBurdenCost) {
        this.canFullBurdenCost = canFullBurdenCost;
    }
    
    
    private Float canLaborCost;

    public Float getCanLaborCost() {
        return canLaborCost;
    }

    public void setCanLaborCost(Float canLaborCost) {
        this.canLaborCost = canLaborCost;
    }
    
    private Float canMaterialCost;

    public Float getCanMaterialCost() {
        return canMaterialCost;
    }

    public void setCanMaterialCost(Float canMaterialCost) {
        this.canMaterialCost = canMaterialCost;
    }
    

    private Float materialCost;

    public Float getMaterialCost() {
        return materialCost;
    }

    public void setMaterialCost(Float materialCost) {
        this.materialCost = materialCost;
    }

    private Float laborCost;

    public Float getLaborCost() {
        return laborCost;
    }

    public void setLaborCost(Float laborCost) {
        this.laborCost = laborCost;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductDetail that = (ProductDetail) o;

        if (benchMarkPrice != null ? !benchMarkPrice.equals(that.benchMarkPrice) : that.benchMarkPrice != null)
            return false;
        if (breakCase != null ? !breakCase.equals(that.breakCase) : that.breakCase != null) return false;
        if (buDesc != null ? !buDesc.equals(that.buDesc) : that.buDesc != null) return false;
        if (listPrice != null ? !listPrice.equals(that.listPrice) : that.listPrice != null) return false;
        if (productName != null ? !productName.equals(that.productName) : that.productName != null) return false;
        if (promoFlg != null ? !promoFlg.equals(that.promoFlg) : that.promoFlg != null) return false;
        if (sku != null ? !sku.equals(that.sku) : that.sku != null) return false;
        if (skuId != null ? !skuId.equals(that.skuId) : that.skuId != null) return false;
        if (versionNo != null ? !versionNo.equals(that.versionNo) : that.versionNo != null) return false;
        if (fullBurdenCost != null ? !fullBurdenCost.equals(that.fullBurdenCost) : that.fullBurdenCost != null)
            return false;
        if (innerpackQty != null ? !innerpackQty.equals(that.innerpackQty) : that.innerpackQty != null) return false;
        if (ilsSkuDesc != null ? !ilsSkuDesc.equals(that.ilsSkuDesc) : that.ilsSkuDesc != null) return false;
        if (canBenchMarkPrice != null ? !canBenchMarkPrice.equals(that.canBenchMarkPrice) : that.canBenchMarkPrice != null)
            return false;
        if (canListPrice != null ? !canListPrice.equals(that.canListPrice) : that.canListPrice != null) return false;
        if (canFullBurdenCost != null ? !canFullBurdenCost.equals(that.canFullBurdenCost) : that.canFullBurdenCost != null) return false;

        if (canMaterialCost != null ? !canMaterialCost.equals(that.canMaterialCost) : that.canMaterialCost != null) return false;
        if (canLaborCost != null ? !canLaborCost.equals(that.canLaborCost) : that.canLaborCost != null) return false;
        if (materialCost != null ? !materialCost.equals(that.materialCost) : that.materialCost != null) return false;
        if (laborCost != null ? !laborCost.equals(that.laborCost) : that.laborCost != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = skuId != null ? skuId.hashCode() : 0;
        result = 31 * result + (sku != null ? sku.hashCode() : 0);
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (buDesc != null ? buDesc.hashCode() : 0);
        result = 31 * result + (versionNo != null ? versionNo.hashCode() : 0);
        result = 31 * result + (promoFlg != null ? promoFlg.hashCode() : 0);
        result = 31 * result + (breakCase != null ? breakCase.hashCode() : 0);
        result = 31 * result + (benchMarkPrice != null ? benchMarkPrice.hashCode() : 0);
        result = 31 * result + (listPrice != null ? listPrice.hashCode() : 0);
        result = 31 * result + (fullBurdenCost != null ? fullBurdenCost.hashCode() : 0);
        result = 31 * result + (innerpackQty != null ? innerpackQty.hashCode() : 0);
        result = 31 * result + (ilsSkuDesc != null ? ilsSkuDesc.hashCode() : 0);
        result = 31 * result + (canBenchMarkPrice != null ? canBenchMarkPrice.hashCode() : 0);
        result = 31 * result + (canListPrice != null ? canListPrice.hashCode() : 0);
        result = 31 * result + (canFullBurdenCost != null ? canFullBurdenCost.hashCode() : 0);
        
        result = 31 * result + (canMaterialCost != null ? canMaterialCost.hashCode() : 0);
        result = 31 * result + (canLaborCost != null ? canLaborCost.hashCode() : 0);
        result = 31 * result + (materialCost != null ? materialCost.hashCode() : 0);
        result = 31 * result + (laborCost != null ? laborCost.hashCode() : 0);
        return result;
    }
}
