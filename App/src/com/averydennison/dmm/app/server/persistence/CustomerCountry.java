package com.averydennison.dmm.app.server.persistence;

import java.io.Serializable;
import java.util.Collection;

public class CustomerCountry implements Serializable {
	    private Long customerId;

	    public Long getCustomerId() {
	        return customerId;
	    }

	    public void setCustomerId(Long customerId) {
	        this.customerId = customerId;
	    }

	    private String customerName;

	    public String getCustomerName() {
	        return customerName;
	    }

	    public void setCustomerName(String customerName) {
	        this.customerName = customerName;
	    }

	    private Boolean activeFlg;

	    public Boolean isActiveFlg() {
	        return activeFlg;
	    }

	    public void setActiveFlg(Boolean activeFlg) {
	        this.activeFlg = activeFlg;
	    }

	    Long countryId;
	    public Long getCountryId() {
		     return countryId;
		 }

		 public void setCountryId(Long countryId) {
		        this.countryId=countryId;
		 }

		 String countryName;
		 public String getCountryName() {
		     return countryName;
		 }

		 public void setCountryName(String countryName) {
		        this.countryName= countryName;
		 }
		 
		 private Boolean activeCountryFlg;
		 public Boolean isActiveCountryFlg() {
		      return activeCountryFlg;
		 }

		 public void setActiveCountryFlg(Boolean activeCountryFlg) {
		     this.activeCountryFlg = activeCountryFlg;
		 }
		    
	    @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;

	        CustomerCountry customerCountry = (CustomerCountry) o;
	        if (customerId != null ? !customerId.equals(customerCountry.customerId) : customerCountry.customerId != null) return false;
	        if (customerName != null ? !customerName.equals(customerCountry.customerName) : customerCountry.customerName != null)
	            return false;
	        if (activeFlg != null ? !activeFlg.equals(customerCountry.activeFlg) : customerCountry.activeFlg != null) return false;

	        return true;
	    }

	    @Override
	    public int hashCode() {
	        int result = customerId != null ? customerId.hashCode() : 0;
	        result = 31 * result + (customerName != null ? customerName.hashCode() : 0);
	        result = 31 * result + (activeFlg != null ? activeFlg.hashCode() : 0);
	        return result;
	    }

	    private Collection<Display> displaysByCustomerId;

	    public Collection<Display> getDisplaysByCustomerId() {
	        return displaysByCustomerId;
	    }

	    public void setDisplaysByCustomerId(Collection<Display> displaysByCustomerId) {
	        this.displaysByCustomerId = displaysByCustomerId;
	    }

	    @Override
	    public String toString() {
	        return "CustomerCountry{" +
	                "customerId=" + customerId +
	                ", customerName='" + customerName + '\'' +
	                ", displaysByCustomerId=" + displaysByCustomerId +
	                ", countryId=" + countryId +
	                ", countryName='" + countryName + '\'' +
	                '}';
	    }
	}

