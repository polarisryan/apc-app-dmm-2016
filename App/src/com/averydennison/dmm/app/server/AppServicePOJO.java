package com.averydennison.dmm.app.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.gwtwidgets.server.spring.ServletUtils;

import com.averydennison.dmm.app.client.AppService;
import com.averydennison.dmm.app.client.KitComponentDetail;
import com.averydennison.dmm.app.client.exception.DuplicateException;
import com.averydennison.dmm.app.client.models.BooleanDTO;
import com.averydennison.dmm.app.client.models.BuCostCenterSplitDTO;
import com.averydennison.dmm.app.client.models.CalculationSummaryDTO;
import com.averydennison.dmm.app.client.models.CorrugateComponentsDetailDTO;
import com.averydennison.dmm.app.client.models.CorrugateDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisHistoryDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.averydennison.dmm.app.client.models.CostPriceDTO;
import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.CstBuPrgmPctDTO;
import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.CustomerMarketingDTO;
import com.averydennison.dmm.app.client.models.CustomerTypeDTO;
import com.averydennison.dmm.app.client.models.DisplayCostCommentDTO;
import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.client.models.DisplayCostImageDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayDcAndPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayDcDTO;
import com.averydennison.dmm.app.client.models.DisplayForMassUpdateDTO;
import com.averydennison.dmm.app.client.models.DisplayLogisticsDTO;
import com.averydennison.dmm.app.client.models.DisplayPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayPlannerDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.client.models.InPogDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.LocationDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerDTO;
import com.averydennison.dmm.app.client.models.ProductDetailDTO;
import com.averydennison.dmm.app.client.models.ProgramBuDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodDTO;
import com.averydennison.dmm.app.client.models.SlimPackoutDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.averydennison.dmm.app.client.models.UserDTO;
import com.averydennison.dmm.app.client.models.UserTypeDTO;
import com.averydennison.dmm.app.client.models.ValidBuDTO;
import com.averydennison.dmm.app.client.models.ValidLocationDTO;
import com.averydennison.dmm.app.client.models.ValidPlannerDTO;
import com.averydennison.dmm.app.client.models.ValidPlantDTO;
import com.averydennison.dmm.app.client.util.Constants;
import com.averydennison.dmm.app.server.dao.util.ConverterFactory;
import com.averydennison.dmm.app.server.managers.CalculationSummaryManager;
import com.averydennison.dmm.app.server.managers.CorrugateComponentManager;
import com.averydennison.dmm.app.server.managers.CorrugateManager;
import com.averydennison.dmm.app.server.managers.CountryManager;
import com.averydennison.dmm.app.server.managers.CstBuPrgmPctManager;
import com.averydennison.dmm.app.server.managers.CustomerCountryManager;
import com.averydennison.dmm.app.server.managers.CustomerManager;
import com.averydennison.dmm.app.server.managers.CustomerMarketingManager;
import com.averydennison.dmm.app.server.managers.CustomerPctManager;
import com.averydennison.dmm.app.server.managers.DisplayCostCommentManager;
import com.averydennison.dmm.app.server.managers.DisplayCostImageManager;
import com.averydennison.dmm.app.server.managers.DisplayCostManager;
import com.averydennison.dmm.app.server.managers.DisplayDcManager;
import com.averydennison.dmm.app.server.managers.DisplayLogisticsManager;
import com.averydennison.dmm.app.server.managers.DisplayManager;
import com.averydennison.dmm.app.server.managers.DisplayMaterialManager;
import com.averydennison.dmm.app.server.managers.DisplayPackoutManager;
import com.averydennison.dmm.app.server.managers.DisplayPlannerManager;
import com.averydennison.dmm.app.server.managers.DisplaySalesRepManager;
import com.averydennison.dmm.app.server.managers.DisplayScenarioManager;
import com.averydennison.dmm.app.server.managers.DisplayShipWaveManager;
import com.averydennison.dmm.app.server.managers.KitComponentManager;
import com.averydennison.dmm.app.server.managers.LocationCountryManager;
import com.averydennison.dmm.app.server.managers.LocationManager;
import com.averydennison.dmm.app.server.managers.ManufacturerCountryManager;
import com.averydennison.dmm.app.server.managers.ManufacturerManager;
import com.averydennison.dmm.app.server.managers.ProductDetailManager;
import com.averydennison.dmm.app.server.managers.ProductPlannerManager;
import com.averydennison.dmm.app.server.managers.ProgramBuManager;
import com.averydennison.dmm.app.server.managers.PromoPeriodCountryManager;
import com.averydennison.dmm.app.server.managers.PromoPeriodManager;
import com.averydennison.dmm.app.server.managers.ScenarioStatusManager;
import com.averydennison.dmm.app.server.managers.StatusManager;
import com.averydennison.dmm.app.server.managers.StructureManager;
import com.averydennison.dmm.app.server.managers.UserChangeLogManager;
import com.averydennison.dmm.app.server.managers.UserManager;
import com.averydennison.dmm.app.server.managers.ValidBuManager;
import com.averydennison.dmm.app.server.managers.ValidLocationManager;
import com.averydennison.dmm.app.server.managers.ValidPlannerManager;
import com.averydennison.dmm.app.server.managers.ValidPlantManager;
import com.averydennison.dmm.app.server.persistence.CalculationSummary;
import com.averydennison.dmm.app.server.persistence.Corrugate;
import com.averydennison.dmm.app.server.persistence.CorrugateComponent;
import com.averydennison.dmm.app.server.persistence.CorrugateComponentsDetail;
import com.averydennison.dmm.app.server.persistence.CostAnalysisKitComponent;
import com.averydennison.dmm.app.server.persistence.CstBuPrgmPct;
import com.averydennison.dmm.app.server.persistence.Customer;
import com.averydennison.dmm.app.server.persistence.CustomerCountry;
import com.averydennison.dmm.app.server.persistence.CustomerMarketing;
import com.averydennison.dmm.app.server.persistence.CustomerPct;
import com.averydennison.dmm.app.server.persistence.Display;
import com.averydennison.dmm.app.server.persistence.DisplayCost;
import com.averydennison.dmm.app.server.persistence.DisplayCostComment;
import com.averydennison.dmm.app.server.persistence.DisplayCostImage;
import com.averydennison.dmm.app.server.persistence.DisplayDc;
import com.averydennison.dmm.app.server.persistence.DisplayForMassUpdate;
import com.averydennison.dmm.app.server.persistence.DisplayLogistics;
import com.averydennison.dmm.app.server.persistence.DisplayPackout;
import com.averydennison.dmm.app.server.persistence.DisplayPlanner;
import com.averydennison.dmm.app.server.persistence.DisplayScenario;
import com.averydennison.dmm.app.server.persistence.DisplayShipWave;
import com.averydennison.dmm.app.server.persistence.KitComponent;
import com.averydennison.dmm.app.server.persistence.Manufacturer;
import com.averydennison.dmm.app.server.persistence.ManufacturerCountry;
import com.averydennison.dmm.app.server.persistence.ProductDetail;
import com.averydennison.dmm.app.server.persistence.ProductPlanner;
import com.averydennison.dmm.app.server.persistence.ProgramBu;
import com.extjs.gxt.ui.client.data.BaseModelData;

/**
 * User: Spart Arguello
 * Date: Oct 13, 2009
 * Time: 2:14:18 PM
 */
public class AppServicePOJO implements AppService {
	private CustomerManager customerManager;
	private CustomerMarketingManager customerMarketingManager;
	private DisplayCostManager displayCostManager;
	private DisplayDcManager displayDcManager;
	private DisplayLogisticsManager displayLogisticsManager;
	private DisplayManager displayManager;
	private DisplayMaterialManager displayMaterialManager;
	private DisplayPackoutManager displayPackoutManager;
	private DisplayPlannerManager displayPlannerManager;
	private DisplaySalesRepManager displaySalesRepManager;
	private DisplayShipWaveManager displayShipWaveManager;
	private KitComponentManager kitComponentManager;
	private LocationManager locationManager;
	private ManufacturerManager manufacturerManager;
	private ProductDetailManager productDetailManager;
	private ProductPlannerManager productPlannerManager;
	private PromoPeriodManager promoPeriodManager;
	private StatusManager statusManager;
	private StructureManager structureManager;
	private UserManager userManager;
	private ValidPlantManager validPlantManager;
	private ValidPlannerManager validPlannerManager;
	private ValidLocationManager validLocationManager;
	private ValidBuManager validBuManager;
	private DisplayCostCommentManager displayCostCommentManager;
	private UserChangeLogManager userChangeLogManager;
	private DisplayCostImageManager displayCostImageManager;
	private CustomerPctManager customerPctManager;
	private DisplayScenarioManager displayScenarioManager;
	private ScenarioStatusManager scenarioStatusManager;
	private CorrugateManager corrugateManager;
	private CorrugateComponentManager corrugateComponentManager;
	private CountryManager countryManager;
	private CalculationSummaryManager calculationSummaryManager;
	private LocationCountryManager locationCountryManager;
	private ManufacturerCountryManager manufacturerCountryManager;
	private PromoPeriodCountryManager promoPeriodCountryManager;
	private CustomerCountryManager customerCountryManager;
	private ProgramBuManager programBuManager;
	private CstBuPrgmPctManager cstBuPrgmPctManager;

	private enum CacheType{MANUFACTURER, CUSTOMER, LOCATION, STRUCTURE, USER};

	private Map<Boolean, InPogDTO> inPogDTOMap = new HashMap<Boolean, InPogDTO>();
	private Map<Long, PromoPeriodDTO> promoPeriodDTOMap = new HashMap<Long, PromoPeriodDTO>();
	private Map<Long, PromoPeriodCountryDTO> promoPeriodCountryDTOMap = new HashMap<Long, PromoPeriodCountryDTO>();
	private Map<Long, ManufacturerDTO> manufacturerDTOMap = new HashMap<Long, ManufacturerDTO>();
	private Map<Long, ManufacturerCountryDTO> manufacturerCountryDTOMap = new HashMap<Long, ManufacturerCountryDTO>();
	private Map<Long, CountryDTO> countryDTOMap = new HashMap<Long, CountryDTO>();
	private Map<Long, LocationDTO> locationDTOMap = new HashMap<Long, LocationDTO>();
	private Map<Long, LocationCountryDTO> locationCountryDTOMap = new HashMap<Long, LocationCountryDTO>();

	public Map<Long, LocationDTO> getLocationMapDTOs() {
		if (locationDTOMap.size() == 0) {
			loadLocationMap();
		}
		return locationDTOMap;
	}
	
	public Map<Long, LocationCountryDTO> getLocationCountryMapDTOs() {
		if (locationCountryDTOMap.size() == 0) {
			loadLocationMap();
		}
		return locationCountryDTOMap;
	}
	
	public Map<Long, LocationDTO> getDefaultLocationMapDTOs() {
		if (locationDTOMap.size() == 0) {
			loadDefaultLocationMap();
		}
		return locationDTOMap;
	}
	
	public Map<Long, PromoPeriodDTO> getPromoPeriodMapDTOs() {
		if (promoPeriodDTOMap.size() == 0) {
			loadPromoPeriodMap();
		}
		return promoPeriodDTOMap;
	}

	private void loadPromoPeriodMap() {
		if (promoPeriodDTOs == null || promoPeriodDTOs.size() == 0) {
			promoPeriodDTOs = getPromoPeriodDTOs();
		}

		for (int i = 0; i < promoPeriodDTOs.size(); i++) {
			promoPeriodDTOMap.put(promoPeriodDTOs.get(i).getPromoPeriodId(), promoPeriodDTOs.get(i));
		}
	}

	
	public Map<Long, PromoPeriodCountryDTO> getPromoPeriodCountryMapDTOs() {
		if (promoPeriodCountryDTOMap.size() == 0) {
			loadPromoPeriodCountryMap();
		}
		return promoPeriodCountryDTOMap;
	}

	private void loadPromoPeriodCountryMap() {
		if (promoPeriodCountryDTOs == null || promoPeriodCountryDTOs.size() == 0) {
			promoPeriodCountryDTOs = getPromoPeriodCountryDTOs();
		}

		for (int i = 0; i < promoPeriodCountryDTOs.size(); i++) {
			promoPeriodCountryDTOMap.put(promoPeriodCountryDTOs.get(i).getPromoPeriodId(), promoPeriodCountryDTOs.get(i));
		}
	}
	
	public Map<Boolean, InPogDTO> getInPogMapDTOs() {
		if (inPogDTOMap.size() == 0) {
			loadInPogMap();
		}
		return inPogDTOMap;
	}

	private void loadInPogMap() {
		if (inPogDTOs == null || inPogDTOs.size() == 0) {
			inPogDTOs = getInPogDTOs();
		}

		for (int i = 0; i < inPogDTOs.size(); i++) {
			inPogDTOMap.put(inPogDTOs.get(i).getInPogYorNId(), inPogDTOs.get(i));
		}
	}
	
	private void loadDefaultLocationMap() {
		if (locationDTOs == null || locationDTOs.size() == 0) {
			locationDTOs = getLocationDTOs();
		}
		for (int i = 0; i < locationDTOs.size(); i++) {
			locationDTOMap.put(locationDTOs.get(i).getLocationId(), locationDTOs.get(i));
		}
	}
	
	private void loadLocationMap() {
		if (validLocationDTOs == null || validLocationDTOs.size() == 0) {
			validLocationDTOs = getValidLocationDTOs();
		}

		for (int i = 0; i < validLocationDTOs.size(); i++) {
			//locationDTOMap.put(validLocationDTOs.get(i).getDcCode(), validLocationDTOs.get(i));
		}
	}
	
	public DisplayDTO getDisplayDTO(DisplayDTO displayDTO) {
		Display display = displayManager.getDisplayDAO().findById(displayDTO.getDisplayId());
		return ConverterFactory.convert(display, true);
	}

	private Map<Long, CustomerDTO> customerMap = null;

	public Map<Long, CustomerDTO> getCustomerDTOMap() {
		if (customerMap == null) {
			loadCustomerDTOMap();
		}
		return customerMap;
	}

	private void loadCustomerDTOMap() {
		customerMap = new HashMap<Long, CustomerDTO>();
		List<CustomerDTO> list = this.getCustomerDTOs();
		for (int i = 0; i < list.size(); i++) {
			customerMap.put(list.get(i).getCustomerId(), list.get(i));
		}
	}

	
	private Map<Long, CustomerCountryDTO> customerCountryMap = null;

	public Map<Long, CustomerCountryDTO> getCustomerCountryDTOMap() {
		if (customerCountryMap == null) {
			loadCustomerCountryDTOMap();
		}
		return customerCountryMap;
	}

	private void loadCustomerCountryDTOMap() {
		customerCountryMap = new HashMap<Long, CustomerCountryDTO>();
		List<CustomerCountryDTO> list = this.getCustomerCountryDTOs();
		for (int i = 0; i < list.size(); i++) {
			customerCountryMap.put(list.get(i).getCustomerId(), list.get(i));
		}
	}
	
	private Map<Long, StatusDTO> statusMap;
	public Map<Long, StatusDTO> getStatusDTOMap() {
		if (statusMap == null) {
			loadStatusDTOMap();
		}
		return statusMap;
	}

	private void loadStatusDTOMap() {
		statusMap = new HashMap<Long, StatusDTO>();
		List<StatusDTO> list = getStatusDTOs();
		for (int i = 0; i < list.size(); i++) {
			statusMap.put(list.get(i).getStatusId(), list.get(i));
		}
	}

	
	private Map<Long, LocationCountryDTO> locationCountryMap;
	public Map<Long, LocationCountryDTO> getLocationCountryDTOMap() {
		if (locationCountryMap == null) {
			loadLocationCountryDTOMap();
		}
		return locationCountryMap;
	}
	
	private void loadLocationCountryDTOMap() {
		locationCountryMap = new HashMap<Long, LocationCountryDTO>();
		List<LocationCountryDTO> list = getLocationCountryDTOs();
		for (int i = 0; i < list.size(); i++) {
			locationCountryMap.put(list.get(i).getLocationId(), list.get(i));
		}
	}
	
	
	private Map<Long, LocationDTO> locationMap;
	public Map<Long, LocationDTO> getLocationDTOMap() {
		if (locationMap == null) {
			loadLocationDTOMap();
		}
		return locationMap;
	}
	
	private void loadLocationDTOMap() {
		locationMap = new HashMap<Long, LocationDTO>();
		List<LocationDTO> list = getLocationDTOs();
		for (int i = 0; i < list.size(); i++) {
			locationMap.put(list.get(i).getLocationId(), list.get(i));
		}
	}
	
	private List<StatusDTO> statusDTOs;
	private List<CustomerDTO> customerDTOs;
	private List<CustomerCountryDTO> customerCountryDTOs;
	private List<StructureDTO> structureDTOs;
	private List<DisplaySalesRepDTO> displaySalesRepDTOs;
	private List<DisplaySalesRepDTO> approverDTOs;
	private List<ManufacturerDTO> manufacturerDTOs;
	private List<ManufacturerCountryDTO> manufacturerCountryDTOs;
	private List<PromoPeriodDTO> promoPeriodDTOs;
	private List<PromoPeriodCountryDTO> promoPeriodCountryDTOs;
	private List<ValidPlantDTO> validPlantDTOs;
	private List<ValidPlannerDTO> validPlannerDTOs;
	private List<ValidLocationDTO> validLocationDTOs;
	private List<ValidBuDTO> validBuDTOs;
	private List<LocationDTO> locationDTOs;
	private List<LocationCountryDTO> locationCountryDTOs;
	private List<CountryDTO> activeCountryFlagDTOs;
	private List<CountryDTO> countryDTOs;
	private List<InPogDTO> inPogDTOs;

	public List<StatusDTO> getStatusDTOs() {
		if (statusDTOs == null) {
			loadStatusDTOs();
		}
		return statusDTOs;
	}

	public List<LocationDTO> getLocationDTOs() {
		if (locationDTOs == null) {
			locationDTOs=loadLocationDTOs();
		}
		return locationDTOs;
	}
	
	public List<LocationCountryDTO> getLocationCountryDTOs() {
		if (locationCountryDTOs == null) {
			locationCountryDTOs=loadLocationCountryDTOs();
		}
		return locationCountryDTOs;
	}
	
	public List<CustomerDTO> getCustomerDTOs() {
		if (customerDTOs == null) {
			loadCustomerDTOs();
		}
		return customerDTOs;
	}

	public List<CustomerCountryDTO> getCustomerCountryDTOs() {
		if (customerCountryDTOs == null) {
			loadCustomerCountryDTOs();
		}
		return customerCountryDTOs;
	}
	
	public List<StructureDTO> getStructureDTOs() {
		if (structureDTOs == null) {
			loadstructureDTOs();
		}
		return structureDTOs;
	}

	public List<DisplaySalesRepDTO> getDisplaySalesRepDTOs() {
		if (displaySalesRepDTOs == null) {
			loadDisplaySalesRepDTOs();
		}
		return displaySalesRepDTOs;
	}

	public List<PromoPeriodDTO> getPromoPeriodDTOs() {
		if (promoPeriodDTOs == null) {
			loadPromoPeriods();
		}
		return promoPeriodDTOs;
	}
	
	public List<PromoPeriodCountryDTO> getPromoPeriodCountryDTOs() {
		if (promoPeriodCountryDTOs == null) {
			loadPromoPeriodCountries();
		}
		return promoPeriodCountryDTOs;
	}
	
	public List<InPogDTO> getInPogDTOs() {
		if (inPogDTOs == null) {
			loadInPogYnDTOs();
		}
		return inPogDTOs;
	}
	
	public DisplayScenarioDTO saveDisplayScenarioDTO(DisplayScenarioDTO displayScenarioDTO) {
		return displayScenarioManager.save(displayScenarioDTO);
	}

	public DisplayDTO saveDisplayDTO(DisplayDTO displayDTO) {
		boolean isNew = displayDTO.getDisplayId() == null;
		DisplayDTO displayResultDTO=displayManager.save(displayDTO);
		List<DisplayScenario> lstDisplayScenarios=displayScenarioManager.getDisplayScenarioDAO().getDisplayScenarios(displayResultDTO.getDisplayId());
		DisplayScenarioDTO objDisplayScenarioDTO;
		for(DisplayScenario objDisplayScenario:lstDisplayScenarios){
			if(objDisplayScenario.getScenarioApprovalFlg().equalsIgnoreCase("Y")){
				objDisplayScenario.setDisplaySalesRepId(displayResultDTO.getDisplaySalesRepId());
				objDisplayScenario.setSku(displayResultDTO.getSku());
				objDisplayScenario.setManufacturerId(displayResultDTO.getManufacturerId());
				objDisplayScenario.setPromoPeriodId(displayResultDTO.getPromoPeriodId());
				objDisplayScenario.setScenariodStatusId(displayResultDTO.getStatusId());
				objDisplayScenario.setPromoYear(displayResultDTO.getPromoYear());
				objDisplayScenarioDTO=ConverterFactory.convert(objDisplayScenario);
				displayScenarioManager.save(objDisplayScenarioDTO);
			}
		}
		if(isNew){ //saving program bu for the first time
			/*
			 * Code change by Mari
			 * Date: 10092012
			 * Populate Program_% for the valid bu based on the customer id.
			 */
			List<CstBuPrgmPct> lstCstBuPrgmPct = cstBuPrgmPctManager.findCstBuPrgmPctByCustomerId(displayDTO.getCustomerId());
			/*
			 * Ended
			 */
			for(ValidBuDTO dtos: getValidBuDTOs()){
				ProgramBuDTO programDTO = new ProgramBuDTO();
				programDTO.setDisplayId(displayResultDTO.getDisplayId());
				programDTO.setBuCode(dtos.getBuCode());
				programDTO.setBuDesc(dtos.getBuDesc());
				programDTO.setProgramPct(0.00);
				/*
				 * Code change by Mari
				 * Date: 10092012
				 * Populate Program_% for the valid bu based on the customer id.
				 */
				for(CstBuPrgmPct  objCstBuPrgmPct:lstCstBuPrgmPct){
					if(objCstBuPrgmPct.getBu().getBuCode().equalsIgnoreCase(dtos.getBuCode())){
						if(objCstBuPrgmPct.getProgramPct()!=null){
							programDTO.setProgramPct(objCstBuPrgmPct.getProgramPct());
						}
						break;
					}
				}
				/*
				 * Ended
				 */
				programDTO.setDtCreated(displayResultDTO.getDtCreated());
				programDTO.setDtLastChanged(displayResultDTO.getDtLastChanged());
				programDTO.setUserCreated(displayResultDTO.getUserCreated());
				programDTO.setUserLastChanged(displayResultDTO.getUserLastChanged());
				programBuManager.save(programDTO);
			}
		}
		return displayResultDTO;
	}
	
	

	public  List<DisplayForMassUpdateDTO> massUpdateSelectedItems( String userName,  List<Long> parameters, DisplayForMassUpdateDTO displayMassUpdateDTO) {
		CustomerMarketing cm;
		Display disp;
		List<DisplayForMassUpdateDTO> displayForMassUpdateDTOList=null;
		if(parameters!=null && parameters.size()>0){
			for(Long displayId:parameters){
				disp = displayManager.getDisplayDAO().findById(displayId) ;
				if(disp!=null){
					if(displayMassUpdateDTO.getCustomerId()!=null)
						disp.setCustomerId(displayMassUpdateDTO.getCustomerId());
					if(displayMassUpdateDTO.getStructureId()!=null)
						disp.setStructureId(displayMassUpdateDTO.getStructureId());
					if(displayMassUpdateDTO.getDisplaySalesRepId()!=null)
						disp.setDisplaySalesRepId(displayMassUpdateDTO.getDisplaySalesRepId());
					if(displayMassUpdateDTO.getStatusId()!=null)
						disp.setStatusId(displayMassUpdateDTO.getStatusId());
					if(displayMassUpdateDTO.getCorrugateId()!=null)
						disp.setManufacturerId(displayMassUpdateDTO.getCorrugateId());
					if(displayMassUpdateDTO.getPromoPeriodId()!=null)
						disp.setPromoPeriodId(displayMassUpdateDTO.getPromoPeriodId());
					if(displayMassUpdateDTO.isSkuMixFinalFlg()!=null)
						disp.setSkuMixFinalFlg(displayMassUpdateDTO.isSkuMixFinalFlg().equals("Y")?true:false);
					disp.setDtLastChanged(new Date());
					disp.setUserLastChanged(userName);
					displayManager.getDisplayDAO().merge(disp); 
					List<DisplayScenario> lstDisplayScenarios=displayScenarioManager.getDisplayScenarioDAO().getDisplayScenarios(disp.getDisplayId());
					DisplayScenarioDTO objDisplayScenarioDTO;
					for(DisplayScenario objDisplayScenario:lstDisplayScenarios){
						if(objDisplayScenario.getScenarioApprovalFlg().equalsIgnoreCase("Y")){
							if(displayMassUpdateDTO.getDisplaySalesRepId()!=null)
								objDisplayScenario.setDisplaySalesRepId(displayMassUpdateDTO.getDisplaySalesRepId());
							if(displayMassUpdateDTO.getCorrugateId()!=null)
								objDisplayScenario.setManufacturerId(displayMassUpdateDTO.getCorrugateId());
							if(displayMassUpdateDTO.getPromoPeriodId()!=null)
								objDisplayScenario.setPromoPeriodId(displayMassUpdateDTO.getPromoPeriodId());
							if(displayMassUpdateDTO.getStatusId()!=null)
								objDisplayScenario.setScenariodStatusId(displayMassUpdateDTO.getStatusId());
							objDisplayScenarioDTO=ConverterFactory.convert(objDisplayScenario);
							displayScenarioManager.save(objDisplayScenarioDTO);
						}
					}
				}
				cm = customerMarketingManager.getCustomerMarketingDAO().findByDisplayId(displayId);
				if(cm!=null){
					System.out.println("Customer Marketing is available already for display#"+displayId);
					cm.setDisplayId(displayId);
					cm.setDtLastChanged(new Date());
					cm.setUserLastChanged(userName);
				}else{
					System.out.println("Customer Marketing is NOT available for display#"+displayId);
					cm= new CustomerMarketing();
					cm.setDisplayId(displayId);
					cm.setDtCreated(new Date());
					cm.setUserCreated(userName);
				}
				if(displayMassUpdateDTO.getPriceAvailabilityDate()!=null)
					cm.setPriceAvailabilityDate(displayMassUpdateDTO.getPriceAvailabilityDate());
				if(displayMassUpdateDTO.getSentSampleDate()!=null)
					cm.setSentSampleDate(displayMassUpdateDTO.getSentSampleDate());
				if(displayMassUpdateDTO.getFinalArtworkDate()!=null)
					cm.setFinalArtworkDate(displayMassUpdateDTO.getFinalArtworkDate());
				if(displayMassUpdateDTO.getPimCompletedDate()!=null)
					cm.setPimCompletedDate(displayMassUpdateDTO.getPimCompletedDate());
				if(displayMassUpdateDTO.getPricingAdminDate()!=null)
					cm.setPricingAdminDate(displayMassUpdateDTO.getPricingAdminDate());
				if(displayMassUpdateDTO.getProjectLevel()!=null)
					cm.setProjectLevelPct(displayMassUpdateDTO.getProjectLevel().shortValue());
				if(displayMassUpdateDTO.getInitialRendering()!=null)
					cm.setInitialRenderingFlg(displayMassUpdateDTO.getInitialRendering().equals("Y")?true:false);
				if(displayMassUpdateDTO.getStructureApproved()!=null)
					cm.setStructureApprovedFlg(displayMassUpdateDTO.getStructureApproved().equals("Y")?true:false);
				if(displayMassUpdateDTO.getOrderIn()!=null)
					cm.setOrderInFlg(displayMassUpdateDTO.getOrderIn().equals("Y")?true:false);
				if(displayMassUpdateDTO.getComments()!=null)
					cm.setComments(displayMassUpdateDTO.getComments()==null?"":displayMassUpdateDTO.getComments());
				
					customerMarketingManager.getCustomerMarketingDAO().attachDirty(cm);
			}
		}else{
			System.out.println(" There is no item selected from the display selection grid");
			displayForMassUpdateDTOList=null;
		}
		System.out.println("AppServicePOJO:massUpdateSelectedItems ends");   
		return displayForMassUpdateDTOList;
	}
	
	public DisplayDTO copyDisplayDTO(DisplayDTO displayCopyDTO, String user) {
		Display displaySavedObj=ConverterFactory.convert(displayCopyDTO);
		displayCopyDTO.setDisplayId(null);
		displayCopyDTO.setDtCreated(new Date());
		displayCopyDTO.setUserCreated(user);
		displayCopyDTO.setDtLastChanged(new Date());
		displayCopyDTO.setUserLastChanged(user);    	
		displayCopyDTO.setStatusId(1L);
		displayCopyDTO.setSkuMixFinalFlg(Boolean.FALSE);
		//Double currYear=(System.currentTimeMillis()/1000/3600/24/365.25 +1970);
		//displayCopyDTO.setPromoYear(currYear.longValue());
		displayCopyDTO=displayManager.save(displayCopyDTO);
		displayCopyDTO.setRygStatusFlg("G");
		//Customer Marketing Info
		CustomerMarketing customerMarketing=customerMarketingManager.getCustomerMarketingDAO().findByDisplayId(displaySavedObj.getDisplayId());
		if(customerMarketing!=null){
			customerMarketing.setCustomerMarketingId(null);
			customerMarketing.setDisplayId(displayCopyDTO.getDisplayId());
			customerMarketing.setDtCreated(new Date());
			customerMarketing.setUserCreated(user);
			customerMarketing.setDtLastChanged(new Date());
			customerMarketing.setUserLastChanged(user);    	
			customerMarketingManager.getCustomerMarketingDAO().attachDirty(customerMarketing);
		}
		//copy display logistics info
		DisplayLogistics displayLosgistics=displayLogisticsManager.getDisplayLogisticsDAO().findByDisplayId(displaySavedObj.getDisplayId());
		if(displayLosgistics!=null){
			displayLosgistics.setDisplayLogisticsId(null);
			displayLosgistics.setDisplayId(displayCopyDTO.getDisplayId());
			displayLosgistics.setDtCreated(new Date());
			displayLosgistics.setUserCreated(user);
			displayLosgistics.setDtLastChanged(new Date());
			displayLosgistics.setUserLastChanged(user);    	
			displayLogisticsManager.getDisplayLogisticsDAO().attachDirty(displayLosgistics);
		}
		//Copy Scenario based Cost Analysis
		List<DisplayScenario> listScenarios=displayScenarioManager.getDisplayScenarioDAO().getDisplayScenarios(displaySavedObj.getDisplayId());
		DisplayScenarioDTO displayScenarioCopyDTO=null;
		for (DisplayScenario displayScenarioCopy:listScenarios){
			displayScenarioCopyDTO=ConverterFactory.convert(displayScenarioCopy);
			displayScenarioCopyDTO.setDisplayId(displayCopyDTO.getDisplayId());
			copyDisplayScenarioDTO(displayScenarioCopyDTO, user,false);
		}
		//copy ship wave info
		List<DisplayShipWave> listDisplayShipWave = displayShipWaveManager.getDisplayShipWaveDAO().findDisplayShipWavesByDisplay(displaySavedObj) ;
		List<DisplayShipWave> listDisplayShipWaveCopy=new ArrayList<DisplayShipWave>();
		DisplayShipWave objDisplayShipWaveCopy=null;
		for (DisplayShipWave objDisplayShipWave:listDisplayShipWave){
			objDisplayShipWave.setDisplayShipWaveId(null);
			objDisplayShipWave.setDisplayId(displayCopyDTO.getDisplayId());
			objDisplayShipWave.setDtCreated(new Date());
			objDisplayShipWave.setUserCreated(user);
			objDisplayShipWave.setDtLastChanged(new Date());
			objDisplayShipWave.setUserLastChanged(user);    	
			objDisplayShipWaveCopy=displayShipWaveManager.getDisplayShipWaveDAO().merge(objDisplayShipWave) ;
			listDisplayShipWaveCopy.add(objDisplayShipWaveCopy);
		}
		//copy display dc info and packout info (fullfilment)
		List <DisplayDc> listDisplayDc=displayDcManager.getDisplayDcDAO().findDisplayDcByDisplayId(displaySavedObj.getDisplayId());
		List <DisplayPackout> listDisplayPackoutManager=null;
		DisplayDc ObjDisplayDcCopy;
		Long objDisplayDcId=null;
		for (DisplayDc objDisplayDc:listDisplayDc){
			objDisplayDcId=objDisplayDc.getDisplayDcId();
			listDisplayPackoutManager=displayPackoutManager.getDisplayPackoutDAO().findDisplayPackoutsByDisplayDc(objDisplayDc);
			//copy display dc
			objDisplayDc.setDisplayDcId(null);
			objDisplayDc.setDisplayId(displayCopyDTO.getDisplayId());
			objDisplayDc.setDtCreated(new Date());
			objDisplayDc.setUserCreated(user);
			objDisplayDc.setDtLastChanged(new Date());
			objDisplayDc.setUserLastChanged(user);    	
			ObjDisplayDcCopy=displayDcManager.getDisplayDcDAO().merge(objDisplayDc);

			for (DisplayPackout objDisplayPackoutManager:listDisplayPackoutManager){
				for (DisplayShipWave objDisplayShipWaves:listDisplayShipWaveCopy){
					if(objDisplayShipWaves.getDisplayId().equals(ObjDisplayDcCopy.getDisplayId()) && 
							objDisplayDcId.equals(objDisplayPackoutManager.getDisplayDcId())){ 
						//copy packout
						if(objDisplayPackoutManager.getDisplayShipWaveByDisplayShipWaveId().getShipWaveNum().equals(objDisplayShipWaves.getShipWaveNum()) ){
							objDisplayPackoutManager.setDisplayPackoutId(null);
							objDisplayPackoutManager.setDisplayDcId(ObjDisplayDcCopy.getDisplayDcId());
							objDisplayPackoutManager.setDisplayShipWaveId(objDisplayShipWaves.getDisplayShipWaveId());
							objDisplayPackoutManager.setDtCreated(new Date());
							objDisplayPackoutManager.setUserCreated(user);
							objDisplayPackoutManager.setDtLastChanged(new Date());
							objDisplayPackoutManager.setUserLastChanged(user);    	
							displayPackoutManager.getDisplayPackoutDAO().merge(objDisplayPackoutManager);
							break;
						}
					}
				}
			}
		}
		//Copy Corrugate Component
		Corrugate objCorrugate=corrugateManager.getCorrugateDAO().findByDisplayId(displaySavedObj.getDisplayId()); 
		if(objCorrugate!=null){
			objCorrugate.setCorrugateId(null);
			objCorrugate.setDisplayId(displayCopyDTO.getDisplayId());
			corrugateManager.getCorrugateDAO().attachDirty(objCorrugate);
		}
		
		List<CorrugateComponent> lstCorrugateComponent=corrugateComponentManager.getCorrugateComponentDAO().findByDisplayId(displaySavedObj.getDisplayId());
		for (CorrugateComponent objCorrugateComponent:lstCorrugateComponent){
			if(objCorrugateComponent!=null){
				objCorrugateComponent.setCorrugateComponentId(null);
				objCorrugateComponent.setDisplayId(displayCopyDTO.getDisplayId());
				objCorrugateComponent.setDtCreated(new Date());
				objCorrugateComponent.setUserCreated(user);
				objCorrugateComponent.setDtLastChanged(new Date());
				objCorrugateComponent.setUserLastChanged(user);    
				corrugateComponentManager.getCorrugateComponentDAO().attachDirty(objCorrugateComponent);
			}
		}
		List<ProgramBu> loadedProgramBuList = programBuManager.findByDisplayId(displaySavedObj.getDisplayId());
		for(ProgramBu dtos: loadedProgramBuList){
			ProgramBuDTO programDTO = new ProgramBuDTO();
			programDTO.setDisplayId(displayCopyDTO.getDisplayId());
			programDTO.setBuCode(dtos.getBu().getBuCode());
			programDTO.setBuDesc(dtos.getBu().getBuDesc());
			programDTO.setProgramPct(dtos.getProgramPct());
			programDTO.setDtCreated(displayCopyDTO.getDtCreated());
			programDTO.setDtLastChanged(displayCopyDTO.getDtLastChanged());
			programDTO.setUserCreated(displayCopyDTO.getUserCreated());
			programDTO.setUserLastChanged(displayCopyDTO.getUserLastChanged());
			programBuManager.save(programDTO);
		}
		/*CalculationSummary objCalculationSummary=calculationSummaryManager.getCalculationSummaryDAO().loadByDisplayId(displaySavedObj.getDisplayId()); 
		if(objCalculationSummary!=null){
			objCalculationSummary.setCalculationSummaryId(null);
			objCalculationSummary.setDisplayId(displayCopyDTO.getDisplayId());
			calculationSummaryManager.getCalculationSummaryDAO().attachDirty(objCalculationSummary);
		}*/
		return displayCopyDTO;
	}

	public DisplayScenarioDTO copyDisplayScenarioDTO(DisplayScenarioDTO displayScenarioCopyDTO, String user, boolean copyComments) {
		DisplayScenario displayScenarioSavedObj=ConverterFactory.convert(displayScenarioCopyDTO);
		displayScenarioCopyDTO.setDisplayScenarioId(null);
		displayScenarioCopyDTO.setDtCreated(new Date());
		displayScenarioCopyDTO.setUserCreated(user);
		displayScenarioCopyDTO.setDtLastChanged(new Date());
		displayScenarioCopyDTO.setUserLastChanged(user);    	
		displayScenarioCopyDTO=displayScenarioManager.save(displayScenarioCopyDTO);
		//Customer Marketing Info
		DisplayCost displayCost=displayCostManager.getDisplayCostDAO().loadByDisplayScenarioId(displayScenarioSavedObj.getDisplayScenarioId());
		DisplayCostDTO dirtyDisplayCostDTO=null;
		if(displayCost!=null){
			displayCost.setDisplayCostId(null);
			displayCost.setDisplayScenarioId(displayScenarioCopyDTO.getDisplayScenarioId());
			displayCost.setDisplayId(displayScenarioCopyDTO.getDisplayId());
			displayCost.setDtCreated(new Date());
			displayCost.setUserCreated(user);
			displayCost.setDtLastChanged(new Date());
			displayCost.setUserLastChanged(user);
			dirtyDisplayCostDTO=ConverterFactory.convert(displayCost);
			//displayCostManager.getDisplayCostDAO().attachDirty(displayCost);
			displayCostManager.save(dirtyDisplayCostDTO);
		}
		
		//copy kit components
		List<KitComponent> listKitComponents=kitComponentManager.getKitComponentDAO().findByDisplayScenarioId(displayScenarioSavedObj.getDisplayScenarioId()); 
		for (KitComponent objKitComponents:listKitComponents){
			objKitComponents.setKitComponentId(null);
			objKitComponents.setDisplayId(displayScenarioCopyDTO.getDisplayId());
			objKitComponents.setDisplayScenarioId(displayScenarioCopyDTO.getDisplayScenarioId());
			objKitComponents.setDtCreated(new Date());
			objKitComponents.setUserCreated(user);
			objKitComponents.setDtLastChanged(new Date());
			objKitComponents.setUserLastChanged(user);    	
			kitComponentManager.getKitComponentDAO().attachDirty(objKitComponents);
		}
		//Copy Corrugate Component
		List<DisplayCostImage> lstDisplayCostImage=displayCostImageManager.getDisplayCostImageDAO().findByDisplayScenarioId(displayScenarioSavedObj.getDisplayScenarioId()); 
		for (DisplayCostImage objDisplayScenario:lstDisplayCostImage){
			if(objDisplayScenario!=null){
				objDisplayScenario.setImageId(null);
				objDisplayScenario.setDisplayScenarioId(displayScenarioCopyDTO.getDisplayScenarioId());
				objDisplayScenario.setDisplayId(displayScenarioCopyDTO.getDisplayId());
				displayCostImageManager.getDisplayCostImageDAO().attachDirty(objDisplayScenario);
			}
		}
		//Copy DisplayCostComment 
		if(copyComments){
			List<DisplayCostComment> lstDisplayCostComment=displayCostCommentManager.getDisplayCostCommentDAO().findByDisplayScenarioId(displayScenarioSavedObj.getDisplayScenarioId()); 
			for (DisplayCostComment objDisplayCostComment:lstDisplayCostComment){
				if(objDisplayCostComment!=null){
					objDisplayCostComment.setDisplayCostCommentId(null);
					objDisplayCostComment.setDisplayScenarioId(displayScenarioCopyDTO.getDisplayScenarioId());
					displayCostCommentManager.getDisplayCostCommentDAO().attachDirty(objDisplayCostComment);
				}
			}
		}
		return displayScenarioCopyDTO;
	}
	
	private void loadPromoPeriods() {
		promoPeriodDTOs = promoPeriodManager.getPromoPeriodDTOs();
	}

	private void loadPromoPeriodCountries() {
		promoPeriodCountryDTOs = promoPeriodCountryManager.getPromoPeriodCountryDTOs();
	}
	
	private void loadDisplaySalesRepDTOs() {
		displaySalesRepDTOs = displaySalesRepManager.getDisplaySalesRepDTOs();
	}
	
	private void loadApproverDTOs() {
		approverDTOs = displaySalesRepManager.getApproverDTOs();
	}

	private void loadstructureDTOs() {
		structureDTOs = structureManager.getStructureDTOs();
	}

	private void loadCustomerDTOs() {
		customerDTOs = customerManager.getCustomerDTOs();
	}
	
	private void loadCustomerCountryDTOs() {
		customerCountryDTOs = customerCountryManager.getCustomerCountryDTOs();
	}
	
	private void loadInPogYnDTOs() {
		inPogDTOs = new ArrayList<InPogDTO>();
		inPogDTOs.add(new InPogDTO(Boolean.TRUE,"Yes"));
		inPogDTOs.add(new InPogDTO(Boolean.FALSE,"No"));
	}

	public String getMessage(String msg) {
		return "Client said: \"" + msg + "\"<br>Server answered: \"Hi!\"";
	}

	public List<DisplayDTO> getDisplays() {
		List<Display> displays = displayManager.getDisplayDAO().getAllDisplays();
		//TODO erase mock
//		return ConverterFactory.convertDisplays(displays.subList(0, displays.size() > 20 ? 20 : displays.size() - 1));
		return ConverterFactory.convertDisplays(displays);
	}

	public List<DisplayForMassUpdateDTO> getSelectedDisplaysForMassUpdate(List<Long> parameters) {
		List<DisplayForMassUpdate> displays = displayManager.getDisplayDAO().getSelectedDisplaysForMassUpdate(parameters);
		return ConverterFactory.convertDisplayForMassUpdates(displays);
	}

	private void loadStatusDTOs() {
		statusDTOs = statusManager.getStatusesDTOs();
	}

	public CustomerMarketingDTO getCustomerMarketingDTO(Long displayId) {
		CustomerMarketing cm = customerMarketingManager.getCustomerMarketingDAO().findByDisplayId(displayId);
		return ConverterFactory.convert(cm);
	}

	public CustomerMarketingDTO saveCustomerMarketingDTO(
			CustomerMarketingDTO customerMarketing, DisplayDTO display) {
		displayManager.save(display);
		return customerMarketingManager.save(customerMarketing);
	}

	public DisplayLogisticsDTO getDisplayLogisticsInfoDTO(Long displayId) {
		DisplayLogistics dl = displayLogisticsManager.getDisplayLogisticsDAO().findByDisplayId(displayId);
		return ConverterFactory.convert(dl);
	}

	public DisplayLogisticsDTO saveDisplayLogisticsInfoDTO(
			DisplayLogisticsDTO displayLogistics, DisplayDTO display) {

		DisplayLogisticsDTO r = displayLogisticsManager.save(displayLogistics);

		//display.setDisplayLogisticsId(r.getDisplayLogisticsId());
		//DisplayManager dm = new DisplayManager();
		//dm.save(display);

		return r;
	}

	public List<KitComponentDTO> getKitComponents(DisplayDTO display, DisplayScenarioDTO displayScenarioDTO) {

		//List<KitComponent> kitComponents = kitComponentManager.getKitComponentDAO().findByDisplayId(display.getDisplayId());
		if(displayScenarioDTO==null) return null;
		List<KitComponent> kitComponents = kitComponentManager.getKitComponentDAO().findByDisplayScenarioId(displayScenarioDTO.getDisplayScenarioId());
		List<KitComponentDTO> kitComponentDTOs = ConverterFactory.convertKitComponents(kitComponents);
		Iterator<KitComponentDTO> iter = kitComponentDTOs.iterator();
		while (iter.hasNext()) {
			KitComponentDTO comp = (KitComponentDTO) iter.next();
			comp.updateCalculatedFields(display.getQtyForecast());
		}
		return kitComponentDTOs;
	}

	public KitComponentDTO getKitComponentDTO(Long kitComponentId, DisplayDTO display, DisplayScenarioDTO displayScenarioDTO) {
		System.out.println("AppServiceImpl.getKitComponentDTO(): getting kit component detail for kit id=" + kitComponentId);
		KitComponent km = kitComponentManager.getKitComponentDAO().findById(kitComponentId);
		System.out.println("AppServiceImpl.getKitComponentDTO(): got kit component detail");
		KitComponentDTO comp = ConverterFactory.convert(km);
		if(displayScenarioDTO!=null && displayScenarioDTO.getDisplayScenarioId()!=null){
			comp.updateCalculatedFields(displayScenarioDTO.getActualQty());
		}else{
			comp.updateCalculatedFields(display.getActualQty());
		}
		return comp;
	}

	public KitComponentDTO saveKitComponentDTO(
			KitComponentDTO kitComponent, DisplayDTO display, List<DisplayPlannerDTO> store) {
		// save kit component info
		//extract benchmark price, invoice price a
		KitComponentDTO kcDTO=null;
		System.out.println("AppServicePOJO:saveKitCOmponentDTO starts");
		try{
			Properties props = new Properties();
			ProductDetail pd=null;
			String sCountryName=display.getCountryByCountryId()==null?"":display.getCountryByCountryId().getCountryName();
			List<ProductDetail> pds=null;
			System.out.println("AppServicePOJO:saveKitCOmponentDTO display.getDisplaySetupId()="+display.getDisplaySetupId());
			if(!display.getDisplaySetupId().equals(2L)){//DISPLAY_SETUP_FG_NEW_UPC
				System.out.println("AppServicePOJO:saveKitCOmponentDTO not DISPLAY_SETUP_FG_NEW_UPC");
				pd = productDetailManager.getProductDetailDAO().findById(kitComponent.getSkuId());
				if(pd!=null ){
					if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_USA)){
						System.out.println("AppServicePOJO:saveKitCOmponentDTO ..US..");
						if(pd!=null && pd.getBenchMarkPrice()!=null ){
							kitComponent.setBenchMarkPerUnit(pd.getBenchMarkPrice());
						}
						if(pd!=null &&	pd.getListPrice()!=null ){
							kitComponent.setInvoiceUnitPrice(pd.getListPrice());
						}
						
						/*
						 * Date 08/05/2014
						 * By Mari
						 * Description COGS changed into Material Cost+ labor Cost
						 */
						
						if(pd!=null && pd.getFullBurdenCost()!=null ){
							System.out.println("AppServicePOJO:saveKitCOmponentDTO UnitedStates getFullBurdenCost()="+pd.getFullBurdenCost());
							kitComponent.setCogsUnitPrice(pd.getMaterialCost()+pd.getLaborCost());
						}else{
							kitComponent.setCogsUnitPrice(0.00f);
						}
					}else if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_CAN)){
						System.out.println("AppServicePOJO:saveKitCOmponentDTO ..Canada..");
						if(pd!=null && pd.getCanBenchMarkPrice()!=null ){
							kitComponent.setBenchMarkPerUnit(pd.getCanBenchMarkPrice());
						}
						if(pd!=null &&	pd.getCanListPrice()!=null ){
							kitComponent.setInvoiceUnitPrice(pd.getCanListPrice());
						}
						if(pd!=null && pd.getCanFullBurdenCost()!=null ){
							System.out.println("AppServicePOJO:saveKitCOmponentDTO CANADA getFullBurdenCost()="+(pd.getCanMaterialCost()+pd.getCanLaborCost()));
							kitComponent.setCogsUnitPrice(pd.getCanMaterialCost()+pd.getCanLaborCost());
						}else{
							kitComponent.setCogsUnitPrice(0.00f);
						}
					}
				}
			}else{
				pds = productDetailManager.getProductDetailDAO().findBySku(kitComponent.getFinishedGoodsSku());
				if(pds!=null && pds.size()>0){
					if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_USA)){
						if(pds!=null && pds.get(0).getBenchMarkPrice()!=null ){
							System.out.println("AppServicePOJO:saveKitCOmponentDTO UnitedStates getBenchMarkPrice()="+pds.get(0).getBenchMarkPrice());
							kitComponent.setBenchMarkPerUnit(pds.get(0).getBenchMarkPrice());
						}else{
							kitComponent.setBenchMarkPerUnit(0.00f);
						}
						if(pds!=null &&	pds.get(0).getListPrice()!=null ){
							System.out.println("AppServicePOJO:saveKitCOmponentDTO UnitedStates getListPrice()="+pds.get(0).getListPrice());
							kitComponent.setInvoiceUnitPrice(pds.get(0).getListPrice());
						}else{
							kitComponent.setInvoiceUnitPrice(0.00f);
						}
						/*
						 * Date 08/05/2014
						 * By Mari
						 * Description COGS changed into Material Cost+ labor Cost
						 */
						if(pds!=null && pds.get(0).getFullBurdenCost()!=null ){
							System.out.println("AppServicePOJO:saveKitCOmponentDTO UnitedStates getFullBurdenCost()="+pds.get(0).getFullBurdenCost());
							kitComponent.setCogsUnitPrice(pds.get(0).getMaterialCost()+pds.get(0).getLaborCost());
						}else{
							kitComponent.setCogsUnitPrice(0.00f);
						}
					}else if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_CAN)){
						if(pds!=null && pds.get(0).getCanBenchMarkPrice()!=null ){
							System.out.println("AppServicePOJO:saveKitCOmponentDTO CANADA getBenchMarkPrice()="+pds.get(0).getCanBenchMarkPrice());
							kitComponent.setBenchMarkPerUnit(pds.get(0).getCanBenchMarkPrice());
						}else{
							kitComponent.setBenchMarkPerUnit(0.00f);
						}
						if(pds!=null &&	pds.get(0).getCanListPrice()!=null ){
							System.out.println("AppServicePOJO:saveKitCOmponentDTO CANADA getListPrice()="+pds.get(0).getCanListPrice());
							kitComponent.setInvoiceUnitPrice(pds.get(0).getCanListPrice());
						}else{
							kitComponent.setInvoiceUnitPrice(0.00f);
						}
						/*
						 * Date 08/05/2014
						 * By Mari
						 * Description COGS changed into Material Cost+ labor Cost
						 */
						if(pds!=null && pds.get(0).getCanFullBurdenCost()!=null ){
							System.out.println("AppServicePOJO:saveKitCOmponentDTO CANADA getFullBurdenCost()="+(pds.get(0).getCanMaterialCost()+pds.get(0).getCanLaborCost()));
							kitComponent.setCogsUnitPrice(pds.get(0).getCanMaterialCost()+pds.get(0).getCanLaborCost());
						}else{
							kitComponent.setCogsUnitPrice(0.00f);
						}
					}
				}else{
					kitComponent.setBenchMarkPerUnit(0.00f);
					kitComponent.setInvoiceUnitPrice(0.00f);
					kitComponent.setCogsUnitPrice(0.00f);
				}
			}
		System.out.println("AppServicePOJO:saveKitComponent..5.."+kitComponent.getPeReCalcDate());
		System.out.println("AppServicePOJO:saveKitComponent..5.1.."+kitComponent.getDisplayScenarioId());
		
		/*
		 * Code Added By : Marianandan Arockiasamy
		 * Date : 08/06/2014
		 * Description : Include Program Percentage
		 */
		List <ProgramBu> listPbu = programBuManager.findByDisplayId(kitComponent.getDisplayId());
		Iterator<ProgramBu> iterBbu = listPbu.iterator();
		ProgramBu pbu=null;
		while (iterBbu.hasNext()) {
			pbu = (ProgramBu) iterBbu.next();
			if (pbu.getBu().getBuDesc().equalsIgnoreCase(kitComponent.getBuDesc())){
				 if(kitComponent.getOverrideExceptionPct() != null && kitComponent.getOverrideExceptionPct().doubleValue() != 0.0D){
					   System.out.println("AppServicePOJO:saveKitComponent ..5.2..<<OverrideExcPct>>: "+ Float.valueOf(kitComponent.getOverrideExceptionPct().floatValue()));
					   kitComponent.setProgPrcnt(Float.valueOf(kitComponent.getOverrideExceptionPct().floatValue()));
				   }else{
					   System.out.println("AppServicePOJO:saveKitComponent ..5.3..<<EstimatedProgramPct>>: "+Float.valueOf( pbu.getProgramPct().floatValue()));
					   kitComponent.setProgPrcnt(Float.valueOf(pbu.getProgramPct().floatValue()));
				   }
			}
		}
		kcDTO = kitComponentManager.save(kitComponent);
		/*
		 * Description : Calling PriceException procedure has been disabled for test purpose
		 * Date :  11/17/2010
		 * By Mari
		 * 
		 * saveILSPriceException(kcDTO.getKitComponentId(),kitComponent.getPeReCalcDate());
		 */
		saveILSPriceException(kcDTO.getKitComponentId(),kitComponent.getPeReCalcDate());
		Iterator<DisplayPlannerDTO> iter = store.iterator();
		DisplayPlannerDTO dpDTO=null;
		while (iter.hasNext()) {
			dpDTO = (DisplayPlannerDTO)iter.next();
			if (dpDTO.getKitComponentId() == null) dpDTO.setKitComponentId(kcDTO.getKitComponentId());
			displayPlannerManager.save(dpDTO);
		}
		}catch(Exception ex){
			System.out.println("AppServicePOJO:saveKitComponent error"+ ex.toString());
		}
		return kcDTO;
	}


	public List<KitComponentDTO> switchOnDisplayTypesInKitComponent(DisplayDTO display, boolean applyKitVerSetup, boolean applyFGDisplayRef ) {
		List<KitComponent> kitComponents = kitComponentManager.getKitComponentDAO().findByDisplayId(display.getDisplayId());
		List<KitComponentDTO> kitComponentDTOs = ConverterFactory.convertKitComponents(kitComponents);
		Iterator<KitComponentDTO> iter = kitComponentDTOs.iterator();
		List<KitComponentDTO> kitComponentDTOList = new ArrayList<KitComponentDTO>();
		KitComponentDTO comp;
		while (iter.hasNext()) {
			comp = (KitComponentDTO) iter.next();
			if(applyKitVerSetup){
				comp.setFinishedGoodsQty(null);
				comp.setFinishedGoodsSku(null);
				comp.setBreakCase(KitComponentDetail.PRODUCTION_VERSION);
				comp.setPlantBreakCaseFlg(Boolean.FALSE);
				comp.setPbcVersion(null);
				String s = comp.getSku();
				if (s != null && s.length()==15) s = s.substring(10);
				if (s == null) s = "";
				comp.setKitVersionCode(s);
			}
			if(applyFGDisplayRef){
				comp.setKitVersionCode(null);
				comp.setPbcVersion(null);
				comp.setBreakCase(null);
				comp.setPlantBreakCaseFlg(Boolean.FALSE);
				if(comp.getQtyPerFacing()==null || comp.getQtyPerFacing().equals(0L) ) comp.setQtyPerFacing(1L);
				if(comp.getNumberFacings()==null || comp.getNumberFacings().equals(0L) ) comp.setNumberFacings(1L);
				if(comp.getQtyPerDisplay()==null || comp.getQtyPerFacing()==null || comp.getQtyPerDisplay().equals(0L) || comp.getQtyPerFacing().equals(0L) )
					comp.setQtyPerDisplay(null);
				else
					comp.setQtyPerDisplay(comp.getNumberFacings() * comp.getQtyPerFacing());
				if(comp.getQtyPerDisplay()==null || display.getQtyForecast()==null || comp.getQtyPerDisplay().equals(0L) || display.getQtyForecast().equals(0L)  )
					comp.setInventoryRequirement(null);
				else
					comp.setInventoryRequirement(comp.getQtyPerDisplay() * display.getQtyForecast());
			}
			if(applyKitVerSetup || applyFGDisplayRef){
				comp.setDtLastChanged(new Date());
			}
			kitComponentManager.save(comp);
			kitComponentDTOList.add(comp);
		}
		return kitComponentDTOList;
	}
	
	 

	public List<KitComponentDTO> saveKitComponentDTOList(List<KitComponentDTO> list) {
		List<KitComponentDTO> kitComponentDTOList = new ArrayList<KitComponentDTO>();
		for (KitComponentDTO kitComponentDTO:list) {
			KitComponentDTO dto = kitComponentManager.save(kitComponentDTO);
			kitComponentDTOList.add(dto);
			
		}
		System.out.println("AppServicePOJO:saveKitComponentDTOList ends");
		return kitComponentDTOList;
	}

	public Boolean deleteKitComponent(KitComponentDTO kitComponent) {
		displayPlannerManager.getDisplayPlannerDAO().deleteByKitComponentId(kitComponent.getKitComponentId());
		kitComponentManager.getKitComponentDAO().delete(ConverterFactory.convert(kitComponent));
		return true;
	}

	public List<DisplayPlannerDTO> getDisplayPlanners(KitComponentDTO kitComponent, DisplayDTO display) {
		System.out.println("AppServicePOJO:getDisplayPlanners..starts..");
		Map<Long, DisplayDc> displayDc = displayDcManager.findDisplayDcMapByDisplay(ConverterFactory.convert(display));
		List<DisplayPlanner> displayPlanners = new ArrayList<DisplayPlanner>();
		if (kitComponent.getKitComponentId()!=null) {
			displayPlanners = displayPlannerManager.getDisplayPlannerDAO().findByKitComponentId(kitComponent.getKitComponentId());        	
		}
		if (displayPlanners != null){
			System.out.println("AppServiceImpl: displayPlanners loaded " + displayPlanners.size());
		}else{
			System.out.println("AppServiceImpl: displayPlanners is null ");
		}
		List<DisplayPlannerDTO> displayPlannerDTOs = new ArrayList<DisplayPlannerDTO>();

		System.out.println("AppServiceImpl: displayPlanners iterate thru display dcs ");
		Set<Long> keys = displayDc.keySet();
		Iterator<Long> iter = keys.iterator();
		while (iter.hasNext()) {
			Long displayDcId = (Long)iter.next();
			System.out.println("AppServiceImpl: displayPlanners iterating displayDCId= " + displayDcId);
			DisplayDc dc = (DisplayDc)displayDc.get(displayDcId);
			String dcCode = dc.getLocationByLocationId().getLocationCode();
			System.out.println("AppServiceImpl: got DC name= " + dc.getLocationByLocationId().getLocationName());
			DisplayPlannerDTO dpDTO = new DisplayPlannerDTO();
			boolean found = false;
			if (displayPlanners != null) {
				System.out.println("AppServiceImpl: displayPlanners checking display planners size=" + displayPlanners.size());
				Iterator<DisplayPlanner>dpIter = displayPlanners.iterator();
				while (dpIter.hasNext()) {
					DisplayPlanner dp = (DisplayPlanner)dpIter.next();
					System.out.println("AppServiceImpl: displayPlanners matching to display planner id=" + dp.getDisplayPlannerId());
					if (dp.getDisplayDcId().equals(displayDcId)) {
						dpDTO = ConverterFactory.convert(dp);
						dpDTO.setDcName(dc.getLocationByLocationId().getLocationName());
						dpDTO.setDcCode(dc.getLocationByLocationId().getLocationCode());
						found = true;
					}
				}    		
			}
			if (!found) {
				System.out.println("AppServiceImpl: displayPlanners dc not found, creating new planner record");
				dpDTO.setDcName(dc.getLocationByLocationId().getLocationName());
				dpDTO.setDcCode(dc.getLocationByLocationId().getLocationCode());
				dpDTO.setDisplayDcId(displayDcId);
				dpDTO.setPlantOverrideFlg(false);
				// get plant / planner info
				List<ProductPlanner> productPlanners = productPlannerManager.getProductPlannerDAO().findBySkuIdDc(kitComponent.getSkuId(), dcCode);
				if (productPlanners != null) {
					for (int i = 0; i < productPlanners.size(); i++) {
						System.out.println("AppServiceImpl: match product planner " + dpDTO.getDcName() + " to " + productPlanners.get(i).getDcName());
						if (productPlanners.get(i).getDcName().equals(dpDTO.getDcName())) {
							dpDTO.setPlantCode(productPlanners.get(i).getPlantCode());
							dpDTO.setPlannerCode(productPlanners.get(i).getPlannerCode());
							//continue;
						}
					}    				
				}
			}
			System.out.println("AppServiceImpl: add display planner record for " + dpDTO.getDcName());
			displayPlannerDTOs.add(dpDTO);
		}

		// remove obsolete display planner records
		if (displayPlanners != null) {
			Iterator<DisplayPlanner>dpIter = displayPlanners.iterator();
			while (dpIter.hasNext()) {
				DisplayPlanner dp = (DisplayPlanner)dpIter.next();
				Long displayDcId = dp.getDisplayDcId();
				if (!displayDc.containsKey(displayDcId)) {
					displayPlannerManager.getDisplayPlannerDAO().delete(dp);
				}
			}
		}
		System.out.println("AppServicePOJO:getDisplayPlanners..ends..");
		return displayPlannerDTOs;
	}

	public DisplayPlannerDTO getDisplayPlanner(KitComponentDTO kitComponent, DisplayPlannerDTO dpDTO) {
		List<ProductPlanner> productPlanners = productPlannerManager.getProductPlannerDAO().findBySkuIdDc(kitComponent.getSkuId(), dpDTO.getDcCode());
		if (productPlanners != null) {
			System.out.println("AppServiceImpl: checking product planner records size=" + productPlanners.size());
			for (int i = 0; i < productPlanners.size(); i++) {
				dpDTO.setPlantCode(productPlanners.get(i).getPlantCode());
				dpDTO.setPlannerCode(productPlanners.get(i).getPlannerCode());
			}    				
		}    	
		else {
			dpDTO.setPlantCode("");
			dpDTO.setPlannerCode("");			
		}
		return dpDTO;
	}

	public List<CostPriceDTO> getProductDetail(String sCountryName,KitComponentDTO kitComponent) {
		System.out.println("AppServiceImpl.getProductDetail(): getting kit component cost for sku_id=" + kitComponent.getSkuId());
		/*Properties props = new Properties();
		try {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("DmmConstants.properties");
		props.load(inputStream);
		} catch (IOException e) {
			System.out.println("AppServicePOJO getProductDetail property file reading error" + e.toString() );
		}
		String strCanada = props.getProperty("canada");
		String strUnitedStates = props
		.getProperty("unitedstates");*/
		/*System.out.println("AppServicePOJO.getProductDetail: getting kit component [sCountryName]=["+ "["+sCountryName+"," +"]");*/
		ProductDetail pd = productDetailManager.getProductDetailDAO().findById(kitComponent.getSkuId());
		CostPriceDTO cpDTO;
		List<CostPriceDTO> costs = new ArrayList<CostPriceDTO>();
/*		System.out.println("AppServicePOJO.getProductDetail(): getting kit component [sCountryName,strCanada,strUnitedStates]=["+ "["+sCountryName+"," + strCanada +"," +strUnitedStates+"]");
*/		if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_USA)){
			cpDTO = new CostPriceDTO("Bench", pd.getBenchMarkPrice(), kitComponent.getQtyPerDisplay());
			costs.add(cpDTO);
			cpDTO = new CostPriceDTO("List", pd.getListPrice(), kitComponent.getQtyPerDisplay());
			costs.add(cpDTO);
			cpDTO = new CostPriceDTO("COGS", (pd.getLaborCost()+pd.getMaterialCost()), kitComponent.getQtyPerDisplay());
			costs.add(cpDTO);
		}else if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_CAN)){
			cpDTO = new CostPriceDTO("Bench", pd.getCanBenchMarkPrice(), kitComponent.getQtyPerDisplay());
			costs.add(cpDTO);
			cpDTO = new CostPriceDTO("List", pd.getCanListPrice(), kitComponent.getQtyPerDisplay());
			costs.add(cpDTO);
			cpDTO = new CostPriceDTO("COGS",  (pd.getCanLaborCost()+pd.getCanMaterialCost()), kitComponent.getQtyPerDisplay());
			costs.add(cpDTO);
		}
/*		System.out.println("AppServicePOJO.getProductDetail(): getting kit component [sCountryName,strCanada,strUnitedStates]=["+ "["+sCountryName+"," + strCanada +"," +strUnitedStates+"]");
*/
		return costs;
	}

	public List<CostPriceDTO> getProductDetailForFGSKU(String sCountryName, KitComponentDTO kitComponent) {
		/*Properties props = new Properties();
		try {
		InputStream inputStream = this.getClass().getClassLoader()
				.getResourceAsStream("DmmConstants.properties");
			props.load(inputStream);
		} catch (IOException e) {
			System.out.println("AppServicePOJO getProductDetailForFGSKU property file reading error" + e.toString() );
		}
		String strCanada = props
				.getProperty("canada");
		String strUnitedStates = props
		.getProperty("unitedstates");
		System.out.println("AppServicePOJO.getProductDetailForFGSKU(): getting kit component cost for getFinishedGoodsSku=" + kitComponent.getFinishedGoodsSku());
		System.out.println("AppServicePOJO.getProductDetailForFGSKU(): getting kit component [sCountryName,strCanada,strUnitedStates]=["+ "["+sCountryName+"," + strCanada +"," +strUnitedStates+"]");*/
		/*System.out.println("AppServicePOJO.getProductDetailForFGSKU(): getting kit component [sCountryName]=["+ "["+sCountryName+"," +"]");*/
		List<ProductDetail> pd = productDetailManager.getProductDetailDAO().findBySku(kitComponent.getFinishedGoodsSku());
		CostPriceDTO cpDTO;
		List<CostPriceDTO> costs = new ArrayList<CostPriceDTO>();
		if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_USA)){
			if(pd!=null && pd.size()>0){
				cpDTO = new CostPriceDTO("Bench", pd.get(0).getBenchMarkPrice(), kitComponent.getQtyPerDisplay());
				costs.add(cpDTO);
				cpDTO = new CostPriceDTO("List", pd.get(0).getListPrice(), kitComponent.getQtyPerDisplay());
				costs.add(cpDTO);
				cpDTO = new CostPriceDTO("COGS", (pd.get(0).getLaborCost()+  pd.get(0).getMaterialCost()), kitComponent.getQtyPerDisplay());
				costs.add(cpDTO);
			}
		}else if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_CAN)){
			if(pd!=null && pd.size()>0){
				cpDTO = new CostPriceDTO("Bench", pd.get(0).getCanBenchMarkPrice(), kitComponent.getQtyPerDisplay());
				costs.add(cpDTO);
				cpDTO = new CostPriceDTO("List", pd.get(0).getCanListPrice(), kitComponent.getQtyPerDisplay());
				costs.add(cpDTO);
				cpDTO = new CostPriceDTO("COGS", (pd.get(0).getCanLaborCost()+  pd.get(0).getCanMaterialCost()), kitComponent.getQtyPerDisplay());
				costs.add(cpDTO);
			}
		}
		return costs;
	}
	
	public List<LocationDTO> loadLocationDTOs() {
		System.out.println("AppServicePOJO:loadLocationDTOs() starts");
		return ConverterFactory.convertLocationList(locationManager.getAllActiveLocations());
	}

	public List<LocationCountryDTO> loadLocationCountryDTOs() {
		System.out.println("AppServicePOJO:loadLocationCountryDTOs() starts");
		return ConverterFactory.convertLocationCountryList(locationCountryManager.getAllActiveLocationsCountry());
	}
	
	public Map<Long, DisplayDcDTO> findDisplayDcByDisplay(DisplayDTO display) {
		return ConverterFactory.convertDcMap(displayDcManager.findDisplayDcMapByDisplay(ConverterFactory.convert(display)));
	}

	public Map<Long, DisplayDcDTO> saveDisplayDcDTOList(List<DisplayDcDTO> displayDcDTOList) {
		Map<Long, DisplayDcDTO> map = new HashMap<Long, DisplayDcDTO>();
		for (int i = 0; i < displayDcDTOList.size(); i++) {
			DisplayDcDTO dto = displayDcManager.save(displayDcDTOList.get(i));
			map.put(dto.getDisplayDcId(), dto);
		}
		return map;
	}

	public List<DisplayShipWaveDTO> saveDisplayShipingWaveDTOListTest(List<DisplayShipWaveDTO> displayShipWaveDTOList) {
		List<DisplayShipWaveDTO> list = new ArrayList<DisplayShipWaveDTO>();
		for (int i = 0; i < displayShipWaveDTOList.size(); i++) {
			DisplayShipWaveDTO dto = displayShipWaveManager.save(displayShipWaveDTOList.get(i));
			list.add(dto);
		}
		return list;
	}

	public Map<Long, DisplayShipWaveDTO> saveDisplayShipingWaveDTOList(List<DisplayShipWaveDTO> displayShipWaveDTOList) {
		Map<Long, DisplayShipWaveDTO> map = new HashMap<Long, DisplayShipWaveDTO>();
		for (int i = 0; i < displayShipWaveDTOList.size(); i++) {
			DisplayShipWaveDTO dto = displayShipWaveManager.save(displayShipWaveDTOList.get(i));
			map.put(dto.getDisplayShipWaveId(), dto);
		}
		return map;
	}

	public Map<Long, DisplayShipWaveDTO> loadDisplayShippingWaveDTOMap(DisplayDTO display) {
		Map<Long, DisplayShipWaveDTO> map = new HashMap<Long, DisplayShipWaveDTO>();

		List<DisplayShipWaveDTO> list = displayShipWaveManager.findDisplayShipWavesByDisplay(display);
		for (int i = 0; i < list.size(); i++) {
			map.put(list.get(i).getDisplayShipWaveId(), list.get(i));
		}
		return map;
	}

	public List<DisplayShipWaveDTO> loadDisplayShippingWaveDTOList(DisplayDTO displayDTO) {
		List<DisplayShipWaveDTO> list = displayShipWaveManager.findDisplayShipWavesByDisplay(displayDTO);
		return list;
	}

	public List<DisplayPackoutDTO> saveDisplayPackoutDTOList(List<DisplayPackoutDTO> list) {
		List<DisplayPackoutDTO> displayPackoutDTOList = new ArrayList<DisplayPackoutDTO>();

		for (int i = 0; i < list.size(); i++) {
			DisplayPackoutDTO dto = displayPackoutManager.save(list.get(i));
			displayPackoutDTOList.add(dto);
		}
		return displayPackoutDTOList;
	}

	public Map<Long, DisplayPackoutDTO> loadDisplayPackouts(DisplayDcDTO displayDcDTO) {
		Map<Long, DisplayPackoutDTO> map = new HashMap<Long, DisplayPackoutDTO>();
		List<DisplayPackoutDTO> list = displayPackoutManager.findDisplayPackoutsByDisplayDc(displayDcDTO);
		for (int i = 0; i < list.size(); i++) {
			map.put(list.get(i).getDisplayPackoutId(), list.get(i));
		}
		return map;
	}

	public List<DisplayDcAndPackoutDTO> loadDisplayDcAndPackouts(DisplayDTO display) {
		return displayDcManager.findDisplayDcAndPackoutsByDisplay(display);
	}

	public List<DisplayPackoutDTO> loadDisplayPackoutList(DisplayDcDTO displayDcDTO) {
		return displayPackoutManager.findDisplayPackoutsByDisplayDc(displayDcDTO);
	}

	public List<SlimPackoutDTO> loadSlimDisplayPackoutGrid(DisplayDTO displayDTO) {
		return displayPackoutManager.findDisplayPackoutsByDisplay(displayDTO);
	}

	public Boolean deleteDisplayShipWaves(DisplayShipWaveDTO displayShipWaveDTO ) {
		DisplayShipWave d = ConverterFactory.convert(displayShipWaveDTO);
		if(d.getDisplayShipWaveId()!=null){
			List<DisplayPackout> packoutList=displayPackoutManager.getDisplayPackoutDAO().findDisplayPackoutsByShipWave(d);
			if((packoutList!=null) && (packoutList.size()>0)){
				for (int i = 0; i < packoutList.size(); i++) {
					displayPackoutManager.getDisplayPackoutDAO().delete(packoutList.get(i));
				}
				displayShipWaveManager.delete(displayShipWaveDTO);
				return Boolean.FALSE;
			}else{
				displayShipWaveManager.delete(displayShipWaveDTO);
				System.out.println("deleteDisplayShipWaves deleted because no packout is attached");
			}
			displayShipWaveManager.delete(displayShipWaveDTO);
		}
		return Boolean.TRUE;
	}


	public DisplayDTO savePackoutData(DisplayDTO displayModel, List<DisplayDcAndPackoutDTO>displayDcAndPackoutList, List<DisplayShipWaveDTO>displayShippingWaveList) {
		// delete packout detail
		System.out.println("AppServicePOJO.savePackoutData(): delete packout detail");
		for (int i = 0; i < displayDcAndPackoutList.size(); i++) {
			DisplayDcAndPackoutDTO dapDTO = (DisplayDcAndPackoutDTO)displayDcAndPackoutList.get(i);
			if (dapDTO.getType().equals("Packout") && (dapDTO.getDeleteFlg() != null && dapDTO.getDeleteFlg().equals("Y"))) {
				DisplayPackoutDTO displayPackoutDTO = DisplayPackoutDTO.displayPackoutDTO(dapDTO);
				if (displayPackoutDTO.getDisplayPackoutId() != null)
					displayPackoutManager.delete(displayPackoutDTO);
			}
		}    	

		// delete display dc (and display_planner child records)
		System.out.println("AppServicePOJO.savePackoutData(): delete dc");
		for (int i = 0; i < displayDcAndPackoutList.size(); i++) {
			DisplayDcAndPackoutDTO dapDTO = (DisplayDcAndPackoutDTO)displayDcAndPackoutList.get(i);
			if (dapDTO.getType().equals("DC") && (dapDTO.getDeleteFlg() != null && dapDTO.getDeleteFlg().equals("Y"))) {
				DisplayDcDTO displayDcDTO = DisplayDcDTO.displayDcDTO(dapDTO);
				if (displayDcDTO.getDisplayDcId() != null)
					displayPlannerManager.getDisplayPlannerDAO().deleteByDisplayDcId(displayDcDTO.getDisplayDcId());
				displayDcManager.delete(displayDcDTO);
			}
		}

		// delete ship waves
		System.out.println("AppServicePOJO.savePackoutData(): ship waves");
		for (int i = 0; i < displayShippingWaveList.size(); i++) {
			DisplayShipWaveDTO dto = displayShippingWaveList.get(i);
			if (dto.isDeleteFlg() != null && dto.isDeleteFlg()){
				displayShipWaveManager.delete(dto);
			}
		}

		// save shipping wave (save ids)
		System.out.println("AppServicePOJO.savePackoutData(): save ship waves");
		Map<Integer, Long> shipWaveIds = new HashMap<Integer, Long>();
		for (int i = 0; i < displayShippingWaveList.size(); i++) {
			DisplayShipWaveDTO dto = displayShipWaveManager.save(displayShippingWaveList.get(i));
			shipWaveIds.put(dto.getShipWaveNum(), dto.getDisplayShipWaveId());
		}

		// package and save or DC nodes (save dc ids)
		System.out.println("AppServicePOJO.savePackoutData(): save dc");
		Map<Long, Long> displayDcIds = new HashMap<Long, Long>();
		for (int i = 0; i < displayDcAndPackoutList.size(); i++) {
			DisplayDcAndPackoutDTO dapDTO = (DisplayDcAndPackoutDTO)displayDcAndPackoutList.get(i);
			if (dapDTO.getType().equals("DC") && (dapDTO.getDeleteFlg() == null || dapDTO.getDeleteFlg().equals("N"))) {
				DisplayDcDTO displayDcDTO = DisplayDcDTO.displayDcDTO(dapDTO);
				DisplayDcDTO dto = displayDcManager.save(displayDcDTO);
				displayDcIds.put(dto.getLocationId(), dto.getDisplayDcId());
			}
		}
		// package and save packout detail
		for (int i = 0; i < displayDcAndPackoutList.size(); i++) {
			DisplayDcAndPackoutDTO dapDTO = (DisplayDcAndPackoutDTO)displayDcAndPackoutList.get(i);
			if (dapDTO.getType().equals("Packout") && (dapDTO.getDeleteFlg() == null || dapDTO.getDeleteFlg().equals("N"))) {
				dapDTO.setDisplayShipWaveId((Long)shipWaveIds.get(new Integer(dapDTO.getShipWaveNum().intValue())));
				dapDTO.setDisplayDcId((Long)displayDcIds.get(dapDTO.getLocationId()));
				DisplayPackoutDTO displayPackoutDTO = DisplayPackoutDTO.displayPackoutDTO(dapDTO);
				displayPackoutManager.save(displayPackoutDTO);
			}
		}    
		/*
		 * Description : Fix for defect # 170. Display Planner Assignment. Errors when adding kit component before DCs.  Does not tie DC to component
		 * 				 revisited this issue and avoided saving of kit component and calling price exception procedure. instead, saves only the association
		 *               of kitcomponent and display planner.
		 * Date		   : 10-22-2010
		 * By		   : Mari	  	
		 */
		List<KitComponent> kitComponentList=null ;
		if (displayModel.getDisplayId() != null) {
			kitComponentList=kitComponentManager.getKitComponentDAO().findByDisplayId(displayModel.getDisplayId());
		}
		List<DisplayPlannerDTO> displayPlannerList=null;
		Iterator<DisplayPlannerDTO> iter=null;
		DisplayPlannerDTO dpDTO=null;
		for(KitComponent kitComponent:kitComponentList){
			displayPlannerList=getDisplayPlanners(ConverterFactory.convert(kitComponent), displayModel);
			iter = displayPlannerList.iterator();
			if(iter!=null){
				while (iter.hasNext()) {
					dpDTO = (DisplayPlannerDTO)iter.next();
					if (dpDTO.getKitComponentId() == null) dpDTO.setKitComponentId(kitComponent.getKitComponentId());
					displayPlannerManager.save(dpDTO);
				}
			}
		}
		return displayManager.save(displayModel);
	}

	public List<ValidPlantDTO> getValidPlantDTOs() {
		if (validPlantDTOs == null) {
			loadValidPlantDTOs();
		}
		return validPlantDTOs;
	}

	private void loadValidPlantDTOs() {
		validPlantDTOs = validPlantManager.getValidPlantDTOs();
	}

	public List<ValidPlannerDTO> getValidPlannerDTOs() {
		if (validPlannerDTOs == null) {
			loadValidPlannerDTOs();
		}
		return validPlannerDTOs;
	}

	private void loadValidPlannerDTOs() {
		validPlannerDTOs = validPlannerManager.getValidPlannerDTOs();
	}

	public List<ValidLocationDTO> getValidLocationDTOs() {
		if (validLocationDTOs == null) {
			loadValidLocationDTOs();
		}
		return validLocationDTOs;
	}

	private void loadValidLocationDTOs() {
		validLocationDTOs = validLocationManager.getValidLocationDTOs();
	}

	public List<ProductDetailDTO> getProductDetail(String sku) {
		List<ProductDetail> productDetails = productDetailManager.getProductDetailDAO().findBySku(sku);
		List<ProductDetailDTO> productDetailDTOs = ConverterFactory.convertProductDetails(productDetails);
		return productDetailDTOs;
	}

	public UserDTO getUserInfo() {
		return userManager.getCurrentUserInfo();
	}

	public List<ValidBuDTO> getValidBuDTOs() {
		if (validBuDTOs == null) {
			validBuDTOs = validBuManager.getValidBuDTOs();
		}
		return validBuDTOs;
	}

	public DisplayDTO saveCopiedDisplayDTO(DisplayDTO displayDTO) {
		return null;
	}

	public BooleanDTO isDuplicateSKU(String sku) {
		return this.displayManager.isDuplicateSKU(sku);
	}

	public List<ManufacturerDTO> getManufacturerDTOs() {
		if (manufacturerDTOs == null) {
			loadManufacturers();
		}
		return manufacturerDTOs;
	}
	
	private void loadManufacturers() {
		manufacturerDTOs = ConverterFactory.convertManufacturerList(manufacturerManager.getManufacturers());
	}
	
	public List<ManufacturerCountryDTO> getManufacturerCountryDTOs() {
		if (manufacturerCountryDTOs == null) {
			loadManufacturersCountry();
		}
		return manufacturerCountryDTOs;
	}
	
	private void loadManufacturersCountry() {
		manufacturerCountryDTOs = ConverterFactory.convertManufacturerCountryList(manufacturerCountryManager.getManufacturersCountry());
	}
	
	public List<ManufacturerCountryDTO> getAllManufacturerCountryDTOs() {
		return ConverterFactory.convertManufacturerCountryList(manufacturerCountryManager.getAllManufacturersCountry());	
	}
	
	public List<StatusDTO> getRYGStatusDTOs() {
		List<StatusDTO> rygStatusStore = new ArrayList<StatusDTO>();

		StatusDTO rygStatusModel = new StatusDTO();
		rygStatusModel.setStatusId(new Long(1));
		rygStatusModel.setStatusName("R");
		rygStatusModel.setDescription("Red");
		rygStatusStore.add(rygStatusModel);

		rygStatusModel = new StatusDTO();
		rygStatusModel.setStatusId(new Long(2));
		rygStatusModel.setStatusName("G");
		rygStatusModel.setDescription("Green");
		rygStatusStore.add(rygStatusModel);

		rygStatusModel = new StatusDTO();
		rygStatusModel.setStatusId(new Long(3));
		rygStatusModel.setStatusName("Y");
		rygStatusModel.setDescription("Yellow");
		rygStatusStore.add(rygStatusModel);
		return rygStatusStore;
	}

	public  void setFilters(Map<String, BaseModelData> filters){
		HttpSession session = ServletUtils.getRequest().getSession();
		session.setAttribute("filters", filters);
	}
	public Map<String, BaseModelData> getFilters(){
		return (Map<String, BaseModelData>)ServletUtils.getRequest().getSession().getAttribute("filters");
	}

	public String getServletPath(){
		return ServletUtils.getRequest().getContextPath();
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public void setCustomerMarketingManager(
			CustomerMarketingManager customerMarketingManager) {
		this.customerMarketingManager = customerMarketingManager;
	}

	public void setDisplayManager(DisplayManager displayManager) {
		this.displayManager = displayManager;
	}

	public void setDisplayDcManager(DisplayDcManager displayDcManager) {
		this.displayDcManager = displayDcManager;
	}

	public void setLocationManager(LocationManager locationManager) {
		this.locationManager = locationManager;
	}

	public void setProductDetailManager(ProductDetailManager productDetailManager) {
		this.productDetailManager = productDetailManager;
	}

	public void setProductPlannerManager(ProductPlannerManager productPlannerManager) {
		this.productPlannerManager = productPlannerManager;
	}

	public void setPromoPeriodManager(PromoPeriodManager promoPeriodManager) {
		this.promoPeriodManager = promoPeriodManager;
	}

	public void setDisplayPlannerManager(DisplayPlannerManager displayPlannerManager) {
		this.displayPlannerManager = displayPlannerManager;
	}

	public void setDisplaySalesRepManager(
			DisplaySalesRepManager displaySalesRepManager) {
		this.displaySalesRepManager = displaySalesRepManager;
	}

	public void setStructureManager(StructureManager structureManager) {
		this.structureManager = structureManager;
	}

	public void setStatusManager(StatusManager statusManager) {
		this.statusManager = statusManager;
	}

	public void setValidPlantManager(ValidPlantManager validPlantManager) {
		this.validPlantManager = validPlantManager;
	}

	public void setValidPlannerManager(ValidPlannerManager validPlannerManager) {
		this.validPlannerManager = validPlannerManager;
	}

	public void setValidLocationManager(ValidLocationManager validLocationManager) {
		this.validLocationManager = validLocationManager;
	}

	public void setPromoPeriodDTOMap(Map<Long, PromoPeriodDTO> promoPeriodDTOMap) {
		this.promoPeriodDTOMap = promoPeriodDTOMap;
	}

	public void setCustomerManager(CustomerManager customerManager) {
		this.customerManager = customerManager;
	}
	public void setDisplayMaterialManager(	DisplayMaterialManager displayMaterialManager) {
		this.displayMaterialManager = displayMaterialManager;
	}

	public void setDisplayPackoutManager(DisplayPackoutManager displayPackoutManager) {
		this.displayPackoutManager = displayPackoutManager;
	}

	public void setDisplayShipWaveManager(DisplayShipWaveManager displayShipWaveManager) {
		this.displayShipWaveManager = displayShipWaveManager;
	}

	public void setKitComponentManager(KitComponentManager kitComponentManager) {
		this.kitComponentManager = kitComponentManager;
	}
	public void setDisplayCostManager(DisplayCostManager displayCostManager) {
		this.displayCostManager = displayCostManager;
	}

	public void setDisplayLogisticsManager(
			DisplayLogisticsManager displayLogisticsManager) {
		this.displayLogisticsManager = displayLogisticsManager;
	}

	public void setValidBuManager(ValidBuManager validBuManager) {
		this.validBuManager = validBuManager;
	}

	public ManufacturerManager getManufacturerManager() {
		return manufacturerManager;
	}

	public void setManufacturerManager(ManufacturerManager manufacturerManager) {
		this.manufacturerManager = manufacturerManager;
	}

	public void setDisplayCostCommentManager(DisplayCostCommentManager displayCostCommentManager) {
		this.displayCostCommentManager = displayCostCommentManager;
	}

	public void setUserChangeLogManager(UserChangeLogManager userChangeLogManager) {
		this.userChangeLogManager = userChangeLogManager;
	}

	public void setDisplayCostImageManager(DisplayCostImageManager displayCostImageManager) {
		this.displayCostImageManager = displayCostImageManager;
	}

	public void setCustomerPctManager(CustomerPctManager customerPctManager) {
		this.customerPctManager = customerPctManager;
	}
	
	public void setCustomerCountryManager(CustomerCountryManager customerCountryManager) {
		this.customerCountryManager = customerCountryManager;
	}
	
	public CustomerCountryManager getCustomerCountryManager() {
		return customerCountryManager;
	}
	
	public void setPromoPeriodCountryManager(PromoPeriodCountryManager promoPeriodCountryManager) {
		this.promoPeriodCountryManager = promoPeriodCountryManager;
	}
	
	public PromoPeriodCountryManager getPromoPeriodCountryManager() {
		return promoPeriodCountryManager;
	}
	
	public void setManufacturerCountryManager(ManufacturerCountryManager manufacturerCountryManager) {
		this.manufacturerCountryManager = manufacturerCountryManager;
	}
	
	public ManufacturerCountryManager getManufacturerCountryManager() {
		return manufacturerCountryManager;
	}
	
	public void setLocationCountryManager(LocationCountryManager locationCountryManager) {
		this.locationCountryManager = locationCountryManager;
	}
	
	public LocationCountryManager getLocationCountryManager() {
		return locationCountryManager;
	}
	
	public void callDisplayDeleteStoredProcedure(Long id, String username) {
		this.displayManager.callDisplayDeleteStoredProcedure(id, username);
	}

	public Map<Long, ManufacturerDTO> getManufacturerMapDTOs() {
		if (manufacturerDTOMap.size() == 0) {
			manufacturerMap();
		}
		return manufacturerDTOMap;
	}    

	private void manufacturerMap() {
		if (manufacturerDTOs == null || manufacturerDTOs.size() == 0) {
			manufacturerDTOs = getManufacturerDTOs();
		}
		System.out.println("manufacturerDTOs.size()="+manufacturerDTOs.size());
		for (int i = 0; i < manufacturerDTOs.size(); i++) {
			System.out.println("[ManufacturerId,Name]="+manufacturerDTOs.get(i).getManufacturerId() + " "+manufacturerDTOs.get(i).getName() );
			manufacturerDTOMap.put(manufacturerDTOs.get(i).getManufacturerId(), manufacturerDTOs.get(i));
		}
	}

	public Map<Long, ManufacturerCountryDTO> getManufacturerCountryMapDTOs() {
		if (manufacturerCountryDTOMap.size() == 0) {
			manufacturerCountryMap();
		}
		return manufacturerCountryDTOMap;
	}    

	private void manufacturerCountryMap() {
		if (manufacturerCountryDTOs == null || manufacturerCountryDTOs.size() == 0) {
			manufacturerCountryDTOs = getManufacturerCountryDTOs();
		}
		for (int i = 0; i < manufacturerCountryDTOs.size(); i++) {
			manufacturerCountryDTOMap.put(manufacturerCountryDTOs.get(i).getManufacturerId(), manufacturerCountryDTOs.get(i));
		}
	}
	
	public Map<Long, CountryDTO> getCountryMapDTOs() {
		if (countryDTOMap.size() == 0) {
			countryMap();
		}
		return countryDTOMap;
	}    

	private void countryMap() {
		if (activeCountryFlagDTOs == null || activeCountryFlagDTOs.size() == 0) {
			activeCountryFlagDTOs = getCountryDTOs();
		}
		for (int i = 0; i < activeCountryFlagDTOs.size(); i++) {
			countryDTOMap.put(activeCountryFlagDTOs.get(i).getCountryId(),activeCountryFlagDTOs.get(i));
		}
	}
	
	public Map<Long, CountryDTO> getActiveCountryFlagMapDTOs() {
		if (countryDTOMap.size() == 0) {
			activeCountryFlagMap();
		}
		return countryDTOMap;
	}    

	private void activeCountryFlagMap() {
		if (activeCountryFlagDTOs == null || activeCountryFlagDTOs.size() == 0) {
			activeCountryFlagDTOs = loadCountryFlagDTOs();
		}
		for (int i = 0; i < activeCountryFlagDTOs.size(); i++) {
			countryDTOMap.put(activeCountryFlagDTOs.get(i).getCountryId(),activeCountryFlagDTOs.get(i));
		}
	}
	
	public List<CostAnalysisKitComponentDTO> saveCostAnalysisKitComponentInfo(List<CostAnalysisKitComponentDTO> costAnalysisDTOList, Long qtyForeCase, Double custProgramPct) {
		List<CostAnalysisKitComponentDTO> updateCostAnalysisKitComponentDTOs = ConverterFactory.updateCostAnalysisKitComponentDTOInfo(costAnalysisDTOList,qtyForeCase, custProgramPct);
		List<CostAnalysisKitComponent> costAnalysisKitComponents = ConverterFactory.convertCostAnalysisKitComponentDTOs(updateCostAnalysisKitComponentDTOs);
		Iterator<CostAnalysisKitComponent> iter = costAnalysisKitComponents.iterator();
		CostAnalysisKitComponent costAnalysisComp;
		KitComponent kitComp;
		System.out.println("AppServicePOJO:saveCostAnalysisKitComponentInfo starts");
		try{
			while (iter.hasNext()) {
				costAnalysisComp = (CostAnalysisKitComponent) iter.next();
				kitComp =  kitComponentManager.getKitComponentDAO().findById(costAnalysisComp.getKitComponentId());
				kitComp.setI2ForecastFlg(costAnalysisComp.getI2ForecastFlg().equalsIgnoreCase("Y")?true:false);
				kitComp.setQtyPerFacing(costAnalysisComp.getQtyPerDisplay());
				kitComp.setNumberFacings(costAnalysisComp.getNumberFacings());
				if(costAnalysisComp.getPriceException()!=null){
					kitComp.setPriceException( (costAnalysisComp.getPriceException().floatValue()));
				}else{
					kitComp.setPriceException(0.0f);
				}
				if(costAnalysisComp.getExcessInvtPct()!=null){
					kitComp.setExcessInvtPct(costAnalysisComp.getExcessInvtPct().floatValue());
				}else{
					kitComp.setExcessInvtPct(0.0f);
				}
				if(costAnalysisComp.getExcessInvtDollar()!=null){
					kitComp.setExcessInvtDollar(costAnalysisComp.getExcessInvtDollar().floatValue());
				}else{
					kitComp.setExcessInvtDollar(0.0f);
				}
				if(costAnalysisComp.getInPogYn()!=null){
					kitComp.setInPogYn(costAnalysisComp.getInPogYn().equalsIgnoreCase("Y")?true:false);
				}else{
					kitComp.setInPogYn(false);
				}
				if(costAnalysisComp.getOverrideExcPct()!=null){
					kitComp.setOverrideExceptionPct(costAnalysisComp.getOverrideExcPct().floatValue());
				}else{
					kitComp.setOverrideExceptionPct(0.0f);
				}
				
				/*
				 * By Marianandan Arockiasamy
				 * Date : 08/06/2014
				 * Description : Program Percentage
				 */
					
					   if(costAnalysisComp.getOverrideExcPct() != null && costAnalysisComp.getOverrideExcPct().doubleValue() != 0.0D){
						   System.out.println("AppServicePOJO:saveCostAnalysisKitComponentInfo <<OverrideExcPct>>: "+ Float.valueOf(costAnalysisComp.getOverrideExcPct().floatValue()));
		                   kitComp.setProgPrcnt(Float.valueOf(costAnalysisComp.getOverrideExcPct().floatValue()));
					   }else{
						   System.out.println("AppServicePOJO:saveCostAnalysisKitComponentInfo <<EstimatedProgramPct>>: "+ costAnalysisComp.getEstimatedProgramPct().floatValue());
		                    kitComp.setProgPrcnt(Float.valueOf(costAnalysisComp.getEstimatedProgramPct().floatValue()));
					   }
					   
				
				System.out.println("AppServicePOJO:saveCostAnalysisKitComponentInfo "+ costAnalysisComp.getOverrideExcPct());
				kitComp.setDtLastChanged(new Date());
				kitComponentManager.getKitComponentDAO().attachDirty(kitComp);
			}
			System.out.println("AppServicePOJO:saveCostAnalysisKitComponentInfo ends");
		}catch(Exception ex){
			System.out.println("AppServicePOJO:saveCostAnalysisKitComponentInfo ..error.."+ex.toString());
		}
		return updateCostAnalysisKitComponentDTOs;
	}
	
	public List<CorrugateComponentsDetailDTO> saveCorrugateComponentInfo(List<CorrugateComponentsDetailDTO> corrugateDTOList, DisplayDTO displayDTO) {
		System.out.println("AppServicePOJO:saveCorrugateComponentInfo starts");
		List<CorrugateComponentsDetailDTO> updatecorrugateComponentDTOs = ConverterFactory.updateCorrugateComponentDTOInfo(corrugateDTOList,displayDTO);
		List<CorrugateComponentsDetail> corrugateComponents = ConverterFactory.convertCorrComponentDTOs(updatecorrugateComponentDTOs);
		Iterator<CorrugateComponentsDetail> iter = corrugateComponents.iterator();
		CorrugateComponentsDetail corrCompDetail;
		CorrugateComponent corrComp;
		try{
			while (iter.hasNext()) {
				corrCompDetail = (CorrugateComponentsDetail) iter.next();
				corrComp =  corrugateComponentManager.getCorrugateComponentDAO().findById(corrCompDetail.getCorrugateComponentId());
				if(corrComp==null){
					corrComp=new CorrugateComponent();
				}
				corrComp.setManufacturerId(corrCompDetail.getManufacturerId());
				corrComp.setDisplayId(corrCompDetail.getDisplayId());
				corrComp.setTotalCost(corrCompDetail.getTotalCost());
				corrComp.setVendorPartNum(corrCompDetail.getVendorPartNum());
				corrComp.setGloviaPartNum(corrCompDetail.getGloviaPartNum());
				corrComp.setDescription(corrCompDetail.getDescription());
				corrComp.setBoardTest(corrCompDetail.getBoardTest());
				corrComp.setMaterial(corrCompDetail.getMaterial());
				corrComp.setCountryId(corrCompDetail.getCountryId());
				corrComp.setQtyPerDisplay(corrCompDetail.getQtyPerDisplay());
				corrComp.setPiecePrice(corrCompDetail.getPiecePrice());
				corrComp.setTotalCost(corrCompDetail.getTotalCost());
				corrComp.setTotalQty(corrCompDetail.getTotalQty());
				/*
				 * Note: Implement GIPM way
				 * Date:01-06-2011
				 */
				corrComp.setUserCreated(corrCompDetail.getUserCreated());
				corrComp.setDtCreated(corrCompDetail.getDtCreated());
				corrComp.setUserLastChanged(corrCompDetail.getUserLastChanged());
				corrComp.setDtLastChanged(corrCompDetail.getDtLastChanged());
				corrugateComponentManager.getCorrugateComponentDAO().attachDirty(corrComp);
			}
		}catch(Exception ex){
			System.out.println("AppServicePOJO:saveCostAnalysisKitComponentInfo ..error.."+ex.toString());
		}
		System.out.println("AppServicePOJO:saveCostAnalysisKitComponentInfo ends");
		return updatecorrugateComponentDTOs;
	}
	public List<CostAnalysisKitComponentDTO> getCostAnalysisKitComponentInfo(DisplayScenarioDTO displayScenarioDTO, Double customerProgramPct) {
		System.out.println("AppServicePOJO:getCostAnalysisKitComponentInfo starts");
		if (displayScenarioDTO==null || displayScenarioDTO.getDisplayScenarioId()==null ) return null;
		System.out.println("AppServicePOJO:getCostAnalysisKitComponentInfo displayScenarioId="+displayScenarioDTO.getDisplayScenarioId());
		List<CostAnalysisKitComponent> costAnalysisKitComponents = kitComponentManager.getKitComponentDAO().findCostAnalysisKitComponentByDisplayId(displayScenarioDTO.getDisplayId(),displayScenarioDTO.getDisplayScenarioId());
		List<CostAnalysisKitComponentDTO> costAnalysisKitComponentDTOs = ConverterFactory.convertCostAnalysisKitComponents(costAnalysisKitComponents);
		if(costAnalysisKitComponentDTOs==null || costAnalysisKitComponentDTOs.size()==0) return null;
		Iterator<CostAnalysisKitComponentDTO> iter = costAnalysisKitComponentDTOs.iterator();
		CostAnalysisKitComponentDTO comp;
		while (iter.hasNext()) {
			comp = (CostAnalysisKitComponentDTO) iter.next();
			comp.updateCalculatedFields(displayScenarioDTO.getActualQty(),customerProgramPct );
		}
		System.out.println("AppServicePOJO:getCostAnalysisKitComponentInfo end");
		return costAnalysisKitComponentDTOs;
	}
	
	public List<CostAnalysisKitComponentDTO> reCalculateILSPriceException(DisplayDTO displayDTO,Date reCalcDate,Double customerProgramPct, DisplayScenarioDTO displayScenarioDTO) {
		System.out.println("AppServicePOJO:reCalculateILSPriceException starts");
		if(displayScenarioDTO==null || displayScenarioDTO.getDisplayScenarioId()==null) return null;
		List<CostAnalysisKitComponent> costAnalysisKitComponents = kitComponentManager.getKitComponentDAO().findCostAnalysisKitComponentByDisplayId(displayDTO.getDisplayId(),displayScenarioDTO.getDisplayScenarioId());
		if(costAnalysisKitComponents==null || costAnalysisKitComponents.size()==0) return null;
		Iterator<CostAnalysisKitComponent> iter = costAnalysisKitComponents.iterator();
		CostAnalysisKitComponent comp;
		KitComponent kitComponent;
		if(reCalcDate==null){
			reCalcDate=new Date();
		}
		kitComponentManager.getKitComponentDAO().updateReCalcDate(displayDTO.getDisplayId(), reCalcDate);
		while (iter.hasNext()) {
			comp = (CostAnalysisKitComponent) iter.next();
			System.out.println("AppServicePOJO:reCalculateILSPriceException 3...comp.getKitComponentId()="+comp.getKitComponentId());
			
			/*
			 * re-calculate
			 */
			
			kitComponent=kitComponentManager.getKitComponentDAO().findById(comp.getKitComponentId());
			saveCountryWisePriceData(kitComponent, displayDTO);
			kitComponentManager.getKitComponentDAO().callILSPriceExceptionForKitComponent(comp.getKitComponentId(),reCalcDate);
			System.out.println("reCalculateILSPriceException : foreCastQty="+displayDTO.getQtyForecast() + " customerProgramPct="+customerProgramPct+ " compDTO.kitId= "+comp.getKitComponentId());
		}
		return getCostAnalysisKitComponentInfo(displayScenarioDTO,customerProgramPct);
	}
	
	public void saveCountryWisePriceData(KitComponent kitComponent,DisplayDTO display  ){
		System.out.println("AppServicePOJO:saveCountryWisePriceData starts");
		try{
			/*Properties props = new Properties();
			try {
				System.out.println("AppServicePOJO:saveCountryWisePriceData ..1..");
				InputStream inputStream = this.getClass().getClassLoader()
					.getResourceAsStream("DmmConstants.properties");
				System.out.println("AppServicePOJO:saveCountryWisePriceData ..2..");
				props.load(inputStream);
			} catch (IOException e) {
				System.out.println("AppServicePOJO property file reading error" + e.toString() );
			}
			System.out.println("AppServicePOJO:saveCountryWisePriceData ..3..");
			String strCanada = props.getProperty("canada");
			String strUnitedStates = props.getProperty("unitedstates");
			*/
			ProductDetail pd=null;
			String sCountryName=display.getCountryByCountryId()==null?"":display.getCountryByCountryId().getCountryName();
/*			System.out.println("AppServicePOJO.saveCountryWisePriceData: getting kit component [sCountryName,strCanada,strUnitedStates]=["+ "["+sCountryName+"," + strCanada +"," +strUnitedStates+"]");
*/			/*System.out.println("AppServicePOJO.saveCountryWisePriceData: getting kit component [sCountryName]=["+ "["+sCountryName+"," +"]");*/
			List<ProductDetail> pds=null;
			if(!display.getDisplaySetupId().equals(2L)){//DISPLAY_SETUP_FG_NEW_UPC
				pd = productDetailManager.getProductDetailDAO().findById(kitComponent.getSkuId());
				if(pd!=null ){
					if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_USA)){
						if(pd!=null && pd.getBenchMarkPrice()!=null ){
							kitComponent.setBenchMarkPerUnit(pd.getBenchMarkPrice());
						}else{
							kitComponent.setBenchMarkPerUnit(0.00f);
						}
						if(pd!=null &&	pd.getListPrice()!=null ){
							kitComponent.setInvoiceUnitPrice(pd.getListPrice());
						}else{
							kitComponent.setInvoiceUnitPrice(0.00f);
						}
						if(pd!=null && pd.getFullBurdenCost()!=null ){
							kitComponent.setCogsUnitPrice(pd.getLaborCost()+ pd.getMaterialCost() );
						}else{
							kitComponent.setCogsUnitPrice(0.00f);
						}
					}else if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_CAN)){
						if(pd!=null && pd.getCanBenchMarkPrice()!=null ){
							kitComponent.setBenchMarkPerUnit(pd.getCanBenchMarkPrice());
						}else{
							kitComponent.setBenchMarkPerUnit(0.00f);
						}
						if(pd!=null &&	pd.getCanListPrice()!=null ){
							kitComponent.setInvoiceUnitPrice(pd.getCanListPrice());
						}else{
							kitComponent.setInvoiceUnitPrice(0.00f);
						}
						if(pd!=null && pd.getCanFullBurdenCost()!=null ){
							kitComponent.setCogsUnitPrice(pd.getCanLaborCost()+ pd.getCanMaterialCost() );
						}else{
							kitComponent.setCogsUnitPrice(0.00f);
						}
					}
				}
			}else{
				System.out.println("AppServicePOJO:saveCountryWisePriceData yes DISPLAY_SETUP_FG_NEW_UPC");
				pds = productDetailManager.getProductDetailDAO().findBySku(kitComponent.getFinishedGoodsSku());
/*				System.out.println("AppServicePOJO.saveCountryWisePriceData: getting kit component [sCountryName,strCanada,strUnitedStates]=["+ "["+sCountryName+"," + strCanada +"," +strUnitedStates+"]");
*/				if(pds!=null && pds.size()>0){
					if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_USA)){
						if(pds!=null && pds.get(0).getBenchMarkPrice()!=null ){
							kitComponent.setBenchMarkPerUnit(pds.get(0).getBenchMarkPrice());
						}else{
							kitComponent.setBenchMarkPerUnit(0.00f);
						}
						if(pds!=null &&	pds.get(0).getListPrice()!=null ){
							kitComponent.setInvoiceUnitPrice(pds.get(0).getListPrice());
						}else{
							kitComponent.setInvoiceUnitPrice(0.00f);
						}
						if(pds!=null && pds.get(0).getFullBurdenCost()!=null ){
							kitComponent.setCogsUnitPrice(pds.get(0).getLaborCost()+pds.get(0).getMaterialCost());
						}else{
							kitComponent.setCogsUnitPrice(0.00f);
						}
					}else if(sCountryName.equalsIgnoreCase(Constants.DISPLAY_COUNTRY_CAN)){
						if(pds!=null && pds.get(0).getCanBenchMarkPrice()!=null ){
							kitComponent.setBenchMarkPerUnit(pds.get(0).getCanBenchMarkPrice());
						}else{
							kitComponent.setBenchMarkPerUnit(0.00f);
						}
						if(pds!=null &&	pds.get(0).getCanListPrice()!=null ){
							kitComponent.setInvoiceUnitPrice(pds.get(0).getCanListPrice());
						}else{
							kitComponent.setInvoiceUnitPrice(0.00f);
						}
						if(pds!=null && pds.get(0).getCanFullBurdenCost()!=null ){
							kitComponent.setCogsUnitPrice(pds.get(0).getCanLaborCost() + pds.get(0).getCanMaterialCost() );
						}else{
							kitComponent.setCogsUnitPrice(0.00f);
						}
					}
				}else{
					kitComponent.setBenchMarkPerUnit(0.00f);
					kitComponent.setInvoiceUnitPrice(0.00f);
					kitComponent.setCogsUnitPrice(0.00f);
				}
			}
		kitComponentManager.getKitComponentDAO().attachDirty(kitComponent);
		System.out.println("AppServicePOJO:saveCountryWisePriceData ends");
		}catch(Exception ex){
			System.out.println("AppServicePOJO:saveKitComponent error"+ ex.toString());
		}
	}

	public List<CorrugateComponentsDetailDTO> getCorrugateComponentInfo(DisplayDTO displayDTO) {
		System.out.println("AppServicePOJO:getCostAnalysisKitComponentInfo starts");
		if (displayDTO==null || displayDTO.getDisplayId()==null ) return null;
		List<CorrugateComponentsDetail> corrugateComponentDetails = corrugateComponentManager.getCorrugateComponentDAO().findCorrugateComponentByDisplayId(displayDTO.getDisplayId());
		List<CorrugateComponentsDetailDTO> corrugateComponentsDetailDTOs = ConverterFactory.convertCorrugateComponents(corrugateComponentDetails);
		if(corrugateComponentsDetailDTOs==null || corrugateComponentsDetailDTOs.size()==0) return null;
		Iterator<CorrugateComponentsDetailDTO> iter = corrugateComponentsDetailDTOs.iterator();
		CorrugateComponentsDetailDTO comp;
		while (iter.hasNext()) {
			comp = (CorrugateComponentsDetailDTO) iter.next();
			comp.updateCalculatedFields(displayDTO.getActualQty() );
		}
		System.out.println("AppServicePOJO:getCostAnalysisKitComponentInfo end");
		return corrugateComponentsDetailDTOs;
	}
	
	public DisplayCostDTO getDisplayCostDTO(DisplayDTO displayDTO) {
		DisplayCost displayCost;
		DisplayCostDTO displayCostsDTO = null;
		try{
			displayCost = displayCostManager.getDisplayCostDAO().loadByDisplayId(displayDTO.getDisplayId());
			displayCostsDTO = ConverterFactory.convert(displayCost);
		}catch(Exception ex){
			System.out.println("AppServicePOJO:getDisplayCostDTO error : "+ ex.toString());
		}
		System.out.println("AppServicePOJO:getDisplayCostDTO ends");
		return displayCostsDTO;
	}
	
	public List<DisplayCostDTO> getDisplayCostDTOs(DisplayDTO displayDTO) {
		List<DisplayCost> displayCosts;
		List<DisplayCostDTO> displayCostsDTOs = null;
		try{
			displayCosts = displayCostManager.getDisplayCostDAO().findByDisplayId(displayDTO.getDisplayId());
			displayCostsDTOs = ConverterFactory.convertDisplayCosts(displayCosts);
		}catch(Exception ex){
			System.out.println("AppServicePOJO:getDisplayCostDTO error : "+ ex.toString());
		}
		System.out.println("AppServicePOJO:getDisplayCostDTO total size " + displayCostsDTOs.size());
		System.out.println("AppServicePOJO:getDisplayCostDTO total ends " );

		return displayCostsDTOs;
	}
	
	public DisplayCostDTO saveDisplayCostDTO(DisplayCostDTO displayCostDTO) {
		return displayCostManager.save(displayCostDTO);
	}

	public Boolean deleteCostAnalysisKitComponent(CostAnalysisKitComponentDTO kitComponent) {
		displayPlannerManager.getDisplayPlannerDAO().deleteByKitComponentId(kitComponent.getKitComponentId());
		KitComponent kitCOmponent =kitComponentManager.getKitComponentDAO().findById(kitComponent.getKitComponentId());
		kitComponentManager.getKitComponentDAO().delete(kitCOmponent);
		return true;
	}

	public List<DisplayCostCommentDTO> getDisplayCostCommentDTO( Long displayScenarioId) {
		System.out.println("AppServicePOJO:getDisplayCostCommentDTO starts");
		List<DisplayCostComment> displayCostComments;
		List<DisplayCostCommentDTO> displayCostCommentsDTOs = null;
		//MessageBox.alert("AppServicePOJO:getDisplayCostCommentDTO","..1..displayScenarioId="+displayScenarioId,null);
		try{
			if(displayScenarioId==null) return null;
			displayCostComments = displayCostCommentManager.getDisplayCostCommentDAO().findByDisplayScenarioId(displayScenarioId);
			if(displayCostComments!=null && displayCostComments.size()>0 )
				displayCostCommentsDTOs = ConverterFactory.convertDisplayCostComments(displayCostComments);

		}catch(Exception ex){
			System.out.println("AppServicePOJO:getDisplayCostCommentDTO error "+ ex.toString());
		}
		System.out.println("AppServicePOJO:getDisplayCostCommentDTO ends");
		return displayCostCommentsDTOs;
	}

	public DisplayCostCommentDTO saveDisplayCostCommentDTO(DisplayCostCommentDTO displayCostCommentDTO) {
		System.out.println("AppServicePOJO:saveDisplayCostCommentDTO starts");
		return displayCostCommentManager.save(displayCostCommentDTO);
	}

	public List<CostAnalysisHistoryDTO> getUserChangeLogDTO(Long displayId, Long displayScenarioId, String tableName){
		if(displayScenarioId==null) return null;
		return userChangeLogManager.getCostAnalysisHistoryLog(displayId,displayScenarioId,tableName);
	}

	public void saveILSPriceException(Long kitComponentId, Date peReCalcDate){
		System.out.println("AppServicePOJO:saveILSPriceException starts" );
		if(peReCalcDate==null){
			peReCalcDate=new Date();
		}
		kitComponentManager.getKitComponentDAO().callILSPriceExceptionForKitComponent(kitComponentId,peReCalcDate);
	}

	public List<BuCostCenterSplitDTO> loadBUCostCenterSplitGroup(Double mdfPctVal,Double discountPctVal,
			Boolean bDefaultProgramDiscount,
			Long displayId, Long actualQtyVal, Double totalDisplaysOrdered,
			Double totalActualInvoiceTotCostVal, Double totalDisplayCostsVal,
			Double customerProgramPercentageVal,
			List<CostAnalysisKitComponentDTO> costAnalysisKitComponents) {
		System.out.println("AppServicePOJO:loadBUCostCenterSplitGroup starts");
		System.out.println("AppServicePOJO:loadBUCostCenterSplitGroup bDefaultProgramDiscount "+bDefaultProgramDiscount);
		List<ProgramBu> programBus=null;
		List<ProgramBuDTO> programBusDTOs=null;
		programBus=programBuManager.findByDisplayId(displayId);
		programBusDTOs=ConverterFactory.convertProgramBus(programBus);
		System.out.println("AppServicePOJO:loadBUCostCenterSplitGroup programBusDTOs "+ (programBusDTOs!=null ? programBusDTOs.size(): "NULL") + ", "+ displayId);
		System.out.println("AppServicePOJO:loadBUCostCenterSplitGroup ends");
		return ConverterFactory.convertBuCostCenterSplit( mdfPctVal, discountPctVal,programBusDTOs,actualQtyVal,totalDisplaysOrdered,totalActualInvoiceTotCostVal,customerProgramPercentageVal,totalDisplayCostsVal,costAnalysisKitComponents);
	}

	public List<DisplayCostImageDTO>  reloadDisplayCostImages(Long displayId,Long displayScenarioId){
		List<DisplayCostImage>   displayCostImages=displayCostImageManager.getDisplayCostImageDAO().findByDisplayScenarioId(displayScenarioId);
		return ConverterFactory.convertDisplayCostImage(displayCostImages);
	}

	public List<DisplayCostImageDTO>  saveCostAnalysisImage(DisplayCostImageDTO displayCostImageDTO){
		System.out.println("AppServicePOJO:saveCostAnalysisImage displayScnearioId=" + displayCostImageDTO.getDisplayScenarioId());
		return  displayCostImageManager.save(displayCostImageDTO);
	}
	
	public List<DisplayCostImageDTO>  deleteCostAnalysisImage(DisplayCostImageDTO displayCostImageDTO){
		return  displayCostImageManager.delete(displayCostImageDTO);
	}

	public DisplayScenarioManager getDisplayScenarioManager() {
		return displayScenarioManager;
	}

	public void setDisplayScenarioManager(
			DisplayScenarioManager displayScenarioManager) {
		this.displayScenarioManager = displayScenarioManager;
	}

	public ScenarioStatusManager getScenarioStatusManager() {
		return scenarioStatusManager;
	}

	public void setScenarioStatusManager(ScenarioStatusManager scenarioStatusManager) {
		this.scenarioStatusManager = scenarioStatusManager;
	}
	
	public void setCorrugateComponentManager(
			CorrugateComponentManager corrugateComponentManager) {
		this.corrugateComponentManager = corrugateComponentManager;
	}

	public CorrugateComponentManager getCorrugateComponentManager() {
		return corrugateComponentManager;
	}
	
	public void setCorrugateManager(
			CorrugateManager corrugateManager) {
		this.corrugateManager = corrugateManager;
	}

	public void setCstBuPrgmPctManager(CstBuPrgmPctManager cstBuPrgmPctManager) {
		this.cstBuPrgmPctManager = cstBuPrgmPctManager;
	}
	
	public void setProgramBuManager(ProgramBuManager programBuManager) {
		this.programBuManager = programBuManager;
	}
	
	public CorrugateManager getCorrugateManager() {
		return corrugateManager;
	}
	
	public void setCountryManager(
			CountryManager countryManager) {
		this.countryManager = countryManager;
	}

	public CountryManager getCountryManager() {
		return countryManager;
	}
	
	@Override
	public List<DisplayScenarioDTO> getDisplayScenarioDTO(Long displayId) {
		List<DisplayScenario> displayScenarios =displayScenarioManager.getDisplayScenarioDAO().getDisplayScenarios(displayId);
		return ConverterFactory.convertDisplayScenario(displayScenarios);
	}
	@Override
	public List<CountryDTO> getCountryDTOs() {
		if (countryDTOs == null) {
			countryDTOs=loadCountryDTOs();
		}
		return countryDTOs;
	}

	public List<CountryDTO> loadCountryDTOs() {
		System.out.println("AppServicePOJO:loadCountryDTOs() starts");
		return ConverterFactory.convertCountryList(countryManager.getAllActiveCountries());
	}
	
	public List<CountryDTO> getCountryFlagDTOs() {
		if (activeCountryFlagDTOs == null) {
			activeCountryFlagDTOs=loadCountryFlagDTOs();
		}
		return activeCountryFlagDTOs;
	}

	public List<CountryDTO> loadCountryFlagDTOs() {
		System.out.println("AppServicePOJO:loadCountryFlagDTOs() starts");
		return ConverterFactory.convertCountryList(countryManager.getAllActiveCountryFlag());
	}
	
	public Boolean callCorrugateComponentDelete(CorrugateComponentsDetailDTO corrComponentDetailDTO) {
		CorrugateComponent CorrComponent =corrugateComponentManager.getCorrugateComponentDAO().findById(corrComponentDetailDTO.getCorrugateComponentId());
		corrugateComponentManager.getCorrugateComponentDAO().delete(CorrComponent);
		return true;
	}
	
	public CorrugateDTO saveCorrugateDTO(CorrugateDTO corrugateDTO) {
		System.out.println("AppServicePOJO:saveCorrugateDTO starts");
		List<DisplayScenario> approvedScenarios= null;
		DisplayScenario approvedScenario= null;
		DisplayCost approvedDisplayCost=null;
		approvedScenarios=displayScenarioManager.getDisplayScenarioDAO().findApprovedScenario(corrugateDTO.getDisplayId());
		if(approvedScenarios!=null && approvedScenarios.size()>0){
			approvedScenario=approvedScenarios.get(0);
			approvedDisplayCost=displayCostManager.getDisplayCostDAO().loadByDisplayScenarioId(approvedScenario.getDisplayScenarioId());
			corrugateDTO.setCorrugateCost(approvedDisplayCost.getCorrugateCost());
		}
		return corrugateManager.save(corrugateDTO);
	}
	
	public CorrugateDTO getCorrugateDTO(DisplayDTO displayDTO) {
		Corrugate corrugate;
		List<DisplayScenario> approvedScenarios= null;
		DisplayScenario approvedScenario= null;
		CorrugateDTO corrugateDTO = null;
		DisplayCost approvedDisplayCost=null;
		try{
			corrugate = corrugateManager.getCorrugateDAO().findByDisplayId(displayDTO.getDisplayId());
		/*	select * from display_cost where display_id=1198 and display_scenario_id=432
			select * from display_scenario where display_id=1198 and scenario_approval_flg='Y'*/
			approvedScenarios=displayScenarioManager.getDisplayScenarioDAO().findApprovedScenario(displayDTO.getDisplayId());
			if(approvedScenarios!=null && approvedScenarios.size()>0){
				approvedScenario=approvedScenarios.get(0);
				approvedDisplayCost=displayCostManager.getDisplayCostDAO().loadByDisplayScenarioId(approvedScenario.getDisplayScenarioId());
				corrugate.setCorrugateCost(approvedDisplayCost.getCorrugateCost());
			}
			corrugateDTO = ConverterFactory.convert(corrugate);
		}catch(Exception ex){
			System.out.println("AppServicePOJO:getCorrugateDTO error : "+ ex.toString());
		}
		System.out.println("AppServicePOJO:getCorrugateDTOs total ends " );
		return corrugateDTO;
	}
	
	public CalculationSummaryDTO saveCalculationSummaryDTO(CalculationSummaryDTO calculationSummaryDTO) {
		System.out.println("AppServicePOJO:saveCalculationSummaryDTO starts");
		return calculationSummaryManager.save(calculationSummaryDTO);
	}
	
	public void setCalculationSummaryManager(CalculationSummaryManager calculationSummaryManager) {
		this.calculationSummaryManager = calculationSummaryManager;
	}

	public ManufacturerCountryDTO saveManufacturerCountryDTO(ManufacturerCountryDTO manufacturerCountryDTO) throws DuplicateException{
    	ManufacturerCountry existing = manufacturerCountryManager.findByNameAndCountry(manufacturerCountryDTO.getName(), manufacturerCountryDTO.getCountryId());
    	if(existing != null && !existing.getManufacturerId().equals(manufacturerCountryDTO.getManufacturerId())){
    		throw new DuplicateException();
    	}
    	ManufacturerCountryDTO result = manufacturerManager.save(manufacturerCountryDTO);
    	clearCache(CacheType.MANUFACTURER);
    	return result;
	}

	public void deleteManufacturerCountryDTO(ManufacturerCountryDTO manufacturerCountryDTO) {
		manufacturerManager.delete(manufacturerCountryDTO);
		clearCache(CacheType.MANUFACTURER);
	}
	
	public List<CustomerDTO> getAllCustomerDTOs() {
		return customerManager.getAllCustomerDTOs();
	}

	public List<CustomerTypeDTO> getCustomerTypeDTOs() {
		return customerManager.getAllCustomerTypeDTOs();
	}

	public CustomerDTO saveCustomerDTO(CustomerDTO customerDTO) throws DuplicateException{
		System.out.println("AppServicePOJO:saveCustomerDTO starts");
		CustomerCountry existing = customerCountryManager.findByNameAndCountry(customerDTO.getCustomerName(), customerDTO.getCustomerCountryId());
    	if(existing != null && !existing.getCustomerId().equals(customerDTO.getCustomerId())){
    		throw new DuplicateException();
    	}		
    	CustomerDTO result = customerManager.save(customerDTO);
    	clearCache(CacheType.CUSTOMER);
    	return result;
	}

	public void deleteCustomerDTO(CustomerDTO customerDTO) {
		customerManager.delete(customerDTO);
		clearCache(CacheType.CUSTOMER);
	}

	@Override
	public List<LocationCountryDTO> getAllLocationCountryDTOs() {
		return ConverterFactory.convertLocationCountryList(locationCountryManager.getAllLocationsCountry());
	}

	@Override
	public LocationCountryDTO saveLocationCountryDTO(LocationCountryDTO locationCountryDTO) {
		LocationCountryDTO result = locationManager.save(locationCountryDTO);
    	clearCache(CacheType.LOCATION);
    	return result;
	}
	
	@Override
	public void deleteLocationCountryDTO(LocationCountryDTO model) {
		locationManager.delete(model);
		clearCache(CacheType.LOCATION);
	}

	@Override
	public List<StructureDTO> getAllStructureDTOs() {
		return ConverterFactory.convertStructures(structureManager.getAllStructures());
	}

	@Override
	public StructureDTO saveStructureDTO(StructureDTO dto) {
		StructureDTO result = structureManager.save(dto);
    	clearCache(CacheType.STRUCTURE);
    	return result;
	}
	
	@Override
	public void deleteStructureDTO(StructureDTO model) {
		structureManager.delete(model);
		clearCache(CacheType.STRUCTURE);
	}

	@Override
	public List<DisplaySalesRepDTO> getAllDisplaySalesRepDTOs() {
		return ConverterFactory.convertDisplaySalesReps(displaySalesRepManager.getAllDisplaySalesReps(), true);
	}

	@Override
	public DisplaySalesRepDTO saveDisplaySalesRepDTO(DisplaySalesRepDTO dto) {
		DisplaySalesRepDTO result = displaySalesRepManager.save(dto);
    	clearCache(CacheType.USER);
    	return result;
	}
	
	@Override
	public void deleteDisplaySalesRepDTO(DisplaySalesRepDTO model) {
		displaySalesRepManager.delete(model);
		clearCache(CacheType.USER);
	}

	@Override
	public List<UserTypeDTO> getUserTypeDTOs() {
		return userManager.getAllUserTypeDTOs();
	}
	
	private void clearCache(CacheType cacheType) {
		switch (cacheType) {
		case MANUFACTURER:
			manufacturerCountryDTOs = null;
			manufacturerCountryDTOMap.clear();
			manufacturerDTOs = null;
			manufacturerDTOMap.clear();
			break;
		case CUSTOMER:
			customerCountryDTOs = null;
			customerCountryMap = null;
			customerDTOs = null;
			customerMap = null;
			break;
		case LOCATION:
			locationCountryDTOs = null;
			locationCountryDTOMap.clear();
			locationCountryMap = null;
			locationDTOs = null;
			locationMap = null;
			locationDTOMap.clear();
			break;
		case STRUCTURE:
			structureDTOs = null;
			break;
		case USER:
			displaySalesRepDTOs = null;
			approverDTOs = null;
			break;
		}
	}

	public List<DisplaySalesRepDTO> getApproverDTOs() {
		if (approverDTOs == null) {
			loadApproverDTOs();
		}
		return approverDTOs;
	}

	@Override
	public void saveProgramUploadBusForDisplays(List<DisplayDTO> displayDTOs ) {
		List<ProgramBuDTO> programList=null;
		for(DisplayDTO objDisplayDTO:displayDTOs ){
			programList= ConverterFactory.convertProgramBus(programBuManager.findByDisplayId(objDisplayDTO.getDisplayId()));
			saveProgramUploadedBus(programList,objDisplayDTO );
		}
	}
	
	@Override
	public List<ProgramBuDTO> loadProgramBus(Long displayId) {
		return ConverterFactory.convertProgramBus(programBuManager.findByDisplayId(displayId));
	}

	@Override
	public List<ProgramBuDTO> saveProgramBus(List<ProgramBuDTO> programList) {
		List<ProgramBuDTO> result = new ArrayList<ProgramBuDTO>();
		for(ProgramBuDTO program: programList){
			result.add(programBuManager.save(program));
		}
		return result;
	}

	/*
	 * Code change by Mari
	 * Date: 10092012
	 * Populate Program_% for the valid bu based on the customer id.
	 */
	@Override
	public List<ProgramBuDTO> saveProgramUploadedBus(List<ProgramBuDTO> programList, DisplayDTO displayDTO) {
		System.out.println("AppServicePOJO:saveProgramUploadedBus starts");
		List<CstBuPrgmPct> lstCstBuPrgmPct = cstBuPrgmPctManager.findCstBuPrgmPctByCustomerId(displayDTO.getCustomerId());
		List<ProgramBuDTO> result = new ArrayList<ProgramBuDTO>();
		for(ProgramBuDTO program: programList){
			for(CstBuPrgmPct  objCstBuPrgmPct:lstCstBuPrgmPct){
				//System.out.println("AppServicePOJO:saveProgramUploadedBus objCstBuPrgmPct.getBu().getBuCode()="+objCstBuPrgmPct.getBu().getBuCode());
				//System.out.println("AppServicePOJO:saveProgramUploadedBus program.getBuCode()="+program.getBuCode());
				if(objCstBuPrgmPct.getBu().getBuCode().equalsIgnoreCase(program.getBuCode())){
					//System.out.println("AppServicePOJO:saveProgramUploadedBus objCstBuPrgmPct.getProgramPct()="+objCstBuPrgmPct.getProgramPct());
					if(objCstBuPrgmPct.getProgramPct()!=null){
						program.setProgramPct(objCstBuPrgmPct.getProgramPct());
						//System.out.println("AppServicePOJO:saveProgramUploadedBus objCstBuPrgmPct.getProgramPct()="+objCstBuPrgmPct.getProgramPct());
					}
					break;
				}
			}
			result.add(programBuManager.save(program));
		}
		System.out.println("AppServicePOJO:saveProgramUploadedBus ends");
		return result;
	}
	/*
	 * End
	 */
	@Override
	public List<CstBuPrgmPctDTO> loadCstBuPrgmPcts(Long customerId) {
		
		return ConverterFactory.convertCstBuPrgmPcts(cstBuPrgmPctManager.findCstBuPrgmPctByCustomerId(customerId));
	}
	
}