package com.averydennison.dmm.app.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;



public class FileUploadServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		Properties props = new Properties();
		try {
		InputStream inputStream = this.getClass().getClassLoader()
				.getResourceAsStream("DmmConstants.properties");
			props.load(inputStream);
		} catch (IOException e) {
			System.out.println("FileUploiadWindow property file reading error" + e.toString() );
		}
		String rootDirectory = props
				.getProperty("imageFolder");
		File rootFolder = new File(rootDirectory);
		if(!rootFolder.exists()){
			System.out.println("Image Folder Created " + rootDirectory);
			rootFolder.mkdirs();
		}else{
			System.out.println("Image Folder already exists " + rootDirectory);
		}
		System.out.println("FileUploadServlet starts ");
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (isMultipart) {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			try {
				List items = upload.parseRequest(request);
				Iterator iter = items.iterator();
				System.out.println("FileUploadServlet 1 ");
				System.out.println("<<<FileUploadServlet itemSize : >>>" + items.size());
				String fileName="";
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();
					System.out.println("FileUploadServlet 2 ");

					if (item.isFormField()) {
					} else {
						fileName = item.getName();
						if (fileName != null && !fileName.equals("")) {
							fileName = FilenameUtils.getName(fileName);
							System.out.println("FileUploadServlet 3 " + rootDirectory + fileName);
							File uploadedFile = new File(rootDirectory + fileName);
							 try
							    {
							    InputStream uploadedStream = item.getInputStream();
							    OutputStream out=new FileOutputStream(uploadedFile);
							    byte buf[]=new byte[1024];
							    int len;
							    while((len=uploadedStream.read(buf))>0)
							    out.write(buf,0,len);
							    out.close();
							    uploadedStream.close();
							    System.out.println("FileUploadServlet 4 rootDirectory + fileName :" +  rootDirectory + fileName);
							    }
							    catch (IOException e){
							    	e.printStackTrace();
									System.out.println("FileUploadServlet starts error--2-- "+ e.toString());
							    }
							}
					}
				}
				response.flushBuffer();
				response.getWriter().flush();
				response.getWriter().write(rootDirectory + fileName);
			} catch (FileUploadException e) {
				System.out.println("FileUploadServlet starts error--1-- "+ e.toString());

				e.printStackTrace();
			}
			System.out.println("FileUploadServlet  ended ");
		}
		System.out.println("FileUploadServlet ends test");

	}
}
