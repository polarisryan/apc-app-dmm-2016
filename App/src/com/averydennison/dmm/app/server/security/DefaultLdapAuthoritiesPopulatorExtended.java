package com.averydennison.dmm.app.server.security;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.naming.directory.SearchControls;

import org.apache.log4j.Logger;
import org.springframework.ldap.core.ContextSource;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.ldap.SpringSecurityLdapTemplate;
import org.springframework.security.ldap.populator.DefaultLdapAuthoritiesPopulator;

public class DefaultLdapAuthoritiesPopulatorExtended extends DefaultLdapAuthoritiesPopulator{
	private static Logger logger = Logger.getLogger(DefaultLdapAuthoritiesPopulatorExtended.class);
	
	private SpringSecurityLdapTemplate ldapTemplate;
	private String rolePrefix = "ROLE_";

	public void setRolePrefix(String rolePrefix) {
		this.rolePrefix = rolePrefix;
	}

	public DefaultLdapAuthoritiesPopulatorExtended(ContextSource contextSource,
			String groupSearchBase) {
		super(contextSource, groupSearchBase);
		ldapTemplate = new SpringSecurityLdapTemplate(contextSource);
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		ldapTemplate.setSearchControls(searchControls);
	}
	
	@SuppressWarnings("unchecked")
	private Set getRolesCn(String groupDn, String attr){

		Set auth = new HashSet();
		if (logger.isDebugEnabled()) {
            logger.debug("Searching for roles for DN = " + "'" + groupDn + "' in search base '" + getGroupSearchBase() + "'");
        }
		Set<String> roles = ldapTemplate.searchForSingleAttributeValues(getGroupSearchBase(), "(member={0})",
					new String[]{groupDn}, attr);
		if (logger.isDebugEnabled()) {
            logger.debug("Roles from search: " + roles);
        }
			Iterator ur = roles.iterator();
			while(ur.hasNext()){
				String s = (String)ur.next();
				s = s.toUpperCase();
				auth.add(new GrantedAuthorityImpl(rolePrefix + s));
			}
		roles = null;
		return auth;
	}
	

	@SuppressWarnings("unchecked")
	private Set getRecursiveNestedDn(String groupDn){
		
		if (logger.isDebugEnabled()) {
            logger.debug("Getting nested authorities for group " + groupDn);
        }
		Set<String> recRoles = new HashSet();
		Set<String> roles = ldapTemplate.searchForSingleAttributeValues(getGroupSearchBase(), "(member={0})",
				new String[]{groupDn}, "distinguishedName");
		recRoles.addAll(roles);
		Iterator<String> ur = roles.iterator();
		while(ur.hasNext()){
			String s = (String)ur.next();
			getRecursiveNestedDn(s);
		}
		roles = null;
		return recRoles;
	}
	
	@SuppressWarnings("unchecked")
	public Set getAdditionalRoles(org.springframework.ldap.core.DirContextOperations user, String username){
		
		String userDn = user.getNameInNamespace();
		System.out.println(userDn);
		
		Set<GrantedAuthority> roles = getGroupMembershipRoles(userDn, username);
		Set<String> userRoles = getRecursiveNestedDn(userDn);

		Iterator ur = userRoles.iterator();
		while(ur.hasNext()){
			String s = (String)ur.next();
			Set list = getRolesCn(s, "cn");
			roles.addAll(list);
		}
		GrantedAuthority[] arr = (GrantedAuthority[])roles.toArray(new GrantedAuthority[roles.size()]);
		List list = Arrays.asList(arr);
		Set<GrantedAuthority> newRoles = new HashSet(list);
		list = null;
		userRoles = null;
		return newRoles;
	}

}
