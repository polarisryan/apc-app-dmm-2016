package com.averydennison.dmm.app.client;


import java.util.Date;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayLogisticsDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.CheckBoxGroup;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Element;
/**
 * User: Spart Arguello
 * Date: Oct 26, 2009
 * Time: 9:13:27 AM
 */
public class DisplayLogisticsInfo extends LayoutContainer implements DirtyInterface{
    private FormData formData;
    private VerticalPanel vp;
    public static final String SAVE = "Save";
    public static final String RETURN_TO_SELECTION = "Return to Selection";
    private DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
    private DisplayDTO displayModel;
    private DisplayLogisticsDTO displayLogisticsModel;
    private ChangeListener displayLogisticsListener;
    private ChangeListener displayModelListener;
    private final XNumberField length = new XNumberField();
    private final XNumberField width = new XNumberField();
    private final XNumberField height = new XNumberField();
    private final XNumberField weight = new XNumberField();
    private final TextField<String> placardSpecialLabelRqt = new TextField<String>();
    private final CheckBox serialShip = new CheckBox();
    private final NumberField displayPerLayer = new NumberField();
    private final NumberField layersPerPallet = new NumberField();
    private final NumberField displaysPerPallet = new NumberField();
    private final NumberField numberOfTotPallets = new NumberField();
    private final NumberField numberOfPalletsPerTrailer = new NumberField();
    private final NumberField numberOfTrailers = new NumberField();
    private final CheckBox doubleStack = new CheckBox();
    private DisplayTabs displayTabs;
   
    
    private DisplayLogisticsDTO getControlModel(){
        DisplayLogisticsDTO controlModel = new DisplayLogisticsDTO();
        if(height.getValue()!=null) controlModel.setDisplayHeight(height.getValue().floatValue());
        if(length.getValue()!=null) controlModel.setDisplayLength(length.getValue().floatValue());
        if(weight.getValue()!=null) controlModel.setDisplayWeight(weight.getValue().floatValue());
        if(width.getValue()!=null) controlModel.setDisplayWidth(width.getValue().floatValue());
        if(displayPerLayer.getValue()!=null) controlModel.setDisplayPerLayer(Long.valueOf(displayPerLayer.getValue().longValue()));
        if(displayLogisticsModel.isDoubleStackFlg()!=null || doubleStack.isDirty()) controlModel.setDoubleStackFlg(doubleStack.getValue());
        if(layersPerPallet.getValue()!=null) controlModel.setLayersPerPallet(Long.valueOf(layersPerPallet.getValue().longValue()));
        if(numberOfPalletsPerTrailer.getValue()!=null) controlModel.setPalletsPerTrailer(Long.valueOf(numberOfPalletsPerTrailer.getValue().longValue()));
        if(displayLogisticsModel.isSerialShipFlg()!=null || serialShip.isDirty()) controlModel.setSerialShipFlg(serialShip.getValue());
        if(placardSpecialLabelRqt.getValue()!=null) controlModel.setSpecialLabetRqt(placardSpecialLabelRqt.getValue());
        return controlModel;
    }
    
    private boolean overrideDirty = false;
    public void overrideDirty(){
        this.overrideDirty = true;
    }
    
    public boolean isDirty(){
        if(overrideDirty==true) return false;        
    	return !different();
    }
    
    private boolean different(){
        return this.displayLogisticsModel.equalsDisplayLogisticsInfo(getControlModel());
    }

    public DisplayLogisticsInfo() {
        formData = new FormData("95%");
        vp = new VerticalPanel();
        vp.setSpacing(10);
    }
    
    private void setPermissions(){
    	if(!App.isAuthorized("DisplayLogisticsInfo.SAVE")) btnSave.disable();
    }

    private SelectionListener listener = new SelectionListener<ComponentEvent>() {
        public void componentSelected(ComponentEvent ce) {
            Button btn = (Button) ce.getComponent();
            if (btn.getText().equals(SAVE)) {
                save(btn, null, null);
            } else if (btn.getText().equals(RETURN_TO_SELECTION)) {
                if(isDirty()){
                    String title = "You have unsaved information!";
                    String msg = "Would you like to save?";                             
                    MessageBox box = new MessageBox();
                    box.setButtons(MessageBox.YESNOCANCEL);
                    box.setIcon(MessageBox.QUESTION);
                    box.setTitle("Save Changes?");
                    box.addCallback(l);
                    box.setTitle(title);
                    box.setMessage(msg);
                    box.show();
                }else{
                    forwardToDisplayProjectSelection();
                }
            }
        }
    };
    
    private final Listener<MessageBoxEvent> l = new Listener<MessageBoxEvent>() {
        public void handleEvent(MessageBoxEvent mbe) {
            Button btn = mbe.getButtonClicked();
            if (btn.getText().equals("Yes")) {
                save(btn, null, null);
                forwardToDisplayProjectSelection();
            }
            if (btn.getText().equals("No")) {
                forwardToDisplayProjectSelection();
            }
            if (btn.getText().equals("Cancel")) {
            }
        }
    };
    private void forwardToDisplayProjectSelection(){
        clearModel();
        App.getVp().clear();
        App.getVp().add(new DisplayProjectSelection());
    }
    public void save(Button btn, TabPanel panel, TabItem tabItem){
    	if (displayModel.getDisplayId() == null) {
   		 	MessageBox.alert("Validation Error", "There is no display defined " , null);
   		 	return;
    	}
        if (!length.validate()) return;
        if (!width.validate()) return;
        if (!height.validate()) return;
        if (!weight.validate()) return;
        if (!placardSpecialLabelRqt.validate()) return;
        if (!serialShip.validate()) return;
        if (!displayPerLayer.validate()) return;
        if (!layersPerPallet.validate()) return;
        if (!displaysPerPallet.validate()) return;
        if (!numberOfTotPallets.validate()) return;
        if (!numberOfPalletsPerTrailer.validate()) return;
        if (!numberOfTrailers.validate()) return;
        if (!doubleStack.validate()) return;
        if (displayLogisticsModel != null && isDirty()) {
            displayLogisticsModel.setDisplayId(displayModel.getDisplayId());
            displayLogisticsModel.setDisplayHeight((Float) height.getValue());
            displayLogisticsModel.setDisplayLength((Float) length.getValue());
            displayLogisticsModel.setDisplayWidth((Float) width.getValue());
            displayLogisticsModel.setDisplayWeight((Float) weight.getValue());
            displayLogisticsModel.setDisplayPerLayer((Long) displayPerLayer.getValue());
            displayLogisticsModel.setLayersPerPallet((Long) layersPerPallet.getValue());
            displayLogisticsModel.setPalletsPerTrailer((Long) numberOfPalletsPerTrailer.getValue());
            displayLogisticsModel.setDoubleStackFlg(doubleStack.getValue());
            displayLogisticsModel.setSpecialLabetRqt(placardSpecialLabelRqt.getValue());
            displayLogisticsModel.setSerialShipFlg(serialShip.getValue());
            displayLogisticsModel.setUserLastChanged(App.getUser().getUsername());
            displayLogisticsModel.setDtLastChanged(new Date());
            if (displayLogisticsModel.getDisplayLogisticsId() == null) {
                displayLogisticsModel.setDtCreated(new Date());
                displayLogisticsModel.setUserCreated(App.getUser().getUsername());
            }
            final MessageBox box = MessageBox.wait("Progress",
                    "Saving display logistics info, please wait...", "Saving...");
            AppService.App.getInstance().saveDisplayLogisticsInfoDTO(displayLogisticsModel, displayModel,
                    new Service.SaveDisplayLogisticsInfoDTO(getDisplayLogisticsInfo(), displayLogisticsModel,
                            btnSave, box));
        }
    }
    
    private DisplayLogisticsInfo getDisplayLogisticsInfo() {
        return this;
    }

    private void clearModel() {
        if (displayModel != null) displayModel.removeChangeListener(displayModelListener);
        if (displayLogisticsModel != null) displayLogisticsModel.removeChangeListener(displayLogisticsListener);
        displayModel = null;
        displayLogisticsModel = null;
        overrideDirty = false;
        displayTabs.resetDisplayModel();
        setData("display", null);
    }


    private final Button btnSave = new Button(SAVE, listener);

    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        createHeader();
        createForm();
        setModelsAndView();
        add(vp);
    }

    private void setModelsAndView() {
        this.displayModel = displayTabs.getDisplayModel();
        if (displayModel == null) {
            displayModel = new DisplayDTO();
        }
        if (displayModel.getDisplayId() != null) {
            displayTabHeader.setDisplay(displayModel);
        }
        displayLogisticsModel = this.getData("displayLogistics");
        if (displayLogisticsModel == null) {
            displayLogisticsModel = new DisplayLogisticsDTO();
        }
        updateView();
        setPermissions();
        setReadOnly();
        displayLogisticsListener = new ChangeListener() {
            public void modelChanged(com.extjs.gxt.ui.client.data.ChangeEvent event) {
                displayLogisticsModel = (DisplayLogisticsDTO) event.getSource();
                if (displayLogisticsModel != null && displayLogisticsModel.getDisplayLogisticsId() != null) {
                    updateView();
                }
            }
        };
        displayModelListener = new ChangeListener() {
			public void modelChanged(ChangeEvent event) {
				displayModel = (DisplayDTO) event.getSource();
				if (displayModel != null && displayModel.getDisplayId() != null) {
					displayTabs.setDisplayModel(displayModel);
					displayTabs.setData("display", displayModel);
			        if (displayModel != null && displayModel.getDisplayId() != null) {
			            displayTabHeader.setDisplay(displayModel);
			        }
				}
			}
		};
	    displayModel.addChangeListener(displayModelListener);
        displayLogisticsModel.addChangeListener(displayLogisticsListener);
    }

    private void setReadOnly() {
        Boolean b = this.getData("readOnly");
        if (b == null) return;
        length.setReadOnly(b);
        width.setReadOnly(b);
        height.setReadOnly(b);
        weight.setReadOnly(b);
        placardSpecialLabelRqt.setReadOnly(b);
        serialShip.setReadOnly(b);
        displayPerLayer.setReadOnly(b);
        layersPerPallet.setReadOnly(b);
        displaysPerPallet.setReadOnly(true);
        numberOfTotPallets.setReadOnly(true);
        numberOfPalletsPerTrailer.setReadOnly(b);
        numberOfTrailers.setReadOnly(true);
        doubleStack.setReadOnly(b);
        if (b) btnSave.disable();
    }

    final NumberFormat logisticNumberFmt = NumberFormat.getFormat("####.###");
    private void updateView() {
    	this.displayModel=displayTabs.getDisplayModel();
    	if (displayModel != null && this.displayModel.getDisplayId() != null) {
            this.displayTabHeader.setDisplay(this.displayModel);
        }
        if(displayLogisticsModel.getDisplayLength()!=null){
        	length.setFormat(logisticNumberFmt);
        	length.setValue(displayLogisticsModel.getDisplayLength());
            displayLogisticsModel.setDisplayLength(length.getValue().floatValue());
        }
        if(displayLogisticsModel.getDisplayWidth()!=null){
        	width.setFormat(logisticNumberFmt);
        	width.setValue(displayLogisticsModel.getDisplayWidth());
            displayLogisticsModel.setDisplayWidth(width.getValue().floatValue());
        }
        if(displayLogisticsModel.getDisplayHeight()!=null){
        	height.setFormat(logisticNumberFmt);
        	height.setValue(displayLogisticsModel.getDisplayHeight());
        	displayLogisticsModel.setDisplayHeight(height.getValue().floatValue());
        }
        if(displayLogisticsModel.getDisplayWeight()!=null){
        	weight.setFormat(logisticNumberFmt);
        	weight.setValue(displayLogisticsModel.getDisplayWeight());
            displayLogisticsModel.setDisplayWeight(weight.getValue().floatValue());
        }
        placardSpecialLabelRqt.setValue(displayLogisticsModel.getSpecialLabetRqt());
        if(displayLogisticsModel.isSerialShipFlg()!=null){
            serialShip.setValue(displayLogisticsModel.isSerialShipFlg());
            serialShip.setOriginalValue(displayLogisticsModel.isSerialShipFlg());
        }
        displayPerLayer.setValue(displayLogisticsModel.getDisplayPerLayer());
        layersPerPallet.setValue(displayLogisticsModel.getLayersPerPallet());
        numberOfPalletsPerTrailer.setValue(displayLogisticsModel.getPalletsPerTrailer());
        if(displayLogisticsModel.isDoubleStackFlg()!=null){
            doubleStack.setValue(displayLogisticsModel.isDoubleStackFlg());
            doubleStack.setOriginalValue(displayLogisticsModel.isDoubleStackFlg());
        }
        updateCalculatedValues();
        Boolean b = this.getData("readOnly");
        if (b == null) return;
        if (b) btnSave.disable();
    }

    private void updateCalculatedValues() {
        Long totPallets = 0L;
        Long numTrailers = 0L;
        Long dispPerPallet = 0L;
        Long dispPerLayer = (Long) displayPerLayer.getValue();
        Long layerPerPallet = (Long) layersPerPallet.getValue();
        Long palletsPerTrailer = (Long) numberOfPalletsPerTrailer.getValue();
        if (layerPerPallet != null && dispPerLayer != null)
            dispPerPallet = layerPerPallet * dispPerLayer;
        if (dispPerPallet > 0 && displayModel.getQtyForecast() != null) {
            double d = new Double(displayModel.getQtyForecast()).doubleValue() / new Double(dispPerPallet).doubleValue();
            totPallets = new Double(d % 1 > 0 ? d + 1 : d).longValue();
        }
        if (palletsPerTrailer != null && palletsPerTrailer > 0) {
            double d = new Double(totPallets).doubleValue() / new Double(palletsPerTrailer).doubleValue();
            numTrailers = new Double(d % 1 > 0 ? d + 1 : d).longValue();
        }
        displaysPerPallet.setReadOnly(true);
        numberOfTotPallets.setReadOnly(true);
        numberOfTrailers.setReadOnly(true);
        displaysPerPallet.setEnabled(false);
        numberOfTotPallets.setEnabled(false);
        numberOfTrailers.setEnabled(false);
        displaysPerPallet.setValue(dispPerPallet);
        numberOfTotPallets.setValue(totPallets);
        numberOfTrailers.setValue(numTrailers);
    }

    private void createHeader() {
        add(displayTabHeader);
    }

   
    private void createForm() {
        FormPanel formPanel = new FormPanel();
        formPanel.setHeaderVisible(false);
        formPanel.setFrame(true);
        formPanel.setSize(1190, -1);
        formPanel.setLabelAlign(FormPanel.LabelAlign.TOP);
        formPanel.setButtonAlign(Style.HorizontalAlignment.CENTER);
        LayoutContainer main = new LayoutContainer();
        main.setLayout(new ColumnLayout());
        LayoutContainer left = new LayoutContainer();
        LayoutContainer center = new LayoutContainer();
        LayoutContainer right = new LayoutContainer();
        FormLayout leftLayout = new FormLayout();
        left.setStyleAttribute("paddingRight", "10px");
        leftLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        FormLayout centerLayout = new FormLayout();
        left.setStyleAttribute("paddingRight", "10px");
        centerLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        FormLayout rightLayout = new FormLayout();
        rightLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        left.setStyleAttribute("paddingRight", "10px");
        left.setLayout(leftLayout);
        center.setStyleAttribute("paddingRight", "10px");
        center.setLayout(centerLayout);
        right.setStyleAttribute("paddingLeft", "10px");
        right.setLayout(rightLayout);
        FieldSet displaySpecifications = new FieldSet();
        FormLayout displaySpecificationsFormLayout = new FormLayout();
        displaySpecificationsFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        displaySpecifications.setLayout(displaySpecificationsFormLayout);
        displaySpecifications.setHeading("Display Dimensions");
        displaySpecifications.setCollapsible(false);
        left.add(displaySpecifications);
        FieldSet displayLogistics = new FieldSet();
        FormLayout displayLogisticssFormLayout = new FormLayout();
        displayLogisticssFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        displayLogistics.setLayout(displayLogisticssFormLayout);
        displayLogistics.setHeading("Pallet Configuration");
        displayLogistics.setCollapsible(false);
        center.add(displayLogistics);
        length.setFieldLabel("Length (in)");
        length.setPropertyEditorType(Float.class);
        length.setAllowBlank(true);
        length.setMaxLength(8);
        length.setAllowDecimals(true);
        length.setDecimalPrecision(3);
        length.setWholeNumberPrecision(3);
        displaySpecifications.add(length, formData);
        width.setFieldLabel("Width (in)");
        width.setPropertyEditorType(Float.class);
        width.setAllowBlank(true);
        width.setMaxLength(8);
        width.setAllowDecimals(true);
        width.setDecimalPrecision(3);
        width.setWholeNumberPrecision(3);
        displaySpecifications.add(width, formData);
        height.setFieldLabel("Height (in)");
        height.setPropertyEditorType(Float.class);
        height.setAllowBlank(true);
        height.setMaxLength(8);
        height.setAllowDecimals(true);
        height.setDecimalPrecision(3);
        height.setWholeNumberPrecision(3);
        displaySpecifications.add(height, formData);
        weight.setFieldLabel("Weight (lbs)");
        weight.setPropertyEditorType(Float.class);
        weight.setAllowBlank(true);
        weight.setMaxLength(8);
        weight.setAllowDecimals(true);
        weight.setDecimalPrecision(3);
        weight.setWholeNumberPrecision(3);
        displaySpecifications.add(weight, formData);
        placardSpecialLabelRqt.setFieldLabel("Placard/Special Label Requirement");
        placardSpecialLabelRqt.setAllowBlank(true);
        placardSpecialLabelRqt.setMaxLength(25);
        displaySpecifications.add(placardSpecialLabelRqt, formData);
        CheckBoxGroup checkGroup1 = new CheckBoxGroup();
        checkGroup1.setLabelSeparator(" ");
        serialShip.setBoxLabel("Serial Ship");
        checkGroup1.add(serialShip);
        displaySpecifications.add(checkGroup1);
        displayPerLayer.setFieldLabel("Displays Per Layer");
        displayPerLayer.setPropertyEditorType(Long.class);
        displayPerLayer.setAllowBlank(true);
        displayPerLayer.setMaxLength(10);
        displayPerLayer.addListener(Events.KeyUp, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                if (displayPerLayer.getValue() != null) updateCalculatedValues();
            }
        });
        displayLogistics.add(displayPerLayer, formData);
        layersPerPallet.setFieldLabel("Layers Per Pallet");
        layersPerPallet.setPropertyEditorType(Long.class);
        layersPerPallet.setAllowBlank(true);
        layersPerPallet.setMaxLength(10);
        layersPerPallet.addListener(Events.KeyUp, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                if (layersPerPallet.getValue() != null) updateCalculatedValues();
            }
        });
        displayLogistics.add(layersPerPallet, formData);
        displaysPerPallet.setFieldLabel("Displays Per Pallet");
        displaysPerPallet.setPropertyEditorType(Long.class);
        displaysPerPallet.setAllowBlank(true);
        displayLogistics.add(displaysPerPallet, formData);
        numberOfTotPallets.setFieldLabel("# Tot Pallets");
        numberOfTotPallets.setPropertyEditorType(Long.class);
        numberOfTotPallets.setAllowBlank(true);
        displayLogistics.add(numberOfTotPallets, formData);
        numberOfPalletsPerTrailer.setFieldLabel("# Pallets/Trailer");
        numberOfPalletsPerTrailer.setPropertyEditorType(Long.class);
        numberOfPalletsPerTrailer.setAllowBlank(true);
        numberOfPalletsPerTrailer.setMaxLength(10);
        numberOfPalletsPerTrailer.addListener(Events.KeyUp, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                if (numberOfPalletsPerTrailer.getValue() != null) updateCalculatedValues();
            }
        });
        displayLogistics.add(numberOfPalletsPerTrailer, formData);
        numberOfTrailers.setFieldLabel("# Trailers");
        numberOfTrailers.setPropertyEditorType(Long.class);
        numberOfTrailers.setAllowBlank(true);
        displayLogistics.add(numberOfTrailers, formData);
        doubleStack.setBoxLabel("Double Stack");
        CheckBoxGroup checkGroup = new CheckBoxGroup();
        checkGroup.setLabelSeparator(" ");
        checkGroup.add(doubleStack);
        displayLogistics.add(checkGroup);
        main.add(left, new ColumnData(.50));
        main.add(center, new ColumnData(.50));
        formPanel.add(main);
        formPanel.addButton(new Button(RETURN_TO_SELECTION, listener));
        formPanel.addButton(btnSave);
        vp.add(formPanel);
    }

    public void reload() {
        if (displayLogisticsModel == null) {
            displayLogisticsModel = new DisplayLogisticsDTO();
        }
        if (displayModel != null && displayModel.getDisplayId() != null) {
            AppService.App.getInstance().getDisplayDTO(displayModel, new Service.ReloadDisplay(displayModel));
            updateHeaderView();
        }
        if (displayModel.getDisplayId() != null && displayModel.getDisplayId() > 0) {
            AppService.App.getInstance().getDisplayLogisticsInfoDTO(displayModel.getDisplayId(),
                    new Service.ReloadDisplayLogistics(displayLogisticsModel));
            updateView();
        }
        overrideDirty = false;
    }

    private void updateHeaderView() {
        displayTabHeader.setDisplay(displayModel);
    }

    public DisplayTabs getDisplayTabs() {
        return displayTabs;
    }

    public void setDisplayTabs(DisplayTabs displayTabs) {
        this.displayTabs = displayTabs;
    }
}
