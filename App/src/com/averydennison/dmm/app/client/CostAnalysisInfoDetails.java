package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;

public class CostAnalysisInfoDetails extends LayoutContainer {
	    private FormData formData;
	    private final ComboBox<StatusDTO> status = new ComboBox<StatusDTO>();
	    private final ComboBox<DisplaySalesRepDTO> projectManager = new ComboBox<DisplaySalesRepDTO>();
	    private final ComboBox<ManufacturerCountryDTO> manufacturer = new ComboBox<ManufacturerCountryDTO>();
	    private final ComboBox<LocationCountryDTO> location = new ComboBox<LocationCountryDTO>();



	  public CostAnalysisInfoDetails() {
	        setLayout(new FlowLayout(10));
	        formData = new FormData("95%");
	        
	        LayoutContainer left = new LayoutContainer();
	        LayoutContainer center = new LayoutContainer();
	        LayoutContainer right = new LayoutContainer();
	        
	        FormLayout leftLayout = new FormLayout();
	        left.setStyleAttribute("paddingRight", "5px");
	        leftLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
	        left.setLayout(leftLayout);
	        
	        FormLayout centerLayout = new FormLayout();
	        center.setStyleAttribute("paddingRight", "5px");
	        centerLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
	        center.setLayout(centerLayout);
	        
	        FormLayout rightLayout = new FormLayout();
	        right.setStyleAttribute("paddingRight", "5px");
	        rightLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
	        right.setLayout(rightLayout);
	        
	        
	        FieldSet firstColumnData = new FieldSet();
	        FormLayout firstColumnDataFormLayout = new FormLayout();
	        firstColumnDataFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
	        firstColumnData.setLayout(firstColumnDataFormLayout);
	        left.add(firstColumnData);//, formData);
	        
	        DateField infoDetailsDate = new DateField();
	        infoDetailsDate.setFieldLabel("Date");
	        infoDetailsDate.setWidth(75);
	        infoDetailsDate.setFieldLabel("infoDetailsDate");
	        infoDetailsDate.setAllowBlank(true);
	        firstColumnData.add(infoDetailsDate , formData);

	        
	        NumberField infoDetailsYear = new NumberField();
	        infoDetailsYear.setFieldLabel("Year");
	        infoDetailsYear.setAllowBlank(true);
	        infoDetailsYear.setReadOnly(false);
	        infoDetailsYear.setMinLength(4);
	        infoDetailsYear.setMaxLength(4);
	        firstColumnData.add(infoDetailsYear, formData);
	        
	        
	        NumberField infoDetailsProjectNo = new NumberField();
	        infoDetailsProjectNo.setFieldLabel("CM Project #");
	        infoDetailsProjectNo.setAllowBlank(true);
	        infoDetailsProjectNo.setReadOnly(false);
	        infoDetailsProjectNo.setMinLength(4);
	        infoDetailsProjectNo.setMaxLength(4);
	        firstColumnData.add(infoDetailsProjectNo, formData);
	        
	        
	        FieldSet secondColumnData = new FieldSet();
	        FormLayout secondColumnDataFormLayout = new FormLayout();
	        secondColumnDataFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
	        secondColumnData.setLayout(secondColumnDataFormLayout);
	        center.add(secondColumnData);
	   
	        Service service = new Service();
	        ListStore<StatusDTO> statuses = new ListStore<StatusDTO>();
	        service.setStatusStore(statuses);
	        
	        ListStore<CustomerCountryDTO> customers = new ListStore<CustomerCountryDTO>();
	        service.setCustomerCountryStore(null, customers);
	        
	        
	        ListStore<DisplaySalesRepDTO> displaySalesReps = new ListStore<DisplaySalesRepDTO>();
	        AppService.App.getInstance().getDisplaySalesRepDTOs(new Service.SetProjectManagerStore(displaySalesReps));
	        
	        ListStore<ManufacturerCountryDTO> manufacturers = new ListStore<ManufacturerCountryDTO>();
	        service.setManufacturerCountryStore(null, manufacturers);
	        
	        ListStore<LocationCountryDTO> locations = new ListStore<LocationCountryDTO>();
	        service.setLocationCountryStore(null,locations);
	        
	        manufacturer.setFieldLabel("Corrugate Vendor");
	        manufacturer.setDisplayField(ManufacturerCountryDTO.NAME);
	        manufacturer.setStore(manufacturers);
	        manufacturer.setWidth(100);
	        manufacturer.setAllowBlank(true);
	        manufacturer.setEditable(false);
	        manufacturer.setTriggerAction(ComboBox.TriggerAction.ALL);
	        secondColumnData.add(manufacturer, formData);
	        
	        
	        location.setFieldLabel("Packout Vendor");
	        location.setDisplayField(ManufacturerCountryDTO.NAME);
	        location.setStore(locations);
	        location.setWidth(100);
	        location.setAllowBlank(true);
	        location.setEditable(false);
	        location.setTriggerAction(ComboBox.TriggerAction.ALL);
	        secondColumnData.add(location, formData);
	        
	        projectManager.setFieldLabel("Project Manager");
	        projectManager.setDisplayField(DisplaySalesRepDTO.FULL_NAME);
	        projectManager.setWidth(100);
	        projectManager.setStore(displaySalesReps);
	        projectManager.setAllowBlank(false);
	        projectManager.setEditable(false);
	        projectManager.setTriggerAction(ComboBox.TriggerAction.ALL);
	        secondColumnData.add(projectManager, formData);
	        
	       
	        FieldSet thirdColumnData = new FieldSet();
	        FormLayout thirdColumnDataFormLayout = new FormLayout();
	        thirdColumnDataFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
	        thirdColumnData.setLayout(thirdColumnDataFormLayout);
	        right.add(thirdColumnData);
	        
	        
	        status.setFieldLabel("Status");
	        status.setDisplayField(StatusDTO.STATUS_NAME);
	        status.setWidth(100);
	        status.setStore(statuses);
	        status.setAllowBlank(false);
	        status.setEditable(false);
	        status.setTriggerAction(ComboBox.TriggerAction.ALL);
	        
	        thirdColumnData.add(status, formData);
	        
	        
	        ContentPanel cp = new ContentPanel();
	        cp.setBodyBorder(false);
	        cp.setHeaderVisible(false);
	        cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
	        cp.setLayout(new FitLayout());
	        cp.setSize(635, 200);
	        LayoutContainer main = new LayoutContainer();
	        main.setLayout(new ColumnLayout());
	         main.add(left);
	         main.add(center);
         main.add(right);
	        cp.add(main)	;	
	        add(cp);
	  }
}
