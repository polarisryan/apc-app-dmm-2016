package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.models.SelectFieldToMassUpdateDTO;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Nov 2, 2009
 * Time: 9:11:01 AM
 */
public class DisplayProjectSelection extends LayoutContainer {
    
    private FilterPanel filterPanel = new FilterPanel();
    private SelectionGrid selectionGrid ;
    
    public DisplayProjectSelection() {
        //this.setSize(935, -1);cp.setSize(1210, 450);
    	this.setSize(1180, -1);
        selectionGrid = new SelectionGrid();
        selectionGrid.setSelectFieldToMassUpdate(new SelectFieldToMassUpdateDTO());
        this.filterPanel.setSelectionGrid(selectionGrid);
    }

    public DisplayProjectSelection(SelectionGrid selectionGrid) {
        //this.setSize(935, -1);
    	this.setSize(1180, -1);
        this.selectionGrid=selectionGrid;
        this.filterPanel.setSelectionGrid(selectionGrid);
    }
    
    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        buildHeader();
        buildBody();
    }

    private void buildHeader() {
        add(new HeaderPanel(HeaderPanel.PROJECTS));
        add(filterPanel);
    }

    private void buildBody() {
        add(selectionGrid);
    }
}
