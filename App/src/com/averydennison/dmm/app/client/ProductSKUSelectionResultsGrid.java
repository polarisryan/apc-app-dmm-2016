package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.averydennison.dmm.app.client.models.ProductDetailDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;

/**
 * User: Spart Arguello
 * Date: Oct 30, 2009
 * Time: 5:06:30 PM
 */
public class ProductSKUSelectionResultsGrid extends LayoutContainer {
    public static final String SELECT_SKU = "Select";
    public static final String CREATE_SKU = "Create SKU";
    public static final String CANCEL = "Cancel";

    private Window window;
    private Grid<ProductDetailDTO> grid;
    private ListStore<ProductDetailDTO> store = new ListStore<ProductDetailDTO>();
    private DisplayDTO displayDTO = new DisplayDTO();
    private DisplayScenarioDTO displayScenarioDTO = new DisplayScenarioDTO();
    private List<Long> kitComponentIds;
    private Long seqNum;
    private KitComponentInfoGrid kitGrid;
    private DisplayCostDataGroup displayCostDataGroup;

    
    public DisplayScenarioDTO getDisplayScenarioDTO() {
		return displayScenarioDTO;
	}


	public void setDisplayScenarioDTO(DisplayScenarioDTO displayScenarioDTO) {
		this.displayScenarioDTO = displayScenarioDTO;
	}


	public Window getWindow() {
		return window;
	}


	public void setWindow(Window window) {
		this.window = window;
	}


	public ProductSKUSelectionResultsGrid(KitComponentInfoGrid kitGrid, Window parent, DisplayDTO display, DisplayScenarioDTO displayScenarioDTO,  Long seq,DisplayCostDataGroup displayCostDataGroup) {
        this.window = parent;
        this.displayDTO = display;
        this.seqNum = seq;
        this.kitGrid=kitGrid;
        this.displayScenarioDTO=displayScenarioDTO;
        this.displayCostDataGroup=displayCostDataGroup;
       //System.out.println("ProductSKUSelectionResultsGrid:ProductSKUSelectionResultsGrid(..)" + getDisplayCostDataGroup().getDisplayScenarioDTO().getDisplayScenarioId());
        setLayout(new FlowLayout(0));
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        ColumnConfig column = new ColumnConfig();
        column.setId("sku");
        column.setHeader("SKU");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(150);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("ilsSkuDesc");
        column.setHeader("Product Description");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(440);
        configs.add(column);

        ColumnModel cm = new ColumnModel(configs);

        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setHeaderVisible(false);
        cp.setButtonAlign(Style.HorizontalAlignment.RIGHT);
        cp.setLayout(new FitLayout());
        cp.setSize(615, 300);

        grid = new Grid<ProductDetailDTO>(store, cm);
       // grid.setEnableColumnResize(true);
        grid.setStyleAttribute("borderTop", "none");
        grid.setStripeRows(true);
        grid.setBorders(true);
        grid.addListener(Events.RowDoubleClick, new Listener<GridEvent<ModelData>>() {
          	public void handleEvent(GridEvent<ModelData> be) {
                ProductDetailDTO pd = (ProductDetailDTO)grid.getSelectionModel().getSelectedItem();
                /*
				 * Quick fix: For ProductSKUSelection window becoming greyed when double click event
				 * Date     : 01-14-2010, 10.43 am
				 * By       : Mari  
				 */
          		/*grid.disable();*/
                /*
				 * Quick fix: For ProductSKUSelection window costanalysis screen selection displayDTO become NULL when dirty selection
				 * Date     : 01-20-2011, 11.40 am
				 * By       : Mari  
				 */
          	    if(getKitGrid()!=null){
                	displayDTO=getKitGrid().getDisplayTabs().getDisplayModel(); // DisplayDTO.displaySetupId will have new value if the display is modified
                }else{
                	displayDTO=getDisplayCostDataGroup().getDisplayTabs().getDisplayModel();
                }
                KitComponentDTO kitComponentDTO = new KitComponentDTO();
                kitComponentDTO.setSkuId(pd.getSkuId());
                kitComponentDTO.setSku(pd.getSku());
                kitComponentDTO.setSequenceNum(seqNum);
                kitComponentDTO.setProductName(pd.getIlsSkuDesc());
                kitComponentDTO.setBuDesc(pd.getBuDesc());
                kitComponentDTO.setRegCasePack(pd.getInnerpackQty());
                kitComponentDTO.setPromoFlg(pd.isPromoFlg());
                kitComponentDTO.setSourcePimFlg(true);
                kitComponentDTO.setVersionNo(pd.getVersionNo());
                KitComponentDetail kitComponentDetail=null;
                //System.out.println("ProductSKUSelectionResultsGrid:SELECT_SKU getDisplayScenarioDTO().id 1..="+ getDisplayScenarioDTO().getDisplayScenarioId());
                //System.out.println("ProductSKUSelectionResultsGrid:SELECT_SKU getDisplayCostDataGroup().getDisplayScenarioDTO().id 2..="+ getDisplayCostDataGroup().getDisplayScenarioDTO().getDisplayScenarioId());
                //System.out.println("ProductSKUSelectionResultsGrid:SELECT_SKU getDisplayCostDataGroup().getDisplayScenarioDTO().id 3..="+ getDisplayCostDataGroup().getCostAnalysisInfo().getDisplayScenarioModel().getDisplayScenarioId());
                if(getKitGrid()!=null)
                	kitComponentDetail = new KitComponentDetail(kitComponentDTO, displayDTO, null,kitComponentIds, new Long(kitComponentIds.size()), false,false,null/*,null*/);
                else
                	kitComponentDetail = new KitComponentDetail(kitComponentDTO, displayDTO,getDisplayCostDataGroup().getDisplayScenarioDTO(), kitComponentIds, new Long(kitComponentIds.size()), false,true,getDisplayCostDataGroup().getCostAnalysisInfo().reCalculateDate.getValue()/*,getDisplayCostDataGroup()*/);	
                App.getVp().clear();
                App.getVp().add(kitComponentDetail);
                getWindow().close();         	
          	}
        });
        cp.add(grid);


        SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
            public void componentSelected(ButtonEvent ce) {
                Button button = (Button) ce.getComponent();
                if (kitComponentIds == null) kitComponentIds = new LinkedList<Long>();
                if(getKitGrid()!=null){
                	displayDTO=getKitGrid().getDisplayTabs().getDisplayModel(); // DisplayDTO.displaySetupId will have new value if the display is modified
                }else{
                	displayDTO=getDisplayCostDataGroup().getDisplayTabs().getDisplayModel();
                }
                if (button.getText().equals(CREATE_SKU)) {
                    App.getVp().clear();
                    KitComponentDTO kitComponentDTO = new KitComponentDTO();
                    kitComponentDTO.setSourcePimFlg(false);
                    kitComponentDTO.setSequenceNum(seqNum);
                    KitComponentDetail kitComponentDetail=null;
                   // System.out.println("ProductSKUSelectionResultsGrid:SELECT_SKU getDisplayScenarioDTO().id 2.1..="+ getDisplayScenarioDTO().getDisplayScenarioId());
                   // System.out.println("ProductSKUSelectionResultsGrid:SELECT_SKU getDisplayCostDataGroup().getDisplayScenarioDTO().id 2.2..="+ getDisplayCostDataGroup().getDisplayScenarioDTO().getDisplayScenarioId());
                  //  System.out.println("ProductSKUSelectionResultsGrid:SELECT_SKU getDisplayCostDataGroup().getDisplayScenarioDTO().id 2.3..="+ getDisplayCostDataGroup().getCostAnalysisInfo().getDisplayScenarioModel().getDisplayScenarioId());                    
                    if(getKitGrid()!=null)
                    	kitComponentDetail = new KitComponentDetail(kitComponentDTO, displayDTO, null, kitComponentIds, new Long(kitComponentIds.size()), false,false,null/*,null*/);
                    else
                    	kitComponentDetail = new KitComponentDetail(kitComponentDTO, displayDTO, getDisplayCostDataGroup().getDisplayScenarioDTO(), kitComponentIds, new Long(kitComponentIds.size()), false, true,getDisplayCostDataGroup().getCostAnalysisInfo().reCalculateDate.getValue()/*,getDisplayCostDataGroup()*/);
                    	App.getVp().add(kitComponentDetail);
                    	getWindow().close();
                } else if (button.getText().equals(SELECT_SKU)) {
                    if (grid.getSelectionModel().getSelectedItem() != null) {
                       // button.disable();
                        ProductDetailDTO pd = (ProductDetailDTO) grid.getSelectionModel().getSelectedItem();
                        KitComponentDTO kitComponentDTO = new KitComponentDTO();
                        kitComponentDTO.setSkuId(pd.getSkuId());
                        kitComponentDTO.setSku(pd.getSku());
                        kitComponentDTO.setSequenceNum(seqNum);
                        kitComponentDTO.setProductName(pd.getIlsSkuDesc());
                        kitComponentDTO.setBuDesc(pd.getBuDesc());
                        kitComponentDTO.setRegCasePack(pd.getInnerpackQty());
                        kitComponentDTO.setPromoFlg(pd.isPromoFlg());
                        kitComponentDTO.setSourcePimFlg(true);
                        kitComponentDTO.setVersionNo(pd.getVersionNo());
                        KitComponentDetail kitComponentDetail =null;
                        //System.out.println("ProductSKUSelectionResultsGrid:SELECT_SKU getDisplayScenarioDTO().id 3.1..="+ getDisplayScenarioDTO().getDisplayScenarioId());
                        //System.out.println("ProductSKUSelectionResultsGrid:SELECT_SKU getDisplayCostDataGroup().getDisplayScenarioDTO().id 3.2..="+ getDisplayCostDataGroup().getDisplayScenarioDTO().getDisplayScenarioId());
                        //System.out.println("ProductSKUSelectionResultsGrid:SELECT_SKU getDisplayCostDataGroup().getDisplayScenarioDTO().id 3.3..="+ getDisplayCostDataGroup().getCostAnalysisInfo().getDisplayScenarioModel().getDisplayScenarioId());                        
                        if(getKitGrid()!=null){
                        	kitComponentDetail = new KitComponentDetail(kitComponentDTO, displayDTO, null,kitComponentIds, new Long(kitComponentIds.size()), false, false,null/*,null*/);
                        }else{
                        	kitComponentDetail = new KitComponentDetail(kitComponentDTO, displayDTO, getDisplayCostDataGroup().getDisplayScenarioDTO(), kitComponentIds, new Long(kitComponentIds.size()), false, true,getDisplayCostDataGroup().getCostAnalysisInfo().reCalculateDate.getValue()/*,getDisplayCostDataGroup()*/);
                        }
                        App.getVp().clear();
                        App.getVp().add(kitComponentDetail);
                        getWindow().close();
                    } else {
                        //Info.display("Select SKU", "Please select a SKU");
                    }
                } else if (button.getText().equals(CANCEL)) {
                	getWindow().close();
                } else {
                    //Info.display("Click Event", "The '{0}' button was clicked.", button.getText());
                }
            }
        };
        cp.addButton(new Button(CREATE_SKU, listener));
        cp.addButton(new Button(SELECT_SKU, listener));
        cp.addButton(new Button(CANCEL, listener));
        add(cp);
    }

  
	public DisplayDTO getDisplayDTO() {
		return displayDTO;
	}


	public void setDisplayDTO(DisplayDTO displayDTO) {
		this.displayDTO = displayDTO;
	}


	public DisplayCostDataGroup getDisplayCostDataGroup() {
		return displayCostDataGroup;
	}


	public void setDisplayCostDataGroup(DisplayCostDataGroup displayCostDataGroup) {
		this.displayCostDataGroup = displayCostDataGroup;
	}

	public KitComponentInfoGrid getKitGrid() {
		return kitGrid;
	}

	public void setKitGrid(KitComponentInfoGrid kitGrid) {
		this.kitGrid = kitGrid;
	}

	public void load(String sku, List<Long> ids, Long seq) {
    	this.seqNum = seq;
    	this.kitComponentIds = ids;
    	
        final MessageBox loadProgressBar = MessageBox.wait("Progress",
                "Searching for product information, please wait...", "Searching...");

        AppService.App.getInstance().getProductDetail( sku,
                new Service.LoadProductDetail(store, loadProgressBar));
    }

}
