package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.google.gwt.user.client.Element;

public class DisplayAdminArea extends LayoutContainer {
   
    public DisplayAdminArea() {
    	this.setSize(1180, -1);
    }
    
    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        buildHeader();
        buildBody();
    }

    private void buildHeader() {
        add(new HeaderPanel(HeaderPanel.ADMIN));
    }

    private void buildBody() {
        add(new AdminTabs());
    }
}
