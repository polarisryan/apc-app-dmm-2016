package com.averydennison.dmm.app.client;

import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Oct 21, 2009
 * Time: 12:41:28 PM
 */
public class KitComponentInfo extends LayoutContainer implements DirtyInterface {

    private VerticalPanel vp;
    private DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();

    private DisplayDTO display;
    private DisplayTabs displayTabs;
    private Boolean readOnly;
    private KitComponentInfoGrid kitComponentInfoGrid;
    private ChangeListener displayListener;
    
    public KitComponentInfo() {
        vp = new VerticalPanel();
        vp.setSpacing(10);
    }

    
    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        System.out.println("KitComponentINfo:onRender starts");
        setModelsAndView();
        createHeader();
        createForm();
        System.out.println("KitComponentINfo:onRender ends");
        add(vp);

    }

   
    
    private void setModelsAndView() {
        System.out.println("KitComponentInfo: set models and view");
        display = displayTabs.getDisplayModel();

        if (display == null) {
            display = new DisplayDTO();
        }
        readOnly = this.getData("readOnly");
        if (readOnly == null) readOnly = false;

        updateView();
        
    	displayListener = new ChangeListener() {
			public void modelChanged(ChangeEvent event) {
				display = (DisplayDTO) event.getSource();
				if (display != null && display.getDisplayId() != null) {
					displayTabs.setDisplayModel(display);
					displayTabs.setData("display", display);
					if (display != null && display.getDisplayId() != null) {
			            displayTabHeader.setDisplay(display);
			        }
				}
			}
		};
		display.addChangeListener(displayListener);
    }

    private void createHeader() {
        add(displayTabHeader);
    }

    public DisplayTabHeaderV2 getDisplayTabHeader() {
    	return displayTabHeader;
    }
    
    private void updateView() {
    	this.display = displayTabs.getDisplayModel();
        if (this.display.getDisplayId() != null) {
            displayTabHeader.setDisplay(this.display);
        }
    }
    
    private boolean overrideDirty = false;
    
    KitComponentInfoGrid kcig;
    List<DisplayScenarioDTO> displayScenarioDTOs;
    
    public List<DisplayScenarioDTO> getDisplayScenarioDTOs() {
		return displayScenarioDTOs;
	}


	public void setDisplayScenarioDTOs(List<DisplayScenarioDTO> displayScenarioDTOs) {
		this.displayScenarioDTOs = displayScenarioDTOs;
	}


	public void reload() {
    	overrideDirty = false;
    	this.display = displayTabs.getDisplayModel();
    	setModelsAndView();
    	kcig.getSkuMixFinalCheckBox().setValue(this.display.isSkuMixFinalFlg());
    	kcig.reload(this.display, getDisplayScenarioDTOs());
    }
    
	public void  reloadGrid(){
		try{
		kcig.reload(this.display, getDisplayScenarioDTOs());
		}catch(Exception ex){
			System.out.println("KitComponentInfo:reloadGrid error "+ ex.toString());
		}
	}
	
    public void overrideDirty(){
        this.overrideDirty = true;
    }
    
    public boolean isDirty(){
        if(overrideDirty==true) return false;        
    	return kitComponentInfoGrid.isDirty();
    }

    public void save(Button btn, TabPanel panel, TabItem tabItem) {
    	if (display.getDisplayId() == null) {
    		 MessageBox.alert("Validation Error", "There is no display defined " , null);
    	}else{
    		kitComponentInfoGrid.save(btn, panel, tabItem);
    	}
    }

    private void createForm() {
        LayoutContainer main = new LayoutContainer();
        main.setLayout(new ColumnLayout());
        kcig = new KitComponentInfoGrid(display, this, readOnly);
        this.kitComponentInfoGrid=kcig;
        add(kcig);
    }

    public DisplayTabs getDisplayTabs() {
        return displayTabs;
    }

    public void setDisplayTabs(DisplayTabs displayTabs) {
        this.displayTabs = displayTabs;
    }

    public KitComponentInfoGrid getKitComponentInfoGrid() {
        return this.kitComponentInfoGrid;
    }
}
