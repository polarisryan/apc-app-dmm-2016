package com.averydennison.dmm.app.client.exception;

import java.io.Serializable;

public class DuplicateException extends Exception implements Serializable{

	private static final long serialVersionUID = -4071065388324786449L;

	public DuplicateException() {
		super();
	}

	public DuplicateException(String message) {
		super(message);
	}

}
