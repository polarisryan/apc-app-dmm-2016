package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.averydennison.dmm.app.client.admin.AbstractAdminPanel;
import com.averydennison.dmm.app.client.admin.CorrugateVendors;
import com.averydennison.dmm.app.client.admin.Customers;
import com.averydennison.dmm.app.client.exception.DuplicateException;
import com.averydennison.dmm.app.client.models.BooleanDTO;
import com.averydennison.dmm.app.client.models.BuCostCenterSplitDTO;
import com.averydennison.dmm.app.client.models.CalculationSummaryDTO;
import com.averydennison.dmm.app.client.models.CorrugateComponentsDetailDTO;
import com.averydennison.dmm.app.client.models.CorrugateDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisHistoryDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.averydennison.dmm.app.client.models.CostPriceDTO;
import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.CustomerMarketingDTO;
import com.averydennison.dmm.app.client.models.CustomerTypeDTO;
import com.averydennison.dmm.app.client.models.Destination;
import com.averydennison.dmm.app.client.models.DisplayCostCommentDTO;
import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.client.models.DisplayCostImageDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayDcAndPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayDcDTO;
import com.averydennison.dmm.app.client.models.DisplayForMassUpdateDTO;
import com.averydennison.dmm.app.client.models.DisplayLogisticsDTO;
import com.averydennison.dmm.app.client.models.DisplayPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayPlannerDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.client.models.Folder;
import com.averydennison.dmm.app.client.models.InPogDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.LocationDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerDTO;
import com.averydennison.dmm.app.client.models.ProductDetailDTO;
import com.averydennison.dmm.app.client.models.ProgramBuDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodDTO;
import com.averydennison.dmm.app.client.models.SlimPackoutDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.averydennison.dmm.app.client.models.UserDTO;
import com.averydennison.dmm.app.client.models.ValidBuDTO;
import com.averydennison.dmm.app.client.models.ValidLocationDTO;
import com.averydennison.dmm.app.client.models.ValidPlannerDTO;
import com.averydennison.dmm.app.client.models.ValidPlantDTO;
import com.averydennison.dmm.app.client.util.Constants;
import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeEventSource;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreFilter;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * User: Spart Arguello
 * Date: Nov 5, 2009
 * Time: 11:08:30 AM
 * todo revisit/review/redesign/rewrite
 */
public class Service {

	private static final Map<Long, StatusDTO> statusMap = new HashMap<Long, StatusDTO>();
	private static final Map<Long, CustomerDTO> customerMap = new HashMap<Long, CustomerDTO>();
	private static final Map<Long, CustomerCountryDTO> customerCountryMap = new HashMap<Long, CustomerCountryDTO>();
	private static final Map<Long, StructureDTO> structureMap = new HashMap<Long, StructureDTO>();
	private static final Map<Long, DisplaySalesRepDTO> projectManagers = new HashMap<Long, DisplaySalesRepDTO>();
	private static final Map<Long, PromoPeriodDTO> promoPeriodMap = new HashMap<Long, PromoPeriodDTO>();
	private static final Map<Long, PromoPeriodCountryDTO> promoPeriodCountryMap = new HashMap<Long, PromoPeriodCountryDTO>();
	

	private static final Map<Long, ManufacturerDTO> manufacturerMap = new HashMap<Long, ManufacturerDTO>();
	private static final Map<Long, ManufacturerCountryDTO> manufacturerCountryMap = new HashMap<Long, ManufacturerCountryDTO>();
	private static final Map<Long, LocationDTO> locationMap = new HashMap<Long, LocationDTO>();
	private static final Map<Long, LocationCountryDTO> locationCountryMap = new HashMap<Long, LocationCountryDTO>();

	private static final Map<Long, CountryDTO> countryMap = new HashMap<Long, CountryDTO>();
	private static final Map<Boolean, InPogDTO> inPogYnMap = new HashMap<Boolean, InPogDTO>();

	public static Map<Long, StatusDTO> getStatusMap() {
		if (statusMap.size() == 0) {
			loadStatusMap();
		}
		return statusMap;
	}

	public static Map<Long, LocationDTO> getLocationMap() {
		if (locationMap.size() == 0) {
			loadLocationMap();
		}
		return locationMap;
	}
	
	public static Map<Long, LocationCountryDTO> getLocationCountryMap() {
		if (locationCountryMap.size() == 0) {
			loadLocationMap();
		}
		return locationCountryMap;
	}
	
	public static Map<Long, CustomerDTO> getCustomerMap() {
		if (customerMap.size() == 0) {
			loadCustomerMap();
		}
		return customerMap;
	}

	public static Map<Long, CustomerCountryDTO> getCustomerCountryMap() {
		if (customerCountryMap.size() == 0) {
			loadCustomerCountryMap();
		}
		return customerCountryMap;
	}
	
	public static Map<Long, StructureDTO> getStructureMap() {
		if (structureMap.size() == 0) {
			loadStructureMap();
		}
		return structureMap;
	}

	public static Map<Long, DisplaySalesRepDTO> getProjectManagerMap() {
		if (projectManagers.size() == 0) {
			loadProjectManagerMap();
		}
		return projectManagers;
	}

	
	public static class SetInPogStore implements AsyncCallback<List<InPogDTO>> {
		private ListStore<InPogDTO> store = new ListStore<InPogDTO>();

		public SetInPogStore(ListStore<InPogDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Structures through server", "using client side structures", null);
			this.store.add(getClientPogDTOList());
		}

		private List<InPogDTO> getClientPogDTOList() {
			List<InPogDTO> inpogs = new ArrayList<InPogDTO>();
			inpogs.add(new InPogDTO(Boolean.TRUE, "Yes"));
			inpogs.add(new InPogDTO(Boolean.FALSE, "No"));
			return inpogs;
		}

		public void onSuccess(List<InPogDTO> result) {
			this.store.add(result);
			if (inPogYnMap.size() == 0) {
				for (int i = 0; i < result.size(); i++) {
					inPogYnMap.put(result.get(i).getInPogYorNId(), result.get(i));
				}
			}
	}
	}
	
	public static Map<Boolean, InPogDTO> getInPogYnMap() {
		if (inPogYnMap.size() == 0) {
			loadInPogYnMap();
		}
		return inPogYnMap;
	}

	private static void loadInPogYnMap() {
		AppService.App.getInstance().getInPogMapDTOs(new LoadInPogYnMap(inPogYnMap));
	}


	public static class LoadInPogYnMap implements AsyncCallback<Map<Boolean, InPogDTO>> {
		private Map<Boolean, InPogDTO> inPogYnMap = new HashMap<Boolean, InPogDTO>();

		public LoadInPogYnMap(Map<Boolean, InPogDTO> inPogYnDTOMap) {
			this.inPogYnMap = inPogYnDTOMap;
		}

		public void onFailure(Throwable throwable) {
			//Info.display("Failed to get promo periods through server", "try again later...");
		}

		public void onSuccess(Map<Boolean, InPogDTO> inPogYnDTOMap) {
			this.inPogYnMap = inPogYnDTOMap;
		}
	}
	
	public static Map<Long, PromoPeriodDTO> getPromoPeriodMap() {
		if (promoPeriodMap.size() == 0) {
			loadPromoPeriodMap();
		}
		return promoPeriodMap;
	}

	private static void loadPromoPeriodMap() {
		AppService.App.getInstance().getPromoPeriodMapDTOs(new LoadPromoPeriodMap(promoPeriodMap));
	}


	public static class LoadPromoPeriodMap implements AsyncCallback<Map<Long, PromoPeriodDTO>> {
		private Map<Long, PromoPeriodDTO> promoPeriodMap = new HashMap<Long, PromoPeriodDTO>();

		public LoadPromoPeriodMap(Map<Long, PromoPeriodDTO> promoPeriodDTOMap) {
			this.promoPeriodMap = promoPeriodDTOMap;
		}

		public void onFailure(Throwable throwable) {
			//Info.display("Failed to get promo periods through server", "try again later...");
		}

		public void onSuccess(Map<Long, PromoPeriodDTO> promoPeriodDTOMap) {
			this.promoPeriodMap = promoPeriodDTOMap;
		}
	}

	public static Map<Long, PromoPeriodCountryDTO> getPromoPeriodCountryMap() {
		if (promoPeriodCountryMap.size() == 0) {
			loadPromoPeriodCountryMap();
		}
		return promoPeriodCountryMap;
	}

	private static void loadPromoPeriodCountryMap() {
		AppService.App.getInstance().getPromoPeriodCountryMapDTOs(new LoadPromoPeriodCountryMap(promoPeriodCountryMap));
	}


	public static class LoadPromoPeriodCountryMap implements AsyncCallback<Map<Long, PromoPeriodCountryDTO>> {
		private Map<Long, PromoPeriodCountryDTO> promoPeriodCountryMap = new HashMap<Long, PromoPeriodCountryDTO>();

		public LoadPromoPeriodCountryMap(Map<Long, PromoPeriodCountryDTO> promoPeriodCountryDTOMap) {
			this.promoPeriodCountryMap = promoPeriodCountryDTOMap;
		}

		public void onFailure(Throwable throwable) {
			//Info.display("Failed to get promo periods through server", "try again later...");
		}

		public void onSuccess(Map<Long, PromoPeriodCountryDTO> promoPeriodCountryDTOMap) {
			this.promoPeriodCountryMap = promoPeriodCountryDTOMap;
		}
	}
	
	public static Map<Long, ManufacturerDTO> getManufacturerMap() {
		if (manufacturerMap.size() == 0) {
			loadManufacturerMap();
		}
		return manufacturerMap;
	}

	private static void loadManufacturerMap() {
		AppService.App.getInstance().getManufacturerMapDTOs(new LoadManufacturerMap(manufacturerMap));
	}

	public static class LoadManufacturerMap implements AsyncCallback<Map<Long, ManufacturerDTO>> {
		private Map<Long, ManufacturerDTO> manufacturerMap = new HashMap<Long, ManufacturerDTO>();

		public LoadManufacturerMap(Map<Long, ManufacturerDTO> manufacturerDTOMap) {
			this.manufacturerMap = manufacturerDTOMap;
		}

		public void onFailure(Throwable throwable) {
			//Info.display("Failed to get manufacturer through server", "try again later...");
		}

		public void onSuccess(Map<Long, ManufacturerDTO> manufacturerDTOMap) {
			this.manufacturerMap = manufacturerDTOMap;
		}
	}

	
	public static Map<Long, ManufacturerCountryDTO> getManufacturerCountryMap() {
		if (manufacturerCountryMap.size() == 0) {
			loadManufacturerCountryMap();
		}
		return manufacturerCountryMap;
	}

	private static void loadManufacturerCountryMap() {
		AppService.App.getInstance().getManufacturerCountryMapDTOs(new LoadManufacturerCountryMap(manufacturerCountryMap));
	}

	public static class LoadManufacturerCountryMap implements AsyncCallback<Map<Long, ManufacturerCountryDTO>> {
		private Map<Long, ManufacturerCountryDTO> manufacturerCountryMap = new HashMap<Long, ManufacturerCountryDTO>();

		public LoadManufacturerCountryMap(Map<Long, ManufacturerCountryDTO> manufacturerCountryDTOMap) {
			this.manufacturerCountryMap = manufacturerCountryDTOMap;
		}

		public void onFailure(Throwable throwable) {
			//Info.display("Failed to get manufacturer through server", "try again later...");
		}

		public void onSuccess(Map<Long, ManufacturerCountryDTO> manufacturerCountryDTOMap) {
			this.manufacturerCountryMap = manufacturerCountryDTOMap;
		}
	}
	public static Map<Long, CountryDTO> getCountryMap() {
		if (countryMap.size() == 0) {
			loadCountryMap();
		}
		return countryMap;
	}

	public static Map<Long, CountryDTO> getActiveCountryFlagMap() {
		if (countryMap.size() == 0) {
			loadActiveCountryFlagMap();
		}
		return countryMap;
	}
	
	private static void loadActiveCountryFlagMap() {
		AppService.App.getInstance().getActiveCountryFlagMapDTOs(new LoadCountryMap(countryMap));
	}
	
	private static void loadCountryMap() {
		AppService.App.getInstance().getCountryMapDTOs(new LoadCountryMap(countryMap));
	}

	public static class LoadCountryMap implements AsyncCallback<Map<Long, CountryDTO>> {
		private Map<Long, CountryDTO> countryMap = new HashMap<Long, CountryDTO>();

		public LoadCountryMap(Map<Long, CountryDTO> countryDTOMap) {
			this.countryMap = countryDTOMap;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get country through server", "try again later...",null);
		}

		public void onSuccess(Map<Long, CountryDTO> countryDTOMap) {
			this.countryMap = countryDTOMap;
		}
	}
	

	private static void loadProjectManagerMap() {
		//todo
	}

	private static void loadStructureMap() {
		//todo
	}

	private static void loadCustomerMap() {
		AppService.App.getInstance().getCustomerDTOMap(new LoadCustomerMap(customerMap));
	}

	public static class LoadCustomerMap implements AsyncCallback<Map<Long, CustomerDTO>> {
		private Map<Long, CustomerDTO> customers;

		public LoadCustomerMap(Map<Long, CustomerDTO> customers) {
			this.customers = customers;
		}

		public void onFailure(Throwable throwable) {
			//Info.display("Failed to get customer through server", "using client side customers");
			this.customers = getClientCustomerDTOMap();
		}

		private Map<Long, CustomerDTO> getClientCustomerDTOMap() {
			List<CustomerDTO> list = getClientCustomerDTOList();
			for (int i = 0; i < list.size(); i++) {
				customerMap.put(list.get(i).getCustomerId(), list.get(i));
			}
			return customerMap;
		}

		public void onSuccess(Map<Long, CustomerDTO> result) {
			this.customers = result;
		}
	}

	private static void loadCustomerCountryMap() {
		AppService.App.getInstance().getCustomerCountryDTOMap(new LoadCustomerCountryMap(customerCountryMap));
	}

	public static class LoadCustomerCountryMap implements AsyncCallback<Map<Long, CustomerCountryDTO>> {
		private Map<Long, CustomerCountryDTO> customersCountry;

		public LoadCustomerCountryMap(Map<Long, CustomerCountryDTO> customersCountry) {
			this.customersCountry = customersCountry;
		}

		public void onFailure(Throwable throwable) {
			//Info.display("Failed to get customer through server", "using client side customers");
			this.customersCountry = getClientCustomerCountryDTOMap();
		}

		private Map<Long, CustomerCountryDTO> getClientCustomerCountryDTOMap() {
			List<CustomerCountryDTO> list = getClientCustomerCountryDTOList();
			for (int i = 0; i < list.size(); i++) {
				customerCountryMap.put(list.get(i).getCustomerId(), list.get(i));
			}
			return customerCountryMap;
		}

		public void onSuccess(Map<Long, CustomerCountryDTO> result) {
			this.customersCountry = result;
		}
	}
	
	public static class LoadStatusMap implements AsyncCallback<Map<Long, StatusDTO>> {
		private Map<Long, StatusDTO> statuses;

		public LoadStatusMap(Map<Long, StatusDTO> statuses) {
			this.statuses = statuses;
		}

		public void onFailure(Throwable throwable) {
			//Info.display("Failed to get statuses through server", "using client side statuses");
			this.statuses = getClientStatusDTOMap();
		}

		public void onSuccess(Map<Long, StatusDTO> result) {
			this.statuses = result;
		}
	}

	public static class LoadLocationMap implements AsyncCallback<Map<Long, LocationDTO>> {
		private Map<Long, LocationDTO> locations;

		public LoadLocationMap(Map<Long, LocationDTO> locations) {
			this.locations = locations;
		}

		public void onFailure(Throwable throwable) {
			//Info.display("Failed to get statuses through server", "using client side statuses");
			this.locations = getClientLocationDTOMap();
		}

		public void onSuccess(Map<Long, LocationDTO> result) {
			this.locations = result;
		}
	}
	
	public static class LoadLocationCountryMap implements AsyncCallback<Map<Long, LocationCountryDTO>> {
		private Map<Long, LocationCountryDTO> locations;

		public LoadLocationCountryMap(Map<Long, LocationCountryDTO> locations) {
			this.locations = locations;
		}

		public void onFailure(Throwable throwable) {
			this.locations = getClientLocationCountryDTOMap();
		}

		public void onSuccess(Map<Long, LocationCountryDTO> result) {
			this.locations = result;
		}
	}
	
	private static Map<Long, LocationDTO> getClientLocationDTOMap() {
		Map<Long, LocationDTO> locations = new HashMap<Long, LocationDTO>();
		locations.put(1L, new LocationDTO(1L, "T5", "T5-Tijuana LFDC", Boolean.TRUE));
		locations.put(2L, new LocationDTO(2L, "A1", "A1-Alliance Display and Packaging", Boolean.TRUE));
		locations.put(3L, new LocationDTO(3L, "FO", "FO-Fontana Distribution Center", Boolean.TRUE));
		locations.put(4L, new LocationDTO(4L, "F2", "F2-Fontana Kit DC", Boolean.TRUE));
		locations.put(5L, new LocationDTO(5L, "GE", "GE-Genco Distributors, Lebanon", Boolean.TRUE));
		return locations;
	}
	
	private static Map<Long, LocationCountryDTO> getClientLocationCountryDTOMap() {
		Map<Long, LocationCountryDTO> locations = new HashMap<Long, LocationCountryDTO>();
		locations.put(1L,  new LocationCountryDTO(1L, "T5", "T5-Tijuana LFDC", Boolean.TRUE, 1L, "UNITED STATES"));
		locations.put(2L, new LocationCountryDTO(2L, "A1", "A1-Alliance Display and Packaging", Boolean.TRUE, 1L, "UNITED STATES"));
		locations.put(3L, new LocationCountryDTO(3L, "FO", "FO-Fontana Distribution Center", Boolean.TRUE, 1L, "UNITED STATES"));
		locations.put(4L, new LocationCountryDTO(4L, "F2", "F2-Fontana Kit DC", Boolean.TRUE, 1L, "UNITED STATES"));
		locations.put(5L, new LocationCountryDTO(5L, "GE", "GE-Genco Distributors, Lebanon", Boolean.TRUE, 1L, "UNITED STATES"));
		return locations;
	}
	
	private static List<LocationDTO> getClientLocationDTOList() {
		List<LocationDTO> locations = new ArrayList<LocationDTO>();
		locations.add(new LocationDTO(1L, "T5", "T5-Tijuana LFDC", Boolean.TRUE));
		locations.add( new LocationDTO(2L, "A1", "A1-Alliance Display and Packaging", Boolean.TRUE));
		locations.add( new LocationDTO(3L, "FO", "FO-Fontana Distribution Center", Boolean.TRUE));
		locations.add( new LocationDTO(4L, "F2", "F2-Fontana Kit DC", Boolean.TRUE));
		locations.add( new LocationDTO(5L, "GE", "GE-Genco Distributors, Lebanon", Boolean.TRUE));
		
		return locations;
	}
	
	private static List<LocationCountryDTO> getClientLocationCountryDTOList() {
		List<LocationCountryDTO> locationsCountry = new ArrayList<LocationCountryDTO>();
		locationsCountry.add(new LocationCountryDTO(1L, "T5", "T5-Tijuana LFDC", Boolean.TRUE, 1L, "UNITED STATES"));
		locationsCountry.add( new LocationCountryDTO(2L, "A1", "A1-Alliance Display and Packaging", Boolean.TRUE, 1L, "UNITED STATES"));
		locationsCountry.add( new LocationCountryDTO(3L, "FO", "FO-Fontana Distribution Center", Boolean.TRUE, 1L, "UNITED STATES"));
		locationsCountry.add( new LocationCountryDTO(4L, "F2", "F2-Fontana Kit DC", Boolean.TRUE, 1L, "UNITED STATES"));
		locationsCountry.add( new LocationCountryDTO(5L, "GE", "GE-Genco Distributors, Lebanon", Boolean.TRUE, 1L, "UNITED STATES"));
		return locationsCountry;
	}
	
	private static Map<Long, StatusDTO> getClientStatusDTOMap() {
		Map<Long, StatusDTO> statuses = new HashMap<Long, StatusDTO>();
		statuses.put(1L, new StatusDTO(1L, "Proposed", "Proposed"));
		statuses.put(2L, new StatusDTO(2L, "Pending", "Pending"));
		statuses.put(3L, new StatusDTO(3L, "Approved", "Approved"));
		statuses.put(4L, new StatusDTO(4L, "Completed", "Completed"));
		statuses.put(5L, new StatusDTO(5L, "Cancelled", "Cancelled"));
		return statuses;
	}

	private static List<StatusDTO> getClientStatusDTOList() {
		List<StatusDTO> statuses = new ArrayList<StatusDTO>();
		statuses.add(new StatusDTO(1L, "Proposed", "Proposed"));
		statuses.add(new StatusDTO(2L, "Pending", "Pending"));
		statuses.add(new StatusDTO(3L, "Approved", "Approved"));
		statuses.add(new StatusDTO(4L, "Completed", "Completed"));
		statuses.add(new StatusDTO(5L, "Cancelled", "Cancelled"));
		return statuses;
	}
	
	
	private static List<CountryDTO> getClientCountryDTOList() {
		List<CountryDTO> countries = new ArrayList<CountryDTO>();
		countries.add(new CountryDTO(1L, "Country I" ,"Country I",Boolean.TRUE,Boolean.TRUE) );
		countries.add(new CountryDTO(2L, "Country II" ,"Country II",Boolean.TRUE,Boolean.TRUE));
		countries.add(new CountryDTO(3L, "Country III" ,"Country III",Boolean.TRUE,Boolean.TRUE));
		countries.add(new CountryDTO(4L, "Country IV" ,"Country IV",Boolean.TRUE,Boolean.TRUE));
		countries.add(new CountryDTO(5L, "Country V" ,"Country V",Boolean.TRUE,Boolean.TRUE));
		return countries;
	}

	private static List<ManufacturerDTO> getClientManufacturerDTOList() {
		List<ManufacturerDTO> manufacturers = new ArrayList<ManufacturerDTO>();
		manufacturers.add(new ManufacturerDTO(1L, "OCC"));
		manufacturers.add(new ManufacturerDTO(2L, "Justman"));
		manufacturers.add(new ManufacturerDTO(3L, "International Paper"));
		manufacturers.add(new ManufacturerDTO(4L, "PCA"));
		manufacturers.add(new ManufacturerDTO(5L, "Sonoco-Corrflex"));
		manufacturers.add(new ManufacturerDTO(6L, "Smurfit-Stone"));
		manufacturers.add(new ManufacturerDTO(7L, "Rapid Displays"));
		manufacturers.add(new ManufacturerDTO(8L, "Cornerstone"));
		manufacturers.add(new ManufacturerDTO(9L, "Dual Graphics"));
		manufacturers.add(new ManufacturerDTO(10L, "ULINE"));
		manufacturers.add(new ManufacturerDTO(11L, "FFR"));
		manufacturers.add(new ManufacturerDTO(12L, "United Merchants"));
		manufacturers.add(new ManufacturerDTO(13L, "Felbro"));
		manufacturers.add(new ManufacturerDTO(14L, "U.S. Display Group"));
		return manufacturers;
	}
	
	private static List<ManufacturerCountryDTO> getClientManufacturerCountryDTOList() {
		List<ManufacturerCountryDTO> manufacturersCountry = new ArrayList<ManufacturerCountryDTO>();
		manufacturersCountry.add(new ManufacturerCountryDTO(1L, "OCC",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(2L, "Justman",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(3L, "International Paper",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(4L, "PCA",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(5L, "Sonoco-Corrflex",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(6L, "Smurfit-Stone",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(7L, "Rapid Displays",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(8L, "Cornerstone",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(9L, "Dual Graphics",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(10L, "ULINE",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(11L, "FFR",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(12L, "United Merchants",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(13L, "Felbro",1L, "US"));
		manufacturersCountry.add(new ManufacturerCountryDTO(14L, "U.S. Display Group",1L, "US"));
		return manufacturersCountry;
	}

	private static void loadStatusMap() {
		AppService.App.getInstance().getStatusDTOMap(new LoadStatusMap(statusMap));
	}

	public void setStatusStore(ListStore<StatusDTO> store) {
		AppService.App.getInstance().getStatusDTOs(new SetStatusStore(store));
	}

	public void setCustomerStore(ListStore<CustomerDTO> store) {
		AppService.App.getInstance().getCustomerDTOs(new SetCustomerStore(store));
	}

	public void setCustomerCountryStore(Long countryId, ListStore<CustomerCountryDTO> store) {
		AppService.App.getInstance().getCustomerCountryDTOs(new SetCustomerCountryStore(countryId, store));
	}
	
	public void setManufacturerStore(ListStore<ManufacturerDTO> store) {
		AppService.App.getInstance().getManufacturerDTOs(new SetManufacturerStore(store));
	}

	public void setManufacturerCountryStore(Long countryId, ListStore<ManufacturerCountryDTO> store) {
		AppService.App.getInstance().getManufacturerCountryDTOs(new SetManufacturersCountryStore(countryId, store));
	}

	
	public void setLocationStore(ListStore<LocationDTO> store) {
		AppService.App.getInstance().getLocationDTOs(new SetLocationStore(store));
	}

	public void setLocationCountryStore(Long countryId, ListStore<LocationCountryDTO> store) {
		AppService.App.getInstance().getLocationCountryDTOs(new SetLocationCountryStore(countryId, store));
	}
	
	private static void loadLocationMap() {
		AppService.App.getInstance().getLocationMapDTOs(new LoadLocationMap(locationMap));
	}
	
	private static void loadLocationCountryMap() {
		AppService.App.getInstance().getLocationCountryMapDTOs(new LoadLocationCountryMap(locationCountryMap));
	}
	
	public static List<Destination> getDestinations() {
		List<Destination> destinations = new ArrayList<Destination>();
		destinations.add(new Destination("DOM"));
		destinations.add(new Destination("OS"));
		return destinations;
	}

	public static class SetStatusStore implements AsyncCallback<List<StatusDTO>> {
		private ListStore<StatusDTO> store;

		public SetStatusStore(ListStore<StatusDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get statuses through server", "using client side statuses", null);
			this.store.add(getClientStatusDTOList());
		}

		public void onSuccess(List<StatusDTO> result) {
			this.store.add(result);
			if (statusMap.size() == 0) {
				for (int i = 0; i < result.size(); i++) {
					statusMap.put(result.get(i).getStatusId(), result.get(i));
				}
			}
		}
	}

	public static class SetCountryStore implements AsyncCallback<List<CountryDTO>> {
		private ListStore<CountryDTO> store;

		public SetCountryStore(ListStore<CountryDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get countries through server", "using client side countries", null);
			this.store.add(getClientCountryDTOList());
		}

		public void onSuccess(List<CountryDTO> result) {
			if(result==null) return;
			this.store.add(result);
			if (countryMap.size() == 0) {
				for (int i = 0; i < result.size(); i++) {
					countryMap.put(result.get(i).getCountryId(), result.get(i));
				}
			}
		}
	}
	
	public static class SetLocationStore implements AsyncCallback<List<LocationDTO>> {
		private ListStore<LocationDTO> store;

		public SetLocationStore(ListStore<LocationDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get locations through server", "using client side locations", null);
			this.store.add(getClientLocationDTOList());
		}

		public void onSuccess(List<LocationDTO> result) {
			this.store.add(result);

			if (locationMap.size() == 0) {
				for (int i = 0; i < result.size(); i++) {
					locationMap.put(result.get(i).getLocationId(), result.get(i));
				}
			}
		}
	}
	
	public static class SetLocationCountryStore implements AsyncCallback<List<LocationCountryDTO>> {
		private ListStore<LocationCountryDTO> store= new ListStore<LocationCountryDTO>();
		private Long countryId;
		public SetLocationCountryStore(Long countryId, ListStore<LocationCountryDTO> store) {
			this.store = store;
			this.countryId=countryId;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get locations through server", "using client side locations", null);
			this.store.add(getClientLocationCountryDTOList());
		}

		public void onSuccess(List<LocationCountryDTO> result) {
			this.store.removeAll();
			this.store.add(result);
			if (locationCountryMap.size() == 0) {
				for (int i = 0; i < result.size(); i++) {
					locationCountryMap.put(result.get(i).getLocationId(), result.get(i));
				}
			}
			if(this.countryId!=null  ){
				CountryDTO countryDTO=countryMap.get(this.countryId);
				this.store.filter(PromoPeriodCountryDTO.COUNTRY_NAME, countryDTO.getCountryName());
			}
		}
	}
	
	public static class SetManufacturerStore implements AsyncCallback<List<ManufacturerDTO>> {
		private ListStore<ManufacturerDTO> store;

		public SetManufacturerStore(ListStore<ManufacturerDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get statuses through server", "using client side statuses", null);
			this.store.add(getClientManufacturerDTOList());
		}

		public void onSuccess(List<ManufacturerDTO> result) {
			this.store.add(result);

			if (manufacturerMap.size() == 0) {
				for (int i = 0; i < result.size(); i++) {
					manufacturerMap.put(result.get(i).getManufacturerId(), result.get(i));
				}
			}
		}
	}

	
	public static class SetManufacturersCountryStore implements AsyncCallback<List<ManufacturerCountryDTO>> {
		private ListStore<ManufacturerCountryDTO> store = new ListStore<ManufacturerCountryDTO>();
		private Long countryId;

		public SetManufacturersCountryStore(Long countryId, ListStore<ManufacturerCountryDTO> store) {
			this.store = store;
			this.countryId=countryId;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get statuses through server", "using client side statuses", null);
			this.store.add(getClientManufacturerCountryDTOList());
		}

		public void onSuccess(List<ManufacturerCountryDTO> result) {
			//this.store.removeAll();
			this.store.add(result);
			
			if (manufacturerCountryMap.size() == 0) {
				for (int i = 0; i < result.size(); i++) {
					manufacturerCountryMap.put(result.get(i).getManufacturerId(), result.get(i));
				}
			}
			if(this.countryId!=null  ){
				CountryDTO countryDTO=countryMap.get(this.countryId);
				this.store.filter(ManufacturerCountryDTO.COUNTRY_NAME, countryDTO.getCountryName());
			}
		}
	}
	
	
	public static class SetCustomerStore implements AsyncCallback<List<CustomerDTO>> {
		private ListStore<CustomerDTO> store = new ListStore<CustomerDTO>();

		public SetCustomerStore(ListStore<CustomerDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Customers through server", "using client side customers", null);
			this.store.add(getClientCustomerDTOList());
		}

		public void onSuccess(List<CustomerDTO> result) {
			this.store.add(result);
			if (customerMap.size() == 0) {
				for (int i = 0; i < result.size(); i++) {
					customerMap.put(result.get(i).getCustomerId(), result.get(i));
				}
			}
		}
	}

	public static class SetCustomerCountryStore implements AsyncCallback<List<CustomerCountryDTO>> {
		private ListStore<CustomerCountryDTO> store = new ListStore<CustomerCountryDTO>();
		private Long countryId;
		public SetCustomerCountryStore(Long countryId, ListStore<CustomerCountryDTO> store) {
			this.store = store;
			this.countryId=countryId;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Customers through server", "using client side customers", null);
			this.store.add(getClientCustomerCountryDTOList());
		}

		public void onSuccess(List<CustomerCountryDTO> result) {
			//this.store.removeAll();
			this.store.add(result);
			if (customerCountryMap.size() == 0) {
				for (int i = 0; i < result.size(); i++) {
					customerCountryMap.put(result.get(i).getCustomerId(), result.get(i));
				}
			}
			if(this.countryId!=null ){
				CountryDTO countryDTO=countryMap.get(this.countryId);
				this.store.filter(CustomerCountryDTO.COUNTRY_NAME, countryDTO.getCountryName());
			}
		
		}
	}
	
	private static List<CustomerDTO> getClientCustomerDTOList() {
		List<CustomerDTO> statuses = new ArrayList<CustomerDTO>();
		statuses.add(new CustomerDTO(1L, "Office Depot", "Office Depot"));
		statuses.add(new CustomerDTO(2L, "Office Max", "Office Max"));
		statuses.add(new CustomerDTO(3L, "Staples", "Staples"));
		statuses.add(new CustomerDTO(4L, "Home Depot", "Home Depot"));
		return statuses;
	}

	private static List<CustomerCountryDTO> getClientCustomerCountryDTOList() {
		List<CustomerCountryDTO> customerCountry = new ArrayList<CustomerCountryDTO>();
		customerCountry.add(new CustomerCountryDTO(1L, "AAFES", 1L, "UNITED STATES"));
		customerCountry.add(new CustomerCountryDTO(2L, "Alco Duckwall", 1L, "UNITED STATES"));
		customerCountry.add(new CustomerCountryDTO(3L, "Bi-Mart", 1L, "UNITED STATES"));
		customerCountry.add(new CustomerCountryDTO(4L, "Big Lots", 1L, "UNITED STATES"));
		return customerCountry;
	}
	
	public static class SetStructuresStore implements AsyncCallback<List<StructureDTO>> {
		private ListStore<StructureDTO> store = new ListStore<StructureDTO>();

		public SetStructuresStore(ListStore<StructureDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Structures through server", "using client side structures", null);
			this.store.add(getClientStructuresDTOList());
		}

		private List<StructureDTO> getClientStructuresDTOList() {
			List<StructureDTO> structures = new ArrayList<StructureDTO>();
			structures.add(new StructureDTO(1L, "Full Pallet", "Full Pallet"));
			structures.add(new StructureDTO(2L, "Half Pallet", "Half Pallet"));
			structures.add(new StructureDTO(3L, "9 Facing Octagon", "9 Facing Octagon"));
			structures.add(new StructureDTO(4L, "Tray", "Tray"));
			structures.add(new StructureDTO(5L, "8 Facing Tower", "8 Facing Tower"));
			structures.add(new StructureDTO(6L, "Endcap Tray", "Endcap Tray"));
			structures.add(new StructureDTO(7L, "Gravity Feed", "Gravity Feed"));
			structures.add(new StructureDTO(8L, "Dump Bin", "Dump Bin"));
			structures.add(new StructureDTO(9L, "Clip Strip", "Clip Strip"));
			structures.add(new StructureDTO(10L, "Power Wing", "Power Wing"));
			structures.add(new StructureDTO(11L, "9 Facing Tower", "9 Facing Tower"));
			structures.add(new StructureDTO(12L, "6 Facing Tower", "6 Facing Tower"));
			structures.add(new StructureDTO(13L, "4 Facing Tower", "4 Facing Tower"));
			structures.add(new StructureDTO(14L, "Quarter Pallet", "Quarter Pallet"));
			return structures;
		}

		public void onSuccess(List<StructureDTO> result) {
			this.store.add(result);
			for (int i = 0; i < result.size(); i++) {
				structureMap.put(result.get(i).getStructureId(), result.get(i));
			}
		}
	}

	public static class SetProjectManagerStore implements AsyncCallback<List<DisplaySalesRepDTO>> {
		private ListStore<DisplaySalesRepDTO> store = new ListStore<DisplaySalesRepDTO>();

		public SetProjectManagerStore(ListStore<DisplaySalesRepDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Project Managers through server", "using client side data", null);
			this.store.add(getClientDisplaySalesRepDTOList());
		}

		private List<? extends DisplaySalesRepDTO> getClientDisplaySalesRepDTOList() {
			List<DisplaySalesRepDTO> projectManagers = new ArrayList<DisplaySalesRepDTO>();
			projectManagers.add(new DisplaySalesRepDTO(1L, "Robert", "McIver", null));
			projectManagers.add(new DisplaySalesRepDTO(2L, "Kara", "Johnson", null));
			projectManagers.add(new DisplaySalesRepDTO(3L, "Michaela", "Consunji", null));
			projectManagers.add(new DisplaySalesRepDTO(4L, "Brenda", "Impellezeri", null));
			return null;
		}

		public void onSuccess(List<DisplaySalesRepDTO> result) {
			this.store.add(result);
			for (int i = 0; i < result.size(); i++) {
				projectManagers.put(result.get(i).getDisplaySalesRepId(), result.get(i));
			}
		}
	}

	public static class SetPromoPeriodsStore implements AsyncCallback<List<PromoPeriodDTO>> {
		private ListStore<PromoPeriodDTO> store = new ListStore<PromoPeriodDTO>();

		public SetPromoPeriodsStore(ListStore<PromoPeriodDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Promo Periods through server", "using client side data", null);
			this.store.add(getClientPromoPeriodsDTOList());
		}

		private List<? extends PromoPeriodDTO> getClientPromoPeriodsDTOList() {
			List<PromoPeriodDTO> promoPeriodDTOs = new ArrayList<PromoPeriodDTO>();
			promoPeriodDTOs.add(new PromoPeriodDTO(1L, "Q1-Taxtime/BTB", null, null));
			promoPeriodDTOs.add(new PromoPeriodDTO(2L, "Q2-Father's Day/Mother's", null, null));
			promoPeriodDTOs.add(new PromoPeriodDTO(3L, "Q3-BTS", null, null));
			promoPeriodDTOs.add(new PromoPeriodDTO(4L, "Q4-Fall/Holiday/Falliday", null, null));
			return null;
		}

		public void onSuccess(List<PromoPeriodDTO> promoPeriodDTOList) {
			this.store.add(promoPeriodDTOList);
			for (int i = 0; i < promoPeriodDTOList.size(); i++) {
				promoPeriodMap.put(promoPeriodDTOList.get(i).getPromoPeriodId(), promoPeriodDTOList.get(i));
			}
		}
	}
	
	
	public static class SetPromoPeriodsCountryStore implements AsyncCallback<List<PromoPeriodCountryDTO>> {
		private ListStore<PromoPeriodCountryDTO> store = new ListStore<PromoPeriodCountryDTO>();
		private Long countryId;
		public SetPromoPeriodsCountryStore(Long countryId, ListStore<PromoPeriodCountryDTO> store) {
			this.store = store;
			this.countryId=countryId;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Promo Periods through server", "using client side data", null);
			this.store.add(getClientPromoPeriodsCountryDTOList());
		}

		private List<? extends PromoPeriodCountryDTO> getClientPromoPeriodsCountryDTOList() {
			List<PromoPeriodCountryDTO> promoPeriodCountryDTOs = new ArrayList<PromoPeriodCountryDTO>();
			promoPeriodCountryDTOs.add(new PromoPeriodCountryDTO(1L, "Q1-Taxtime/BTB", null, null));
			promoPeriodCountryDTOs.add(new PromoPeriodCountryDTO(2L, "Q2-Father's Day/Mother's", null, null));
			promoPeriodCountryDTOs.add(new PromoPeriodCountryDTO(3L, "Q3-BTS", null, null));
			promoPeriodCountryDTOs.add(new PromoPeriodCountryDTO(4L, "Q4-Fall/Holiday/Falliday", null, null));
			return null;
		}

		public void onSuccess(List<PromoPeriodCountryDTO> promoPeriodCountryDTOList) {
			/*store.removeAll();
			if( this.countryId==null){
				PromoPeriodCountryDTO allPromoPeriod = new PromoPeriodCountryDTO();
				 allPromoPeriod.setPromoPeriodName("All");
				 promoPeriodCountryDTOList.add(allPromoPeriod);
			}*/
			store.add(promoPeriodCountryDTOList);
			for (int i = 0; i < promoPeriodCountryDTOList.size(); i++) {
					promoPeriodCountryMap.put(promoPeriodCountryDTOList.get(i).getPromoPeriodId(), promoPeriodCountryDTOList.get(i));
			}
			if(this.countryId!=null  ){
				CountryDTO countryDTO=countryMap.get(this.countryId);
				this.store.filter(PromoPeriodCountryDTO.COUNTRY_NAME, countryDTO.getCountryName());
		/*	}else{
				 PromoPeriodCountryDTO allPromoPeriod = new PromoPeriodCountryDTO();
			     allPromoPeriod.setPromoPeriodName("All");
			     promoPeriodCountryDTOList.add(allPromoPeriod);*/
			}
		}
	}
	
	public static class SaveCAScenarioDisplayDTO implements AsyncCallback<DisplayDTO> {
		private DisplayDTO display = new DisplayDTO();
		private Button button = new Button();
		private MessageBox box;
		private CostAnalysisInfo costAnalysisInfo;

		public SaveCAScenarioDisplayDTO(DisplayDTO display, CostAnalysisInfo costAnalysisInfo, Button button, MessageBox box) {
			this.display = display;
			this.button = button;
			if (button != null) button.disable();
			this.box = box;
			this.costAnalysisInfo=costAnalysisInfo;
		}

		public void onFailure(Throwable throwable) {
			if (box != null) box.close();
			MessageBox.alert("Failed to save display, refresh", throwable.toString(), null);
			if (button != null) button.enable();
		}

		public void onSuccess(DisplayDTO displayDTO) {
			this.display.notify(new ChangeEvent(ChangeEventSource.Update, displayDTO));
			this.display = displayDTO;
			costAnalysisInfo.getDisplayTabHeader().setDisplay(displayDTO);
			costAnalysisInfo.getDisplayTabHeader().setData("display", display);
			costAnalysisInfo.getDisplayTabs().setDisplayModel(display);
			if (button != null) button.enable();
			if (box != null) box.close();
		}
	}


	public static class SaveDisplayDTO implements AsyncCallback<DisplayDTO> {
		private DisplayDTO display = new DisplayDTO();
		private DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
		private Button button = new Button();
		private MessageBox box;

		public SaveDisplayDTO(DisplayDTO display, DisplayTabHeaderV2 displayTabHeader, Button button, MessageBox box) {
			this.display = display;
			this.displayTabHeader = displayTabHeader;
			this.button = button;
			if (button != null) button.disable();
			this.box = box;
		}

		public void onFailure(Throwable throwable) {
			if (box != null) box.close();
			MessageBox.alert("Failed to save display, refresh", throwable.toString(), null);
			if (button != null) button.enable();
		}

		public void onSuccess(DisplayDTO displayDTO) {
			this.display.notify(new ChangeEvent(ChangeEventSource.Update, displayDTO));
			this.display = displayDTO;
			displayTabHeader.setDisplay(displayDTO);
			displayTabHeader.setData("display", display);
			if (button != null) button.enable();
			if (box != null) box.close();
		}
	}

	public static class SaveDisplayDTOAndLoadScenario implements AsyncCallback<DisplayDTO> {
		private DisplayDTO display = new DisplayDTO();
		private DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
		private Button button = new Button();
		private MessageBox box;
		private DisplayTabs displayTabs;
		private TabPanel panel;
		private TabItem tabItem;

		public SaveDisplayDTOAndLoadScenario(DisplayDTO display, DisplayTabHeaderV2 displayTabHeader, DisplayTabs displayTabs, Button button, MessageBox box,TabPanel panel, TabItem tabItem) {
			this.display = display;
			this.displayTabHeader = displayTabHeader;
			this.button = button;
			if (button != null) button.disable();
			this.box = box;
			this.displayTabs=displayTabs;
			this.panel=panel;
			this.tabItem=tabItem;
		}

		public void onFailure(Throwable throwable) {
			if (box != null) box.close();
			MessageBox.alert("Failed to save display, refresh", throwable.toString(), null);
			if (button != null) button.enable();
		}

		public void onSuccess(DisplayDTO displayDTO) {
			this.display.notify(new ChangeEvent(ChangeEventSource.Update, displayDTO));
			this.display = displayDTO;
			displayTabHeader.setDisplay(displayDTO);
			displayTabHeader.setData("display", display);
			if (button != null) button.enable();
			if (box != null) box.close();
			displayTabs.setDisplayModel(displayDTO);
			displayTabs.costAnalysisInfo.setData("display", displayDTO);
			/*
			 * Added By Mari
			 * Date :09-16-2011
			 */
			displayTabs.costAnalysisInfo.getDisplayTabs().setData("display", displayDTO);
			displayTabs.costAnalysisInfo.loadScenarios(displayDTO.getDisplayId());
			if (button.getText().equals("Yes") && panel != null && tabItem != null) {
	            panel.setSelection(tabItem);
	        }
		}
	}


	public static class SaveCopiedDisplayDTO implements AsyncCallback<DisplayDTO> {
		private DisplayGeneralInfo dgi;
		private DisplayDTO display = new DisplayDTO();
		private DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
		private Button button = new Button();
		private MessageBox box;

		public SaveCopiedDisplayDTO(DisplayGeneralInfo dgi, DisplayDTO display, DisplayTabHeaderV2 displayTabHeader,
				Button button, MessageBox box) {
			this.dgi = dgi;
			this.display = display;
			this.displayTabHeader = displayTabHeader;
			this.button = button;
			this.button.disable();
			this.box = box;
		}

		public void onFailure(Throwable throwable) {
			box.close();
			MessageBox.alert("Failed to save display, refresh", throwable.toString(), null);
			button.enable();
		}

		public void onSuccess(DisplayDTO displayDTO) {
			this.dgi.setData("display", displayDTO);
			this.dgi.setData("copy", Boolean.FALSE);
			this.dgi.setData("readOnly", Boolean.FALSE);
			this.display.notify(new ChangeEvent(ChangeEventSource.Update, displayDTO));
			this.display = displayDTO;
			button.enable();
			this.dgi.getDisplayTabs().setDisplayModel(displayDTO);
			displayTabHeader.setDisplay(displayDTO);
			displayTabHeader.setData("display", display);
			this.dgi.getRYGStatusFlg().setValue(this.dgi.getRYGStatusStore().findModel(StatusDTO.STATUS_NAME, "G"));
			/*
			 * Allow copied year instead of default
			 */
			//Double currYear=(System.currentTimeMillis()/1000/3600/24/365.25 +1970);
			//this.dgi.getPromoYear().setValue(currYear.longValue());
			this.dgi.checkApprovers(displayDTO);
			this.dgi.getDisplayTabs().reloadCopiedScenarios(displayDTO);
			box.close();
		}
	}

	public static class ReloadDisplay implements AsyncCallback<DisplayDTO> {
		private DisplayDTO display;

		public ReloadDisplay(DisplayDTO display) {
			this.display = display;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get display through server", "make sure display still exists...", null);
		}

		public void onSuccess(DisplayDTO displayDTO) {
			this.display.notify(new ChangeEvent(ChangeEventSource.Update, displayDTO));
			this.display = displayDTO;
		}
	}


	
	public static class LoadDisplay implements AsyncCallback<DisplayDTO> {
		private VerticalPanel vp;
		private Button b;
		private Boolean readOnly;
		private Boolean copy;

		public LoadDisplay(VerticalPanel verticalPanel, Button button, Boolean readOnly, Boolean copy) {
			this.vp = verticalPanel;
			this.b = button;
			this.readOnly = readOnly;
			this.copy = copy;
		}

		public void onFailure(Throwable throwable) {
			b.disable();
			MessageBox.alert("Failed to get display through server", "make sure display still exists...", null);
			b.enable();
			this.vp.clear();
			this.vp.add(new DisplayProjectSelection());
		}

		public void onSuccess(DisplayDTO displayDTO) {
			b.disable();
			this.vp.clear();
			DisplayTabs displayTabs = new DisplayTabs(displayDTO, readOnly, copy);
			this.vp.add(displayTabs);
			b.enable();
		}
	}

	public static class LoadDisplays implements AsyncCallback<List<DisplayDTO>> {
		private ListStore<DisplayDTO> store;
		private MessageBox box;
		StoreFilter<DisplayDTO> oneFilter = new StoreFilter<DisplayDTO>() {
			public boolean select(Store<DisplayDTO> displayDTOStore, DisplayDTO parent, DisplayDTO item, String property) {
				if(property!=null && property.length()>0) App.setFilters(property);
				String[] filters = property.split(SelectionGrid.DELIMITER);
				String customer = filters[0];
				String sku = filters[1];
				String structure = filters[2];
				String manager = filters[3];
				String status = filters[4];
				String promo = filters[5];
				String country = filters[6];

				boolean MANAGER = ( manager.equals(FilterPanel.ALL) || manager.equals(item.getDisplaySalesRepByDisplaySalesRepId().getFullName()) );
				boolean CUSTOMER = ( customer.equals(FilterPanel.ALL) || customer.equals(item.getCustomerByCustomerId().getCustomerName()) );
				boolean STATUS = ( status.equals(FilterPanel.ALL) || status.equals(item.getStatusByStatusId().getStatusName()) );
				boolean STRUCTURE = ( structure.equals(FilterPanel.ALL) || structure.equals(item.getStructureByStructureId().getStructureName()) );
				boolean PROMO = (promo.equals(FilterPanel.ALL) || promo.equals(item.getPromoPeriodByPromoPeriodId().getPromoPeriodName()));
				boolean COUNTRY=false;
				if(country == null || country.equals(FilterPanel.ALL) || country.length() == 0) {
					COUNTRY = true;
				}else if(item.getCountryByCountryId()!=null) {
					COUNTRY = country.equals(item.getCountryByCountryId().getCountryName());
				}
				boolean SKU = false;
				if(sku == null || sku.equals(FilterPanel.ALL) || sku.length() == 0) SKU = true;
				else if (item.getSku()!=null && sku.startsWith("*") && sku.endsWith("*")) SKU =  item.getSku().contains(sku.replace("*", ""));
				else if (item.getSku()!=null && sku.startsWith("*")) SKU = item.getSku().endsWith(sku.replace("*", ""));
				else if (item.getSku()!=null && sku.endsWith("*")) SKU = item.getSku().startsWith(sku.replace("*", ""));
				else if(item.getSku()!=null && sku!=null && sku.length()>0 && !sku.contains("*")) SKU = item.getSku().equals(sku);
				return MANAGER && CUSTOMER && STATUS && STRUCTURE && PROMO && COUNTRY && SKU ;
			}
		};
		public LoadDisplays(ListStore<DisplayDTO> store, MessageBox box) {
			this.store = store;
			this.box = box;
		}

		public void onFailure(Throwable throwable) {
			box.close();
			MessageBox.alert("Failed to get displays through server", "try again later...", null);
		}

		public void onSuccess(List<DisplayDTO> displays) {
			if(store.getFilters()!=null&&store.getFilters().size()>0) store.getFilters().removeAll(store.getFilters());
			store.removeAll();
			store.add(displays);
			store.addFilter(oneFilter);
			if(App.getFilters() != null && App.getFilters().length() > 0) store.applyFilters(App.getFilters());
			box.close();
		}
	}


	public static class SaveMassUpdateRecords implements AsyncCallback<List<DisplayForMassUpdateDTO>> {
		private ListStore<DisplayForMassUpdateDTO> store;
		DisplayForMassUpdateDTO displayMassUpdateDTO;
		private MessageBox box;
		private Button btnApplyUpdates;

		public SaveMassUpdateRecords(ListStore<DisplayForMassUpdateDTO> store,  Button btnApplyUpdates, MessageBox box){
			this.store = store;
			this.box = box;
			this.btnApplyUpdates=btnApplyUpdates;
			this.btnApplyUpdates.disable();
		}

		public void onFailure(Throwable throwable) {
			box.close();
			String errorMsg = "";
			StackTraceElement[] stack = throwable.getStackTrace();
			for (int i = 0; i < stack.length; i++) {
				errorMsg += stack[i].toString();  
				errorMsg+="\n";
			}
			System.out.println("Service.SaveMassUpdateRecords.onFailure ErrorMsg="+errorMsg);
			MessageBox.alert("Failed to save displays for mass update through server", "try again later...", null);
			btnApplyUpdates.enable();
		}

		private final Listener<MessageBoxEvent> massUpdateListener = new Listener<MessageBoxEvent>() {
			public void handleEvent(MessageBoxEvent mbe) {
				Button btn = mbe.getButtonClicked();
				App.getVp().clear();
				App.getVp().add(new DisplayProjectSelection());
			}
		};

		public void onSuccess(List<DisplayForMassUpdateDTO> displays) {
			box.close();
			btnApplyUpdates.enable();
			String title = "Mass Update Complete";
			String msg = "The mass update process is complete. Press OK to return to selection screen"; 	
			MessageBox.alert(title, msg, massUpdateListener);
		}
	}



	public static class LoadSelectedDisplaysForMassUpdate implements AsyncCallback<List<DisplayForMassUpdateDTO>> {
		private ListStore<DisplayForMassUpdateDTO> store;
		private MessageBox box;


		public LoadSelectedDisplaysForMassUpdate(ListStore<DisplayForMassUpdateDTO> store, MessageBox box) {
			this.store = store;
			this.box = box;
		}

		public void onFailure(Throwable throwable) {
			box.close();
			MessageBox.alert("Failed to get displays for mass update through server", "try again later...", null);
		}

		public void onSuccess(List<DisplayForMassUpdateDTO> displays) {
			store.removeAll();
			store.add(displays);
			store.commitChanges();
			box.close();
		}
	}


	public static class MaintainFilterSelections{
		private CustomerDTO selectedCustomer;

		public CustomerDTO getSelectedCustomer() {
			return selectedCustomer;
		}

		public void setSelectedCustomer(CustomerDTO selectedCustomer) {
			this.selectedCustomer = selectedCustomer;
		}
	}

	public static class LoadCustomerMarketing implements AsyncCallback<CustomerMarketingDTO> {
		private CustomerMarketingInfo cmi;

		public LoadCustomerMarketing(CustomerMarketingInfo customerMarketingInfo) {
			this.cmi = customerMarketingInfo;
		}

		public void onFailure(Throwable caught) {
			MessageBox.alert("Failed to get display through server", "make sure display still exists...", null);
		}

		public void onSuccess(CustomerMarketingDTO customerMarketing) {
			if (customerMarketing != null && customerMarketing.getCustomerMarketingId() != null) {
				cmi.setData("customerMarketing", customerMarketing);
			}
		}
	}

	public static class ReloadCustomerMarketing implements AsyncCallback<CustomerMarketingDTO> {
		private CustomerMarketingDTO cmDTO;

		public ReloadCustomerMarketing(CustomerMarketingDTO customerMarketingDTO) {
			this.cmDTO = customerMarketingDTO;
		}

		public void onFailure(Throwable caught) {
			MessageBox.alert("Failed to get customer marketing through server", "make sure display still exists...", null);
		}

		public void onSuccess(CustomerMarketingDTO customerMarketing) {
			if (customerMarketing != null && customerMarketing.getCustomerMarketingId() != null) {
				this.cmDTO.notify(new ChangeEvent(ChangeEventSource.Update, customerMarketing));
				this.cmDTO = customerMarketing;
			}
		}

	}

	public static class SaveMarketingInfoDTO implements AsyncCallback<CustomerMarketingDTO> {
		private CustomerMarketingInfo cmi;
		private CustomerMarketingDTO customer;// = new CustomerMarketingDTO();
		private Button button = new Button();
		private MessageBox box;

		public SaveMarketingInfoDTO(CustomerMarketingInfo customerMarketingInfo,
				CustomerMarketingDTO customerMarketignDTO, Button button, MessageBox box) {
			this.cmi = customerMarketingInfo;
			this.customer = customerMarketignDTO;
			this.button = button;
			this.button.disable();
			this.box = box;
		}

		public void onFailure(Throwable caught) {
			this.button.enable();
			box.close();
			MessageBox.alert("Failed to save customer marketing", customer.toString(), null);
		}

		public void onSuccess(CustomerMarketingDTO customerMarketingDTO) {
			this.customer.notify(new ChangeEvent(ChangeEventSource.Update, customerMarketingDTO));
			this.cmi.setData("customerMarketing", customerMarketingDTO);
			this.customer = customerMarketingDTO;
			button.enable();
			box.close();
		}
	}

	public static class LoadDisplayLogistics implements AsyncCallback<DisplayLogisticsDTO> {
		private DisplayLogisticsInfo dli;

		public LoadDisplayLogistics(DisplayLogisticsInfo displayLogisticsInfo) {
			this.dli = displayLogisticsInfo;
		}

		public void onFailure(Throwable caught) {
			MessageBox.alert("Failed to get display logistics through server", "make sure display still exists...", null);
		}

		public void onSuccess(DisplayLogisticsDTO displayLogistics) {
			this.dli.setData("displayLogistics", displayLogistics);
		}
	}

	public static class ReloadDisplayLogistics implements AsyncCallback<DisplayLogisticsDTO> {
		private DisplayLogisticsDTO dl;

		public ReloadDisplayLogistics(DisplayLogisticsDTO displayLogistics) {
			this.dl = displayLogistics;
		}

		public void onFailure(Throwable caught) {
			MessageBox.alert("Failed to get display logistics through server", "make sure display still exists...refresh", null);
		}

		public void onSuccess(DisplayLogisticsDTO displayLogistics) {
			if (displayLogistics != null && displayLogistics.getDisplayLogisticsId() != null) {
				this.dl.notify(new ChangeEvent(ChangeEventSource.Update, displayLogistics));
				this.dl = displayLogistics;
			}
		}
	}

	public static class SaveDisplayLogisticsInfoDTO implements AsyncCallback<DisplayLogisticsDTO> {
		private DisplayLogisticsInfo dli;
		private DisplayLogisticsDTO dl;
		private Button button = new Button();
		private MessageBox box;

		public SaveDisplayLogisticsInfoDTO(DisplayLogisticsInfo displayLogisticsInfo, DisplayLogisticsDTO displayLogisticsDTO, Button button, MessageBox box) {
			this.dli = displayLogisticsInfo;
			this.dl = displayLogisticsDTO;
			this.button = button;
			this.button.disable();
			this.box = box;
		}

		public void onFailure(Throwable caught) {
			box.close();
			button.enable();
			MessageBox.alert("Failed to save display logistics through server", "make sure display still exists...refresh", null);
		}

		public void onSuccess(DisplayLogisticsDTO displayLogisticsDTO) {
			if (displayLogisticsDTO != null && displayLogisticsDTO.getDisplayLogisticsId() != null) {
				this.dli.setData("displayLogistics", displayLogisticsDTO);
				dl.notify(new ChangeEvent(ChangeEventSource.Update, displayLogisticsDTO));
			}
			button.enable();
			box.close();
		}
	}

	public static class LoadKitComponents implements AsyncCallback<List<KitComponentDTO>> {
		private ListStore<KitComponentDTO> store;

		public LoadKitComponents(ListStore<KitComponentDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get displays through server", "try again later...", null);
		}

		public void onSuccess(List<KitComponentDTO> kitComponents) {
			if (kitComponents != null) {
				store.removeAll();
				store.commitChanges();
				store.add(kitComponents);
				store.commitChanges();
			}else{
				store.removeAll();
			}
		}
	}

	public static class DeleteKitComponents implements AsyncCallback<Boolean> {
		private ListStore<KitComponentDTO> store;
		private KitComponentDTO kitComponent;
		private Button button;

		public DeleteKitComponents(KitComponentDTO kitComponent, ListStore<KitComponentDTO> store, Button button) {
			this.store = store;
			this.button = button;
			this.kitComponent = kitComponent;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to delete kit component through server", "try again later...", null);
		}

		public void onSuccess(Boolean result) {
			System.out.println("Service: DeleteKitComponents");
			store.remove(kitComponent);
			button.enable();
		}
	}

	public static class LoadKitComponent implements AsyncCallback<KitComponentDTO> {
		private DisplayDTO displayDTO;
		private DisplayScenarioDTO displayScenarioDTO;
		private List<Long> ids;
		private Long selectedIdx;
		private VerticalPanel vp;
		private Button b;
		private Boolean readOnly;
		private MessageBox box;
		private Date peReCalcDate;
		private Boolean sourceFromCostAnalysis;

		public LoadKitComponent(DisplayDTO displayDTO, DisplayScenarioDTO displayScenarioDTO, List<Long> ids, Long selectedIdx, VerticalPanel verticalPanel, Button button, Boolean readOnly, MessageBox box,Boolean sourceFromCostAnalysis,Date peReCalcDate ) {
			this.displayDTO = displayDTO;
			this.displayScenarioDTO=displayScenarioDTO;
			this.ids = ids;
			this.selectedIdx = selectedIdx;
			this.vp = verticalPanel;
			this.b = button;
			this.readOnly = readOnly;
			this.box = box;
			this.peReCalcDate=peReCalcDate;
			this.sourceFromCostAnalysis=sourceFromCostAnalysis;
		}

		public void onFailure(Throwable throwable) {
			box.close();
			b.disable();
			MessageBox.alert("Failed to get kit component through server", "make sure kit component still exists...", null);
			b.enable();
			this.vp.clear();
			this.vp.add(new DisplayProjectSelection());
		}

		public void onSuccess(KitComponentDTO kitComponentDTO) {
			b.disable();
			this.vp.clear();
			try{
				//MessageBox.alert("Service:LoadKitComponent", "displayDTO.country="+displayDTO.getCountryByCountryId().getCountryName(), null);
			KitComponentDetail kitComponentDetail = new KitComponentDetail(kitComponentDTO, displayDTO, displayScenarioDTO,ids, selectedIdx, readOnly,sourceFromCostAnalysis,peReCalcDate/*,null*/);
			Boolean bDisplayReadOnly;
			if(readOnly==null){
				bDisplayReadOnly=Boolean.FALSE;
			}else{ 
				bDisplayReadOnly=readOnly;
			}
			if(bDisplayReadOnly){
				kitComponentDetail.overrideDirty();
			}
			this.vp.add(kitComponentDetail);
			b.enable();
			box.close();
			// hack to set radio buttons on detail page
			if (  (!displayDTO.getDisplaySetupId().equals(KitComponentDetail.DISPLAY_SETUP_FG_NEW_UPC)) && (kitComponentDTO.getBreakCase() == null || kitComponentDTO.getBreakCase().equals(KitComponentDetail.PRODUCTION_VERSION))){
				kitComponentDetail.getRadio1().setValue(true);
			}else if (kitComponentDTO.getBreakCase().equals(KitComponentDetail.BREAK_CASE)){
				kitComponentDetail.getRadio2().setValue(true);
			}else if (kitComponentDTO.getBreakCase().equals(KitComponentDetail.PLANT_BREAK_CASE)){
				kitComponentDetail.getRadio3().setValue(true);
			}
			}catch(Exception ex){
				System.out.println("Service: LoadKitComponent:onSuccess error.... :" + ex.toString());
			}
		}
	}

	public static class SaveKitComponentDTO implements AsyncCallback<KitComponentDTO> {
		private KitComponentDTO kc;
		private Button button = new Button();
		private KitComponentInfoPlannerGrid plannerGrid;
		private MessageBox box;
		//private DisplayScenarioDTO displayScenarioDTO;
		//private CostAnalysisInfo costAnalysisInfo;

		public SaveKitComponentDTO(KitComponentDTO kitComponentDTO, MessageBox box, KitComponentInfoPlannerGrid plannerGrid, Button button/*, DisplayScenarioDTO displayScenarioDTO, CostAnalysisInfo costAnalysisInfo*/) {
			this.kc = kitComponentDTO;
			this.plannerGrid = plannerGrid;
			this.button = button;
			this.button.disable();
			this.box = box;
			//this.displayScenarioDTO=displayScenarioDTO;
			//this.costAnalysisInfo=costAnalysisInfo;
		}

		public void onFailure(Throwable caught) {
			box.close();
			MessageBox.alert("Failed to save kit component", "", null);
		}

		public void onSuccess(KitComponentDTO result) {
			kc.notify(new ChangeEvent(ChangeEventSource.Update, result));
			kc = result;
			plannerGrid.reload(result.getKitComponentId());
			button.enable();
			box.close();
		}
	}


	public static class SaveKitComponentDTOList implements AsyncCallback<List<KitComponentDTO>> {
		private KitComponentInfo kitComponentInfo;
		private Button button = new Button();
		private MessageBox box;
		EditorGrid<KitComponentDTO> editorGrid;
		private TabPanel panel;
		TabItem tabItem;

		public SaveKitComponentDTOList(KitComponentInfo kitComponentInfo, EditorGrid<KitComponentDTO> editorGrid, MessageBox box, Button button, TabPanel panel, TabItem tabItem) {
			this.kitComponentInfo = kitComponentInfo;
			this.box = box;
			this.button = button;
			this.editorGrid=editorGrid;
			this.button.disable();
			this.panel=panel;
			this.tabItem=tabItem;
		}

		public void onFailure(Throwable throwable) {
			box.close();
		}

		public void onSuccess(List<KitComponentDTO> kitComponentDTOList) {
			editorGrid.getStore().removeAll();
			editorGrid.getStore().commitChanges();
			editorGrid.getStore().add(kitComponentDTOList);
			editorGrid.getStore().commitChanges();
			button.disable();
			box.close();
			if ( panel != null && tabItem != null) {
				panel.setSelection(tabItem);
			}
		}
	}


	public static class SwitchOnDisplayTypesInKitComponent implements AsyncCallback<List<KitComponentDTO>>{
		private Button button = new Button();
		private MessageBox box;
		public SwitchOnDisplayTypesInKitComponent(Button button, MessageBox box) {
			this.button = button;
			if (button != null) button.disable();
			this.box = box;
		}
		public void onFailure(Throwable arg0) {
			this.button.enable();
		}

		public void onSuccess(List<KitComponentDTO> kitCOmponentDTOList) {
			if (button != null) button.enable();
			if (box != null) box.close();
		}
	}

	public static class LoadDisplayPlanners implements AsyncCallback<List<DisplayPlannerDTO>> {
		private ListStore<DisplayPlannerDTO> store;

		public LoadDisplayPlanners(ListStore<DisplayPlannerDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get displays through server", "try again later...", null);
		}

		public void onSuccess(List<DisplayPlannerDTO> displayPlanners) {
			store.removeAll();
			store.commitChanges();
			store.add(displayPlanners);
			store.commitChanges();
		}
	}

	public static class UpdateDisplayPlanner implements AsyncCallback<DisplayPlannerDTO> {
		EditorGrid<DisplayPlannerDTO> grid;
		private MessageBox box;
		private int row;

		public UpdateDisplayPlanner(EditorGrid<DisplayPlannerDTO> grid, int selectedRow, MessageBox box) {
			this.grid = grid;
			this.box = box;
			this.row = selectedRow;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get displays through server", "try again later...", null);
			box.close();
		}

		public void onSuccess(DisplayPlannerDTO displayPlanner) {
			System.out.println("Service: got display planner info");            
			if (displayPlanner != null) {
				System.out.println("Service: display planner info was not null");
				grid.getStore().getModels().get(row).setPlannerCode(displayPlanner.getPlannerCode());
				grid.getStore().getModels().get(row).setPlantCode(displayPlanner.getPlantCode());    			    			
			}
			else {
				System.out.println("Service: display planner info was null");
				grid.getStore().getModels().get(row).setPlannerCode(null);
				grid.getStore().getModels().get(row).setPlantCode(null);    			
			}
			System.out.println("Service: got display planner info. commit changes");
			grid.getStore().commitChanges();
			System.out.println("Service: got display planner info. commit changes done");
			grid.getView().refresh(true);
			System.out.println("Service: got display planner info. refresh done");
			box.close();
		}
	}

	public static class LoadKitComponentCost implements AsyncCallback<List<CostPriceDTO>> {
		private ListStore<CostPriceDTO> store;

		public LoadKitComponentCost(ListStore<CostPriceDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get product detail through server", "try again later...", null);
		}

		public void onSuccess(List<CostPriceDTO> costPrice) {
			store.removeAll();
			store.commitChanges();
			store.add(costPrice);
			store.commitChanges();
		}
	}

	public static class LoadProductDetailForFGSKU implements AsyncCallback<List<CostPriceDTO>> {
		Grid<CostPriceDTO> grid;

		public LoadProductDetailForFGSKU( Grid<CostPriceDTO> grid) {
			this.grid=grid;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get LoadProductDetailForFGSKU through server", "try again later...", null);
		}

		public void onSuccess(List<CostPriceDTO> costPrice) {
			System.out.println("Service: LoadProductDetailForFGSKU info. commit changes");
			if (costPrice != null  ) {
				grid.getStore().removeAll();
				grid.getStore().commitChanges();
				grid.getStore().add(costPrice);
				System.out.println("Service: LoadProductDetailForFGSKU info. commit changes done");
				grid.getView().refresh(true);
			}else{
				grid.getStore().removeAll();
			}
		}
	}
	public static class LoadLocationStore implements AsyncCallback<List<LocationDTO>> {
		private ListStore<LocationDTO> store;

		public LoadLocationStore(ListStore<LocationDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get active locations through server", "try again later...", null);
		}

		public void onSuccess(List<LocationDTO> locations) {
			store.add(locations);
		}
	}
	public static class LoadLocationCountryStore implements AsyncCallback<List<LocationCountryDTO>> {
		private ListStore<LocationCountryDTO> store = new ListStore<LocationCountryDTO>();
		private Long countryId;
		public LoadLocationCountryStore(Long countryId, ListStore<LocationCountryDTO> store) {
			this.store = store;
			this.countryId=countryId;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get active locationsCountry through server", "try again later...", null);
		}

		public void onSuccess(List<LocationCountryDTO> locations) {
			//this.store.removeAll();
			this.store.add(locations);
			if(this.countryId!=null  ){
				CountryDTO countryDTO=countryMap.get(this.countryId);
				this.store.filter(LocationCountryDTO.COUNTRY_NAME, countryDTO.getCountryName());
			}
		
		}
	}
	
	public static class LoadLocationList implements AsyncCallback<List<LocationDTO>> {
		private PackoutDetailsTree packoutDetailsTree;

		public LoadLocationList(PackoutDetailsTree packoutDetailsTree) {
			this.packoutDetailsTree = packoutDetailsTree;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get active locations through server", "try again later...", null);
		}

		public void onSuccess(List<LocationDTO> list) {
			this.packoutDetailsTree.setLocationList(list);
			Map<Long, LocationDTO> locationMap = new HashMap<Long, LocationDTO>();
			for (int i = 0; i < list.size(); i++) {
				locationMap.put(list.get(i).getLocationId(), list.get(i));
			}
			this.packoutDetailsTree.setLocationMap(locationMap);
		}
	}

	public static class LoadDisplayDcDTO implements AsyncCallback<Map<Long, DisplayDcDTO>> {
		private PackoutDetailInfo packoutDetailInfo;
		private Map<Long, DisplayDcDTO> dcMap = new HashMap<Long, DisplayDcDTO>();

		public LoadDisplayDcDTO(PackoutDetailInfo packoutDetailInfo, Map<Long, DisplayDcDTO> dcMap) {
			this.packoutDetailInfo = packoutDetailInfo;
			this.dcMap = dcMap;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get distribution centers through server", "", null);
		}

		public void onSuccess(Map<Long, DisplayDcDTO> resultDcMap) {
			this.dcMap = resultDcMap;
			packoutDetailInfo.setDcMap(resultDcMap);
			packoutDetailInfo.packoutDetailsTree.loadDCNodes();
			packoutDetailInfo.updatePackoutDetailsFieldSetHeading();
		}
	}

	public static class SaveDisplayDcDTOList implements AsyncCallback<Map<Long, DisplayDcDTO>> {
		private Map<Long, DisplayDcDTO> dcMap = new HashMap<Long, DisplayDcDTO>();
		private MessageBox box = new MessageBox();
		private PackoutDetailInfo packoutDetailInfo;

		public SaveDisplayDcDTOList(PackoutDetailInfo packoutDetailInfo, Map<Long, DisplayDcDTO> dcMap, MessageBox box) {
			this.packoutDetailInfo = packoutDetailInfo;
			this.dcMap = dcMap;
			this.box = box;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save display DC data", throwable.toString(), null);
			box.close();
		}

		public void onSuccess(Map<Long, DisplayDcDTO> resultDcMap) {
			this.dcMap = resultDcMap;
			this.packoutDetailInfo.setDcMap(resultDcMap);
			this.packoutDetailInfo.packoutDetailsTree.loadDCNodes();
			this.packoutDetailInfo.updatePackoutDetailsFieldSetHeading();
			//Info.display("Successfully saved display DC data", "");
			box.close();
		}
	}
	public static class SaveDisplayPackoutDTOList implements AsyncCallback<List<DisplayPackoutDTO>> {
		private PackoutDetailsTreeItemGrid grid;
		private MessageBox box;

		public SaveDisplayPackoutDTOList(PackoutDetailsTreeItemGrid grid, MessageBox box) {
			this.grid = grid;
			this.box = box;
		}

		public void onFailure(Throwable throwable) {
			box.close();
			MessageBox.alert("Failed to save display packout data", throwable.toString(), null);
		}

		public void onSuccess(List<DisplayPackoutDTO> displayPackoutList) {
			this.grid.getEditorGrid().stopEditing();
			this.grid.getEditorGrid().getStore().removeAll();
			this.grid.getEditorGrid().getStore().commitChanges();
			this.grid.getEditorGrid().getStore().add(displayPackoutList);
			this.grid.getEditorGrid().getStore().commitChanges();
			box.close();
		}
	}

	public static class LoadDisplayPackoutDTOList implements AsyncCallback<List<DisplayPackoutDTO>> {
		private PackoutDetailsTreeItemGrid grid;

		public LoadDisplayPackoutDTOList(PackoutDetailsTreeItemGrid grid) {
			this.grid = grid;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to load display packouts", throwable.toString(), null);
		}

		public void onSuccess(List<DisplayPackoutDTO> displayPackoutList) {
			//Info.display("Successfully loaded display packouts", displayPackoutList.size() + "");
			this.grid.getEditorGrid().stopEditing();
			this.grid.getEditorGrid().getStore().removeAll();
			this.grid.getEditorGrid().getStore().commitChanges();
			this.grid.getEditorGrid().getStore().add(displayPackoutList);
			this.grid.getEditorGrid().getStore().commitChanges();
		}
	}

	public static class LoadDisplayDcAndPackoutList implements AsyncCallback<List<DisplayDcAndPackoutDTO>> {
		private TreeStore<ModelData> store;
		private PackoutDetailInfo packoutDetailInfo;
		private PackoutDetailTreeGrid packoutDetail;

		public LoadDisplayDcAndPackoutList(TreeStore<ModelData> store, PackoutDetailTreeGrid packoutDetail, PackoutDetailInfo packoutDetailInfo) {
			this.store = store;
			this.packoutDetail = packoutDetail;
			this.packoutDetailInfo = packoutDetailInfo;
		}

		public void onFailure(Throwable throwable) {
			System.out.println("LoadDisplayDcAndPackoutList(): failure: " + throwable.getMessage());
			String errorMsg = "";
			StackTraceElement[] stack = throwable.getStackTrace();
			for (int i = 0; i < stack.length; i++) {
				errorMsg += stack[i].toString();    		
			}
			MessageBox.alert("Failed to get display dcs and packouts through server", "try again later...", null);
		}

		public void onSuccess(List<DisplayDcAndPackoutDTO> displayDcAndPackouts) {
			System.out.println("Service: display dc and packouts list size=" + displayDcAndPackouts.size());
			packoutDetail.setDisplayDcAndPackoutList(displayDcAndPackouts);

			// build tree model

			Folder root = new Folder("root", 0L, "", 0L);
			Folder node = null;
			try{
				Iterator<DisplayDcAndPackoutDTO>iter = displayDcAndPackouts.iterator();
				while (iter.hasNext()) {
					DisplayDcAndPackoutDTO dap = (DisplayDcAndPackoutDTO)iter.next();
					if (dap.getType().equals("DC")) {
						node = new Folder(dap.getDcCode(), dap.getDcQuantity(), dap.getRowId(), dap.getLocationId());
						root.add((Folder) node);
					}
					else if (dap.getType().equals("Packout")) {    	
						node.add(dap);
					}
				}
			}catch(Exception ex){
				System.out.println("Service: LoadDisplayDcAndPackoutList:onSuccess error.... :" + ex.toString());
			}
			System.out.println("Service: Folder info " + root.getChildCount());
			store.removeAll();
			store.commitChanges();
			store.add(root.getChildren(), true);
			store.commitChanges();

			packoutDetail.updateTreeData();
			packoutDetailInfo.updatePackoutDetailsFieldSetHeading();
		}
	}

	public static class LoadSlimDisplayPackoutGrid implements AsyncCallback<List<SlimPackoutDTO>> {
		SlimDisplayPackoutGrid grid;

		public LoadSlimDisplayPackoutGrid(SlimDisplayPackoutGrid grid) {
			this.grid = grid;
		}

		public void onFailure(Throwable throwable) {
			//MessageBox.alert("Failed to load slim packouts", throwable.toString(), null);
		}

		public void onSuccess(List<SlimPackoutDTO> displayPackoutList) {
			this.grid.getStore().removeAll();
			this.grid.getStore().commitChanges();
			this.grid.getStore().add(displayPackoutList);
			this.grid.getStore().commitChanges();
		}
	}

	public static class SaveDisplayShippingWaveDTOList implements AsyncCallback<List<DisplayShipWaveDTO>> {
		private PackoutDetailInfo packoutDetailInfo;
		private MessageBox box;

		public SaveDisplayShippingWaveDTOList(PackoutDetailInfo packoutDetailInfo, MessageBox box) {
			this.packoutDetailInfo = packoutDetailInfo;
			this.box = box;
		}

		public void onFailure(Throwable throwable) {
			box.close();
			MessageBox.alert("Failed to save display shipping wave data", throwable.toString(), null);
		}

		public void onSuccess(List<DisplayShipWaveDTO> displayShipWaveDTOList) {
			this.packoutDetailInfo.getPackoutDetailInfoShippingWavesGrid().getEditorGrid().getStore().removeAll();
			this.packoutDetailInfo.getPackoutDetailInfoShippingWavesGrid().getEditorGrid().getStore().commitChanges();
			this.packoutDetailInfo.getPackoutDetailInfoShippingWavesGrid().getEditorGrid().getStore().add(displayShipWaveDTOList);
			this.packoutDetailInfo.getPackoutDetailInfoShippingWavesGrid().getEditorGrid().getStore().commitChanges();
			this.packoutDetailInfo.updateShippingWavesFieldSetHeading();
			box.close();
		}
	}

	public static class DeleteDisplayShipWaveDTOList implements AsyncCallback<Boolean> {
		private ListStore<DisplayShipWaveDTO> store;
		private PackoutDetailInfoGrid packoutDisplayInfoGrid;
		private DisplayShipWaveDTO displayShipWaveDTO;
		private MessageBox box;

		public DeleteDisplayShipWaveDTOList(PackoutDetailInfoGrid packoutDisplayInfoGrid, ListStore<DisplayShipWaveDTO> store,  DisplayShipWaveDTO displayShipWaveDTO, MessageBox box) {
			this.store = store;
			this.box = box;
			this.displayShipWaveDTO=displayShipWaveDTO;
			this.packoutDisplayInfoGrid = packoutDisplayInfoGrid;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to delete dipslay shipwave through server", "try again later...", null);
		}

		public void onSuccess(Boolean result) {
			System.out.println("DeleteDisplayShipWaveDTOList: onSuccess Ship wave delete");
			this.packoutDisplayInfoGrid.getStore().remove(displayShipWaveDTO);
			this.packoutDisplayInfoGrid.getStore().commitChanges();
			// MessageBox.alert("Delete", "Ship wave is deleted from the list", null);
			box.close();
		}
	}

	public static class LoadDisplayShippingWaveDTOList implements AsyncCallback<List<DisplayShipWaveDTO>> {
		private PackoutDetailInfo packoutDetailInfo;

		public LoadDisplayShippingWaveDTOList(PackoutDetailInfo packoutDetailInfo) {
			this.packoutDetailInfo = packoutDetailInfo;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to delete shipping wave data", throwable.toString(), null);
		}

		public void onSuccess(List<DisplayShipWaveDTO> displayShipWaveDTOList) {
			this.packoutDetailInfo.packoutDetailInfoShippingWavesGrid.getStore().removeAll();
			this.packoutDetailInfo.packoutDetailInfoShippingWavesGrid.getStore().commitChanges();
			this.packoutDetailInfo.packoutDetailInfoShippingWavesGrid.getStore().add(displayShipWaveDTOList);
			this.packoutDetailInfo.packoutDetailInfoShippingWavesGrid.getStore().commitChanges();
			this.packoutDetailInfo.updateShippingWavesFieldSetHeading();
		}
	}

	public static class SavePackoutData implements AsyncCallback<DisplayDTO> {
		private PackoutDetailInfo packoutDetailInfo;
		private DisplayDTO display = new DisplayDTO();
		private DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
		private MessageBox box;
		private Button btn;
		private TabPanel panel;
		private TabItem tabItem;

		public SavePackoutData(DisplayDTO display, Button btn, DisplayTabHeaderV2 displayTabHeader, PackoutDetailInfo packoutDetailInfo, MessageBox box,TabPanel panel, TabItem tabItem) {
			System.out.println("Service.savePackoutData(): constructor");
			this.packoutDetailInfo = packoutDetailInfo;
			this.display = display;
			this.displayTabHeader = displayTabHeader;
			this.box = box;
			this.panel=panel;
			this.tabItem=tabItem;
			this.btn=btn;
			if (btn != null) btn.disable();
			System.out.println("Service.savePackoutData(): constructor all setup");
		}

		public void onFailure(Throwable throwable) {
			box.close();

			System.out.println("Service.savePackoutData(): failure: " + throwable.getMessage());
			StackTraceElement[] stack = throwable.getStackTrace();
			if (btn != null) btn.enable();
			for (int i = 0; i < stack.length; i++) {
				System.out.println(stack[i].toString());    		
			}
			MessageBox.alert("Failed to save packout, refresh", throwable.toString(), null);
		}

		public void onSuccess(DisplayDTO displayDTO) {
			this.display.notify(new ChangeEvent(ChangeEventSource.Update, displayDTO));
			System.out.println("Service.savePackoutData(): success");
			this.display = displayDTO;
			displayTabHeader.setDisplay(displayDTO);
			displayTabHeader.setData("display", displayDTO);
			packoutDetailInfo.packoutDetailsTree.reload();
			this.packoutDetailInfo.getDisplayTabs().setDisplayModel(displayDTO);
			packoutDetailInfo.packoutDetailInfoShippingWavesGrid.reload();
			if (btn != null) btn.enable();
			if ( panel != null && tabItem != null) {
				System.out.println("Panel & TabItem is not NULL");
				System.out.println(" TabItem Title : " + tabItem.getText() + " Title : "+ tabItem.getTitle());
				panel.setSelection(tabItem);
			}else{
				System.out.println("Panel & TabItem is NULL");

			}
			box.close();
		}
	}

	public static class SetValidPlantStore implements AsyncCallback<List<ValidPlantDTO>> {
		private ListStore<ValidPlantDTO> store = new ListStore<ValidPlantDTO>();

		public SetValidPlantStore(ListStore<ValidPlantDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Plant through server", "", null);
		}

		public void onSuccess(List<ValidPlantDTO> result) {
			this.store.add(result);
		}
	}

	public static class SetValidPlannerStore implements AsyncCallback<List<ValidPlannerDTO>> {
		private ListStore<ValidPlannerDTO> store = new ListStore<ValidPlannerDTO>();

		public SetValidPlannerStore(ListStore<ValidPlannerDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Planner through server", "", null);
		}

		public void onSuccess(List<ValidPlannerDTO> result) {
			this.store.add(result);
		}
	}

	public static class SetValidLocationStore implements AsyncCallback<List<ValidLocationDTO>> {
		private ListStore<ValidLocationDTO> store = new ListStore<ValidLocationDTO>();

		public SetValidLocationStore(ListStore<ValidLocationDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Location through server", "", null);
		}

		public void onSuccess(List<ValidLocationDTO> result) {
			this.store.add(result);
		}
	}

	public static class LoadProductDetail implements AsyncCallback<List<ProductDetailDTO>> {
		private ListStore<ProductDetailDTO> store;
		private MessageBox box;

		public LoadProductDetail(ListStore<ProductDetailDTO> store, MessageBox box) {
			this.store = store;
			this.box = box;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get product detail through server", "try again later...", null);
			box.close();
		}

		public void onSuccess(List<ProductDetailDTO> productDetails) {
			store.removeAll();
			if (productDetails != null) store.add(productDetails);
			box.close();
			if (productDetails == null)
				MessageBox.alert("Search Results", "No items found.", null);
		}
	}

	public static class SetValidBuStore implements AsyncCallback<List<ValidBuDTO>> {
		private ListStore<ValidBuDTO> store = new ListStore<ValidBuDTO>();

		public SetValidBuStore(ListStore<ValidBuDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get BU through server", "", null);
		}

		public void onSuccess(List<ValidBuDTO> result) {
			if (result == null)
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>  Service.SetValidBuStore(): result is null");
			if (store == null)
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>  Service.SetValidBuStore(): store is null");
			this.store.add(result);
		}
	}

	public static class LoadUserInfo implements AsyncCallback<UserDTO> {

		@Override
		public void onFailure(Throwable caught) {
			MessageBox.alert("DMM About", "An error has occured", null);
		}

		@Override
		public void onSuccess(UserDTO result) {
			App.setUser(result);
			App.loadApp();
		}
	}

	public static class IsDuplicateSKU implements AsyncCallback<BooleanDTO>{
		private DisplayGeneralInfo dgi;
		public IsDuplicateSKU(DisplayGeneralInfo dgi){
			this.dgi = dgi;
		}
		private final Listener<MessageBoxEvent> skuListener = new Listener<MessageBoxEvent>() {
			public void handleEvent(MessageBoxEvent mbe) {
				Button btn = mbe.getButtonClicked();
				if (btn.getText().equals("Yes")) {
					dgi.setSkuFocus();
				}
				if (btn.getText().equals("No")) {
					dgi.save(btn, null, null);
				}
			}
		};
		public void onFailure(Throwable caught){
			//todo
		}
		public void onSuccess(BooleanDTO isDuplicate){
			this.dgi.setDuplicateSKU(isDuplicate);
			if(isDuplicate.getValue()){
				String title = "Duplicate Sku?";
				String msg = "The SKU you have entered already exists, do you wish to change it?"; 		           			
				MessageBox box = new MessageBox();
				box.setButtons(MessageBox.YESNO);
				box.setIcon(MessageBox.QUESTION);
				box.setTitle("Save Changes?");
				box.addCallback(skuListener);
				box.setTitle(title);
				box.setMessage(msg);
				box.show();
			}else{
				dgi.save(dgi.getActionButton(), null, null);
			}
		}
	}

	public static class LoadManufacturerStore implements AsyncCallback<List<ManufacturerCountryDTO>> {
		private DisplayGeneralInfo dgi;
		public LoadManufacturerStore(DisplayGeneralInfo dgi) {
			this.dgi = dgi;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get manufacturers through server", "try again later...", null);
		}

		public void onSuccess(List<ManufacturerCountryDTO> manufacturersCountry) {
			this.dgi.getManufacturersCountryStore().add(manufacturersCountry);
			if(this.dgi.getDisplayModel().getCountryId()!=null){
				CountryDTO countryDTO=countryMap.get(this.dgi.getDisplayModel().getCustomerId());
				this.dgi.getManufacturersCountryStore().filter(ManufacturerCountryDTO.COUNTRY_NAME, countryDTO.getCountryName());
			}
        	if(this.dgi.getDisplayModel().getManufacturerId()!=null){
				this.dgi.getManufacturerCountry().setValue(this.dgi.getManufacturersCountryStore().findModel(ManufacturerCountryDTO.MANUFACTURER_ID, this.dgi.getDisplayModel().getManufacturerId()));
			}else{
				this.dgi.getManufacturerCountry().setEmptyText("Select Manufacturer...");
			}
		}
	}

	public static class LoadManufacturerStoreDisplayCost implements AsyncCallback<List<ManufacturerCountryDTO>> {
		private CostAnalysisInfoGroup dgi;
		private DisplayScenarioDTO displayScenarioDTO;
		public LoadManufacturerStoreDisplayCost(CostAnalysisInfoGroup dgi, DisplayScenarioDTO displayScenarioDTO) {
			this.dgi = dgi;
			this.displayScenarioDTO=displayScenarioDTO;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get manufacturers through server", "try again later...", null);
		}

		public void onSuccess(List<ManufacturerCountryDTO> manufacturers) {
			this.dgi.getManufacturerStore().add(manufacturers);
			
			if(this.dgi.displayDTO.getCustomerId()!=null){
				CountryDTO countryDTO=countryMap.get(this.dgi.displayDTO.getCustomerId());
				this.dgi.getManufacturerStore().filter(ManufacturerCountryDTO.COUNTRY_NAME, countryDTO.getCountryName());
			}
			
			if(this.dgi.displayDTO.getManufacturerId()!=null){
				this.dgi.getManufacturer().setValue(this.dgi.getManufacturerStore().findModel(ManufacturerDTO.MANUFACTURER_ID, displayScenarioDTO.getManufacturerId()));
			}else{
				this.dgi.getManufacturer().setEmptyText("Select Manufacturer...");
			}
		}
	}

	public static class LoadRYGStatusStore implements AsyncCallback<List<StatusDTO>> {
		private DisplayGeneralInfo dgi;
		public LoadRYGStatusStore(DisplayGeneralInfo dgi) {
			this.dgi = dgi;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get RYGStatus through server", "try again later...", null);
		}
		public void onSuccess(List<StatusDTO> rygStatuses) {
			this.dgi.getRYGStatusStore().add(rygStatuses);
			if(this.dgi.getDisplayModel().getRygStatusFlg() !=null){
				this.dgi.getRYGStatusFlg().setValue(this.dgi.getRYGStatusStore().findModel(StatusDTO.STATUS_NAME, this.dgi.getDisplayModel().getRygStatusFlg()));
			}else{
				this.dgi.getRYGStatusFlg().setValue(this.dgi.getRYGStatusStore().findModel(StatusDTO.STATUS_NAME, "G"));
			}
		}
	}

	public static class GetServletPath implements AsyncCallback<String> {
		public GetServletPath() {
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get servlet path through server", "...", null);
		}

		public void onSuccess(String servletPath) {
			App.setServletPath(servletPath);
		}
	}

	public static class CallDisplayDelete implements AsyncCallback<Void>{
		SelectionGrid selectionGrid;
		Button btn;
		public CallDisplayDelete(SelectionGrid selectionGrid, Button btn){
			this.selectionGrid = selectionGrid;
			this.btn = btn;
		}
		public void onFailure(Throwable arg0) {
			this.btn.enable();
		}

		public void onSuccess(Void arg0) {
			this.btn.enable();
			selectionGrid.loadDisplays();
		}
	}

	public static class LoadCostAnalysisKitComponentInfo implements AsyncCallback<List<CostAnalysisKitComponentDTO>> {
		private ListStore<CostAnalysisKitComponentDTO> store;
		DisplayCostDataGroup selectionGrid;
		MessageBox box;
		DisplayScenarioDTO displayScenarioDTO;
		boolean comparisonView;
		public LoadCostAnalysisKitComponentInfo(DisplayCostDataGroup selectionGrid, ListStore<CostAnalysisKitComponentDTO> store, MessageBox box,DisplayScenarioDTO displayScenarioDTO, boolean comparisonView) {
			this.store = store;
			this.selectionGrid=selectionGrid;
			this.box=box;
			this.displayScenarioDTO=displayScenarioDTO;
			this.comparisonView=comparisonView;
		}
		public void onFailure(Throwable throwable) {
			if (box!=null) box.close();
			MessageBox.alert("Failed to get Cost analysis component through server", "try again later...", null);
			
		}
		public void onSuccess(List<CostAnalysisKitComponentDTO> kitComponents) {
			if (box!=null) box.close();
			if (kitComponents != null) {
				store.removeAll();
				store.commitChanges();
				store.add(kitComponents);
				store.commitChanges();
				selectionGrid.getCostAnalysisInfo().reCalculateDate.setValue(store.getModels().get(0).getPeReCalcDate()==null?new Date():store.getModels().get(0).getPeReCalcDate());
			}else{
				selectionGrid.getCostAnalysisInfo().reCalculateDate.clear();
				store.removeAll();
			}
			selectionGrid.calculateFinancialDataFields();
			selectionGrid.getCostAnalysisInfo().loadBUCostCenter(store.getModels(),displayScenarioDTO);
			//selectionGrid.updateCustomProgramDiscount();
			selectionGrid.getCostAnalysisInfo().loadComments(displayScenarioDTO);
			selectionGrid.getCostAnalysisInfo().loadHistory(displayScenarioDTO);
			selectionGrid.getCostAnalysisInfo().reloadDisplayCostImages(displayScenarioDTO.getDisplayId(), displayScenarioDTO.getDisplayScenarioId(), 1L);
			
		}
	}

	public static class LoadCorrugateComponentInfo implements AsyncCallback<List<CorrugateComponentsDetailDTO>> {
		private ListStore<CorrugateComponentsDetailDTO> store;
		CorrugateComponentEditorGrid corrCompEditorGrid;
		MessageBox box;
		DisplayDTO displayDTO;
		public LoadCorrugateComponentInfo(CorrugateComponentEditorGrid corrCompEditorGrid, ListStore<CorrugateComponentsDetailDTO> store, MessageBox box,DisplayDTO displayDTO) {
			this.store = store;
			this.corrCompEditorGrid=corrCompEditorGrid;
			this.box=box;
			this.displayDTO=displayDTO;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Cost analysis component through server", "try again later...", null);
			if (box!=null) box.close();
		}
		public void onSuccess(List<CorrugateComponentsDetailDTO> corrComponents) {
			if (corrComponents != null) {
				store.removeAll();
				store.commitChanges();
				store.add(corrComponents);
				store.commitChanges();
			}else{
				store.removeAll();
			}
			if (box!=null) box.close();
		}
	}

	public static class SaveCostAnalysisKitComponentInfo implements AsyncCallback<List<CostAnalysisKitComponentDTO>> {
		private ListStore<CostAnalysisKitComponentDTO> store;
		Grid<CostAnalysisKitComponentDTO> editorGrid;
		DisplayCostDataGroup selectionGrid;
		private MessageBox box;
		private DisplayScenarioDTO displayScenarioDTO;
		public SaveCostAnalysisKitComponentInfo(DisplayCostDataGroup selectionGrid, Grid<CostAnalysisKitComponentDTO> editorGrid,ListStore<CostAnalysisKitComponentDTO> store,MessageBox box,DisplayScenarioDTO displayScenarioDTO) {
			this.store = store;
			this.editorGrid=editorGrid;
			this.selectionGrid=selectionGrid;
			this.box = box;
			this.displayScenarioDTO=displayScenarioDTO; 
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed","Failed to get cost analysis kit conponent info through server, try again later...", null);
			if (box!=null) box.close();
		}
		public void onSuccess(List<CostAnalysisKitComponentDTO> kitComponents) {
			if (kitComponents != null) {
				editorGrid.getStore().removeAll();
				editorGrid.getStore().commitChanges();
				editorGrid.getStore().add(kitComponents);
				editorGrid.getStore().commitChanges();
				editorGrid.getView().refresh(true);
				if (box!=null) box.close();
				//selectionGrid.getCostAnalysisInfo().reload();
				//selectionGrid.updateView(displayScenarioDTO,true);
				
				int currIndex=selectionGrid.getCostAnalysisInfo().vpScenarioPanelTabs.getSelectedItem().getTabIndex();
				DisplayScenarioDTO curDisplayScenarioDTO=selectionGrid.getCostAnalysisInfo().displayScenarioDTOList.get(currIndex);
				
				selectionGrid.getCostAnalysisInfo().reload();
				//selectionGrid.getCostAnalysisInfo().updateView(selectionGrid.getCostAnalysisInfo().displayCostModels.get(currIndex),true,curDisplayScenarioDTO);
				
				selectionGrid.getCostAnalysisInfo().updateView(selectionGrid.getCostAnalysisInfo().getDisplayCostModel(),true,curDisplayScenarioDTO);
				if(displayScenarioDTO.getScenarioApprovalFlg() !=null && displayScenarioDTO.getScenarioApprovalFlg().equalsIgnoreCase("Y")){
					selectionGrid.getCostAnalysisInfo().saveApproveScenarioToDisplay(displayScenarioDTO);
				}
			}
		}
	}

	public static class SaveILSPriceException implements AsyncCallback<Void>{
		private MessageBox box;
		public SaveILSPriceException( MessageBox box){
			this.box = box;
		}
		public void onFailure(Throwable arg0) {
			box.close();
		}
		public void onSuccess(Void arg0) {
			box.close();
		}
	}

	public static class DeleteCostAnalysisKitComponentInfo implements AsyncCallback<Boolean> {
		private ListStore<CostAnalysisKitComponentDTO> store;
		private CostAnalysisKitComponentDTO kitComponent;
		private MessageBox box;
		DisplayCostDataGroup selectionGrid;
		Grid<CostAnalysisKitComponentDTO> editorGrid;
		public DeleteCostAnalysisKitComponentInfo( DisplayCostDataGroup selectionGrid, Grid<CostAnalysisKitComponentDTO> editorGrid,CostAnalysisKitComponentDTO kitComponent, ListStore<CostAnalysisKitComponentDTO> store,MessageBox box) {
			this.store = store;
			this.kitComponent = kitComponent;
			this.box=box;
			this.editorGrid=editorGrid;
			this.selectionGrid=selectionGrid;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Delete Failed", "to delete cost analysis kit component through server, try again later...", null);
			box.close();
		}
		public void onSuccess(Boolean result) {
			System.out.println("Service: DeleteCostAnalysisKitComponents");
			editorGrid.getStore().commitChanges();
			store.remove(kitComponent);
			editorGrid.getStore().commitChanges();
			editorGrid.getView().refresh(true);
			if (box!=null) box.close();
			int currIndex=selectionGrid.getCostAnalysisInfo().vpScenarioPanelTabs.getSelectedItem().getTabIndex();
			DisplayScenarioDTO curDisplayScenarioDTO=selectionGrid.getCostAnalysisInfo().displayScenarioDTOList.get(currIndex);
			selectionGrid.getCostAnalysisInfo().reload();
			selectionGrid.getCostAnalysisInfo().updateView(selectionGrid.getCostAnalysisInfo().displayCostModels.get(currIndex),true,curDisplayScenarioDTO);
			if(curDisplayScenarioDTO.getScenarioApprovalFlg() !=null && curDisplayScenarioDTO.getScenarioApprovalFlg().equalsIgnoreCase("Y")){
				selectionGrid.getCostAnalysisInfo().saveApproveScenarioToDisplay(curDisplayScenarioDTO);
			}
		}
	}
	
	public static class ReloadDisplayCosts implements AsyncCallback<List<DisplayCostDTO>> {
		private CostAnalysisInfo costAnalysisInfo;
		private DisplayDTO displayModel;
		public DisplayCostDTO displayCostModel;
		public ReloadDisplayCosts(DisplayDTO displayDTO, DisplayCostDTO displayCostModel,CostAnalysisInfo costAnalysisInfo) {
			this.costAnalysisInfo=costAnalysisInfo;
			this.displayModel=displayDTO;
			this.displayCostModel=displayCostModel;
		}
		public void onFailure(Throwable caught) {
			MessageBox.alert("Failed to get display cost data through server", "make sure display still exists...error " + caught.toString(), null);
		}
		public void onSuccess(List<DisplayCostDTO> displayCosts) {
			costAnalysisInfo.setData("display", displayModel);
			if (displayCosts != null && displayCosts.size() > 0){
				costAnalysisInfo.setData("displayCostModels", displayCosts);
				int scenarioNum=0;
				if(costAnalysisInfo.getData("reloadedScenarioNum")==null){
					scenarioNum=displayCosts.size();
				}else{
					scenarioNum=((Long)costAnalysisInfo.getData("reloadedScenarioNum")).intValue();
					costAnalysisInfo.setData("reloadedScenarioNum",null);
				}
				if(scenarioNum>0){
					this.displayCostModel.notify(new ChangeEvent(ChangeEventSource.Update, displayCosts.get(scenarioNum-1)));
					this.displayCostModel=displayCosts.get(scenarioNum-1);
					costAnalysisInfo.updateView(displayCosts.get(scenarioNum-1),true,costAnalysisInfo.displayScenarioDTOList.get(scenarioNum-1));
				}
				costAnalysisInfo.setData("reloadedScenarioNum",null);
			}else{
				if(costAnalysisInfo.displayCostDataGroupList.size()>0){
					costAnalysisInfo.displayCostDataGroupList.get(costAnalysisInfo.vpScenarioPanelTabs.getSelectedItem().getTabIndex()).foreCastQty.setValue(displayModel.getQtyForecast());
					costAnalysisInfo.displayCostDataGroupList.get(costAnalysisInfo.vpScenarioPanelTabs.getSelectedItem().getTabIndex()).actualQty.setValue(displayModel.getActualQty());
				}
			}
		}
	}
	
	public static class LoadDisplayScenarioInfo implements AsyncCallback<List<DisplayScenarioDTO>> {
		CostAnalysisInfo costAnalysisInfo;
		public LoadDisplayScenarioInfo(CostAnalysisInfo costAnalysisInfo) {
			this.costAnalysisInfo = costAnalysisInfo;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Display Scenario info through server", "try again later...", null);
		}
		public void onSuccess(List<DisplayScenarioDTO> displayScenarioDTOs) {
			System.out.println("Service:LoadDisplayScenarioInfo starts");
			if (displayScenarioDTOs == null || displayScenarioDTOs.size() == 0) {
				System.out.println("Service:LoadDisplayScenarioInfo EMPTY");
				DisplayScenarioDTO displayScenarioDTO =costAnalysisInfo.getDefaultScenarioData() ;
				displayScenarioDTO.setScenarioNum(new Long(1));
				displayScenarioDTO.setScenarioApprovalFlg("N");
				displayScenarioDTO.setDisplayScenarioId(null);
				displayScenarioDTO.setDtCreated(new Date());
				displayScenarioDTO.setUserCreated(App.getUser().getUsername());
				displayScenarioDTOs=new ArrayList<DisplayScenarioDTO>();
				displayScenarioDTOs.add(displayScenarioDTO);
			}else{
				System.out.println("Service:ReloadDisplayScenarioInfo NOT EMPTY Size="+displayScenarioDTOs.size());
			}
			costAnalysisInfo.setData("displayScenarioDTOs",displayScenarioDTOs);
			if (displayScenarioDTOs != null && displayScenarioDTOs.size() > 0) {
				for( DisplayScenarioDTO objDisplayScenario:displayScenarioDTOs){
					if(costAnalysisInfo.displayScenarioDTOList.size()==displayScenarioDTOs.size()){
						costAnalysisInfo.displayScenarioDTOList.set((objDisplayScenario.getScenarioNum().intValue())-1,objDisplayScenario );
						costAnalysisInfo.displayScenarioModel.notify(new ChangeEvent(ChangeEventSource.Update, objDisplayScenario));
						costAnalysisInfo.reloadcostAnalysisInfoData(objDisplayScenario);
						costAnalysisInfo.reloadcostAnalysisInfoComparisonData(objDisplayScenario);
					}
				}
			}
			System.out.println("Service:ReloadDisplayScenarioInfo ends");
		}
	}
	
	public static class LoadDisplayScenarioInfoForKitComponentInfo implements AsyncCallback<List<DisplayScenarioDTO>> {
		KitComponentInfo kitComponentInfo;
		CostAnalysisInfo costAnalysisInfo;
		public LoadDisplayScenarioInfoForKitComponentInfo(KitComponentInfo kitComponentInfo, CostAnalysisInfo costAnalysisInfo) {
			this.kitComponentInfo = kitComponentInfo;
			this.costAnalysisInfo = costAnalysisInfo;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Display Scenario info through server", "try again later...", null);
		}
		public void onSuccess(List<DisplayScenarioDTO> displayScenarioDTOs) {
			System.out.println("Service:LoadDisplayScenarioInfoForKitComponentInfo starts");
			if (displayScenarioDTOs == null || displayScenarioDTOs.size() == 0) {
				System.out.println("Service:LoadDisplayScenarioInfoForKitComponentInfo EMPTY");
				DisplayScenarioDTO displayScenarioDTO =new  DisplayScenarioDTO();
				displayScenarioDTO.setScenarioNum(new Long(1));
				displayScenarioDTO.setDisplayScenarioId(null);
				displayScenarioDTO.setDtCreated(new Date());
				displayScenarioDTO.setUserCreated(App.getUser().getUsername());
				displayScenarioDTOs=new ArrayList<DisplayScenarioDTO>();
				displayScenarioDTOs.add(displayScenarioDTO);
			}else{
				System.out.println("Service:LoadDisplayScenarioInfoForKitComponentInfo NOT EMPTY Size="+displayScenarioDTOs.size());
			}
			this.kitComponentInfo.setData("displayScenarioDTOs",displayScenarioDTOs);
			this.kitComponentInfo.setDisplayScenarioDTOs(displayScenarioDTOs);
			this.costAnalysisInfo.setData("displayScenarioDTOs",displayScenarioDTOs);
			this.kitComponentInfo.reloadGrid();
			System.out.println("Service:LoadDisplayScenarioInfoForKitComponentInfo ends");
		}
	}
	
	public static class LoadDisplayScenarioInfoForKitComponent implements AsyncCallback<List<DisplayScenarioDTO>> {
		KitComponentDetail kitComponentDetail;
		public LoadDisplayScenarioInfoForKitComponent(KitComponentDetail kitComponentDetail) {
			this.kitComponentDetail = kitComponentDetail;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Display Scenario info through server", "try again later...", null);
		}
		public void onSuccess(List<DisplayScenarioDTO> displayScenarioDTOs) {
			System.out.println("Service:LoadDisplayScenarioInfoForKitComponent starts");
			if (displayScenarioDTOs == null || displayScenarioDTOs.size() == 0) {
				System.out.println("Service:LoadDisplayScenarioInfoForKitComponent EMPTY");
				DisplayScenarioDTO displayScenarioDTO =new  DisplayScenarioDTO();
				displayScenarioDTO.setScenarioNum(new Long(1));
				displayScenarioDTO.setDisplayScenarioId(null);
				displayScenarioDTO.setDtCreated(new Date());
				displayScenarioDTO.setUserCreated(App.getUser().getUsername());
				displayScenarioDTOs=new ArrayList<DisplayScenarioDTO>();
				displayScenarioDTOs.add(displayScenarioDTO);
			}else{
				System.out.println("Service:LoadDisplayScenarioInfoForKitComponent NOT EMPTY Size="+displayScenarioDTOs.size());
			}
			kitComponentDetail.setData("displayScenarioDTOs",displayScenarioDTOs);
			System.out.println("Service:LoadDisplayScenarioInfoForKitComponentends");
		}
	}
	public static class ReLoadDisplayScenarioInfo implements AsyncCallback<List<DisplayScenarioDTO>> {
		CostAnalysisInfo costAnalysisInfo;
		Long scenarioNum;
		public ReLoadDisplayScenarioInfo(CostAnalysisInfo costAnalysisInfo, Long scenarioNum) {
			this.costAnalysisInfo = costAnalysisInfo;
			this.scenarioNum=scenarioNum;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Display Scenario info through server", "try again later...", null);
		}
		public void onSuccess(List<DisplayScenarioDTO> displayScenarioDTOs) {
			if (displayScenarioDTOs == null || displayScenarioDTOs.size() == 0) {
				DisplayScenarioDTO displayScenarioDTO =costAnalysisInfo.getDefaultScenarioData() ;
				displayScenarioDTO.setScenarioNum(new Long(1));
				displayScenarioDTO.setDisplayScenarioId(null);
				displayScenarioDTO.setDtCreated(new Date());
				displayScenarioDTO.setUserCreated(App.getUser().getUsername());
				displayScenarioDTOs=new ArrayList<DisplayScenarioDTO>();
				displayScenarioDTOs.add(displayScenarioDTO);
				DisplayCostDTO copiedDisplayCostDTO = new DisplayCostDTO();
				costAnalysisInfo.setData("copiedDisplayCost", copiedDisplayCostDTO);
			}else{
				System.out.println("Service:ReloadDisplayScenarioInfo NOT EMPTY Size="+displayScenarioDTOs.size());
			}
			//costAnalysisInfo.vpScenarioPanelTabs.setSelection(item);
			costAnalysisInfo.setData("displayScenarioDTOs",displayScenarioDTOs);
			System.out.println("Service:ReloadDisplayScenarioInfo starts");
			System.out.println("Service:ReloadDisplayScenarioInfo reloadedScenarioNum="+scenarioNum);
			costAnalysisInfo.setData("reloadedScenarioNum",scenarioNum);
			costAnalysisInfo.reload();
			System.out.println("Service:ReloadDisplayScenarioInfo ends");
		}
	}
	
	
	public static class ReLoadCopiedDisplayScenario implements AsyncCallback<DisplayScenarioDTO> {
		CostAnalysisInfo costAnalysisInfo;
		DisplayScenarioDTO displayScenarioDTO;
		MessageBox box;
		public ReLoadCopiedDisplayScenario(CostAnalysisInfo costAnalysisInfo, DisplayScenarioDTO displayScenarioDTO, MessageBox box) {
			this.costAnalysisInfo = costAnalysisInfo;
			this.displayScenarioDTO=displayScenarioDTO;
			this.box=box;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get Display Scenario info through server", "try again later...", null);
			if(box!=null) box.close();
		}
		public void onSuccess(DisplayScenarioDTO displayScenarioDTO) {
			System.out.println("Service:ReLoadCopiedDisplayScenario starts");
			System.out.println("Service:ReLoadCopiedDisplayScenario starts");
			System.out.println("Service:ReLoadCopiedDisplayScenario reloadedScenarioNum="+displayScenarioDTO.getScenarioNum());
			costAnalysisInfo.setData("reloadedScenarioNum",displayScenarioDTO.getScenarioNum());
			//costAnalysisInfo.loadScenarios(displayScenarioDTO.getDisplayId());
			costAnalysisInfo.getDisplayTabs().reloadScenarios(displayScenarioDTO.getScenarioNum());
			costAnalysisInfo.displayScenarioDTOList.set((displayScenarioDTO.getScenarioNum().intValue())-1,displayScenarioDTO );
			//costAnalysisInfo.reload();
			System.out.println("Service:ReloadDisplayScenarioInfo ends");
			if(box!=null) box.close();
		}
	}
	
	public static class SaveDisplayCostDTO implements AsyncCallback<DisplayCostDTO> {
		private DisplayCostDataGroup dcdg;
		private DisplayCostDTO displayCostDTO; 
		private MessageBox box;
		private TabPanel panel;
		private TabItem tabItem;
		private DisplayScenarioDTO displayScenarioDTO;
		public SaveDisplayCostDTO(DisplayCostDataGroup dcdg, DisplayCostDTO displayCostDTO, MessageBox box,TabPanel panel, TabItem nextTab,DisplayScenarioDTO displayScenarioDTO) {
			this.dcdg = dcdg;
			this.displayCostDTO = displayCostDTO;
			this.box=box;
			this.panel=panel;
			this.tabItem=nextTab;
			this.displayScenarioDTO = displayScenarioDTO;
		}
		public void onFailure(Throwable caught) {
			MessageBox.alert("Failed to save display cost through server", "try again later...", null);
			if (box != null)  box.close();
		}
		public void onSuccess(DisplayCostDTO displayCostDTO) {
			if (box != null)  box.close();
			if ( panel != null && tabItem != null) {
				panel.setSelection(tabItem);
				return;
			}
			this.displayCostDTO.notify(new ChangeEvent(ChangeEventSource.Update, displayCostDTO));
			dcdg.getCostAnalysisInfo().setDisplayCostModel(displayCostDTO);
			dcdg.getCostAnalysisInfo().setData("displayCostModel",displayCostDTO);
			List<DisplayCostDTO> displayCostModels = dcdg.getCostAnalysisInfo().getData("displayCostModels");
			if (displayCostModels == null) {
				displayCostModels = new Vector<DisplayCostDTO>();
			}
			displayCostModels.add(displayCostDTO);
			dcdg.getCostAnalysisInfo().setData("displayCostModels", displayCostModels);
			dcdg.getCostAnalysisInfo().setData("copiedDisplayCost",null);
			
			dcdg.setDisplayScenarioDTO(this.displayScenarioDTO);
			dcdg.getCostAnalysisInfo().setDisplayCostModel(displayCostDTO);
		//	dcdg.getCostAnalysisInfo().getDisplayCostModel().notify(new ChangeEvent(ChangeEventSource.Update, displayCostDTO));
			this.displayCostDTO = displayCostDTO;
			/*
			 * Code added Date:11/25/2010
			 * dcdg.getCostAnalysisInfo().loadHistory(displayCostDTO.getDisplayId(),displayCostDTO.getDisplayScenarioId());
			 */
			/*
			 * Changed By : Mari
			 * Code added Date:10/04/2012
			 * update NetSales, Vm Pct, Vm$, Program Discount
			 */
			 
			 
			//dcdg.getCostAnalysisInfo().reload();
			//dcdg.getCostAnalysisInfo().updateView(displayCostDTO,true,this.displayScenarioDTO);
			dcdg.loadCostAnalysisComponentInfo(displayScenarioDTO, false);
			
			/*
			 * Changed By : Mari
			 * Code added Date:10/04/2012
			 * update NetSales, Vm Pct, Vm$, Program Discount
			 */
			//dcdg.getCostAnalysisInfo().calcdeductedNetSalesValAp(this.displayScenarioDTO.getScenarioNum().intValue()-1);
			//dcdg.loadCostAnalysisComponentInfo(this.displayScenarioDTO, false);
			

			/*
			 * Code added Date:10/04/2012
			 * End
			 */
			
			
			if(this.displayScenarioDTO.getScenarioApprovalFlg() !=null && this.displayScenarioDTO.getScenarioApprovalFlg().equalsIgnoreCase("Y")){
				dcdg.getCostAnalysisInfo().saveApproveScenarioToDisplay(this.displayScenarioDTO);
			}
			
			dcdg.getCostAnalysisInfo().getDisplayCostModel().notify(new ChangeEvent(ChangeEventSource.Update, displayCostDTO));

		}
	}
	
	public static class SaveDisplayScenarioInfo implements AsyncCallback<DisplayScenarioDTO> {
		private DisplayScenarioDTO displayScenario;
		private Button button = new Button();
		private MessageBox box;
		private CostAnalysisInfo costAnalysisInfo;
		private TabPanel panel;
		private TabItem nextTab;
		public SaveDisplayScenarioInfo(DisplayScenarioDTO displayScenario, Button button, MessageBox box,CostAnalysisInfo costAnalysisInfo,TabPanel panel, TabItem nextTab) {
			this.displayScenario = displayScenario;
			this.button = button;
			if (button != null) button.disable();
			this.box = box;
			this.costAnalysisInfo=costAnalysisInfo;
			this.panel=panel;
			this.nextTab=nextTab;
		}
		public void onFailure(Throwable throwable) {
			if (box != null) box.close();
			MessageBox.alert("Failed to save display scenario, refresh", throwable.toString(), null);
			if (button != null) button.enable();
		}
		public void onSuccess(DisplayScenarioDTO displayScenarioDTO) {
			if (button != null) button.enable();
			if (box != null) box.close();
			costAnalysisInfo.loadScenarios(displayScenarioDTO.getDisplayId());
			this.displayScenario.notify(new ChangeEvent(ChangeEventSource.Update, displayScenarioDTO));
			if(displayScenarioDTO!=null){
				int tabIndex=displayScenario.getScenarioNum().intValue()-1;
				costAnalysisInfo.displayScenarioDTOList.set(tabIndex, displayScenarioDTO);
				costAnalysisInfo.saveDisplayCost(displayScenarioDTO,panel,nextTab);
			}
		}
	}
	public static class ReloadDisplayCostComment implements AsyncCallback<List<DisplayCostCommentDTO>> {
		private ListStore<DisplayCostCommentDTO> displayCostCommentModel;
		private CostAnalysisInfo costAnalysisInfo;
		private int tabSecnarioIdx;

		public ReloadDisplayCostComment(ListStore<DisplayCostCommentDTO> displayCostCommentDTO,CostAnalysisInfo costAnalysisInfo,int tabSecnarioIdx) {
			this.displayCostCommentModel = displayCostCommentDTO;
			this.costAnalysisInfo=costAnalysisInfo;
			this.tabSecnarioIdx=tabSecnarioIdx;
		}

		public void onFailure(Throwable caught) {
			MessageBox.alert("Failed to get display cost data through server", "make sure display still exists...error " + caught.toString(), null);
		}

		public void onSuccess(List<DisplayCostCommentDTO> displayCostComments) {
			costAnalysisInfo.displayCommentGridList.get(tabSecnarioIdx).getStore().removeAll();
			if (displayCostComments != null && displayCostComments.size()>0) {
				if (displayCostComments != null) {
					costAnalysisInfo.displayCommentGridList.get(tabSecnarioIdx).getStore().removeAll();
					costAnalysisInfo.displayCommentGridList.get(tabSecnarioIdx).getStore().commitChanges();
					costAnalysisInfo.displayCommentGridList.get(tabSecnarioIdx).getStore().add(displayCostComments);
					costAnalysisInfo.displayCommentGridList.get(tabSecnarioIdx).getStore().commitChanges();
				}
			}
		}
	}
	
	public static class ReloadCompareDisplayCostComment implements AsyncCallback<List<DisplayCostCommentDTO>> {
		private ListStore<DisplayCostCommentDTO> displayCostCommentStore;
		private CostAnalysisInfo costAnalysisInfo;
		private int tabSecnarioIdx;

		public ReloadCompareDisplayCostComment(ListStore<DisplayCostCommentDTO> displayCostCommentStore,CostAnalysisInfo costAnalysisInfo,int tabSecnarioIdx) {
			this.displayCostCommentStore = displayCostCommentStore;
			this.costAnalysisInfo=costAnalysisInfo;
			this.tabSecnarioIdx=tabSecnarioIdx;
		}

		public void onFailure(Throwable caught) {
			MessageBox.alert("Failed to get display cost data through server", "make sure display still exists...error " + caught.toString(), null);
		}

		public void onSuccess(List<DisplayCostCommentDTO> displayCostComments) {
			costAnalysisInfo.displayComparisonCommentGridList.get(tabSecnarioIdx).getStore().removeAll();
			if (displayCostComments != null && displayCostComments.size()>0) {
				if (displayCostComments != null) {
					//MessageBox.alert("Service:ReloadCompareDisplayCostComment", "displayCostComments.getName="+displayCostComments.get(0).getCostComment(), null);
					costAnalysisInfo.displayComparisonCommentGridList.get(tabSecnarioIdx).getStore().removeAll();
					costAnalysisInfo.displayComparisonCommentGridList.get(tabSecnarioIdx).getStore().commitChanges();
					costAnalysisInfo.displayComparisonCommentGridList.get(tabSecnarioIdx).getStore().add(displayCostComments);
					costAnalysisInfo.displayComparisonCommentGridList.get(tabSecnarioIdx).getStore().commitChanges();
				}
			}
		}
	}	
	public static class SaveDisplayCostCommentDTO implements AsyncCallback<DisplayCostCommentDTO> {
		private DisplayCostCommentDTO displayCostCommentDTO ;
		private CostAnalysisInfo costAnalysisInfo;
		private DisplayScenarioDTO displayScenarioDTO;

		public SaveDisplayCostCommentDTO(CostAnalysisInfo costAnalysisInfo,DisplayCostCommentDTO displayCostCommentDTO, DisplayScenarioDTO displayScenarioDTO) {
			this.displayCostCommentDTO = displayCostCommentDTO;
			this.costAnalysisInfo =costAnalysisInfo;
			this.displayScenarioDTO=displayScenarioDTO;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save display comments, refresh", throwable.toString(), null);
		}

		public void onSuccess(DisplayCostCommentDTO displayCostComment) {
			costAnalysisInfo.loadComments(displayScenarioDTO);
		}
	}
	
	public static class SaveApplyForApproval implements AsyncCallback<DisplayScenarioDTO> {
		private CostAnalysisInfo costAnalysisInfo;
		private MessageBox box;
		private int scenarioTabIdx;
		
		public SaveApplyForApproval(CostAnalysisInfo costAnalysisInfo,MessageBox box,int scenarioTabIdx) {
			this.costAnalysisInfo =costAnalysisInfo;
			this.box=box;
			this.scenarioTabIdx=scenarioTabIdx;
		}

		public void onFailure(Throwable throwable) {
			if (box != null) box.close();
			MessageBox.alert("Failed to save display comments, refresh", throwable.toString(), null);
		}
		public void onSuccess(DisplayScenarioDTO displayScenarioDTO) {
			costAnalysisInfo.loadScenarios(displayScenarioDTO.getDisplayId());
			/*
			 * Code modified by Mari
			 * Date: 10-11-2012
			 * 
			 */
			/*if(displayScenarioDTO.getScenarioApprovalFlg().equalsIgnoreCase("Y")){
				costAnalysisInfo.saveApproveScenarioToDisplay(displayScenarioDTO);
			}*/
			displayScenarioDTO.setScenarioApprovalFlg("Y");
			costAnalysisInfo.saveSummaryCalculation(costAnalysisInfo.displayCostCompareGroupList, displayScenarioDTO,scenarioTabIdx);
			costAnalysisInfo.saveApproveScenarioToDisplay(displayScenarioDTO);
			if (box != null) box.close();
		}
	}

	public static class ReloadCostAnalysisHistory implements AsyncCallback<List<CostAnalysisHistoryDTO>> {
		private ListStore<CostAnalysisHistoryDTO> costAnalysisHistoryModels;
		private CostAnalysisHistoryGrid costAnalysisHistoryGrid;

		public ReloadCostAnalysisHistory(ListStore<CostAnalysisHistoryDTO> costAnalysisHistoryModels,CostAnalysisHistoryGrid costAnalysisHistoryGrid) {
			this.costAnalysisHistoryModels = costAnalysisHistoryModels;
			this.costAnalysisHistoryGrid=costAnalysisHistoryGrid;
		}

		public void onFailure(Throwable caught) {
			MessageBox.alert("Load Falied ", "make sure cost analysis history still exists...error " + caught.toString(), null);
		}

		public void onSuccess(List<CostAnalysisHistoryDTO> costAnalysisHistoryModels) {
			if (costAnalysisHistoryModels != null && costAnalysisHistoryModels.size()>0) {
				costAnalysisHistoryGrid.historyGrid.getStore().removeAll();
				costAnalysisHistoryGrid.historyGrid.getStore().commitChanges();
				costAnalysisHistoryGrid.historyGrid.getStore().add(costAnalysisHistoryModels);
				costAnalysisHistoryGrid.historyGrid.getStore().commitChanges();
			}
		}
	}

	public static class LoadBUCostCenterSplitGroup implements AsyncCallback<List<BuCostCenterSplitDTO>> {
		private ListStore<BuCostCenterSplitDTO> buCostCenterSplitModels;
		private CostAnalysisInfoBUCostCtrSplitGrid costAnalysisInfoBUCostCtrSplitGrid;
		private MessageBox box;
		private int tabScenarioIdx;
		private DisplayCostDataGroup displayCostDataGroup;
		public Double dSumActualInvoiceWithPrgm ;
		public LoadBUCostCenterSplitGroup(DisplayCostDataGroup displayCostDataGroup, 
				ListStore<BuCostCenterSplitDTO> buCostCenterSplitModels,
				CostAnalysisInfoBUCostCtrSplitGrid costAnalysisInfoBUCostCtrSplitGrid,
				MessageBox box, int tabScenarioIdx ) {
				this.buCostCenterSplitModels = buCostCenterSplitModels;
				this.costAnalysisInfoBUCostCtrSplitGrid=costAnalysisInfoBUCostCtrSplitGrid;
				this.box=box;
				this.tabScenarioIdx= tabScenarioIdx;
				this.displayCostDataGroup=displayCostDataGroup;
		}

		public void onFailure(Throwable caught) {
			MessageBox.alert("Load Falied ", "make sure bu cost center split still exists...error " + caught.toString(), null);
			box.close();
		}

		public void onSuccess(List<BuCostCenterSplitDTO> buCostCenterSplitModels) {
			dSumActualInvoiceWithPrgm=0.00;
			box.close();
			if (buCostCenterSplitModels != null && buCostCenterSplitModels.size()>0) {
				costAnalysisInfoBUCostCtrSplitGrid.buCostCtrSplitgrid.getStore().removeAll();
				costAnalysisInfoBUCostCtrSplitGrid.buCostCtrSplitgrid.getStore().commitChanges();
				costAnalysisInfoBUCostCtrSplitGrid.buCostCtrSplitgrid.getStore().add(buCostCenterSplitModels);
				costAnalysisInfoBUCostCtrSplitGrid.buCostCtrSplitgrid.getStore().commitChanges();
				List<CostAnalysisKitComponentDTO> objCostAnalysisKitComponentDTOs=  displayCostDataGroup.costAnalysisKitComponentStore.getModels();
				for(CostAnalysisKitComponentDTO r : objCostAnalysisKitComponentDTOs) {
					if(r.getTotalActualInvoiceWpgrm()!=null) {
						dSumActualInvoiceWithPrgm+=r.getTotalActualInvoiceWpgrm();
					}
				}
				
			}else{
				costAnalysisInfoBUCostCtrSplitGrid.buCostCtrSplitgrid.getStore().removeAll();
			}
			displayCostDataGroup.calculateFinancialDataFields();
			displayCostDataGroup.updateCustomProgramDiscount(dSumActualInvoiceWithPrgm, true,tabScenarioIdx);
		}
	}

	public static class ReloadDisplayCostImage implements AsyncCallback<List<DisplayCostImageDTO>> {
		private ListStore<DisplayCostImageDTO> displayCostImageModels;
		private FileUploadWindow fileUploadWindow;
		private Long selectedIndex;
		private CostAnalysisInfo costAnalysisInfo;
		public ReloadDisplayCostImage(ListStore<DisplayCostImageDTO> displayCostImageModels ,FileUploadWindow fileUploadWindow, CostAnalysisInfo costAnalysisInfo, Long selectedIndex) {
			this.displayCostImageModels = displayCostImageModels;
			this.fileUploadWindow=fileUploadWindow;
			this.selectedIndex=selectedIndex;
			this.costAnalysisInfo=costAnalysisInfo;
		}

		public void onFailure(Throwable caught) {
			MessageBox.alert("Load Falied ", "make sure cost analysis history still exists...error " + caught.toString(), null);
		}

		public void onSuccess(List<DisplayCostImageDTO> displayCostImageModels) {
			int scenarioTabIndex=costAnalysisInfo.vpScenarioPanelTabs.getSelectedItem().getTabIndex();
			if (displayCostImageModels != null && displayCostImageModels.size()>0) {
				Image image ;
				int currentIndex=1;
				fileUploadWindow.store.removeAll();
				fileUploadWindow.store.commitChanges();
				fileUploadWindow.store.add(displayCostImageModels);
				fileUploadWindow.store.commitChanges();
				for (DisplayCostImageDTO displayCostImageModel:displayCostImageModels){
					if(currentIndex==selectedIndex){
						image	= new Image();
						image.setSize("415px", "305");
						image.setUrl(displayCostImageModel.getUrl());
						fileUploadWindow.grid.clear();
						fileUploadWindow.grid.clearCell(0, 0);
						fileUploadWindow.grid.setWidget(0, 0, image);
						fileUploadWindow.grid.getCellFormatter().setStyleName(0, 0, "tableCell-image");
						fileUploadWindow.grid.getCellFormatter().setAlignment(0, 0, HasHorizontalAlignment.ALIGN_CENTER, HasVerticalAlignment.ALIGN_MIDDLE);
						fileUploadWindow.lblImageCount.setFieldLabel("<center>" + selectedIndex +"  of " + displayCostImageModels.size() + "</center>");
						fileUploadWindow.setSelectedIdx(selectedIndex);
						fileUploadWindow.setTotalIdx(new Long(displayCostImageModels.size()));
						break;
					}
					currentIndex++;
				}
				fileUploadWindow.setTotalIdx(new Long(displayCostImageModels.size()));
				if((selectedIndex==displayCostImageModels.size()) &&   displayCostImageModels.size()>1){
					fileUploadWindow.btnNextImage.disable();
					fileUploadWindow.btnPreviousImage.enable();
				}else if(selectedIndex==1 &&  displayCostImageModels.size()>1){
					fileUploadWindow.btnNextImage.enable();
					fileUploadWindow.btnPreviousImage.disable();
				}else if(selectedIndex<displayCostImageModels.size() && selectedIndex>1 && displayCostImageModels.size()>1){
					fileUploadWindow.btnNextImage.enable();
					fileUploadWindow.btnPreviousImage.enable();
				}else if(selectedIndex>displayCostImageModels.size()){
					fileUploadWindow.setSelectedIdx(new Long(displayCostImageModels.size()));
					fileUploadWindow.btnNextImage.disable();
					fileUploadWindow.btnPreviousImage.enable();
				}else if(displayCostImageModels.size()>1){
					fileUploadWindow.btnNextImage.enable();
					fileUploadWindow.btnPreviousImage.disable();
				}else{
					fileUploadWindow.btnNextImage.enable();
					fileUploadWindow.btnPreviousImage.enable();
				}
			}else{
				fileUploadWindow.store.removeAll();
				fileUploadWindow.store.commitChanges();
				fileUploadWindow.store.add(displayCostImageModels);
				fileUploadWindow.store.commitChanges();
				fileUploadWindow.grid.clear();
				fileUploadWindow.grid.clearCell(0, 0);
				fileUploadWindow.lblImageCount.setFieldLabel("<center>0  of 0</center>");
				fileUploadWindow.setTotalIdx(new Long(0L));
			}
		}
	}

	public static class SaveDisplayCostImageDTO implements AsyncCallback<List<DisplayCostImageDTO>> {
		private List<DisplayCostImageDTO> displayCostImageDTOs ;
		private FileUploadWindow fileUploadWindow;

		public SaveDisplayCostImageDTO(FileUploadWindow fileUploadWindow,List<DisplayCostImageDTO> displayCostImageDTOs) {
			this.displayCostImageDTOs = displayCostImageDTOs;
			this.fileUploadWindow =fileUploadWindow;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save display image, refresh", throwable.toString(), null);
		}

		public void onSuccess(List<DisplayCostImageDTO> displayCostImageDTO) {

			if(displayCostImageDTO!=null) fileUploadWindow.setSelectedIdx(new Long(displayCostImageDTO.size()));
			if(displayCostImageDTO!=null) fileUploadWindow.setTotalIdx(new Long(displayCostImageDTO.size()));
			if(displayCostImageDTO!=null) fileUploadWindow.reloadDisplayCostImages(displayCostImageDTO.get(displayCostImageDTO.size()-1).getDisplayId(),displayCostImageDTO.get(displayCostImageDTO.size()-1).getDisplayScenarioId(),new Long(displayCostImageDTO.size()) );
		}
	}

	public static class DeleteDisplayCostImageDTO implements AsyncCallback<List<DisplayCostImageDTO>> {
		private List<DisplayCostImageDTO> displayCostImageDTOs ;
		private FileUploadWindow fileUploadWindow;
		MessageBox box;

		public DeleteDisplayCostImageDTO(FileUploadWindow fileUploadWindow,List<DisplayCostImageDTO> displayCostImageDTOs, MessageBox box) {
			this.displayCostImageDTOs = displayCostImageDTOs;
			this.fileUploadWindow =fileUploadWindow;
			this.box=box;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save display image, refresh", throwable.toString(), null);
			box.close();
		}

		public void onSuccess(List<DisplayCostImageDTO> displayCostImageDTO) {
			if(displayCostImageDTO!=null) fileUploadWindow.setSelectedIdx(new Long(displayCostImageDTO.size()));
			if(displayCostImageDTO!=null) fileUploadWindow.setTotalIdx(new Long(displayCostImageDTO.size()));
			if(displayCostImageDTO.size()>0){
				if(displayCostImageDTO!=null) fileUploadWindow.reloadDisplayCostImages(displayCostImageDTO.get(displayCostImageDTO.size()-1).getDisplayId(),displayCostImageDTO.get(displayCostImageDTO.size()-1).getDisplayScenarioId(),new Long(displayCostImageDTO.size()) );
			}else{
				fileUploadWindow.grid.clear();
				fileUploadWindow.grid.clearCell(0, 0);
				fileUploadWindow.setSelectedIdx(new Long(0));
				fileUploadWindow.setTotalIdx(new Long(0));
				fileUploadWindow.reloadDisplayCostImages(new Long(0),new Long(0),new Long(0) );
			}
			box.close();
		}
	}
	public static class SaveDisplayAfterApproval implements AsyncCallback<DisplayDTO> {
		private DisplayDTO display ;
		private CostAnalysisInfo costAnalysisInfo;
		private DisplayScenarioDTO displayScenarioDTO;
		private int scenarioTabIdx;
		
		
		public SaveDisplayAfterApproval(CostAnalysisInfo costAnalysisInfo,DisplayDTO display,DisplayScenarioDTO displayScenarioDTO, int scenarioTabIdx ) {
			this.display =display;
			this.costAnalysisInfo=costAnalysisInfo;
			this.displayScenarioDTO=displayScenarioDTO;
			this.scenarioTabIdx=scenarioTabIdx;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save display comments, refresh", throwable.toString(), null);
		}
		public void onSuccess(DisplayDTO displayDTO) {
			costAnalysisInfo.displayTabHeader.setDisplay(displayDTO);
			costAnalysisInfo.displayTabHeader.setData("display", display);
			costAnalysisInfo.getDisplayTabs().setDisplayModel(displayDTO);
			costAnalysisInfo.getDisplayTabs().setData("display", displayDTO);
			//MessageBox.alert("SaveDisplayAfterApproval", "SaveDisplayAfterApproval..1..", null);
			//costAnalysisInfo.saveSummaryCalculation(costAnalysisInfo.displayCostCompareGroupList, displayScenarioDTO,scenarioTabIdx);
			this.display.notify(new ChangeEvent(ChangeEventSource.Update, displayDTO));
			this.display = displayDTO;
		}
	}
	
	public static class SaveWithDefaultScenario implements AsyncCallback<DisplayScenarioDTO> {
		private DisplayScenarioDTO displayScenario;
		private KitComponentDetail kitComponentDetail;
		public SaveWithDefaultScenario( DisplayScenarioDTO displayScenario, KitComponentDetail kitComponentDetail) {
			this.displayScenario = displayScenario;
			this.kitComponentDetail=kitComponentDetail;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save default display scenario, refresh", throwable.toString(), null);
		}
		public void onSuccess(DisplayScenarioDTO displayScenarioDTO) {
			if(displayScenarioDTO!=null){
				kitComponentDetail.saveDefaultScenario(displayScenarioDTO);
			}else{
			}
		}
	}

	public static class SaveWithDefaultScenarioAndCost implements AsyncCallback<DisplayScenarioDTO> {
		private DisplayScenarioDTO displayScenario;
		private KitComponentDetail kitComponentDetail;
		public SaveWithDefaultScenarioAndCost( DisplayScenarioDTO displayScenario, KitComponentDetail kitComponentDetail) {
			this.displayScenario = displayScenario;
			this.kitComponentDetail=kitComponentDetail;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save default display scenario, refresh", throwable.toString(), null);
		}
		public void onSuccess(DisplayScenarioDTO displayScenarioDTO) {
			if(displayScenarioDTO!=null){
				kitComponentDetail.saveDefaultScenario(displayScenarioDTO);
				kitComponentDetail.saveDefaultDisplayCost(displayScenarioDTO);
			}else{
			}
		}
	}
    
	public static class SaveDefaultDisplayCostDTO implements AsyncCallback<DisplayCostDTO> {
		private DisplayCostDTO displayCost;
		public SaveDefaultDisplayCostDTO(  DisplayCostDTO displayCostDTO) {
			this.displayCost = displayCostDTO;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save default display cost, refresh", throwable.toString(), null);
		}
		public void onSuccess(DisplayCostDTO displayCost) {
			if(displayCost!=null){
			}else{
			}
		}
	}
	
	public static class CallCorrugateComponentDelete implements AsyncCallback<Void>{
		CorrugateComponentEditorGrid corrCompEditorGrid;
		Button btn;
		private MessageBox box;
		public CallCorrugateComponentDelete(CorrugateComponentEditorGrid corrCompEditorGrid, Button btn,	MessageBox box){
			this.corrCompEditorGrid = corrCompEditorGrid;
			this.btn = btn;
			this.box = box;
		}
		public void onFailure(Throwable arg0) {
			box.close();
			this.btn.enable();
		}

		public void onSuccess(Void arg0) {
			this.btn.enable();
			corrCompEditorGrid.loadCorrugateComponents();
			box.close();
		}
	}
	
	public static class SaveCorrugateComponentDTOList implements AsyncCallback<List<CorrugateComponentsDetailDTO>> {
		private Button button = new Button();
		private MessageBox box;
		Grid<CorrugateComponentsDetailDTO> componentGrid;
		CorrugateComponentEditorGrid corrugateComponentEditorGrid;
		DisplayDTO displayDTO;

		public SaveCorrugateComponentDTOList( CorrugateComponentEditorGrid corrugateComponentEditorGrid, Grid<CorrugateComponentsDetailDTO> componentGrid, DisplayDTO displayDTO, MessageBox box, Button button) {
			this.box = box;
			this.button = button;
			this.componentGrid=componentGrid;
			this.corrugateComponentEditorGrid=corrugateComponentEditorGrid;
			this.displayDTO=displayDTO;
			if(this.button != null)this.button.disable();
		}

		public void onFailure(Throwable throwable) {
			button.enable();
			if (box != null) box.close();
		}

		public void onSuccess(List<CorrugateComponentsDetailDTO> projectDTOList) {
			componentGrid.getStore().removeAll();
			componentGrid.getStore().commitChanges();
			componentGrid.getStore().add(projectDTOList);
			componentGrid.getStore().commitChanges();
			corrugateComponentEditorGrid.loadCorrugateComponents();
			if(button != null) button.enable();
			box.close();
		}
	}
	
	public static class DeleteCorrugateKitComponentInfo implements AsyncCallback<Boolean> {
		private ListStore<CorrugateComponentsDetailDTO> store;
		private CorrugateComponentsDetailDTO corrComponent;
		private MessageBox box;
		CorrugateComponentEditorGrid corrCompEditorGrid;
		Grid<CorrugateComponentsDetailDTO> editorGrid;
		public DeleteCorrugateKitComponentInfo( CorrugateComponentEditorGrid corrCompEditorGrid, Grid<CorrugateComponentsDetailDTO> editorGrid,CorrugateComponentsDetailDTO corrComponent, ListStore<CorrugateComponentsDetailDTO> store,MessageBox box) {
			this.store = store;
			this.corrComponent = corrComponent;
			this.box=box;
			this.editorGrid=editorGrid;
			this.corrCompEditorGrid=corrCompEditorGrid;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Delete Corrugate", "Failed to delete component through server, try again later...", null);
			if(box!=null) box.close();
		}
		public void onSuccess(Boolean result) {
			System.out.println("Service: DeleteCorrugateComponents");
			if(box!=null) box.close();
			editorGrid.getStore().commitChanges();
			store.remove(corrComponent);
			editorGrid.getStore().commitChanges();
			editorGrid.getView().refresh(true);
		}
	}
	
	public static class SaveCorrugateDTO implements AsyncCallback<CorrugateDTO> {
		private CorrugateDTO corrugateDTO ;
		private CorrugateComponentInfo compInfo;
		//private MessageBox box;
		private TabPanel panel;
		private TabItem nextTab;
		private CorrugateInfoGroup corrugateInfoGroup;
		
		//public SaveCorrugateDTO(CorrugateComponentInfo compInfo,CorrugateDTO corrugateDTO,MessageBox box,TabPanel panel, TabItem nextTab,CorrugateInfoGroup corrugateInfoGroup ) {
		public SaveCorrugateDTO(CorrugateComponentInfo compInfo,CorrugateDTO corrugateDTO,TabPanel panel, TabItem nextTab,CorrugateInfoGroup corrugateInfoGroup ) {
			this.corrugateDTO =corrugateDTO;
			this.compInfo=compInfo;
			//this.box=box;
			this.panel=panel;
			this.nextTab=nextTab;
			this.corrugateInfoGroup=corrugateInfoGroup;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save display comments, refresh", throwable.toString(), null);
			//if(box!=null) box.close();
		}
		public void onSuccess(CorrugateDTO corrugateDTO) {
			if ( panel != null && nextTab != null) {
				panel.setSelection(nextTab);
				return;
			}
			this.corrugateDTO.notify(new ChangeEvent(ChangeEventSource.Update, corrugateDTO));
			compInfo.setData(Constants.DISPLAY_CORRUGATE, corrugateDTO);
			//if(box!=null) box.close();
			corrugateInfoGroup.setCorrugateDTO(corrugateDTO);
			corrugateInfoGroup.resetDisplay(corrugateDTO);
		}
	}
	
	public static class ReloadCorrugate implements AsyncCallback<CorrugateDTO> {
		private CorrugateComponentInfo corrCompInfo;
		private DisplayDTO displayModel;
		private CorrugateDTO corrugateModel;
		private CorrugateInfoGroup corrugateInfoGroup;
		private CorrugateComponentEditorGrid corrCompEditorGroup;
		public ReloadCorrugate(DisplayDTO displayDTO, CorrugateDTO corrugateModel, CorrugateComponentInfo corrCompInfo,CorrugateInfoGroup corrugateInfoGroup, CorrugateComponentEditorGrid corrCompEditorGroup) {
			this.corrCompInfo=corrCompInfo;
			this.displayModel=displayDTO;
			this.corrugateModel=corrugateModel;
			this.corrugateInfoGroup=corrugateInfoGroup;
			this.corrCompEditorGroup =corrCompEditorGroup;
		}
		public void onFailure(Throwable caught) {
			MessageBox.alert("Failed to get display cost data through server", "make sure display still exists...error " + caught.toString(), null);
		}
		public void onSuccess(CorrugateDTO displayCorrugate) {
			this.corrugateModel.notify(new ChangeEvent(ChangeEventSource.Update, displayCorrugate));
			corrCompInfo.getDisplayTabs().setDisplayModel(displayModel);
			corrugateInfoGroup.setCorrugateDTO(displayCorrugate);
			corrCompInfo.setData("displayCorrugate", displayCorrugate);
			corrugateInfoGroup.resetDisplay(displayCorrugate);
			corrugateInfoGroup.setDisplayDTO(displayModel);
			corrCompEditorGroup.setDisplayDTO(displayModel);
			this.corrugateModel=displayCorrugate;
			corrCompEditorGroup.loadCorrugateComponents();
		}
	}
	
	public static class SaveCalculationSummary implements AsyncCallback<CalculationSummaryDTO> {
		private CalculationSummaryDTO calculationSummaryDTO ;
		private CostAnalysisInfo costAnalysisInfo;
		
		
		public SaveCalculationSummary(CostAnalysisInfo costAnalysisInfo,CalculationSummaryDTO calculationSummaryDTO ) {
			this.calculationSummaryDTO =calculationSummaryDTO;
			this.costAnalysisInfo=costAnalysisInfo;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("SaveCalculationSummary Failed", throwable.toString(), null);
		}
		public void onSuccess(CalculationSummaryDTO calculationSummaryDTO) {
			//MessageBox.alert("SaveCalculationSummary", "SaveCalculationSummary..success..", null);
			this.calculationSummaryDTO = calculationSummaryDTO;
		}
	}
	
	public static class DeleteManufacturerCountryDTO implements AsyncCallback<Void>{
		private CorrugateVendors corrugateVendors;
		private Button btn;
		public DeleteManufacturerCountryDTO(CorrugateVendors corrugateVendors, Button btn) {
			this.corrugateVendors = corrugateVendors;
			this.btn = btn;
		}
		public void onFailure(Throwable arg0) {
			this.btn.enable();
		}

		public void onSuccess(Void arg0) {
			this.btn.enable();
			corrugateVendors.fireEvent(Events.BeforeRemove);
		}
	}
	
	public static class LoadAdminData<T extends BaseModel> implements AsyncCallback<List<T>> {
		private AbstractAdminPanel<T> panel;
		public LoadAdminData(AbstractAdminPanel<T> panel) {
			this.panel = panel;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get data through server", "try again later...", null);
		}

		public void onSuccess(List<T> models) {
			this.panel.setData(models);
		}
	}
	
	public static class SetModelStore<T extends BaseModel> implements AsyncCallback<List<T>> {
		private ListStore<T> store;

		public SetModelStore(ListStore<T> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get data through server", "using client side data", null);
		}

		public void onSuccess(List<T> result) {
			if(result==null) return;
			this.store.add(result);
		}
	}

	public static class SetComboboxValues<T extends BaseModel> implements AsyncCallback<List<T>> {
		private ComboBox<T> combo;
		private Long activeId;
		private String idField;
		public SetComboboxValues(ComboBox<T> combo, Long activeId, String idField) {
			this.combo = combo;
			this.activeId = activeId;
			this.idField = idField;
		}
		
		public void onFailure(Throwable arg0) {
			MessageBox.alert("Failed to get data through server", "using client side data", null);
		}

		public void onSuccess(List<T> result) {
			combo.getStore().removeAll();
			combo.getStore().add(result);
			if(activeId == null || idField == null) return;
			for(T dto: combo.getStore().getModels()){
				if(activeId.equals(dto.get(idField))){
					combo.setValue(dto);
				}
			}
		}
	}

	public static class SaveAdminDTO<T extends BaseModel> implements AsyncCallback<T> {
		private AbstractAdminPanel<T> panel;
		public SaveAdminDTO(AbstractAdminPanel<T> panel) {
			this.panel = panel;
		}

		public void onFailure(Throwable throwable) {
			if (throwable instanceof DuplicateException) {
				MessageBox.alert("Failed to save", "Duplicate record found, please check item you are trying to save!", null);
				return;
			}
			MessageBox.alert("Failed to save", throwable.toString(), null);
		}

		public void onSuccess(T result) {
			panel.finishEdit(result);
		}
	}
	
	public static class DeleteAdminDTO<T extends BaseModel> implements AsyncCallback<Void>{
		private AbstractAdminPanel<T> panel;
		private T dto;
		
		public DeleteAdminDTO(AbstractAdminPanel<T> panel, T dto) {
			this.panel = panel;
			this.dto = dto;
		}
		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to delete", throwable.toString(), null);
			panel.finishDelete(dto, false);
		}

		public void onSuccess(Void result) {
			panel.finishDelete(dto, true);
		}
	}
	
	public static class LoadProgramBu implements AsyncCallback<List<ProgramBuDTO>>{
		private ListStore<ProgramBuDTO> store;

		public LoadProgramBu(ListStore<ProgramBuDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to get program bu through server", "try later", null);
		}

		public void onSuccess(List<ProgramBuDTO> result) {
			this.store.removeAll();
			this.store.add(result);
		}
	}
	
	public static class UpdateProgramBu implements AsyncCallback<List<ProgramBuDTO>>{
		private ListStore<ProgramBuDTO> store;

		public UpdateProgramBu(ListStore<ProgramBuDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save program bu", "Any changes will be rejected", null);
			store.rejectChanges();
		}

		public void onSuccess(List<ProgramBuDTO> result) {
			this.store.commitChanges();
		}
	}
	
	public static class UpdateProgramUploadedBu implements AsyncCallback<List<ProgramBuDTO>>{
		private ListStore<ProgramBuDTO> store;

		public UpdateProgramUploadedBu(ListStore<ProgramBuDTO> store) {
			this.store = store;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("Failed to save program bu", "Any changes will be rejected", null);
			store.rejectChanges();
		}

		public void onSuccess(List<ProgramBuDTO> result) {
			this.store.removeAll();
			this.store.add(result);
			this.store.commitChanges();
		}
	}
	
	
	public static class SaveProgramUploadBusForDisplays implements AsyncCallback<Void>{
		private MessageBox box;
		public SaveProgramUploadBusForDisplays(MessageBox box ) {
			this.box=box;
		}

		public void onFailure(Throwable throwable) {
			MessageBox.alert("BU Program Upload", "Bu Program % Upload for the selected displays are failed", null);
			if (box != null) box.close();
		}

		public void onSuccess(Void result) {
			MessageBox.alert("Bu Program Upload", "BU Program % Upload for the selected displays are successfully completed", null);
			if (box != null) box.close();
		}
	}
	
}
