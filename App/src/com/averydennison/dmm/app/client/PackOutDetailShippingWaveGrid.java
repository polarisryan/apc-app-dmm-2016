package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.KitComponent;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;

/**
 * User: Spart Arguello
 * Date: Oct 27, 2009
 * Time: 11:39:01 AM
 */
public class PackOutDetailShippingWaveGrid extends LayoutContainer {
    public PackOutDetailShippingWaveGrid() {
        setLayout(new FlowLayout(10));

        //int x = 100;
        int x = 125;

        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        ColumnConfig column = new ColumnConfig();
        column.setId("shippingDate");
        column.setHeader("Shipping Date");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x - 20);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("quantity");
        column.setHeader("Quantity");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x - 20);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("mustArriveBy");
        column.setHeader("Must Arrive By");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("comment");
        column.setHeader("Comment");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x * 2);
        configs.add(column);

        //TODO call server
        ListStore<KitComponent> store = new ListStore<KitComponent>();
        store.add(PackOutDetailShippingWaveGrid.getCostPricing());

        ColumnModel cm = new ColumnModel(configs);

        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        //cp.setHeading("Main Displays");
        cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
        cp.setLayout(new FitLayout());
        cp.setSize(700, 150);

        Grid<KitComponent> grid = new Grid<KitComponent>(store, cm);
        grid.setColumnResize(true);
        grid.setStyleAttribute("borderTop", "none");
        grid.setStripeRows(true);
        grid.setBorders(true);
        cp.add(grid);

        add(cp);
    }

    public static List<KitComponent> getCostPricing() {
        List<KitComponent> list = new ArrayList<KitComponent>();
        for (int i = 0; i < 1; i++) {
            list.add(new KitComponent());
        }
        return list;
    }
}
