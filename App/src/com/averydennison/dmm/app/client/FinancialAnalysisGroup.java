package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.google.gwt.i18n.client.NumberFormat;

public class FinancialAnalysisGroup {
	
	public ContentPanel createGroup(){
		
		ContentPanel financialAnalysisPanel = new ContentPanel();
		financialAnalysisPanel.setBodyBorder(false);
		financialAnalysisPanel.setHeaderVisible(false);
		financialAnalysisPanel.setLayout(new RowLayout());
		financialAnalysisPanel.setSize(225, 425);

		HorizontalPanel financialAnalysishp1 = new HorizontalPanel();
		HorizontalPanel financialAnalysishp2 = new HorizontalPanel();
		HorizontalPanel financialAnalysishp3 = new HorizontalPanel();
		HorizontalPanel financialAnalysishp4 = new HorizontalPanel();

		HorizontalPanel financialAnalysishp5 = new HorizontalPanel();
		HorizontalPanel financialAnalysishp6 = new HorizontalPanel();
		HorizontalPanel financialAnalysishp7 = new HorizontalPanel();
		HorizontalPanel financialAnalysishp8 = new HorizontalPanel();


		HorizontalPanel financialAnalysishp9 = new HorizontalPanel();
		HorizontalPanel financialAnalysishp10 = new HorizontalPanel();
		HorizontalPanel financialAnalysishp11 = new HorizontalPanel();
		HorizontalPanel financialAnalysishp12 = new HorizontalPanel();

		int x=50;
		
		LabelField corrugateCostsLabel = new LabelField("Corrugate Costs:");
		corrugateCostsLabel.setWidth((x*2)+30);
		financialAnalysishp1.add(corrugateCostsLabel);


		LabelField corrugateCosts = new LabelField();
		corrugateCosts.setText(NumberFormat.getFormat(("$###,###,##0.00")).format(164285.00));
		corrugateCosts.setStyleName("text-element-no-underline");
		corrugateCosts.setLabelSeparator(":");
		corrugateCosts.setWidth(x+30);
		financialAnalysishp1.add(corrugateCosts);

		LabelField fullfillmentCostsLabel = new LabelField("Fullfillment Costs:");
		fullfillmentCostsLabel.setWidth((x*2)+30);
		financialAnalysishp2.add(fullfillmentCostsLabel);


		LabelField fullfillmentCosts = new LabelField();
		fullfillmentCosts.setText(NumberFormat.getFormat(("$###,###,##0.00")).format(174000.00));
		fullfillmentCosts.setStyleName("text-element-no-underline");
		fullfillmentCosts.setLabelSeparator(":");
		fullfillmentCosts.setFieldLabel("Fullfillment Costs");
		fullfillmentCosts.setWidth(x+30);
		financialAnalysishp2.add(fullfillmentCosts);

		LabelField otherCostsLabel = new LabelField("Other Costs:");
		otherCostsLabel.setWidth((x*2)+30);
		financialAnalysishp3.add(otherCostsLabel);

		LabelField otherCosts = new LabelField();
		otherCosts.setText("$19,797.00");
		otherCosts.setLabelSeparator(":");
		otherCosts.setStyleName("text-element-underline");
		otherCosts.setFieldLabel("Other Costs");
		otherCosts.setWidth(x+30);
		financialAnalysishp3.add(otherCosts);

		LabelField totalDisplayCostsLabel = new LabelField("Total Display Costs:");
		totalDisplayCostsLabel.setWidth((x*2)+30);;
		financialAnalysishp4.add(totalDisplayCostsLabel);

		LabelField totalDisplayCosts = new LabelField();
		totalDisplayCosts.setText("$358,087.00");
		totalDisplayCosts.setLabelSeparator(":");
		totalDisplayCosts.setStyleName("text-element-no-underline");
		totalDisplayCosts.setFieldLabel("Total Display Costs");
		totalDisplayCosts.setWidth(x+30);
		financialAnalysishp4.add(totalDisplayCosts);

		LabelField tradeSalesLabel = new LabelField("Trade Sales:");
		tradeSalesLabel.setWidth((x*2)+30);
		financialAnalysishp5.add(tradeSalesLabel);

		LabelField tradeSales = new LabelField();
		tradeSales.setText("$786,593.00");
		tradeSales.setLabelSeparator(":");
		tradeSales.setStyleName("text-element-no-underline");
		tradeSales.setFieldLabel("Trade Sales");
		tradeSales.setWidth(x+30);
		financialAnalysishp5.add(tradeSales);

		LabelField custProgramDiscLabel = new LabelField("Cust Program Disc:");
		custProgramDiscLabel.setWidth((x*2)+30);
		financialAnalysishp6.add(custProgramDiscLabel);


		LabelField custProgramDisc = new LabelField();
		custProgramDisc.setStyleName("text-element-underline");
		custProgramDisc.setText(NumberFormat.getFormat(("$###,###,##0.00")).format(0.00));
		custProgramDisc.setWidth(x+30);
		custProgramDisc.setLabelSeparator(":");
		custProgramDisc.setFieldLabel("Cust Program Disc");
		financialAnalysishp6.add(custProgramDisc);

		LabelField netSalesLabel = new LabelField("Net Sales:");
		netSalesLabel.setWidth((x*2)+30);
		financialAnalysishp7.add(netSalesLabel);


		LabelField netSales = new LabelField();
		netSales.setText(NumberFormat.getFormat(("$###,###,##0.00")).format(786593.00));
		netSales.setLabelSeparator(":");
		netSales.setStyleName("text-element-no-underline");
		netSales.setFieldLabel("Net Sales");
		netSales.setWidth(x+30);
		financialAnalysishp7.add(netSales);

		LabelField cOGSLabel = new LabelField("(COGS):");
		cOGSLabel.setWidth((x*2)+30);
		financialAnalysishp8.add(cOGSLabel);

		LabelField cOGS = new LabelField();
		cOGS.setText(NumberFormat.getFormat(("$###,###,##0.00")).format(223309.00));
		cOGS.setLabelSeparator(":");
		cOGS.setFieldLabel("(COGS)");
		cOGS.setStyleName("text-element-underline");
		cOGS.setWidth(x+30);
		financialAnalysishp8.add(cOGS);

		LabelField productVarMarginLabel = new LabelField("$ Product Var Margin:");
		productVarMarginLabel.setWidth((x*2)+30);
		financialAnalysishp9.add(productVarMarginLabel);

		LabelField productVarMargin = new LabelField();
		productVarMargin.setText(NumberFormat.getFormat(("$###,###,##0.00")).format(2244309.00));
		productVarMargin.setLabelSeparator(":");
		productVarMargin.setStyleName("text-element-no-underline");
		productVarMargin.setFieldLabel("$ Product Var Margin");
		productVarMargin.setWidth(x+30);
		financialAnalysishp9.add(productVarMargin);

		LabelField displayCostsLabel = new LabelField("Display Costs:");
		displayCostsLabel.setWidth((x*2)+30);
		financialAnalysishp10.add(displayCostsLabel);

		LabelField displayCosts = new LabelField();
		displayCosts.setText(NumberFormat.getFormat(("$###,###,##0.00")).format(28809.00));
		displayCosts.setLabelSeparator(":");
		displayCosts.setStyleName("text-element-underline");
		displayCosts.setFieldLabel("Display Costs");
		displayCosts.setWidth(x+30);
		financialAnalysishp10.add(displayCosts);

		LabelField vmAmountLabel = new LabelField("VM $:");
		vmAmountLabel.setWidth((x*2)+30);
		financialAnalysishp11.add(vmAmountLabel);

		LabelField vmAmount = new LabelField();
		vmAmount.setText(NumberFormat.getFormat(("$###,###,##0.00")).format(2609.00));
		vmAmount.setLabelSeparator(":");
		vmAmount.setStyleName("text-element-no-underline");
		vmAmount.setFieldLabel("VM $");
		vmAmount.setWidth(x+30);
		financialAnalysishp11.add(vmAmount);

		LabelField vmPercentageLabel = new LabelField("VM %:");
		vmPercentageLabel.setWidth((x*2)+30);
		financialAnalysishp12.add(vmPercentageLabel);

		LabelField vmPercentage = new LabelField();
		vmPercentage.setText(NumberFormat.getFormat(("$###,###,##0.00")).format(709.00));
		vmPercentage.setLabelSeparator(":");
		vmPercentage.setStyleName("text-element-no-underline");
		vmPercentage.setFieldLabel("VM %");
		vmPercentage.setWidth(x+30);
		financialAnalysishp12.add(vmPercentage);





		LabelField spaceLabel = new LabelField();
		spaceLabel.setLabelSeparator(" ");
		spaceLabel.setFieldLabel(" ");
		spaceLabel.setWidth(x+30);

		LabelField spaceLabel1 = new LabelField();
		spaceLabel1.setLabelSeparator(" ");
		spaceLabel1.setFieldLabel(" ");
		spaceLabel1.setWidth(x+30);

		LabelField spaceLabel2 = new LabelField();
		spaceLabel2.setLabelSeparator(" ");
		spaceLabel2.setFieldLabel(" ");
		spaceLabel2.setWidth(x+30);

		LabelField spaceLabel3 = new LabelField();
		spaceLabel3.setLabelSeparator(" ");
		spaceLabel3.setFieldLabel(" ");
		spaceLabel3.setWidth(x+30);

		LabelField spaceLabel4 = new LabelField();
		spaceLabel4.setLabelSeparator(" ");
		spaceLabel4.setFieldLabel(" ");
		spaceLabel4.setWidth(x+30);


		financialAnalysishp1.setSpacing(2);
		financialAnalysishp2.setSpacing(2);
		financialAnalysishp3.setSpacing(2);
		financialAnalysishp4.setSpacing(2);

		financialAnalysishp5.setSpacing(2);
		financialAnalysishp6.setSpacing(2);
		financialAnalysishp7.setSpacing(2);
		financialAnalysishp8.setSpacing(2);

		financialAnalysishp9.setSpacing(2);
		financialAnalysishp10.setSpacing(2);
		financialAnalysishp11.setSpacing(2);
		financialAnalysishp12.setSpacing(2);


		financialAnalysisPanel.add(financialAnalysishp1);
		financialAnalysisPanel.add(financialAnalysishp2);
		financialAnalysisPanel.add(financialAnalysishp3);
		financialAnalysisPanel.add(financialAnalysishp4);
		financialAnalysisPanel.add(financialAnalysishp5);
		financialAnalysisPanel.add(financialAnalysishp6);
		financialAnalysisPanel.add(financialAnalysishp7);
		financialAnalysisPanel.add(financialAnalysishp8);
		financialAnalysisPanel.add(financialAnalysishp9);
		financialAnalysisPanel.add(financialAnalysishp10);
		financialAnalysisPanel.add(financialAnalysishp11);
		financialAnalysisPanel.add(financialAnalysishp12);
		
		return financialAnalysisPanel;

	}

}
