package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDcAndPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.client.util.KeyUtil;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.dnd.GridDragSource;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * User: Spart Arguello
 * Date: Oct 26, 2009
 * Time: 10:26:16 AM
 */
public class PackoutDetailInfoGrid extends LayoutContainer implements ValidationHelper {

    private PackoutDetailInfo packoutDetailInfo;
    private final ListStore<DisplayShipWaveDTO> displayShippingWaveStore = new ListStore<DisplayShipWaveDTO>();
    private EditorGrid<DisplayShipWaveDTO> editorGrid;

    // validation properties
    private Long sumShipWave;
    private boolean displayShipWaveModified=false;
    private HashMap<Integer, Long> shipWaveQty = new HashMap<Integer, Long>();

    private Button btnAdd;
    private Button btnDelete;
    
    public Boolean isDirty() {
    	return displayShipWaveModified;
    }
    
    public PackoutDetailInfoGrid(PackoutDetailInfo packoutDetailInfo) {
        this.packoutDetailInfo = packoutDetailInfo;
        setLayout(new FlowLayout(0));
        //int x = 75;
        int x = 95;
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        ColumnConfig column = new ColumnConfig();
//        RowNumberer rowNumberer = new RowNumberer();
//        rowNumberer.setHeader("Shipping Wave");

//        rowNumberer.setWidth(x-15);
//        configs.add(rowNumberer);

//        CheckBoxSelectionModel<DisplayShipWaveDTO> checkBoxSelectionModel = new CheckBoxSelectionModel<DisplayShipWaveDTO>();
//        configs.add(checkBoxSelectionModel.getColumn());

        column.setId(DisplayShipWaveDTO.SHIP_WAVE_NUM);
        column.setHeader("<center>Ship<br>Wave</center>");

        column.setAlignment(Style.HorizontalAlignment.CENTER);
        column.setWidth(x - 20);
        column.setSortable(false);
        column.setMenuDisabled(true);
        TextField<String> textField = new TextField<String>();
        configs.add(column);

        Boolean bDisplayReadOnly;
        if(packoutDetailInfo.getData("readOnly")==null){
        	bDisplayReadOnly=Boolean.FALSE;
        }else{ 
        	bDisplayReadOnly=packoutDetailInfo.getData("readOnly");
        }
        DateField dateField = new DateField();
        dateField.getPropertyEditor().setFormat(DateTimeFormat.getShortDateFormat());
        column = new ColumnConfig();
        column.setId(DisplayShipWaveDTO.MUST_ARRIVE_DATE);
        column.setHeader("<center>Must<br>Arrive<br>By Date</center>");
        column.setAlignment(HorizontalAlignment.CENTER);
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        if(!bDisplayReadOnly){
        	column.setEditor(new CellEditor(dateField));
        }
        column.setAlignment(Style.HorizontalAlignment.CENTER);
        column.setWidth(x+10);
        column.setMenuDisabled(true);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayShipWaveDTO.QUANTITY);
        column.setHeader("<center>Qty</center>");
        column.setWidth(x - 20);
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setSortable(false);//todo fix String to Long cast exception
        column.setMenuDisabled(true);

        final TextField<Long> qty = new TextField<Long>();
        qty.setMinLength(1);
        qty.setMaxLength(10);
        qty.setAllowBlank(false);
        qty.setValidator(new Validator() {
            public String validate(Field<?> field, String value) {
                if (Long.valueOf(value.trim()).equals(0L)) return "invalid qty";
                return null;
            }
        });
        qty.setAutoValidate(true);
//        qty.addListener(Events.KeyUp, new Listener<FieldEvent>() {
//            public void handleEvent(FieldEvent be) {
//                if (qty.validate()) {
//                    qty.setValue(Long.valueOf(qty.getValue()));
//                }
//            }
//        });
        qty.addListener(Events.KeyDown, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
                int keyCode = ke.getKeyCode();
                if (!KeyUtil.isDigit(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress()) {
                    ke.stopEvent();
                }
            }
        });
        CellEditor qtyCellEditor = new CellEditor(qty);
        if(!bDisplayReadOnly){
        	column.setEditor(qtyCellEditor);
        }
        configs.add(column);
        CheckColumnConfig checkColumn = new CheckColumnConfig(DisplayShipWaveDTO.DESTINATION_FLG, "<center>Off<br>Shore</center>", 60);
        checkColumn.setAlignment(HorizontalAlignment.CENTER);
        CellEditor checkBoxEditor = new CellEditor(new CheckBox());
        if(!bDisplayReadOnly){
        	checkColumn.setEditor(checkBoxEditor);
        }
        configs.add(checkColumn);
        column = new ColumnConfig();
        column.setId(DisplayShipWaveDTO.PO_QTY);
        column.setHeader("<center>PO<br>Qty</center>");
        column.setAlignment(HorizontalAlignment.CENTER);
        column.setWidth(x - 25);
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setSortable(false);
        column.setMenuDisabled(true);
        configs.add(column);
        
        //TODO call server

        //store.add(PackoutDetailInfoGrid.getDisplayLogistics());

        ColumnModel cm = new ColumnModel(configs);
        ContentPanel cp = new ContentPanel();
        cp.setHeaderVisible(false);
        cp.setBodyBorder(false);
        cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
        cp.setLayout(new FitLayout());
        //cp.setSize(310, 300);
        //cp.setSize(435, 300);
        cp.setSize(400, 300);

        GridSelectionModel<DisplayShipWaveDTO> gridSelectionModel = new GridSelectionModel<DisplayShipWaveDTO>();
        editorGrid = new EditorGrid<DisplayShipWaveDTO>(displayShippingWaveStore, cm);
        editorGrid.setColumnResize(true);
        editorGrid.setStyleAttribute("borderTop", "none");
        editorGrid.setStripeRows(true);
        editorGrid.setBorders(true);
        editorGrid.setTrackMouseOver(false);
        editorGrid.setSelectionModel(gridSelectionModel);
        editorGrid.addPlugin(checkColumn);
        GridDragSource gridDragSource = new GridDragSource(editorGrid);
        cp.add(editorGrid);
        SelectionListener add = new SelectionListener<ComponentEvent>() {
            public void componentSelected(ComponentEvent ce) {
                editorGrid.stopEditing();
                DisplayShipWaveDTO d = new DisplayShipWaveDTO();
                d.setDisplayId(getDisplayId());
                d.setShipWave(findLatestShipWaveId(displayShippingWaveStore) + 1);
                d.setMustArriveDate(new Date());
                d.setQuantity(getActualForecastQuantity());
                displayShippingWaveStore.add(d);
                editorGrid.startEditing(displayShippingWaveStore.getCount() - 1, 0);
                setDisplayShipWaveModified(true);
            }
        };
        SelectionListener delete = new SelectionListener<ComponentEvent>() {
            public void componentSelected(ComponentEvent ce) {
                editorGrid.stopEditing();
                List<DisplayDcAndPackoutDTO> displayDcAndPackoutList = getPackoutDetailInfo().packoutDetailsTree.getDisplayDcAndPackoutList();
                boolean bRecExsits=false;
                if(displayDcAndPackoutList!=null && displayDcAndPackoutList.size()>0){ 
                	for (int i = 0; i < displayDcAndPackoutList.size(); i++) {
	                	DisplayDcAndPackoutDTO dapDTO = (DisplayDcAndPackoutDTO)displayDcAndPackoutList.get(i);
	                	if(	( dapDTO.getShipWaveNum() !=null) && 
	                			(dapDTO.getShipWaveNum() == editorGrid.getSelectionModel().getSelectedItem().getShipWaveNum().longValue()) && 
	                			(!dapDTO.getDeleteFlg().equalsIgnoreCase("Y"))){
	                			bRecExsits=true;
	                	}
                	}
                }
                if(bRecExsits){
                	System.out.println("Record Exists");
                	MessageBox.alert("Delete?", "Ship wave is attached to a packout. Cannot delete the ship wave", null);
                }else{
                	System.out.println("Record Not Exists");
                	if(editorGrid.getSelectionModel().getSelectedItem()!=null)
                		deleteDisplayShippingWaves(displayShippingWaveStore,editorGrid.getSelectionModel().getSelectedItem());
                }
            }
        };
        btnAdd=new Button("Add", add);
        btnDelete=new Button("Delete", delete);
        if(bDisplayReadOnly){
        	btnAdd.disable();
        	btnDelete.disable();
        }
        cp.addButton(btnAdd);
        cp.addButton(btnDelete);
        add(cp);
    }

    private Long getDisplayId() {
        Long id = packoutDetailInfo.getDisplayTabs().getDisplayModel().getDisplayId();
        return id;
    }
    
    private Integer findLatestShipWaveId(ListStore<DisplayShipWaveDTO>  shippingWaveStore){
    	List<DisplayShipWaveDTO> displayShipWaveList = shippingWaveStore.getModels();
    	DisplayShipWaveDTO dapDTO=null;
    	List<Integer> shipWaveIdList=new ArrayList<Integer>();
        if(displayShipWaveList!=null && displayShipWaveList.size()>0){ 
        	dapDTO =displayShipWaveList.get(displayShipWaveList.size()-1);
        	return dapDTO.getShipWaveNum();
        }else{
        	return new Integer(0);/* First Row */
        }
    }
    
    public PackoutDetailInfoGrid getPackoutDetailInfoShippingWavesGrid() {
        return this;
    }

    
    
    public void  deleteDisplayShippingWaves(ListStore<DisplayShipWaveDTO> displayShippingWaveStore,DisplayShipWaveDTO displayShipWaveDTO){
    	
    	 setDisplayShipWaveModified(true);
    	 final MessageBox box = MessageBox.wait("Progress",
                 "Deleting ship wave info, please wait...", "Deleting...");
         AppService.App.getInstance().deleteDisplayShipWaves(displayShipWaveDTO,
                 new Service.DeleteDisplayShipWaveDTOList(getPackoutDetailInfoShippingWavesGrid(),displayShippingWaveStore,displayShipWaveDTO, box));
         
    }
    
    public void saveDisplayShippingWaves() {
        List<DisplayShipWaveDTO> list = this.displayShippingWaveStore.getModels();
        final MessageBox box = MessageBox.wait("Progress",
                "Saving packout detail info, please wait...", "Saving...");
        AppService.App.getInstance().saveDisplayShipingWaveDTOListTest(list,
                new Service.SaveDisplayShippingWaveDTOList(packoutDetailInfo, box));
    }

    public void reload() {
    	packoutDetailInfo.loadDisplayShippingWaves();
	}
    
    public EditorGrid<DisplayShipWaveDTO> getEditorGrid() {
        return editorGrid;
    }

    public PackoutDetailInfo getPackoutDetailInfo() {
		return packoutDetailInfo;
	}

	public void setPackoutDetailInfo(PackoutDetailInfo packoutDetailInfo) {
		this.packoutDetailInfo = packoutDetailInfo;
	}

	public ListStore<DisplayShipWaveDTO> getStore() {
        return displayShippingWaveStore;
    }

    public Long getDisplayShippingWaveSum() {
    	return sumShipWave;
    }

    public HashMap<Integer, Long> getShipWaveQty() {
    	return shipWaveQty;
    }
    
    public boolean isDisplayShipWaveModified() {
		return displayShipWaveModified;
	}

	public void setDisplayShipWaveModified(boolean displayShipWaveModified) {
		this.displayShipWaveModified = displayShipWaveModified;
	}

	public void getQuantities() {
        sumShipWave = 0L;
        shipWaveQty.clear();
        List<DisplayShipWaveDTO> list = this.displayShippingWaveStore.getModels();
        for (int i = 0; i < list.size(); i++) {
        	Integer shipWaveNum = list.get(i).getShipWaveNum();
        	Long qty = list.get(i).getQuantity();
        	if (qty != null) {
	        	sumShipWave += qty;
	        	shipWaveQty.put(shipWaveNum, qty);
        	}
        }
    }

    public boolean validateControls() {
        this.editorGrid.stopEditing();
        this.editorGrid.getStore().commitChanges();
        if (this.editorGrid.getStore().getModels().size() == 0 && this.getDisplayShippingWaveSum() == null) return false;
        if (this.editorGrid.getStore().getModels().size() == 0 && this.getDisplayShippingWaveSum() == 0L) return true;
        if (this.editorGrid.getStore().getModifiedRecords().size() == 0) return true;
        if ((this.editorGrid.getStore().getModels().size() > 0 && this.getDisplayShippingWaveSum() > 0)) {
            for (int i = 0; i < this.editorGrid.getStore().getCount(); i++) {
                if (!this.editorGrid.getColumnModel().getColumn(2).getEditor().getField().validate()) return false;
            }
            return true;
        }
        return true;
    }

    private Long getActualForecastQuantity() {
        return packoutDetailInfo.getActualForecastQuantity();
    }
}
