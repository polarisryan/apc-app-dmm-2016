package com.averydennison.dmm.app.client;

import java.util.Date;

import com.averydennison.dmm.app.client.models.CorrugateDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.DateTimePropertyEditor;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;

public class CorrugateInfoGroup {
	private  DateField dateEntered;
	/*private  DateField currugateDueDate ;
	private  DateField actualDeliveryDate ;*/
	private  ComboBox<StructureDTO> structureType;
	private  NumberField actualQty;
	public  CurrencyField corrugateCost;
	public static final String datePattern = "MM/dd/yyyy";
	public static final String currencyPattern="$#,###,###0.00";
	public DisplayDTO displayDTO;
	public CorrugateComponentInfo corrugateComponentInfo;
	public CorrugateDTO corrugateDTO;
	private void setEmptyText() {
		dateEntered.setEmptyText("Date Entered");
	/*	currugateDueDate.setEmptyText("Corrugate Due Date...");
		actualDeliveryDate.setEmptyText("Actual Delivery Date...");*/
		structureType.setEmptyText("Display Types...");
		actualQty.setEmptyText("Actual Displays...");
		corrugateCost.setEmptyText("0.00");
	}
	public CorrugateInfoGroup(){
		 
	}
	public CorrugateDTO getCorrugateDTO() {
		return corrugateDTO;
	}

	public void setCorrugateDTO(CorrugateDTO corrugateDTO) {
		this.corrugateDTO = corrugateDTO;
	}
	
	public DisplayDTO getDisplayDTO() {
		return displayDTO;
	}

	public void setDisplayDTO(DisplayDTO displayDTO) {
		this.displayDTO = displayDTO;
	}
	
	public CorrugateComponentInfo getCorrugateComponentInfo() {
		return corrugateComponentInfo;
	}

	public void setCorrugateComponentInfo(CorrugateComponentInfo corrugateComponentInfo) {
		this.corrugateComponentInfo = corrugateComponentInfo;
	}
	
	public void resetDisplay( CorrugateDTO corrugateDTO){
		if(corrugateDTO==null) {
			dateEntered.setValue(new Date());
			corrugateCost.setValue(0.00);
			return;
		}
		if(corrugateDTO.getCreateDate()!=null){
			dateEntered.setValue(corrugateDTO.getCreateDate());
			dateEntered.setEditable(false);
			dateEntered.setEnabled(false);
			corrugateCost.setValue(0.00);
		}else{
			dateEntered.setEditable(true);
			dateEntered.setEnabled(true);
			dateEntered.setValue(new Date());
		}
		/*actualDeliveryDate.setValue(corrugateDTO.getDeliveryDate());
		currugateDueDate.setValue(corrugateDTO.getCorrugateDueDate());*/
		corrugateCost.setValue(corrugateDTO.getCorrugateCost()==null?0.00:corrugateDTO.getCorrugateCost());
	}
	
	public ContentPanel cp;
	public ContentPanel createCorrugateInfoGroup(CorrugateDTO corrugateDTO, Boolean readOnly){
		dateEntered = new DateField();
		/*actualDeliveryDate = new DateField();
		currugateDueDate = new DateField();*/
		structureType = new ComboBox<StructureDTO>();
	    actualQty = new NumberField();
	    
	    corrugateCost= new CurrencyField();
	    corrugateCost.setMaxLength(11);
	    corrugateCost.setAutoValidate(true);
	    corrugateCost.addInputStyleName("text-element-no-underline");	
		corrugateCost.setEditable(false);
		corrugateCost.setEnabled(false);
		if(readOnly){
			dateEntered.setEditable(false);
			dateEntered.setEnabled(false);
			corrugateCost.setEditable(false);
			corrugateCost.setEnabled(false);
		}
		cp = new ContentPanel();
		cp.setBodyBorder(false);
		cp.setHeaderVisible(false);
		cp.setLayout(new RowLayout());
		cp.setSize(525, 50);
		HorizontalPanel hp1 = new HorizontalPanel();
		cp.add(hp1);
		ListStore<StructureDTO> structures = new ListStore<StructureDTO>();
		AppService.App.getInstance().getStructureDTOs(new Service.SetStructuresStore(structures));
		
		structureType.setFieldLabel("Structure Type");
	    structureType.setDisplayField(StructureDTO.STRUCTURE_NAME);
	    structureType.setWidth(100);
	    structureType.setStore(structures);
	    structureType.setAllowBlank(false);
	    structureType.setEditable(false);
	    structureType.setTriggerAction(ComboBox.TriggerAction.ALL);
        final DateTimePropertyEditor dateTimePropertyEditor = new DateTimePropertyEditor(datePattern);

		structureType.setWidth(85+23);
		dateEntered.setWidth(82+23);
/*		actualDeliveryDate.setWidth(82+23);
		currugateDueDate.setWidth(82+23);
*/		structureType.setWidth(85+23);
		actualQty.setWidth(85+23);
		corrugateCost.setWidth(82+23);
		corrugateCost.setStyleAttribute("alignment", "right");
		
		dateEntered.setPropertyEditor(dateTimePropertyEditor);
/*		actualDeliveryDate.setPropertyEditor(dateTimePropertyEditor);
		currugateDueDate.setPropertyEditor(dateTimePropertyEditor);*/
		LabelField buDescLabel = new LabelField("Date Entered");
		buDescLabel.setWidth(120);
		hp1.add(buDescLabel);
		hp1.add(dateEntered);
		
		HorizontalPanel spacer1 = new HorizontalPanel();
		spacer1.setWidth(35);
		hp1.add(spacer1);
		buDescLabel = new LabelField("Approved Corrugate Cost");
		buDescLabel.setWidth(150);
		hp1.add(buDescLabel);
		hp1.add(corrugateCost);
		setEmptyText();
		return cp;
	}
	
	public void saveCorrugate(DisplayDTO displayModel,TabPanel panel, TabItem nextTab){
		System.out.println("CorrugateInfoGroup.saveCorrugate starts");
		if (displayModel.getDisplayId() == null) {
			MessageBox.alert("Validation Error", "No display found" , null);
			return;
		}else{
			setDisplayDTO(displayModel);
		}
		CorrugateDTO corrugateDTO=getCorrugateDTO();
		if(corrugateDTO==null){
			corrugateDTO=new CorrugateDTO();
		}
		corrugateDTO.setDisplayId(displayModel.getDisplayId());
		if(dateEntered.getValue()!=null)  {
			corrugateDTO.setCreateDate(dateEntered.getValue());
		}else{
			corrugateDTO.setCreateDate(new Date());
		}
		/*if(actualDeliveryDate.getValue() !=null){
			corrugateDTO.setDeliveryDate(actualDeliveryDate.getValue());
		}
		if(currugateDueDate.getValue() !=null){
			corrugateDTO.setCorrugateDueDate(currugateDueDate.getValue());
		}*/
		if(corrugateCost.getValue() !=null){
			corrugateDTO.setCorrugateCost(corrugateCost.getValue().floatValue());
		}
		getCorrugateComponentInfo().saveCorrugate(corrugateDTO,panel,nextTab);
	}
	
	public CorrugateDTO getCurrentCorrugateModelFromControls(){
		CorrugateDTO controlModel = new CorrugateDTO();
		System.out.println("+++CorrugateInfoGroup:getCurrentCorrugateModelFromControls() starts");
		try{
			if(dateEntered.getValue()!=null) controlModel.setCreateDate(dateEntered.getValue());
			/*if(actualDeliveryDate.getValue()!=null) controlModel.setDeliveryDate(actualDeliveryDate.getValue());
			if(currugateDueDate.getValue()!=null) controlModel.setCorrugateDueDate(currugateDueDate.getValue());*/
			if(corrugateCost.getValue()!=null) controlModel.setCorrugateCost(corrugateCost.getValue().floatValue());
		}catch(Exception ex){
			System.out.println("=======CorrugateInfoGroup:getCurrentCorrugateModelFromControls() error "+ ex.toString());
		}
		System.out.println("CorrugateInfoGroup:getCurrentCorrugateModelFromControls() ends");
		return controlModel;
	}
	
}
