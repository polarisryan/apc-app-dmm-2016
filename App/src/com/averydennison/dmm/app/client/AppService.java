package com.averydennison.dmm.app.client;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.averydennison.dmm.app.client.exception.DuplicateException;
import com.averydennison.dmm.app.client.models.BooleanDTO;
import com.averydennison.dmm.app.client.models.BuCostCenterSplitDTO;
import com.averydennison.dmm.app.client.models.CalculationSummaryDTO;
import com.averydennison.dmm.app.client.models.CorrugateComponentsDetailDTO;
import com.averydennison.dmm.app.client.models.CorrugateDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisHistoryDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.averydennison.dmm.app.client.models.CostPriceDTO;
import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.CstBuPrgmPctDTO;
import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.CustomerMarketingDTO;
import com.averydennison.dmm.app.client.models.CustomerTypeDTO;
import com.averydennison.dmm.app.client.models.DisplayCostCommentDTO;
import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.client.models.DisplayCostImageDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayDcAndPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayDcDTO;
import com.averydennison.dmm.app.client.models.DisplayForMassUpdateDTO;
import com.averydennison.dmm.app.client.models.DisplayLogisticsDTO;
import com.averydennison.dmm.app.client.models.DisplayPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayPlannerDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.client.models.InPogDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.LocationDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerDTO;
import com.averydennison.dmm.app.client.models.ProductDetailDTO;
import com.averydennison.dmm.app.client.models.ProgramBuDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodDTO;
import com.averydennison.dmm.app.client.models.SlimPackoutDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.averydennison.dmm.app.client.models.UserDTO;
import com.averydennison.dmm.app.client.models.UserTypeDTO;
import com.averydennison.dmm.app.client.models.ValidBuDTO;
import com.averydennison.dmm.app.client.models.ValidLocationDTO;
import com.averydennison.dmm.app.client.models.ValidPlannerDTO;
import com.averydennison.dmm.app.client.models.ValidPlantDTO;
import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * User: Spart Arguello
 * Date: Oct 13, 2009
 * Time: 2:14:16 PM
 */
@RemoteServiceRelativePath("gwt-rpc/appservice.rpc")
public interface AppService extends RemoteService {

    String getMessage(String msg);

    List<DisplayDTO> getDisplays();
    
    List<DisplayForMassUpdateDTO> getSelectedDisplaysForMassUpdate(List<Long> parameters) ;
    
    List<DisplayForMassUpdateDTO>  massUpdateSelectedItems( String userName, List<Long> parameters, DisplayForMassUpdateDTO displayMassUpdateDTO);
    
    List<StatusDTO> getStatusDTOs();

    List<CustomerDTO> getCustomerDTOs();
    
    List<CustomerCountryDTO> getCustomerCountryDTOs();

    List<StructureDTO> getStructureDTOs();

    List<DisplaySalesRepDTO> getDisplaySalesRepDTOs();

    List<PromoPeriodDTO> getPromoPeriodDTOs();
    List<PromoPeriodCountryDTO> getPromoPeriodCountryDTOs();
    
    Map<Long, ManufacturerDTO> getManufacturerMapDTOs();
    Map<Long, ManufacturerCountryDTO> getManufacturerCountryMapDTOs();

    DisplayDTO saveDisplayDTO(DisplayDTO display);
    
    DisplayDTO copyDisplayDTO(DisplayDTO displayDTO, String user) ;

    Map<Long, StatusDTO> getStatusDTOMap();
    
    Map<Long, LocationCountryDTO> getLocationCountryDTOMap();
    Map<Long, LocationDTO> getLocationDTOMap();

    Map<Long, CustomerCountryDTO> getCustomerCountryDTOMap();
    Map<Long, CustomerDTO> getCustomerDTOMap();

    DisplayDTO getDisplayDTO(DisplayDTO displayDTO);

    CustomerMarketingDTO getCustomerMarketingDTO(Long displayId);

    Map<Long, PromoPeriodDTO> getPromoPeriodMapDTOs();
    Map<Long, PromoPeriodCountryDTO> getPromoPeriodCountryMapDTOs();
    
    Map<Long, LocationDTO> getLocationMapDTOs();
    Map<Long, LocationCountryDTO> getLocationCountryMapDTOs();

    DisplayLogisticsDTO getDisplayLogisticsInfoDTO(Long displayId);

    DisplayLogisticsDTO saveDisplayLogisticsInfoDTO(DisplayLogisticsDTO displayLogistics, DisplayDTO display);

    CustomerMarketingDTO saveCustomerMarketingDTO(CustomerMarketingDTO customerMarketing, DisplayDTO display);

    List<LocationDTO> loadLocationDTOs();
    List<LocationCountryDTO> loadLocationCountryDTOs();

    Map<Long, DisplayDcDTO> findDisplayDcByDisplay(DisplayDTO display);

    Map<Long, DisplayDcDTO> saveDisplayDcDTOList(List<DisplayDcDTO> displayDcDTOList);

    Map<Long, DisplayShipWaveDTO> saveDisplayShipingWaveDTOList(List<DisplayShipWaveDTO> displayShipWaveDTOList);

    Map<Long, DisplayShipWaveDTO> loadDisplayShippingWaveDTOMap(DisplayDTO display);

    List<DisplayPackoutDTO> saveDisplayPackoutDTOList(List<DisplayPackoutDTO> list);

    Map<Long, DisplayPackoutDTO> loadDisplayPackouts(DisplayDcDTO displayDcDTO);

    List<KitComponentDTO> getKitComponents(DisplayDTO display, DisplayScenarioDTO displayScenarioDTO);

    KitComponentDTO getKitComponentDTO(Long kitComponentId, DisplayDTO display, DisplayScenarioDTO displayScenarioDTO);

    KitComponentDTO saveKitComponentDTO(KitComponentDTO kitComponent, DisplayDTO display, List<DisplayPlannerDTO> store);
    
    List<KitComponentDTO> saveKitComponentDTOList(List<KitComponentDTO> list);

    Boolean deleteKitComponent(KitComponentDTO kitComponent);

    List<DisplayPlannerDTO> getDisplayPlanners(KitComponentDTO kitComponent, DisplayDTO display);

    DisplayPlannerDTO getDisplayPlanner(KitComponentDTO kitComponent, DisplayPlannerDTO dpDTO);

    List<CostPriceDTO> getProductDetail(String sCountryName,  KitComponentDTO kitComponent);
    
    List<CostPriceDTO> getProductDetailForFGSKU(String sCountryName,  KitComponentDTO kitComponent);

    List<DisplayPackoutDTO> loadDisplayPackoutList(DisplayDcDTO displayDcDTO);

    List<DisplayDcAndPackoutDTO> loadDisplayDcAndPackouts(DisplayDTO display);
    
    List<DisplayShipWaveDTO> saveDisplayShipingWaveDTOListTest(List<DisplayShipWaveDTO> displayShipWaveDTOList);

    List<DisplayShipWaveDTO> loadDisplayShippingWaveDTOList(DisplayDTO displayDTO);

    List<SlimPackoutDTO> loadSlimDisplayPackoutGrid(DisplayDTO displayDTO);

    DisplayDTO savePackoutData(DisplayDTO displayModel, List<DisplayDcAndPackoutDTO>displayDcAndPackoutList, List<DisplayShipWaveDTO>displayShippingWaveList);

    List<ValidPlantDTO> getValidPlantDTOs();

    List<ValidPlannerDTO> getValidPlannerDTOs();

    List<ValidLocationDTO> getValidLocationDTOs();
    
    List<LocationDTO> getLocationDTOs();
    List<LocationCountryDTO> getLocationCountryDTOs();

    List<ValidBuDTO> getValidBuDTOs();

    List<ProductDetailDTO> getProductDetail( String sku);

    UserDTO getUserInfo();

    //todo
    DisplayDTO saveCopiedDisplayDTO(DisplayDTO display);
    
    BooleanDTO isDuplicateSKU(String sku);
    
    List<ManufacturerDTO> getManufacturerDTOs();
    List<ManufacturerCountryDTO> getManufacturerCountryDTOs();
	List<ManufacturerCountryDTO> getAllManufacturerCountryDTOs();
    
    List<StatusDTO> getRYGStatusDTOs();
    
    void setFilters(Map<String, BaseModelData> filters);
    
    String getServletPath();
    
    void callDisplayDeleteStoredProcedure(Long id, String username);
    
    Boolean deleteDisplayShipWaves(DisplayShipWaveDTO displayShipWaveDTO);
    Boolean deleteCostAnalysisKitComponent(
			CostAnalysisKitComponentDTO costAnalysisKitComponent);
    
    List<KitComponentDTO> switchOnDisplayTypesInKitComponent(DisplayDTO model,	boolean applyKitVerSetup, boolean applyFGDisplayRef);
    List<CostAnalysisKitComponentDTO> getCostAnalysisKitComponentInfo(DisplayScenarioDTO displayScenarioDTO, Double customerProgramPct);
	List<CostAnalysisKitComponentDTO> saveCostAnalysisKitComponentInfo( List<CostAnalysisKitComponentDTO> costAnalysisDTOList, Long qtyForecast, Double custProgramPct);
	DisplayCostDTO saveDisplayCostDTO(DisplayCostDTO displayCostModel);
	List<DisplayCostCommentDTO> getDisplayCostCommentDTO(Long displayScenarioId);
	DisplayCostCommentDTO saveDisplayCostCommentDTO(DisplayCostCommentDTO displayCostCommentDTO);
	List<CostAnalysisHistoryDTO> getUserChangeLogDTO(Long displayId, Long displayScenarioId, String tableName);
	void saveILSPriceException(Long kitComponentId, Date peReCalcDate);
	List<BuCostCenterSplitDTO> loadBUCostCenterSplitGroup(Double mdfPctVal,Double discountPctVal, Boolean bDefaultProgramDiscount, Long customerId,Long foreCastQtyVal, Double totalDisplaysOrdered, Double totalActualInvoiceTotCostVal, Double totalDisplayCostsVal, Double customerProgramPercentageVal, List<CostAnalysisKitComponentDTO> costAnalysisKitComponentStore);
	List<DisplayCostImageDTO> reloadDisplayCostImages(Long displayId, Long displayCostId);
	List<DisplayCostImageDTO> saveCostAnalysisImage( DisplayCostImageDTO displayCostImageDTO);
	List<DisplayCostImageDTO> deleteCostAnalysisImage(DisplayCostImageDTO displayCostImageDTO);
	List<CostAnalysisKitComponentDTO> reCalculateILSPriceException(DisplayDTO displayDTO, Date reCalcDate,  Double customerPct, DisplayScenarioDTO displayScenarioDTO);
	DisplayScenarioDTO saveDisplayScenarioDTO(DisplayScenarioDTO displayScenarioModel);
	List<DisplayScenarioDTO> getDisplayScenarioDTO(Long displayId);
	List<DisplayCostDTO> getDisplayCostDTOs(DisplayDTO displayModel);
	DisplayScenarioDTO copyDisplayScenarioDTO(DisplayScenarioDTO displayScenarioModel, String userName, boolean copyComments);
	/**
     * Utility/Convenience class.
     * Use AppService.App.getInstance () to access static instance of AppServiceAsync
     */
    public static class App {
        private static AppServiceAsync app = null;

        public static synchronized AppServiceAsync getInstance() {
            if (app == null) {
                app = (AppServiceAsync) GWT.create(AppService.class);
            }
            return app;
        }
        public static synchronized DmmConstants getDmmConstants() {
           DmmConstants constants = (DmmConstants)GWT.create(DmmConstants.class); 
           return constants;

        }
    }
    List<CountryDTO> getCountryDTOs();
    List<CountryDTO> getCountryFlagDTOs();
    List<CorrugateComponentsDetailDTO> getCorrugateComponentInfo(DisplayDTO displayDTO);
    List<CorrugateComponentsDetailDTO> saveCorrugateComponentInfo(List<CorrugateComponentsDetailDTO> corrCompDTOList, DisplayDTO displayDTO);
    Boolean callCorrugateComponentDelete(CorrugateComponentsDetailDTO corrComponentDetailDTO);
    CorrugateDTO getCorrugateDTO(DisplayDTO displayDTO)  ;
    CorrugateDTO saveCorrugateDTO(CorrugateDTO corrugateDTO)  ;
    CalculationSummaryDTO saveCalculationSummaryDTO(CalculationSummaryDTO calculationSummaryDTO)  ;
    Map<Boolean, InPogDTO> getInPogMapDTOs();
    List<InPogDTO> getInPogDTOs();
    Map<Long, CountryDTO> getCountryMapDTOs();
    Map<Long, CountryDTO> getActiveCountryFlagMapDTOs();

	ManufacturerCountryDTO saveManufacturerCountryDTO(ManufacturerCountryDTO manufacturerCountryDTO) throws DuplicateException;
	void deleteManufacturerCountryDTO(ManufacturerCountryDTO manufacturerCountryDTO);

	List<CustomerDTO> getAllCustomerDTOs();
	List<CustomerTypeDTO> getCustomerTypeDTOs();

	CustomerDTO saveCustomerDTO(CustomerDTO customerDTO) throws DuplicateException;
	void deleteCustomerDTO(CustomerDTO customerDTO);

	List<LocationCountryDTO> getAllLocationCountryDTOs();
	LocationCountryDTO saveLocationCountryDTO(LocationCountryDTO locationCountryDTO);
	void deleteLocationCountryDTO(LocationCountryDTO model);

	List<StructureDTO> getAllStructureDTOs();
	StructureDTO saveStructureDTO(StructureDTO dto);
	void deleteStructureDTO(StructureDTO model);

	List<DisplaySalesRepDTO> getAllDisplaySalesRepDTOs();
	DisplaySalesRepDTO saveDisplaySalesRepDTO(DisplaySalesRepDTO dto);
	void deleteDisplaySalesRepDTO(DisplaySalesRepDTO model);

	List<UserTypeDTO> getUserTypeDTOs();
	List<DisplaySalesRepDTO> getApproverDTOs();

	List<ProgramBuDTO> loadProgramBus(Long displayId);
	List<ProgramBuDTO> saveProgramBus(List<ProgramBuDTO> programList);
	List<ProgramBuDTO> saveProgramUploadedBus(List<ProgramBuDTO> programList, DisplayDTO displayDTO);
	List<CstBuPrgmPctDTO> loadCstBuPrgmPcts(Long customerId);
	void saveProgramUploadBusForDisplays(List<DisplayDTO> displayDTOs);

}
