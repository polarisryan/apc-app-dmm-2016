package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.google.gwt.user.client.Element;

/**
 * User: Mari
 * Date: Feb 3, 2010
 * Time: 5:25:01 PM
 */
public class MassUpdateSelection extends LayoutContainer {
	 	
		private MassUpdate massUpdate;
		List<Long> displayIdList = new ArrayList<Long>();   

	    public MassUpdateSelection(EditorGrid<DisplayDTO> displayGrid,SelectionGrid selectionGrid) {	        
	        // get display id list
			List<DisplayDTO> displayDTOList= displayGrid.getSelectionModel().getSelectedItems();
	        if(displayDTOList!=null && displayDTOList.size()>0){
	        	for(DisplayDTO displayDTO:displayDTOList){
	        		displayIdList.add(displayDTO.getDisplayId());
	        	}
	        }        
	    }

	    @Override
	    protected void onRender(Element parent, int index) {
	        super.onRender(parent, index);
	        buildHeader();
	        buildBody();
	    }

	    private void buildHeader() {
	    	add(new HeaderPanel());
	    }

	    private void buildBody() {
	    	massUpdate = new MassUpdate(displayIdList);
	    	add(massUpdate);
	    }
}
