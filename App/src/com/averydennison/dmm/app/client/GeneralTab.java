package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.google.gwt.user.client.Window;

/**
 * User: Spart Arguello
 * Date: Oct 16, 2009
 * Time: 11:16:36 AM
 */
public class GeneralTab extends LayoutContainer {

    public GeneralTab() {
        VerticalPanel vp = new VerticalPanel();
        vp.setSpacing(10);

        String txt = "Avery Dennison";

        TabPanel panel = new TabPanel();
        panel.setPlain(true);
        panel.setSize(900, 250);

//        TabItem ajax1 = new TabItem("Ajax Tab");
//        ajax1.setScrollMode(Scroll.AUTO);
//        //ajax1.addStyleName("pad-text");
//        ajax1.setAutoLoad(new RequestBuilder(RequestBuilder.GET, "ajax1.html"));
//        panel.add(ajax1);

        TabItem dgit = new TabItem("Display General Info");
        dgit.addListener(Events.Select, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent be) {
//                TabItem ti = (TabItem)be.getComponent();
//                //Info.display("Tab Event", "The '{0}' tab was selected.", ti.getText());
                Window.alert("Event Tab Was Selected");
            }
        });
        //dgit.addStyleName("pad-text");
        dgit.add(new DisplayGeneralInfo());
        panel.add(dgit);

        TabItem eventTab = new TabItem("Event Tab");
        eventTab.addListener(Events.Select, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent be) {
                Window.alert("Event Tab Was Selected");
            }
        });
        //eventTab.addStyleName("pad-text");
        eventTab.addText("I am tab 4's content. I also have an event listener attached.");
        panel.add(eventTab);

        TabItem kcit = new TabItem("Kit Component Info");
        kcit.addListener(Events.Select, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent be) {
                Window.alert("Event Tab Was Selected");
            }
        });
        //kcit.addStyleName("pad-text");
        kcit.addText("I am tab 4's content. I also have an event listener attached.");
        panel.add(kcit);

        TabItem cmit = new TabItem("Customer Marketing Info");
        cmit.addListener(Events.Select, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent be) {
                Window.alert("Event Tab Was Selected");
            }
        });
        //cmit.addStyleName("pad-text");
        cmit.addText("I am tab 4's content. I also have an event listener attached.");
        panel.add(cmit);

        TabItem dlit = new TabItem("Display Logistics Info");
        dlit.addListener(Events.Select, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent be) {
                Window.alert("Event Tab Was Selected");
            }
        });
        //dlit.addStyleName("pad-text");
        dlit.addText("I am tab 4's content. I also have an event listener attached.");
        panel.add(dlit);

        TabItem ccit = new TabItem("Corrugate Component Info");
        ccit.addListener(Events.Select, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent be) {
                Window.alert("Event Tab Was Selected");
            }
        });
        //ccit.addStyleName("pad-text");
        ccit.addText("I am tab 4's content. I also have an event listener attached.");
        panel.add(ccit);

        TabItem cait = new TabItem("Cost Analysis Info");
        ccit.addListener(Events.Select, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent be) {
                Window.alert("Event Tab Was Selected");
            }
        });
        //ccit.addStyleName("pad-text");
        ccit.addText("I am tab 4's content. I also have an event listener attached.");
        panel.add(cait);

        vp.add(panel);
        add(vp);
    }

}
