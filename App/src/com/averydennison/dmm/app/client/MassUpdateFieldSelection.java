package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.models.DisplayForMassUpdateDTO;
import com.averydennison.dmm.app.client.models.SelectFieldToMassUpdateDTO;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.user.client.Element;

public class MassUpdateFieldSelection extends Window {
	public static final String OK = "OK";

	private MassUpdateFieldSelection parent;
	private SelectFieldToMassUpdateDTO selectFieldToMassUpdate ;
	private EditorGrid<DisplayForMassUpdateDTO> dataGrid;
	private Grid<DisplayForMassUpdateDTO> displayGrid;
	
	private CheckBox priceAvailCheckBox = new CheckBox();
	private CheckBox structureCheckBox = new CheckBox();
	private CheckBox sampleToCustomersCheckBox = new CheckBox();
	private CheckBox statusCheckBox = new CheckBox();
	private CheckBox projectManagerCheckBox = new CheckBox();
	private CheckBox finalArtworkCheckBox = new CheckBox();
	private CheckBox pimCompletedOnCheckBox = new CheckBox();
	private CheckBox corrugateMfgtCheckBox = new CheckBox();
	private CheckBox pricingAdminValidationCheckBox = new CheckBox();
	private CheckBox promoPeriodMfgtCheckBox = new CheckBox();
	private CheckBox projectConfidenceLevelCheckBox = new CheckBox();
	private CheckBox initRenderingEstimateCheckBox = new CheckBox();
	private CheckBox skuMixFinalCheckBox = new CheckBox();
	private CheckBox structuredApprovedCheckBox = new CheckBox();
	private CheckBox orderReceivedCheckBox = new CheckBox();
	private CheckBox commentsCheckBox = new CheckBox();

	
	public MassUpdateFieldSelection(EditorGrid<DisplayForMassUpdateDTO> dataGrid, Grid<DisplayForMassUpdateDTO> displayGrid, SelectFieldToMassUpdateDTO selectFieldToMassUpdate) {
		this.parent = this;
		this.setSize(550, 475);
		this.setPlain(true);
		this.setResizable(false);
		this.setHeading("Select Fields to Update");
		this.setLayout(new FitLayout());
		this.selectFieldToMassUpdate = selectFieldToMassUpdate;
		this.displayGrid = displayGrid;
		this.dataGrid = dataGrid;		
	}

	@Override
	protected void onRender(Element parent, int index) {
		super.onRender(parent, index);
		createForm();
	}
	
	// button listener for page
    private SelectionListener<ButtonEvent> listn = new SelectionListener<ButtonEvent>() {
		public void componentSelected(ButtonEvent ce) {
            Button button = (Button) ce.getComponent();
            if (button.getText().equals(OK)) {
                selectFieldToMassUpdate.setbStructure(structureCheckBox.getValue() == null ? false : structureCheckBox.getValue());
                selectFieldToMassUpdate.setbProjectManager(projectManagerCheckBox.getValue() == null ? false : projectManagerCheckBox.getValue());
                selectFieldToMassUpdate.setbStatus(statusCheckBox.getValue() == null ? false : statusCheckBox.getValue());
                selectFieldToMassUpdate.setbCorrugateMfgt(corrugateMfgtCheckBox.getValue() == null ? false : corrugateMfgtCheckBox.getValue());
                selectFieldToMassUpdate.setbPromoPeriod(promoPeriodMfgtCheckBox.getValue() == null ? false : promoPeriodMfgtCheckBox.getValue()) ;
                selectFieldToMassUpdate.setbSkuMixFinal(skuMixFinalCheckBox.getValue() == null ? false : skuMixFinalCheckBox.getValue()) ;
                selectFieldToMassUpdate.setbPriceAvailCompleteOnDt(priceAvailCheckBox.getValue() == null ? false : priceAvailCheckBox.getValue());
                selectFieldToMassUpdate.setbSampleToCustomerBy(sampleToCustomersCheckBox.getValue() == null ? false : sampleToCustomersCheckBox.getValue());
                selectFieldToMassUpdate.setbFinalArtworkPODueDt(finalArtworkCheckBox.getValue() == null ? false : finalArtworkCheckBox.getValue()) ;
                selectFieldToMassUpdate.setbPIMCompletedonDt(pimCompletedOnCheckBox.getValue() == null ? false : pimCompletedOnCheckBox.getValue()) ;
                selectFieldToMassUpdate.setbPricingAdminValidationDt(pricingAdminValidationCheckBox.getValue() == null ? false : pricingAdminValidationCheckBox.getValue()) ;
                selectFieldToMassUpdate.setbProjectConfidenceLevel(projectConfidenceLevelCheckBox.getValue() == null ? false : projectConfidenceLevelCheckBox.getValue()) ;
                selectFieldToMassUpdate.setbInitRenderingEstQuote(initRenderingEstimateCheckBox.getValue() == null ? false : initRenderingEstimateCheckBox.getValue()) ;
                selectFieldToMassUpdate.setbStructApproved(structuredApprovedCheckBox.getValue() == null ? false : structuredApprovedCheckBox.getValue());
                selectFieldToMassUpdate.setbOrdersReceived(orderReceivedCheckBox.getValue() == null ? false : orderReceivedCheckBox.getValue()) ;
                selectFieldToMassUpdate.setbComments(commentsCheckBox.getValue() == null ? false : commentsCheckBox.getValue()); ;

                
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.STRUCTURE_NAME).setHidden(!selectFieldToMassUpdate.isbStructure());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.FULL_NAME).setHidden(!selectFieldToMassUpdate.isbProjectManager());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.STATUS_NAME).setHidden(!selectFieldToMassUpdate.isbStatus());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.CORRUGATE_NAME).setHidden(!selectFieldToMassUpdate.isbCorrugateMfgt());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.PROMO_PERIOD_NAME).setHidden(!selectFieldToMassUpdate.isbPromoPeriod());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.SKU_MIX_FINAL_TF).setHidden(!selectFieldToMassUpdate.isbSkuMixFinal());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.PRICE_AVAILABILITY_DATE).setHidden(!selectFieldToMassUpdate.isbPriceAvailCompleteOnDt());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.SENT_SAMPLE_DATE).setHidden(!selectFieldToMassUpdate.isbSampleToCustomerBy());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.FINAL_ARTWORK_DATE).setHidden(!selectFieldToMassUpdate.isbFinalArtworkPODueDt());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.PIM_COMPLETED_DATE).setHidden(!selectFieldToMassUpdate.isbPIMCompletedonDt());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.PRICING_ADMIN_DATE).setHidden(!selectFieldToMassUpdate.isbPricingAdminValidationDt());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.PROJECT_CONFIDENCE_LEVEL).setHidden(!selectFieldToMassUpdate.isbProjectConfidenceLevel());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.INITIAL_RENDERING_TF).setHidden(!selectFieldToMassUpdate.isbInitRenderingEstQuote());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.STRUCTURE_APPROVED_TF).setHidden(!selectFieldToMassUpdate.isbStructApproved());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.ORDERS_RECEIVED_TF).setHidden(!selectFieldToMassUpdate.isbOrdersReceived());
            	dataGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.COMMENTS).setHidden(!selectFieldToMassUpdate.isbComments());
            	dataGrid.getView().refresh(true);
            	
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.STRUCTURE_NAME).setHidden(!selectFieldToMassUpdate.isbStructure());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.FULL_NAME).setHidden(!selectFieldToMassUpdate.isbProjectManager());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.STATUS_NAME).setHidden(!selectFieldToMassUpdate.isbStatus());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.CORRUGATE_NAME).setHidden(!selectFieldToMassUpdate.isbCorrugateMfgt());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.PROMO_PERIOD_NAME).setHidden(!selectFieldToMassUpdate.isbPromoPeriod());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.SKU_MIX_FINAL_FLG).setHidden(!selectFieldToMassUpdate.isbSkuMixFinal());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.PRICE_AVAILABILITY_DATE).setHidden(!selectFieldToMassUpdate.isbPriceAvailCompleteOnDt());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.SENT_SAMPLE_DATE).setHidden(!selectFieldToMassUpdate.isbSampleToCustomerBy());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.FINAL_ARTWORK_DATE).setHidden(!selectFieldToMassUpdate.isbFinalArtworkPODueDt());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.PIM_COMPLETED_DATE).setHidden(!selectFieldToMassUpdate.isbPIMCompletedonDt());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.PRICING_ADMIN_DATE).setHidden(!selectFieldToMassUpdate.isbPricingAdminValidationDt());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.PROJECT_CONFIDENCE_LEVEL).setHidden(!selectFieldToMassUpdate.isbProjectConfidenceLevel());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.INITIAL_RENDERING).setHidden(!selectFieldToMassUpdate.isbInitRenderingEstQuote());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.STRUCTURE_APPROVED).setHidden(!selectFieldToMassUpdate.isbStructApproved());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.ORDER_IN).setHidden(!selectFieldToMassUpdate.isbOrdersReceived());
            	displayGrid.getColumnModel().getColumnById(DisplayForMassUpdateDTO.COMMENTS).setHidden(!selectFieldToMassUpdate.isbComments());
            	displayGrid.getView().refresh(true);
            	
            	clearModel();
            	parent.hide();
            }
        }
    };

	private final Button btnOK = new Button(OK, listn);

	private void clearModel() {
		this.removeAllListeners();
	}

	public void createForm() {
        VerticalPanel vp1 = new VerticalPanel();
        VerticalPanel vp2 = new VerticalPanel();
        vp1.setSpacing(5);
        vp2.setSpacing(5);
    
        ContentPanel panel = new ContentPanel();
        panel.setHeaderVisible(false);
        panel.setSize(540, -1);
        panel.setFrame(true);
        panel.setButtonAlign(HorizontalAlignment.CENTER);

        // setup Display General Info column
        LabelField genralInfoTabLabel = new LabelField("Display General Info");
        genralInfoTabLabel.setFieldLabel("genralInfoTabLabel");
        genralInfoTabLabel.setWidth(250);
        vp1.add(genralInfoTabLabel);

        // Display General Info : Structure
        structureCheckBox.setBoxLabel("Structure");
        structureCheckBox.setValue(selectFieldToMassUpdate.isbStructure());
        structureCheckBox.setWidth(250);
        structureCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbStructure(structureCheckBox.getValue() == null ? false : structureCheckBox.getValue());
            }
        });
        vp1.add(structureCheckBox);
        
        // Display General Info : Project Manager
        projectManagerCheckBox.setBoxLabel("Project Manager");
        projectManagerCheckBox.setValue(selectFieldToMassUpdate.isbProjectManager());
        projectManagerCheckBox.setWidth(250);
        projectManagerCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbProjectManager(projectManagerCheckBox.getValue() == null ? false : projectManagerCheckBox.getValue());
            }
        });
        vp1.add(projectManagerCheckBox);
        
        // Display General Info : Status
        statusCheckBox.setBoxLabel("Status");
        statusCheckBox.setValue(selectFieldToMassUpdate.isbStatus());
        statusCheckBox.setWidth(250);
        statusCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbStatus(statusCheckBox.getValue() == null ? false : statusCheckBox.getValue());
            }
        });
        vp1.add(statusCheckBox);
        
        // Display General Info : Corrugate Mfg
        corrugateMfgtCheckBox.setBoxLabel("Corrugate Mfg");
        corrugateMfgtCheckBox.setValue(selectFieldToMassUpdate.isbCorrugateMfgt());
        corrugateMfgtCheckBox.setWidth(250);
        corrugateMfgtCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbCorrugateMfgt(corrugateMfgtCheckBox.getValue() == null ? false : corrugateMfgtCheckBox.getValue());
            }
        });
        vp1.add(corrugateMfgtCheckBox);
        
        // Display General Info : Promo Period
        promoPeriodMfgtCheckBox.setBoxLabel("Promo Period");
        promoPeriodMfgtCheckBox.setValue(selectFieldToMassUpdate.isbPromoPeriod());
        promoPeriodMfgtCheckBox.setWidth(250);
        promoPeriodMfgtCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbPromoPeriod(promoPeriodMfgtCheckBox.getValue() == null ? false : promoPeriodMfgtCheckBox.getValue()) ;
            }
        });
        vp1.add(promoPeriodMfgtCheckBox);
        
        // Display General Info : SKU Mix Final
        skuMixFinalCheckBox.setBoxLabel("SKU Mix Final");
        skuMixFinalCheckBox.setValue(selectFieldToMassUpdate.isbSkuMixFinal());
        skuMixFinalCheckBox.setWidth(250);
        skuMixFinalCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbSkuMixFinal(skuMixFinalCheckBox.getValue() == null ? false : skuMixFinalCheckBox.getValue()) ;
            }
        });
        vp1.add(skuMixFinalCheckBox);
        
        
        // setup Customer Marketing Info column
        LabelField customerMarketingLabel = new LabelField("Customer Marketing Info");
        customerMarketingLabel.setFieldLabel("customerMarketingLabel");
        customerMarketingLabel.setWidth(250);
        vp2.add(customerMarketingLabel);
    	        
        // Customer Marketing Info : Price Avail Complete On
        priceAvailCheckBox.setBoxLabel("Price Avail Complete On Dt.");
        priceAvailCheckBox.setValue(selectFieldToMassUpdate.isbPriceAvailCompleteOnDt());
        priceAvailCheckBox.setWidth(250);
        priceAvailCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbPriceAvailCompleteOnDt(priceAvailCheckBox.getValue() == null ? false : priceAvailCheckBox.getValue());
            }
        });
        vp2.add(priceAvailCheckBox);
        
        // Customer Marketing Info : Sample To Customer By
        sampleToCustomersCheckBox.setBoxLabel("Sample To Customer By");
        sampleToCustomersCheckBox.setValue(selectFieldToMassUpdate.isbSampleToCustomerBy());
        sampleToCustomersCheckBox.setWidth(250);
        sampleToCustomersCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbSampleToCustomerBy(sampleToCustomersCheckBox.getValue() == null ? false : sampleToCustomersCheckBox.getValue());
            }
        });
        vp2.add(sampleToCustomersCheckBox);
        
        // Customer Marketing Info : Final Artwork & PO Due Dt        
        finalArtworkCheckBox.setBoxLabel("Final Artwork & PO Due Dt.");
        finalArtworkCheckBox.setValue(selectFieldToMassUpdate.isbFinalArtworkPODueDt());
        finalArtworkCheckBox.setWidth(250);
        finalArtworkCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbFinalArtworkPODueDt(finalArtworkCheckBox.getValue() == null ? false : finalArtworkCheckBox.getValue()) ;
            }
        });
        vp2.add(finalArtworkCheckBox);
        
        // Customer Marketing Info : PIM Completed on Dt
        pimCompletedOnCheckBox.setBoxLabel("PIM Completed on Dt.");
        pimCompletedOnCheckBox.setValue(selectFieldToMassUpdate.isbPIMCompletedonDt());
        pimCompletedOnCheckBox.setWidth(250);
        pimCompletedOnCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbPIMCompletedonDt(pimCompletedOnCheckBox.getValue() == null ? false : pimCompletedOnCheckBox.getValue()) ;
            }
        });
        vp2.add(pimCompletedOnCheckBox);
        
        // Customer Marketing Info : Pricing Admin Validation Dt	        
        pricingAdminValidationCheckBox.setBoxLabel("Pricing Admin Validation Dt.");
        pricingAdminValidationCheckBox.setValue(selectFieldToMassUpdate.isbPricingAdminValidationDt());
        pricingAdminValidationCheckBox.setWidth(250);
        pricingAdminValidationCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbPricingAdminValidationDt(pricingAdminValidationCheckBox.getValue() == null ? false : pricingAdminValidationCheckBox.getValue()) ;
            }
        });
        vp2.add(pricingAdminValidationCheckBox);

        // Customer Marketing Info : Project Confidence Level	        
        projectConfidenceLevelCheckBox.setBoxLabel("Project Confidence Level");
        projectConfidenceLevelCheckBox.setValue(selectFieldToMassUpdate.isbProjectConfidenceLevel());
        projectConfidenceLevelCheckBox.setWidth(250);
        projectConfidenceLevelCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbProjectConfidenceLevel(projectConfidenceLevelCheckBox.getValue() == null ? false : projectConfidenceLevelCheckBox.getValue()) ;
            }
        });
        vp2.add(projectConfidenceLevelCheckBox);

        // Customer Marketing Info : Init Rendering & Est Quote
        initRenderingEstimateCheckBox.setBoxLabel("Init Rendering & Est Quote (Y/N)");
        initRenderingEstimateCheckBox.setValue(selectFieldToMassUpdate.isbInitRenderingEstQuote());
        initRenderingEstimateCheckBox.setWidth(250);
        initRenderingEstimateCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbInitRenderingEstQuote(initRenderingEstimateCheckBox.getValue() == null ? false : initRenderingEstimateCheckBox.getValue()) ;
            }
        });
        vp2.add(initRenderingEstimateCheckBox);

        // Customer Marketing Info : Struct Approved
        structuredApprovedCheckBox.setBoxLabel("Struct Approved (Y/N)");
        structuredApprovedCheckBox.setValue(selectFieldToMassUpdate.isbStructApproved());
        structuredApprovedCheckBox.setWidth(250);
        structuredApprovedCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbStructApproved(structuredApprovedCheckBox.getValue() == null ? false : structuredApprovedCheckBox.getValue());
            }
        });
        vp2.add(structuredApprovedCheckBox);

        // Customer Marketing Info : Orders Received
        orderReceivedCheckBox.setBoxLabel("Orders Received (Y/N)");
        orderReceivedCheckBox.setValue(selectFieldToMassUpdate.isbOrdersReceived());
        orderReceivedCheckBox.setWidth(250);
        orderReceivedCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbOrdersReceived(orderReceivedCheckBox.getValue() == null ? false : orderReceivedCheckBox.getValue()) ;
            }
        });
        vp2.add(orderReceivedCheckBox);

        // Customer Marketing Info : Comments
        commentsCheckBox.setBoxLabel("Comments");
        commentsCheckBox.setValue(selectFieldToMassUpdate.isbComments());
        commentsCheckBox.setWidth(250);
        commentsCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                selectFieldToMassUpdate.setbComments(commentsCheckBox.getValue() == null ? false : commentsCheckBox.getValue()); ;
            }
        });
        vp2.add(commentsCheckBox);

        HorizontalPanel hp = new HorizontalPanel();
        hp.setSpacing(10);
        hp.add(vp1);
        hp.add(vp2);
        panel.add(hp);

        btnOK.setWidth(80);
        panel.addButton(btnOK);

        add(panel);		
	}

	public SelectFieldToMassUpdateDTO getSelectFieldToMassUpdateDTO() {
		return selectFieldToMassUpdate;
	}
	
	public int selectedFieldCount() {
		int count = 0;
		
        if (selectFieldToMassUpdate.isbStructure()) count++;
        if (selectFieldToMassUpdate.isbProjectManager()) count++;
        if (selectFieldToMassUpdate.isbStatus()) count++;
        if (selectFieldToMassUpdate.isbCorrugateMfgt()) count++;
        if (selectFieldToMassUpdate.isbPromoPeriod()) count++;
        if (selectFieldToMassUpdate.isbSkuMixFinal()) count++;
        if (selectFieldToMassUpdate.isbPriceAvailCompleteOnDt()) count++;
        if (selectFieldToMassUpdate.isbSampleToCustomerBy()) count++;
        if (selectFieldToMassUpdate.isbFinalArtworkPODueDt()) count++;
        if (selectFieldToMassUpdate.isbPIMCompletedonDt()) count++;
        if (selectFieldToMassUpdate.isbPricingAdminValidationDt()) count++;
        if (selectFieldToMassUpdate.isbProjectConfidenceLevel()) count++;
        if (selectFieldToMassUpdate.isbInitRenderingEstQuote()) count++;
        if (selectFieldToMassUpdate.isbStructApproved()) count++;
        if (selectFieldToMassUpdate.isbOrdersReceived()) count++;
        if (selectFieldToMassUpdate.isbComments()) count++;

		return count;
	}

	public int selectedColumnPos(int columnIndex) {
		int count = 4;
		
        if (selectFieldToMassUpdate.isbStructure() && columnIndex >= 4) count++;
        if (selectFieldToMassUpdate.isbProjectManager() && columnIndex >= 5) count++;
        if (selectFieldToMassUpdate.isbStatus() && columnIndex >= 6) count++;
        if (selectFieldToMassUpdate.isbCorrugateMfgt() && columnIndex >= 7) count++;
        if (selectFieldToMassUpdate.isbPromoPeriod() && columnIndex >= 8) count++;
        if (selectFieldToMassUpdate.isbSkuMixFinal() && columnIndex >= 9) count++;
        if (selectFieldToMassUpdate.isbPriceAvailCompleteOnDt() && columnIndex >= 10) count++;
        if (selectFieldToMassUpdate.isbSampleToCustomerBy() && columnIndex >= 11) count++;
        if (selectFieldToMassUpdate.isbFinalArtworkPODueDt() && columnIndex >= 12) count++;
        if (selectFieldToMassUpdate.isbPIMCompletedonDt() && columnIndex >= 13) count++;
        if (selectFieldToMassUpdate.isbPricingAdminValidationDt() && columnIndex >= 14) count++;
        if (selectFieldToMassUpdate.isbProjectConfidenceLevel() && columnIndex >= 15) count++;
        if (selectFieldToMassUpdate.isbInitRenderingEstQuote() && columnIndex >= 16) count++;
        if (selectFieldToMassUpdate.isbStructApproved() && columnIndex >= 17) count++;
        if (selectFieldToMassUpdate.isbOrdersReceived() && columnIndex >= 18) count++;
        if (selectFieldToMassUpdate.isbComments() && columnIndex >= 19) count++;

		return count;
	}

}
