package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayPlannerDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.averydennison.dmm.app.client.models.ValidPlannerDTO;
import com.averydennison.dmm.app.client.models.ValidPlantDTO;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.user.client.Timer;

/**
 * User: Alvin Wong
 * Date: Dec 14, 2009
 * Time: 17:20:00 PM
 */
public class KitComponentInfoPlannerGrid extends LayoutContainer {
    
    private KitComponentDTO kitComponentDTO = new KitComponentDTO();
    private DisplayDTO displayDTO;
    private ListStore<DisplayPlannerDTO> store = new ListStore<DisplayPlannerDTO>();
    private boolean kitComponentInfoPlannerGridModified=false;

    public ListStore<DisplayPlannerDTO> getStore() {
    	return store;
    }
    
	public DisplayDTO getDisplayDTO() {
		return displayDTO;
	}

	public void setDisplayDTO(DisplayDTO displayDTO) {
		this.displayDTO = displayDTO;
	}

	public boolean isKitComponentInfoPlannerGridModified() {
		return kitComponentInfoPlannerGridModified;
	}

	public void setKitComponentInfoPlannerGridModified(
			boolean kitComponentInfoPlannerGridModified) {
		this.kitComponentInfoPlannerGridModified = kitComponentInfoPlannerGridModified;
	}

	public KitComponentInfoPlannerGrid(KitComponentDTO kitComponent, DisplayDTO display, Boolean readOnly) {
		this.kitComponentDTO = kitComponent;
		this.displayDTO = display;

		System.out.println("KitComponentInfoPlannerGrid(): display id=" + display.getDisplayId());

        ListStore<ValidPlantDTO> plants = new ListStore<ValidPlantDTO>();
        AppService.App.getInstance().getValidPlantDTOs(new Service.SetValidPlantStore(plants));

        ListStore<ValidPlannerDTO> planners = new ListStore<ValidPlannerDTO>();
        AppService.App.getInstance().getValidPlannerDTOs(new Service.SetValidPlannerStore(planners));
		
        setLayout(new FlowLayout(0));

        int x = 100;


        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        ColumnConfig column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.DC_NAME);
        column.setHeader("DC");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x + 43);
        configs.add(column);

        final ComboBox<ValidPlantDTO> plantCombo = new ComboBox<ValidPlantDTO>();
        plantCombo.setFieldLabel("Plant");
        plantCombo.setDisplayField(ValidPlantDTO.PLANT_CODE);
        plantCombo.setValueField(ValidPlantDTO.PLANT_CODE);
        plantCombo.setWidth(100);
        plantCombo.setStore(plants);
        plantCombo.setAllowBlank(false);
        plantCombo.setTypeAhead(true);
        plantCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
        
        
        CellEditor plantComboEditor = new CellEditor(plantCombo) {
            @Override
            public Object preProcessValue(Object name) {
              if (name == null) {
                return null;
              }
              String valueAttr = ((ComboBox)plantCombo).getValueField();
              if (valueAttr == null)
                valueAttr = "value";
              return ((ComboBox)plantCombo).getStore().findModel(valueAttr,
                                                            name);
            }

            @Override
            public Object postProcessValue(Object value) {
              if (value == null) {
                return null;
              }
              String valueAttr = ((ComboBox)plantCombo).getValueField();
              if (valueAttr == null)
                valueAttr = "value";
              return ((ValidPlantDTO)value).get(valueAttr);
            }
          }; 

             
        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.PLANT_CODE);
        column.setHeader("Plant");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x - 5);
        if (!readOnly)
        	column.setEditor(plantComboEditor);  
        configs.add(column);

        final ComboBox<ValidPlannerDTO> plannerCombo = new ComboBox<ValidPlannerDTO>();
        plannerCombo.setFieldLabel("Planner");
        plannerCombo.setDisplayField(ValidPlannerDTO.PLANNER_CODE);
        plannerCombo.setValueField(ValidPlannerDTO.PLANNER_CODE);
        plannerCombo.setWidth(100);
        plannerCombo.setStore(planners);
        plannerCombo.setAllowBlank(false);
        plannerCombo.setTypeAhead(true);
        plannerCombo.setTriggerAction(ComboBox.TriggerAction.ALL);

        CellEditor plannerComboEditor = new CellEditor(plannerCombo) {
            @Override
            public Object preProcessValue(Object name) {
              if (name == null) {
                return null;
              }
              String valueAttr = ((ComboBox)plannerCombo).getValueField();
              if (valueAttr == null)
                valueAttr = "value";
              return ((ComboBox)plannerCombo).getStore().findModel(valueAttr,
                                                            name);
            }

            @Override
            public Object postProcessValue(Object value) {
              if (value == null) {
                return null;
              }
              String valueAttr = ((ComboBox)plannerCombo).getValueField();
              if (valueAttr == null)
                valueAttr = "value";
              return ((ValidPlannerDTO)value).get(valueAttr);
            }
          };         	

        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.PLANNER_CODE);
        column.setHeader("Planner");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x + 40);
        if (!readOnly)
        	column.setEditor(plannerComboEditor);  
        configs.add(column);

        TextField<String> leadTime = new TextField<String>();   
        leadTime.setAllowBlank(false);
        leadTime.setMaxLength(3);
        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.LEAD_TIME);
        column.setHeader("Lead Time");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x - 30);
        if (!readOnly)
        	column.setEditor(new CellEditor(leadTime));  
        configs.add(column);

        CheckColumnConfig poCol = new CheckColumnConfig(DisplayPlannerDTO.PLANT_OVERRIDE_FLG, "Plant/Planner Override", 55);
        CellEditor checkBoxEditor2 = new CellEditor(new CheckBox());
        if (!readOnly)
        	poCol.setEditor(checkBoxEditor2);
        poCol.setWidth(x + 30);
        configs.add(poCol);

        // hidden columns
        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.DC_CODE);
        column.setHidden(true);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.DISPLAY_PLANNER_ID);
        column.setHidden(true);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.KIT_COMPONENT_ID);
        column.setHidden(true);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.DISPLAY_DC_ID);
        column.setHidden(true);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.DT_CREATED);
        column.setHidden(true);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.DT_LAST_CHANGED);
        column.setHidden(true);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.USER_CREATED);
        column.setHidden(true);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayPlannerDTO.USER_LAST_CHANGED);
        column.setHidden(true);
        configs.add(column);

        ColumnModel cm = new ColumnModel(configs);

        // call server
        //if (kitComponentDTO.getKitComponentId() != null) {
	   	    System.out.println("KitComponentInfoPlannerGrid: loading kit component planners for kit_component_id=" + kitComponentDTO.getKitComponentId());
	        AppService.App.getInstance().getDisplayPlanners(kitComponentDTO, displayDTO, new Service.LoadDisplayPlanners(store));	
        //}
        
        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setHeaderVisible(false);
        cp.setLayout(new FitLayout());
        cp.setSize(600, 110);

        final EditorGrid<DisplayPlannerDTO> grid = new EditorGrid<DisplayPlannerDTO>(store, cm);
        grid.setColumnResize(true);
        grid.setStyleAttribute("borderTop", "none");
        grid.setStripeRows(true);
        grid.setBorders(true);
        grid.addListener(Events.BeforeEdit, new Listener<GridEvent<ModelData>>() {
        	 public void handleEvent(GridEvent<ModelData> be) {
        		 DisplayPlannerDTO dpDTO = (DisplayPlannerDTO)be.getRecord().getModel();
        		 Boolean override = dpDTO.isPlantOverrideFlg();
        		 if (!override) {
        			 if (be.getColIndex() == 1 || be.getColIndex() == 2) be.setCancelled(true);
        		 }
        	 }
        });
        grid.addListener(Events.AfterEdit, new Listener<GridEvent<ModelData>>() {
	       	public void handleEvent(GridEvent<ModelData> be) {
	       		DisplayPlannerDTO dpDTO = (DisplayPlannerDTO)be.getRecord().getModel();
	       		Boolean override = dpDTO.isPlantOverrideFlg();
	       		int selectedRow = be.getRowIndex();
	    		if (!override && be.getColIndex() == 4) {
	                final MessageBox progressBar = MessageBox.wait("Progress",
	                         "Refreshing planning info from PIM, please wait...", "Loading...");
	    	 	    AppService.App.getInstance().getDisplayPlanner(kitComponentDTO, dpDTO, new Service.UpdateDisplayPlanner(grid, selectedRow, progressBar));	    			 
	    		}
	    		setKitComponentInfoPlannerGridModified(true);
	       	 }
        });
        cp.add(grid, new FitData(0));

        add(cp);
    }
	public void reload(Long kitComponentId) {
		// need to pass in kit component id, for new kit components
		// otherwise, grid gets cleared on reload
		kitComponentDTO.setKitComponentId(kitComponentId);
        AppService.App.getInstance().getDisplayPlanners(kitComponentDTO, displayDTO, new Service.LoadDisplayPlanners(store));
	}
}