package com.averydennison.dmm.app.client;

import com.google.gwt.i18n.client.Constants; 

public interface DmmConstants extends Constants { 
    String imageUploadPath(); 
    String imageFolder();
    String customerPct();
    String vmPct();
    String vmAmount();
} 
