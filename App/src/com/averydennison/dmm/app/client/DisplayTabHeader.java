package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Oct 21, 2009
 * Time: 3:53:56 PM
 */
public class DisplayTabHeader extends LayoutContainer {

    private VerticalPanel vp;
    private DisplayDTO display;
    private TextField<String> skuTextField = new TextField<String>();
    private TextField<String> desc = new TextField<String>();
    private TextField<String> structureTextField = new TextField<String>();
    private TextField<String> custTextField = new TextField<String>();
    private TextField<String> destTextField = new TextField<String>();
    private TextField<String> statusTextField = new TextField<String>();
    private TextField<String> forecastQtyTextField = new TextField<String>();
    private TextField<String> prdDueAtPackOutTextField = new TextField<String>();
    private TextField<String> shipFromLocDateTextField = new TextField<String>();
    private TextField<String> packoutTextField = new TextField<String>();
    private CheckBox skuMixFinalCheckBox = new CheckBox();

    public DisplayTabHeader() {
        vp = new VerticalPanel();
        vp.setSpacing(10);
    }

    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        createHeader();
        add(vp);
    }

    private void createHeader() {

        HorizontalPanel hp1 = new HorizontalPanel();
        HorizontalPanel hp2 = new HorizontalPanel();
        HorizontalPanel hp3 = new HorizontalPanel();
        HorizontalPanel hp4 = new HorizontalPanel();
        HorizontalPanel hp5 = new HorizontalPanel();
        HorizontalPanel hp6 = new HorizontalPanel();
        HorizontalPanel hp7 = new HorizontalPanel();
        HorizontalPanel hp8 = new HorizontalPanel();
        HorizontalPanel hp9 = new HorizontalPanel();
        HorizontalPanel hp10 = new HorizontalPanel();
        HorizontalPanel hp11 = new HorizontalPanel();
        HorizontalPanel hp12 = new HorizontalPanel();

        VerticalPanel vp1 = new VerticalPanel();
        VerticalPanel vp2 = new VerticalPanel();
        VerticalPanel vp3 = new VerticalPanel();
        VerticalPanel vp4 = new VerticalPanel();
        VerticalPanel vp5 = new VerticalPanel();
        VerticalPanel vp6 = new VerticalPanel();

        hp1.setSpacing(2);
        hp2.setSpacing(2);
        hp3.setSpacing(2);
        hp4.setSpacing(2);
        hp5.setSpacing(2);
        hp6.setSpacing(2);
        hp7.setSpacing(2);
        hp8.setSpacing(2);
        hp9.setSpacing(2);
        hp10.setSpacing(2);
        hp11.setSpacing(2);
        hp12.setSpacing(2);

        vp1.setSpacing(2);
        vp2.setSpacing(2);
        vp3.setSpacing(2);
        vp4.setSpacing(2);
        vp5.setSpacing(2);
        vp6.setSpacing(2);

        ContentPanel panel = new ContentPanel();
        panel.setHeaderVisible(false);
        panel.setLayout(new ColumnLayout());
        panel.setSize(935, -1);
        panel.setFrame(true);
        panel.setCollapsible(true);


        LabelField skuLabel = new LabelField("SKU:");
        skuLabel.setReadOnly(true);
        skuLabel.setWidth(30);
        hp1.add(skuLabel);

        skuTextField.setWidth(120);
        skuTextField.setFieldLabel("skuTextField");
        skuTextField.setAllowBlank(true);
        skuTextField.setReadOnly(true);
        skuTextField.setMinLength(15);
        skuTextField.setMaxLength(15);
        hp1.add(skuTextField);

        vp1.add(hp1);

        LabelField custLabel = new LabelField("CST:");
        custLabel.setFieldLabel("customerLabel");
        custLabel.setWidth(30);
        custLabel.setReadOnly(true);
        hp2.add(custLabel);

        custTextField.setWidth(120);
        custTextField.setFieldLabel("custTextField");
        custTextField.setAllowBlank(true);
        custTextField.setReadOnly(true);
        custTextField.setMinLength(15);
        custTextField.setMaxLength(15);
        hp2.add(custTextField);

        vp1.add(hp2);


        desc.setWidth(175);
        desc.setFieldLabel("Description");
        desc.setAllowBlank(true);
        desc.setReadOnly(true);
        hp3.add(desc);

        vp2.add(hp3);

        LabelField structureLabel = new LabelField("Structure:");
        structureLabel.setFieldLabel("structureLabel");
        structureLabel.setWidth(50);
        structureLabel.setReadOnly(true);
        hp4.add(structureLabel);

        structureTextField.setWidth(75);
        structureTextField.setFieldLabel("structureTextField");
        structureTextField.setAllowBlank(true);
        structureTextField.setReadOnly(true);
        structureTextField.setMinLength(15);
        structureTextField.setMaxLength(15);
        hp4.add(structureTextField);

        vp2.add(hp4);

        LabelField destLabel = new LabelField("Dest:");
        destLabel.setFieldLabel("destLabel");
        destLabel.setWidth(40);
        destLabel.setReadOnly(true);
        hp5.add(destLabel);

        destTextField.setWidth(75);
        destTextField.setFieldLabel("destTextField");
        destTextField.setAllowBlank(true);
        destTextField.setReadOnly(true);
        destTextField.setMinLength(15);
        destTextField.setMaxLength(15);
        hp5.add(destTextField);

        vp3.add(hp5);

        LabelField forecastQtyLabel = new LabelField("Qty:");
        forecastQtyLabel.setFieldLabel("forecastQtyLabel");
        forecastQtyLabel.setWidth(40);
        forecastQtyLabel.setReadOnly(true);
        hp6.add(forecastQtyLabel);

        forecastQtyTextField.setWidth(75);
        forecastQtyTextField.setFieldLabel("forecastQtyTextField");
        forecastQtyTextField.setAllowBlank(true);
        forecastQtyTextField.setReadOnly(true);
        forecastQtyTextField.setMinLength(15);
        forecastQtyTextField.setMaxLength(15);
        hp6.add(forecastQtyTextField);

        vp3.add(hp6);

        LabelField statusLabel = new LabelField("Status:");
        statusLabel.setFieldLabel("statusLabel");
        statusLabel.setWidth(50);
        statusLabel.setReadOnly(true);
        hp7.add(statusLabel);

        statusTextField.setWidth(75);
        statusTextField.setFieldLabel("statusTextField");
        statusTextField.setAllowBlank(true);
        statusTextField.setReadOnly(true);
        statusTextField.setMinLength(15);
        statusTextField.setMaxLength(15);
        hp7.add(statusTextField);

        vp4.add(hp7);

        skuMixFinalCheckBox.setBoxLabel("SKU Mix Final");
        //skuMixFinalCheckBox.setValue(false);
        skuMixFinalCheckBox.setWidth(100);
        skuMixFinalCheckBox.setReadOnly(true);
        hp8.add(skuMixFinalCheckBox);

        vp4.add(hp8);

        LabelField packOutLocationLabel = new LabelField("Pack-Out Locs:");
        packOutLocationLabel.setFieldLabel("packOutLocationLabel");
        packOutLocationLabel.setWidth(90);
        packOutLocationLabel.setHeight(20);
        packOutLocationLabel.setReadOnly(true);
        hp9.add(packOutLocationLabel);

        vp5.add(hp9);

        packoutTextField.setWidth(100);
        packoutTextField.setFieldLabel("packoutTextField");
        packoutTextField.setAllowBlank(true);
        packoutTextField.setReadOnly(true);
        packoutTextField.setMinLength(15);
        packoutTextField.setMaxLength(15);
        hp10.add(packoutTextField);

        vp5.add(hp10);

        LabelField prdDueAtPackOutLabel = new LabelField("Prd Due:");
        prdDueAtPackOutLabel.setFieldLabel("prdDueAtPackOutLabel");
        prdDueAtPackOutLabel.setWidth(60);
        prdDueAtPackOutLabel.setHeight(7);
        prdDueAtPackOutLabel.setReadOnly(true);
        hp11.add(prdDueAtPackOutLabel);

        prdDueAtPackOutTextField.setWidth(75);
        prdDueAtPackOutTextField.setFieldLabel("prdDueAtPackOutTextField");
        prdDueAtPackOutTextField.setAllowBlank(true);
        prdDueAtPackOutTextField.setReadOnly(true);
        prdDueAtPackOutTextField.setMinLength(15);
        prdDueAtPackOutTextField.setMaxLength(15);
        hp11.add(prdDueAtPackOutTextField);

        LabelField shipFromLocDateLabel = new LabelField("Ship Dt:");
        shipFromLocDateLabel.setFieldLabel("shipFromLocDateLabel");
        shipFromLocDateLabel.setWidth(60);
        shipFromLocDateLabel.setHeight(7);
        shipFromLocDateLabel.setReadOnly(true);
        hp12.add(shipFromLocDateLabel);

        shipFromLocDateTextField.setWidth(75);
        shipFromLocDateTextField.setFieldLabel("shipFromLocDateTextField");
        shipFromLocDateTextField.setAllowBlank(true);
        shipFromLocDateTextField.setReadOnly(true);
        shipFromLocDateTextField.setMinLength(15);
        shipFromLocDateTextField.setMaxLength(15);
        hp12.add(shipFromLocDateTextField);

        vp6.add(hp11);
        vp6.add(hp12);


        panel.add(vp1, new ColumnData(160));
        panel.add(vp2, new ColumnData(185));
        panel.add(vp3, new ColumnData(145));
        panel.add(vp4, new ColumnData(145));
        panel.add(vp5, new ColumnData(125));
        panel.add(vp6, new ColumnData(145));

        vp.add(panel);
    }

    public void setDisplay(DisplayDTO display) {
        this.display = display;
        updateView();
    }

    public void setSkuMixFinal(Boolean mix) {
    	skuMixFinalCheckBox.setValue(mix);
    }
    
    private void updateView() {
        if (display != null && display.getDisplayId() != null) {
            skuTextField.setEmptyText(display.getSku());
            custTextField.setEmptyText(Service.getCustomerMap().get(display.getCustomerId()).getCustomerName());
            desc.setEmptyText(display.getDescription());

            structureTextField.setEmptyText(Service.getStructureMap().get(display.getStructureId()).getStructureName());
            packoutTextField.setEmptyText("(2) Pack-Out");
            destTextField.setEmptyText(display.getDestination());
            statusTextField.setEmptyText(Service.getStatusMap().get(display.getStatusId()).getStatusName());
            forecastQtyTextField.setEmptyText(display.getQtyForecast().toString());
            prdDueAtPackOutTextField.setEmptyText("07/31/2009");
            shipFromLocDateTextField.setEmptyText("08/19/2009");
            skuMixFinalCheckBox.setValue(display.isSkuMixFinalFlg());
        }
    }
}
