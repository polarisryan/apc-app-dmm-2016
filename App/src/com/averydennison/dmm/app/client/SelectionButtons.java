package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;

/**
 * User: Spart Arguello
 * Date: Oct 15, 2009
 * Time: 4:02:12 PM
 */
public class SelectionButtons extends LayoutContainer {
    private ButtonBar buttonBar;
    private SelectionListener listener;

    public SelectionButtons() {
        SelectionListener listener = new SelectionListener<ComponentEvent>() {
            public void componentSelected(ComponentEvent ce) {
                Button btn = (Button) ce.getComponent();
                //Info.display("Click Event", "The '{0}' button was clicked.", btn.getText());
            }
        };
        buttonBar = new ButtonBar();
        buttonBar.setWidth(900);
        buttonBar.add(new FillToolItem());
        buttonBar.add(new Button("View", listener));
        buttonBar.add(new Button("Edit", listener));
        buttonBar.add(new Button("Create", listener));
        buttonBar.add(new Button("Copy", listener));

        add(buttonBar);
    }
}
