package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.DateTimePropertyEditor;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;

public class CostAnalysisInfoGroup {
	public  ComboBox<StatusDTO> status ;
	public  ComboBox<DisplaySalesRepDTO> projectManager;
	public  ComboBox<ManufacturerCountryDTO> manufacturer;
	public  ComboBox<LocationCountryDTO> packLocation;
	public  ComboBox<PromoPeriodCountryDTO> promoPeriod;
	public  TextField<String> topLevel;
	public  NumberField infoDetailsYear ;
	public  DateField infoDetailsDate ;
	public  TextField<String> infoDetailsProjectNo ;
	public static final String datePattern = "MM/dd/yyyy";
	public static final String currencyPattern="$#,###,###0.00";
	public DisplayDTO displayDTO;
	
	private void setEmptyText() {
        projectManager.setEmptyText("Select Project CMA...");
		infoDetailsDate.setEmptyText("Select Date...");
		infoDetailsYear.setEmptyText("Enter Promo Year...");
		infoDetailsProjectNo.setEmptyText("Enter CM Project #...");
		status.setEmptyText("Select Status...");
		promoPeriod.setEmptyText("Select Promo Period...");
		topLevel.setEmptyText("Enter Top Level SKU...");
		manufacturer.setEmptyText("Select Corrugate...");
		packLocation.setEmptyText("Select Packing Location...");
		
	}
	
	public void enableDisableButtons(boolean bEditable){
			projectManager.setReadOnly(bEditable);
			infoDetailsDate.setReadOnly(bEditable);
			infoDetailsYear.setReadOnly(bEditable);
			infoDetailsProjectNo.setReadOnly(bEditable);
			status.setReadOnly(bEditable);
			promoPeriod.setReadOnly(bEditable);
			topLevel.setReadOnly(bEditable);
			manufacturer.setReadOnly(bEditable);
			packLocation.setReadOnly(bEditable);
			if(!bEditable){
				/*
				 * Force readonly, because it is never going to be used
				 */
				infoDetailsProjectNo.setReadOnly(true);
			}
	}
	public CostAnalysisInfoGroup(){
		 	
	}
	
	Double currYear=(System.currentTimeMillis()/1000/3600/24/365.25 +1970);
	public void resetDisplay(DisplayCostDTO displayCostModel, DisplayScenarioDTO displayScenario){
		topLevel.setValue(displayScenario.getSku());
		status.setValue(Service.getStatusMap().get(displayScenario.getScenariodStatusId()));
		projectManager.setValue(Service.getProjectManagerMap().get(displayScenario.getDisplaySalesRepId()));
		manufacturer.setValue(Service.getManufacturerCountryMap().get(displayScenario.getManufacturerId()));
		promoPeriod.setValue(Service.getPromoPeriodCountryMap().get(displayScenario.getPromoPeriodId()));
		infoDetailsYear.setValue(displayScenario.getPromoYear()==null ?currYear.longValue(): displayScenario.getPromoYear());
		packLocation.setValue(Service.getLocationCountryMap().get(displayScenario.getPackoutVendorId()));
		infoDetailsDate.setValue(displayCostModel.getCaDate());
	}
	public ContentPanel cp;
	ListStore<PromoPeriodCountryDTO> promoPeriodsCountry;
	ListStore<LocationCountryDTO> locationsCountry;
	ListStore<ManufacturerCountryDTO> manufacturersCountry; 
	public ContentPanel createGroup(DisplayDTO display, DisplayScenarioDTO displayScenarioDTO){
		status = new ComboBox<StatusDTO>();
	    projectManager = new ComboBox<DisplaySalesRepDTO>();
	    manufacturer = new ComboBox<ManufacturerCountryDTO>();
	    promoPeriod = new ComboBox<PromoPeriodCountryDTO>();
	    topLevel = new TextField<String>();
	    packLocation = new ComboBox<LocationCountryDTO>();
	    infoDetailsYear = new NumberField();
	    infoDetailsDate = new DateField();
	    infoDetailsProjectNo = new TextField<String>();
	    cp = new ContentPanel();
		cp.setBodyBorder(false);
		cp.setHeaderVisible(false);
		cp.setLayout(new RowLayout());
		cp.setSize(645, 76);
		HorizontalPanel hp1 = new HorizontalPanel();
		HorizontalPanel hp2 = new HorizontalPanel();
		HorizontalPanel hp3 = new HorizontalPanel();
		cp.add(hp1);
		cp.add(hp2);
		cp.add(hp3);
		this.displayDTO=display;
		Service service = new Service();
		ListStore<StatusDTO> statuses = new ListStore<StatusDTO>();
		service.setStatusStore(statuses);
		ListStore<CustomerCountryDTO> customers = new ListStore<CustomerCountryDTO>();
		service.setCustomerCountryStore(display.getCountryId(),customers);
		ListStore<DisplaySalesRepDTO> displaySalesReps = new ListStore<DisplaySalesRepDTO>();
		AppService.App.getInstance().getDisplaySalesRepDTOs(new Service.SetProjectManagerStore(displaySalesReps));
		manufacturersCountry = new ListStore<ManufacturerCountryDTO>();
		AppService.App.getInstance().getManufacturerCountryDTOs(new Service.SetManufacturersCountryStore(display.getCountryId(),manufacturersCountry));
		promoPeriodsCountry = new ListStore<PromoPeriodCountryDTO>();
		AppService.App.getInstance().getPromoPeriodCountryDTOs(new Service.SetPromoPeriodsCountryStore(display.getCountryId(),promoPeriodsCountry));
		locationsCountry = new ListStore<LocationCountryDTO>();
        AppService.App.getInstance().loadLocationCountryDTOs( new Service.LoadLocationCountryStore(display.getCountryId(),locationsCountry));
		projectManager.setDisplayField(DisplaySalesRepDTO.FULL_NAME);
		projectManager.setStore(displaySalesReps);
		projectManager.setTriggerAction(ComboBox.TriggerAction.ALL);
		final DateTimePropertyEditor dateTimePropertyEditor = new DateTimePropertyEditor(datePattern);
		infoDetailsDate.setPropertyEditor(dateTimePropertyEditor);
		infoDetailsDate.setAllowBlank(true);
		infoDetailsYear.setAllowBlank(true);
		infoDetailsYear.setMinLength(4);
		infoDetailsYear.setMaxLength(4);
		infoDetailsProjectNo.setMaxLength(40);

		status.setDisplayField(StatusDTO.STATUS_NAME);
		status.setStore(statuses);
		status.setEditable(false);
		status.setTriggerAction(ComboBox.TriggerAction.ALL);
		
		promoPeriod.setDisplayField(PromoPeriodCountryDTO.PROMO_PERIOD_NAME);
		promoPeriod.setStore(promoPeriodsCountry);
		promoPeriod.setEditable(false);
		promoPeriod.setTriggerAction(ComboBox.TriggerAction.ALL);
		promoPeriod.addListener(Events.Expand, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent ke) {
	            	/*if(displayDTO.getCountryId()!=null){
	            		promoPeriodsCountry.filter(PromoPeriodCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(displayDTO.getCountryId()).getCountryName());
	            	}
	            	promoPeriod.setEnabled(true);*/
	            	System.out.println("CostAnalysisInfoGroup:promoPeriod countryId="+App.getDisplayTabs().getDisplayModel());
	            	if(App.getDisplayTabs().getDisplayModel().getCountryId()!=null){
	            		promoPeriodsCountry.filter(PromoPeriodCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(App.getDisplayTabs().getDisplayModel().getCountryId()).getCountryName());
	            	}
	            	promoPeriod.setEnabled(true);
	            }
	        });
		    
		topLevel.setMaxLength(15);
		topLevel.setMinLength(15);
		
		manufacturer.setDisplayField(ManufacturerCountryDTO.NAME);
		manufacturer.setStore(manufacturersCountry);
		manufacturer.setEditable(false);
		manufacturer.setTriggerAction(ComboBox.TriggerAction.ALL);
		
	 	packLocation.setFieldLabel("Location");
        packLocation.setDisplayField(LocationCountryDTO.LOCATION_NAME);
        packLocation.setValueField(LocationCountryDTO.LOCATION_CODE);

        packLocation.setStore(locationsCountry);
        packLocation.setAllowBlank(true);
        packLocation.setTypeAhead(true);
        packLocation.setEditable(false);
        packLocation.setTriggerAction(ComboBox.TriggerAction.ALL);
        manufacturer.addListener(Events.Expand, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
            	//System.out.println(App.getDisplayTabs().getDisplayModel())
            	/*if(displayDTO.getCountryId()!=null){
            		manufacturersCountry.filter(ManufacturerCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(displayDTO.getCountryId()).getCountryName());
            	}
            	manufacturer.setEnabled(true);
*/           
            	System.out.println("CostAnalysisInfoGroup:manufacturer countryId="+App.getDisplayTabs().getDisplayModel());
            	if(App.getDisplayTabs().getDisplayModel().getCountryId()!=null){
            		manufacturersCountry.filter(ManufacturerCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(App.getDisplayTabs().getDisplayModel().getCountryId()).getCountryName());
            	}
            	manufacturer.setEnabled(true);
            	}
        });
        packLocation.addListener(Events.Expand, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
            	/*if(displayDTO.getCountryId()!=null){
            		locationsCountry.filter(LocationCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(displayDTO.getCountryId()).getCountryName());
            	}
            	packLocation.setEnabled(true);*/
            	System.out.println("CostAnalysisInfoGroup:packLocation countryId="+App.getDisplayTabs().getDisplayModel());
            	if(App.getDisplayTabs().getDisplayModel().getCountryId()!=null){
            		locationsCountry.filter(LocationCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(App.getDisplayTabs().getDisplayModel().getCountryId()).getCountryName());
            	}
            	packLocation.setEnabled(true);
            }
        });

        hp1.setSpacing(2);
		hp2.setSpacing(2);
		hp3.setSpacing(2);

		projectManager.setWidth(105+23);
		infoDetailsProjectNo.setWidth(105+23);
		topLevel.setWidth(105+23);
		infoDetailsDate.setWidth(105+23);
		status.setWidth(105+23);
		packLocation.setWidth(105+23);	
		infoDetailsYear.setWidth(105+23);
		promoPeriod.setWidth(105+23);
		manufacturer.setWidth(105+23);

		LabelField buDescLabel = new LabelField("CMA");
		buDescLabel.setWidth(30);
		hp1.add(buDescLabel);

		hp1.add(projectManager);

		HorizontalPanel spacer1 = new HorizontalPanel();
		spacer1.setWidth(2);
		hp1.add(spacer1);

		buDescLabel = new LabelField("CM Project #");
		buDescLabel.setWidth(103);
		hp1.add(buDescLabel);

		hp1.add(infoDetailsProjectNo);

		HorizontalPanel spacer12 = new HorizontalPanel();
		spacer12.setWidth(2);
		hp1.add(spacer12);

		buDescLabel = new LabelField("Top Level SKU");
		buDescLabel.setWidth(108);
		hp1.add(buDescLabel);
		hp1.add(topLevel);


		buDescLabel = new LabelField("Date");
		buDescLabel.setWidth(30);
		hp2.add(buDescLabel);
		hp2.add(infoDetailsDate);

		HorizontalPanel spacer2 = new HorizontalPanel();
		spacer2.setWidth(2);
		hp2.add(spacer2);

		buDescLabel = new LabelField("Status");
		buDescLabel.setWidth(103);
		hp2.add(buDescLabel);
		hp2.add(status);

		HorizontalPanel spacer21 = new HorizontalPanel();
		spacer21.setWidth(2);
		hp2.add(spacer21);

		buDescLabel = new LabelField("Corrugate Vendor");
		buDescLabel.setWidth(108);
		hp2.add(buDescLabel);
		hp2.add(manufacturer);

		buDescLabel = new LabelField("Year");
		buDescLabel.setWidth(30);
		hp3.add(buDescLabel);
		hp3.add(infoDetailsYear);

		HorizontalPanel spacer3 = new HorizontalPanel();
		spacer3.setWidth(2);
		hp3.add(spacer3);

		buDescLabel = new LabelField("Promo Period");
		buDescLabel.setWidth(103);
		hp3.add(buDescLabel);
		hp3.add(promoPeriod);

		HorizontalPanel spacer31 = new HorizontalPanel();
		spacer31.setWidth(2);
		hp3.add(spacer31);
		
		buDescLabel = new LabelField("Packout Location");
		buDescLabel.setWidth(108);
		hp3.add(buDescLabel);
		hp3.add(packLocation);

		setEmptyText();
		return cp;
	}
	
	  public ContentPanel vendorPanel;
	  
	  public  TextField<String> vendorManufacturer;
	  public  TextField<String> vendorPackVendor;
		
	  public ContentPanel createCostAnalysisVendorInfoPanel(Long countryId, DisplayScenarioDTO displayScenarioDTO) {
		vendorPanel = new ContentPanel();
		vendorPanel.setBodyBorder(false);
		vendorPanel.setHeaderVisible(false);
		vendorPanel.setLayout(new RowLayout());
		vendorPanel.setSize(245, 56);
		HorizontalPanel hp1 = new HorizontalPanel();
		HorizontalPanel hp2 = new HorizontalPanel();
		hp1.setSpacing(2);
		hp2.setSpacing(2);
		vendorPanel.add(hp1);
		vendorPanel.add(hp2);
		Service service = new Service();
		ListStore<ManufacturerCountryDTO> manufacturers = new ListStore<ManufacturerCountryDTO>();
		AppService.App.getInstance().getManufacturerCountryDTOs(new Service.SetManufacturersCountryStore(countryId,manufacturers));
		ListStore<LocationCountryDTO> locations = new ListStore<LocationCountryDTO>();		
		AppService.App.getInstance().loadLocationCountryDTOs(new Service.LoadLocationCountryStore(countryId,locations));
		vendorManufacturer = new TextField<String>();
		vendorPackVendor = new TextField<String>();
		vendorManufacturer.setReadOnly(true);
		vendorManufacturer.setFieldLabel("manufacturerTextField");
		vendorManufacturer.setAllowBlank(true);
		vendorManufacturer.setReadOnly(true);
		vendorPackVendor.setReadOnly(true);
		vendorPackVendor.setFieldLabel("packVendorField");
		vendorPackVendor.setAllowBlank(true);
		vendorPackVendor.setReadOnly(true);
		vendorPackVendor.setWidth(150+23);	
		vendorManufacturer.setWidth(150+23);
		LabelField buDescLabel = new LabelField("Packout Location");
		buDescLabel.setWidth(108);
		hp2.add(buDescLabel);
		hp2.add(vendorPackVendor);
		buDescLabel = new LabelField("Corrugate Vendor");
		buDescLabel.setWidth(108);
		hp1.add(buDescLabel);
		hp1.add(vendorManufacturer);
		setEmptyVendorText();
		return vendorPanel;
	    }

	  public void setEmptyVendorText(){
		  vendorManufacturer.setEmptyText("");
		  vendorPackVendor.setEmptyText("");
	  }
		
	private ListStore<ManufacturerCountryDTO> manufacturerStore = new ListStore<ManufacturerCountryDTO>();
	public ListStore<ManufacturerCountryDTO> getManufacturerStore() {
        return manufacturerStore;
    }
	public ComboBox<ManufacturerCountryDTO> getManufacturer() {
        return manufacturer;
    }


	
	
	
}
