package com.averydennison.dmm.app.client;

import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDcDTO;
import com.averydennison.dmm.app.client.models.LocationDTO;
import com.averydennison.dmm.app.client.util.KeyUtil;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.user.client.ui.Grid;

/**
 * User: Spart Arguello
 * Date: Dec 8, 2009
 * Time: 10:43:58 AM
 */
public class DCNode extends LayoutContainer implements ValidationHelper {
    private DisplayDcDTO dcModel;
    private ChangeListener dcListener;
    final ListStore<LocationDTO> locationStore = new ListStore<LocationDTO>();
    final ComboBox<LocationDTO> locationComboBox = new ComboBox<LocationDTO>();
    final TextField<Long> qty = new TextField<Long>();
    private List<LocationDTO> locationList;

    public DCNode(List<LocationDTO> locationList) {
        this.locationList = locationList;
        setup();
    }

    private SelectionListener buttonListener = new SelectionListener<ComponentEvent>() {
        public void componentSelected(ComponentEvent ce) {
            Button btn = (Button) ce.getComponent();

            if (btn.getText().equals("DELETE")) {
                //Info.display("remove node from display", "");
            }
        }
    };

    private void setup() {
        setLayout(new FlowLayout(10));

        int x = 50;

        //locationStore = new ListStore<LocationDTO>();
        //AppService.App.getInstance().loadLocationDTOs(new Service.LoadLocationStore(locationStore));
        locationStore.add(this.locationList);
        locationComboBox.setStore(locationStore);
        locationComboBox.setWidth(x);
        locationComboBox.setDisplayField(LocationDTO.LOCATION_CODE);
        locationComboBox.setTriggerAction(ComboBox.TriggerAction.ALL);
        locationComboBox.setEditable(false);

        locationComboBox.setAutoValidate(true);

        locationComboBox.setAllowBlank(false);


        qty.setAllowBlank(false);
        qty.setMinLength(1);
        qty.setMaxLength(10);
        qty.setWidth(x * 2);

        qty.setValidator(new Validator() {
            public String validate(Field<?> field, String value) {
                if (Long.valueOf(value.trim()).equals(0L)) return "invalid qty";
                return null;
            }
        });

        qty.setAutoValidate(true);
//        qty.addListener(Events.KeyUp, new Listener<FieldEvent>() {
//            public void handleEvent(FieldEvent be) {
//                if(qty.validate()){
//                   qty.setValue(Long.valueOf(qty.getValue()));
//                }
//            }
//        });
        qty.addListener(Events.KeyDown, new Listener<FieldEvent>() {

            public void handleEvent(FieldEvent ke) {
                int keyCode = ke.getKeyCode();
                if (!KeyUtil.isDigit(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress()) {
                    ke.stopEvent();
                }
            }
        });

        Grid grid = new Grid(1, 5);
//        grid.setCellPadding(10);
        grid.setWidget(0, 0, locationComboBox);
        grid.setHTML(0, 1, "&nbsp;&nbsp;&nbsp;&nbsp;");
        grid.setWidget(0, 2, qty);
        grid.setHTML(0, 3, "&nbsp;&nbsp;&nbsp;&nbsp;");
        grid.setWidget(0, 4, new Button("Delete", buttonListener));
        grid.setSize("105px", "55px");

        add(grid);
    }

    public boolean validateControls() {
        if (!locationComboBox.validate()) return false;
        if (!qty.validate()) return false;
        else return true;
    }

    public void disableControls() {
        locationComboBox.disable();
        qty.disable();
    }

    public void enableControls() {
        locationComboBox.enable();
        qty.enable();
    }

    public Long getQty() {
        return Long.valueOf(qty.getRawValue());
    }

    public Long getLocationId() {
        return locationComboBox.getSelection().get(0).getLocationId();
    }

    public DisplayDcDTO getDcModel() {
        return dcModel;
    }

    public void setDcModel(DisplayDcDTO dcModel) {
        this.dcModel = dcModel;
        updateQuantityField();
    }

    private LocationDTO location;

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
        udpateLocationCombo();
    }

    private void updateQuantityField() {
        qty.setValue(dcModel.getQuantity());
    }

    private void udpateLocationCombo() {
        if (locationStore == null || locationStore.getCount() == 0) {
            AppService.App.getInstance().loadLocationDTOs(new Service.LoadLocationStore(locationStore));
        }
        if (locationStore.getCount() > 0 && location != null) {
            locationComboBox.setValue(location);
        }
    }
}
