package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.averydennison.dmm.app.client.models.BuCostCenterSplitDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.util.KeyUtil;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.VerticalAlignment;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.AggregationRenderer;
import com.extjs.gxt.ui.client.widget.grid.AggregationRowConfig;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.SummaryType;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Element;

public class DisplayCostDataGroup extends LayoutContainer implements DirtyInterface{
	final NumberFormat pctFormat = NumberFormat.getFormat("##0.00");
	final String currencyFormat = "$###,###,##0.00";
	final String percentFormat = "##0.00%";
	public NumberField overageScrapNo;
	public NumberField overageScrapPct ;
	public NumberField totalDisplaysOrdered ;
	public NumberField foreCastQty ;
	public NumberField actualQty ;
	public NumberField customerProgramPercentage ;
	public NumberField  mdfPct;
	public NumberField  discountPct;
	public NumberField tradeSalesChanged;
	
	
	public CurrencyField corrugateCostPerUnit;
	public CurrencyField fullfillmentCostPerUnit ;
	public CurrencyField miscCost;
	public CurrencyField palletCost ;
	public CurrencyField shipTestCost ;
	public CurrencyField totalArtworkCost;
	public CurrencyField printingPlateCost;
	public CurrencyField dieCuttingCost ;
	public CurrencyField cTPCost ;
	public CurrencyField expectedFrieghtCost ;
	public CurrencyField fines ;
	public LabelField lblCorrugateCosts ;
	public LabelField lblFullfillmentCosts ;
	public LabelField lblOtherCosts ;
	public LabelField lblTotalDisplayCosts ;
	public LabelField lblTradeSales ;
	public LabelField lblMdfDollor;
	public LabelField lblDiscountDollor;
	public LabelField lblCustProgramDisc ;
	public LabelField lblNetSales ;
	public LabelField lblCOGS ;
	public LabelField lblProductVarMargin ;
	public LabelField lblDisplayCosts ;
	public LabelField lblVmAmount ;
	public LabelField lblCmPercentage ;
	public LabelField lblExcessVmAmount ;
	public LabelField lblExcessCmPercentage ;
	public LabelField lblHurdlesMet ;
	public LabelField hurdlesMetLabel ;
	public LabelField  mdfPctLabel;
	public LabelField  discountPctLabel;
	
	
	private Window window = new Window();
	final int overAgeMaxValue=999;
	final int qtyMaxValue=9999;
	final Double currencyMaxValue=99999.99d;
	final Double currencyMaxValueForTradeSales=999999.99d;
	final Double discountMaxValue=100.00d;
	final int percentMaxValue=100;
	private DisplayDTO displayDTO;
	private DisplayScenarioDTO displayScenarioDTO;
	private CostAnalysisInfo costAnalysisInfo;
	private DisplayTabs displayTabs;
	public static final String SEARCH = "Search";

	public DisplayTabs getDisplayTabs() {
		return displayTabs;
	}
	public void setDisplayTabs(DisplayTabs displayTabs) {
		this.displayTabs = displayTabs;
	}
	
	public DisplayScenarioDTO getDisplayScenarioDTO() {
		return displayScenarioDTO;
	}
	public void setDisplayScenarioDTO(DisplayScenarioDTO displayScenarioDTO) {
		this.displayScenarioDTO = displayScenarioDTO;
	}
	public DisplayDTO getDisplayDTO() {
		return displayDTO;
	}
	public void setDisplayDTO(DisplayDTO displayDTO) {
		this.displayDTO = displayDTO;
	}

	@Override
	protected void onRender(Element parent, int index) {
		super.onRender(parent, index);
		System.out.println("DisplayCostDataGroup:onRender starts");
	}
	
	public DisplayCostDataGroup(){
		overageScrapNo = new NumberField();
		overageScrapPct = new NumberField();
		totalDisplaysOrdered = new NumberField();
		foreCastQty = new NumberField();
		actualQty = new NumberField();
		customerProgramPercentage = new NumberField();
		mdfPct= new NumberField();
		discountPct= new NumberField();
		tradeSalesChanged = new NumberField();
		
		corrugateCostPerUnit = new CurrencyField();
		fullfillmentCostPerUnit = new CurrencyField();
		miscCost = new CurrencyField();
		palletCost = new CurrencyField();
		shipTestCost = new CurrencyField();
		totalArtworkCost = new CurrencyField();
		printingPlateCost = new CurrencyField();
		dieCuttingCost = new CurrencyField();
		cTPCost = new CurrencyField();
		expectedFrieghtCost = new CurrencyField();
		fines = new CurrencyField();

		lblCorrugateCosts = new LabelField();
		lblFullfillmentCosts = new LabelField();
		lblOtherCosts = new LabelField();
		lblTotalDisplayCosts = new LabelField();
		lblTradeSales = new LabelField();
		lblMdfDollor = new LabelField();
		lblDiscountDollor = new LabelField();
		
		lblCustProgramDisc = new LabelField();
		lblNetSales = new LabelField();
		lblCOGS = new LabelField();
		lblProductVarMargin = new LabelField();
		lblDisplayCosts = new LabelField();
		lblVmAmount = new LabelField();
		lblCmPercentage = new LabelField();
		lblHurdlesMet = new LabelField();
		hurdlesMetLabel = new LabelField();
		lblExcessVmAmount = new LabelField();
		lblExcessCmPercentage = new LabelField();
		
		 totalActualInvoiceTotCostVal = new Double(0.00);
		 totalCogsVal=new Double(0.00);
		 totalDisplayCostsVal=new Double(0.00);
		 overageScrapNoVal=new Double(0.00);
		 totalDisplaysOrderedVal=new Double(0.00);
		 corrugateCostPerUnitVal=new Double(0.00);
		 fullfillmentCostPerUnitVal=new Double(0.00);
		 corrugateCostVal=new Double(0.00);
		 fullfillmentCostVal=new Double(0.00);
		 miscCostVal =new Double(0.00);
		 palletCostVal=new Double(0.00);
		 shipTestCostVal=new Double(0.00);
		 totalArtworkCostVal=new Double(0.00);
		 printingPlateCostVal=new Double(0.00);
		 dieCuttingCostVal=new Double(0.00);
		 cTPCosttVal =new Double(0.00);
		 otherCostsVal=new Double(0.00);
		 tradeSalesVal=new Double(0.00);
		 customerProgramPercentageVal=new Double(0.00);
		 
		 mdfPctVal=new Double(0.00);
		 discountPctVal=new Double(0.00);
		 
		 custProgramDiscVal=new Double(0.00);
		 netSalesVal=new Double(0.00);
		 cOGSVal=new Double(0.00);
		 productVarMarginVal=new Double(0.00);
		 displayCostsVal=new Double(0.00);
		 vmAmountVal=new Double(0.00);
		 vmPercentageVal=new Double(0.00);
		 sumVmDollar=new Double(0.00);
		 sumVmPct=new Double(0.00);
		 totalNumberFacing=new Long(0);
		 totalQtyPerDisplay=new Long(0);
		 totalQtyPerFacing=new Long(0);
	}
	
	ContentPanel displayCostDataPanel;
	HorizontalPanel displayCostDatahp1;
	HorizontalPanel displayCostDatahp2;
	HorizontalPanel displayCostDatahp3;
	HorizontalPanel displayCostDatahp4;
	HorizontalPanel displayCostDatahp5;
	HorizontalPanel displayCostDatahp6;
	HorizontalPanel displayCostDatahp7;
	HorizontalPanel displayCostDatahp8;
	HorizontalPanel displayCostDatahp9;
	HorizontalPanel displayCostDatahp10;
	HorizontalPanel displayCostDatahp11;
	HorizontalPanel displayCostDatahp12;
	HorizontalPanel displayCostDatahp13;
	HorizontalPanel displayCostDatahp14;
	HorizontalPanel displayCostDatahp16;
	HorizontalPanel displayCostDatahp17;
	LabelField foreCastQtyLabel;
	LabelField actualQtyLabel;
	LabelField overageScrapNoLabel;
	LabelField overageScrapPctLabel;
	LabelField totalDisplaysOrderedLabel;
	LabelField corrugateCostPerUnitLabel;
	LabelField fullfillmentCostPerUnitLabel ;
	LabelField cTPCostLabel;
	LabelField dieCuttingCostLabel;
	LabelField printingPlateCostLabel;
	LabelField totalArtworkCostLabel;
	LabelField shipTestCostLabel;
	LabelField palletCostLabel;
	LabelField miscCostLabel;
	LabelField expectedFrieghtCostLabel ;
	LabelField finesLabel;
	
	public ContentPanel createDisplayCostDataGroup(){
		int x=60;
		displayCostDataPanel = new ContentPanel();
		System.out.println("DisplayCostDataGroup:createDisplayCostDataGroup starts");
		try{
			displayCostDataPanel.setBodyBorder(false);
			displayCostDataPanel.setHeaderVisible(false);
			displayCostDataPanel.setLayout(new RowLayout());
			displayCostDataPanel.setSize(322, 432);
			displayCostDatahp1 = new HorizontalPanel();
			displayCostDatahp2 = new HorizontalPanel();
			displayCostDatahp3 = new HorizontalPanel();
			displayCostDatahp4 = new HorizontalPanel();
			displayCostDatahp5 = new HorizontalPanel();
			displayCostDatahp6 = new HorizontalPanel();
			displayCostDatahp7 = new HorizontalPanel();
			displayCostDatahp8 = new HorizontalPanel();
			displayCostDatahp9 = new HorizontalPanel();
			displayCostDatahp10 = new HorizontalPanel();
			displayCostDatahp11 = new HorizontalPanel();
			displayCostDatahp12 = new HorizontalPanel();
			displayCostDatahp13 = new HorizontalPanel();
			displayCostDatahp14 = new HorizontalPanel();
			displayCostDatahp16 = new HorizontalPanel();
			displayCostDatahp17 = new HorizontalPanel();
			displayCostDatahp1.setSpacing(2);
			displayCostDatahp2.setSpacing(2);
			displayCostDatahp3.setSpacing(2);
			displayCostDatahp4.setSpacing(2);
			displayCostDatahp5.setSpacing(2);
			displayCostDatahp6.setSpacing(2);
			displayCostDatahp7.setSpacing(2);
			displayCostDatahp8.setSpacing(2);
			displayCostDatahp9.setSpacing(2);
			displayCostDatahp10.setSpacing(2);
			displayCostDatahp11.setSpacing(2);
			displayCostDatahp12.setSpacing(2);
			displayCostDatahp13.setSpacing(2);
			displayCostDatahp14.setSpacing(2);
			displayCostDatahp16.setSpacing(2);
			displayCostDatahp17.setSpacing(2);
			foreCastQtyLabel = new LabelField("Forecast Qty:");
			foreCastQtyLabel.setWidth(x*4 - 10);
			displayCostDatahp1.add(foreCastQtyLabel);
			foreCastQty.addInputStyleName("text-element-no-underline");
			foreCastQty.setPropertyEditorType(Double.class);
			foreCastQty.setAllowDecimals(false);
			foreCastQty.setTabIndex(1);
			foreCastQty.setMaxLength(5);
			foreCastQty.setWidth(x+10);
			if(getDisplayDTO()!=null)
			foreCastQty.setValue(getDisplayDTO().getQtyForecast());
			foreCastQty.setAutoValidate(true);
			foreCastQty.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					if (!KeyUtil.isDigit(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress() && !ke.isSpecialKey()) {
						ke.stopEvent();
					}
					Double dForeCastQtyVal = (Double) (foreCastQty.getValue()==null?0:foreCastQty.getValue().doubleValue());
					if(dForeCastQtyVal > qtyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			foreCastQty.addListener(Events.Blur, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
				}
			});
			displayCostDatahp1.add(foreCastQty);
			actualQtyLabel = new LabelField("Actual Qty:");
			actualQtyLabel.setWidth(x*4 - 10);
			displayCostDatahp2.add(actualQtyLabel);
			actualQty.addInputStyleName("text-element-no-underline");
			actualQty.setPropertyEditorType(Double.class);
			actualQty.setAllowDecimals(false);
			actualQty.setAutoValidate(true);
			actualQty.setMaxLength(5);
			actualQty.setWidth(x+10);
			if(getDisplayDTO()!=null)
				actualQty.setValue(getDisplayDTO().getActualQty());
			actualQty.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					if (!KeyUtil.isDigit(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress() && !ke.isSpecialKey()) {
						ke.stopEvent();
					}
					Double dActualQtyVal = (Double) (actualQty.getValue()==null?0:actualQty.getValue().doubleValue());
					if(dActualQtyVal > qtyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			actualQty.addListener(Events.Blur, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
				}
			});
			actualQty.setTabIndex(2);
			displayCostDatahp2.add(actualQty);

			overageScrapNoLabel = new LabelField("Overage/Scrap #:");
			overageScrapNoLabel.setWidth(x*4 - 10);
			displayCostDatahp3.add(overageScrapNoLabel);
			overageScrapNo.addInputStyleName("text-element-no-underline");
			overageScrapNo.setPropertyEditorType(Double.class);
			overageScrapNo.setAllowDecimals(false);
			overageScrapNo.setMaxLength(4);
			overageScrapNo.setAutoValidate(true);
			overageScrapNo.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					if (!KeyUtil.isDigit(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress() && !ke.isSpecialKey()) {
						ke.stopEvent();
					}
					Double dOverageScrapNoVal = (Double) (overageScrapNo.getValue()==null?0:overageScrapNo.getValue().doubleValue());
					if(dOverageScrapNoVal.intValue() > overAgeMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			overageScrapNo.addListener(Events.Blur, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
				}
			});
			overageScrapNo.setWidth(x+10);
			overageScrapNo.setTabIndex(3);
			displayCostDatahp3.add(overageScrapNo);

			overageScrapPctLabel = new LabelField("Overage/Scrap %:");
			overageScrapPctLabel.setWidth(x*4 - 10);
			displayCostDatahp4.add(overageScrapPctLabel);
			overageScrapPct.addInputStyleName("text-element-no-underline");
			overageScrapPct.setPropertyEditorType(Double.class);
			overageScrapPct.setMaxLength(7);
			overageScrapPct.setWidth(x+10);
			overageScrapPct.setReadOnly(true);
			overageScrapPct.setAllowDecimals(true);
			overageScrapPct.setTabIndex(-1);
			displayCostDatahp4.add(overageScrapPct);

			totalDisplaysOrderedLabel = new LabelField("Tot Displays Ordered:");
			totalDisplaysOrderedLabel.setWidth(x*4 - 10);
			displayCostDatahp5.add(totalDisplaysOrderedLabel);
			totalDisplaysOrdered.addInputStyleName("text-element-no-underline");
			totalDisplaysOrdered.setPropertyEditorType(Integer.class);
			totalDisplaysOrdered.setAllowDecimals(false);
			totalDisplaysOrdered.setAllowBlank(false);
			totalDisplaysOrdered.setReadOnly(true);
			totalDisplaysOrdered.setMaxLength(5);
			totalDisplaysOrdered.setWidth(x+10);
			totalDisplaysOrdered.setTabIndex(-2);
			displayCostDatahp5.add(totalDisplaysOrdered);

			corrugateCostPerUnitLabel = new LabelField("Corrugate Cost/Unit:");
			corrugateCostPerUnitLabel.setWidth(x*3);
			displayCostDatahp6.add(corrugateCostPerUnitLabel);
			corrugateCostPerUnit.addInputStyleName("text-element-no-underline");
			corrugateCostPerUnit.setMaxLength(11);
			corrugateCostPerUnit.setWidth(x*2);
			corrugateCostPerUnit.setAutoValidate(true);
			corrugateCostPerUnit.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dCorrugateCostPerUnitVal = (Double) (corrugateCostPerUnit.getValue()==null?0:corrugateCostPerUnit.getValue().doubleValue());
					if(dCorrugateCostPerUnitVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			corrugateCostPerUnit.setTabIndex(4);
			displayCostDatahp6.add(corrugateCostPerUnit);

			fullfillmentCostPerUnitLabel = new LabelField("Fulfillment Cost/Unit:");
			fullfillmentCostPerUnitLabel.setWidth(x*3);
			displayCostDatahp7.add(fullfillmentCostPerUnitLabel);
			fullfillmentCostPerUnit.addInputStyleName("text-element-no-underline");
			fullfillmentCostPerUnit.setMaxLength(11);
			fullfillmentCostPerUnit.setWidth(x*2);
			fullfillmentCostPerUnit.setAutoValidate(true);
			fullfillmentCostPerUnit.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dFullfillmentCostPerUnitVal = (Double) (fullfillmentCostPerUnit.getValue()==null?0:fullfillmentCostPerUnit.getValue().doubleValue());
					if(dFullfillmentCostPerUnitVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			fullfillmentCostPerUnit.setTabIndex(5);
			displayCostDatahp7.add(fullfillmentCostPerUnit);

			cTPCostLabel = new LabelField("CTP Cost:");
			cTPCostLabel.setWidth(x*3);
			displayCostDatahp8.add(cTPCostLabel);
			cTPCost.addInputStyleName("text-element-no-underline");
			cTPCost.setWidth(x*2);
			cTPCost.setMaxLength(11);
			cTPCost.setAutoValidate(true);
			cTPCost.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dCTPCostVal = (Double) (cTPCost.getValue()==null?0:cTPCost.getValue().doubleValue());
					if(dCTPCostVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			cTPCost.setTabIndex(6);
			displayCostDatahp8.add(cTPCost);


			dieCuttingCostLabel = new LabelField("Die Cutting Cost:");
			dieCuttingCostLabel.setWidth(x*3);
			displayCostDatahp9.add(dieCuttingCostLabel);
			dieCuttingCost.setWidth(x*2);
			dieCuttingCost.setAutoValidate(true);
			dieCuttingCost.addInputStyleName("text-element-no-underline");		
			dieCuttingCost.setMaxLength(11);
			dieCuttingCost.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dDieCuttingCostVal = (Double) (dieCuttingCost.getValue()==null?0:dieCuttingCost.getValue().doubleValue());
					if(dDieCuttingCostVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			dieCuttingCost.setTabIndex(7);
			displayCostDatahp9.add(dieCuttingCost);

			printingPlateCostLabel = new LabelField("Printing Plate Cost:");
			printingPlateCostLabel.setWidth(x*3);
			displayCostDatahp10.add(printingPlateCostLabel);
			printingPlateCost.addInputStyleName("text-element-no-underline");		
			printingPlateCost.setMaxLength(11);
			printingPlateCost.setWidth(x*2);
			printingPlateCost.setAutoValidate(true);
			printingPlateCost.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dPrintingPlateCostVal = (Double) (printingPlateCost.getValue()==null?0:printingPlateCost.getValue().doubleValue());
					if(dPrintingPlateCostVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			printingPlateCost.setTabIndex(8);
			displayCostDatahp10.add(printingPlateCost);

			totalArtworkCostLabel = new LabelField("Total Artwork Cost:");
			totalArtworkCostLabel.setWidth(x*3);
			displayCostDatahp11.add(totalArtworkCostLabel);
			totalArtworkCost.addInputStyleName("text-element-no-underline");		
			totalArtworkCost.setFieldLabel("Total Artwork Cost");
			totalArtworkCost.setMaxLength(11);
			totalArtworkCost.setWidth(x*2);
			totalArtworkCost.setAutoValidate(true);
			totalArtworkCost.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dTotalArtworkCostVal = (Double) (totalArtworkCost.getValue()==null?0:totalArtworkCost.getValue().doubleValue());
					if(dTotalArtworkCostVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			totalArtworkCost.setTabIndex(9);
			displayCostDatahp11.add(totalArtworkCost);

			shipTestCostLabel = new LabelField("Ship Test Cost:");
			shipTestCostLabel.setWidth(x*3);
			displayCostDatahp12.add(shipTestCostLabel);
			shipTestCost.addInputStyleName("text-element-no-underline");		
			shipTestCost.setMaxLength(11);
			shipTestCost.setWidth(x*2);
			shipTestCost.setAutoValidate(true);
			shipTestCost.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dShipTestCostVal = (Double) (shipTestCost.getValue()==null?0:shipTestCost.getValue().doubleValue());
					if(dShipTestCostVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			shipTestCost.setTabIndex(10);
			displayCostDatahp12.add(shipTestCost);


			palletCostLabel = new LabelField("Pallet Cost:");
			palletCostLabel.setWidth(x*3);
			displayCostDatahp13.add(palletCostLabel);
			palletCost.addInputStyleName("text-element-no-underline");		
			palletCost.setMaxLength(11);
			palletCost.setWidth(x*2);
			palletCost.setAutoValidate(true);
			palletCost.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dPalletCostVal = (Double) (palletCost.getValue()==null?0:palletCost.getValue().doubleValue());
					if(dPalletCostVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			palletCost.setTabIndex(11);
			displayCostDatahp13.add(palletCost);

			miscCostLabel = new LabelField("Misc Cost:");
			miscCostLabel.setWidth(x*3);
			displayCostDatahp14.add(miscCostLabel);
			miscCost.addInputStyleName("text-element-no-underline");	
			miscCost.setMaxLength(11);
			miscCost.setWidth(x*2);
			miscCost.setAutoValidate(true);
			miscCost.setFormat(NumberFormat.getFormat(currencyFormat));
			miscCost.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dMiscCostVal = (Double) (miscCost.getValue()==null?0:miscCost.getValue().doubleValue());
					if(dMiscCostVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});

			miscCost.setTabIndex(12);
			displayCostDatahp14.add(miscCost);
		
			expectedFrieghtCostLabel = new LabelField("Expedited Freight Cost:");
			expectedFrieghtCostLabel.setWidth(x*3);
			displayCostDatahp16.add(expectedFrieghtCostLabel);
			expectedFrieghtCost.addInputStyleName("text-element-no-underline");	
			expectedFrieghtCost.setFieldLabel("Expedited Freight Cost");
			expectedFrieghtCost.setAllowBlank(true);
			expectedFrieghtCost.setFormat(NumberFormat.getFormat(currencyFormat));
			expectedFrieghtCost.setWidth(x*2);
			expectedFrieghtCost.setAutoValidate(true);
			expectedFrieghtCost.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dExpectedFrieghtCostVal = (Double) (expectedFrieghtCost.getValue()==null?0:expectedFrieghtCost.getValue().doubleValue());
					if(dExpectedFrieghtCostVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			expectedFrieghtCost.setMaxLength(11);
			expectedFrieghtCost.setTabIndex(13);
			displayCostDatahp16.add(expectedFrieghtCost);


			finesLabel = new LabelField("Fines:");
			finesLabel.setWidth(x*3);
			displayCostDatahp17.add(finesLabel);
			fines.setFormat(NumberFormat.getFormat(currencyFormat));
			fines.addInputStyleName("text-element-no-underline");	
			fines.setWidth(x*2);
			fines.setTabIndex(14);
			fines.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dFinesVal = (Double) (fines.getValue()==null?0:fines.getValue().doubleValue());
					if(dFinesVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
				}
			});
			fines.setMaxLength(11);
			fines.setAutoValidate(true);
			displayCostDatahp17.add(fines);
			displayCostDataPanel.add(displayCostDatahp1);
			displayCostDataPanel.add(displayCostDatahp2);
			displayCostDataPanel.add(displayCostDatahp3);
			displayCostDataPanel.add(displayCostDatahp4);
			displayCostDataPanel.add(displayCostDatahp5);
			displayCostDataPanel.add(displayCostDatahp6);
			displayCostDataPanel.add(displayCostDatahp7);
			displayCostDataPanel.add(displayCostDatahp8);
			displayCostDataPanel.add(displayCostDatahp9);
			displayCostDataPanel.add(displayCostDatahp10);
			displayCostDataPanel.add(displayCostDatahp11);
			displayCostDataPanel.add(displayCostDatahp12);
			displayCostDataPanel.add(displayCostDatahp13);
			displayCostDataPanel.add(displayCostDatahp14);
			displayCostDataPanel.add(displayCostDatahp16);
			displayCostDataPanel.add(displayCostDatahp17);
		}catch(Exception e){
			System.out.println("====DisplayCostDataGroup:createDisplayCostDataGroup error " + e.toString());
		}
		System.out.println("DisplayCostDataGroup:createDisplayCostDataGroup ends");
		return displayCostDataPanel;
	}

	public void updateView(DisplayScenarioDTO displayScenarioDTO, boolean comparisonView){
		System.out.println("DisplayCostDataGroup:updateView starts");
		loadCostAnalysisComponentInfo(displayScenarioDTO, comparisonView);
		System.out.println("DisplayCostDataGroup:updateView ends");
	}

	ContentPanel financialAnalysisPanel;
	ContentPanel excessInventoryPanel;
	public ContentPanel createExcessInventoryGroup(Long displayScenarioId, boolean minimized){
		try{
		int x=70;
		excessInventoryPanel = new ContentPanel();
		excessInventoryPanel.setBodyBorder(false);
		excessInventoryPanel.setHeaderVisible(false);
		excessInventoryPanel.setLayout(new RowLayout());
		/*
		 * excessInventoryPanel.setSize(274, 67);
		 */
		excessInventoryPanel.setSize(274, 57);
		HorizontalPanel excessInventoryhp1 = new HorizontalPanel();
		HorizontalPanel excessInventoryhp2 = new HorizontalPanel();
		LabelField excessInventoryVMDollarLabel = new LabelField("Excess Inventory CM $:");
		excessInventoryVMDollarLabel.setWidth((x*2)+30);
		excessInventoryhp1.add(excessInventoryVMDollarLabel);
		lblExcessVmAmount.setText(NumberFormat.getFormat((currencyFormat)).format(0.00));
		lblExcessVmAmount.setStyleName("text-element-no-underline");
		lblExcessVmAmount.setLabelSeparator(":");
		lblExcessVmAmount.setWidth(x+30);
		excessInventoryhp1.add(lblExcessVmAmount);
		LabelField excessInventoryVMPctLabel = new LabelField("Excess Inventory CM %:");
		excessInventoryVMPctLabel.setWidth((x*2)+30);
		excessInventoryhp2.add(excessInventoryVMPctLabel);
		lblExcessCmPercentage.setText(NumberFormat.getFormat(("###,###,##0.00")).format(0.00));
		lblExcessCmPercentage.setStyleName("text-element-no-underline");
		lblExcessCmPercentage.setLabelSeparator(":");
		lblExcessCmPercentage.setWidth(x+30);
		excessInventoryhp2.add(lblExcessCmPercentage);
		excessInventoryhp1.setSpacing(2);
		excessInventoryhp2.setSpacing(2);
		excessInventoryPanel.add(excessInventoryhp1);
		excessInventoryPanel.add(excessInventoryhp2);
		}catch(Exception e){
			System.out.println("====DisplayCostDataGroup:createExcessInventoryGroup error " + e.toString());
		}
		System.out.println("DisplayCostDataGroup:createExcessInventoryGroup ends");
		return excessInventoryPanel;
	}
	
	public ContentPanel createFinancialAnalysisGroup(Long displayScenarioId, boolean minimized){
		financialAnalysisPanel = new ContentPanel();
		System.out.println("DisplayCostDataGroup:createFinancialAnalysisGroup starts");
		try{
			int x=70;
			setStyleAttribute("paddingLeft","2px");
			financialAnalysisPanel.setBodyBorder(false);
			financialAnalysisPanel.setHeaderVisible(false);
			financialAnalysisPanel.setLayout(new RowLayout());
			/*
			 * financialAnalysisPanel.setSize(274, 327);
			 */
			financialAnalysisPanel.setSize(274, 337);
			HorizontalPanel financialAnalysishp1 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp2 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp3 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp4 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp5 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp6 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp7 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp8 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp9 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp10 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp11 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp12 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp13 = new HorizontalPanel();
			HorizontalPanel financialAnalysishp14 = new HorizontalPanel();
		
			LabelField corrugateCostsLabel = new LabelField("Corrugate Costs:");
			corrugateCostsLabel.setWidth((x*2)+30);
			financialAnalysishp1.add(corrugateCostsLabel);
			lblCorrugateCosts.setText(NumberFormat.getFormat((currencyFormat)).format(0.00));
			lblCorrugateCosts.setStyleName("text-element-no-underline");
			lblCorrugateCosts.setLabelSeparator(":");
			lblCorrugateCosts.setWidth(x+30);
			financialAnalysishp1.add(lblCorrugateCosts);
			LabelField fullfillmentCostsLabel = new LabelField("Fulfillment Costs:");
			fullfillmentCostsLabel.setWidth((x*2)+30);
			financialAnalysishp2.add(fullfillmentCostsLabel);
			lblFullfillmentCosts.setText(NumberFormat.getFormat((currencyFormat)).format(0.00));
			lblFullfillmentCosts.setStyleName("text-element-no-underline");
			lblFullfillmentCosts.setLabelSeparator(":");
			lblFullfillmentCosts.setFieldLabel("Fulfillment Costs");
			lblFullfillmentCosts.setWidth(x+30);
			financialAnalysishp2.add(lblFullfillmentCosts);
			LabelField otherCostsLabel = new LabelField("Other Costs:");
			otherCostsLabel.setWidth((x*2)+30);
			financialAnalysishp3.add(otherCostsLabel);
			lblOtherCosts.setText("$0.00");
			lblOtherCosts.setLabelSeparator(":");
			lblOtherCosts.setStyleName("text-element-underline");
			lblOtherCosts.setFieldLabel("Other Costs");
			lblOtherCosts.setWidth(x+30);
			financialAnalysishp3.add(lblOtherCosts);
			LabelField totalDisplayCostsLabel = new LabelField("Total Display Costs:");
			totalDisplayCostsLabel.setWidth((x*2)+30);;
			financialAnalysishp4.add(totalDisplayCostsLabel);
			lblTotalDisplayCosts.setText("$0.00");
			lblTotalDisplayCosts.setLabelSeparator(":");
			lblTotalDisplayCosts.setStyleName("text-element-no-underline");
			lblTotalDisplayCosts.setFieldLabel("Total Display Costs");
			lblTotalDisplayCosts.setWidth(x+30);
			financialAnalysishp4.add(lblTotalDisplayCosts);
			
			// R.A. editable trade sales 10.21.2016
			LabelField tradeSalesLabel = new LabelField("Trade Sales:");
			//tradeSalesLabel.setWidth((x*2)+30);
			tradeSalesLabel.setWidth(70);
			financialAnalysishp5.add(tradeSalesLabel);
			
			tradeSalesChanged.addInputStyleName("text-element-no-underline");
			tradeSalesChanged.setPropertyEditorType(Double.class);
			tradeSalesChanged.setMaxLength(9);
		
			tradeSalesChanged.setWidth(x+18);
			tradeSalesChanged.setReadOnly(minimized?true:false);
			tradeSalesChanged.setAllowDecimals(true);
			tradeSalesChanged.setAllowBlank(false);
			tradeSalesChanged.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double tradeSalesChangedVal = (Double) (tradeSalesChanged.getValue()==null?0:tradeSalesChanged.getValue().doubleValue());
					if(tradeSalesChangedVal >= currencyMaxValueForTradeSales){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
					String strTradeSalesChanged=tradeSalesChanged.getValue().toString();
					if(tradeSalesChanged.getValue() != null){
                         int index = strTradeSalesChanged.indexOf('.');
                       if(index!=-1 && strTradeSalesChanged.length() > (index +2)){
                      	 if (KeyUtil.isDigit(keyCode)){
                      		ke.stopEvent();
   						}
                       }
					}
				}
			});
			tradeSalesChanged.setMaxLength(11);
			tradeSalesChanged.setTabIndex(15);
			
			financialAnalysishp5.add(tradeSalesChanged);
			
			lblTradeSales.setText("$0.00");
			lblTradeSales.setLabelSeparator(":");
			lblTradeSales.setStyleName("text-element-no-underline");
			lblTradeSales.setFieldLabel("Trade Sales");
			lblTradeSales.setWidth(x+40);
			financialAnalysishp5.add(lblTradeSales);
			/*
			 * Date: 07-30-2014
			 * By: Mari
			 * Description : Change Cust Program Disc into Trade Program Disc
			 * LabelField custProgramDiscLabel = new LabelField("Cust Program Disc:");
			 */
			LabelField custProgramDiscLabel = new LabelField("Trade Program Disc:");
			custProgramDiscLabel.setWidth((x*2)+30);
			financialAnalysishp6.add(custProgramDiscLabel);
			lblCustProgramDisc.setStyleName("text-element-underline");
			lblCustProgramDisc.setText(NumberFormat.getFormat((currencyFormat)).format(0.00));
			lblCustProgramDisc.setWidth(x+30);
			lblCustProgramDisc.setLabelSeparator(":");
			lblCustProgramDisc.setFieldLabel("Trade Program Disc");
			financialAnalysishp6.add(lblCustProgramDisc);
			/*
			 * Add 2 new fields
			 * 1. MDF %
			 * 2. Discount %
			 * 3. $ MDF
			 * 4. $ Discount
			 * By: Mari
			 * Date: 07/30/2014 
			 */
				
			mdfPctLabel = new LabelField("MDF %:");
			//mdfPctLabel.setWidth((x*2)+50);
			mdfPctLabel.setWidth(70);
			financialAnalysishp13.add(mdfPctLabel);
			mdfPct.addInputStyleName("text-element-no-underline");
			mdfPct.setPropertyEditorType(Double.class);
			mdfPct.setMaxLength(7);
			mdfPct.setWidth(x+18);
			mdfPct.setReadOnly(minimized?true:false);
			mdfPct.setAllowDecimals(true);
			mdfPct.setAllowBlank(false);
			mdfPct.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double dmdfPctVal = (Double) (mdfPct.getValue()==null?0:mdfPct.getValue().doubleValue());
					if(dmdfPctVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
					String strMdfPctVal=mdfPct.getValue().toString();
					if(mdfPct.getValue() != null){
                         int index = strMdfPctVal.indexOf('.');
                       if(index!=-1 && strMdfPctVal.length() > (index +2)){
                      	 if (KeyUtil.isDigit(keyCode)){
                      		ke.stopEvent();
   						}
                       }
					}
				}
			});
			mdfPct.setMaxLength(11);
			mdfPct.setTabIndex(15);
			
			financialAnalysishp13.add(mdfPct);
			
			lblMdfDollor.setText("$0.00");
			lblMdfDollor.setLabelSeparator(":");
			lblMdfDollor.setStyleName("text-element-no-underline");
			lblMdfDollor.setFieldLabel("MDF $");
			lblMdfDollor.setWidth(x+40);
			financialAnalysishp13.add(lblMdfDollor);
			
			discountPctLabel = new LabelField("Discount %:");
			//discountPctLabel.setWidth((x*2)+50);
			discountPctLabel.setWidth(70);
			financialAnalysishp14.add(discountPctLabel);
			discountPct.addInputStyleName("text-element-no-underline");
			discountPct.setPropertyEditorType(Double.class);
			discountPct.setMaxLength(7);
			discountPct.setWidth(x+18);
			discountPct.setReadOnly(minimized?true:false);
			discountPct.setAllowDecimals(true);
			discountPct.setAllowBlank(false);
			discountPct.addListener(Events.KeyDown, new Listener<FieldEvent>() {
				public void handleEvent(FieldEvent ke) {
					int keyCode = ke.getKeyCode();
					Double discountPctVal = (Double) (discountPct.getValue()==null?0:discountPct.getValue().doubleValue());
					if(discountPctVal >= currencyMaxValue){
						if (KeyUtil.isDigit(keyCode)){
							ke.stopEvent();
						}
					}
					String strDiscountPctVal=discountPct.getValue().toString();
					 if(discountPct.getValue() != null){
                          int index = strDiscountPctVal.indexOf('.');
                         if(index!=-1 && strDiscountPctVal.length() > (index+2 )){
                        	 if (KeyUtil.isDigit(keyCode)){
     							ke.stopEvent();
     						}
                         }
                         
					 }       
					
					
				}
			});
			discountPct.setMaxLength(11);
			discountPct.setTabIndex(16);
			financialAnalysishp14.add(discountPct);
			
			lblDiscountDollor.setText("$0.00");
			lblDiscountDollor.setLabelSeparator(":");
			lblDiscountDollor.setStyleName("text-element-no-underline");
			lblDiscountDollor.setFieldLabel("Discount $");
			lblDiscountDollor.setWidth(x+40);
			financialAnalysishp14.add(lblDiscountDollor);
			
			
			LabelField netSalesLabel = new LabelField("Net Sales:");
			netSalesLabel.setWidth((x*2)+30);
			financialAnalysishp7.add(netSalesLabel);
			lblNetSales.setText(NumberFormat.getFormat((currencyFormat)).format(0.00));
			lblNetSales.setLabelSeparator(":");
			lblNetSales.setStyleName("text-element-no-underline");
			lblNetSales.setFieldLabel("Net Sales");
			lblNetSales.setWidth(x+30);
			financialAnalysishp7.add(lblNetSales);
			/*
			 * LabelField cOGSLabel = new LabelField("(COGS):");
			 * 
			 */
			LabelField cOGSLabel = new LabelField("(COGS(Material/Labor)):");
			cOGSLabel.setWidth((x*2)+30);
			financialAnalysishp8.add(cOGSLabel);
			lblCOGS.setText(NumberFormat.getFormat((currencyFormat)).format(0.00));
			lblCOGS.setLabelSeparator(":");
			lblCOGS.setFieldLabel("(COGS(Material/Labor))");
			lblCOGS.setStyleName("text-element-underline");
			lblCOGS.setWidth(x+30);
			financialAnalysishp8.add(lblCOGS);
			/*
			 * Date: 07/30/2014
			 * By: Mari
			 * Description: Change  $ Product Var Margin: into $ Product Contribution Margin
			 * LabelField productVarMarginLabel = new LabelField("$ Product Var Margin:");
			 */
			LabelField productVarMarginLabel = new LabelField("$ Product Contribution Margin:");
			productVarMarginLabel.setWidth((x*2)+30);
			financialAnalysishp9.add(productVarMarginLabel);
			lblProductVarMargin.setText(NumberFormat.getFormat((currencyFormat)).format(0.00));
			lblProductVarMargin.setLabelSeparator(":");
			lblProductVarMargin.setStyleName("text-element-no-underline");
			/*
			 * lblProductVarMargin.setFieldLabel("$ Product Var Margin");
			 */
			lblProductVarMargin.setFieldLabel("$ Product Contribution Margin");
			lblProductVarMargin.setWidth(x+30);
			financialAnalysishp9.add(lblProductVarMargin);
			LabelField displayCostsLabel = new LabelField("Display Costs:");
			displayCostsLabel.setWidth((x*2)+15);
			financialAnalysishp10.add(displayCostsLabel);
			lblDisplayCosts.setText(NumberFormat.getFormat((currencyFormat)).format(0.00));
			lblDisplayCosts.setLabelSeparator(":");
			lblDisplayCosts.setStyleName("text-element-underline");
			lblDisplayCosts.setFieldLabel("Display Costs");
			lblDisplayCosts.setWidth(x+45);
			financialAnalysishp10.add(lblDisplayCosts);
			/*
			 * LabelField vmAmountLabel = new LabelField("VM $:");
			 */
			LabelField vmAmountLabel = new LabelField("CM $:");
			vmAmountLabel.setWidth((x*2)+15);
			financialAnalysishp11.add(vmAmountLabel);
			lblVmAmount.setText(NumberFormat.getFormat((currencyFormat)).format(0.00));
			lblVmAmount.setLabelSeparator(":");
			lblVmAmount.setStyleName("text-element-no-underline");
			/*
			 * lblVmAmount.setFieldLabel("VM $");
			 */
			lblVmAmount.setFieldLabel("CM $");
			lblVmAmount.setWidth(x+45);
			financialAnalysishp11.add(lblVmAmount);
			/*
			 * LabelField vmPercentageLabel = new LabelField("VM %:");
			 */
			LabelField vmPercentageLabel = new LabelField("CM %:");
			vmPercentageLabel.setWidth((x*2)+15);
			financialAnalysishp12.add(vmPercentageLabel);
			lblCmPercentage.setText(NumberFormat.getFormat((currencyFormat)).format(0.00));
			lblCmPercentage.setLabelSeparator(":");
			lblCmPercentage.setStyleName("text-element-no-underline");
			/*
			 * lblCmPercentage.setFieldLabel("VM %");
			 */
			lblCmPercentage.setFieldLabel("CM %");
			lblCmPercentage.setWidth(x+45);
			financialAnalysishp12.add(lblCmPercentage);
			
			hurdlesMetLabel.setWidth((x*2)+30);
			lblHurdlesMet.setLabelSeparator(":");
			lblHurdlesMet.setFieldLabel("Margin and $ Hurdles Met");
			lblHurdlesMet.setWidth(x+30);
		
			LabelField spaceLabel2 = new LabelField();
			spaceLabel2.setLabelSeparator(" ");
			spaceLabel2.setFieldLabel(" ");
			spaceLabel2.setWidth(x+30);
			LabelField spaceLabel3 = new LabelField();
			spaceLabel3.setLabelSeparator(" ");
			spaceLabel3.setFieldLabel(" ");
			spaceLabel3.setWidth(x+30);
			LabelField spaceLabel4 = new LabelField();
			spaceLabel4.setLabelSeparator(" ");
			spaceLabel4.setFieldLabel(" ");
			spaceLabel4.setWidth(x+30);
			financialAnalysishp1.setSpacing(2);
			financialAnalysishp2.setSpacing(2);
			financialAnalysishp3.setSpacing(2);
			financialAnalysishp4.setSpacing(2);
			financialAnalysishp5.setSpacing(2);
			financialAnalysishp6.setSpacing(2);
			financialAnalysishp7.setSpacing(2);
			financialAnalysishp8.setSpacing(2);
			financialAnalysishp9.setSpacing(2);
			financialAnalysishp10.setSpacing(2);
			financialAnalysishp11.setSpacing(2);
			financialAnalysishp12.setSpacing(2);
			financialAnalysishp13.setSpacing(2);
			financialAnalysishp14.setSpacing(2);
		
			financialAnalysisPanel.add(financialAnalysishp1);
			financialAnalysisPanel.add(financialAnalysishp2);
			financialAnalysisPanel.add(financialAnalysishp3);
			financialAnalysisPanel.add(financialAnalysishp4);
			financialAnalysisPanel.add(financialAnalysishp5);
			financialAnalysisPanel.add(financialAnalysishp6);
			financialAnalysisPanel.add(financialAnalysishp13);
			financialAnalysisPanel.add(financialAnalysishp14);
			financialAnalysisPanel.add(financialAnalysishp7);
			financialAnalysisPanel.add(financialAnalysishp8);
			financialAnalysisPanel.add(financialAnalysishp9);
			financialAnalysisPanel.add(financialAnalysishp10);
			financialAnalysisPanel.add(financialAnalysishp11);
			financialAnalysisPanel.add(financialAnalysishp12);
		
		
		}catch(Exception e){
			System.out.println("====DisplayCostDataGroup:calculateFinancialDataFields error " + e.toString());
		}
		System.out.println("DisplayCostDataGroup:createFinancialAnalysisGroup ends");
		return financialAnalysisPanel;
	}
	
	public Double totalActualInvoiceTotCostVal;
	public Double totalActualInvoiceTotCostValWPrgrm;
	public Double totalCogsVal;
	public Double totalDisplayCostsVal;
	public Double overageScrapNoVal;
	public Double totalDisplaysOrderedVal;
	public Double corrugateCostPerUnitVal;
	public Double fullfillmentCostPerUnitVal;
	public Double corrugateCostVal;
	public Double fullfillmentCostVal;
	public Double miscCostVal ;
	public Double palletCostVal;
	public Double shipTestCostVal;
	public Double totalArtworkCostVal;
	public Double printingPlateCostVal;
	public Double dieCuttingCostVal;
	public Double cTPCosttVal ;
	public Double otherCostsVal;
	public Double tradeSalesVal;
	public Double customerProgramPercentageVal;
	public Double mdfPctVal;
	public Double discountPctVal;
	public Double custProgramDiscVal;
	public Double netSalesVal;
	public Double netSalesValAp ;
	public Double cOGSVal;
	public Double productVarMarginVal;
	public Double displayCostsVal;
	public Double vmAmountVal;
	public Double vmPercentageVal;
	public Double excessVmAmountVal;
	public Double excessVmPercentageVal;
	public Double sumVmDollar;
	public Double sumVmPct;
	public Double excessSumInvtDollar;
	public Double excessSumInvtPct;
	public Long totalNumberFacing;
	public Long totalQtyPerDisplay;
	public Long totalQtyPerFacing;
	public Double dforeCastQtyVal ;
	public Double dactualQtyVal;
	public Double sumVmDollarBp;
	public Double sumVmPctBp;

	public void calculateFinancialDataFields(){
		//try{
			//System.out.println("DisplayCostDataGroup:calculateFinancialDataFields starts");
			overageScrapNoVal = (Double) (overageScrapNo.getValue()==null?0:overageScrapNo.getValue().doubleValue());
			dforeCastQtyVal = (Double) (foreCastQty.getValue()==null?0:foreCastQty.getValue().doubleValue());
			dactualQtyVal = (Double) (actualQty.getValue()==null?0:actualQty.getValue().doubleValue());
			double pctVal=0.00;
			if( (dforeCastQtyVal!=null && dforeCastQtyVal>0) && 
					(overageScrapNoVal!=null && overageScrapNoVal>0)){
				pctVal=((overageScrapNoVal/dforeCastQtyVal) * percentMaxValue );
			}
			overageScrapPct.setValue(Double.parseDouble(pctFormat.format(pctVal)));
			/*
			 * Date: 01/03/2013
			 * Description : Modified the formula as per the change request from the client
			 * use actual quantity instead of forecastquantity
			 * totalDisplaysOrdered.setValue((int) (overageScrapNoVal+dforeCastQtyVal));
			 */
			totalDisplaysOrdered.setValue((int) (overageScrapNoVal+dactualQtyVal));
			totalDisplaysOrderedVal = (Double) (totalDisplaysOrdered.getValue()==null?0:totalDisplaysOrdered.getValue().doubleValue());
			corrugateCostPerUnitVal = (Double) (corrugateCostPerUnit.getValue()==null?0:corrugateCostPerUnit.getValue().doubleValue());
			fullfillmentCostPerUnitVal = (Double) (fullfillmentCostPerUnit.getValue()==null?0:fullfillmentCostPerUnit.getValue().doubleValue());
			corrugateCostVal=(corrugateCostPerUnitVal * totalDisplaysOrderedVal );
			fullfillmentCostVal=(fullfillmentCostPerUnitVal * dactualQtyVal );
			lblCorrugateCosts.setValue(NumberFormat.getFormat((currencyFormat)).format(corrugateCostVal));
			lblFullfillmentCosts.setValue(NumberFormat.getFormat((currencyFormat)).format(fullfillmentCostVal));
			miscCostVal = (Double) (miscCost.getValue()==null?0:miscCost.getValue().doubleValue());
			palletCostVal = (Double) (palletCost.getValue()==null?0:palletCost.getValue().doubleValue());
			shipTestCostVal = (Double) (shipTestCost.getValue()==null?0:shipTestCost.getValue().doubleValue());
			totalArtworkCostVal = (Double) (totalArtworkCost.getValue()==null?0:totalArtworkCost.getValue().doubleValue());
			printingPlateCostVal = (Double) (printingPlateCost.getValue()==null?0:printingPlateCost.getValue().doubleValue());
			dieCuttingCostVal = (Double) (dieCuttingCost.getValue()==null?0:dieCuttingCost.getValue().doubleValue());
			cTPCosttVal = (Double) (cTPCost.getValue()==null?0:cTPCost.getValue().doubleValue());
			otherCostsVal  = cTPCosttVal + dieCuttingCostVal + printingPlateCostVal + totalArtworkCostVal + shipTestCostVal + ( palletCostVal * totalDisplaysOrderedVal )+miscCostVal;
			lblOtherCosts.setValue(NumberFormat.getFormat((currencyFormat)).format(otherCostsVal));
			totalDisplayCostsVal=corrugateCostVal+fullfillmentCostVal+otherCostsVal;
			lblTotalDisplayCosts.setValue(NumberFormat.getFormat((currencyFormat)).format(totalDisplayCostsVal));
			totalActualInvoiceTotCostVal=calcTotalActualInvoice();
			totalActualInvoiceTotCostValWPrgrm=calcTotalActualInvoiceWithProgram();
			totalCogsVal=calcTotalCogsVal();
			sumVmDollar=calcSumVmDollar(false);
			sumVmPct=calcSumVmPct(false);
			sumVmDollarBp=calcSumVmDollar(true);
			sumVmPctBp=calcSumVmPct(true);
			excessSumInvtDollar=calcExcessSumVmDollar();
			excessSumInvtPct=calcExcessSumVmPct();

			// display_cost may have trade sales value
			if ((tradeSalesChanged.getValue().doubleValue()) > 0) {
				tradeSalesVal = tradeSalesChanged.getValue().doubleValue();
			} else {
				tradeSalesVal=(totalActualInvoiceTotCostVal * dactualQtyVal );
			}
			lblTradeSales.setValue(NumberFormat.getFormat((currencyFormat)).format(tradeSalesVal));
			 
			custProgramDiscVal=(tradeSalesVal-(totalActualInvoiceTotCostValWPrgrm * dactualQtyVal) );
			//custProgramDiscVal=(totalActualInvoiceTotCostValWPrgrm * dactualQtyVal) ;
			lblCustProgramDisc.setValue(NumberFormat.getFormat((currencyFormat)).format(custProgramDiscVal));
			//netSalesVal=(tradeSalesVal + custProgramDiscVal);
			/*
			 * By Mari
			 * Date 08/05/2014
			 * New Formula for Net Sales fields
			 * Net Sales = Trade Sales - Trade Program Discount - (Trade Sales * MDF%) - (Trade Sales * Discount%)
			 * productVarMarginVal has been changed into Customer Variable Margin
			 */
			mdfPctVal = (Double) (mdfPct.getValue()==null?0:(mdfPct.getValue().doubleValue() ) /  percentMaxValue);
			discountPctVal = (Double) (discountPct.getValue()==null?0:(discountPct.getValue().doubleValue() /  percentMaxValue));
			netSalesVal=(tradeSalesVal - (custProgramDiscVal + (tradeSalesVal*mdfPctVal)+(tradeSalesVal*discountPctVal)));
			lblMdfDollor.setValue(NumberFormat.getFormat((currencyFormat)).format(tradeSalesVal*mdfPctVal));
			lblDiscountDollor.setValue(NumberFormat.getFormat((currencyFormat)).format(tradeSalesVal*discountPctVal));
			lblNetSales.setValue(NumberFormat.getFormat((currencyFormat)).format(netSalesVal));
			cOGSVal=(totalCogsVal*dactualQtyVal) * -1;
			lblCOGS.setValue(NumberFormat.getFormat((currencyFormat)).format(cOGSVal));
			productVarMarginVal=netSalesVal+cOGSVal;
			lblProductVarMargin.setValue(NumberFormat.getFormat((currencyFormat)).format(productVarMarginVal));
			displayCostsVal=totalDisplayCostsVal * -1;
			lblDisplayCosts.setValue(NumberFormat.getFormat((currencyFormat)).format(displayCostsVal));
			vmAmountVal=0.00;
			vmAmountVal=productVarMarginVal +  displayCostsVal;
			excessVmAmountVal=0.00;
			excessVmAmountVal=vmAmountVal-excessSumInvtDollar;
			lblVmAmount.setValue(NumberFormat.getFormat((currencyFormat)).format(vmAmountVal));
			lblExcessVmAmount.setValue(NumberFormat.getFormat((currencyFormat)).format(excessVmAmountVal));
			String vmMarginalAmount = AppService.App.getDmmConstants().vmAmount();
			String vmMarginalPct = AppService.App.getDmmConstants().vmPct();
			lblVmAmount.removeInputStyleName("vmColor-red");
			lblVmAmount.removeInputStyleName("vmColor-yellow");
			lblVmAmount.removeInputStyleName("vmColor-green");
			lblCmPercentage.removeInputStyleName("vmColor-red");
			lblCmPercentage.removeInputStyleName("vmColor-yellow");
			lblCmPercentage.removeInputStyleName("vmColor-green");
			if(vmAmountVal<new Double(vmMarginalAmount)){
				lblVmAmount.addInputStyleName("vmColor-red");
			}else{
				lblVmAmount.addInputStyleName("vmColor-green");
			}
			vmPercentageVal=0.00;
			excessVmPercentageVal=0.00;
			
			if(vmAmountVal!=0 && netSalesVal!=0){
				vmPercentageVal=((vmAmountVal /  netSalesVal) * percentMaxValue );
			}
			if(vmPercentageVal!=0 && excessSumInvtPct!=0  ){
				excessVmPercentageVal=((excessVmAmountVal /  netSalesVal) * percentMaxValue );
			}
			if(vmPercentageVal<new Double(vmMarginalPct)){
				lblCmPercentage.addInputStyleName("vmColor-red");
			}else{
				lblCmPercentage.addInputStyleName("vmColor-green");
			}
			lblCmPercentage.setValue(pctFormat.format(vmPercentageVal));
			lblExcessCmPercentage.setValue(pctFormat.format(excessVmPercentageVal));
	}
	
	public void updateCustomProgramDiscount(Double dSumActualInvoiceWithPrgm, boolean bSummaryCalc, int selectedIdx){
		/*
		 * Date : 09/27/2012
		 * Added by Mari
		 * Update the Summary table when financial calculation finished.
		 */
		DisplayScenarioDTO loadedDisplayScenarioDTO=getCostAnalysisInfo().displayScenarioDTOList.get(selectedIdx);
		custProgramDiscVal=(tradeSalesVal-(dSumActualInvoiceWithPrgm * getCostAnalysisInfo().displayScenarioDTOList.get(selectedIdx).getActualQty()));
		netSalesValAp=(tradeSalesVal - (custProgramDiscVal + (tradeSalesVal*mdfPctVal)+(tradeSalesVal*discountPctVal)));
		lblMdfDollor.setValue(NumberFormat.getFormat((currencyFormat)).format(tradeSalesVal*mdfPctVal));
		lblDiscountDollor.setValue(NumberFormat.getFormat((currencyFormat)).format(tradeSalesVal*discountPctVal));
		lblNetSales.setValue(NumberFormat.getFormat((currencyFormat)).format(netSalesValAp));
		lblCustProgramDisc.setValue(NumberFormat.getFormat((currencyFormat)).format(custProgramDiscVal));
		if(bSummaryCalc && loadedDisplayScenarioDTO.getScenarioApprovalFlg()!=null && loadedDisplayScenarioDTO.getScenarioApprovalFlg().equalsIgnoreCase(CostAnalysisInfo.YES_FLG)){
			getCostAnalysisInfo().saveSummaryCalculation(getCostAnalysisInfo().displayCostDataGroupList, loadedDisplayScenarioDTO, selectedIdx);
		}else{
			System.out.println("updateCustomProgramDiscount:update summaryCalculation ApprovalFlag NO");
		}
		/*
		 * Ended
		 */
	}
	
	Long foreCastQtyVal;
	Long actualQtyVal;
	public void loadCostAnalysisComponentInfo(DisplayScenarioDTO displayScenarioDTO, boolean comparisonView){
		System.out.println("DisplayCostDataGroup:loadCostAnalysisComponentInfo starts");
		try{
		foreCastQtyVal = (Long) (displayScenarioDTO.getQtyForecast()==null?0:displayScenarioDTO.getQtyForecast().longValue());
		actualQtyVal = (Long) (displayScenarioDTO.getActualQty()==null?0:displayScenarioDTO.getActualQty().longValue());
		customerProgramPercentageVal = (Double) (customerProgramPercentage.getValue()==null?0:customerProgramPercentage.getValue().doubleValue());
		AppService.App.getInstance().getCostAnalysisKitComponentInfo(displayScenarioDTO,customerProgramPercentageVal,
				new Service.LoadCostAnalysisKitComponentInfo(this, costAnalysisKitComponentStore,null,displayScenarioDTO,comparisonView));
		}catch(Exception ex){
			System.out.println("====DisplayCostDataGroup:loadCostAnalysisComponentInfo error "+ ex.toString());
		}
		System.out.println("DisplayCostDataGroup:loadCostAnalysisComponentInfo ends");
	}
	public static final int PIM_COL_INDEX=5;
	public static final int PV_COL_INDEX=6;
	public static final int I2_COL_INDEX=7;
	public static final int PBL_COL_INDEX=8;
	public Grid<CostAnalysisKitComponentDTO> componentGrid;
	AggregationRowConfig<CostAnalysisKitComponentDTO> summary = new AggregationRowConfig<CostAnalysisKitComponentDTO>();
	ListStore<CostAnalysisKitComponentDTO> costAnalysisKitComponentStore = new ListStore<CostAnalysisKitComponentDTO>();
	ColumnModel cm ;
	
	public ContentPanel createCostAnalysisInfoGrid(DisplayDTO display, DisplayScenarioDTO displayScenarioDTO, boolean comparisonView){
		ContentPanel cp = new ContentPanel();
	try{
		this.displayTabs = getCostAnalysisInfo().getDisplayTabs();
		Boolean readOnly = getCostAnalysisInfo().getData("readOnly");
	    if (readOnly == null) readOnly = false;
		int x = 100;
		final NumberFormat number = NumberFormat.getFormat("0.00");
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
		setupSelectionDialog();
		ColumnConfig column = new ColumnConfig();
		TextField<String> skuValue = new TextField<String>(); 
		CellEditor skuExceptionEditor =  new CellEditor(skuValue); 
		skuValue.disable();
		skuExceptionEditor.disable();
		column.setId(CostAnalysisKitComponentDTO.SKU);
		column.setEditor(skuExceptionEditor);
		column.setHeader("<center>SKU</center>");
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setWidth(x+15);
		configs.add(column);
		column = new ColumnConfig();
		TextField<String> chkDgtValue = new TextField<String>(); 
		CellEditor chkDgtEditor =  new CellEditor(chkDgtValue); 
		chkDgtValue.disable();
		chkDgtEditor.disable();
		column.setEditor(chkDgtEditor);
		column.setId(CostAnalysisKitComponentDTO.CHKDGT);
		column.setHeader("<center>Check<br>Digit</center>");
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setWidth(x/2);
		configs.add(column);
		column = new ColumnConfig();
		TextField<String> buDescValue = new TextField<String>(); 
		CellEditor buDescEditor =  new CellEditor(buDescValue); 
		buDescValue.disable();
		buDescEditor.disable();
		column.setEditor(buDescEditor);
		column.setId(CostAnalysisKitComponentDTO.BU_DESC);
		column.setHeader("<center>Business<br>Unit</center>");
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setWidth(x+50);
		configs.add(column);
		column = new ColumnConfig();
		TextField<String> prodNameValue = new TextField<String>(); 
		CellEditor prodNameEditor =  new CellEditor(prodNameValue); 
		prodNameValue.disable();
		prodNameEditor.disable();
		column.setEditor(prodNameEditor);
		column.setId(CostAnalysisKitComponentDTO.PRODUCT_NAME);
		column.setHeader("<center>Description</center>");
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setWidth((x*2)-3);
		configs.add(column);
		column = new ColumnConfig();
		final NumberField regCasePackValue = new NumberField();
		regCasePackValue.setPropertyEditorType(Long.class);
		CellEditor regCasePackEditor =  new CellEditor(regCasePackValue); 
		regCasePackValue.disable();
		regCasePackEditor.disable();
		column.setEditor(regCasePackEditor);
		column.setId(CostAnalysisKitComponentDTO.REG_CASE_PACK);
		column.setHeader("<center>Retail<br>Packs Per<br>Inner Carton</center>");
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setWidth(x -30);
		configs.add(column);
		CheckColumnConfig pimCol = new CheckColumnConfig(CostAnalysisKitComponentDTO.SOURCE_PIM_FLG, "PIM", 55){
			@Override
			protected String getCheckState(ModelData model, String property, int rowIndex, int colIndex)
			{
				Boolean value = model.get(property);
				StringBuilder sb = new StringBuilder();
				if (value != null && value) {
					sb.append("-on");
				}else if(value==null ){
					sb.append("-on");
				}else{
					sb.append("-off");
				}
				return sb.toString();
			}
		};
		CheckBox pimValue = new CheckBox(); 
		final CellEditor checkBoxPimEditor = new CellEditor(pimValue);
		pimValue.disable();
		checkBoxPimEditor.disable();
		pimCol.setEditor(checkBoxPimEditor);
		checkBoxPimEditor.disable();
		checkBoxPimEditor.cancelEdit();
		pimCol.addListener(Events.CellDoubleClick, new Listener<FieldEvent>() {
			public void handleEvent(FieldEvent be) {
				be.setCancelled(true);
				checkBoxPimEditor.disable();
			}
		});
		pimCol.setWidth(x / 3);
		pimCol.setAlignment(HorizontalAlignment.LEFT);
		configs.add(pimCol);
		CheckColumnConfig pvCol = new CheckColumnConfig(CostAnalysisKitComponentDTO.PROMO_FLG, "PV", 34){
			@Override
			protected String getCheckState(ModelData model, String property, int rowIndex, int colIndex)
			{
				Boolean value = model.get(property);
				StringBuilder sb = new StringBuilder();
				if (value != null && value) {
					sb.append("-on");
				}else if(value==null ){
					sb.append("-on");
				}else{
					sb.append("-off");
				}
				return sb.toString();
			}
		};
		CheckBox promoValue = new CheckBox(); 
		final CellEditor checkBoxPromoditor = new CellEditor(promoValue);
		promoValue.disable();
		checkBoxPromoditor.disable();
		pvCol.setEditor(checkBoxPromoditor);
		pvCol.setAlignment(HorizontalAlignment.LEFT);
		pvCol.setWidth(x / 3);
		configs.add(pvCol);
		CheckColumnConfig i2Col = new CheckColumnConfig(CostAnalysisKitComponentDTO.I2_FORECAST_FLG, "I2", 34){
			@Override
			protected String getCheckState(ModelData model, String property, int rowIndex, int colIndex)
			{
				Boolean value = model.get(property);
				StringBuilder sb = new StringBuilder();
				if (value != null && value) {
					sb.append("-on");
				}else if(value==null ){
					sb.append("-on");
				}else{
					sb.append("-off");
				}
				return sb.toString();
			}
		};
		CellEditor checkBoxEditorI2 = new CellEditor(new CheckBox());
		checkBoxEditorI2.enable();
		i2Col.setEditor(checkBoxEditorI2);
		if(readOnly) {
			checkBoxEditorI2.disable();
			checkBoxEditorI2.disable();
		}
		i2Col.setWidth(x / 3);
		i2Col.setAlignment(HorizontalAlignment.LEFT);
		configs.add(i2Col);
		CheckColumnConfig pbcBOM = new CheckColumnConfig(CostAnalysisKitComponentDTO.PLANT_BREAK_CASE_FLG, "PBC<br>BOM", 34){
			@Override
			protected String getCheckState(ModelData model, String property, int rowIndex, int colIndex)
			{
				Boolean value = model.get(property);
				StringBuilder sb = new StringBuilder();
				if (value != null && value) {
					sb.append("-on");
				}else if(value==null ){
					sb.append("-on");
				}else{
					sb.append("-off");
				}
				return sb.toString();
			}
		};
		CheckBox plantBreakCaseValue = new CheckBox(); 
		final CellEditor checkBoxPlantBreakCaseditor = new CellEditor(plantBreakCaseValue);
		plantBreakCaseValue.disable();
		checkBoxPlantBreakCaseditor.disable();
		pbcBOM.setEditor(checkBoxPlantBreakCaseditor);
		pbcBOM.setWidth(x / 3);
		pbcBOM.setAlignment(HorizontalAlignment.LEFT);
		configs.add(pbcBOM);
		column = new ColumnConfig();
		TextField<String> pbcVersionValue = new TextField<String>(); 
		CellEditor pbcVersionValueEditor =  new CellEditor(pbcVersionValue); 
		pbcVersionValue.disable();
		pbcVersionValueEditor.disable();
		column.setEditor(pbcVersionValueEditor);
		column.setId(CostAnalysisKitComponentDTO.PBC_VERSION);
		column.setHeader("<center>PBC<br>Vers</center>");
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setWidth(x-30);
		configs.add(column);
		final NumberField nQtyPerFacing = new NumberField(); 
		nQtyPerFacing.setPropertyEditorType(Long.class);
		nQtyPerFacing.addInputStyleName("text-element-no-underline");	
		nQtyPerFacing.setMaxLength(12);
		final CellEditor qtyPerFacingEditor =  new CellEditor(nQtyPerFacing); 
		column = new ColumnConfig();
		column.setId(CostAnalysisKitComponentDTO.QTY_PER_FACING);
		column.setHeader("<center>Qty<br>Per<br>Facing</center>");
		column.setWidth(x / 2);
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setEditor(qtyPerFacingEditor);
		if(readOnly) {
			nQtyPerFacing.disable();
			qtyPerFacingEditor.disable();
		}
		configs.add(column);
		final NumberField nNoPerFacing = new NumberField(); 
		nNoPerFacing.setPropertyEditorType(Long.class);
		nNoPerFacing.addInputStyleName("text-element-no-underline");	
		nNoPerFacing.setMaxLength(12);
		final CellEditor noPerFacingEditor =  new CellEditor(nNoPerFacing); 
		column = new ColumnConfig();
		column.setId(CostAnalysisKitComponentDTO.NUMBER_FACINGS);
		column.setHeader("<center>#<br>of<br>Facings</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x / 2);
		column.setEditor(noPerFacingEditor);
		if(readOnly) {
			nNoPerFacing.disable();
			noPerFacingEditor.disable();
		}
		configs.add(column);
		column = new ColumnConfig();
		final NumberField qtyPerDisplayValue = new NumberField();
		qtyPerDisplayValue.addInputStyleName("text-element-no-underline");	
		qtyPerDisplayValue.setPropertyEditorType(Long.class);
		CellEditor qtyPerDisplayEditor =  new CellEditor(qtyPerDisplayValue); 
		qtyPerDisplayValue.disable();
		qtyPerDisplayEditor.disable();
		column.setEditor(qtyPerDisplayEditor);
		column.setId(CostAnalysisKitComponentDTO.QTY_PER_DISPLAY);
		column.setHeader("<center>Qty<br>Per<br>Display</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x / 2);
		configs.add(column);
		column = new ColumnConfig();
		final NumberField inventoryReqValue = new NumberField();
		inventoryReqValue.addInputStyleName("text-element-no-underline");	
		inventoryReqValue.setPropertyEditorType(Long.class);
		CellEditor inventoryReqEditor =  new CellEditor(inventoryReqValue); 
		inventoryReqValue.disable();
		inventoryReqEditor.disable();
		column.setEditor(inventoryReqEditor);
		column.setId(CostAnalysisKitComponentDTO.INVENTORY_REQUIREMENT);
		column.setHeader("<center>Inv<br>Req</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x / 2);
		configs.add(column);
		
		/*
		 * Description: Added three fields 
		 * 				i. excessInventoryDollar 
		 * 				ii.excessInventoryPercent, 
		 * 				iii.inPogYorN
		 * Date       : 07/11/2011
		 * By		  : Mari
		 * 
		 */
		
		// EXCELL_INV_DOLLAR NUMBER(12,2)
		final NumberField excessInventoryDollar = new NumberField();
		excessInventoryDollar.addInputStyleName("text-element-no-underline");	
		excessInventoryDollar.setPropertyEditorType(Double.class);
		CellEditor excessInventoryDollarEditor =  new CellEditor(excessInventoryDollar); 
		excessInventoryDollar.disable();
		excessInventoryDollarEditor.disable();
		column = new ColumnConfig();
		column.setEditor(excessInventoryDollarEditor);
		column.setId(CostAnalysisKitComponentDTO.EXCESS_INVT_DOLLAR);
		column.setHeader("<center>Excess<br>Inventory<br>$</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x / 2);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		// EXCELL_INV_PCT NUMBER(12,2)
		final NumberField excessInventoryPercent = new NumberField();
		excessInventoryPercent.addInputStyleName("text-element-no-underline");	
		excessInventoryPercent.setPropertyEditorType(Double.class);
		CellEditor excessInventoryPercentEditor =  new CellEditor(excessInventoryPercent); 
		if(readOnly) {
			excessInventoryPercent.disable();
			excessInventoryPercentEditor.disable();
		}
		column = new ColumnConfig();
		column.setEditor(excessInventoryPercentEditor);
		column.setId(CostAnalysisKitComponentDTO.EXCESS_INVT_PCT);
		column.setHeader("<center>Excess<br>Inventory<br>%</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x -35);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"0.00%":number.format(Double.parseDouble(model.get(property).toString()))+"%";
			}
		});
		configs.add(column);
		
		final NumberField estProgramPct = new NumberField();
		estProgramPct.addInputStyleName("text-element-no-underline");	
		estProgramPct.setPropertyEditorType(Double.class);
		CellEditor estProgramDiscEditor =  new CellEditor(estProgramPct); 
		estProgramPct.disable();
		estProgramDiscEditor.disable();
		column = new ColumnConfig();
		column.setEditor(estProgramDiscEditor);
		column.setId(CostAnalysisKitComponentDTO.ESTIMATED_PROGRAM_PCT);
		column.setHeader("<center>Est.<br>Program<br>%</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x -35);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"0.00%":number.format(Double.parseDouble(model.get(property).toString()))+"%";
			}
		});
		configs.add(column);
		
		
		final NumberField overridExceptionPercent = new NumberField();
		overridExceptionPercent.addInputStyleName("text-element-no-underline");	
		overridExceptionPercent.setPropertyEditorType(Double.class);
		CellEditor overridExceptionPercentEditor =  new CellEditor(overridExceptionPercent); 
		if(readOnly) {
			overridExceptionPercent.disable();
			overridExceptionPercentEditor.disable();
		}
		column = new ColumnConfig();
		column.setEditor(overridExceptionPercentEditor);
		column.setId(CostAnalysisKitComponentDTO.OVERRIDE_EXC_PCT);
		column.setHeader("<center>Override<br>Exception<br>%</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x -35);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"0.00%":number.format(Double.parseDouble(model.get(property).toString()))+"%";
			}
		});
		configs.add(column);
		
		CheckColumnConfig inPogYnCol = new CheckColumnConfig(CostAnalysisKitComponentDTO.INPOG_YN, "<center>In<br>POG<br>Y/N?</center>", 55){
			@Override
			protected String getCheckState(ModelData model, String property, int rowIndex, int colIndex)
			{
				Boolean value = model.get(property);
				StringBuilder sb = new StringBuilder();
				if (value != null && value) {
					sb.append("-on");
				}else if(value==null ){
					sb.append("-on");
				}else{
					sb.append("-off");
				}
				return sb.toString();
			}
		};
		CheckBox inPogYnValue = new CheckBox(); 
		final CellEditor checkBoxinPogYnEditor = new CellEditor(inPogYnValue);
		if(readOnly) {
			inPogYnValue.disable();
			checkBoxinPogYnEditor.disable();
		}
		inPogYnCol.setEditor(checkBoxinPogYnEditor);
		checkBoxinPogYnEditor.disable();
		
		checkBoxinPogYnEditor.cancelEdit();
		inPogYnCol.addListener(Events.CellDoubleClick, new Listener<FieldEvent>() {
			public void handleEvent(FieldEvent be) {
				be.setCancelled(true);
				checkBoxinPogYnEditor.disable();
			}
		});
		inPogYnCol.setWidth((x / 3)+3);
		inPogYnCol.setAlignment(HorizontalAlignment.LEFT);
		configs.add(inPogYnCol);
		
		final NumberField nPriceException = new NumberField(); 
		nPriceException.setPropertyEditorType(Double.class);
		nPriceException.addInputStyleName("text-element-no-underline");	
		nPriceException.setMaxLength(12);
		final CellEditor priceExceptionEditor =  new CellEditor(nPriceException); 
		column = new ColumnConfig();
		column.setId(CostAnalysisKitComponentDTO.PRICE_EXCEPTION);
		column.setHeader("<center>Price<br>Exception</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setEditor(priceExceptionEditor);
		if(readOnly) {
			nPriceException.disable();
			priceExceptionEditor.disable();
		}
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		column = new ColumnConfig();
		NumberField actualInvoicePerEaValue = new NumberField(); 
		actualInvoicePerEaValue.setPropertyEditorType(Double.class);
		actualInvoicePerEaValue.addInputStyleName("text-element-no-underline");	
		CellEditor actualInvoicePerEaEditor =  new CellEditor(actualInvoicePerEaValue); 
		actualInvoicePerEaValue.disable();
		actualInvoicePerEaEditor.disable();
		column.setEditor(actualInvoicePerEaEditor);
		column.setId(CostAnalysisKitComponentDTO.ACTUAL_INVOICE_PER_EA);
		column.setHeader("<center>Actual<br>Invoice<br>Per Ea.$</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		
		final NumberField invoiceWithProgram = new NumberField();
		invoiceWithProgram.addInputStyleName("text-element-no-underline");	
		invoiceWithProgram.setPropertyEditorType(Double.class);
		CellEditor invoiceWithProgramEditor =  new CellEditor(invoiceWithProgram); 
		invoiceWithProgram.disable();
		invoiceWithProgramEditor.disable();
		column = new ColumnConfig();
		column.setEditor(invoiceWithProgramEditor);
		column.setId(CostAnalysisKitComponentDTO.INVOICE_WITH_PROGRAM);
		column.setHeader("<center>Invoice<br>With<br>Program</center>");
		//column.setHeader("Invoice<br>With<br>Program");
		column.setAlignment(HorizontalAlignment.RIGHT);
		//column.setWidth(x / 2);
		column.setWidth(x );
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		
		column = new ColumnConfig();
		final  NumberField totActualInvoiceValue = new NumberField();
		totActualInvoiceValue.addInputStyleName("text-element-no-underline");	
		totActualInvoiceValue.setPropertyEditorType(Double.class);
		totActualInvoiceValue.setAllowDecimals(true);
		CellEditor totActualInvoiceEditor =  new CellEditor(totActualInvoiceValue); 
		totActualInvoiceValue.disable();
		totActualInvoiceEditor.disable();
		column.setEditor(totActualInvoiceEditor);
		column.setId(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE);
		column.setHeader("<center>Total<br>Actual<br>Invoice $</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		
		column = new ColumnConfig();
		final  NumberField totActualInvoiceWpgrmValue = new NumberField();
		totActualInvoiceWpgrmValue.addInputStyleName("text-element-no-underline");	
		totActualInvoiceWpgrmValue.setPropertyEditorType(Double.class);
		totActualInvoiceWpgrmValue.setAllowDecimals(true);
		CellEditor totActualInvoiceWpgrmEditor =  new CellEditor(totActualInvoiceWpgrmValue); 
		totActualInvoiceWpgrmValue.disable();
		totActualInvoiceWpgrmEditor.disable();
		column.setEditor(totActualInvoiceWpgrmEditor);
		column.setId(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE_WPGRM);
		column.setHeader("<center>Total Actual<br>Invoice $<br>With Program</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		
		column = new ColumnConfig();
		final NumberField cogsUnitPriceValue = new NumberField();
		cogsUnitPriceValue.setPropertyEditorType(Double.class);
		cogsUnitPriceValue.setAllowDecimals(true);
		cogsUnitPriceValue.addInputStyleName("text-element-no-underline");		
		CellEditor cogsUnitPriceEditor =  new CellEditor(cogsUnitPriceValue); 
		cogsUnitPriceValue.disable();
		cogsUnitPriceEditor.disable();
		column.setEditor(cogsUnitPriceEditor);
		column.setId(CostAnalysisKitComponentDTO.COGS_UNIT_PRICE);
		column.setHeader("<center>COGS<br>(Material/Labor)<br>Per Ea.$</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		column = new ColumnConfig();
		final NumberField totCogsUnitPriceValue = new NumberField();
		totCogsUnitPriceValue.setPropertyEditorType(Double.class);
		totCogsUnitPriceValue.setAllowDecimals(true);
		totCogsUnitPriceValue.addInputStyleName("text-element-no-underline");	
		CellEditor totCogsUnitPriceEditor =  new CellEditor(totCogsUnitPriceValue); 
		totCogsUnitPriceValue.disable();
		totCogsUnitPriceEditor.disable();
		column.setEditor(totCogsUnitPriceEditor);
		column.setId(CostAnalysisKitComponentDTO.TOTAL_COGS_UNIT_PRICE);
		column.setHeader("<center>Total<br>COGS$<br>(Material/Labor)</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		column = new ColumnConfig();
		final NumberField netVm$Value = new NumberField();
		netVm$Value .setPropertyEditorType(Double.class);
		netVm$Value.setAllowDecimals(true);
		netVm$Value.addInputStyleName("text-element-no-underline");	
		CellEditor netVm$Editor =  new CellEditor(netVm$Value); 
		netVm$Value.disable();
		netVm$Editor.disable();
		column.setEditor(netVm$Editor);
		column.setId(CostAnalysisKitComponentDTO.NET_VM_$);
		/*
		 * column.setHeader("<center>Net<br>VM $</center>");
		 */
		column.setHeader("<center>Net<br>CM $</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		column = new ColumnConfig();
		final NumberField netVmPctValue = new NumberField();
		netVmPctValue .setPropertyEditorType(Double.class);
		netVmPctValue.setAllowDecimals(true);
		netVmPctValue.addInputStyleName("text-element-no-underline");	
		CellEditor netVmPctEditor =  new CellEditor(netVmPctValue); 
		netVmPctValue.disable();
		netVmPctEditor.disable();
		column.setEditor(netVmPctEditor);
		column.setId(CostAnalysisKitComponentDTO.NET_VM_PCT);
		/*
		 * column.setHeader("<center>Net<br>VM %</center>");
		 */
		column.setHeader("<center>Net<br>CM %</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"0.00%":number.format(Double.parseDouble(model.get(property).toString())) + "%";
			}
		});
		configs.add(column);
		column = new ColumnConfig();
		final NumberField benchMarkPerEachValue = new NumberField();
		benchMarkPerEachValue .setPropertyEditorType(Double.class);
		benchMarkPerEachValue.setAllowDecimals(true);
		benchMarkPerEachValue.addInputStyleName("text-element-no-underline");	
		CellEditor benchMarkPerEachEditor =  new CellEditor(benchMarkPerEachValue); 
		benchMarkPerEachValue.disable();
		benchMarkPerEachEditor.disable();
		column.setEditor(benchMarkPerEachEditor);
		column.setId(CostAnalysisKitComponentDTO.BENCH_MARK_PER_EACH);
		column.setHeader("<center>Benchmark<br>Per Each $</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);

		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		column = new ColumnConfig();
		final NumberField totBenchMarkValue = new NumberField();
		totBenchMarkValue .setPropertyEditorType(Double.class);
		totBenchMarkValue.setAllowDecimals(true);
		totBenchMarkValue.addInputStyleName("text-element-no-underline");	
		CellEditor totBenchMarkEditor =  new CellEditor(totBenchMarkValue); 
		totBenchMarkValue.disable();
		totBenchMarkEditor.disable();
		column.setEditor(totBenchMarkEditor);
		column.setId(CostAnalysisKitComponentDTO.TOTAL_BENCH_MARK);
		column.setHeader("<center>Total<br>Benchmark $</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		column = new ColumnConfig();
		final NumberField listPricePerEaValue = new NumberField();
		listPricePerEaValue .setPropertyEditorType(Double.class);
		listPricePerEaValue.setAllowDecimals(true);
		listPricePerEaValue.addInputStyleName("text-element-no-underline");	
		CellEditor listPricePerEaEditor =  new CellEditor(listPricePerEaValue); 
		listPricePerEaValue.disable();
		listPricePerEaEditor.disable();
		column.setEditor(listPricePerEaEditor);
		column.setNumberFormat(NumberFormat.getFormat(currencyFormat));
		column.setId(CostAnalysisKitComponentDTO.LIST_PRICE_PER_EA);
		column.setHeader("<center>List Price<br>Per Ea $</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		column = new ColumnConfig();
		final NumberField tot_ListPriceValue = new NumberField();
		tot_ListPriceValue .setPropertyEditorType(Double.class);
		tot_ListPriceValue.setAllowDecimals(true);
		tot_ListPriceValue.addInputStyleName("text-element-no-underline");	
		CellEditor tot_ListPriceEditor =  new CellEditor(tot_ListPriceValue); 
		tot_ListPriceValue.disable();
		tot_ListPriceEditor.disable();
		column.setEditor(tot_ListPriceEditor);
		column.setNumberFormat(NumberFormat.getFormat(currencyFormat));
		column.setId(CostAnalysisKitComponentDTO.TOTAL_LIST_PRICE);
		column.setHeader("<center>Total List<br>Price $</center>");
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property)==null?"$0.00":"$"+number.format(Double.parseDouble(model.get(property).toString()));
			}
		});
		configs.add(column);
		cm = new ColumnModel(configs);
		summary.setHtml(CostAnalysisKitComponentDTO.PBC_VERSION, "<b>Total</b>");   
		summary.setSummaryType(CostAnalysisKitComponentDTO.QTY_PER_FACING, SummaryType.SUM);   
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.QTY_PER_FACING, NumberFormat.getFormat(("#######0")));
		summary.setSummaryType(CostAnalysisKitComponentDTO.NUMBER_FACINGS, SummaryType.SUM);   
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.NUMBER_FACINGS, NumberFormat.getFormat(("#######0"))); 
		summary.setSummaryType(CostAnalysisKitComponentDTO.QTY_PER_DISPLAY, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.QTY_PER_DISPLAY, NumberFormat.getFormat(("#######0"))); 
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.EXCESS_INVT_DOLLAR, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.EXCESS_INVT_DOLLAR, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.EXCESS_INVT_DOLLAR, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' text-align='center' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.EXCESS_INVT_PCT, SummaryType.AVG); 
		//summary.setSummaryFormat(CostAnalysisKitComponentDTO.EXCESS_INVT_PCT, NumberFormat.getFormat((percentFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.EXCESS_INVT_PCT, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					//result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "%</span>";
					result="<span >" + number.format(value.doubleValue()) + "%</span>";
				}
				return result;
			}
		});
		
		/*
		 * Added by Mari
		 * Date : 11-15-2012
		 */
		summary.setCellStyle(CostAnalysisKitComponentDTO.EXCESS_INVT_PCT,"text-align:center");
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.ACTUAL_INVOICE_PER_EA, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.ACTUAL_INVOICE_PER_EA, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.ACTUAL_INVOICE_PER_EA, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					//result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
					result="<span text-align='center' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		
		/*
		 * Added by Mari
		 * Date : 11-15-2012
		 */
		summary.setCellStyle(CostAnalysisKitComponentDTO.ACTUAL_INVOICE_PER_EA,  "text-align:center");
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.INVOICE_WITH_PROGRAM, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.INVOICE_WITH_PROGRAM, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.INVOICE_WITH_PROGRAM, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					//result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
					result="<span >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		/*
		 * Added by Mari
		 * Date : 11-15-2012
		 */
		summary.setCellStyle(CostAnalysisKitComponentDTO.INVOICE_WITH_PROGRAM, "text-align:center");
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;'  >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE_WPGRM, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE_WPGRM, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE_WPGRM, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;'  >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.PRICE_EXCEPTION, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.PRICE_EXCEPTION, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.PRICE_EXCEPTION, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		summary.setSummaryType(CostAnalysisKitComponentDTO.COGS_UNIT_PRICE, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.COGS_UNIT_PRICE, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.COGS_UNIT_PRICE, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		summary.setSummaryType(CostAnalysisKitComponentDTO.TOTAL_COGS_UNIT_PRICE, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.TOTAL_COGS_UNIT_PRICE, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.TOTAL_COGS_UNIT_PRICE, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){

					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		summary.setSummaryType(CostAnalysisKitComponentDTO.BENCH_MARK_PER_EACH, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.BENCH_MARK_PER_EACH, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.BENCH_MARK_PER_EACH, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		summary.setSummaryType(CostAnalysisKitComponentDTO.TOTAL_BENCH_MARK, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.TOTAL_BENCH_MARK, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.TOTAL_BENCH_MARK, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		summary.setSummaryType(CostAnalysisKitComponentDTO.LIST_PRICE_PER_EA, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.LIST_PRICE_PER_EA, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.LIST_PRICE_PER_EA, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		summary.setSummaryType(CostAnalysisKitComponentDTO.TOTAL_LIST_PRICE, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.TOTAL_LIST_PRICE, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.TOTAL_LIST_PRICE, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		summary.setSummaryType(CostAnalysisKitComponentDTO.NET_VM_$, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.NET_VM_$, NumberFormat.getFormat((currencyFormat))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.NET_VM_$, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			}
		});
		summary.setSummaryType(CostAnalysisKitComponentDTO.NET_VM_PCT, SummaryType.AVG); 
		summary.setRenderer(CostAnalysisKitComponentDTO.NET_VM_PCT, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span>" + number.format(value.doubleValue()) + "%</span>";
				}
				return result;
			}
		});
		cm.addAggregationRow(summary);
		loadCostAnalysisComponentInfo(displayScenarioDTO,comparisonView);
		cp.setBodyBorder(false);
		cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
		cp.setLayout(new FitLayout());
		cp.setSize(1130, 300);
		cp.setHeaderVisible(false);
		cp.setFrame(false);
		cp.setHeight(300);
		componentGrid = new Grid<CostAnalysisKitComponentDTO>(costAnalysisKitComponentStore, cm);
		GridSelectionModel<CostAnalysisKitComponentDTO> gm = new GridSelectionModel<CostAnalysisKitComponentDTO>();
		final CustomRowEditor<CostAnalysisKitComponentDTO> re = new CustomRowEditor<CostAnalysisKitComponentDTO>(this);
		re.setClicksToEdit(ClicksToEdit.ONE);
		re.setBodyStyleName("x-row-editor-body");
		componentGrid.setColumnResize(true);
		componentGrid.addPlugin(re); 
		
		componentGrid.setStyleAttribute("borderTop", "none");
		componentGrid.setStyleAttribute("paddingTop","15px");
		componentGrid.setStyleAttribute("paddingRight","5px");
		componentGrid.setStyleAttribute("paddingLeft","15px");
		componentGrid.setStripeRows(true);
		componentGrid.setBorders(true);
		componentGrid.setAutoWidth(true);
		re.addListener(Events.CellMouseDown, new Listener<GridEvent<ModelData>>() {
			public void handleEvent(GridEvent<ModelData> be) {
				int selectedRow = be.getRowIndex();
				int selectedCol = be.getColIndex();
				if((selectedRow > -1) && ( selectedCol==PIM_COL_INDEX || selectedCol==PV_COL_INDEX || selectedCol==PBL_COL_INDEX ) ){
					be.setCancelled(true);
				}
			}
		});
		cp.add(componentGrid);
		System.out.println("DisplayCostDataGroup:createCostAnalysisInfoGrid ends");
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:createCostAnalysisInfoGrid error "+ ex.toString());
		}
		return cp;
	}
	
	public void rejectChanges(){
		costAnalysisKitComponentStore.rejectChanges();
	}
	
	public CostAnalysisInfo getCostAnalysisInfo() {
		return costAnalysisInfo;
	}
	
	public void setCostAnalysisInfo(CostAnalysisInfo costAnalysisInfo) {
		this.costAnalysisInfo = costAnalysisInfo;
	}
	
	private Double ZERO_DOUBLE=0.00;
	public void saveDisplayCost(DisplayCostDTO	displayCostModel, Date caDate, String cmProjectNo,TabPanel panel, TabItem nextTab, DisplayScenarioDTO displayScenarioDTO){
		try{
			if(displayCostModel==null){
				displayCostModel = new DisplayCostDTO();
			}
			if(!overageScrapNo.validate()) return;
			if(!foreCastQty.validate()) return;
			if(!totalDisplaysOrdered.validate()) return;
			if(!customerProgramPercentage.validate()) return;
			
			if(!mdfPct.validate()) return;
			if(!discountPct.validate()) return;
			if(!tradeSalesChanged.validate()) return;
			
			if(!corrugateCostPerUnit.validate()) return;
			if(!fullfillmentCostPerUnit.validate()) return;
			if(!miscCost.validate()) return;
			if(!palletCost.validate()) return;
			if(!shipTestCost.validate()) return;
			if(!totalArtworkCost.validate()) return;
			if(!printingPlateCost.validate()) return;
			if(!dieCuttingCost.validate()) return;
			if(!cTPCost.validate()) return;
			if(!mdfPct.validate()) return;
			if(!discountPct.validate()) return;
			System.out.println("<<Debug>>DisplayCostDataGroup:saveDisplayCost starts"  );
			if (displayCostModel != null ) {
				displayCostModel.setOverageScrapPct(overageScrapNo.getValue()==null? 0:overageScrapNo.getValue().longValue());
				displayCostModel.setCustomerProgramPct(customerProgramPercentage.getValue()==null?0:customerProgramPercentage.getValue().floatValue());
				displayCostModel.setCorrugateCost(corrugateCostPerUnit.getValue()==null?ZERO_DOUBLE.floatValue():corrugateCostPerUnit.getValue().floatValue());
				displayCostModel.setFullfilmentCost(fullfillmentCostPerUnit.getValue()==null?ZERO_DOUBLE.floatValue():fullfillmentCostPerUnit.getValue().floatValue());
				displayCostModel.setMiscCost(miscCost.getValue()==null?ZERO_DOUBLE.floatValue():miscCost.getValue().floatValue());
				displayCostModel.setPalletCost(palletCost.getValue()==null?ZERO_DOUBLE.floatValue():palletCost.getValue().floatValue());
				displayCostModel.setShipTestCost(shipTestCost.getValue()==null?ZERO_DOUBLE.floatValue():shipTestCost.getValue().floatValue());
				displayCostModel.setTotalArtworkCost(totalArtworkCost.getValue()==null?ZERO_DOUBLE.floatValue():totalArtworkCost.getValue().floatValue());
				displayCostModel.setPrintingPlateCost(printingPlateCost.getValue()==null?ZERO_DOUBLE.floatValue():printingPlateCost.getValue().floatValue());
				displayCostModel.setDieCuttingCost(dieCuttingCost.getValue()==null?ZERO_DOUBLE.floatValue():dieCuttingCost.getValue().floatValue());
				displayCostModel.setCtpCost(cTPCost.getValue()==null?ZERO_DOUBLE.floatValue():cTPCost.getValue().floatValue());
				displayCostModel.setExpeditedFreightCost(expectedFrieghtCost.getValue()==null?ZERO_DOUBLE.floatValue():expectedFrieghtCost.getValue().floatValue());
				displayCostModel.setFines(fines.getValue()==null?ZERO_DOUBLE.floatValue():fines.getValue().floatValue());
				displayCostModel.setMdfPct(mdfPct.getValue()==null?ZERO_DOUBLE.floatValue():mdfPct.getValue().floatValue());
				displayCostModel.setTradeSales(tradeSalesChanged.getValue()==null?ZERO_DOUBLE.floatValue():tradeSalesChanged.getValue().floatValue());
				displayCostModel.setDiscountPct(discountPct.getValue()==null?ZERO_DOUBLE.floatValue():discountPct.getValue().floatValue());
				displayCostModel.setUserLastChanged(App.getUser().getUsername());
				displayCostModel.setCaDate(caDate);
				displayCostModel.setDtLastChanged(new Date());
				if(displayCostModel.getDisplayId()==null) displayCostModel.setDisplayId(getDisplayDTO().getDisplayId());
				System.out.println("<<Debug>>DisplayCostDataGroup:saveDisplayCost DisplayDisplay Id ..2.." + getDisplayDTO().getDisplayId() );
				if (displayCostModel.getDisplayCostId() == null) {
					displayCostModel.setDtCreated(new Date());
					displayCostModel.setUserCreated(App.getUser().getUsername());
				}
				final MessageBox boxDisplayCost = MessageBox.wait("Progress",
						"Saving display cost data, please wait...", "Saving...");
				getCostAnalysisInfo().setDisplayCostModel(displayCostModel);
				AppService.App.getInstance().saveDisplayCostDTO(displayCostModel, new Service.SaveDisplayCostDTO(getdisplayCostDataGroup(), displayCostModel,boxDisplayCost, panel,  nextTab, displayScenarioDTO));
			}
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:saveDisplayCost error "+ ex.toString());
		}
	}

	public void resetDisplayCost(DisplayCostDTO displayCostModel, DisplayScenarioDTO displayScenario){
		overageScrapNo.setValue(displayCostModel.getOverageScrapPct());
		customerProgramPercentage.setValue(displayCostModel.getCustomerProgramPct());
		corrugateCostPerUnit.setValue(displayCostModel.getCorrugateCost());
		fullfillmentCostPerUnit.setValue(displayCostModel.getFullfilmentCost());
		miscCost.setValue(displayCostModel.getMiscCost());
		palletCost.setValue(displayCostModel.getPalletCost());
		shipTestCost.setValue(displayCostModel.getShipTestCost());
		totalArtworkCost.setValue(displayCostModel.getTotalArtworkCost());
		printingPlateCost.setValue(displayCostModel.getPrintingPlateCost());
		dieCuttingCost.setValue(displayCostModel.getDieCuttingCost());
		cTPCost.setValue(displayCostModel.getCtpCost());
		expectedFrieghtCost.setValue(displayCostModel.getExpeditedFreightCost());
		fines.setValue(displayCostModel.getFines());
		customerProgramPercentage.setValue(displayCostModel.getCustomerProgramPct());
		foreCastQty.setValue(displayScenario.getQtyForecast());
		actualQty.setValue(displayScenario.getActualQty());
		mdfPct.setValue(displayCostModel.getMdfPct());
		discountPct.setValue(displayCostModel.getDiscountPct());

		// Joseph ********************************
		tradeSalesChanged.setValue(displayCostModel.getTradeSales());
		// Joseph ********************************
	}

	public void save(Button btn) {
		try{
			System.out.println("DisplayCostDataGroup:save..starts..");
			if (costAnalysisKitComponentStore.getModifiedRecords().size() == 0) {
				MessageBox.alert("Validation Error",
						"No record has been modified", null);
				return;
			}
			List<Record> mods = costAnalysisKitComponentStore.getModifiedRecords();
			if (!validateControls(mods))
				return;
			List<CostAnalysisKitComponentDTO> costAnalysisDTOList = new ArrayList<CostAnalysisKitComponentDTO>();
			CostAnalysisKitComponentDTO costAnalysisDTO;
			try{
				for (int i = 0; i < mods.size(); i++) {
					costAnalysisDTO = (CostAnalysisKitComponentDTO)((Record)mods.get(i)).getModel();
					costAnalysisDTOList.add(costAnalysisDTO);
				}
			}catch(Exception ex){
				System.out.println("CostAnalysisInfoGrid ..ERROR.." + ex.getMessage());
			}
			Double customerProgramPercentageVal = (Double) (customerProgramPercentage.getValue()==null?0:customerProgramPercentage.getValue().doubleValue());
			Long foreCastQtyVal = (Long) (foreCastQty.getValue()==null?0:foreCastQty.getValue().longValue());
			final MessageBox box = MessageBox.wait("Progress",
					"Saving cost analysis component, please wait...", "Saving...");
			AppService.App.getInstance().saveCostAnalysisKitComponentInfo(costAnalysisDTOList,foreCastQtyVal,customerProgramPercentageVal, 
					new Service.SaveCostAnalysisKitComponentInfo(getSelectionGrid(),componentGrid,costAnalysisKitComponentStore,box,getCostAnalysisInfo().displayScenarioDTOList.get(getCostAnalysisInfo().vpScenarioPanelTabs.getSelectedItem().getTabIndex())) );
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:save error "+ ex.toString());
		}
		System.out.println("DisplayCostDataGroup:save..ends..");
	}
	
	private DisplayCostDataGroup getSelectionGrid(){
		return this;
	}
	
	public Boolean validateControls(List<Record> mods) {
		for (int i = 0; i < mods.size(); i++) {
			CostAnalysisKitComponentDTO itemProj = (CostAnalysisKitComponentDTO)((Record)mods.get(i)).getModel();
			costAnalysisKitComponentStore.commitChanges();
		}
		return true;
	}
	
	public Double calcTotalActualInvoice() {
		System.out.println("DisplayCostDataGroup:calcTotalActualInvoice starts");
		Double totalActualInvoice=0.0;
		try{
			List<CostAnalysisKitComponentDTO> mods = costAnalysisKitComponentStore.getModels();
			for (int i = 0; i < mods.size(); i++) {
				CostAnalysisKitComponentDTO itemProj = mods.get(i);
				totalActualInvoice+=itemProj.getTotalActualInvoice();
			}
			System.out.println("DisplayCostDataGroup:calcTotalActualInvoice ends");
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:calcTotalActualInvoice error "+ ex.toString());
		}
		return totalActualInvoice;
	}
	
	public Double calcTotalActualInvoiceWithProgram() {
		System.out.println("DisplayCostDataGroup:calcTotalActualInvoiceWithProgram starts");
		Double totalActualInvoiceWithProgram=0.0;
		try{
			List<CostAnalysisKitComponentDTO> mods = costAnalysisKitComponentStore.getModels();
			for (int i = 0; i < mods.size(); i++) {
				CostAnalysisKitComponentDTO itemProj = mods.get(i);
				totalActualInvoiceWithProgram+=itemProj.getTotalActualInvoiceWpgrm();
			}
			System.out.println("DisplayCostDataGroup:calcTotalActualInvoiceWithProgram ends");
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:calcTotalActualInvoiceWithProgram error "+ ex.toString());
		}
		return totalActualInvoiceWithProgram;
	}
	
	public Double calcTotalCogsVal() {
		System.out.println("DisplayCostDataGroup:calctotalCogsVal starts");
		Double totalCogsVal=0.0;
		try{
			List<CostAnalysisKitComponentDTO> mods = costAnalysisKitComponentStore.getModels();
			for (int i = 0; i < mods.size(); i++) {
				CostAnalysisKitComponentDTO itemProj = mods.get(i);
				totalCogsVal+=itemProj.getTotalCogsUnitPrice();
			}
			System.out.println("DisplayCostDataGroup:calctotalCogsVal ends");
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:calctotalCogsVal error "+ ex.toString());
		}
		return totalCogsVal;
	}

	public Double calcExcessSumVmDollar() {
		System.out.println("DisplayCostDataGroup:calcExcessSumVmDollar starts");
		Double calculatedSumVmDollar=0.0;
		try{
			List<CostAnalysisKitComponentDTO> mods = costAnalysisKitComponentStore.getModels();
			for (int i = 0; i < mods.size(); i++) {
				CostAnalysisKitComponentDTO itemProj = mods.get(i);
				calculatedSumVmDollar+=itemProj.getExcessInvtDollar();
			}
			System.out.println("DisplayCostDataGroup:calcExcessSumVmDollar ends");
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:calcExcessSumVmDollar error "+ ex.toString());
		}
		return calculatedSumVmDollar;
	}
	
	public Double calcSumVmDollar(boolean discountBp) {
		System.out.println("DisplayCostDataGroup:calcTotalActualInvoice starts");
		Double calculatedSumVmDollar=0.0;
		try{
			List<CostAnalysisKitComponentDTO> mods = costAnalysisKitComponentStore.getModels();
			for (int i = 0; i < mods.size(); i++) {
				CostAnalysisKitComponentDTO itemProj = mods.get(i);
				if(discountBp){
					calculatedSumVmDollar+=itemProj.getNetVmAmountBp();
				}else{
					calculatedSumVmDollar+=itemProj.getNetVmAmount();
				}
			}
			System.out.println("DisplayCostDataGroup:calcTotalActualInvoice ends");
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:calcTotalActualInvoice error "+ ex.toString());
		}
		return calculatedSumVmDollar;
	}
	
	public Double calcSumVmPct(boolean discountBp ) {
		System.out.println("DisplayCostDataGroup:calcSumVmPct starts");
		Double calculatedSumVmPct=0.00;
		Double calculatedAvgVmPct=0.00;
		try{
			List<CostAnalysisKitComponentDTO> mods = costAnalysisKitComponentStore.getModels();
			for (int i = 0; i < mods.size(); i++) {
				CostAnalysisKitComponentDTO itemProj = mods.get(i);
				if(discountBp){
					calculatedSumVmPct+=itemProj.getNetVmPctBp();
				}else{
					calculatedSumVmPct+=itemProj.getNetVmPct();
				}
			}
			if( mods.size()>0){
				calculatedAvgVmPct=(calculatedSumVmPct/mods.size());
			}
			System.out.println("DisplayCostDataGroup:calcSumVmPct ends");
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:calcSumVmPct error "+ ex.toString());
		}
		return calculatedAvgVmPct;
	}
	
	public Double calcExcessSumVmPct() {
		System.out.println("DisplayCostDataGroup:calcExcessSumVmPct starts");
		Double calculatedSumVmPct=0.00;
		Double calculatedAvgVmPct=0.00;
		try{
			List<CostAnalysisKitComponentDTO> mods = costAnalysisKitComponentStore.getModels();
			for (int i = 0; i < mods.size(); i++) {
				CostAnalysisKitComponentDTO itemProj = mods.get(i);
				calculatedSumVmPct+=itemProj.getExcessInvtPct();
			}
			if( mods.size()>0){
				calculatedAvgVmPct=(calculatedSumVmPct/mods.size());
			}
			System.out.println("DisplayCostDataGroup:calcExcessSumVmPct ends");
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:calcExcessSumVmPct error "+ ex.toString());
		}
		return calculatedAvgVmPct;
	}
	
	public void reCalculatePriceException(DisplayDTO displayDTO, Double customerProgramPct, Date reCalculateDate, DisplayScenarioDTO displayScenarioDTO, boolean comparisonView ){
		System.out.println("DisplayCostDataGroup:reCalculatePriceException starts");
		System.out.println("DisplayCostDataGroup:reCalculatePriceException reCalcDate : "+ reCalculateDate);
		try{
			final MessageBox box = MessageBox.wait("Progress",
					"re processing price exception, please wait...", "Processing...");
			AppService.App.getInstance().reCalculateILSPriceException(displayDTO,reCalculateDate,customerProgramPct,displayScenarioDTO,new Service.LoadCostAnalysisKitComponentInfo(this, costAnalysisKitComponentStore,box, displayScenarioDTO,comparisonView));
			System.out.println("DisplayCostDataGroup:reCalculatePriceException ends");
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:reCalculatePriceException error "+ ex.toString());
		}
	}

	private DisplayCostDataGroup getdisplayCostDataGroup() {
		return this;
	}
	public void showAddKitComponentWindow(){
		window.show();
	}
	public void deleteKitComponent(){
		try{
			if (componentGrid.getSelectionModel().getSelectedItem() != null) {
				Listener<MessageBoxEvent>cb = new Listener<MessageBoxEvent>() {
					public void handleEvent(MessageBoxEvent mbe) {
						String id = mbe.getButtonClicked().getItemId();
						if (Dialog.YES == id) {
							CostAnalysisKitComponentDTO kitComponent = componentGrid.getSelectionModel().getSelectedItem();
							final MessageBox box = MessageBox.wait("Progress",
									"Deleting cost analysis component data, please wait...", "Deleting...");
							AppService.App.getInstance().deleteCostAnalysisKitComponent(kitComponent, 
									new Service.DeleteCostAnalysisKitComponentInfo(getdisplayCostDataGroup(),componentGrid,kitComponent, componentGrid.getStore(),box));
						}
					}
				};
				MessageBox.confirm("Delete?", "Are you sure you want to delete this record?", cb);
			}
		}catch(Exception ex){
			System.out.println("=======DisplayCostDataGroup:deleteKitComponent error "+ ex.toString());
		}
	}
	
	public void setupSelectionDialog() {
		System.out.println("=======DisplayCostDataGroup:setupSelectionDialog getDisplayScenarioDTO()= "+getDisplayScenarioDTO().getDisplayScenarioId());
		window.setSize(675, 425);
		window.setPlain(true);
		window.setHeading("Product SKU Selection");
		window.setLayout(new FitLayout());
		FormPanel panel = new FormPanel();
		panel.setLabelAlign(FormPanel.LabelAlign.TOP);
		panel.setHeaderVisible(false);
		LayoutContainer top = new LayoutContainer();
		top.setLayout(new ColumnLayout());
		LayoutContainer bottom = new LayoutContainer();
		bottom.setLayout(new ColumnLayout());
		LayoutContainer topLeft = new LayoutContainer();
		LayoutContainer topRight = new LayoutContainer();
		FormLayout topLeftFormLayout = new FormLayout();
		topLeftFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
		topLeft.setStyleAttribute("padding", "5px");
		topLeft.setLayout(topLeftFormLayout);
		FormLayout topRightFormLayout = new FormLayout();
		topRightFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
		topRight.setStyleAttribute("padding", "5px");
		topRight.setLayout(topRightFormLayout);
		FormLayout bottomFormLayout = new FormLayout();
		bottomFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
		bottom.setStyleAttribute("padding", "5px");
		bottom.setLayout(bottomFormLayout);
		top.add(topLeft, new ColumnData(210));
		top.add(topRight, new ColumnData(300));
		panel.add(top);
		panel.add(bottom);
		final ProductSKUSelectionResultsGrid productResults = new ProductSKUSelectionResultsGrid(null,window, getDisplayDTO(),getDisplayScenarioDTO(),  0L,this);
		bottom.add(productResults);
		final Button btn;
		final TextField<String> skuSearchString = new TextField<String>();
		skuSearchString.setFieldLabel("SKU Search String");
		skuSearchString.setTabIndex(1);
		KeyListener skuListener = new KeyListener() {
			@Override
			public void componentKeyPress(ComponentEvent event) {
				if (event.getKeyCode() == 13) {
					String sku = skuSearchString.getValue();
					Long maxSeqNum = 0L;
					List<Long> kitComponentIds = new LinkedList<Long>();
					List<CostAnalysisKitComponentDTO> kitComponentList = componentGrid.getStore().getModels();
					for (int i = 0; i < kitComponentList.size(); i++) {
						kitComponentIds.add(kitComponentList.get(i).getKitComponentId());
						if (kitComponentList.get(i).getSequenceNum() > maxSeqNum)
							maxSeqNum = kitComponentList.get(i).getSequenceNum();
					}
					productResults.load(sku, kitComponentIds, maxSeqNum + 1);
					event.cancelBubble();
					event.preventDefault();
					event.setCancelled(true);
					return;
				} 
				else super.componentKeyPress(event);
			}
		};
		skuSearchString.addKeyListener(skuListener);
		topLeft.add(skuSearchString);
		SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
			public void componentSelected(ButtonEvent ce) {
				Button button = (Button) ce.getComponent();
				if (displayDTO.getDisplayId() == null) {
					MessageBox.alert("Validation Error", "There is no display defined " , null);
					return ;
				}
				if (button.getText().equals(SEARCH)) {
					String sku = skuSearchString.getValue();
					Long maxSeqNum = 0L;
					List<Long> kitComponentIds = new LinkedList<Long>();
					List<CostAnalysisKitComponentDTO> kitComponentList = componentGrid.getStore().getModels();
					for (int i = 0; i < kitComponentList.size(); i++) {
						kitComponentIds.add(kitComponentList.get(i).getKitComponentId());
						if (kitComponentList.get(i).getSequenceNum() > maxSeqNum)
							maxSeqNum = kitComponentList.get(i).getSequenceNum();
					}
					productResults.load(sku, kitComponentIds, maxSeqNum + 1);
				}
			}
		};
		btn = new Button(SEARCH, listener);
		btn.setStyleAttribute("margin-top", "23px");
		btn.setTabIndex(2);
		topRight.add(btn);
		window.add(panel, new FitData(4));
	}
	
	public void enableDisableButtons(boolean bEditable){
			overageScrapNo.setReadOnly(bEditable);
			foreCastQty.setReadOnly(bEditable);
			actualQty.setReadOnly(bEditable);
			customerProgramPercentage.setReadOnly(bEditable);
			mdfPct.setReadOnly(bEditable);
			tradeSalesChanged.setReadOnly(bEditable);
			discountPct.setReadOnly(bEditable);
			corrugateCostPerUnit.setReadOnly(bEditable);
			fullfillmentCostPerUnit.setReadOnly(bEditable);
			miscCost.setReadOnly(bEditable);
			palletCost.setReadOnly(bEditable);
			shipTestCost.setReadOnly(bEditable);
			totalArtworkCost.setReadOnly(bEditable);
			printingPlateCost.setReadOnly(bEditable);
			dieCuttingCost.setReadOnly(bEditable);
			cTPCost.setReadOnly(bEditable);
			expectedFrieghtCost.setReadOnly(bEditable);
			fines.setReadOnly(bEditable);
			mdfPct.setReadOnly(bEditable);
			discountPct.setReadOnly(bEditable);
	}
	
	@Override
	public boolean isDirty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void overrideDirty() {
		// TODO Auto-generated method stub
	}
	@Override
	public void save(Button btn, TabPanel panel, TabItem nextTab) {
		// TODO Auto-generated method stub
	}
}


