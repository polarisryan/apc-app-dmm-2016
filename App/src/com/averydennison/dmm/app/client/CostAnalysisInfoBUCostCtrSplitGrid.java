package com.averydennison.dmm.app.client;
import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.BuCostCenterSplitDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridView;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.i18n.client.NumberFormat;

/**
 * User: Mari
 * Date: Aug 4, 2010
 * Time: 8:27:42 AM
 */
public class CostAnalysisInfoBUCostCtrSplitGrid extends LayoutContainer {
	private ListStore<BuCostCenterSplitDTO> store = new ListStore<BuCostCenterSplitDTO>();
	public Grid<BuCostCenterSplitDTO> buCostCtrSplitgrid;
	public ContentPanel createBUCostCenterGroup(NumberField foreCastQty,NumberField customerProgramPercentage,NumberField totalDisplaysOrdered,Double totalActualInvoiceTotCostVal,Double totalDisplayCostsVal,List<CostAnalysisKitComponentDTO> loadedCostAnalysisKitComponentDTO) {
		setLayout(new FlowLayout(12));
		int x = 110;
		ContentPanel cp = new ContentPanel();
		try{
			List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
			ColumnConfig column = new ColumnConfig();
			column.setId(BuCostCenterSplitDTO.BU_DESC);
			column.setHeader("BU Name");
			column.setAlignment(Style.HorizontalAlignment.LEFT);
			column.setWidth(x + 50);
			configs.add(column);
			column = new ColumnConfig();
			column.setId(BuCostCenterSplitDTO.NET_SALES);
			column.setHeader("Net Sales");
			column.setAlignment(Style.HorizontalAlignment.RIGHT);
			column.setNumberFormat(NumberFormat.getFormat("$###,###,##0.00"));
			column.setWidth(x - 5);
			configs.add(column);
			column = new ColumnConfig();
			column.setId("VmPctPrepared");
			column.setHeader("CM %");
			column.setAlignment(Style.HorizontalAlignment.RIGHT);
			column.setNumberFormat(NumberFormat.getFormat("#0.00%"));
			column.setWidth(x - 25);
			configs.add(column);
			column = new ColumnConfig();
			column.setId(BuCostCenterSplitDTO.PCT_OF_COST);
			column.setHeader("% of Costs");
			column.setAlignment(Style.HorizontalAlignment.RIGHT);
			column.setNumberFormat(NumberFormat.getFormat("$###,###,##0.00"));
			column.setWidth(x - 10);
			configs.add(column);
			column = new ColumnConfig();
			column.setId(BuCostCenterSplitDTO.VM_PCT);
			column.setId("buSplitPrepared");
			column.setHeader("BU Split");
			column.setAlignment(Style.HorizontalAlignment.RIGHT);
			column.setWidth(x - 25);
			column.setNumberFormat(NumberFormat.getFormat("#0.00%"));
			configs.add(column);			
			ColumnModel cm = new ColumnModel(configs);
			cp.setBodyBorder(false);
			cp.setHeaderVisible(false);
			cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
			cp.setLayout(new FitLayout());
			cp.setFrame(false);
			cp.setSize(560, 155);
			//cp.setSize(560, 120);
			buCostCtrSplitgrid = new Grid<BuCostCenterSplitDTO>(store, cm);
			buCostCtrSplitgrid.setColumnResize(true);
			buCostCtrSplitgrid.setStyleAttribute("borderTop", "none");
			buCostCtrSplitgrid.setStyleAttribute("paddingTop","15px");
			buCostCtrSplitgrid.setStyleAttribute("paddingRight","5px");
			buCostCtrSplitgrid.setStyleAttribute("paddingLeft","15px");
			buCostCtrSplitgrid.setStripeRows(true);
			buCostCtrSplitgrid.setBorders(true);
			buCostCtrSplitgrid.setView(new GridView(){
				protected ModelData prepareData(ModelData model) {
					model.set("VmPctPrepared", model.get(BuCostCenterSplitDTO.VM_PCT) != null ? model.<Double>get(BuCostCenterSplitDTO.VM_PCT)/100d : null);
					model.set("buSplitPrepared", model.get(BuCostCenterSplitDTO.BU_SPLIT) != null ? model.<Double>get(BuCostCenterSplitDTO.BU_SPLIT)/100d : null);
					return model;
				}
			});
			cp.add(buCostCtrSplitgrid);
		}catch(Exception ex){
			System.out.println("=======CostAnalysisInfoBUCostCtrSplitGrid:createBUCostCenterGrouperror "+ ex.toString());
		}
		return cp;
	}
	public ListStore<BuCostCenterSplitDTO> busCostCenterSplitStore = new ListStore<BuCostCenterSplitDTO>();
	public void loadBUCostCenterSplitGrid(DisplayCostDataGroup displayCostDataGroup, Boolean bDefaultProgramDiscount, Long displayId,NumberField actualQty,NumberField customerProgramPercentage,NumberField totalDisplaysOrdered,Double totalActualInvoiceTotCostVal,Double totalDisplayCostsVal,List<CostAnalysisKitComponentDTO> loadedKitComponents, int tabScenarioIdx){
		Long actualQtyVal;
		Double customerProgramPercentageVal;
		Double totalDisplaysOrderedVal;
		Double mdfPctVal;
		Double discountPctVal;
		
		System.out.println("DisplayCostDataGroup:loadBUCostCenterSplitGrid starts ");
		/*try{*/
			actualQtyVal = (Long) (actualQty.getValue()==null?0:actualQty.getValue().longValue());
			customerProgramPercentageVal = (Double) (customerProgramPercentage==null || customerProgramPercentage.getValue()==null?0:customerProgramPercentage.getValue().doubleValue());
			totalDisplaysOrderedVal = (Double) (totalDisplaysOrdered.getValue()==null?0:totalDisplaysOrdered.getValue().doubleValue());
			totalActualInvoiceTotCostVal = (Double) totalActualInvoiceTotCostVal;
			totalDisplayCostsVal= (Double) totalDisplayCostsVal;
			
			
			mdfPctVal = (Double) (displayCostDataGroup.mdfPct.getValue()==null?0:(displayCostDataGroup.mdfPct.getValue().doubleValue() /  displayCostDataGroup.percentMaxValue));
			discountPctVal = (Double) (displayCostDataGroup.discountPct.getValue()==null?0:(displayCostDataGroup.discountPct.getValue().doubleValue() /  displayCostDataGroup.percentMaxValue));
			
			/*mdfPctVal=displayCostDataGroup.mdfPct.getValue().doubleValue();
			discountPctVal=displayCostDataGroup.discountPct.getValue().doubleValue();*/
			
			final MessageBox progressBar = MessageBox.wait("Calculating",
					"Calculating Business Unit Split...", "Calculating...");
			AppService.App.getInstance().loadBUCostCenterSplitGroup(mdfPctVal,discountPctVal, bDefaultProgramDiscount,displayId,actualQtyVal,totalDisplaysOrderedVal,totalActualInvoiceTotCostVal,totalDisplayCostsVal,customerProgramPercentageVal,loadedKitComponents,
					new Service.LoadBUCostCenterSplitGroup(displayCostDataGroup, busCostCenterSplitStore,this,progressBar,tabScenarioIdx));
		/*}catch(Exception ex){
			System.out.println("DisplayCostDataGroup:loadBUCostCenterSplitGrid error "+ ex.toString());
		}*/
		System.out.println("DisplayCostDataGroup:loadBUCostCenterSplitGrid ends ");
	}
}
