package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.CostAnalysis;
import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.extjs.gxt.ui.client.widget.table.NumberCellRenderer;
import com.google.gwt.i18n.client.NumberFormat;

public class CostAnalysisInfoData extends LayoutContainer {
	private FormData formData;
    private final ComboBox<StatusDTO> status = new ComboBox<StatusDTO>();
    private final ComboBox<DisplaySalesRepDTO> projectManager = new ComboBox<DisplaySalesRepDTO>();
    private final ComboBox<ManufacturerCountryDTO> manufacturer = new ComboBox<ManufacturerCountryDTO>();
    private final ComboBox<LocationCountryDTO> location = new ComboBox<LocationCountryDTO>();

	  public CostAnalysisInfoData() {
	        setLayout(new FlowLayout(10));

	        int x = 100;

	        LayoutContainer left = new LayoutContainer();
	        LayoutContainer center = new LayoutContainer();
	        LayoutContainer right = new LayoutContainer();
	        
	        RowLayout leftLayout = new RowLayout();
	        left.setStyleAttribute("paddingRight", "5px");
	        left.setLayout(leftLayout);
	        
	        RowLayout centerLayout = new RowLayout();
	        center.setStyleAttribute("paddingRight", "5px");
	        center.setLayout(centerLayout);
	        
	        RowLayout rightLayout = new RowLayout();
	        right.setStyleAttribute("paddingRight", "5px");
	        right.setLayout(rightLayout);
	        
	        DateField infoDetailsDate = new DateField();
	        infoDetailsDate.setFieldLabel("Date");
	        infoDetailsDate.setWidth(75);
	        infoDetailsDate.setFieldLabel("infoDetailsDate");
	        infoDetailsDate.setAllowBlank(true);
	        left.add(infoDetailsDate , formData);

	        
	        NumberField infoDetailsYear = new NumberField();
	        infoDetailsYear.setFieldLabel("Year");
	        infoDetailsYear.setAllowBlank(true);
	        infoDetailsYear.setReadOnly(false);
	        infoDetailsYear.setMinLength(4);
	        infoDetailsYear.setMaxLength(4);
	        left.add(infoDetailsYear, formData);
	        
	        
	        NumberField infoDetailsProjectNo = new NumberField();
	        infoDetailsProjectNo.setFieldLabel("CM Project #");
	        infoDetailsProjectNo.setAllowBlank(true);
	        infoDetailsProjectNo.setReadOnly(false);
	        infoDetailsProjectNo.setMinLength(4);
	        infoDetailsProjectNo.setMaxLength(4);
	        left.add(infoDetailsProjectNo, formData);
	        
	        Service service = new Service();
	        ListStore<StatusDTO> statuses = new ListStore<StatusDTO>();
	        service.setStatusStore(statuses);
	        
	        ListStore<CustomerCountryDTO> customers = new ListStore<CustomerCountryDTO>();
	        service.setCustomerCountryStore(null, customers);
	        
	        ListStore<DisplaySalesRepDTO> displaySalesReps = new ListStore<DisplaySalesRepDTO>();
	        AppService.App.getInstance().getDisplaySalesRepDTOs(new Service.SetProjectManagerStore(displaySalesReps));
	        
	        ListStore<ManufacturerCountryDTO> manufacturers = new ListStore<ManufacturerCountryDTO>();
	        service.setManufacturerCountryStore(null, manufacturers);
	        
	        ListStore<LocationCountryDTO> locations = new ListStore<LocationCountryDTO>();
	        service.setLocationCountryStore(null, locations);
	        
	        manufacturer.setFieldLabel("Corrugate Vendor");
	        manufacturer.setDisplayField(ManufacturerCountryDTO.NAME);
	        manufacturer.setStore(manufacturers);
	        manufacturer.setWidth(100);
	        manufacturer.setAllowBlank(true);
	        manufacturer.setEditable(false);
	        manufacturer.setTriggerAction(ComboBox.TriggerAction.ALL);
	        center.add(manufacturer, formData);
	        
	        
	        location.setFieldLabel("Packout Vendor");
	        location.setDisplayField(ManufacturerCountryDTO.NAME);
	        location.setStore(locations);
	        location.setWidth(100);
	        location.setAllowBlank(true);
	        location.setEditable(false);
	        location.setTriggerAction(ComboBox.TriggerAction.ALL);
	        center.add(location, formData);
	        
	        projectManager.setFieldLabel("Project Manager");
	        projectManager.setDisplayField(DisplaySalesRepDTO.FULL_NAME);
	        projectManager.setWidth(100);
	        projectManager.setStore(displaySalesReps);
	        projectManager.setAllowBlank(false);
	        projectManager.setEditable(false);
	        projectManager.setTriggerAction(ComboBox.TriggerAction.ALL);
	        center.add(projectManager, formData);
	        
	        status.setFieldLabel("Status");
	        status.setDisplayField(StatusDTO.STATUS_NAME);
	        status.setWidth(100);
	        status.setStore(statuses);
	        status.setAllowBlank(false);
	        status.setEditable(false);
	        status.setTriggerAction(ComboBox.TriggerAction.ALL);
	        
	        right.add(status, formData);
	        
	        LayoutContainer main = new LayoutContainer();
	        main.setLayout(new ColumnLayout());
	        
	         main.add(left);
	         main.add(center);
	         main.add(right);
	        
	         ContentPanel cp = new ContentPanel();
	         cp.setBodyBorder(false);
	         cp.setHeaderVisible(false);
	         cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
	         cp.setLayout(new FitLayout());
	         cp.setSize(225, 200);
	         cp.add(main);
	         add(cp);
	    }
	}
