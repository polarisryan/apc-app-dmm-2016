package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.models.CorrugateDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.util.Constants;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Oct 26, 2009
 * Time: 11:23:01 AM
 */
public class CorrugateComponentInfo extends LayoutContainer {
    private FormData formData;
    private VerticalPanel vp;
    private static final String SAVE = "Save";
    private Button btnSave;
    private DisplayTabs displayTabs;
    public DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
    public CorrugateComponentEditorGrid corrCompEditorGrid;
    public CorrugateInfoGroup corrugateInfoGroup;
    public CorrugateComponentInfo() {
    	corrugateInfoGroup = new CorrugateInfoGroup();
    	corrCompEditorGrid=new CorrugateComponentEditorGrid();
        formData = new FormData("95%");
        vp = new VerticalPanel();
        vp.setSpacing(10);
    }
    
    private void setPermissions(){
    	//if(!App.isAuthorized("CorrugateComponentInfo.SAVE")) btnSave.disable();
    }

    public DisplayDTO displayModel;
    private ChangeListener displayListener;
    private ChangeListener corrugateListener;
    
    
    public void updateView() {
    	 displayModel = displayTabs.getDisplayModel();
        if (displayModel != null && displayModel.getDisplayId() != null) {
            displayTabHeader.setDisplay(displayModel);
            corrCompEditorGrid.setDisplayDTO(displayModel);
            corrCompEditorGrid.loadCorrugateComponents();
        } else {
        	setEmptyText();
        }
    }
    
    private void setEmptyText(){
    	
    }
    
   
    private void setModelsAndView() {
        displayModel = displayTabs.getDisplayModel();
        if (displayModel == null) {
            displayModel = new DisplayDTO();
        }
        updateView();
        //setReadOnly();
        //setActionButton();
        displayListener = new ChangeListener() {
            public void modelChanged(com.extjs.gxt.ui.client.data.ChangeEvent event) {
                displayModel = (DisplayDTO) event.getSource();
                System.out.println("CorrugateComponentInfo:displayListener TRIGGERED");
                if (displayModel != null && displayModel.getDisplayId() != null) {
                    updateView();
                }
            }
        };
        displayModel.addChangeListener(displayListener);
        corrugateModel = this.getData(Constants.DISPLAY_CORRUGATE);
        if (corrugateModel == null) {
        	corrugateModel = new CorrugateDTO();
        }
        corrugateListener = new ChangeListener() {
            public void modelChanged(com.extjs.gxt.ui.client.data.ChangeEvent event) {
            	corrugateModel = (CorrugateDTO) event.getSource();
            	System.out.println("CorrugateComponentInfo:corrugateListener TRIGGERED");
                if (corrugateModel != null && corrugateModel.getDisplayId() != null) {
                    updateView();
                }
            }
        };
        corrugateModel.addChangeListener(corrugateListener);
    }
    
    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        setModelsAndView();
        createHeader();
        createForm();
        setPermissions();
        //setReadOnly();
        add(vp);
    }

    private void createHeader() {
    	add(displayTabHeader);
    }
    private CorrugateDTO corrugateModel;
    public void reload() {
		try{
			displayModel = displayTabs.getDisplayModel();
			if (displayModel.getDisplayId() != null) {
				displayTabHeader.setDisplay(displayModel);
			}
			corrugateModel = this.getData(Constants.DISPLAY_CORRUGATE);
	        if (corrugateModel == null) {
	        	corrugateModel = new CorrugateDTO();
	        }
	      if (displayModel.getDisplayId() != null ) {
				AppService.App.getInstance().getCorrugateDTO(displayModel,new Service.ReloadCorrugate(displayModel,corrugateModel,this,corrugateInfoGroup,corrCompEditorGrid));
				//overrideDirty = false;
			}
		}catch(Exception ex){
			System.out.println("CorrugateComponentInfo:reload error "+ ex.toString());
		}
	}
    
    public DisplayTabs getDisplayTabs() {
		return displayTabs;
	}

	public void setDisplayTabs(DisplayTabs displayTabs) {
		this.displayTabs = displayTabs;
	}
	
	public void clearModel(){
		  if (displayModel != null) displayModel.removeChangeListener(displayListener);
	      if (corrugateModel != null) corrugateModel.removeChangeListener(corrugateListener);
	      displayModel = null;
	      corrugateModel = null;
	}
	private void createForm() {
        FormPanel formPanel = new FormPanel();
        formPanel.setHeaderVisible(false);
        formPanel.setFrame(true);
        formPanel.setSize(1195, 405);
        formPanel.setLabelAlign(FormPanel.LabelAlign.LEFT);
        LayoutContainer main = new LayoutContainer();
        main.setLayout(new FlowLayout());
        LayoutContainer bottom = new LayoutContainer();
        LayoutContainer top = new LayoutContainer();
        FormLayout bottomLayout = new FormLayout();
        bottomLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        FormLayout topLayout = new FormLayout();
        top.setStyleAttribute("paddingRight", "2px");
        topLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        bottom.setLayout(bottomLayout);
        top.setLayout(topLayout);
        top.setStyleAttribute("paddingRight", "3px");
        displayModel = displayTabs.getDisplayModel();
        corrCompEditorGrid.setDisplayDTO(this.getDisplayTabs().getDisplayModel());
        corrCompEditorGrid.setCorrugateInfoGroup(corrugateInfoGroup);
        Boolean b = this.getData("readOnly");
        Boolean readOnly=false;
        
        if (b!=null && b) {
        	readOnly=true;
        }
        ContentPanel corrCompEditorPanel=corrCompEditorGrid.createForm(readOnly);
        bottom.add(corrCompEditorPanel);
        if(corrugateModel==null){
        	corrugateModel=new CorrugateDTO();
        }
        corrugateInfoGroup.setDisplayDTO(displayModel);
        corrugateInfoGroup.setCorrugateComponentInfo(this);
        top.add(corrugateInfoGroup.createCorrugateInfoGroup( corrugateModel,readOnly));
	    FieldSet corrugateGroupFieldSet = new FieldSet();
		FormLayout corrugateGroup = new FormLayout();
		corrugateGroup.setLabelAlign(FormPanel.LabelAlign.LEFT);
		corrugateGroupFieldSet.setLayout(corrugateGroup);
		corrugateGroupFieldSet.setHeading("Quote Detail");
		corrugateGroupFieldSet.add(top);
		corrugateGroupFieldSet.add(bottom);
		main.add(corrugateGroupFieldSet);
        formPanel.add(main);
        vp.add(formPanel);
    }
    
    public void saveCorrugate(CorrugateDTO corrugateDTO,TabPanel panel, TabItem nextTab){
    	AppService.App.getInstance().saveCorrugateDTO(corrugateDTO, new Service.SaveCorrugateDTO(this,corrugateDTO,panel,  nextTab,corrugateInfoGroup));
    }

	
}
