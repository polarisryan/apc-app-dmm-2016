package com.averydennison.dmm.app.client;

/**
 * User: Spart Arguello
 * Date: Dec 9, 2009
 * Time: 2:00:48 PM
 */
public interface ValidationHelper {
    public boolean validateControls();
}
