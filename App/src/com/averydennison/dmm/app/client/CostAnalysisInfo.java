package com.averydennison.dmm.app.client;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import com.averydennison.dmm.app.client.models.BuCostCenterSplitDTO;
import com.averydennison.dmm.app.client.models.CalculationSummaryDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.averydennison.dmm.app.client.models.DisplayCostCommentDTO;
import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.models.ProgramBuDTO;
import com.averydennison.dmm.app.client.util.GeneralComponentBinding;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.Orientation;
import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.event.TabPanelEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.DateTimePropertyEditor;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.Radio;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.GridView;
import com.extjs.gxt.ui.client.widget.grid.RowEditor;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Timer;

/**
 * User: Mari
 * Date: June 17, 2010
 * Time: 10:06:22 AM
 */
public class CostAnalysisInfo extends LayoutContainer implements DirtyInterface {
	private FormData formData;
	private VerticalPanel vp;
	private Button btnSave;
	public static final String SAVE = "Save";
	public static final String RETURN_TO_SELECTION = "Return to Selection";
	public static final String RE_CALCULATE = "Re Calculate";
	public static final String ADD_NEW_COMMENTS = "Add Comments";
	public static final String CANCEL_COMMENTS = "Clear Comments";
	public static final String ADD_COMPONENT = "Add Component";
	public static final String EDIT_COMPONENT = "Edit Component";
	public static final String APPLY_FOR_APPROVAL = "&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;&nbsp;";
	public static final String YES_FLG="Y";
	public static final String NO_FLG="N";
	public static final String CREATE_SCENARIO = "Create Scenario";
	public static final String COPY_SCENARIO = "Copy Scenario";
	public static final String CANCEL_SCENARIO = "Cancel Scenario";
	public static final String SAVE_SCENARIO = "Save Scenario";
	public static final String VIEW_COMPONENT = "View Component";
	public static final String DELETE_COMPONENT = "Delete Component";
	public static final String SAVE_COMPONENTS = "Save Components";
	public static final String SAVE_DISPLAY = "Save Display";
	public static final String SAVE_COMMENTS = "Save Comments";
	public static final String DISPLAY_COST_MODEL="displayCostModel";
	public static final String DISPLAY_COST_MODELS="displayCostModels";
	public static final String DISPLAY_COST_COMMENT_MODEL="displayCostCommentModel";
	public static final String DISPLAY_COST_SCENARIO_MODEL="displayScenarioModel";
	public static final int NUMBER_ZERO=0;
	public static final int NUMBER_ONE=1;
	public static final int NUMBER_TWO=2;
	public static final int NUMBER_THREE=3;
	public static final int NUMBER_FOUR=4;
	public static final int NUMBER_FIVE=5;
	public static final int NUMBER_SIX=6;
	public static final int NUMBER_SEVEN=7;
	public static final Long STATUS_APPROVED=2L;
	public DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
	private DisplayDTO display;
	public DisplayScenarioDTO displayScenarioModel;
	private DisplayTabs displayTabs;
	public List<DisplayCostDTO> displayCostModels;
	private DisplayCostDTO displayCostModel;
	private DisplayCostCommentDTO displayCostCommentModel;
	private Boolean readOnly;
	private ChangeListener displayListener;
	private ChangeListener displayCostModelListener;
	private ChangeListener displayScenarioModelListener;
	public static final String datePattern = "MM/dd/yyyy";
	public static final String currencyPattern="$#,###,###0.00";
	private ListStore<DisplayCostCommentDTO> displayCostCommentStore = new ListStore<DisplayCostCommentDTO>();
	private ListStore<DisplayCostCommentDTO> compDisplayCostCommentStore ;
	
	public DateField reCalculateDate;
	private Vector<CheckBox>  lstSkuMixFinalCheckBox = new Vector<CheckBox>();
	private Vector<Button> lstBtnSaveScenario = new Vector<Button>();
	private Vector<Button> lstBtnCancelScenario = new Vector<Button>();
	private Vector<Button> lstBtnCreateScenario= new Vector<Button>();
	private Vector<Button> lstBtnCopyScenario= new Vector<Button>();
	private Vector<Button> lstBtnAdd= new Vector<Button>();
	private Vector<Button> lstBtnEdit= new Vector<Button>();
	private Vector<Button> lstBtnDelete= new Vector<Button>();
	private Vector<Button> lstBtnReCalculate= new Vector<Button>();
	private Vector<Button> lstBtnCancelComments= new Vector<Button>();
	private Vector<Button> lstBtnSaveComments= new Vector<Button>();
	private CheckBox  skuMixFinalCheckBox;
	private Button btnSaveScenario;
	private Button btnCancelScenario;
	private Button btnCreateScenario;
	private Button btnCopyScenario;
	private Button btnAdd;
	private Button btnEdit;
	private Button btnDelete;
	private Button btnReCalculate;
	private Button btnCancelComments;
	private Button btnSaveComments;
	
	private Long ZERO_LONG=new Long(0);
	Double currYear=(System.currentTimeMillis()/1000/3600/24/365.25 +1970);
	public static final String SCENARIO_ONE="Scenario 1";
	public static final String SCENARIO_TWO="Scenario 2";
	public static final String SCENARIO_THREE="Scenario 3";
	public static final String SCENARIO_FOUR="Scenario 4";
	public static final String SCENARIO_FIVE="Scenario 5";
	public static final String SCENARIO_COMPARISON="Comparison";
	public static final String EST_PROGRAM = "Est. Program %";
	private TabItem scenarioTabOne ;
	private TabItem scenarioTabTwo ;
	private TabItem scenarioTabThree ;
	private TabItem scenarioTabFour ;
	private TabItem scenarioTabFive ;
	public TabItem scenarioTabComparison ;
	public TabItem estProgramTab;
	private VerticalPanel vpScenarioPanel;
	public TabPanel vpScenarioPanelTabs;
	private List<DisplayScenarioDTO> displayScenarioDTOs=null;
	private CalculationSummaryDTO calculationSummaryDTO=null;
	private Grid<ProgramBuDTO> programGrid;
	
	public CostAnalysisInfo() {
		formData = new FormData("95%");
		vp = new VerticalPanel();
		vp.setSpacing(5);
		vpScenarioPanelTabs=new TabPanel();
		vpScenarioPanelTabs.setResizeTabs(false);
		vpScenarioPanelTabs.setSize(1600, 1400);
	}
	private void setPermissions(){
		/*if(!App.isAuthorized("CostAnalysisInfo.SAVE")) btnSave.disable();*/
	}

	@Override
	protected void onRender(Element parent, int index) {
		super.onRender(parent, index);
		System.out.println("CostAnalysisInfo:onRender starts");
		setModelsAndView();
		createHeader();
		displayScenarioDTOs=getCostAnalysisInfo().getData("displayScenarioDTOs");
		displayCostModels = getCostAnalysisInfo().getData(DISPLAY_COST_MODELS);
		if (displayCostModels == null) {
			displayCostModels = new Vector<DisplayCostDTO>();
		}
		if(displayScenarioDTOs!=null && displayScenarioDTOs.size()>NUMBER_ZERO){
			for( DisplayScenarioDTO selecteddisplayScenarioDTO:displayScenarioDTOs  ){
				if(selecteddisplayScenarioDTO.getScenarioNum()==1 && selecteddisplayScenarioDTO.getDisplayScenarioId()==null ){
					createDefaultForm(selecteddisplayScenarioDTO);
					DisplayCostDTO copiedDisplayCostDTO =	new DisplayCostDTO();
					getCostAnalysisInfo().setData("copiedDisplayCost",copiedDisplayCostDTO );
				}else if(selecteddisplayScenarioDTO.getScenarioNum()==1 && selecteddisplayScenarioDTO.getDisplayScenarioId()!=null ){
					createDefaultForm(selecteddisplayScenarioDTO);
				}else{
					createAddtionalForm(selecteddisplayScenarioDTO);
				}
			}
		}else{
			displayScenarioDTOs= new Vector<DisplayScenarioDTO>();
			DisplayCostDTO copiedDisplayCostDTO =	new DisplayCostDTO();
			getCostAnalysisInfo().setData("copiedDisplayCost",copiedDisplayCostDTO );
			displayScenarioDTOs.add(getDefaultScenarioData());
			createDefaultForm(displayScenarioDTOs.get(0));
		}
		createComparisonForm(scenarioTabComparison);
		createProgramForm(estProgramTab);
		setPermissions();
		setReadOnly();
		add(vp);
	}

	public boolean validate(TabItem tabItem){
		DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
		if(tabItem.getTabIndex() != vpScenarioPanelTabs.getItem(displayScenarioDTOList.size()-1).getTabIndex() ){
			if(copiedDisplayCostDTO !=null ){
				MessageBox.alert("Validation Error", "Un saved scenario exists. Please save the scenario", null);
				return false;
			}
		}
		return true;
	}

	public DisplayScenarioDTO getDefaultScenarioData( ){
		System.out.println("getDefaultScenarioData() starts");
		this.display = displayTabs.getDisplayModel();
		DisplayScenarioDTO dScenarioDTO = new DisplayScenarioDTO();
		dScenarioDTO.setScenarioNum(new Long(1));
		dScenarioDTO.setActualQty(this.display.getActualQty());
		dScenarioDTO.setDisplayId(this.display.getDisplayId());
		dScenarioDTO.setDisplaySalesRepId(this.display.getDisplaySalesRepId());
		dScenarioDTO.setManufacturerId(this.display.getManufacturerId());
		dScenarioDTO.setPackoutVendorId(this.display.getPackoutVendorId());
		dScenarioDTO.setPromoPeriodId(this.display.getPromoPeriodId());
		dScenarioDTO.setPromoYear(this.display.getPromoYear());
		dScenarioDTO.setQtyForecast(this.display.getQtyForecast());
		dScenarioDTO.setSku(this.display.getSku());
		dScenarioDTO.setScenariodStatusId(this.display.getStatusId());
		dScenarioDTO.setDtCreated(new Date());
		dScenarioDTO.setDisplayScenarioId(null);;
		dScenarioDTO.setScenarioApprovalFlg(NO_FLG);
		dScenarioDTO.setUserCreated(App.getUser().getUsername());
		System.out.println("getDefaultScenarioData() ends");
		return dScenarioDTO;
	}

	private TabItem nextTab;

	private TabItem getSelectedTab() {
		return vpScenarioPanelTabs.getSelectedItem();
	}

	public TabItem getNextTab() {
		return nextTab;
	}

	public void setNextTab(TabItem nextTab) {
		this.nextTab = nextTab;
	}

	public void createDefaultForm(DisplayScenarioDTO displayScenarioDTO){
		vpScenarioPanel = new VerticalPanel();
		vpScenarioPanel.setSpacing(5);
		scenarioTabOne = new TabItem(SCENARIO_ONE);
		scenarioTabTwo = new TabItem(SCENARIO_TWO);
		scenarioTabThree = new TabItem(SCENARIO_THREE);
		scenarioTabFour = new TabItem(SCENARIO_FOUR);
		scenarioTabFive = new TabItem(SCENARIO_FIVE);
		scenarioTabComparison = new TabItem(SCENARIO_COMPARISON);
		estProgramTab = new TabItem(EST_PROGRAM);
		scenarioTabOne.setTabIndex(0);
		scenarioTabTwo.setTabIndex(1);
		scenarioTabThree.setTabIndex(2);
		scenarioTabFour.setTabIndex(3);
		scenarioTabFive.setTabIndex(4);
		scenarioTabComparison.setTabIndex(5);
		estProgramTab.setTabIndex(6);
		displayScenarioDTOList.add(displayScenarioDTO );
		createForm(scenarioTabOne,displayScenarioDTO);
		scenarioTabOne.addListener(Events.BeforeSelect, new Listener<ComponentEvent>() {
			public void handleEvent(ComponentEvent be) {
				if(!validate(scenarioTabOne)){
					be.setCancelled(true);
					return;
				}
				final MessageBox box = MessageBox.wait("Progress",
						"Loading Scenario " + scenarioTabOne.getText()+ ", please wait...", "Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}};
					t.schedule(500);
					displayCostModels = getCostAnalysisInfo().getData(DISPLAY_COST_MODELS);
					if (displayCostModels == null) {
						displayCostModels = new Vector<DisplayCostDTO>();
					}
					if(displayCostModels.size()>0 && displayCostModels.get(scenarioTabOne.getTabIndex())!=null){
						updateView(displayCostModels.get(scenarioTabOne.getTabIndex()),true,displayScenarioDTOList.get(scenarioTabOne.getTabIndex()));
					}else if(displayCostModels.size()==0 ){
						reloadcostAnalysisInfoData(getDefaultScenarioData());
					}
			}
		});
		scenarioTabTwo.addListener(Events.BeforeSelect, new Listener<ComponentEvent>() {
			public void handleEvent(ComponentEvent be) {
				if(!validate(scenarioTabTwo)){
					be.setCancelled(true);
					return;
				}
				final MessageBox box = MessageBox.wait("Progress",
						"Loading Scenario " + scenarioTabTwo.getText()+ ", please wait...", "Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}};
					t.schedule(500);
					displayCostModels = getCostAnalysisInfo().getData(DISPLAY_COST_MODELS);
					if (displayCostModels == null) {
						displayCostModels = new Vector<DisplayCostDTO>();
					}
					if(displayCostModels.size()>NUMBER_ONE && displayCostModels.get(scenarioTabTwo.getTabIndex())!=null){
						updateView(displayCostModels.get(scenarioTabTwo.getTabIndex()),true,displayScenarioDTOList.get(scenarioTabTwo.getTabIndex()));
					}
			}
		});
		scenarioTabThree.addListener(Events.BeforeSelect, new Listener<ComponentEvent>() {
			public void handleEvent(ComponentEvent be) {
				if(!validate(scenarioTabThree)){
					be.setCancelled(true);
					return;
				}
				final MessageBox box = MessageBox.wait("Progress",
						"Loading Scenario " + scenarioTabThree.getText()+ ", please wait...", "Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}};
					t.schedule(500);
					displayCostModels = getCostAnalysisInfo().getData(DISPLAY_COST_MODELS);
					if (displayCostModels == null) {
						displayCostModels = new Vector<DisplayCostDTO>();
					}
					if(displayCostModels.size()>NUMBER_TWO && displayCostModels.get(scenarioTabThree.getTabIndex())!=null){
						updateView(displayCostModels.get(scenarioTabThree.getTabIndex()),true,displayScenarioDTOList.get(scenarioTabThree.getTabIndex()));
					}
			}
		});
		scenarioTabFour.addListener(Events.BeforeSelect, new Listener<ComponentEvent>() {
			public void handleEvent(ComponentEvent be) {
				if(!validate(scenarioTabFour)){
					be.setCancelled(true);
					return;
				}
				final MessageBox box = MessageBox.wait("Progress",
						"Loading Scenario " + scenarioTabFour.getText()+ ", please wait...", "Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}};
					t.schedule(500);
					displayCostModels = getCostAnalysisInfo().getData(DISPLAY_COST_MODELS);
					if (displayCostModels == null) {
						displayCostModels = new Vector<DisplayCostDTO>();
					}
					if(displayCostModels.size()>NUMBER_THREE && displayCostModels.get(scenarioTabFour.getTabIndex())!=null){
						updateView(displayCostModels.get(scenarioTabFour.getTabIndex()),true,displayScenarioDTOList.get(scenarioTabFour.getTabIndex()));
					}
			}
		});
		scenarioTabFive.addListener(Events.BeforeSelect, new Listener<ComponentEvent>() {
			public void handleEvent(ComponentEvent be) {
				if(!validate(scenarioTabFive)){
					be.setCancelled(true);
					return;
				}
				final MessageBox box = MessageBox.wait("Progress",
						"Loading Scenario " + scenarioTabFive.getText()+ ", please wait...", "Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}};
					t.schedule(500);
					displayCostModels = getCostAnalysisInfo().getData(DISPLAY_COST_MODELS);
					if (displayCostModels == null) {
						displayCostModels = new Vector<DisplayCostDTO>();
					}
					if(displayCostModels.size()>NUMBER_FOUR && displayCostModels.get(scenarioTabFive.getTabIndex())!=null){
						updateView(displayCostModels.get(scenarioTabFive.getTabIndex()),true,displayScenarioDTOList.get(scenarioTabFive.getTabIndex()));
					}
			}
		});
		scenarioTabComparison.addListener(Events.BeforeSelect, new Listener<ComponentEvent>() {
			public void handleEvent(ComponentEvent be) {
				DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
				if(copiedDisplayCostDTO !=null ){
					MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
					be.setCancelled(true);
					return;
				}
				final MessageBox box = MessageBox.wait("Progress",
						"Loading Scenario " + scenarioTabComparison.getText()+ ", please wait...", "Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}};
					displayCostModels = getCostAnalysisInfo().getData(DISPLAY_COST_MODELS);
					if (displayCostModels == null) {
						displayCostModels = new Vector<DisplayCostDTO>();
					}
					displayScenarioDTOs=getCostAnalysisInfo().getData("displayScenarioDTOs");
					if(displayScenarioDTOs!=null && displayScenarioDTOs.size()>NUMBER_ZERO){
						int tabIndex=-1;
						for( DisplayScenarioDTO selecteddisplayScenarioDTO:displayScenarioDTOs  ){
							tabIndex=selecteddisplayScenarioDTO.getScenarioNum().intValue()-1;
							if(tabIndex==NUMBER_ZERO){
								compScenarioOne.setVisible(true);
							}else if(tabIndex==NUMBER_ONE){
								compScenarioTwo.setVisible(true);
							}else if(tabIndex==NUMBER_TWO){
								compScenarioThree.setVisible(true);
							}else if(tabIndex==NUMBER_THREE){
								compScenarioFour.setVisible(true);
							}else if(tabIndex==NUMBER_FOUR){
								compScenarioFive.setVisible(true);
							}
							displayCostCompareGroupList.get(tabIndex).setCostAnalysisInfo(displayCostDataGroupList.get(tabIndex).getCostAnalysisInfo());
							updateComparisonView(displayCostCompareGroupList.get(tabIndex), displayCostModels.get(tabIndex),true,selecteddisplayScenarioDTO);
						}
					}
					t.schedule(500);
			}
		});
		estProgramTab.addListener(Events.BeforeSelect, new Listener<ComponentEvent>() {
			public void handleEvent(ComponentEvent be) {
				final MessageBox box = MessageBox.wait("Progress",
						"Loading est. program %, please wait...", "Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}};
					
				t.schedule(500);
				AppService.App.getInstance().loadProgramBus(displayTabs.getDisplayModel().getDisplayId(), new Service.LoadProgramBu(programGrid.getStore()));
			}
		});		
		vpScenarioPanelTabs.setSelection(scenarioTabOne);
		vpScenarioPanel.add(vpScenarioPanelTabs);
		vp.add(vpScenarioPanel);
	}

	public void reloadScenarioTab(int scenarioNum){
		System.out.println("CostAnalysisInfo: start");
		if(displayCostModels.size()>NUMBER_ZERO && displayCostModels.get(scenarioNum-1)!=null){
			updateView(displayCostModels.get(scenarioNum-1),true,displayScenarioDTOList.get(scenarioNum-1));
		}
	}
	
	public DisplayScenarioDTO getDisplayScenarioModel() {
		return displayScenarioModel;
	}
	
	public void setDisplayScenarioModel(DisplayScenarioDTO displayScenarioModel) {
		this.displayScenarioModel = displayScenarioModel;
	}
	
	private TabItem addlScenarioTabItem=null;
	public void createAddtionalForm(DisplayScenarioDTO displayScenarioDTO){
		System.out.println("CreateAdditionalForm starts");
		if(displayScenarioDTO.getScenarioNum()==NUMBER_TWO){
			addlScenarioTabItem=scenarioTabTwo;
		}else if(displayScenarioDTO.getScenarioNum()==NUMBER_THREE){
			addlScenarioTabItem=scenarioTabThree;
		}else if(displayScenarioDTO.getScenarioNum()==NUMBER_FOUR){
			addlScenarioTabItem=scenarioTabFour;
		}else if(displayScenarioDTO.getScenarioNum()==NUMBER_FIVE){
			addlScenarioTabItem=scenarioTabFive;
		}
		createForm(addlScenarioTabItem,displayScenarioDTO);
		vpScenarioPanelTabs.setSelection(addlScenarioTabItem);
		displayScenarioDTOList.add(displayScenarioDTO );
	}
	
	public  void setDisplayCostModel(DisplayCostDTO displayCostModel){
		this.displayCostModel=displayCostModel;
	}
	
	public DisplayCostDTO getDisplayCostModel(){
		return displayCostModel;
	}
	
	public void setModelsAndView() {
		display = displayTabs.getDisplayModel();
		if (display == null) {
			display = new DisplayDTO();
		}
		displayCostModel = this.getData(DISPLAY_COST_MODEL);
		if (displayCostModel == null) {
			displayCostModel = new DisplayCostDTO();
		}
		displayCostModels = this.getData(DISPLAY_COST_MODELS);
		if (displayCostModels == null) {
			displayCostModels = new Vector<DisplayCostDTO>();
		}
		displayCostCommentModel = this.getData(DISPLAY_COST_COMMENT_MODEL);
		if (displayCostCommentModel == null) {
			displayCostCommentModel = new DisplayCostCommentDTO();
		}
		displayScenarioModel = this.getData(DISPLAY_COST_SCENARIO_MODEL);
		if (displayScenarioModel == null) {
			displayScenarioModel = new DisplayScenarioDTO();
		}
		vpScenarioPanelTabs.addListener(Events.BeforeSelect, new Listener<TabPanelEvent>() {
			public void handleEvent(TabPanelEvent tpe) {
				if (tpe.getItem() != null)
					setNextTab(tpe.getItem());
			}
		});
		readOnly = this.getData("readOnly");
		if (readOnly == null) readOnly = false;
		displayListener = new ChangeListener() {
			public void modelChanged(ChangeEvent event) {
				System.out.println("++++CostAnalysisInfo:displayListener FIRED++++");
				display = (DisplayDTO) event.getSource();
				if (display != null && display.getDisplayId() != null) {
					displayTabs.setDisplayModel(display);
					displayTabs.setData("display", display);
					if (display != null && display.getDisplayId() != null) {
						displayTabHeader.setDisplay(display);
					}
					displayCostModel=getCostAnalysisInfo().getData(DISPLAY_COST_MODEL);
					if (displayCostModel != null && displayCostModel.getDisplayCostId() != null && vpScenarioPanelTabs!=null){
						if(!(vpScenarioPanelTabs.getSelectedItem().equals(scenarioTabComparison) && vpScenarioPanelTabs.getSelectedItem().equals(estProgramTab)) ){
							updateView((displayCostModels.size()>0?displayCostModels.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()):null),false,displayScenarioDTOList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()) );
						}
					}else{
						if (display != null && display.getDisplayId() != null) {
							if(displayScenarioDTOList!=null) {
								if(displayScenarioDTOList.size()>0 && vpScenarioPanelTabs!=null && vpScenarioPanelTabs.getItemCount()>0 ){
									//if(!vpScenarioPanelTabs.getSelectedItem().equals(scenarioTabComparison)){
									if(!(vpScenarioPanelTabs.getSelectedItem().equals(scenarioTabComparison) && vpScenarioPanelTabs.getSelectedItem().equals(estProgramTab)) ){
										reloadcostAnalysisInfoData(displayScenarioDTOList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()) );
									}
								}
							}
						}
					}
				}
			}
		};
		display.addChangeListener(displayListener);
		displayCostModelListener = new ChangeListener() {
			public void modelChanged(com.extjs.gxt.ui.client.data.ChangeEvent event) {
				System.out.println("++++CostAnalysisInfo:displayCostListener FIRED++++");
				//MessageBox.alert("displayCostModelListener", "++++CostAnalysisInfo:displayCostListener FIRED++++", null);
				displayCostModel = (DisplayCostDTO) event.getSource();
				if (displayCostModel != null && displayCostModel.getDisplayScenarioId() != null){
					System.out.println("CostAnalysisInfo:displayCostListener FIRED ..1.. "+ displayCostModel.getDisplayScenarioId());
					if(!vpScenarioPanelTabs.getSelectedItem().equals(scenarioTabComparison)){
						updateView(displayCostModel,true,displayScenarioDTOList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()) );
					}
					getCostAnalysisInfo().setData(DISPLAY_COST_MODEL,displayCostModel );
				}else{
					if (display != null && display.getDisplayId() != null) {
						if(!vpScenarioPanelTabs.getSelectedItem().equals(scenarioTabComparison)){
							reloadcostAnalysisInfoData(displayScenarioDTOList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()) );
						}
					}
				}
			}
		};
		if(displayCostModel!=null) displayCostModel.addChangeListener(displayCostModelListener);
		displayScenarioModelListener = new ChangeListener() {
			public void modelChanged(com.extjs.gxt.ui.client.data.ChangeEvent event) {
				System.out.println("++++CostAnalysisInfo:displayScenarioListener FIRED++++");
				//MessageBox.alert("DisplayScenarioListener", "++++CostAnalysisInfo:displayScenarioListener FIRED++++", null);
				displayScenarioModel = (DisplayScenarioDTO) event.getSource();
				displayCostModels=getCostAnalysisInfo().getData(DISPLAY_COST_MODELS );
				if (displayScenarioModel != null && displayScenarioModel.getDisplayScenarioId() != null){
					System.out.println("CostAnalysisInfo:displayScenarioModel FIRED ..1.. "+ displayScenarioModel.getDisplayScenarioId());
					if(!vpScenarioPanelTabs.getSelectedItem().equals(scenarioTabComparison)){
						reloadcostAnalysisInfoData(displayScenarioModel );
						reloadcostAnalysisInfoComparisonData(displayScenarioModel);
					}
				}else{
					if (display != null && display.getDisplayId() != null) {
						if(!vpScenarioPanelTabs.getSelectedItem().equals(scenarioTabComparison)){
							reloadcostAnalysisInfoData(displayScenarioDTOList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()) );
						}
					}
				}
			}
		};
		if(displayScenarioModel!=null) displayScenarioModel.addChangeListener(displayScenarioModelListener);
		System.out.println("setModelsAndView ends");
	}
	public void reloadcostAnalysisInfoData(DisplayScenarioDTO displayScenarioDTO){
		System.out.println("CostAnalysisInfo:reloadcostAnalysisInfoData starts");
		int selectedIndex=displayScenarioDTO.getScenarioNum().intValue()-1;
		displayCostDataGroupList.get(selectedIndex).setDisplayScenarioDTO(displayScenarioDTO);
		costAnalysisInfoGroupList.get(selectedIndex).topLevel.setValue(displayScenarioDTO.getSku());
		costAnalysisInfoGroupList.get(selectedIndex).status.setValue(Service.getStatusMap().get(displayScenarioDTO.getScenariodStatusId()));
		costAnalysisInfoGroupList.get(selectedIndex).projectManager.setValue(Service.getProjectManagerMap().get(displayScenarioDTO.getDisplaySalesRepId()));
		costAnalysisInfoGroupList.get(selectedIndex).manufacturer.setValue(Service.getManufacturerCountryMap().get(displayScenarioDTO.getManufacturerId()));
		costAnalysisInfoGroupList.get(selectedIndex).promoPeriod.setValue(Service.getPromoPeriodCountryMap().get(displayScenarioDTO.getPromoPeriodId()));
		costAnalysisInfoGroupList.get(selectedIndex).infoDetailsYear.setValue(displayScenarioDTO.getPromoYear()==null ?currYear.longValue(): displayScenarioDTO.getPromoYear());
		costAnalysisInfoGroupList.get(selectedIndex).packLocation.setValue(Service.getLocationCountryMap().get(displayScenarioDTO.getPackoutVendorId()));
		displayCostDataGroupList.get(selectedIndex).foreCastQty.setValue(displayScenarioDTO.getQtyForecast());
		displayCostDataGroupList.get(selectedIndex).actualQty.setValue(displayScenarioDTO.getActualQty());
		System.out.println("CostAnalysisInfo:reloadcostAnalysisInfoData ends");
	}

	public void reloadcostAnalysisInfoComparisonData(DisplayScenarioDTO displayScenarioDTO){
		int selectedIndex=displayScenarioDTO.getScenarioNum().intValue()-1;
		if(displayScenarioDTO.getManufacturerId()!=null) costAnalysisInfoCompareGroupList.get(selectedIndex).vendorManufacturer.setValue(Service.getManufacturerCountryMap().get(displayScenarioDTO.getManufacturerId()).getName());
		if(displayScenarioDTO.getPackoutVendorId()!=null) costAnalysisInfoCompareGroupList.get(selectedIndex).vendorPackVendor.setValue(Service.getLocationCountryMap().get(displayScenarioDTO.getPackoutVendorId()).getLocationName());
	}
	
	public void updateView(DisplayCostDTO displayCostModel, boolean isDisplayCostUpdate, DisplayScenarioDTO displayScenarioDTO) {
		System.out.println("CostAnalysisInfo:updateView starts");
		int scenarioTabIdx=displayScenarioDTO.getScenarioNum().intValue()-1;
		this.setData(DISPLAY_COST_MODEL, displayCostModel);
		setDisplayCostModel(displayCostModel);
		this.display = displayTabs.getDisplayModel();
		if (this.display.getDisplayId() != null) {
			displayTabHeader.setDisplay(this.display);
		}
		reloadcostAnalysisInfoData(displayScenarioDTO );
		double pctVal=0.0;
		/*
		 * Code added Date:11/18/2010
		 */
		/*if(displayScenarioDTO !=null && displayScenarioDTO.getScenarioApprovalFlg().equalsIgnoreCase(YES_FLG)){
			vpScenarioPanelTabs.getItem(scenarioTabIdx).setTextStyle("x-tab-strip-text-approval");
		}else{
			vpScenarioPanelTabs.getItem(scenarioTabIdx).setTextStyle("x-tab-strip-text");
		}*/
		displayCostDataGroupList.get(scenarioTabIdx).setDisplayScenarioDTO(displayScenarioDTO);
		displayCostDataGroupList.get(scenarioTabIdx).foreCastQty.setValue(displayScenarioDTO.getQtyForecast());
		displayCostDataGroupList.get(scenarioTabIdx).actualQty.setValue(displayScenarioDTO.getActualQty());
		if (displayCostModel != null && displayCostModel.getDisplayCostId()!=null ) {
			if (display != null && display.getDisplayId() != null) {
				displayCostModel.setDisplayId(this.display.getDisplayId());
			}
			if( isDisplayCostUpdate){
				if(displayCostModel.getOverageScrapPct()!=null ){
					displayCostDataGroupList.get(scenarioTabIdx).overageScrapNo.setValue(displayCostModel.getOverageScrapPct());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).overageScrapNo.setValue(null);
				}
				if(displayCostModel.getCustomerProgramPct()!=null ){
					displayCostDataGroupList.get(scenarioTabIdx).customerProgramPercentage.setValue(displayCostModel.getCustomerProgramPct());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).customerProgramPercentage.setValue(null);
				}
				if(displayCostModel.getCaDate()!=null){
					costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsDate.setValue(displayCostModel.getCaDate());
				}else{
					costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsDate.setValue(null);
				}
				if(this.display.getCmProjectNumber()!=null ){
					costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsProjectNo.setValue(this.display.getCmProjectNumber());
				}else{
					costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsProjectNo.setValue(null);
				}
				if(displayCostModel.getCorrugateCost()!=null && displayCostModel.getCorrugateCost() >0.00){
					displayCostDataGroupList.get(scenarioTabIdx).corrugateCostPerUnit.setValue(displayCostModel.getCorrugateCost());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).corrugateCostPerUnit.setValue(null);
				}
				if(displayCostModel.getFullfilmentCost()!=null && displayCostModel.getFullfilmentCost()>0.00 ){
					displayCostDataGroupList.get(scenarioTabIdx).fullfillmentCostPerUnit.setValue(displayCostModel.getFullfilmentCost());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).fullfillmentCostPerUnit.setValue(null);
				}
				if(displayCostModel.getMiscCost()!=null && displayCostModel.getMiscCost()>0.00 ){
					displayCostDataGroupList.get(scenarioTabIdx).miscCost.setValue(displayCostModel.getMiscCost());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).miscCost.setValue(null);
				}
				if(displayCostModel.getPalletCost()!=null && displayCostModel.getPalletCost()>0.00 ){
					displayCostDataGroupList.get(scenarioTabIdx).palletCost.setValue(displayCostModel.getPalletCost());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).palletCost.setValue(null);
				}
				if(displayCostModel.getShipTestCost()!=null && displayCostModel.getShipTestCost()>0.00){
					displayCostDataGroupList.get(scenarioTabIdx).shipTestCost.setValue(displayCostModel.getShipTestCost());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).shipTestCost.setValue(null);
				}
				if(displayCostModel.getTotalArtworkCost()!=null && displayCostModel.getTotalArtworkCost()>0.00){
					displayCostDataGroupList.get(scenarioTabIdx).totalArtworkCost.setValue(displayCostModel.getTotalArtworkCost());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).totalArtworkCost.setValue(null);
				}
				if(displayCostModel.getPrintingPlateCost()!=null && displayCostModel.getPrintingPlateCost()>0.00 ){
					displayCostDataGroupList.get(scenarioTabIdx).printingPlateCost.setValue(displayCostModel.getPrintingPlateCost());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).printingPlateCost.setValue(null);
				}
				if(displayCostModel.getDieCuttingCost()!=null && displayCostModel.getDieCuttingCost()>0.00 ){
					displayCostDataGroupList.get(scenarioTabIdx).dieCuttingCost.setValue(displayCostModel.getDieCuttingCost());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).dieCuttingCost.setValue(null);
				}
				if(displayCostModel.getCtpCost()!=null && displayCostModel.getCtpCost()>0.00 ){
					displayCostDataGroupList.get(scenarioTabIdx).cTPCost.setValue(displayCostModel.getCtpCost());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).cTPCost.setValue(null);
				}
				if(displayCostModel.getExpeditedFreightCost()!=null &&displayCostModel.getExpeditedFreightCost()>0.00){
					displayCostDataGroupList.get(scenarioTabIdx).expectedFrieghtCost.setValue(displayCostModel.getExpeditedFreightCost());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).expectedFrieghtCost.setValue(null);
				}
				if(displayCostModel.getFines()!=null && displayCostModel.getFines()>0.00 ){
					displayCostDataGroupList.get(scenarioTabIdx).fines.setValue(displayCostModel.getFines());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).fines.setValue(null);
				}
				if(displayCostModel.getMdfPct()!=null){
					displayCostDataGroupList.get(scenarioTabIdx).mdfPct.setValue(Double.parseDouble(displayCostDataGroupList.get(scenarioTabIdx).pctFormat.format(displayCostModel.getMdfPct())));
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).mdfPct.setValue(null);
				}
				if(displayCostModel.getDiscountPct()!=null ){
					displayCostDataGroupList.get(scenarioTabIdx).discountPct.setValue(Double.parseDouble(displayCostDataGroupList.get(scenarioTabIdx).pctFormat.format(displayCostModel.getDiscountPct())));
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).discountPct.setValue(null);
				}
				if(displayCostModel.getTradeSales()!=null && displayCostModel.getTradeSales()>0.00){
					displayCostDataGroupList.get(scenarioTabIdx).tradeSalesChanged.setValue(displayCostModel.getTradeSales());
				}else{
					displayCostDataGroupList.get(scenarioTabIdx).tradeSalesChanged.setValue(null);
				}
			}
			long totDisplaysOrdered=0;
			if( (displayCostModel.getOverageScrapPct()!=null && displayCostModel.getOverageScrapPct()>0) &&
					(displayScenarioDTO.getQtyForecast()!=null && displayScenarioDTO.getQtyForecast()>0)
			){
				totDisplaysOrdered=displayCostModel.getOverageScrapPct()+displayScenarioDTO.getQtyForecast();
			}else if(displayCostModel.getOverageScrapPct()!=null && displayCostModel.getOverageScrapPct()>0){
				totDisplaysOrdered=displayCostModel.getOverageScrapPct();
			}else if(displayScenarioDTO.getQtyForecast()!=null && displayScenarioDTO.getQtyForecast()>0){
				totDisplaysOrdered=displayScenarioDTO.getQtyForecast();
			}
			if(totDisplaysOrdered>0){
				displayCostDataGroupList.get(scenarioTabIdx).totalDisplaysOrdered.setValue(totDisplaysOrdered);
			}else{
				displayCostDataGroupList.get(scenarioTabIdx).totalDisplaysOrdered.setValue(null);
			}
			if( (displayScenarioDTO.getQtyForecast()!=null &&  displayScenarioDTO.getQtyForecast()>0) &&
					(displayCostModel.getOverageScrapPct()!=null && displayCostModel.getOverageScrapPct()>0)){
				pctVal=((displayCostModel.getOverageScrapPct()/displayScenarioDTO.getQtyForecast()) * 100 );
			}
			displayCostDataGroupList.get(scenarioTabIdx).overageScrapPct.setValue(pctVal);
		}else{
			System.out.println("CostAnalysisInfo:updateView displayCostModel doesn't exists");
		}
		/*
		 * Code added Date:05/04/2011
		 * update summary_calculation table data when the current scenario is already an approved one
		 */
		//displayCostDataGroupList.get(scenarioTabIdx).loadCostAnalysisComponentInfo(displayScenarioDTO,false);
		getCostAnalysisInfo().getDisplayTabHeader().setDisplay(this.display);
		System.out.println("CostAnalysisInfo:updateView ends");
		enableDisableButtons(this.display,scenarioTabIdx);
		displayCostCompareGroupList.get(scenarioTabIdx).setCostAnalysisInfo(displayCostDataGroupList.get(scenarioTabIdx).getCostAnalysisInfo());
		/*
		 * Added By Mari
		 * Date : 10/04/2012
		 */
		displayCostDataGroupList.get(scenarioTabIdx).setDisplayScenarioDTO(displayScenarioDTO);
		displayCostDataGroupList.get(scenarioTabIdx).calculateFinancialDataFields();
		//displayCostDataGroupList.get(scenarioTabIdx).loadCostAnalysisComponentInfo(displayScenarioDTO,false);
		//loadBUCostCenter(null,displayScenarioDTO);
		//updateComparisonView(displayCostCompareGroupList.get(scenarioTabIdx), displayCostModels.get(scenarioTabIdx),true,displayScenarioDTO);
		displayCostDataGroupList.get(scenarioTabIdx).loadCostAnalysisComponentInfo(displayScenarioDTO,false);
		updateComparisonView(displayCostCompareGroupList.get(scenarioTabIdx), displayCostModel,true,displayScenarioDTO);
	}

	public void updateComparisonView(DisplayCostDataGroup displayScenarioDataGroup, DisplayCostDTO displayCostModel, boolean isDisplayCostUpdate, DisplayScenarioDTO displayScenarioDTO) {
		System.out.println("CostAnalysisInfo:updateComparisonViewView starts");
		int scenarioTabIdx=displayScenarioDTO.getScenarioNum().intValue()-1;
		this.setData(DISPLAY_COST_MODEL, displayCostModel);
		setDisplayCostModel(displayCostModel);
		this.display = displayTabs.getDisplayModel();
		if (this.display.getDisplayId() != null) {
			displayTabHeader.setDisplay(this.display);
		}
		reloadcostAnalysisInfoData(displayScenarioDTO );
		reloadcostAnalysisInfoComparisonData(displayScenarioDTO);
		loadCompareComments(displayScenarioDTO);
		double pctVal=0.0;
		displayScenarioDataGroup.setCostAnalysisInfo(getCostAnalysisInfo());
		displayScenarioDataGroup.setDisplayDTO(this.display);
		displayScenarioDataGroup.setDisplayScenarioDTO(displayScenarioDTO);
		displayScenarioDataGroup.foreCastQty.setValue(displayScenarioDTO.getQtyForecast());
		displayScenarioDataGroup.actualQty.setValue(displayScenarioDTO.getActualQty());
		if (displayCostModel != null && displayCostModel.getDisplayCostId()!=null ) {
			if (display != null && display.getDisplayId() != null) {
				displayCostModel.setDisplayId(this.display.getDisplayId());
			}
			if( isDisplayCostUpdate){
				if(displayCostModel.getOverageScrapPct()!=null ){
					displayScenarioDataGroup.overageScrapNo.setValue(displayCostModel.getOverageScrapPct());
				}else{
					displayScenarioDataGroup.overageScrapNo.setValue(null);
				}
				if(displayCostModel.getCustomerProgramPct()!=null ){
					displayScenarioDataGroup.customerProgramPercentage.setValue(displayCostModel.getCustomerProgramPct());
				}else{
					displayScenarioDataGroup.customerProgramPercentage.setValue(null);
				}
				if(displayCostModel.getCaDate()!=null){
					costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsDate.setValue(displayCostModel.getCaDate());
				}else{
					costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsDate.setValue(null);
				}
				if(displayCostModel.getCorrugateCost()!=null && displayCostModel.getCorrugateCost() >0.00){
					displayScenarioDataGroup.corrugateCostPerUnit.setValue(displayCostModel.getCorrugateCost());
				}else{
					displayScenarioDataGroup.corrugateCostPerUnit.setValue(null);
				}
				if(displayCostModel.getFullfilmentCost()!=null && displayCostModel.getFullfilmentCost()>0.00 ){
					displayScenarioDataGroup.fullfillmentCostPerUnit.setValue(displayCostModel.getFullfilmentCost());
				}else{
					displayScenarioDataGroup.fullfillmentCostPerUnit.setValue(null);
				}
				if(displayCostModel.getMiscCost()!=null && displayCostModel.getMiscCost()>0.00 ){
					displayScenarioDataGroup.miscCost.setValue(displayCostModel.getMiscCost());
				}else{
					displayScenarioDataGroup.miscCost.setValue(null);
				}
				if(displayCostModel.getPalletCost()!=null && displayCostModel.getPalletCost()>0.00 ){
					displayScenarioDataGroup.palletCost.setValue(displayCostModel.getPalletCost());
				}else{
					displayScenarioDataGroup.palletCost.setValue(null);
				}
				if(displayCostModel.getShipTestCost()!=null && displayCostModel.getShipTestCost()>0.00){
					displayScenarioDataGroup.shipTestCost.setValue(displayCostModel.getShipTestCost());
				}else{
					displayScenarioDataGroup.shipTestCost.setValue(null);
				}
				if(displayCostModel.getTotalArtworkCost()!=null && displayCostModel.getTotalArtworkCost()>0.00){
					displayScenarioDataGroup.totalArtworkCost.setValue(displayCostModel.getTotalArtworkCost());
				}else{
					displayScenarioDataGroup.totalArtworkCost.setValue(null);
				}
				if(displayCostModel.getPrintingPlateCost()!=null && displayCostModel.getPrintingPlateCost()>0.00 ){
					displayScenarioDataGroup.printingPlateCost.setValue(displayCostModel.getPrintingPlateCost());
				}else{
					displayScenarioDataGroup.printingPlateCost.setValue(null);
				}
				if(displayCostModel.getDieCuttingCost()!=null && displayCostModel.getDieCuttingCost()>0.00 ){
					displayScenarioDataGroup.dieCuttingCost.setValue(displayCostModel.getDieCuttingCost());
				}else{
					displayScenarioDataGroup.dieCuttingCost.setValue(null);
				}
				if(displayCostModel.getCtpCost()!=null && displayCostModel.getCtpCost()>0.00 ){
					displayScenarioDataGroup.cTPCost.setValue(displayCostModel.getCtpCost());
				}else{
					displayScenarioDataGroup.cTPCost.setValue(null);
				}
				if(displayCostModel.getExpeditedFreightCost()!=null &&displayCostModel.getExpeditedFreightCost()>0.00){
					displayScenarioDataGroup.expectedFrieghtCost.setValue(displayCostModel.getExpeditedFreightCost());
				}else{
					displayScenarioDataGroup.expectedFrieghtCost.setValue(null);
				}
				if(displayCostModel.getFines()!=null && displayCostModel.getFines()>0.00 ){
					displayScenarioDataGroup.fines.setValue(displayCostModel.getFines());
				}else{
					displayScenarioDataGroup.fines.setValue(null);
				}
				
				
				if(displayCostModel.getMdfPct()!=null  ){
					displayScenarioDataGroup.mdfPct.setValue(Double.parseDouble((displayCostDataGroupList.get(scenarioTabIdx).pctFormat.format(displayCostModel.getMdfPct()))));
				}else{
					displayScenarioDataGroup.mdfPct.setValue(null);
				}
				
				if(displayCostModel.getDiscountPct()!=null  ){
					displayScenarioDataGroup.discountPct.setValue(Double.parseDouble((displayCostDataGroupList.get(scenarioTabIdx).pctFormat.format(displayCostModel.getDiscountPct()))));
				}else{
					displayScenarioDataGroup.discountPct.setValue(null);
				}
				
				// Joseph ********************************
				if(displayCostModel.getTradeSales()!=null && displayCostModel.getTradeSales() >0.00){
					displayScenarioDataGroup.tradeSalesChanged.setValue(displayCostModel.getTradeSales());
				}else{
					displayScenarioDataGroup.tradeSalesChanged.setValue(null);
				}
				// Joseph ********************************
			}
			long totDisplaysOrdered=0;
			if( (displayCostModel.getOverageScrapPct()!=null && displayCostModel.getOverageScrapPct()>0) &&
					(displayScenarioDTO.getQtyForecast()!=null && displayScenarioDTO.getQtyForecast()>0)
			){
				totDisplaysOrdered=displayCostModel.getOverageScrapPct()+displayScenarioDTO.getQtyForecast();
			}else if(displayCostModel.getOverageScrapPct()!=null && displayCostModel.getOverageScrapPct()>0){
				totDisplaysOrdered=displayCostModel.getOverageScrapPct();
			}else if(displayScenarioDTO.getQtyForecast()!=null && displayScenarioDTO.getQtyForecast()>0){
				totDisplaysOrdered=displayScenarioDTO.getQtyForecast();
			}
			if(totDisplaysOrdered>0){
				displayScenarioDataGroup.totalDisplaysOrdered.setValue(totDisplaysOrdered);
			}else{
				displayScenarioDataGroup.totalDisplaysOrdered.setValue(null);
			}
			if( (displayScenarioDTO.getQtyForecast()!=null &&  displayScenarioDTO.getQtyForecast()>0) &&
					(displayCostModel.getOverageScrapPct()!=null && displayCostModel.getOverageScrapPct()>0)){
				pctVal=((displayCostModel.getOverageScrapPct()/displayScenarioDTO.getQtyForecast()) * 100 );
			}
	
		}else{
			System.out.println("CostAnalysisInfo:updateComparisonView displayCostModel doesn't exists");
		}
		displayScenarioDataGroup.overageScrapPct.setValue(pctVal);
		displayScenarioDataGroup.foreCastQty.setEditable(false);
		displayScenarioDataGroup.actualQty.setEditable(false);
		displayScenarioDataGroup.overageScrapNo.setEditable(false);
		displayScenarioDataGroup.customerProgramPercentage.setEditable(false);
		displayScenarioDataGroup.corrugateCostPerUnit.setEditable(false);
		displayScenarioDataGroup.fullfillmentCostPerUnit.setEditable(false);
		displayScenarioDataGroup.miscCost.setEditable(false);
		displayScenarioDataGroup.palletCost.setEditable(false);
		displayScenarioDataGroup.shipTestCost.setEditable(false);
		displayScenarioDataGroup.totalArtworkCost.setEditable(false);
		displayScenarioDataGroup.printingPlateCost.setEditable(false);
		displayScenarioDataGroup.dieCuttingCost.setEditable(false);
		displayScenarioDataGroup.cTPCost.setEditable(false);
		displayScenarioDataGroup.expectedFrieghtCost.setEditable(false);
		displayScenarioDataGroup.fines.setEditable(false);
		displayScenarioDataGroup.totalDisplaysOrdered.setEditable(false);
		displayScenarioDataGroup.overageScrapPct.setEditable(false);
		displayScenarioDataGroup.loadCostAnalysisComponentInfo(displayScenarioDTO,true);
		Double dCalcTotalActualInvoiceWpgrm=0.00;
		dCalcTotalActualInvoiceWpgrm=calcTotalActualInvoiceWpgrm(scenarioTabIdx)*displayScenarioDataGroup.actualQtyVal ;
		displayScenarioDataGroup.custProgramDiscVal=displayScenarioDataGroup.tradeSalesVal-dCalcTotalActualInvoiceWpgrm;
		//displayScenarioDataGroup.custProgramDiscVal=dCalcTotalActualInvoiceWpgrm;
		/*
		 * By Mari
		 * Date 08/05/2014
		 * New Formula for Net Sales fields
		 * Net Sales = Trade Sales - Trade Program Discount - (Trade Sales * MDF%) - (Trade Sales * Discount%)
		 * productVarMarginVal has been changed into Customer Variable Margin
		 */
		displayScenarioDataGroup.lblCustProgramDisc.setValue(NumberFormat.getFormat((currencyFormat)).format(displayScenarioDataGroup.custProgramDiscVal));
		displayScenarioDataGroup.mdfPctVal = (Double) (displayScenarioDataGroup.mdfPct.getValue()==null?0:(displayScenarioDataGroup.mdfPct.getValue().doubleValue() ) /  displayScenarioDataGroup.percentMaxValue);
		displayScenarioDataGroup.discountPctVal = (Double) (displayScenarioDataGroup.discountPct.getValue()==null?0:(displayScenarioDataGroup.discountPct.getValue().doubleValue() /  displayScenarioDataGroup.percentMaxValue));
		
		displayScenarioDataGroup.netSalesVal=(displayScenarioDataGroup.tradeSalesVal - (displayScenarioDataGroup.custProgramDiscVal + (displayScenarioDataGroup.tradeSalesVal*displayScenarioDataGroup.mdfPctVal)+(displayScenarioDataGroup.tradeSalesVal*displayScenarioDataGroup.discountPctVal)));
		displayScenarioDataGroup.lblMdfDollor.setValue(NumberFormat.getFormat((currencyFormat)).format(displayScenarioDataGroup.tradeSalesVal*displayScenarioDataGroup.mdfPctVal));
		displayScenarioDataGroup.lblDiscountDollor.setValue(NumberFormat.getFormat((currencyFormat)).format(displayScenarioDataGroup.tradeSalesVal*displayScenarioDataGroup.discountPctVal));
		displayScenarioDataGroup.lblNetSales.setValue(NumberFormat.getFormat((currencyFormat)).format(displayScenarioDataGroup.netSalesVal));
		System.out.println("CostAnalysisInfo:updateComparisonViewView ends");
	}

	final String currencyFormat = "$###,###,##0.00";
	public Double calcTotalActualInvoiceWpgrm(int scenarioTabIdx){
		Double dtotalActualInvoiceWpgrm=0.00;
		List<CostAnalysisKitComponentDTO> objCostAnalysisKitComponentDTO = displayCostCompareGroupList.get(scenarioTabIdx).costAnalysisKitComponentStore.getModels();
		for(CostAnalysisKitComponentDTO r : objCostAnalysisKitComponentDTO) {
			if(r.getTotalActualInvoiceWpgrm()!=null) {
				dtotalActualInvoiceWpgrm+=r.getTotalActualInvoiceWpgrm();
			}
		}
		return dtotalActualInvoiceWpgrm;
	}
	
	private void createHeader() {
		add(displayTabHeader);
	}
	private void setReadOnly() {
		  readOnly = this.getData("readOnly");
	      if (readOnly == null) readOnly = false;
	}
	public ContentPanel cpDataEntry ;
	public LayoutContainer createDataEntry(TabItem scenarioTab, DisplayScenarioDTO displayScenarioDTO){
		System.out.println("CostAnalysisInfo:createDataEntry starts");
		setLayout(new FlowLayout(3));
		LayoutContainer topLeft = new LayoutContainer();
		FormLayout topLeftFormLayout = new FormLayout();
		topLeftFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		topLeft.setStyleAttribute("paddingRight", "2px");
		topLeft.setLayout(topLeftFormLayout);
		FieldSet costAnalysisInfoFldSet = new FieldSet();
		FormLayout productionSkuFormLayout = new FormLayout();
		productionSkuFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		costAnalysisInfoFldSet.setLayout(productionSkuFormLayout);
		costAnalysisInfoFldSet.setHeading("Cost Analysis Info");
		costAnalysisInfoFldSet.setCollapsible(false);
		topLeft.add(costAnalysisInfoFldSet);
		cpDataEntry = new ContentPanel();
		cpDataEntry=costAnalysisInfoGroupList.get(scenarioTab.getTabIndex()).createGroup((DisplayDTO) getCostAnalysisInfo().getDisplayTabs().getData("display"),displayScenarioDTO);
		costAnalysisInfoFldSet.add(cpDataEntry);	
		System.out.println("CostAnalysisInfo:createDataEntry ends");
		return topLeft;
	}
	private TextArea comments;
	public Grid<DisplayCostCommentDTO> displayCommentGrid;
	public Grid<DisplayCostCommentDTO> compDisplayCommentGrid;
	public void loadComments( DisplayScenarioDTO displayScenarioDTO){
		try{
			System.out.println("CostAnalysisInfo:loadComments starts");
			int tabSecnarioIdx =vpScenarioPanelTabs.getSelectedItem().getTabIndex();
			if(!vpScenarioPanelTabs.getSelectedItem().equals(scenarioTabComparison)  ){
				if(commentsList.get(tabSecnarioIdx)!=null ){
					if(commentsList.get(tabSecnarioIdx)!=null && commentsList.get(tabSecnarioIdx).getValue()!=null && commentsList.get(tabSecnarioIdx).getValue().length() >0) {
						commentsList.get(tabSecnarioIdx).clear();
					}
					/*
					 * Description: reload comments after adding kit component
					 * Date       : 01-06-2011   
					 */
					displayScenarioDTOs=getCostAnalysisInfo().getData("displayScenarioDTOs");
					AppService.App.getInstance().getDisplayCostCommentDTO(displayScenarioDTOs.get(tabSecnarioIdx).getDisplayScenarioId(),new Service.ReloadDisplayCostComment(displayCostCommentStoreList.get(tabSecnarioIdx),this,tabSecnarioIdx));
				}
			}
			System.out.println("CostAnalysisInfo:loadComments ends");
		}catch(Exception ex){
			System.out.println("CostAnalysisInfo:loadComments error "+ ex.toString());
		}
	}
	public void loadCompareComments( DisplayScenarioDTO displayScenarioDTO){
		try{
			System.out.println("CostAnalysisInfo:loadCompareComments starts");
			if(displayScenarioDTO!=null && displayScenarioDTO.getDisplayScenarioId()!=null){
					int tabSecnarioIdx =displayScenarioDTO.getScenarioNum().intValue()-1;
					System.out.println("CostAnalyisInfo.loadCompareComments for scenarioId="+displayScenarioDTO.getDisplayScenarioId());
					AppService.App.getInstance().getDisplayCostCommentDTO(displayScenarioDTO.getDisplayScenarioId(),new Service.ReloadCompareDisplayCostComment(compDisplayCostCommentStoreList.get(tabSecnarioIdx),this,tabSecnarioIdx));
			}
			System.out.println("CostAnalyisInfo.loadCompareComments ends");
		}catch(Exception ex){
			System.out.println("CostAnalyisInfo.loadCompareComments error "+ ex.toString());
		}
	}
	
	public Vector<CostAnalysisInfoBUCostCtrSplitGrid> costAnalysisInfoBUCostCtrSplitGridList = new Vector<CostAnalysisInfoBUCostCtrSplitGrid>();
	public Vector<DisplayScenarioDTO> displayScenarioDTOList = new Vector<DisplayScenarioDTO>();
	public Vector<Grid>  displayCommentGridList = new Vector<Grid>();
	public Vector<Grid>  displayComparisonCommentGridList = new Vector<Grid>();
	public Vector<ListStore<DisplayCostCommentDTO>>  displayCostCommentStoreList = new Vector<ListStore<DisplayCostCommentDTO>>();
	public Vector<ListStore<DisplayCostCommentDTO>>  compDisplayCostCommentStoreList = new Vector<ListStore<DisplayCostCommentDTO>>();
	
	public LayoutContainer commentForm(int tabIdx){
		LayoutContainer bottomLeft = new LayoutContainer();
		FormLayout bottomLeftLayout = new FormLayout();
		bottomLeft.setStyleAttribute("paddingRight","2px");
	
		bottomLeftLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		FormLayout displayCostDataFormLayout = new FormLayout();
		displayCostDataFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		FieldSet commentsData = new FieldSet();
		commentsData.setLayout(displayCostDataFormLayout);
		commentsData.setHeading("Comments");

		commentsList.get(tabIdx).setHideLabel(true);
		commentsList.get(tabIdx).setMaxLength(500);
		commentsList.get(tabIdx).setWidth(485);
		commentsList.get(tabIdx).setHeight(40);

		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
		ColumnConfig columncrDate = new ColumnConfig();
		columncrDate.setId(DisplayCostCommentDTO.CREATE_DATE);
		columncrDate.setHeader("Date");
		columncrDate.setAlignment(Style.HorizontalAlignment.LEFT);
		columncrDate.setWidth(75);
		configs.add(columncrDate);
		ColumnConfig columnUserName = new ColumnConfig();
		columnUserName.setId(DisplayCostCommentDTO.USER_NAME);
		columnUserName.setHeader("User Name");
		columnUserName.setAlignment(Style.HorizontalAlignment.LEFT);
		columnUserName.setWidth(75);
		configs.add(columnUserName);
		ColumnConfig columnCostComment = new ColumnConfig();
		columnCostComment.setId(DisplayCostCommentDTO.COST_COMMENT);
		columnCostComment.setHeader("Display Comments");
		columnCostComment.setAlignment(Style.HorizontalAlignment.LEFT);
		columnCostComment.setWidth(350);
		configs.add(columnCostComment);
		ColumnModel cm = new ColumnModel(configs);
		displayCostCommentStoreList.add(displayCostCommentStore);
		displayCommentGrid = new Grid<DisplayCostCommentDTO>(displayCostCommentStoreList.get(tabIdx), cm);
		displayCommentGridList.add(displayCommentGrid);
		displayCommentGridList.get(tabIdx).setColumnResize(true);
		displayCommentGridList.get(tabIdx).setStyleAttribute("borderTop", "none");
		displayCommentGridList.get(tabIdx).setStripeRows(true);
		displayCommentGridList.get(tabIdx).setBorders(true);
		displayCommentGridList.get(tabIdx).setStyleName("nowrapgrid");
		displayCommentGridList.get(tabIdx).setSize(350, 100);
		displayCommentGridList.get(tabIdx).setSelectionModel(new GridSelectionModel<DisplayCostCommentDTO>());
		LabelField commentListsLabel = new LabelField();
		commentListsLabel.setWidth(400);
		commentListsLabel.setHeight(3);
		commentsData.add(displayCommentGridList.get(tabIdx), formData);
		commentsData.add(commentListsLabel, formData);
		commentsData.add(commentsList.get(tabIdx), formData);
		bottomLeft.add(commentsData);
		return bottomLeft;
	}
	public LayoutContainer createComments(int tabIdx){
		System.out.println("CostAnalysisInfo:createComments starts");
		LayoutContainer main = new LayoutContainer();
		try{
			main.setLayout(new ColumnLayout());
			LayoutContainer bottomLeft = commentForm(tabIdx);
			main.add(bottomLeft, new ColumnData(.455));
				costAnalysisInfoBUCostCtrSplitGrid = new CostAnalysisInfoBUCostCtrSplitGrid();
				costAnalysisInfoBUCostCtrSplitGridList.add(costAnalysisInfoBUCostCtrSplitGrid);
				LayoutContainer bottomRight = new LayoutContainer();
				bottomRight.setStyleAttribute("paddingRight","2px");
				FieldSet bUCostCtrSplit = new FieldSet();
				FormLayout bUCostCtrSplitFormLayout = new FormLayout();
				bUCostCtrSplitFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
				bUCostCtrSplit.setLayout(bUCostCtrSplitFormLayout);
				bUCostCtrSplit.setHeading("BU/Cost Ctr Split");
				bottomRight.add(costAnalysisInfoBUCostCtrSplitGridList.get(tabIdx).createBUCostCenterGroup(displayCostDataGroupList.get(tabIdx).foreCastQty,
						displayCostDataGroupList.get(tabIdx).customerProgramPercentage,displayCostDataGroupList.get(tabIdx).totalDisplaysOrdered,displayCostDataGroupList.get(tabIdx).totalActualInvoiceTotCostVal,displayCostDataGroupList.get(tabIdx).totalDisplayCostsVal,displayCostDataGroupList.get(tabIdx).costAnalysisKitComponentStore.getModels()), formData);
				bUCostCtrSplit.add(bottomRight);
				LabelField bUCostCtrSplitLabel = new LabelField();
				bUCostCtrSplitLabel.setWidth(400);
				bUCostCtrSplitLabel.setHeight(5);
				bUCostCtrSplit.add(bUCostCtrSplitLabel, formData);
				main.add(bUCostCtrSplit, new ColumnData(.465));
		}catch(Exception ex){
			System.out.println("=======CostAnalysisInfo:createComments error "+ ex.toString());
		}
		System.out.println("CostAnalysisInfo:createComments ends");
		return main;
	}
	public void saveDisplayCostComments(DisplayCostDTO displayCostModel, DisplayScenarioDTO displayScenarioDTO){
		System.out.println("saveDisplayCostComments(DisplayCostDTO displayCostModel) starts");
		int scenarioTabIdx=vpScenarioPanelTabs.getSelectedItem().getTabIndex();
		try{
			if(commentsList.get(scenarioTabIdx).getValue()==null || commentsList.get(scenarioTabIdx).getValue().length()==0) return ;
			if(displayCostCommentModel==null){
				displayCostCommentModel = new DisplayCostCommentDTO();
			}
			if (!commentsList.get(scenarioTabIdx).validate()){
				MessageBox.alert("Validation Error", "Invalid comments" , null);
				return;
			}
			displayCostCommentModel.setDisplayScenarioId(displayScenarioDTO.getDisplayScenarioId());
			displayCostCommentModel.setCostComment(commentsList.get(scenarioTabIdx).getValue());
			displayCostCommentModel.setUserName(App.getUser().getUsername());
			AppService.App.getInstance().saveDisplayCostCommentDTO(displayCostCommentModel, new Service.SaveDisplayCostCommentDTO( this,displayCostCommentModel,displayScenarioDTO));
		}catch(Exception ex){
			System.out.println("=======saveDisplayCostComments(DisplayCostDTO displayCostModel) error " +ex.toString()); 
		}
		System.out.println("saveDisplayCostComments(DisplayCostDTO displayCostModel) ends"); 
	}
	public void loadBUCostCenter(List<CostAnalysisKitComponentDTO> loadedCostAnalysisKitComponentDTOs, DisplayScenarioDTO displayScenarioDTO){
		System.out.println("CostAnalysisInfo:loadBUCostCenter starts");
		Boolean buBySpecificCustomer=false;
		int scenarioTabIdx=displayScenarioDTO.getScenarioNum().intValue()-1;
		try{
			String sCustomerPcts = AppService.App.getDmmConstants().customerPct();
			String[] arrCustomerPcts=sCustomerPcts.split(",");
			for(String sCustomerPct:arrCustomerPcts){
				if(display.getCustomerId().equals(new Long(sCustomerPct)) )	{
					buBySpecificCustomer=true;
					break;
				}
			}
			/*
			 * Date: 01/03/2013
			 * costAnalysisInfoBUCostCtrSplitGridList.get(scenarioTabIdx).loadBUCostCenterSplitGrid(displayCostDataGroupList.get(scenarioTabIdx), buBySpecificCustomer,display.getDisplayId(),displayCostDataGroupList.get(scenarioTabIdx).foreCastQty,
			 * Description: Change Forecast Qty into Actual Qty
			 * 
			 */
			costAnalysisInfoBUCostCtrSplitGridList.get(scenarioTabIdx).loadBUCostCenterSplitGrid(displayCostDataGroupList.get(scenarioTabIdx), buBySpecificCustomer,display.getDisplayId(),displayCostDataGroupList.get(scenarioTabIdx).actualQty,
					displayCostDataGroupList.get(scenarioTabIdx).customerProgramPercentage,displayCostDataGroupList.get(scenarioTabIdx).totalDisplaysOrdered,displayCostDataGroupList.get(scenarioTabIdx).totalActualInvoiceTotCostVal,displayCostDataGroupList.get(scenarioTabIdx).totalDisplayCostsVal,loadedCostAnalysisKitComponentDTOs,scenarioTabIdx);
		}catch(Exception ex){
			System.out.println("=======CostAnalysisInfo:loadBUCostCenter exception :" + ex.toString());
		}
		System.out.println("CostAnalysisInfo:loadBUCostCenter ends");
	}
	public void loadHistory(DisplayScenarioDTO displayScenarioDTO){
		int scenarioTabIdx=displayScenarioDTO.getScenarioNum().intValue()-1;
		costAnalysisHistoryGridList.get(scenarioTabIdx).reloadHistory(displayScenarioDTO.getDisplayId(),displayScenarioDTO.getDisplayScenarioId());
	}
	public void reloadDisplayCostImages(Long display, Long displayScenarioId, Long defaultSelectedIndex){
		fileUploadWindow.reloadDisplayCostImages(display,displayScenarioId, defaultSelectedIndex);
	}

	private DisplayCostDataGroup displayCostDataGroup;
	private CostAnalysisHistoryGrid costAnalysisHistoryGrid;
	private CostAnalysisInfoBUCostCtrSplitGrid costAnalysisInfoBUCostCtrSplitGrid;
	private FileUploadWindow fileUploadWindow;
	private CostAnalysisInfoGroup costAnalysisInfoGroup;
	public Vector<DisplayCostDataGroup> displayCostDataGroupList = new Vector<DisplayCostDataGroup>();
	private Vector<CostAnalysisInfoGroup> costAnalysisInfoGroupList = new Vector<CostAnalysisInfoGroup>();
	private Vector<CostAnalysisHistoryGrid> costAnalysisHistoryGridList = new Vector<CostAnalysisHistoryGrid>();
	public Vector<DisplayCostDataGroup> displayCostCompareGroupList = new Vector<DisplayCostDataGroup>();
	public Vector<CostAnalysisInfoGroup> costAnalysisInfoCompareGroupList = new Vector<CostAnalysisInfoGroup>();
	private Vector<FileUploadWindow> fileUploadWindowGroupList = new Vector<FileUploadWindow>();
	private Vector<TextArea> commentsList = new Vector<TextArea>();
	
	
	private void createForm(TabItem scenarioTab, DisplayScenarioDTO displayScenarioDTO) {
		System.out.println("CostAnalysisInfo:createForm starts");
		System.out.println("CostAnalysisInfo:createForm displayScenarioDTO.getDisplayScenarioId()=" + displayScenarioDTO.getDisplayScenarioId());
		displayCostDataGroup= new DisplayCostDataGroup();
		costAnalysisHistoryGrid= new CostAnalysisHistoryGrid();
		costAnalysisInfoGroup = new CostAnalysisInfoGroup();
		fileUploadWindow = new FileUploadWindow();
		costAnalysisHistoryGridList.add(costAnalysisHistoryGrid);
		costAnalysisInfoGroupList.add(costAnalysisInfoGroup);
		displayCostDataGroupList.add(displayCostDataGroup);
		fileUploadWindowGroupList.add(fileUploadWindow);
		comments= new TextArea();
		commentsList.add(comments);
		ContentPanel tabFormPanel = new ContentPanel(); 
		tabFormPanel.setHeaderVisible(false);
		tabFormPanel.setLayout(new RowLayout(Orientation.VERTICAL));   
		tabFormPanel.setSize(1597, 1370);
		tabFormPanel.setFrame(true);   
		scenarioTab.setSize(1590, 1360);
		reCalculateDate = new DateField();
		skuMixFinalCheckBox = new CheckBox();
		btnSaveScenario= new Button(SAVE_SCENARIO);
		btnCancelScenario= new Button(CANCEL_SCENARIO);
		btnCreateScenario=new Button(CREATE_SCENARIO);
		btnCopyScenario=new Button(COPY_SCENARIO);
		btnAdd= new Button(ADD_COMPONENT);
		btnEdit= new Button(EDIT_COMPONENT);
		btnDelete=new Button(DELETE_COMPONENT);
		btnReCalculate=new Button(RE_CALCULATE);
		btnCancelComments=new Button(CANCEL_COMMENTS, listener);
		btnSaveComments=new Button(SAVE_COMMENTS, listener);
		/*
		 * By Mari
		 * Date: 07/30/2014
		 */
		btnSaveScenario.setTabIndex(17);
		btnCancelScenario.setTabIndex(18);
		btnCreateScenario.setTabIndex(19);
		btnCopyScenario.setTabIndex(20);
		/*btnAdd.setTabIndex(21);
		btnEdit.setTabIndex(22);
		btnDelete.setTabIndex(23);
		btnReCalculate.setTabIndex(24);*/
		
		
		btnSaveScenario.addSelectionListener(listener);
		btnCancelScenario.addSelectionListener(listener);
		btnCreateScenario.addSelectionListener(listener);
		btnCopyScenario.addSelectionListener(listener);
		btnAdd.addSelectionListener(listener);
		btnEdit.addSelectionListener(listener);
		btnDelete.addSelectionListener(listener);
		btnReCalculate.addSelectionListener(listener);
		btnCancelComments.addSelectionListener(listener);
		btnSaveComments.addSelectionListener(listener);
		lstBtnSaveScenario.add(btnSaveScenario);
		lstBtnCancelScenario.add(btnCancelScenario);
		lstBtnCreateScenario.add(btnCreateScenario);
		lstBtnCopyScenario.add(btnCopyScenario);
		lstBtnAdd.add(btnAdd);
		lstBtnEdit.add(btnEdit);
		lstBtnDelete.add(btnDelete);
		lstBtnReCalculate.add(btnReCalculate);
		lstSkuMixFinalCheckBox.add(skuMixFinalCheckBox);
		lstBtnCancelComments.add(btnCancelComments);
		lstBtnSaveComments.add(btnSaveComments);
		/*
		 * Code added Date:11/18/2010
		 */
		displayCostDataGroup.setDisplayScenarioDTO(displayScenarioDTO);
		displayCostDataGroupList.get(scenarioTab.getTabIndex()).setCostAnalysisInfo(getCostAnalysisInfo());
		displayCostDataGroupList.get(scenarioTab.getTabIndex()).setDisplayDTO(this.display);
		displayCostDataGroupList.get(scenarioTab.getTabIndex()).setDisplayScenarioDTO(displayScenarioDTO);
		displayCostDataGroupList.get(scenarioTab.getTabIndex()).setupSelectionDialog();
		displayCostDataGroupList.get(scenarioTab.getTabIndex()).foreCastQty.setValue(displayScenarioDTO.getQtyForecast());
		displayCostDataGroupList.get(scenarioTab.getTabIndex()).actualQty.setValue(displayScenarioDTO.getActualQty());
		fileUploadWindowGroupList.get(scenarioTab.getTabIndex()).setDisplayId(this.display.getDisplayId());
		fileUploadWindowGroupList.get(scenarioTab.getTabIndex()).setDisplayScenarioId(displayScenarioDTO.getDisplayScenarioId());
		fileUploadWindowGroupList.get(scenarioTab.getTabIndex()).setCostAnalysisInfo(this);
		displayCostDataGroup.setDisplayDTO(this.display);
		ContentPanel costAnalysisComponentPnael = displayCostDataGroupList.get(scenarioTab.getTabIndex()).createCostAnalysisInfoGrid(this.display,displayScenarioDTO,false);
		LayoutContainer topRight = new LayoutContainer();
		topRight.setStyleAttribute("paddingRight","10px");
		LayoutContainer top = new LayoutContainer();
		top.setLayout(new ColumnLayout());
		FormLayout topRightLayout = new FormLayout();
		topRightLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
		topRight.setLayout(topRightLayout);
		LayoutContainer main = new LayoutContainer();
		main.setLayout(new ColumnLayout());
		LayoutContainer left = new LayoutContainer();
		LayoutContainer center = new LayoutContainer();
		LayoutContainer right = new LayoutContainer();
		LayoutContainer bottom = new LayoutContainer();
		FormLayout leftLayout = new FormLayout();
		left.setStyleAttribute("paddingRight","2px");
		leftLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		FormLayout centerLayout = new FormLayout();
		center.setStyleAttribute("paddingRight","2px");
		centerLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		FormLayout rightLayout = new FormLayout();
		rightLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		FormLayout bottomLayout = new FormLayout();
		bottomLayout.setLabelAlign(FormPanel.LabelAlign.RIGHT);
		left.setStyleAttribute("paddingRight","2px");
		left.setLayout(leftLayout);
		center.setStyleAttribute("paddingRight","2px");
		center.setLayout(centerLayout);
		right.setStyleAttribute("paddingRight","2px");
		right.setLayout(rightLayout);
		bottom.setStyleAttribute("paddingRight","2px");
		bottom.setWidth(550);
		bottom.setLayout(bottomLayout);

		FieldSet displayCostData = new FieldSet();
		FormLayout displayCostDataFormLayout = new FormLayout();
		displayCostDataFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		displayCostData.setLayout(displayCostDataFormLayout);
		displayCostData.setHeading("Display Cost Data");
		left.add(displayCostData);

		FieldSet financialAnalysis = new FieldSet();
		FormLayout financialAnalysisFormLayout = new FormLayout();
		financialAnalysisFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		financialAnalysis.setLayout(financialAnalysisFormLayout);
		//financialAnalysis.setStyleAttribute("paddingTop", "1px");
		financialAnalysis.setHeading("Financial Analysis");
		//center.add(financialAnalysis);

		FieldSet imageRenderingSet = new FieldSet();
		FormLayout imageRenderingFormLayout = new FormLayout();
		imageRenderingFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		imageRenderingSet.setLayout(imageRenderingFormLayout);
		imageRenderingSet.setStyleAttribute("paddingLeft","30px");
		imageRenderingSet.setHeading("Image or Rendering");

		right.add(imageRenderingSet);
		ContentPanel displayCostDataPanel=displayCostDataGroupList.get(scenarioTab.getTabIndex()).createDisplayCostDataGroup();
		displayCostData.add(displayCostDataPanel);
		ContentPanel financialAnalysisPanel=displayCostDataGroupList.get(scenarioTab.getTabIndex()).createFinancialAnalysisGroup(displayScenarioDTO.getDisplayScenarioId(),false);
		//financialAnalysisPanel.setStyleAttribute("paddingLeft","5px");
		financialAnalysis.add(financialAnalysisPanel);
		ContentPanel excessInventoryPanel=displayCostDataGroupList.get(scenarioTab.getTabIndex()).createExcessInventoryGroup(displayScenarioDTO.getDisplayScenarioId(),false);
		FieldSet excessInventory = new FieldSet();
		FormLayout excessInventoryFormLayout = new FormLayout();
		//excessInventoryPanel.setStyleAttribute("paddingLeft","5px");
		//financialAnalysis.setStyleAttribute("paddingTop", "1px");
		excessInventoryFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		excessInventory.setStyleAttribute("marginTop", "-10px");
		excessInventory.setLayout(excessInventoryFormLayout);
		excessInventory.setHeading("Excess Inventory");
		excessInventory.add(excessInventoryPanel);
		
		ContentPanel financialAnalysisGroupPanel = new ContentPanel();
		financialAnalysisGroupPanel.setBodyBorder(false);
		financialAnalysisGroupPanel.setHeaderVisible(false);
		financialAnalysisGroupPanel.setLayout(new RowLayout());
		financialAnalysisGroupPanel.add(financialAnalysis);
		financialAnalysisGroupPanel.add(excessInventory);
		center.add(financialAnalysisGroupPanel);
		
		imageRenderingSet.add(fileUploadWindowGroupList.get(scenarioTab.getTabIndex()).createFileUploadForm());
		ButtonBar buttonBar = new ButtonBar();
		buttonBar.setSpacing(10);
		buttonBar.add(btnCancelComments);
		buttonBar.add(btnSaveComments);
		buttonBar.setAlignment(HorizontalAlignment.CENTER);
		bottom.add(buttonBar);

		FieldSet costAnaysisDataHistory = new FieldSet();
		FormLayout costAnaysisDataHistoryFormLayout = new FormLayout();
		costAnaysisDataHistoryFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		costAnaysisDataHistory.setLayout(costAnaysisDataHistoryFormLayout);
		costAnaysisDataHistory.setHeading("History");
		costAnaysisDataHistory.add(costAnalysisHistoryGridList.get(scenarioTab.getTabIndex()).createHistoryForm(display.getDisplayId(),displayScenarioDTO.getDisplayScenarioId()));
		topRight.add(costAnaysisDataHistory);
		main.add(left, new ColumnData(.215));
		main.add(center, new ColumnData(.215));
		main.add(right, new ColumnData(.305));
		top.add(createDataEntry(scenarioTab,displayScenarioDTO), new ColumnData(.430));
		top.add(topRight, new ColumnData(.305));
		tabFormPanel.add(top);   
		tabFormPanel.add(main);

		ButtonBar scenarioButtonBar = new ButtonBar();
		scenarioButtonBar.setAutoWidth(true);
		scenarioButtonBar.setAlignment(HorizontalAlignment.LEFT);
		
		LabelField scenarioCustomLabel = new LabelField();
		scenarioCustomLabel.setWidth(200);
		scenarioCustomLabel.setHeight(3);
		scenarioButtonBar.add(scenarioCustomLabel);
		scenarioButtonBar.add(btnSaveScenario);
		scenarioButtonBar.add(btnCancelScenario);
		scenarioButtonBar.add(btnCreateScenario);
		scenarioButtonBar.add(btnCopyScenario);
		scenarioButtonBar.add(new Button(RETURN_TO_SELECTION, listener));
		tabFormPanel.add(scenarioButtonBar);

		FieldSet componentInfo = new FieldSet();
		FormLayout componentInfoFormLayout = new FormLayout();
		componentInfoFormLayout.setLabelWidth(125);
		componentInfoFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		componentInfo.setLayout(componentInfoFormLayout);
		componentInfo.setHeading("Component Info");
		componentInfo.setStyleAttribute("paddingLeft","10px");
		componentInfo.add(costAnalysisComponentPnael);
	
        skuMixFinalCheckBox.setBoxLabel("SKU Mix Final");
        skuMixFinalCheckBox.setWidth(100);
        skuMixFinalCheckBox.setHeight(30);
        skuMixFinalCheckBox.setValue(display.isSkuMixFinalFlg());
      
        final int tabIdx=scenarioTab.getTabIndex();
        lstSkuMixFinalCheckBox.get(scenarioTab.getTabIndex()).addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
            	display=getCostAnalysisInfo().getDisplayTabs().getDisplayModel();
        		if (display.getDisplayId() != null) {
        			//display=displayTabs.getDisplayModel();
	       		    Boolean skuMix =  ((CheckBox)be.getSource()) .getValue();
	       		    display.setSkuMixFinalFlg(skuMix);
	       		    displayTabs.setDisplayModel(display);
	       		    getCostAnalysisInfo().getDisplayTabs().setDisplayModel(display);
	       		    getCostAnalysisInfo().getDisplayTabHeader().setSkuMixFinal(skuMix);
	       		    enableDisableButtons(display,tabIdx);
	                final MessageBox box = MessageBox.wait("Progress",
	                        "Saving display general info, please wait...", "Saving...");
	                AppService.App.getInstance().saveDisplayDTO(display,
	                        new Service.SaveCAScenarioDisplayDTO(display, getCostAnalysisInfo(), new Button(), box));
	           }
            }
        });
		ButtonBar componentBar = new ButtonBar();
		componentBar.setAlignment(HorizontalAlignment.CENTER);
		componentBar.add(skuMixFinalCheckBox);
		componentBar.add(new FillToolItem());
		LabelField spaceLabelOne = new LabelField();
		spaceLabelOne.setLabelSeparator(" ");
		spaceLabelOne.setFieldLabel(" ");
		spaceLabelOne.setWidth(40);
		componentBar.add(spaceLabelOne);
		componentBar.add(btnAdd);
		componentBar.add(btnEdit);
		componentBar.add(btnDelete);
		componentBar.setSpacing(10);
	 	LabelField spaceLabelTwo = new LabelField();
		spaceLabelTwo.setLabelSeparator(" ");
		spaceLabelTwo.setFieldLabel(" ");
		spaceLabelTwo.setWidth(40);
		componentBar.add(spaceLabelTwo);
		final DateTimePropertyEditor dateTimePropertyEditor = new DateTimePropertyEditor(datePattern);
		reCalculateDate.setPropertyEditor(dateTimePropertyEditor);
		reCalculateDate.setAllowBlank(true);
		reCalculateDate.setWidth(100);
		componentBar.add(reCalculateDate);
		componentBar.add(btnReCalculate);
		/*
		 * componentBar.setTabIndex(16);
		 */
		componentBar.setTabIndex(21);
		componentInfo.add(componentBar);
		LayoutContainer componentInfoLayout = new LayoutContainer();
		componentInfoLayout.setLayout(new ColumnLayout());
		componentInfoLayout.add(componentInfo, new ColumnData(.730));
		tabFormPanel.add(componentInfoLayout);
		LayoutContainer commentsLayout = new LayoutContainer();
		commentsLayout.setLayout(new ColumnLayout());
		commentsLayout.add(createComments(scenarioTab.getTabIndex()), new ColumnData(.795));
		tabFormPanel.add(commentsLayout);
		tabFormPanel.add(bottom);
		scenarioTab.add(tabFormPanel);
		vpScenarioPanelTabs.insert(scenarioTab, scenarioTab.getTabIndex());
		vpScenarioPanelTabs.setSelection(scenarioTab);
		if(vpScenarioPanelTabs.getItemCount()>=NUMBER_ONE){
			vpScenarioPanelTabs.add(scenarioTabComparison);
		}
		vpScenarioPanelTabs.add(estProgramTab);
		enableDisableButtons(display,tabIdx);
		System.out.println("CostAnalysisInfo:createForm ends");
	}
	
	LayoutContainer compScenarioOne = new LayoutContainer();
	LayoutContainer compScenarioTwo = new LayoutContainer();
	LayoutContainer compScenarioThree = new LayoutContainer();
	LayoutContainer compScenarioFour = new LayoutContainer();
	LayoutContainer compScenarioFive = new LayoutContainer();
	LayoutContainer mainCompScenario = new LayoutContainer();
	ContentPanel compFormPanel = new ContentPanel(); 
	ContentPanel programFormPanel = new ContentPanel();

	public void createComparisonForm(TabItem scenarioTab){
		System.out.println("CostAnalysisInfo:createComparisonForm starts");
		compFormPanel.setHeaderVisible(false);
		compFormPanel.setLayout(new RowLayout(Orientation.VERTICAL));   
		compFormPanel.setSize(1597, 1370);
		compFormPanel.setFrame(true);   
		ButtonBar comparisonButtonBar = new ButtonBar();
		comparisonButtonBar.setAlignment(HorizontalAlignment.CENTER);
		comparisonButtonBar.add(new FillToolItem());
		comparisonButtonBar.add(new FillToolItem());
		comparisonButtonBar.add(new Button(RETURN_TO_SELECTION, listener));
		comparisonButtonBar.add(new Button(APPLY_FOR_APPROVAL, listener));
		compFormPanel.add(comparisonButtonBar);
		LabelField spaceLabel = new LabelField();
		spaceLabel.setLabelSeparator(" ");
		spaceLabel.setFieldLabel(" ");
		spaceLabel.setWidth(40);
		compFormPanel.add(spaceLabel);
		compFormPanel.setButtonAlign(Style.HorizontalAlignment.CENTER);
		scenarioTab.setSize(1590, 1360);
		mainCompScenario.setLayout(new ColumnLayout());
		compScenarioOne=createComparisonScenarioPanel(NUMBER_ZERO);
		compScenarioOne.setVisible(false);
		mainCompScenario.add(compScenarioOne, new ColumnData(.200));
		compScenarioTwo=createComparisonScenarioPanel(NUMBER_ONE);
		compScenarioTwo.setVisible(false);
		mainCompScenario.add(compScenarioTwo, new ColumnData(.200));
		compScenarioThree=createComparisonScenarioPanel(NUMBER_TWO);
		mainCompScenario.add(compScenarioThree, new ColumnData(.200));
		compScenarioThree.setVisible(false);
		compScenarioFour=createComparisonScenarioPanel(NUMBER_THREE);
		compScenarioFour.setVisible(false);
		mainCompScenario.add(compScenarioFour, new ColumnData(.200));
		compScenarioFive=createComparisonScenarioPanel(NUMBER_FOUR);
		compScenarioFive.setVisible(false);
		mainCompScenario.add(compScenarioFive, new ColumnData(.200));
		compFormPanel.add(mainCompScenario);
		scenarioTab.add(compFormPanel);
		System.out.println("CostAnalysisInfo:createComparisonForm ends");
	}
	
	public void createProgramForm(TabItem programTab){
		programFormPanel.setHeaderVisible(false);
		programFormPanel.setLayout(new RowLayout(Orientation.VERTICAL));   
		programFormPanel.setSize(1597, 1370);
		programFormPanel.setFrame(true);   
		programFormPanel.setButtonAlign(Style.HorizontalAlignment.CENTER);
		FieldSet info = new FieldSet();
		info.setHeading("Est. Program %");
		info.setWidth(400);
		info.setHeight(400);
		info.setAutoHeight(true);
//TODO		info.setLayout(getFormLayout(120,230));
		ContentPanel form = new ContentPanel();
		form.setHeaderVisible(false);
		form.setWidth(390);
		form.setFrame(false);   
		form.setButtonAlign(Style.HorizontalAlignment.CENTER);
		
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
		TextField<String> buValue = new TextField<String>(); 
		CellEditor buEditor =  new CellEditor(buValue); 
		buValue.disable();
		buEditor.disable();
		ColumnConfig config = new ColumnConfig(ProgramBuDTO.BU_DESC, "Business Unit", 270);
		config.setEditor(buEditor);
		configs.add(config);

		final NumberField nProgrPerc = new NumberField(); 
		nProgrPerc.setPropertyEditorType(Double.class);
		nProgrPerc.addInputStyleName("text-element-no-underline");	
		nProgrPerc.setMaxValue(100);
		nProgrPerc.setMinValue(0);
		final CellEditor noProgrPercEditor =  new CellEditor(nProgrPerc); 
		config = new ColumnConfig(ProgramBuDTO.PROGRAM_PCT, "Est. Program %", 100);
		config.setAlignment(Style.HorizontalAlignment.CENTER);
		config.setEditor(noProgrPercEditor);
		config.setRenderer(new GridCellRenderer<ModelData>() {
			public Object render(ModelData model, String property, com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore<ModelData> store,	Grid<ModelData> grid) {
				return (model.get(property) != null ? model.get(property) : "0") + "%";
			}
		});
		configs.add(config);
		
		programGrid = new Grid<ProgramBuDTO>(new ListStore<ProgramBuDTO>(), new ColumnModel(configs));
		final RowEditor<ProgramBuDTO> re = new RowEditor<ProgramBuDTO>(){
			@Override
			protected void onRender(Element target, int index) {
				super.onRender(target, index);
				saveBtn.setText("Apply");
			}
		};
		re.setClicksToEdit(ClicksToEdit.ONE);
		re.setBodyStyleName("x-row-editor-body");
		programGrid.setWidth(375);
		programGrid.setBorders(true);
		programGrid.setStripeRows(false);
		programGrid.setHeight(220);
		programGrid.addPlugin(re); 
		programGrid.setView(new GridView());
		programGrid.getView().setEmptyText("No programs have been created for current display");
		
		form.add(programGrid);
		Button savePrograms = new Button("Save");
		savePrograms.addSelectionListener(new SelectionListener<ButtonEvent>() {
			@Override
			public void componentSelected(ButtonEvent ce) {
				List<Record> mods = programGrid.getStore().getModifiedRecords();
				if(mods.size() == 0) return;
				List<ProgramBuDTO> programList = new ArrayList<ProgramBuDTO>();
				for (int i = 0; i < mods.size(); i++) {
					ProgramBuDTO dto = (ProgramBuDTO)((Record)mods.get(i)).getModel();
					dto.setDtLastChanged(new Date());  
					dto.setUserLastChanged(App.getUser().getUsername());					
					programList.add(dto);
				}
				AppService.App.getInstance().saveProgramBus(programList, 
						new Service.UpdateProgramBu(programGrid.getStore()));
	
			}
		});
		GeneralComponentBinding programGridBinding = new GeneralComponentBinding(programGrid) {
			protected boolean isButtonEnabled(Button button) {
				return programGrid.getStore().getModifiedRecords().size() > 0;
			}
		};
		programGridBinding.addButton(savePrograms);
		
		/*
		 * Code change by Mari
		 * Date: 10092012
		 * Populate Program_% for the valid bu based on the customer id.
		 */
		
		Button uploadedProgPct = new Button("Uploaded Program %");
		uploadedProgPct.addSelectionListener(new SelectionListener<ButtonEvent>() {
			@Override
			public void componentSelected(ButtonEvent ce) {
				List<ProgramBuDTO> programList = programGrid.getStore().getModels();
				AppService.App.getInstance().saveProgramUploadedBus(programList, displayTabs.getDisplayModel(),
						new Service.UpdateProgramUploadedBu(programGrid.getStore()));
	
			}
		});
		/*
		 * End
		 */

		Button reCalculate = new Button("Re-Calculate");
		reCalculate.addSelectionListener(new SelectionListener<ButtonEvent>() {
			public void componentSelected(ButtonEvent ce) {
				// TODO Add here re-calculate logic
				DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
				if(copiedDisplayCostDTO !=null ){
					MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
					return;
				}
				String title = "Re calculate the price exception!";
				String msg = "Would you like to proceed?"; 		           			
				MessageBox box = new MessageBox();
				box.setButtons(MessageBox.YESNOCANCEL);
				box.setIcon(MessageBox.QUESTION);
				box.setTitle("Re calculate?");
				box.addCallback(recalculateListenerAll);
				box.setTitle(title);
				box.setMessage(msg);
				box.show();
				
			}
		});
		form.addButton(savePrograms);
		form.addButton(uploadedProgPct);
		form.addButton(reCalculate);
		
		info.add(form);
		programFormPanel.add(info);
		programTab.setSize(1590, 1360);
		programTab.add(programFormPanel);
	}
	
	private final Listener<MessageBoxEvent> recalculateListenerAll = new Listener<MessageBoxEvent>() {
		public void handleEvent(MessageBoxEvent mbe) {
			Button btn = mbe.getButtonClicked();
			if (btn.getText().equals("Yes")) {
				System.out.println("CostAnalysisInfo:recalculateListenerAll starts");
				for (int tabIndex=NUMBER_ZERO; tabIndex<NUMBER_FIVE;tabIndex++){
					displayCostDataGroupList.get(tabIndex).reCalculatePriceException(displayTabs.getDisplayModel(),
						displayCostDataGroupList.get(tabIndex).customerProgramPercentage.getValue()==null?new Double(0.0):displayCostDataGroupList.get(tabIndex).customerProgramPercentage.getValue().doubleValue(), 
								reCalculateDate.getValue()==null?new Date():reCalculateDate.getValue(), displayScenarioDTOList.get(tabIndex),false);
				}
			}
			if (btn.getText().equals("No")) {

			}
			if (btn.getText().equals("Cancel")) {

			}
		}
	};

	public Vector<Radio> approvedScenarioRadioList = new Vector<Radio>();
	public LayoutContainer createComparisonScenarioPanel(int scenarioPanelNum){
		displayCostDataGroup= new DisplayCostDataGroup();
		displayCostCompareGroupList.insertElementAt(displayCostDataGroup, scenarioPanelNum);
		costAnalysisInfoGroup = new CostAnalysisInfoGroup();
		costAnalysisInfoCompareGroupList.insertElementAt(costAnalysisInfoGroup, scenarioPanelNum);
		LayoutContainer scenarioContainer = new LayoutContainer();
		scenarioContainer.setStyleAttribute("paddingRight","2px");
		scenarioContainer.setStyleAttribute("paddingRight","2px");
		FormLayout leftLayout = new FormLayout();
		leftLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		scenarioContainer.setLayout(leftLayout);
		FieldSet displayCostDataSet = new FieldSet();
		displayCostDataSet.setHeading("Display Cost Data");
		FormLayout displayCostDataFormLayout = new FormLayout();
		displayCostDataFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		displayCostDataSet.setLayout(displayCostDataFormLayout);
		ContentPanel displayCostDataPanel=displayCostCompareGroupList.get(scenarioPanelNum).createDisplayCostDataGroup();
		displayCostDataSet.setStyleAttribute("marginTop", "-10px");
		displayCostDataSet.add(displayCostDataPanel);
		Radio approvedScenarioRadio = new Radio();
		approvedScenarioRadio.setValue(Boolean.FALSE);
		approvedScenarioRadio.setOriginalValue(Boolean.FALSE);
		if(scenarioPanelNum < displayScenarioDTOs.size()){
			if(displayScenarioDTOs.get(scenarioPanelNum).getScenarioApprovalFlg()!=null && displayScenarioDTOs.get(scenarioPanelNum).getScenarioApprovalFlg().equalsIgnoreCase(YES_FLG)){
				approvedScenarioRadio.setValue(Boolean.TRUE);
				approvedScenarioRadio.setOriginalValue(Boolean.TRUE);
			}
		}
		approvedScenarioRadio.setBoxLabel("Scenario "+ (scenarioPanelNum+1));
		approvedScenarioRadio.setId(new Integer(scenarioPanelNum+1).toString());
		approvedScenarioRadio.setWidth(175);
		approvedScenarioRadio.setLabelSeparator("");
		approvedScenarioRadioList.add(approvedScenarioRadio);    
		FieldSet financialAnalysis = new FieldSet();
		FormLayout financialAnalysisFormLayout = new FormLayout();
		financialAnalysisFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		financialAnalysis.setLayout(financialAnalysisFormLayout);
		financialAnalysis.setHeading("Financial Analysis");
		DisplayScenarioDTO displayScenarioDTO=(displayScenarioDTOList.size()>scenarioPanelNum) ?displayScenarioDTOList.get(scenarioPanelNum):null;
		Long displayScenarioId=displayScenarioDTO!=null? displayScenarioDTO.getDisplayScenarioId():null;
		ContentPanel financialAnalysisPanel=displayCostCompareGroupList.get(scenarioPanelNum).createFinancialAnalysisGroup(displayScenarioId, true);
		
		ContentPanel vendorPanel=costAnalysisInfoCompareGroupList.get(scenarioPanelNum).createCostAnalysisVendorInfoPanel(display.getCountryId(),displayScenarioDTO);
		approvedScenarioRadioList.get(scenarioPanelNum).addListener(Events.OnClick, new Listener<FieldEvent>() {
			public void handleEvent(FieldEvent be) {
				Radio selectedScenarioRadio = (Radio) be.getSource();
				selectedScenarioRadio.setValue(Boolean.TRUE);
				selectedScenarioRadio.setOriginalValue(Boolean.TRUE);
				enableDisableSelectedScenarioRadio(selectedScenarioRadio);
			}
		});
		financialAnalysis.add(financialAnalysisPanel);
		//financialAnalysis.setStyleAttribute("marginTop", "-10px");
		ContentPanel excessInventoryPanel=displayCostCompareGroupList.get(scenarioPanelNum).createExcessInventoryGroup(displayScenarioId, true);
		FieldSet excessInventory = new FieldSet();
		FormLayout excessInventoryFormLayout = new FormLayout();
		excessInventoryFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
		excessInventory.setLayout(excessInventoryFormLayout);
		excessInventory.setHeading("Excess Inventory");
		excessInventory.add(excessInventoryPanel);
		/*
		 * excessInventory.setStyleAttribute("marginTop", "-10px");
		 */
		excessInventory.setStyleAttribute("marginTop", "10px");
		
		
		
		scenarioContainer.add(approvedScenarioRadio);
		scenarioContainer.add(vendorPanel);
		
		ContentPanel financialAnalysisGroupPanel = new ContentPanel();
		//financialAnalysisGroupPanel.setStyleAttribute("paddingTop","2px");
		financialAnalysisGroupPanel.setBodyBorder(false);
		financialAnalysisGroupPanel.setHeaderVisible(false);
		financialAnalysisGroupPanel.setLayout(new RowLayout());
		//financialAnalysisGroupPanel.setLayout(new FitLayout());
		
		//scenarioContainer.add(financialAnalysis);
		financialAnalysisGroupPanel.add(financialAnalysis);
		financialAnalysisGroupPanel.add(excessInventory);
		//scenarioContainer.add(financialAnalysis);
		scenarioContainer.add(financialAnalysisGroupPanel);
		
		scenarioContainer.add(displayCostDataSet);
		List<ColumnConfig> compConfigs = new ArrayList<ColumnConfig>();
		ColumnConfig compColumnCostComment = new ColumnConfig();
		compColumnCostComment.setId(DisplayCostCommentDTO.COST_COMMENT);
		compColumnCostComment.setHeader("Display Comments");
		compColumnCostComment.setAlignment(Style.HorizontalAlignment.LEFT);
		compColumnCostComment.setWidth(290);
		compConfigs.add(compColumnCostComment);
		ColumnModel compCm = new ColumnModel(compConfigs);
		compDisplayCostCommentStore = new ListStore<DisplayCostCommentDTO>();
		compDisplayCostCommentStoreList.add(compDisplayCostCommentStore);
		compDisplayCommentGrid = new Grid<DisplayCostCommentDTO>(compDisplayCostCommentStoreList.get(scenarioPanelNum), compCm);
		displayComparisonCommentGridList.add(compDisplayCommentGrid);
		displayComparisonCommentGridList.get(scenarioPanelNum).setColumnResize(false);
		displayComparisonCommentGridList.get(scenarioPanelNum).setStyleAttribute("borderTop", "none");
		displayComparisonCommentGridList.get(scenarioPanelNum).setStripeRows(false);
		displayComparisonCommentGridList.get(scenarioPanelNum).setBorders(false);
		displayComparisonCommentGridList.get(scenarioPanelNum).setStyleName("nowrapgrid");
		displayComparisonCommentGridList.get(scenarioPanelNum).setSize(300, 100);
		displayComparisonCommentGridList.get(scenarioPanelNum).setSelectionModel(new GridSelectionModel<DisplayCostCommentDTO>());
		scenarioContainer.add(displayComparisonCommentGridList.get(scenarioPanelNum));
		System.out.println("CostAnalysisInfo:createComparisonScenarioPanel ends");
		return scenarioContainer;
	}

	public void enableDisableSelectedScenarioRadio(Radio selectedScenarioRadio){
		for (Radio approvedScenarioRadio:approvedScenarioRadioList){
			if(approvedScenarioRadio.equals(selectedScenarioRadio)){
				approvedScenarioRadio.setValue(Boolean.TRUE);
				approvedScenarioRadio.setOriginalValue(Boolean.TRUE);
			}else{
				approvedScenarioRadio.setValue(Boolean.FALSE);
				approvedScenarioRadio.setOriginalValue(Boolean.FALSE);
			}
		}
	}
	SelectionListener listener = new SelectionListener<ComponentEvent>() {
		public void componentSelected(ComponentEvent ce) {
			display=getCostAnalysisInfo().getDisplayTabs().getData("display");
			Button btn = (Button) ce.getComponent();
			displayScenarioDTOs=getCostAnalysisInfo().getData("displayScenarioDTOs");
			if (!btn.getText().equals(RETURN_TO_SELECTION)) {
				if (display.getDisplayId() == null) {
					MessageBox.alert("Validation Error", "There is no display defined " , null);
					return ;
				}
			}
			if(!vpScenarioPanelTabs.getSelectedItem().equals(scenarioTabComparison)){
				displayCostDataGroupList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).setDisplayDTO(display);
			}
			if (btn.getText().equals(ADD_COMPONENT)) {
				displayScenarioDTOs=getCostAnalysisInfo().getData("displayScenarioDTOs");
				DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
				if(copiedDisplayCostDTO ==null &&  displayScenarioDTOs!=null && displayScenarioDTOs.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).getDisplayScenarioId()!=null){
					displayCostDataGroupList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).setDisplayScenarioDTO(displayScenarioDTOs.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()));
					displayCostDataGroupList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).showAddKitComponentWindow();
				}else{
					MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
				}
			}else if (btn.getText().equals(EDIT_COMPONENT)) {
				DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
				if(copiedDisplayCostDTO !=null ){
					MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
					return;
				}
				 displayScenarioDTOs=getCostAnalysisInfo().getData("displayScenarioDTOs");
				 displayCostDataGroupList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).setDisplayScenarioDTO(displayScenarioDTOs.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()));
				 if (displayCostDataGroupList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).componentGrid.getSelectionModel().getSelectedItem() != null) {
					 btn.disable();
	                    Long selectedIdx = 0L;
	                    Long kitComponentId = displayCostDataGroupList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).componentGrid.getSelectionModel().getSelectedItem().getKitComponentId();
	                    List<Long> kitComponentIds = new LinkedList<Long>();
	                    List<CostAnalysisKitComponentDTO> kitComponentList = displayCostDataGroupList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).componentGrid.getStore().getModels();
	                    for (int i = 0; i < kitComponentList.size(); i++) {
	                        Long id = kitComponentList.get(i).getKitComponentId();
	                        if (id == kitComponentId) selectedIdx = new Long(i);
	                        kitComponentIds.add(id);
	                    }
	                    final MessageBox loadProgressBar = MessageBox.wait("Progress", "Loading cost analsysis kit component...", "Loading..."); 
	                    AppService.App.getInstance().getKitComponentDTO(kitComponentId, display,displayScenarioDTOs.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()),
	                            new Service.LoadKitComponent(display,displayScenarioDTOs.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()), kitComponentIds, selectedIdx, App.getVp(), btn, readOnly, loadProgressBar,true,reCalculateDate.getValue()==null?new Date():reCalculateDate.getValue()));
	                } else {
	                	MessageBox.alert("Validation Error","Select Kit Component, Then press Edit Component",null);
	                }
			} else if (btn.getText().equals(APPLY_FOR_APPROVAL)) {
				displayScenarioDTOs=getCostAnalysisInfo().getData("displayScenarioDTOs");
				int scenarioIdx=-1;
				for (DisplayScenarioDTO displayScenarioDTO:displayScenarioDTOs){
					scenarioIdx=displayScenarioDTO.getScenarioNum().intValue()-1;
						final MessageBox box = MessageBox.wait("Progress",
								"Saving approval status of Scenario " + displayScenarioDTO.getScenarioNum()+ ", please wait...", "Saving...");
						Timer t = new Timer() {
							public void run() {
								box.close();
							}};
							t.schedule(500);
							if(approvedScenarioRadioList.get(scenarioIdx).getValue()){
								displayScenarioDTO.setScenarioApprovalFlg(YES_FLG);
							}else{
								displayScenarioDTO.setScenarioApprovalFlg(NO_FLG);
							}
							displayScenarioDTO.setUserLastChanged(App.getUser().getUsername());
							displayScenarioDTO.setDtLastChanged(new Date());
							AppService.App.getInstance().saveDisplayScenarioDTO(displayScenarioDTO,new Service.SaveApplyForApproval(getCostAnalysisInfo(),box,scenarioIdx));
				}
			}else if (btn.getText().equals(DELETE_COMPONENT)) {
				DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
				if(copiedDisplayCostDTO !=null ){
					MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
					return;
				}
				displayCostDataGroupList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).deleteKitComponent();
			} else if (btn.getText().equals(RETURN_TO_SELECTION)) {
				forwardToDisplayProjectSelection();
			} else if (btn.getText().equals(RE_CALCULATE)) {
				DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
				if(copiedDisplayCostDTO !=null ){
					MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
					return;
				}
				String title = "Re calculate the price exception!";
				String msg = "Would you like to proceed?"; 		           			
				MessageBox box = new MessageBox();
				box.setButtons(MessageBox.YESNOCANCEL);
				box.setIcon(MessageBox.QUESTION);
				box.setTitle("Re calculate?");
				box.addCallback(recalculateListener);
				box.setTitle(title);
				box.setMessage(msg);
				box.show();
			}else if (btn.getText().equals(SAVE_DISPLAY)) {
				save(btn,null,null);
			}else if(btn.getText().equals(SAVE_COMMENTS)){
				DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
				if(copiedDisplayCostDTO==null && displayScenarioDTOList!=null && displayScenarioDTOList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).getDisplayScenarioId()!=null){
					saveDisplayCostComments(getDisplayCostModel(), displayScenarioDTOList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()));
				}else{
					MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
					return;
				}
			}else if (btn.getText().equals(CANCEL_COMMENTS)) {
				commentsList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).reset();

			}else if (btn.getText().equals(CANCEL_SCENARIO)) {
				DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
				if(copiedDisplayCostDTO !=null ){
					MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
					return;
				}
				final MessageBox box = MessageBox.wait("Progress",
						"Cancelling " + vpScenarioPanelTabs.getSelectedItem().getText() + ", please wait...", "Cancelling...");
				Timer t = new Timer() {
					public void run() {
						resetCurrentModelValues(vpScenarioPanelTabs.getSelectedItem().getTabIndex());
						box.close();
					}};
					t.schedule(500);
			}else if (btn.getText().equals(SAVE_SCENARIO)) {
				if(vpScenarioPanelTabs.getSelectedItem().getTabIndex() != vpScenarioPanelTabs.getItem(displayScenarioDTOList.size()-1).getTabIndex() ){
					DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
					if(copiedDisplayCostDTO !=null ){
						MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
						return;
					}
				}
				save(btn,null,null);
			}else if (btn.getText().equals(CREATE_SCENARIO)) {
				if(vpScenarioPanelTabs.getItemCount()<NUMBER_SEVEN){
					System.out.println("CREATE_SCENARIO Count="+vpScenarioPanelTabs.getItemCount());
					DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
					if(copiedDisplayCostDTO !=null ){
						MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
						return;
					}
					DisplayCostDTO copiedDisplayCost=new DisplayCostDTO(); 
					getCostAnalysisInfo().setData("copiedDisplayCost", copiedDisplayCost);
					if(vpScenarioPanelTabs.getItemCount()==NUMBER_THREE){
						final MessageBox box = MessageBox.wait("Progress",
								"Creating " + scenarioTabTwo.getText() + ", please wait...", "Creating...");
						Timer t = new Timer() {
							public void run() {
								DisplayScenarioDTO displayScenarioDTO = getDefaultScenarioData();
								displayScenarioDTO.setScenarioNum(new Long(NUMBER_TWO));
								displayScenarioDTO.setScenarioApprovalFlg(NO_FLG);
								displayScenarioDTO.setDtCreated(new Date());
								displayScenarioDTO.setUserCreated(App.getUser().getUsername());
								displayScenarioDTO.setDisplayScenarioId(null);
								displayScenarioDTOList.insertElementAt(displayScenarioDTO, 1);
								displayScenarioDTOs.add(displayScenarioDTO);
								createForm(scenarioTabTwo,displayScenarioDTO );
								box.close();
							}};
							t.schedule(500);
					}else if(vpScenarioPanelTabs.getItemCount()==NUMBER_FOUR){/*Include one  for comparison*/
						final MessageBox box = MessageBox.wait("Progress",
								"Creating " + scenarioTabThree.getText() + ", please wait...", "Creating...");
						Timer t = new Timer() {
							public void run() {
								DisplayScenarioDTO displayScenarioDTO = getDefaultScenarioData();
								displayScenarioDTO.setScenarioNum(new Long(NUMBER_THREE));
								displayScenarioDTO.setScenarioApprovalFlg(NO_FLG);
								displayScenarioDTO.setDtCreated(new Date());
								displayScenarioDTO.setUserCreated(App.getUser().getUsername());
								displayScenarioDTO.setDisplayScenarioId(null);
								displayScenarioDTOList.insertElementAt(displayScenarioDTO, 2);
								displayScenarioDTOs.add(displayScenarioDTO);
								createForm(scenarioTabThree,displayScenarioDTO );
								box.close();
							}};
							t.schedule(500);
					}else if(vpScenarioPanelTabs.getItemCount()==NUMBER_FIVE){
						final MessageBox box = MessageBox.wait("Progress",
								"Creating " + scenarioTabFour.getText() + ", please wait...", "Creating...");
						Timer t = new Timer() {
							public void run() {
								DisplayScenarioDTO displayScenarioDTO =getDefaultScenarioData();
								displayScenarioDTO.setScenarioNum(new Long(NUMBER_FOUR));
								displayScenarioDTO.setScenarioApprovalFlg(NO_FLG);
								displayScenarioDTO.setDtCreated(new Date());
								displayScenarioDTO.setUserCreated(App.getUser().getUsername());
								displayScenarioDTO.setDisplayScenarioId(null);
								displayScenarioDTOList.insertElementAt(displayScenarioDTO, 3);
								displayScenarioDTOs.add(displayScenarioDTO);
								createForm(scenarioTabFour,displayScenarioDTO );
								box.close();
							}};
							t.schedule(500);
					}else if(vpScenarioPanelTabs.getItemCount()==NUMBER_SIX){
						final MessageBox box = MessageBox.wait("Progress",
								"Creating " + scenarioTabFive.getText() + ", please wait...", "Creating...");
						Timer t = new Timer() {
							public void run() {
								DisplayScenarioDTO displayScenarioDTO = getDefaultScenarioData();
								displayScenarioDTO.setScenarioNum(new Long(NUMBER_FIVE));
								displayScenarioDTO.setScenarioApprovalFlg(NO_FLG);
								displayScenarioDTO.setDtCreated(new Date());
								displayScenarioDTO.setUserCreated(App.getUser().getUsername());
								displayScenarioDTO.setDisplayScenarioId(null);
								displayScenarioDTOList.insertElementAt(displayScenarioDTO, 4);
								displayScenarioDTOs.add(displayScenarioDTO);
								createForm(scenarioTabFive,displayScenarioDTO );
								box.close();
							}};
							t.schedule(500);
					}

				}else{
					MessageBox.alert("Validation Error", "Maximum number of scenario per project is aleady reached", null);
				}
			}else if (btn.getText().equals(COPY_SCENARIO)) {
				System.out.println("CostAnalysisInfo:Copying scenario Starts");
				if(vpScenarioPanelTabs.getItemCount()<NUMBER_SEVEN){
					DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
					if(copiedDisplayCostDTO !=null ){
						MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
						return;
					}
					Listener<MessageBoxEvent> cb = new Listener<MessageBoxEvent>() {
						public void handleEvent(MessageBoxEvent mbe) {
							String id = mbe.getButtonClicked().getItemId();
							if (Dialog.YES == id) {
								final MessageBox box = MessageBox.wait("Progress",
										"Copying Scenario " + displayScenarioDTOList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex()).getScenarioNum() + ", please wait...", "Loading...");
								Timer t = new Timer() {
									public void run() {
										TabItem selectedTabItem = null;
										DisplayScenarioDTO selectedDisplayScenarioDTO=null;
										selectedDisplayScenarioDTO=displayScenarioDTOList.get(vpScenarioPanelTabs.getSelectedItem().getTabIndex());
										if(vpScenarioPanelTabs.getItemCount()==NUMBER_THREE){
											selectedTabItem=scenarioTabTwo;
											selectedDisplayScenarioDTO.setScenarioNum(new Long(NUMBER_TWO));
										}else if(vpScenarioPanelTabs.getItemCount()==NUMBER_FOUR){//Include one  for comparison
											selectedTabItem=scenarioTabThree;
											selectedDisplayScenarioDTO.setScenarioNum(new Long(NUMBER_THREE));
										}else if(vpScenarioPanelTabs.getItemCount()==NUMBER_FIVE){
											selectedTabItem=scenarioTabFour;
											selectedDisplayScenarioDTO.setScenarioNum(new Long(NUMBER_FOUR));
										}else if(vpScenarioPanelTabs.getItemCount()==NUMBER_SIX){
											selectedTabItem=scenarioTabFive;
											selectedDisplayScenarioDTO.setScenarioNum(new Long(NUMBER_FIVE));
										}
										selectedDisplayScenarioDTO.setScenarioApprovalFlg(NO_FLG);
										displayScenarioDTOList.insertElementAt(selectedDisplayScenarioDTO, displayScenarioDTOList.size());
										displayScenarioDTOs.add(selectedDisplayScenarioDTO);
										displayCostModels = getCostAnalysisInfo().getData(DISPLAY_COST_MODELS);
										createForm(selectedTabItem,selectedDisplayScenarioDTO);
										AppService.App.getInstance().copyDisplayScenarioDTO(selectedDisplayScenarioDTO,App.getUser().getUsername(),true, new Service.ReLoadCopiedDisplayScenario(getCostAnalysisInfo(), selectedDisplayScenarioDTO,box ));
									}};
									t.schedule(500); 
							}
						}
					};
					MessageBox.confirm("Copy?", "This will create a copy of this scenario. Do you wish to continue?", cb);
				}else{
					MessageBox.alert("Validation Error", "Maximum number of scenario per project is aleady reached", null);
				}
				System.out.println("CostAnalysisInfo:Copying scenario ends");
			}
		}
	};
	public void overrideDirty() {
		overrideDirty = true;
	}
	private boolean overrideDirty = false;

	public boolean isDirty() {
		if(overrideDirty==true) return false;
		return !different();
	}
	private boolean different(){
		boolean displayDirty=true;
		boolean displayCostDirty=true;
		try{
			int scenarioTabIdx= vpScenarioPanelTabs.getSelectedItem().getTabIndex();
			displayScenarioDTOs=getCostAnalysisInfo().getData("displayScenarioDTOs");
			if(this.display!=null) displayDirty= displayScenarioDTOs.get(scenarioTabIdx).equalsCostAnalysisDisplayScenarioInfo(getCurrentDisplayModelFromControls(scenarioTabIdx));
			displayCostModels=getCostAnalysisInfo().getData(DISPLAY_COST_MODELS);
			displayCostDirty=displayCostModels.get(scenarioTabIdx).equalsDisplayCostInfo(getCurrentDisplayCostModelFromControls(scenarioTabIdx));
		}catch(Exception ex){
			System.out.println("CostAnalysisInfo:different error "+ ex.toString());
		}
		return displayDirty && displayCostDirty ;
	}

	private void resetCurrentModelValues(int scenarioTabIdx){
		displayCostDataGroupList.get(scenarioTabIdx).resetDisplayCost(displayCostModels.get(scenarioTabIdx),displayScenarioDTOList.get(scenarioTabIdx));
		costAnalysisInfoGroupList.get(scenarioTabIdx).resetDisplay(displayCostModels.get(scenarioTabIdx),displayScenarioDTOList.get(scenarioTabIdx));
	}

	private DisplayDTO getCurrentDisplayModelFromControls(int scenarioTabIdx){
		DisplayDTO controlModel = new DisplayDTO();
		System.out.println("+++CostAnalysisInfo:getCurrentDisplayModelFromControls() starts scenarioTabIdx="+scenarioTabIdx);
		try{
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).topLevel.getValue()!=null) controlModel.setSku(costAnalysisInfoGroupList.get(scenarioTabIdx).topLevel.getValue());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).status.getValue()!=null) controlModel.setStatusId(costAnalysisInfoGroupList.get(scenarioTabIdx).status.getValue().getStatusId());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).projectManager.getValue()!=null) controlModel.setDisplaySalesRepId(costAnalysisInfoGroupList.get(scenarioTabIdx).projectManager.getValue().getDisplaySalesRepId());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).manufacturer.getValue()!=null) controlModel.setManufacturerId(costAnalysisInfoGroupList.get(scenarioTabIdx).manufacturer.getValue().getManufacturerId());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).promoPeriod.getValue()!=null) controlModel.setPromoPeriodId(costAnalysisInfoGroupList.get(scenarioTabIdx).promoPeriod.getValue().getPromoPeriodId());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsYear!=null) controlModel.setPromoYear(costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsYear.getValue().longValue());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).packLocation.getValue()!=null) controlModel.setPackoutVendorId(costAnalysisInfoGroupList.get(scenarioTabIdx).packLocation.getValue().getLocationId());
			if(displayCostDataGroupList.get(scenarioTabIdx).foreCastQty.getValue()!=null) controlModel.setQtyForecast(displayCostDataGroupList.get(scenarioTabIdx).foreCastQty.getValue().longValue());
			if(displayCostDataGroupList.get(scenarioTabIdx).actualQty.getValue()!=null) controlModel.setActualQty(displayCostDataGroupList.get(scenarioTabIdx).actualQty.getValue().longValue());
		}catch(Exception ex){
			System.out.println("=======CostAnalysisInfo:getCurrentDisplayModelFromControls error "+ ex.toString());
		}
		System.out.println("CostAnalysisInfo:getCurrentDisplayModelFromControls() ends");
		return controlModel;
	}

	private DisplayCostDTO getCurrentDisplayCostModelFromControls(int scenarioTabIdx){
		System.out.println("CostAnalysisInfo:getCurrentDisplayCostModelFromControls() starts scenarioTabIdx="+scenarioTabIdx);
		DisplayCostDTO controlModel = new DisplayCostDTO();
		try{
			controlModel.setOverageScrapPct(displayCostDataGroupList.get(scenarioTabIdx).overageScrapNo.getValue()==null?0:displayCostDataGroupList.get(scenarioTabIdx).overageScrapNo.getValue().longValue());
			controlModel.setCustomerProgramPct(displayCostDataGroupList.get(scenarioTabIdx).customerProgramPercentage.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).customerProgramPercentage.getValue().floatValue());
			controlModel.setCorrugateCost(displayCostDataGroupList.get(scenarioTabIdx).corrugateCostPerUnit.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).corrugateCostPerUnit.getValue().floatValue());
			controlModel.setFullfilmentCost(displayCostDataGroupList.get(scenarioTabIdx).fullfillmentCostPerUnit.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).fullfillmentCostPerUnit.getValue().floatValue());
			controlModel.setMiscCost(displayCostDataGroupList.get(scenarioTabIdx).miscCost.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).miscCost.getValue().floatValue());
			controlModel.setPalletCost(displayCostDataGroupList.get(scenarioTabIdx).palletCost.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).palletCost.getValue().floatValue());
			controlModel.setShipTestCost(displayCostDataGroupList.get(scenarioTabIdx).shipTestCost.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).shipTestCost.getValue().floatValue());
			controlModel.setTotalArtworkCost(displayCostDataGroupList.get(scenarioTabIdx).totalArtworkCost.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).totalArtworkCost.getValue().floatValue());
			controlModel.setPrintingPlateCost(displayCostDataGroupList.get(scenarioTabIdx).printingPlateCost.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).printingPlateCost.getValue().floatValue());
			controlModel.setDieCuttingCost(displayCostDataGroupList.get(scenarioTabIdx).dieCuttingCost.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).dieCuttingCost.getValue().floatValue());
			controlModel.setCtpCost(displayCostDataGroupList.get(scenarioTabIdx).cTPCost.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).cTPCost.getValue().floatValue());
			controlModel.setExpeditedFreightCost(displayCostDataGroupList.get(scenarioTabIdx).expectedFrieghtCost.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).expectedFrieghtCost.getValue().floatValue());
			controlModel.setFines(displayCostDataGroupList.get(scenarioTabIdx).fines.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).fines.getValue().floatValue());
			controlModel.setCustomerProgramPct(displayCostDataGroupList.get(scenarioTabIdx).customerProgramPercentage.getValue()==null?new Float(0.0):displayCostDataGroupList.get(scenarioTabIdx).customerProgramPercentage.getValue().floatValue());
			controlModel.setCaDate(costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsDate.getValue());
		}catch(Exception ex){
			System.out.println("=======CostAnalysisInfo:getCurrentDisplayCostModelFromControls error "+ ex.toString());
		}
		System.out.println("CostAnalysisInfo:getCurrentDisplayCostModelFromControls() ends");
		return controlModel;
	}

	public void save(Button btn, TabPanel panel, TabItem nextTab) {
		int scenarioTabIdx = vpScenarioPanelTabs.getSelectedItem().getTabIndex();
		System.out.println("CostAnalysisInfo:save starts");
		displayCostDataGroupList.get(scenarioTabIdx).setCostAnalysisInfo(getCostAnalysisInfo());
		if(displayCostDataGroupList.get(scenarioTabIdx).foreCastQty.getValue()!=null && displayCostDataGroupList.get(scenarioTabIdx).foreCastQty.getValue().longValue()> ZERO_LONG){
			if(displayCostDataGroupList.get(scenarioTabIdx).foreCastQty.getValue()!=null)displayScenarioDTOList.get(scenarioTabIdx).setQtyForecast(displayCostDataGroupList.get(scenarioTabIdx).foreCastQty.getValue().longValue());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).projectManager.getValue()!=null) displayScenarioDTOList.get(scenarioTabIdx).setDisplaySalesRepId(costAnalysisInfoGroupList.get(scenarioTabIdx).projectManager.getValue().getDisplaySalesRepId());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).manufacturer.getValue()!=null) displayScenarioDTOList.get(scenarioTabIdx).setManufacturerId(costAnalysisInfoGroupList.get(scenarioTabIdx).manufacturer.getValue().getManufacturerId()) ;
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).promoPeriod.getValue()!=null) displayScenarioDTOList.get(scenarioTabIdx).setPromoPeriodId(costAnalysisInfoGroupList.get(scenarioTabIdx).promoPeriod.getValue().getPromoPeriodId());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).topLevel.getValue()!=null) displayScenarioDTOList.get(scenarioTabIdx).setSku(costAnalysisInfoGroupList.get(scenarioTabIdx).topLevel.getValue());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsYear.getValue()!=null) displayScenarioDTOList.get(scenarioTabIdx).setPromoYear(costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsYear.getValue().longValue());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).status.getValue()!=null) displayScenarioDTOList.get(scenarioTabIdx).setScenariodStatusId(costAnalysisInfoGroupList.get(scenarioTabIdx).status.getValue().getStatusId());
			if(costAnalysisInfoGroupList.get(scenarioTabIdx).packLocation.getValue()!=null) displayScenarioDTOList.get(scenarioTabIdx).setPackoutVendorId(costAnalysisInfoGroupList.get(scenarioTabIdx).packLocation.getValue().getLocationId());
			if(displayCostDataGroupList.get(scenarioTabIdx).actualQty.getValue()!=null) displayScenarioDTOList.get(scenarioTabIdx).setActualQty(displayCostDataGroupList.get(scenarioTabIdx).actualQty.getValue().longValue());
			final MessageBox box = MessageBox.wait("Progress", "Saving display data for Scnario " + (scenarioTabIdx+1) + ", please wait...", "Saving...");
			displayScenarioDTOList.get(scenarioTabIdx).setDisplayId(display.getDisplayId());
			if (displayScenarioDTOList.get(scenarioTabIdx).getDisplayScenarioId() == null) {
				displayScenarioDTOList.get(scenarioTabIdx).setDtCreated(new Date());
				displayScenarioDTOList.get(scenarioTabIdx).setUserCreated(App.getUser().getUsername());
			}else{
				displayScenarioDTOList.get(scenarioTabIdx).setDtLastChanged(new Date());
				displayScenarioDTOList.get(scenarioTabIdx).setUserLastChanged(App.getUser().getUsername());
			}
			displayScenarioDTOList.get(scenarioTabIdx).setScenarioNum(new Long(scenarioTabIdx+1));
			AppService.App.getInstance().saveDisplayScenarioDTO(displayScenarioDTOList.get(scenarioTabIdx), new Service.SaveDisplayScenarioInfo(displayScenarioDTOList.get(scenarioTabIdx),  btn, box,this,panel,nextTab));
		}else{
			MessageBox.alert("Validation Error", "Invalid Forecast quantity" , null);
		}
		System.out.println("CostAnalysisInfo:save ends");
	}

	public void saveDisplayCost(DisplayScenarioDTO displayScenarioDTO, TabPanel panel, TabItem nextTab){
		int scenarioTabIdx = vpScenarioPanelTabs.getSelectedItem().getTabIndex();
		System.out.println("CostAnalysisInfo:saveDisplayCost starts");
		System.out.println("CostAnalysisInfo:saveDisplayCost displayScenarioDTO.getDisplayScenarioId()="+displayScenarioDTO.getDisplayScenarioId());
		fileUploadWindowGroupList.get(scenarioTabIdx).setDisplayScenarioId(displayScenarioDTO.getDisplayScenarioId());
		displayCostModels = getCostAnalysisInfo().getData(DISPLAY_COST_MODELS);
		if (displayCostModels == null) {
			displayCostModels = new Vector<DisplayCostDTO>();
		}
		DisplayCostDTO displayCostDTO =displayCostModels.size()>scenarioTabIdx?displayCostModels.get(scenarioTabIdx):null;
		if(displayCostDTO==null){
			/* Save Newly created scenario*/
			displayCostDTO=getCurrentDisplayCostModelFromControls(scenarioTabIdx);
			displayCostDTO.setDisplayId(this.display.getDisplayId());
			displayCostDTO.setDisplayCostId(null);
		}
		displayCostDTO.setDisplayScenarioId(displayScenarioDTO.getDisplayScenarioId());
		displayCostDataGroupList.get(scenarioTabIdx).saveDisplayCost(displayCostDTO,costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsDate.getValue(),costAnalysisInfoGroupList.get(scenarioTabIdx).infoDetailsProjectNo.getValue(), panel,  nextTab,displayScenarioDTO );
		System.out.println("CostAnalysisInfo:saveDisplayCost ends");
	}

	private CostAnalysisInfo getCostAnalysisInfo() {
		return this;
	}

	public void reload() {
		try{
			this.display = displayTabs.getDisplayModel();
			if (displayCostModel == null) {
				displayCostModel = new DisplayCostDTO();
			}
			if (this.display.getDisplayId() != null) {
				displayTabHeader.setDisplay(this.display);
			}
			if (this.display.getDisplayId() != null ) {
				AppService.App.getInstance().getDisplayCostDTOs(this.display,new Service.ReloadDisplayCosts(this.display,displayCostModel,this));
				overrideDirty = false;
			}
		}catch(Exception ex){
			System.out.println("CostAnalysisInfo:reload error "+ ex.toString());
		}
	}
	
	public void enableDisableButtons(DisplayDTO display, int tabIdx){
		System.out.println("CostAnalysisInfo:enableDisableButtons starts");
		 readOnly = this.getData("readOnly");
	     if (readOnly == null) readOnly = false;
				int tbx=-1;
				for( CheckBox chkskuMixFinal:lstSkuMixFinalCheckBox  ){
					tbx+=1;
					System.out.println("CostAnalysisInfo:enableDisableButtons index="+  tbx);
					chkskuMixFinal.setValue(display.isSkuMixFinalFlg());
					if( (display.isSkuMixFinalFlg()==null || !display.isSkuMixFinalFlg()) && !readOnly){
			         	lstBtnAdd.get(tbx).enable();
			        	lstBtnEdit.get(tbx).enable();
			        	lstBtnDelete.get(tbx).enable();
			        	lstBtnReCalculate.get(tbx).enable();
			        	lstBtnSaveScenario.get(tbx).enable();
			        	lstBtnCancelScenario.get(tbx).enable();
			        	lstBtnCreateScenario.get(tbx).enable();
			        	lstBtnCopyScenario.get(tbx).enable();
			        	lstBtnCancelComments.get(tbx).enable();
			        	lstBtnSaveComments.get(tbx).enable();
			        	fileUploadWindowGroupList.get(tbx).enableDisableButtons(false);
			        	displayCostDataGroupList.get(tbx).enableDisableButtons(false);
			        	costAnalysisInfoGroupList.get(tbx).enableDisableButtons(false);
			        	System.out.println("CostAnalysisInfo:enableDisableButtons readonly FALSE");
			        }else{
			        	lstBtnAdd.get(tbx).disable();
			         	lstBtnEdit.get(tbx).disable();
			         	lstBtnDelete.get(tbx).disable();
			         	lstBtnReCalculate.get(tbx).disable();
			         	lstBtnSaveScenario.get(tbx).disable();
			         	lstBtnCancelScenario.get(tbx).disable();
			         	lstBtnCreateScenario.get(tbx).disable();
			         	lstBtnCopyScenario.get(tbx).disable();
			         	lstBtnCancelComments.get(tbx).disable();
			        	lstBtnSaveComments.get(tbx).disable();
			        	fileUploadWindowGroupList.get(tbx).enableDisableButtons(true);
			        	displayCostDataGroupList.get(tbx).enableDisableButtons(true);
			        	costAnalysisInfoGroupList.get(tbx).enableDisableButtons(true);
			        	System.out.println("CostAnalysisInfo:enableDisableButtons readonly TRUE");
			        }
					if(readOnly){
						lstSkuMixFinalCheckBox.get(tbx).disable();
					}else{
						lstSkuMixFinalCheckBox.get(tbx).enable();
					}
			}
		System.out.println("CostAnalysisInfo:enableDisableButtons ends");
	}

	private void forwardToDisplayProjectSelection(){
		clearModel();
		App.getVp().clear();
		App.getVp().add(new DisplayProjectSelection());
	}

	private void clearModel() {
		if (display != null) display.removeChangeListener(displayListener);
		if (displayCostModel != null) displayCostModel.removeChangeListener(displayCostModelListener);
		display = null;
		displayCostModel = null;
		overrideDirty = false;
		displayTabs.resetDisplayModel();
		setData("display", null);
	}

	private final Listener<MessageBoxEvent> l = new Listener<MessageBoxEvent>() {
		public void handleEvent(MessageBoxEvent mbe) {
			Button btn = mbe.getButtonClicked();
			if (btn.getText().equals("Yes")) {
				save(btn, null, null);
				forwardToDisplayProjectSelection();
			}
			if (btn.getText().equals("No")) {
				forwardToDisplayProjectSelection();
			}
			if (btn.getText().equals("Cancel")) {
			}
		}
	};

	private final Listener<MessageBoxEvent> recalculateListener = new Listener<MessageBoxEvent>() {
		public void handleEvent(MessageBoxEvent mbe) {
			Button btn = mbe.getButtonClicked();
			if (btn.getText().equals("Yes")) {
				System.out.println("CostAnalysisInfo:reCalculateListener starts");
				int tabIndex=vpScenarioPanelTabs.getSelectedItem().getTabIndex();
				displayCostDataGroupList.get(tabIndex).reCalculatePriceException(displayTabs.getDisplayModel(),
						displayCostDataGroupList.get(tabIndex).customerProgramPercentage.getValue()==null?new Double(0.0):displayCostDataGroupList.get(tabIndex).customerProgramPercentage.getValue().doubleValue(), 
								reCalculateDate.getValue()==null?new Date():reCalculateDate.getValue(), displayScenarioDTOList.get(tabIndex),false);
			}
			if (btn.getText().equals("No")) {

			}
			if (btn.getText().equals("Cancel")) {

			}
		}
	};

	 public DisplayTabHeaderV2 getDisplayTabHeader() {
	    	return displayTabHeader;
	 }
	 
	public DisplayTabs getDisplayTabs() {
		return displayTabs;
	}

	public void setDisplayTabs(DisplayTabs displayTabs) {
		this.displayTabs = displayTabs;
	}

	public void loadScenarios(Long displayId){
		if (displayId != null && displayId > 0)
			AppService.App.getInstance().getDisplayScenarioDTO(displayId,new Service.LoadDisplayScenarioInfo(this));
	}

	public void saveApproveScenarioToDisplay(DisplayScenarioDTO displayScenarioDTO) {
		this.display = displayTabs.getDisplayModel();
		if (this.display.getDisplayId() != null) {
			displayTabHeader.setDisplay(this.display);
		}
		if (display == null) {
			MessageBox.alert("Validation Error", "No display found" , null);
			return;
		}
		if(displayScenarioDTO.getQtyForecast()!=null)this.display.setQtyForecast(displayScenarioDTO.getQtyForecast().longValue());
		if(displayScenarioDTO.getDisplaySalesRepId()!=null) this.display.setDisplaySalesRepId(displayScenarioDTO.getDisplaySalesRepId());
		if(displayScenarioDTO.getManufacturerId()!=null) this.display.setManufacturerId(displayScenarioDTO.getManufacturerId()) ;
		if(displayScenarioDTO.getPromoPeriodId()!=null) this.display.setPromoPeriodId(displayScenarioDTO.getPromoPeriodId());
		if(displayScenarioDTO.getSku()!=null) this.display.setSku(displayScenarioDTO.getSku());
		if(displayScenarioDTO.getPromoYear()!=null) this.display.setPromoYear(displayScenarioDTO.getPromoYear());
		if(displayScenarioDTO.getScenariodStatusId()!=null) this.display.setStatusId(displayScenarioDTO.getScenariodStatusId());
		/*
		 * Description : if(displayScenarioDTO.getPackoutVendorId()!=null) this.display.setPackoutVendorId(displayScenarioDTO.getPackoutVendorId());
		 * This code is commented, because there is no foregin key relation between display.packoutLocation and packoutLocationId
		 * Date : 12-12-2010
		 * By : Mari
		 */
		int scenarioTabIdx =displayScenarioDTO.getScenarioNum().intValue()-1;
		displayScenarioDTOList.get(scenarioTabIdx).setScenarioApprovalFlg(YES_FLG);
		if(displayScenarioDTO.getActualQty()!=null) this.display.setActualQty(displayScenarioDTO.getActualQty());
		AppService.App.getInstance().saveDisplayDTO(this.display, new Service.SaveDisplayAfterApproval(this,this.display,displayScenarioDTO,scenarioTabIdx));
		//saveSummaryCalculation(this.displayCostCompareGroupList, displayScenarioDTO,scenarioTabIdx);
		
	}
	
	public void saveSummaryCalculationAlerts(DisplayScenarioDTO displayScenarioDTO, int scenarioTabIdx){
		MessageBox.alert("displayCostDataGroupList:totalCogsVal", ""+displayCostDataGroupList.get(scenarioTabIdx).totalCogsVal, null);
		MessageBox.alert("displayCostDataGroupList:corrugateCostVal", ""+displayCostDataGroupList.get(scenarioTabIdx).corrugateCostVal, null);
		MessageBox.alert("displayCostDataGroupList:fullfillmentCostVal", ""+displayCostDataGroupList.get(scenarioTabIdx).fullfillmentCostVal, null);
		MessageBox.alert("displayCostDataGroupList:displayCostsVal", ""+displayCostDataGroupList.get(scenarioTabIdx).displayCostsVal, null);
		MessageBox.alert("displayCostDataGroupList:netSalesVal", ""+displayCostDataGroupList.get(scenarioTabIdx).netSalesVal, null);
		MessageBox.alert("displayCostDataGroupList:otherCostsVal", ""+displayCostDataGroupList.get(scenarioTabIdx).otherCostsVal, null);
		MessageBox.alert("displayCostDataGroupList:productVarMarginVal", ""+displayCostDataGroupList.get(scenarioTabIdx).productVarMarginVal, null);
		MessageBox.alert("displayCostDataGroupList:totalActualInvoiceTotCostVal", ""+displayCostDataGroupList.get(scenarioTabIdx).totalActualInvoiceTotCostVal, null);
		MessageBox.alert("displayCostDataGroupList:totalDisplayCostsVal", ""+displayCostDataGroupList.get(scenarioTabIdx).totalDisplayCostsVal, null);
		MessageBox.alert("displayCostDataGroupList:sumVmDollar", ""+displayCostDataGroupList.get(scenarioTabIdx).sumVmDollar, null);
		MessageBox.alert("displayCostDataGroupList:sumVmPct", ""+displayCostDataGroupList.get(scenarioTabIdx).sumVmPct, null);
		
	}
	
	
	public void saveSummaryCalculation(Vector<DisplayCostDataGroup> objDisplayCostDataGroupList, DisplayScenarioDTO displayScenarioDTO, int scenarioTabIdx){
		 /* Description : Storing data for Summary Calculation Report
		 * Date : 02-05-2011
		 * By : Mari
		 */
		try{
		System.out.println("CostAnalysisInfo:saveSummaryCalculation starts");
		calculationSummaryDTO= new CalculationSummaryDTO();
		calculationSummaryDTO.setDisplayId(displayScenarioDTO.getDisplayId());
		calculationSummaryDTO.setDisplayScenarioId(displayScenarioDTO.getDisplayScenarioId());
		calculationSummaryDTO.setActualQty(displayScenarioDTO.getActualQty());
		calculationSummaryDTO.setCustomerId(display.getCustomerId());
		//if(objDisplayCostDataGroupList.get(scenarioTabIdx).customerProgramPercentage.getValue()!=null) calculationSummaryDTO.setCustProgDisc(objDisplayCostDataGroupList.get(scenarioTabIdx).customerProgramPercentage.getValue().floatValue());
		calculationSummaryDTO.setDescription(display.getDescription());
		if(displayScenarioDTO.getQtyForecast()!=null) calculationSummaryDTO.setForeCastQty(displayScenarioDTO.getQtyForecast());
		if(displayScenarioDTO.getPromoPeriodId()!=null) calculationSummaryDTO.setPromoPeriodId(displayScenarioDTO.getPromoPeriodId());
		calculationSummaryDTO.setSku(displayScenarioDTO.getSku());
		calculationSummaryDTO.setStatusId(displayScenarioDTO.getScenariodStatusId());
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).corrugateCostPerUnitVal!=null) 
			calculationSummaryDTO.setCorrugateCostPerUnit(objDisplayCostDataGroupList.get(scenarioTabIdx).corrugateCostPerUnitVal.floatValue());
		else
			calculationSummaryDTO.setCorrugateCostPerUnit(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).fullfillmentCostPerUnitVal!=null) 
			calculationSummaryDTO.setLaborCostPerUnit(objDisplayCostDataGroupList.get(scenarioTabIdx).fullfillmentCostPerUnitVal.floatValue());
		else
			calculationSummaryDTO.setLaborCostPerUnit(0.00F);
		
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).totalCogsVal!=null) 
			calculationSummaryDTO.setSumCogs(objDisplayCostDataGroupList.get(scenarioTabIdx).totalCogsVal.floatValue());
		else
			calculationSummaryDTO.setSumCogs(0.00F);
		
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).corrugateCostVal!=null) 
			calculationSummaryDTO.setSumCorrugateCost(objDisplayCostDataGroupList.get(scenarioTabIdx).corrugateCostVal.floatValue());
		else
			calculationSummaryDTO.setSumCorrugateCost(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).fullfillmentCostVal!=null) 
			calculationSummaryDTO.setSumFulFillmentCost(objDisplayCostDataGroupList.get(scenarioTabIdx).fullfillmentCostVal.floatValue());
		else
			calculationSummaryDTO.setSumFulFillmentCost(0.00F);
		
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).displayCostsVal!=null) 
			calculationSummaryDTO.setSumDisplayCost(objDisplayCostDataGroupList.get(scenarioTabIdx).displayCostsVal.floatValue());
		else
			calculationSummaryDTO.setSumDisplayCost(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).tradeSalesVal!=null) 
			calculationSummaryDTO.setSumNetSalesBp(objDisplayCostDataGroupList.get(scenarioTabIdx).tradeSalesVal.floatValue());
		else
			calculationSummaryDTO.setSumNetSalesBp(0.00F);
		
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).netSalesValAp!=null) 
			calculationSummaryDTO.setSumNetSalesAp(objDisplayCostDataGroupList.get(scenarioTabIdx).netSalesValAp.floatValue());
		else
			calculationSummaryDTO.setSumNetSalesAp(0.00F);
		
		
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).custProgramDiscVal!=null) 
			calculationSummaryDTO.setCustProgDisc(objDisplayCostDataGroupList.get(scenarioTabIdx).custProgramDiscVal.floatValue());
		else
			calculationSummaryDTO.setCustProgDisc(0.00F);
		
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).otherCostsVal!=null) 
			calculationSummaryDTO.setSumOtherCost(objDisplayCostDataGroupList.get(scenarioTabIdx).otherCostsVal.floatValue());
		else
			calculationSummaryDTO.setSumOtherCost(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).productVarMarginVal!=null) 
			calculationSummaryDTO.setSumProdVarMargin(objDisplayCostDataGroupList.get(scenarioTabIdx).productVarMarginVal.floatValue());
		else
			calculationSummaryDTO.setSumProdVarMargin(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).totalActualInvoiceTotCostVal!=null) 
			calculationSummaryDTO.setSumTotActualInvoice(objDisplayCostDataGroupList.get(scenarioTabIdx).totalActualInvoiceTotCostVal.floatValue());
		else
			calculationSummaryDTO.setSumTotActualInvoice(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).totalDisplayCostsVal!=null) 
			calculationSummaryDTO.setSumTotDispCostNoShipping(objDisplayCostDataGroupList.get(scenarioTabIdx).totalDisplayCostsVal.floatValue());
		else
			calculationSummaryDTO.setSumTotDispCostNoShipping(0.00F);
		
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).sumVmDollar!=null) 
			calculationSummaryDTO.setSumVmDollarAp(objDisplayCostDataGroupList.get(scenarioTabIdx).sumVmDollar.floatValue());
		else
			calculationSummaryDTO.setSumVmDollarAp(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).sumVmPct!=null) 
			calculationSummaryDTO.setSumVmPctAp(objDisplayCostDataGroupList.get(scenarioTabIdx).sumVmPct.floatValue());
		else
			calculationSummaryDTO.setSumVmPctAp(0.00F);
		
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).sumVmDollarBp!=null) 
			calculationSummaryDTO.setSumVmDollarBp(objDisplayCostDataGroupList.get(scenarioTabIdx).sumVmDollarBp.floatValue());
		else
			calculationSummaryDTO.setSumVmDollarBp(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).sumVmPctBp!=null) 
			calculationSummaryDTO.setSumVmPctBp(objDisplayCostDataGroupList.get(scenarioTabIdx).sumVmPctBp.floatValue());
		else
			calculationSummaryDTO.setSumVmPctBp(0.00F);
		
		String vmMarginalAmount = AppService.App.getDmmConstants().vmAmount();
		String vmMarginalPct = AppService.App.getDmmConstants().vmPct();
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).vmAmountVal<new Double(vmMarginalAmount)){
			if(objDisplayCostDataGroupList.get(scenarioTabIdx).vmAmountVal!=null) 
				calculationSummaryDTO.setVmDollarHurdleRed(objDisplayCostDataGroupList.get(scenarioTabIdx).vmAmountVal.floatValue());
			else
				calculationSummaryDTO.setVmDollarHurdleRed(0.00F);
		}else{
			if(objDisplayCostDataGroupList.get(scenarioTabIdx).vmAmountVal!=null) 
				calculationSummaryDTO.setVmDollarHurdleGreen(objDisplayCostDataGroupList.get(scenarioTabIdx).vmAmountVal.floatValue());
			else
				calculationSummaryDTO.setVmDollarHurdleGreen(0.00F);
		}
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).vmPercentageVal<new Double(vmMarginalPct)){
			if(objDisplayCostDataGroupList.get(scenarioTabIdx).vmPercentageVal!=null) 
				calculationSummaryDTO.setVmPctHurdleRed(objDisplayCostDataGroupList.get(scenarioTabIdx).vmPercentageVal.floatValue());
			else
				calculationSummaryDTO.setVmPctHurdleRed(0.00F);
		}else{
			if(objDisplayCostDataGroupList.get(scenarioTabIdx).vmPercentageVal!=null) 
				calculationSummaryDTO.setVmPctHurdleGreen(objDisplayCostDataGroupList.get(scenarioTabIdx).vmPercentageVal.floatValue());
			else
				calculationSummaryDTO.setVmPctHurdleGreen(0.00F);
		}
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).tradeSalesVal!=null) 
			calculationSummaryDTO.setSumTradeSales(objDisplayCostDataGroupList.get(scenarioTabIdx).tradeSalesVal.floatValue());
		else
			calculationSummaryDTO.setSumTradeSales(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).excessVmPercentageVal!=null) 
			calculationSummaryDTO.setExcessInvtVmPct(objDisplayCostDataGroupList.get(scenarioTabIdx).excessVmPercentageVal.floatValue());
		else
			calculationSummaryDTO.setExcessInvtVmPct(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).excessVmAmountVal!=null) 
			calculationSummaryDTO.setExcessInvtVmDollar(objDisplayCostDataGroupList.get(scenarioTabIdx).excessVmAmountVal.floatValue());
		else
			calculationSummaryDTO.setExcessInvtVmDollar(0.00F);
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).mdfPct.getValue() !=null) 
			calculationSummaryDTO.setMdfPct
					((objDisplayCostDataGroupList.get(scenarioTabIdx).mdfPctVal.floatValue() *  
							objDisplayCostDataGroupList.get(scenarioTabIdx).tradeSalesVal.floatValue()));
		else
			calculationSummaryDTO.setMdfPct(0.00F);
		
		if(objDisplayCostDataGroupList.get(scenarioTabIdx).discountPct.getValue() !=null) 
			calculationSummaryDTO.setDiscountPct((objDisplayCostDataGroupList.get(scenarioTabIdx).discountPctVal.floatValue() *  
					objDisplayCostDataGroupList.get(scenarioTabIdx).tradeSalesVal.floatValue()));
		else
			calculationSummaryDTO.setDiscountPct(0.00F);
		AppService.App.getInstance().saveCalculationSummaryDTO(calculationSummaryDTO, 
				new Service.SaveCalculationSummary(this,calculationSummaryDTO));
		}catch(Exception e){
			System.out.println("CostAnalysisInfo:saveSummaryCalculation error "+e.toString());
			MessageBox.alert("Check..Error..", "CostAnalysisInfo:saveSummaryCalculation Error" + e.toString() , null);
		}
		System.out.println("CostAnalysisInfo:saveSummaryCalculation ends");
	}
	
	public CheckBox getSkuMixFinalCheckBox() {
		return skuMixFinalCheckBox;
	}

	public void setSkuMixFinalCheckBox(CheckBox skuMixFinalCheckBox) {
		this.skuMixFinalCheckBox = skuMixFinalCheckBox;
	}
		
}
