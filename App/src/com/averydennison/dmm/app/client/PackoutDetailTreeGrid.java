package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

//import com.allen_sauer.gwt.log.client.Log;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayDcAndPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.client.models.Folder;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.treegrid.EditorTreeGrid;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGridCellRenderer;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGridSelectionModel;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Element;

/**
 * User: Alvin Wong
 * Date: Dec 14, 2009
 * Time: 17:20:00 PM
 */
public class PackoutDetailTreeGrid extends LayoutContainer {
    public static final String ADD_DC = "Add Packout Loc";
    public static final String ADD_PACKOUT_DETAIL = "Add Packout Detail";
    public static final String DELETE = "Delete";
    public static final String SHOW_DATA = "Show Data";
    public static final String SELECT = "Select";
    public static final String CANCEL = "Cancel";
    
    public static final int PACKOUT_COL_ZERO=0; //Packout Location
    public static final int PACKOUT_COL_ONE=1; //Ship From Location
    
    public static final int PACKOUT_COL_FOUR=4; //Ship From Date
    public static final int PACKOUT_COL_FIVE=5;//ShipLoc Product Due Date
    public static final int PACKOUT_COL_SIX=6; //Product Due Date
    public static final int PACKOUT_COL_SEVEN=7; //Corr Due Date

    public static final int PACKOUT_COL_EIGHT=8; //Corr Due Date

    
    public static final int PACKOUT_COL_ELEVEN=11;//PO Quantity
    
    public static final int TWENTY_ONE_DAYS=21;
    public static final int SEVEN_DAYS=7;
    
    
    
    private VerticalPanel vp;
    private DisplayDTO displayDTO;
    private Boolean readOnly = false;
    private Window window = new Window();
    private PackoutDetailInfo packoutDetailInfo;

    private EditorTreeGrid<ModelData> tree;
    private Grid<DisplayShipWaveDTO> shipWaveGrid;
    private TreeStore<ModelData> store = new TreeStore<ModelData>();
    private ListStore<LocationCountryDTO> locationsCountry = new ListStore<LocationCountryDTO>();
    private List<DisplayDcAndPackoutDTO> displayDcAndPackoutList = new ArrayList<DisplayDcAndPackoutDTO>();
    
    // validation properties
    private Long sumDCNodes;
    private HashMap<String, Long> dcQty = new HashMap<String, Long>();
    private HashMap<String, Long> sumPackoutByDc = new HashMap<String, Long>();
    private HashMap<Integer, Long> sumPackoutByShipWave = new HashMap<Integer, Long>();
    // form buttons
    private Button btnAddDC;
    private Button btnAddPackoutDetail;
    private Button btnDelete;
    private boolean displayPackoutModified=false;

	public PackoutDetailTreeGrid(DisplayDTO display, PackoutDetailInfo packoutDetailInfo, Boolean readOnly) {
        vp = new VerticalPanel();
		this.readOnly = readOnly;
		this.displayDTO = display;
        setLayout(new FlowLayout(0));
		this.packoutDetailInfo = packoutDetailInfo;
    }

    public TreeStore<ModelData> getStore() {
    	return store;
    }
    
    public void setDisplayDcAndPackoutList(List<DisplayDcAndPackoutDTO> list) {
    	this.displayDcAndPackoutList = list;
    }
    
    public List<DisplayDcAndPackoutDTO> getDisplayDcAndPackoutList() {
    	getTreeData();
    	return this.displayDcAndPackoutList;
    }
    
    public Boolean isDirty() {
    	getTreeData();
    	for (int i = 0; i < displayDcAndPackoutList.size(); i++) {
    		Boolean dirty = displayDcAndPackoutList.get(i).isDirty();
    		if (dirty != null && dirty) return true;
    	}
    	return false;
    }
    
    
    public boolean isDisplayPackoutModified() {
		return displayPackoutModified;
	}

	public void setDisplayPackoutModified(boolean displayPackoutModified) {
		this.displayPackoutModified = displayPackoutModified;
	}

	@Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        setModelsAndView();
        createForm();
        setPermissions();
        setReadOnly();
        add(vp);
    }

    private void setModelsAndView() {
    	
    	
    }

    public void clearModel() {
    	tree.removeAllListeners();
    	shipWaveGrid.removeAllListeners();
    	this.removeAllListeners();
    }
    
    private void setPermissions(){
    	if(!App.isAuthorized("PackoutDetailInfo.SAVE")) {
    		btnAddDC.disable();
    		btnAddPackoutDetail.disable();
    		btnDelete.disable();
    	}
    }

    private void setReadOnly() {
        if (readOnly == null) return;
        if (readOnly) {
    		btnAddDC.disable();
    		btnAddPackoutDetail.disable();
    		btnDelete.disable();
        }
    }

    private ComboBox<LocationCountryDTO> dcCombo = new ComboBox<LocationCountryDTO>();
    private ComboBox<LocationCountryDTO> shipFromLocationCombo = new ComboBox<LocationCountryDTO>();
    private void createForm() {
        addPackoutDetailDialog();
        // reload function will populate tree grid
        AppService.App.getInstance().getLocationCountryDTOs(new Service.LoadLocationCountryStore(displayDTO.getCountryId(),locationsCountry));
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        
       // final ComboBox<LocationCountryDTO> dcCombo = new ComboBox<LocationCountryDTO>();
        dcCombo.setFieldLabel("Location");
        dcCombo.setDisplayField(LocationCountryDTO.LOCATION_NAME);
        dcCombo.setValueField(LocationCountryDTO.LOCATION_CODE);
        dcCombo.setWidth(100);
        dcCombo.setStore(locationsCountry);
        dcCombo.setAllowBlank(false);
        dcCombo.setTypeAhead(true);
        dcCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
        dcCombo.setMinListWidth(270);
        
        // final ComboBox<LocationCountryDTO> shipFromLocationCombo = new ComboBox<LocationCountryDTO>();
        shipFromLocationCombo.setFieldLabel("ShipFromLocation");
        shipFromLocationCombo.setDisplayField(LocationCountryDTO.LOCATION_NAME);
        shipFromLocationCombo.setValueField(LocationCountryDTO.LOCATION_CODE);
        shipFromLocationCombo.setWidth(100);
        shipFromLocationCombo.setStore(locationsCountry);
        shipFromLocationCombo.setAllowBlank(false);
        shipFromLocationCombo.setTypeAhead(true);
        shipFromLocationCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
        shipFromLocationCombo.setMinListWidth(270);
        
        CellEditor dcComboEditor = new CellEditor(dcCombo) {
            @Override
            public Object preProcessValue(Object name) {
              if (name == null) {
                return null;
              }
              String valueAttr = ((ComboBox<LocationCountryDTO>)dcCombo).getValueField();
              if (valueAttr == null)
                valueAttr = "value";
              return ((ComboBox<LocationCountryDTO>)dcCombo).getStore().findModel(valueAttr,
                                                            name);
            }

            @Override
            public Object postProcessValue(Object value) {
              if (value == null) {
                return null;
              }
              String valueAttr = ((ComboBox<LocationCountryDTO>)dcCombo).getValueField();
              if (valueAttr == null)
                valueAttr = "value";
              return ((LocationCountryDTO)value).get(valueAttr);
            }
          }; 

          CellEditor dcComboShipFromLocEditor = new CellEditor(shipFromLocationCombo) {
              @Override
              public Object preProcessValue(Object name) {
                if (name == null) {
                  return null;
                }
                String valueAttr = ((ComboBox<LocationCountryDTO>)shipFromLocationCombo).getValueField();
                if (valueAttr == null)
                  valueAttr = "value";
                return ((ComboBox<LocationCountryDTO>)shipFromLocationCombo).getStore().findModel(valueAttr,
                                                              name);
              }

              @Override
              public Object postProcessValue(Object value) {
                if (value == null) {
                  return null;
                }
                String valueAttr = ((ComboBox<LocationCountryDTO>)shipFromLocationCombo).getValueField();
                if (valueAttr == null)
                  valueAttr = "value";
                return ((LocationCountryDTO)value).get(valueAttr);
              }
            }; 
        
          
            
        //Col 0    
	    ColumnConfig dcCode = new ColumnConfig(DisplayDcAndPackoutDTO.DC_CODE, "<center>Packout<br>Location</center>", 10);  
	    dcCode.setAlignment(HorizontalAlignment.CENTER);
	    dcCode.setRenderer(new TreeGridCellRenderer<ModelData>());   
        if (!readOnly){
        	dcCode.setEditor(dcComboEditor);
        }
        //dcCode.setWidth(135);
        dcCode.setWidth(105);
	    configs.add(dcCode);
	    
	    //Col 1
	    ColumnConfig colShipFromLoc = new ColumnConfig(DisplayDcAndPackoutDTO.SHIP_FROM_DCCODE, "<center>Ship<br>From<br>Loc</center>", 10); 
	    colShipFromLoc.setAlignment(HorizontalAlignment.CENTER);

        if (!readOnly){
        	colShipFromLoc.setEditor(dcComboShipFromLocEditor);
        }
        //colShipFromLoc.setWidth(135);
        colShipFromLoc.setWidth(105);
	    configs.add(colShipFromLoc);
	    
	    //Col 2
	    ColumnConfig shipWave = new ColumnConfig(DisplayDcAndPackoutDTO.SHIP_WAVE_NUM, "<center>Ship<br>Wave</center>", 10);   
	    shipWave.setAlignment(HorizontalAlignment.CENTER);
	    shipWave.setWidth(40);
	    configs.add(shipWave);
	    
	    //Col 3
	    ColumnConfig mustArrive = new ColumnConfig(DisplayDcAndPackoutDTO.MUST_ARRIVE_DATE, "<center>Must<br>Arrive<br>By Date</center>", 10); 
	    mustArrive.setAlignment(HorizontalAlignment.CENTER);

	    mustArrive.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
	    mustArrive.setWidth(53);
	    configs.add(mustArrive);
	    
	    //Col 4
	    DateField shipFromDateField = new DateField();
        shipFromDateField.getPropertyEditor().setFormat(DateTimeFormat.getShortDateFormat());
	    ColumnConfig shipFrom = new ColumnConfig(DisplayDcAndPackoutDTO.SHIP_FROM_DATE, "<center>Ship<br>From<br>Date</center>", 10);  
	    shipFrom.setAlignment(HorizontalAlignment.CENTER);
	    shipFrom.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        if (!readOnly){
        	shipFrom.setEditor(new CellEditor(shipFromDateField));
    	}
        shipFrom.setAlignment(Style.HorizontalAlignment.LEFT);
	    shipFrom.setWidth(53);
	    configs.add(shipFrom);
	    
	    //Col 5
	    DateField shipLocDueDateField = new DateField();
	    shipLocDueDateField.getPropertyEditor().setFormat(DateTimeFormat.getShortDateFormat());
	    ColumnConfig shipLocDue = new ColumnConfig(DisplayDcAndPackoutDTO.SHIP_LOC_PRODUCT_DUE_DATE, "<center>Ship Loc<br>Product<br>Due Date</center>", 10);
	    shipLocDue.setAlignment(HorizontalAlignment.CENTER);
	    shipLocDue.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        if (!readOnly){
        	shipLocDue.setEditor(new CellEditor(shipLocDueDateField));
        }
        shipLocDue.setAlignment(Style.HorizontalAlignment.LEFT);
        shipLocDue.setWidth(58);
	    configs.add(shipLocDue);
	    
	    //Col 6
	    DateField prodDueDateField = new DateField();
        prodDueDateField.getPropertyEditor().setFormat(DateTimeFormat.getShortDateFormat());
	    ColumnConfig prodDue = new ColumnConfig(DisplayDcAndPackoutDTO.PRODUCT_DUE_DATE, "<center>Packout<br>Product<br>Due Date</center>", 10); 
	    prodDue.setAlignment(HorizontalAlignment.CENTER);
	    prodDue.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        if (!readOnly){
        	prodDue.setEditor(new CellEditor(prodDueDateField));
        }
	    prodDue.setAlignment(Style.HorizontalAlignment.LEFT);
	    prodDue.setWidth(58);
	    configs.add(prodDue);

	    //Col 7
	    DateField corDueDateField = new DateField();
	    corDueDateField.getPropertyEditor().setFormat(DateTimeFormat.getShortDateFormat());
	    ColumnConfig corDue = new ColumnConfig(DisplayDcAndPackoutDTO.CORRUGATE_DUE_DATE, "<center>Corrugate<br>Due Date</center>", 10);  
	    corDue.setAlignment(HorizontalAlignment.CENTER);
	    corDue.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        if (!readOnly){
        	corDue.setEditor(new CellEditor(corDueDateField));
        }
        corDue.setAlignment(Style.HorizontalAlignment.LEFT);
        corDue.setWidth(65);
	    configs.add(corDue);
	    
	    //Col 8
	    DateField deliveryDateField = new DateField();
	    deliveryDateField.getPropertyEditor().setFormat(DateTimeFormat.getShortDateFormat());
	    ColumnConfig deliDate = new ColumnConfig(DisplayDcAndPackoutDTO.DELIVERY_DATE, "<center>Actual<br>Delivery<br>Date</center>", 10);  
	    deliDate.setAlignment(HorizontalAlignment.CENTER);
	    deliDate.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        if (!readOnly){
        	deliDate.setEditor(new CellEditor(deliveryDateField));
        }
        deliDate.setAlignment(Style.HorizontalAlignment.LEFT);
        deliDate.setWidth(60);
	    configs.add(deliDate);
	    
	    //Col 9
        NumberField poQtyField = new NumberField();
	    poQtyField.setValidator(new Validator() {
            public String validate(Field<?> field, String value) {
                if (Long.valueOf(value.trim()).equals(0L)) return "Invalid Qty";
                return null;
            }
        });
	    poQtyField.setPropertyEditorType(Long.class);
	    ColumnConfig poQty = new ColumnConfig(DisplayDcAndPackoutDTO.PO_QUANTITY, "<center>Qty</center>", 10);   
        if (!readOnly){
        	poQty.setEditor(new CellEditor(poQtyField));
        }
	    poQty.setWidth(38);
	    configs.add(poQty);
	    NumberField balanceField = new NumberField();
	    balanceField.setPropertyEditorType(Long.class);
	    
	    //Col 10
	    ColumnConfig balance = new ColumnConfig(DisplayDcAndPackoutDTO.PO_QTY_BALANCE, "<center>PO<br>Qty</center>", 10);  

	    balance.setWidth(38);
	    configs.add(balance);
        ColumnModel cm = new ColumnModel(configs);
        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setHeaderVisible(false);
        cp.setLayout(new FitLayout());
        cp.setFrame(false);
        cp.setSize(680, 300);
        tree = new EditorTreeGrid<ModelData>(store, cm);
        tree.setClicksToEdit(ClicksToEdit.ONE);   
        tree.setBorders(true);   
        tree.setSize(670, 400);  
        
        tree.setTrackMouseOver(false);
        tree.setSelectionModel(new TreeGridSelectionModel<ModelData>());
        tree.addListener(Events.AfterEdit, new Listener<GridEvent<ModelData>>() {
        	public void handleEvent(GridEvent<ModelData> be) {
        		ModelData item = be.getModel();
        		setDisplayPackoutModified(true);
          		if (item instanceof Folder) {
          			if (be.getColIndex() == 0) {
          				String dcCode = ((Folder)item).getDcCode();
      					 Long locationId = getLocationId(dcCode);
      					((Folder)item).setLocationid(locationId);
      					// update children
      					List<ModelData>children = tree.getTreeStore().getChildren(item);
      					 Long shipFromLocationId =null;
      					for (int i = 0; i < children.size(); i++) {
      						DisplayDcAndPackoutDTO dap = ((DisplayDcAndPackoutDTO)children.get(i));
      						dap.setDcCode(dcCode);
      						dap.setLocationId(locationId);
      						if(dap.getShipFromDcCode()!=null && dap.getShipFromDcCode().toString().trim().length() >0){
      							shipFromLocationId = getLocationId(dap.getShipFromDcCode()); 
      							dap.setShipFromLocationId(shipFromLocationId);
      						}
      					}          				 
          			}
          		}else{
          			/*
          			 * Edit packout data
          			 * If ShipFromDate is edited and Product Due date is empty
          			 * assign product due date with 21 days prior to shipFromDate
          			 * assign corrugate due date with 28 days (21+7) prior to shipFromDate
          			 */
          			DisplayDcAndPackoutDTO dap = ((DisplayDcAndPackoutDTO)item);
          			Date corrugateDate=null;
          			Date productDueDate=null;
          			Date shipLocProductDueDate=null;

          			String dapDcCode = dap.getDcCode();
          			Long dapShipWaveNum = dap.getShipWaveNum();
          			String dapShipFromDcCode=dap.getShipFromDcCode();
          			if (shipWaveExistsInDC(dapDcCode, dapShipWaveNum, dapShipFromDcCode)) {//Deplicate ship wave num and ship from loc record for the packout
      					MessageBox.alert("Validation Error", "Duplicate Packout Loc, Ship From Loc, Ship Wave combo exists [" + dapDcCode + "," +  dapShipFromDcCode + "," +  dapShipWaveNum + "]", null);
	              	 }else{																	//Not a duplicate ship wave num and ship from loc record for the packout
	          			if (be.getColIndex() == PACKOUT_COL_FOUR) {
	          				
	          				if((dap.getShipLocProductDueDate()==null || dap.getShipLocProductDueDate().toString().trim().length()==0) && 
		                  			(dap.getShipFromDate()!=null || dap.getShipFromDate().toString().trim().length()>0))
		          			{
	          					shipLocProductDueDate=new Date(dap.getShipFromDate().getTime());
	          					shipLocProductDueDate.setDate(shipLocProductDueDate.getDate() - SEVEN_DAYS);
		          				dap.setShipLocProductDueDate(shipLocProductDueDate);
		          			}
	          				if((dap.getProductDueDate()==null || dap.getProductDueDate().toString().trim().length()==0) && 
	                  			(dap.getShipFromDate()!=null || dap.getShipFromDate().toString().trim().length()>0))
	          				{
	          					productDueDate=new Date(dap.getShipFromDate().getTime());
	          					productDueDate.setDate(productDueDate.getDate() - TWENTY_ONE_DAYS);
	          				    dap.setProductDueDate(productDueDate);
	          				}
	          				if((dap.getCorrugateDueDate()==null || dap.getCorrugateDueDate().toString().trim().length()==0) && 
	          				(dap.getProductDueDate()!=null || dap.getProductDueDate().toString().trim().length()>0))
	          				{
	          					corrugateDate=new Date(dap.getProductDueDate().getTime());
	          					corrugateDate.setDate(corrugateDate.getDate() - SEVEN_DAYS);
	          					dap.setCorrugateDueDate(corrugateDate);
	          				}
	          			}
	          			if (be.getColIndex() == PACKOUT_COL_SIX) {
	          				if((dap.getCorrugateDueDate()==null || dap.getCorrugateDueDate().toString().trim().length()==0) && 
	              			(dap.getProductDueDate()!=null || dap.getProductDueDate().toString().trim().length()>0))
	          				{	
	          					corrugateDate=new Date(dap.getProductDueDate().getTime());
	          					corrugateDate.setDate(corrugateDate.getDate() - SEVEN_DAYS);
	          					dap.setCorrugateDueDate(corrugateDate);
	          				}
	          			}
             		 }
          		}
        		updateTreeData();
        	}
        });
        tree.addListener(Events.BeforeEdit, new Listener<GridEvent<ModelData>>() {
       	public void handleEvent(GridEvent<ModelData> be) {
   		 if (be.getModel() instanceof Folder) {
   			 if (be.getColIndex() == PACKOUT_COL_ONE || be.getColIndex() == PACKOUT_COL_FOUR || be.getColIndex() == PACKOUT_COL_FIVE || be.getColIndex() == PACKOUT_COL_SIX || be.getColIndex() == PACKOUT_COL_SEVEN || be.getColIndex() == PACKOUT_COL_EIGHT|| be.getColIndex() == PACKOUT_COL_ELEVEN) be.setCancelled(true);
   		 }
   		 else if (be.getModel() instanceof DisplayDcAndPackoutDTO) {
   			 if (be.getColIndex() == PACKOUT_COL_ZERO) be.setCancelled(true);
   		 }
       	}
       });
       cp.add(tree);
       SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
            public void componentSelected(ButtonEvent ce) {
                Button button = (Button) ce.getComponent();
                if (button.getText().equals(ADD_DC)) {
                	addNewDC();
                }
                else if (button.getText().equals(ADD_PACKOUT_DETAIL)) {
              		if (tree.getSelectionModel().getSelectedItem() == null) {
                        MessageBox.alert("Add Packout Detail", "Please select a DC to add this detail to...", null);              			
              		}
              		else {
              			int idx = tree.getTreeStore().indexOf(tree.getSelectionModel().getSelectedItem());
              			//System.out.println("Tree item index = " + idx);
              			window.show();
              		}
                }
                else if (button.getText().equals(DELETE)) {
              		if (tree.getSelectionModel().getSelectedItem() == null) {
                        MessageBox.alert("Delete Packout Detail", "Please select an item to delete...", null);              			
              		}
              		else {
                    	String deleteWarning = "";
	              		ModelData item = tree.getSelectionModel().getSelectedItem();
	              		if (item instanceof Folder) {
	              			deleteWarning = "Deleting DC " + ((Folder)item).getDcCode() + " will also remove it's packout detail. Are you sure you want to delete this DC?";
	              		}
	              		else if (item instanceof DisplayDcAndPackoutDTO) {
	              			deleteWarning = "Are you sure you want to delete this packout detail record?";
	              		}
                    	Listener<MessageBoxEvent>cb = new Listener<MessageBoxEvent>() {
                    		public void handleEvent(MessageBoxEvent mbe) {
                    			String id = mbe.getButtonClicked().getItemId();
                    			if (Dialog.YES == id) {
                                	String rowId = "";
            	              		ModelData item = tree.getSelectionModel().getSelectedItem();
            	              		if (item instanceof Folder) {
            	              			// get children
            	              			List<ModelData>children = ((Folder)item).getChildren();
            	              			for (int i = 0; i < children.size(); i++) {            	              				
            	              				DisplayDcAndPackoutDTO child = (DisplayDcAndPackoutDTO)children.get(i);
                		              		rowId = child.getRowId();
                	              			DisplayDcAndPackoutDTO dap = displayDcAndPackoutList.get(getDisplayDcAndPackoutDTO(rowId));
                		              		dap.setDeleteFlg("Y");
                		              		dap.setDirty(true);
                	              			store.remove(item, child);
            	              			}
            	              			// mark parent for delete
            	              			rowId = ((Folder)item).getRowId();
            	              			DisplayDcAndPackoutDTO dap = displayDcAndPackoutList.get(getDisplayDcAndPackoutDTO(rowId));
            		              		dap.setDeleteFlg("Y");
            		              		dap.setDirty(true);

            		              		//reloadGrid(null);
            	              			store.remove(item);
            	              			store.commitChanges();
            	              			tree.getTreeView().refresh(true);
            	              			updateTreeData();
            	              		}
            	              		else if (item instanceof DisplayDcAndPackoutDTO) {
            	              			rowId = ((DisplayDcAndPackoutDTO)item).getRowId();
                	              		if (rowId != null && rowId.length() > 0) {
                	              			DisplayDcAndPackoutDTO dap = displayDcAndPackoutList.get(getDisplayDcAndPackoutDTO(rowId));
                		              		dap.setDeleteFlg("Y");
                		              		dap.setDirty(true);

               		                		ModelData parent = tree.getTreeStore().getParent(item);
               		                		//reloadGrid(parent);
                	              			store.remove(parent, item);
                	              			store.commitChanges();
                	              			tree.getTreeView().refresh(true);
                		              		updateTreeData();
                	              		}
            	              		}
                    			}
                    		}
                    	};
                    	MessageBox.confirm("Delete?", deleteWarning, cb);
              		}
                }
            }
        };

        btnAddDC = new Button(ADD_DC, listener);
        btnAddPackoutDetail = new Button(ADD_PACKOUT_DETAIL, listener);
        btnDelete = new Button(DELETE, listener);

        cp.addButton(btnAddDC);
        cp.addButton(btnAddPackoutDetail);
        cp.addButton(btnDelete);
       
        dcCombo.addListener(Events.Expand, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
            	System.out.println("PackoutDetailTreeGrid:packLocation countryId="+App.getDisplayTabs().getDisplayModel());
            	if(App.getDisplayTabs().getDisplayModel().getCountryId()!=null){
            		locationsCountry.filter(LocationCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(App.getDisplayTabs().getDisplayModel().getCountryId()).getCountryName());
            	}
            	dcCombo.setEnabled(true);
            }
        });
        
        shipFromLocationCombo.addListener(Events.Expand, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
            	System.out.println("PackoutDetailTreeGrid:shipFromLocationCombo countryId="+App.getDisplayTabs().getDisplayModel());
            	if(App.getDisplayTabs().getDisplayModel().getCountryId()!=null){
            		locationsCountry.filter(LocationCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(App.getDisplayTabs().getDisplayModel().getCountryId()).getCountryName());
            	}
            	shipFromLocationCombo.setEnabled(true);
            }
        });
        if (readOnly) {
        	btnAddDC.disable();
        	btnAddPackoutDetail.disable();
        	btnDelete.disable();
        }

        vp.add(cp);
    }
	
	public void reload() {
		System.out.println("PackoutDetailTreeGrid.readLoad starts");
		if(displayDTO.getDisplayId()==null){
			displayDTO=packoutDetailInfo.getDisplayTabs().getDisplayModel();
		}
        AppService.App.getInstance().loadDisplayDcAndPackouts(displayDTO, new Service.LoadDisplayDcAndPackoutList(store, this, packoutDetailInfo));
		System.out.println("PackoutDetailTreeGrid.readLoad ends");
	}	
	
	private void reloadGrid(ModelData parent) {
        // build tree model
        Folder root = new Folder("root", 0L, "", 0L);
        Folder node = null;
        try{
		Iterator<DisplayDcAndPackoutDTO>iter = displayDcAndPackoutList.iterator();
		while (iter.hasNext()) {
			DisplayDcAndPackoutDTO dap = (DisplayDcAndPackoutDTO)iter.next();
			String deleted = dap.getDeleteFlg();
			if (deleted == null || deleted.equals("N")) {
				if (dap.getType().equals("DC")) {
					node = new Folder(dap.getDcCode(), dap.getDcQuantity(), dap.getRowId(), dap.getLocationId());
					root.add((Folder) node);
				}
				else if (dap.getType().equals("Packout")) {    				
	    			node.add(dap);
				}
			}
		}
        }catch(Exception ex){
        	//Log.info("PackoutDetailTreeGrid:reloadGrid(ModelData parent) error.... :" + ex.toString());
        	System.out.println("PackoutDetailTreeGrid:reloadGrid(ModelData parent) error.... :" + ex.toString());
        }
       
        store.removeAll();
        store.commitChanges();
        store.add(root.getChildren(), true);
        store.commitChanges();	

        if (parent != null) {
        	if (parent instanceof Folder)
        		System.out.println("reload grid: parent = " + ((Folder)parent).getDcCode());
        	else 
        		System.out.println("reload grid: parent is not a folder ");
        	
        	tree.setExpanded(parent, true, true);
        	//tree.getTreeView().
        }
	}
	
	public Boolean validateControls() {
        tree.getTreeStore().commitChanges();
		List<ModelData> l = tree.getTreeStore().getModels();
        Iterator<ModelData> iter = l.iterator();
        while (iter.hasNext()) {
        	ModelData item = (ModelData)iter.next();
            
      		if (item instanceof Folder) {
      			String dcCode = ((Folder) item).getDcCode();
				Long qty = ((Folder) item).getPoQuantity();

      			if (dcCode == null || dcCode.length() == 0) {
                    MessageBox.alert("Validation Error", "A blank DC record exists. Please select a DC.", null);
      				return false;
      			}
      			if (qty == null) {
                    MessageBox.alert("Validation Error", "There is no quantity defined for DC " + dcCode, null);
      				return false;
      			}
      		}
      		else if (item instanceof DisplayDcAndPackoutDTO) {
      			String dcCode = ((DisplayDcAndPackoutDTO) item).getDcCode();
      			Long shipWaveNum = ((DisplayDcAndPackoutDTO) item).getShipWaveNum();
      			Long qty = ((DisplayDcAndPackoutDTO) item).getPoQuantity();
      			if (qty == null) {
                    MessageBox.alert("Validation Error", "There is no Packout Detail quantity defined for DC " + dcCode + ", Ship Wave " + shipWaveNum, null);
      				return false;
      			}      			
      		}        
        }		
		return true;
	}

	
	public Boolean crossValidatePackoutDetailsVsShipWaves() {return true;}
	public Boolean compareDCNodeTotalQtyAgainstPackoutDetailsQty() {return true;}
	public void saveDisplayPackouts() {}
	public void saveDCNodes() {}
	public void loadDCNodes() {}


	
	private void addPackoutDetailDialog() {
        window.setSize(350, 300);
        window.setPlain(true);
        window.setHeading("Shipping Wave Selection");
        window.setLayout(new FitLayout());
        final Button btnSelect = new Button(SELECT);
        final Button btnCancel = new Button(CANCEL);

        // get shipping wave data
        PackoutDetailInfoGrid srcShipWaveGrid = packoutDetailInfo.getPackoutDetailInfoShippingWavesGrid();
        ListStore<DisplayShipWaveDTO> store = new ListStore<DisplayShipWaveDTO>();
        if (srcShipWaveGrid != null) store = srcShipWaveGrid.getStore();

        FormPanel panel = new FormPanel();
        panel.setHeaderVisible(false);

        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        
        ColumnConfig column = new ColumnConfig();

        column.setId(DisplayShipWaveDTO.SHIP_WAVE_NUM);
        column.setHeader("Ship Wave");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(75);
        column.setSortable(false);
        column.setMenuDisabled(true);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayShipWaveDTO.MUST_ARRIVE_DATE);
        column.setHeader("Must Arrive By Date");
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(125);
        column.setMenuDisabled(true);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayShipWaveDTO.QUANTITY);
        column.setHeader("Quantity");
        column.setWidth(75);
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setSortable(false);
        column.setMenuDisabled(true);

        configs.add(column);

        ColumnModel cm = new ColumnModel(configs);

        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setHeaderVisible(false);
        cp.setButtonAlign(Style.HorizontalAlignment.RIGHT);
        cp.setLayout(new FitLayout());
        cp.setSize(303, 245);

        shipWaveGrid = new Grid<DisplayShipWaveDTO>(store, cm);
        shipWaveGrid.setColumnResize(true);
        shipWaveGrid.setStyleAttribute("borderTop", "none");
        shipWaveGrid.setStripeRows(true);
        shipWaveGrid.setBorders(true);
        shipWaveGrid.addListener(Events.RowDoubleClick, new Listener<GridEvent<ModelData>>() {
          	public void handleEvent(GridEvent<ModelData> be) {
          		DisplayShipWaveDTO sw = (DisplayShipWaveDTO)shipWaveGrid.getSelectionModel().getSelectedItem();
          		if (sw != null) {
          			shipWaveGrid.disable();
          			String dcCode = "";
              		ModelData item = tree.getSelectionModel().getSelectedItem();
              		if (item instanceof Folder)
              			dcCode = ((Folder)item).getDcCode();
              		else if (item instanceof DisplayDcAndPackoutDTO)
              			dcCode = ((DisplayDcAndPackoutDTO)item).getDcCode();
          			// add a new packout detail to tree grid
              		if (dcCode != null && dcCode.length() > 0) {
              			insertPackoutDetail(dcCode, sw);   
              		}
              		else { 
                        MessageBox.alert("Add Packout Detail", "Please enter a DC before adding Packout Detail...", null);                  			
              		}
              		
          			shipWaveGrid.enable();
          		}
                window.hide();         	
          	}
        });
        cp.add(shipWaveGrid);

        SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
            public void componentSelected(ButtonEvent ce) {
            	try{
                Button button = (Button) ce.getComponent();
                //Log.info("PackoutDetailsTreeGrid Listener..1.. " );
                if (button.getText().equals(SELECT)) {
                    if (shipWaveGrid.getSelectionModel().getSelectedItem() != null) {
                    	//Log.info("PackoutDetailsTreeGrid Listener..2.. " );
                  		DisplayShipWaveDTO sw = (DisplayShipWaveDTO)shipWaveGrid.getSelectionModel().getSelectedItem();
                  		//Log.info("PackoutDetailsTreeGrid Listener..3.. " );
                  		if (sw != null) {
                            button.disable();
                  			shipWaveGrid.disable();
                  			//Log.info("PackoutDetailsTreeGrid Listener..4.. " );
                  			String dcCode = "";
                      		ModelData item = tree.getSelectionModel().getSelectedItem();
                      		//Log.info("PackoutDetailsTreeGrid Listener..5.. " );
                      		if (item instanceof Folder){
                      			dcCode = ((Folder)item).getDcCode();
                      			//Log.info("PackoutDetailsTreeGrid Listener..6.. " );
                      		}else if (item instanceof DisplayDcAndPackoutDTO){
                      			dcCode = ((DisplayDcAndPackoutDTO)item).getDcCode();
                      			//Log.info("PackoutDetailsTreeGrid Listener..7.. " );
                      		}
                  			// add a new packout detail to tree grid
                      		//Log.info("PackoutDetailsTreeGrid Listener..8.. " );
                      		if (dcCode != null && dcCode.length() > 0) {
                      			//Log.info("PackoutDetailsTreeGrid Listener..9.. " );
                      			insertPackoutDetail(dcCode, sw);   
                      		}
                      		else { 
                      			//Log.info("PackoutDetailsTreeGrid Listener..10.. " );
                                MessageBox.alert("Add Packout Detail", "Please enter a DC before adding Packout Detail...", null);
                      		}
                      		//Log.info("PackoutDetailsTreeGrid Listener..11.. " );
                  			window.hide();
                  			//Log.info("PackoutDetailsTreeGrid Listener..12.. " );
                  			shipWaveGrid.enable();
                  			//Log.info("PackoutDetailsTreeGrid Listener..13.. " );
                            button.enable();
                            //Log.info("PackoutDetailsTreeGrid Listener..14.. " );
                  		}
                    } else {
                        MessageBox.alert("Shipping Wave", "Please click on a Shipping Wave record to select...", null);                  			
                    }
                } else if (button.getText().equals(CANCEL)) {
                    window.hide();
                } 
            	}catch(Exception ex){
            		System.out.println("PackoutDetailsTreeGrid Listener " + ex.toString());
                    //MessageBox.alert("PackoutDetailsTreeGrid Listener Error", "Error occured..."+ ex.toString(),null);                  			

            	}
            }
        };

        btnSelect.addSelectionListener(listener);
        btnCancel.addSelectionListener(listener);
        
        cp.addButton(btnSelect);
        cp.addButton(btnCancel);
        
        panel.add(cp);
        window.add(panel, new FitData(4));
	}
	
	private void addNewDC() {
		DisplayDcAndPackoutDTO dapDTO = new DisplayDcAndPackoutDTO();
		dapDTO.setDcCode("");
		dapDTO.setType("DC");
		dapDTO.setDirty(true);
		/*
		 * dapDTO.setDisplayId(displayDTO.getDisplayId());
		 * This code is changed because displayId is empty when dirty flag dirty flag raised during tab
		 *  navigation from display general info to fulfillment
		 * 03/24/2010
		 */
		dapDTO.setDisplayId(getDisplayId());
		dapDTO.setRowId(String.valueOf((new Date()).getTime()));
		dapDTO.setDtCreated(new Date());
		dapDTO.setUserCreated(App.getUser().getUsername());
		displayDcAndPackoutList.add(dapDTO);
		tree.getTreeStore().add(new Folder(dapDTO.getDcCode(), dapDTO.getDcQuantity(), dapDTO.getRowId(), dapDTO.getLocationId()), true);
		tree.getTreeStore().commitChanges();
        tree.getTreeView().refresh(true);
	}
	
	private Long getDisplayId() {
        Long id = packoutDetailInfo.getDisplayTabs().getDisplayModel().getDisplayId();
        return id;
    }
	
	private void insertPackoutDetail(String dcCode, DisplayShipWaveDTO sw) {
    	ModelData parent;
    	try{
    	ModelData item = tree.getSelectionModel().getSelectedItem();
    	if (item instanceof DisplayDcAndPackoutDTO) {
    		parent = tree.getTreeStore().getParent(item);
    	}
    	else parent = item;
    	DisplayDcAndPackoutDTO dapDTO = new DisplayDcAndPackoutDTO();
		dapDTO.setDisplayId(displayDTO.getDisplayId());
		dapDTO.setLocationId(((Folder)parent).getLocationId());
		dapDTO.setDcCode(((Folder)parent).getDcCode());
		Long locationId = getLocationId(((Folder)parent).getDcCode());
		dapDTO.setShipFromDcCode(((Folder)parent).getDcCode());
		dapDTO.setShipFromLocationId(locationId);
		dapDTO.setDcQuantity(((Folder)parent).getPoQuantity());
		dapDTO.setShipWaveNum(sw.getShipWaveNum().longValue());
		dapDTO.setMustArriveDate(sw.getMustArriveDate());
		dapDTO.setDisplayShipWaveId(sw.getDisplayShipWaveId());
		dapDTO.setPoDtCreated(new Date());
		dapDTO.setPoDtLastChanged(new Date());
		dapDTO.setPoUserCreated(App.getUser().getUsername());
		dapDTO.setPoUserLastChanged("");
		dapDTO.setDeleteFlg("N");
		dapDTO.setDirty(true);
		dapDTO.setType("Packout");
		dapDTO.setRowId(String.valueOf((new Date()).getTime()));
		displayDcAndPackoutList.add(dapDTO);
		tree.getTreeStore().add(parent, dapDTO, true);
		tree.getTreeStore().commitChanges();
        tree.getTreeView().refresh(true);
        if (parent != null) tree.setExpanded(parent, true);
    	}catch(Exception ex){
    		System.out.println("PackoutDetailsTreeGrid insertPackoutDetail..Error.. " + ex.toString() );
    		//Mari: to resolve non-working shipwave window select event Dt: 09-03-2010
    	}
	}

	private void getTreeData() {
        int idx = 0;
        DisplayDcAndPackoutDTO dapDTO;        
        
        tree.getTreeStore().commitChanges();
		List<ModelData> l = tree.getTreeStore().getModels();
        Iterator<ModelData> iter = l.iterator();
        while (iter.hasNext()) {
        	ModelData item = (ModelData)iter.next();
            
      		if (item instanceof Folder) {
            	idx = getDisplayDcAndPackoutDTO(((Folder) item).getRowId());
            	if (idx > -1) {
            		dapDTO = displayDcAndPackoutList.get(idx);
            		dapDTO.updateDC(
        				((Folder) item).getDcCode(), 
        				((Folder) item).getPoQuantity(), 
        				((Folder) item).getLocationId()
            		);
            	}
      		}
      		else if (item instanceof DisplayDcAndPackoutDTO) {
            	idx = getDisplayDcAndPackoutDTO(((DisplayDcAndPackoutDTO) item).getRowId());
            	if (idx > -1) {
            		dapDTO = displayDcAndPackoutList.get(idx);
            		if(dapDTO.getShipFromDcCode()!=null && dapDTO.getShipFromDcCode().toString().trim().length()>0){
            			dapDTO.setShipFromLocationId(getLocationId(dapDTO.getShipFromDcCode()));
            		}
            		dapDTO.updatePackout(
            			((DisplayDcAndPackoutDTO) item).getShipWaveNum(),
            			((DisplayDcAndPackoutDTO) item).getShipLocProductDueDate(),
    					((DisplayDcAndPackoutDTO) item).getProductDueDate(),
    					((DisplayDcAndPackoutDTO) item).getShipFromDate(),
    					((DisplayDcAndPackoutDTO) item).getMustArriveDate(),
    					((DisplayDcAndPackoutDTO) item).getDisplayShipWaveId(),
    					((DisplayDcAndPackoutDTO) item).getPoQuantity()
    				);
            	}
      		}        
        }		
	}

	private boolean shipWaveExistsInDC(String dc, Long swNum, String sFDc) {	
        tree.getTreeStore().commitChanges();
		List<ModelData> l = tree.getTreeStore().getModels();
        Iterator<ModelData> iter = l.iterator();
        String  shipFromDcCode=null;
        Long shipWaveNum=null;
        int recOccurance=0;
        while (iter.hasNext()) {
        	ModelData item = (ModelData)iter.next();
      		if (item instanceof DisplayDcAndPackoutDTO) {
      			String dcCode = ((DisplayDcAndPackoutDTO) item).getDcCode();
      			String deleted = ((DisplayDcAndPackoutDTO) item).getDeleteFlg();
      			if (dc.equals(dcCode) && (deleted == null || deleted.equals("N"))) {
	      			Long shipWave = new Long(0);
	      			shipWaveNum = ((DisplayDcAndPackoutDTO) item).getShipWaveNum();
	      			shipFromDcCode = ((DisplayDcAndPackoutDTO) item).getShipFromDcCode();
	      			if (shipWaveNum != null) shipWave = new Long(shipWaveNum.intValue());
	      			if (swNum.equals(shipWave) && sFDc.equals(shipFromDcCode)) {
	      				recOccurance++;
	      				if(recOccurance>1){
	      					return true;
	      				}
	      			}
      			}
      		}        
        }		
		return false;
	}
	
	public void updateTreeData() {
		getQuantities();
		
		// update packout detail
        tree.getTreeStore().commitChanges();
		List<ModelData> l = tree.getTreeStore().getModels();
        Iterator<ModelData> iter = l.iterator();
        while (iter.hasNext()) {
        	ModelData item = (ModelData)iter.next();
      		if (item instanceof Folder) {
      			Long poQty = sumPackoutByDc.get(((Folder) item).getDcCode());
      			if (poQty != null)
					((Folder) item).setPoBalanceQty(poQty);
      		}        
        }		
        tree.getTreeStore().commitChanges();
        tree.getTreeView().refresh(true);
        
        // update shipping wave grid
        EditorGrid<DisplayShipWaveDTO> grid = packoutDetailInfo.getPackoutDetailInfoShippingWavesGrid().getEditorGrid();
   		int size = grid.getStore().getModels().size();
		for (int i = 0; i < size; i++) {
			Integer shipWaveNum = ((DisplayShipWaveDTO)grid.getStore().getModels().get(i)).getShipWaveNum();
			Long poQty = sumPackoutByShipWave.get(shipWaveNum);
			if (poQty != null)
				((DisplayShipWaveDTO)grid.getStore().getModels().get(i)).setPoQty(poQty);
		}
		grid.getStore().commitChanges();
		grid.getView().refresh(true);
		
        String heading = "Packout Details - Total Qty (" + getSumOfAllDCNodes() + ") ";
        System.out.print(heading);
		packoutDetailInfo.getPackoutDetailsFieldSet().setHeading(heading);

	}

	private int getDisplayDcAndPackoutDTO(String rowId) {
		int row = 0;
		Iterator<DisplayDcAndPackoutDTO>iter = displayDcAndPackoutList.iterator();
		while (iter.hasNext()) {
			DisplayDcAndPackoutDTO dapDTO = (DisplayDcAndPackoutDTO)iter.next();
			if (dapDTO.getRowId().equals(rowId)) return row;
			row++;
		}
		return -1;
	}
	
	private Long getLocationId(String locationCode) {
		List<LocationCountryDTO> locationList = locationsCountry.getModels();
		for (int i = 0; i < locationList.size(); i++) {
			LocationCountryDTO dto = (LocationCountryDTO)locationList.get(i);
			if (locationCode.equals(dto.getLocationCode())) return dto.getLocationId();
		}
		return -1L;
	}

	public Long getSumOfAllDCNodes() {
		return sumDCNodes;
	}

	public HashMap<String, Long> getDcQty() {
		return dcQty;
	}

	public HashMap<String, Long> getSumPackoutByDc() {
		return sumPackoutByDc;
	}

	public HashMap<Integer, Long> getSumPackoutByShipWave() {
		return sumPackoutByShipWave;
	}

		public void getQuantities() {
		sumDCNodes = 0L;
		dcQty.clear();
		sumPackoutByDc.clear();
		sumPackoutByShipWave.clear();
		
		tree.getTreeStore().commitChanges();
		List<ModelData> l = tree.getTreeStore().getModels();
        Iterator<ModelData> iter = l.iterator();
        while (iter.hasNext()) {
        	ModelData item = (ModelData)iter.next();
      		if (item instanceof Folder) {
      			String dc = ((Folder) item).getDcCode();
      			String rowId = ((Folder)item).getRowId();
      			DisplayDcAndPackoutDTO dap = (DisplayDcAndPackoutDTO)displayDcAndPackoutList.get(getDisplayDcAndPackoutDTO(rowId));
      			if (dap.getDeleteFlg() == null || dap.getDeleteFlg().equals("N")) {
	      			Long currQty = 0L;
	      			Long qty = ((Folder) item).getPoQuantity();
	      			if (qty != null) {
		      			if (dc != null && dc.length() > 0) {
		      				currQty = dcQty.get(dc); 
		      				if (currQty == null) currQty = 0L;
		      				dcQty.put(dc, currQty + qty); 
		      			}
	      				sumDCNodes += qty;
		      			System.out.println("PakoutDetailTreeGrid.getQuantities(): sumDcNodes(" + dc + ")=" + sumDCNodes);
	      			}
      			}
      		}
      		else if (item instanceof DisplayDcAndPackoutDTO) {
      			String dc = ((DisplayDcAndPackoutDTO) item).getDcCode();
      			String rowId = ((DisplayDcAndPackoutDTO) item).getRowId();
      			Integer shipWave = null;
      			Long shipWaveNum = ((DisplayDcAndPackoutDTO) item).getShipWaveNum();
      			if (shipWaveNum != null) shipWave = new Integer(shipWaveNum.intValue());
      			Long qty = ((DisplayDcAndPackoutDTO) item).getPoQuantity();
      			DisplayDcAndPackoutDTO dap = (DisplayDcAndPackoutDTO)displayDcAndPackoutList.get(getDisplayDcAndPackoutDTO(rowId));
      			if (dap.getDeleteFlg() == null || dap.getDeleteFlg().equals("N")) {
	      			Long currQty = 0L;
	      			if (qty != null) {
		      			if (dc != null && dc.length() > 0) {
		      				currQty = sumPackoutByDc.get(dc); 
		      				if (currQty == null) currQty = 0L;
		      				sumPackoutByDc.put(dc, currQty + qty); 
		      			}
		      			if (shipWave != null) {
		      				currQty = sumPackoutByShipWave.get(shipWave);
		      				if (currQty == null) currQty = 0L;
		      				sumPackoutByShipWave.put(shipWave, currQty + qty);
		      			}
	      			}	
      			}
      		}
        }
	}
}