package com.averydennison.dmm.app.client;

import java.util.Date;

import com.averydennison.dmm.app.client.models.CustomerMarketingDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.util.KeyUtil;
import com.averydennison.dmm.app.client.util.StringUtil;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.CheckBoxGroup;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.DateTimePropertyEditor;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Oct 23, 2009
 * Time: 11:57:21 AM
 */
public class CustomerMarketingInfo extends LayoutContainer implements DirtyInterface{
    private FormData formData;
    private VerticalPanel vp;
    public static final String SAVE = "Save";
    public static final String RETURN_TO_SELECTION = "Return to Selection";
    public static final String REGEX_DATE_PATTERN = "\\d{1,2}/\\d{1,2}/\\d{4}";
    public static final String datePattern = "MM/dd/yyyy";
    private DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();

    private final DateField priceAvailabilityCompletedOn = new DateField();
    private final DateField sampleToCustomerBy = new DateField();
    private final DateField finalArtworkAndPOToVendor = new DateField();
    private final DateField pIMCompletedON = new DateField();
    private final DateField pricingAdminValidation = new DateField();
    private final TextField<String> qSIDocNumber = new TextField<String>();
    private final NumberField projectLevel = new NumberField();
    private final TextArea comments = new TextArea();
    private final CheckBox initialRenderingAndEstQuote = new CheckBox();
    private final CheckBox structureApproved = new CheckBox();
    private final CheckBox orderIn = new CheckBox();

    private DisplayTabs displayTabs;
    private DisplayDTO displayModel;
    private CustomerMarketingDTO customerMarketingModel;
    private ChangeListener customerMarketingModelListener;
    private ChangeListener displayModelListener;
    
    
    private void forwardToDisplayProjectSelection(){
        clearModel();
        App.getVp().clear();
        App.getVp().add(new DisplayProjectSelection());
    }
    
    private boolean overrideDirty = false;
    public void overrideDirty(){
        overrideDirty = true;
    }
    public boolean isDirty(){
        if(overrideDirty==true) return false;
    	return !different();
    }
    
    public boolean different(){
        if(displayModel.getQsiDocNumber() == null){
            if(getControlDisplayModel().getQsiDocNumber()!=null)
                return false;
        } else if(!displayModel.getQsiDocNumber().equals(getControlDisplayModel().getQsiDocNumber()))
            return false;
        return customerMarketingModel.equalsCustomerMarketingInfo(getControlCustomerModel());
    }
    
    private DisplayDTO getControlDisplayModel(){
        DisplayDTO controlModel = new DisplayDTO();
        if(qSIDocNumber.getValue()!=null) controlModel.setQsiDocNumber(qSIDocNumber.getValue());
        return controlModel;
    }

    CustomerMarketingDTO getControlCustomerModel(){
        CustomerMarketingDTO controlModel = new CustomerMarketingDTO();
        if(comments.getValue()!=null) 
        		controlModel.setComments(comments.getValue());
        if(pIMCompletedON.getValue()!=null) 
        		controlModel.setPimCompletedDate(pIMCompletedON.getValue());
        if(priceAvailabilityCompletedOn.getValue()!=null) 
        		controlModel.setPriceAvailabilityDate(priceAvailabilityCompletedOn.getValue());
        if(sampleToCustomerBy.getValue()!=null) 
        	controlModel.setSentSampleDate(sampleToCustomerBy.getValue());
        if(pricingAdminValidation.getValue()!=null) 
        	controlModel.setPricingAdminDate(pricingAdminValidation.getValue());
        if(finalArtworkAndPOToVendor.getValue()!=null) 
        	controlModel.setFinalArtworkDate(finalArtworkAndPOToVendor.getValue());
        if(initialRenderingAndEstQuote.isDirty() || customerMarketingModel.getInitialRendering()!=null) 
        	controlModel.setIntialRendering(initialRenderingAndEstQuote.getValue());
        if(orderIn.isDirty() || customerMarketingModel.getOrderIn()!=null) 
        		controlModel.setOrderIn(orderIn.getValue());
        if(projectLevel.getValue()!=null) 
        		controlModel.setProjectLevelPct(Short.valueOf(projectLevel.getValue().toString()));    
        if(structureApproved.isDirty() || customerMarketingModel.getStructureApproved()!=null) 
        	controlModel.setStructureApproved(structureApproved.getValue());
        return controlModel;
    }
    
    private final Listener<MessageBoxEvent> l = new Listener<MessageBoxEvent>() {
        public void handleEvent(MessageBoxEvent mbe) {
            Button btn = mbe.getButtonClicked();
            if (btn.getText().equals("Yes")) {
                save(btn, null, null);
                forwardToDisplayProjectSelection();
            }
            if (btn.getText().equals("No")) {
                forwardToDisplayProjectSelection();
            }
            if (btn.getText().equals("Cancel")) {
            }
        }
    };
    public CustomerMarketingInfo() {
        formData = new FormData("95%");
        vp = new VerticalPanel();
        vp.setSpacing(10);
    }

    private SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
        public void componentSelected(ButtonEvent ce) {
            Button btn = (Button) ce.getComponent();
            if (btn.getText().equals(SAVE)) {
                save(btn, null, null);
            } else if (btn.getText().equals(RETURN_TO_SELECTION)) {
                if(isDirty()){
                    String title = "You have unsaved information!";
                    String msg = "Would you like to save?";                             
                    MessageBox box = new MessageBox();
                    box.setButtons(MessageBox.YESNOCANCEL);
                    box.setIcon(MessageBox.QUESTION);
                    box.setTitle("Save Changes?");
                    box.addCallback(l);
                    box.setTitle(title);
                    box.setMessage(msg);
                    box.show();
                }else{
                    forwardToDisplayProjectSelection();
                }
            }
        }
    };
    public void save(Button btn, TabPanel panel, TabItem tabItem){
    	if (displayModel.getDisplayId() == null) {
   		 	MessageBox.alert("Validation Error", "There is no display defined " , null);
   		 	return;
    	}
        if (!priceAvailabilityCompletedOn.validate()) return;
        if (!sampleToCustomerBy.validate()) return;
        if (!finalArtworkAndPOToVendor.validate()) return;
        if (!pIMCompletedON.validate()) return;
        if (!pricingAdminValidation.validate()) return;
        if (!qSIDocNumber.validate()) return;
        if (!projectLevel.validate()) return;
        if (!comments.validate()) return;
        if (!initialRenderingAndEstQuote.validate()) return;
        if (!structureApproved.validate()) return;

        if (!orderIn.validate()) return;

        else if (customerMarketingModel != null && isDirty()) {

            displayModel.setQsiDocNumber(qSIDocNumber.getValue());
            customerMarketingModel.setPriceAvailabilityDate(priceAvailabilityCompletedOn.getValue());
            customerMarketingModel.setSentSampleDate(sampleToCustomerBy.getValue());
            customerMarketingModel.setFinalArtworkDate(finalArtworkAndPOToVendor.getValue());

            customerMarketingModel.setPimCompletedDate(pIMCompletedON.getValue());
            customerMarketingModel.setPricingAdminDate(pricingAdminValidation.getValue());
            if (projectLevel.getValue() != null) {
                customerMarketingModel.setProjectLevelPct((Short) projectLevel.getValue());
            } else customerMarketingModel.setProjectLevelPct(null);
            if(comments.getValue()!=null){
                customerMarketingModel.setComments(StringUtil.format(comments.getValue()));
                comments.setValue(StringUtil.format(comments.getValue()));
            }else{
            	customerMarketingModel.setComments(comments.getValue());
            }
            customerMarketingModel.setIntialRendering(initialRenderingAndEstQuote.getValue());
            customerMarketingModel.setStructureApproved(structureApproved.getValue());
            customerMarketingModel.setOrderIn(orderIn.getValue());
            customerMarketingModel.setDisplayId(displayModel.getDisplayId());

            customerMarketingModel.setUserLastChanged(App.getUser().getUsername());
            customerMarketingModel.setDtLastChanged(new Date());
            if (customerMarketingModel.getCustomerMarketingId() == null) {
                customerMarketingModel.setDtCreated(new Date());
                customerMarketingModel.setUserCreated(App.getUser().getUsername());
            }
            final MessageBox box = MessageBox.wait("Progress",
                    "Saving customer marketing info, please wait...", "Saving...");
            AppService.App.getInstance().saveCustomerMarketingDTO(customerMarketingModel, displayModel,
                    new Service.SaveMarketingInfoDTO(getCustomerMarketingInfo(), customerMarketingModel,
                            btnSave, box));
        }
    }
    
    private CustomerMarketingInfo getCustomerMarketingInfo() {
        return this;
    }

    private void clearModel() {
        if (displayModel != null) displayModel.removeChangeListener(displayModelListener);
        if (customerMarketingModel != null) customerMarketingModel.removeChangeListener(customerMarketingModelListener);
        displayModel = null;
        customerMarketingModel = null;
        overrideDirty = false;
        displayTabs.resetDisplayModel();
        setData("display", null);
    }

    private final Button btnSave = new Button(SAVE, listener);

    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);

        setModelsAndView();
        createHeader();
        createForm();
        add(vp);
    }

    private void setModelsAndView() {
        displayModel = displayTabs.getDisplayModel();
        if (displayModel == null) {
            displayModel = new DisplayDTO();
        }
        if (displayModel.getDisplayId() != null) {
            displayTabHeader.setDisplay(displayModel);
        }

        customerMarketingModel = this.getData("customerMarketing");
        if (customerMarketingModel == null) {
            customerMarketingModel = new CustomerMarketingDTO();
        }
        updateView();
        setPermissions();
        setReadOnly();
        displayModelListener = new ChangeListener() {
			public void modelChanged(ChangeEvent event) {
				displayModel = (DisplayDTO) event.getSource();
				if (displayModel != null && displayModel.getDisplayId() != null) {
					displayTabs.setDisplayModel(displayModel);
					displayTabs.setData("display", displayModel);
			        if (displayModel != null && displayModel.getDisplayId() != null) {
			            displayTabHeader.setDisplay(displayModel);
			        }
				}
			}
		};
		displayModel.addChangeListener(displayModelListener);
		customerMarketingModelListener = new ChangeListener() {
            public void modelChanged(com.extjs.gxt.ui.client.data.ChangeEvent event) {
                customerMarketingModel = (CustomerMarketingDTO) event.getSource();
                if (customerMarketingModel != null && customerMarketingModel.getCustomerMarketingId() != null)
                    updateView();
            }
        };
        customerMarketingModel.addChangeListener(customerMarketingModelListener);
    }
    private void setPermissions(){
    	if(!App.isAuthorized("CustomerMarketingInfo.SAVE")) btnSave.disable();
    }
    
    private void setReadOnly() {
        Boolean b = this.getData("readOnly");
        if (b == null) return;
        priceAvailabilityCompletedOn.setReadOnly(b);
        sampleToCustomerBy.setReadOnly(b);
        finalArtworkAndPOToVendor.setReadOnly(b);
        pIMCompletedON.setReadOnly(b);
        pricingAdminValidation.setReadOnly(b);
        qSIDocNumber.setReadOnly(b);
        projectLevel.setReadOnly(b);
        comments.setReadOnly(b);
        initialRenderingAndEstQuote.setReadOnly(b);
        structureApproved.setReadOnly(b);
        orderIn.setReadOnly(b);
        if (b) btnSave.disable();
    }

    private void updateView() {
    	this.displayModel=displayTabs.getDisplayModel();
        if (displayModel != null && this.displayModel.getDisplayId() != null) {
            this.displayTabHeader.setDisplay(this.displayModel);
        }
        if (customerMarketingModel != null && customerMarketingModel.getCustomerMarketingId() != null) {
            priceAvailabilityCompletedOn.setValue(customerMarketingModel.getPriceAvailabilityDate());
            sampleToCustomerBy.setValue(customerMarketingModel.getSentSampleDate());
            finalArtworkAndPOToVendor.setValue(customerMarketingModel.getFinalArtworkDate());
            pIMCompletedON.setValue(customerMarketingModel.getPimCompletedDate());
            pricingAdminValidation.setValue(customerMarketingModel.getPricingAdminDate());
            qSIDocNumber.setValue(this.displayModel.getQsiDocNumber());
            projectLevel.setValue(customerMarketingModel.getProjectLevelPct());
            comments.setValue(customerMarketingModel.getComments());
            if(customerMarketingModel.getInitialRendering()!=null){
                initialRenderingAndEstQuote.setValue(customerMarketingModel.getInitialRendering());
                initialRenderingAndEstQuote.setOriginalValue(customerMarketingModel.getInitialRendering());
            }
            if(customerMarketingModel.getStructureApproved()!=null){
                structureApproved.setValue(customerMarketingModel.getStructureApproved());
                structureApproved.setOriginalValue(customerMarketingModel.getStructureApproved());
            }
            if(customerMarketingModel.getOrderIn()!=null) {
                orderIn.setValue(customerMarketingModel.getOrderIn());
                orderIn.setOriginalValue(customerMarketingModel.getOrderIn());
            }            
        }
    }

    private void createHeader() {
        add(displayTabHeader);
    }

    private void createForm() {

        FormPanel formPanel = new FormPanel();
        formPanel.setHeaderVisible(false);
        formPanel.setFrame(true);
        formPanel.setSize(1190, -1);
        formPanel.setLabelAlign(FormPanel.LabelAlign.TOP);
        formPanel.setButtonAlign(Style.HorizontalAlignment.CENTER);

        LayoutContainer main = new LayoutContainer();
        main.setLayout(new ColumnLayout());

        LayoutContainer left = new LayoutContainer();
        LayoutContainer center = new LayoutContainer();
        LayoutContainer right = new LayoutContainer();

        FormLayout leftLayout = new FormLayout();
        left.setStyleAttribute("paddingRight", "10px");
        leftLayout.setLabelAlign(FormPanel.LabelAlign.TOP);

        FormLayout centerLayout = new FormLayout();
        left.setStyleAttribute("paddingRight", "10px");
        centerLayout.setLabelAlign(FormPanel.LabelAlign.TOP);

        FormLayout rightLayout = new FormLayout();
        rightLayout.setLabelAlign(FormPanel.LabelAlign.TOP);

        left.setStyleAttribute("paddingRight", "10px");
        left.setLayout(leftLayout);

        center.setStyleAttribute("paddingRight", "10px");
        center.setLayout(centerLayout);

        right.setStyleAttribute("paddingLeft", "10px");
        right.setLayout(rightLayout);

        priceAvailabilityCompletedOn.setFieldLabel("Price Availability Completed On");
        
        final DateTimePropertyEditor dateTimePropertyEditor = new DateTimePropertyEditor(datePattern);
        priceAvailabilityCompletedOn.setPropertyEditor(dateTimePropertyEditor);
        priceAvailabilityCompletedOn.setAutoValidate(true);
        priceAvailabilityCompletedOn.setValidator(dateValidator);
        priceAvailabilityCompletedOn.setAllowBlank(true);
        priceAvailabilityCompletedOn.addListener(Events.KeyPress, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
                int keyCode = ke.getKeyCode();
                if (!KeyUtil.isValidDateChar(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress()) {
                    ke.stopEvent();
                }
            }
        });
        left.add(priceAvailabilityCompletedOn, formData);

        sampleToCustomerBy.setFieldLabel("Sample to Customer By");
        sampleToCustomerBy.setPropertyEditor(new DateTimePropertyEditor("MM/dd/yyyy"));
        sampleToCustomerBy.setAutoValidate(true);
        sampleToCustomerBy.setAllowBlank(true);
        sampleToCustomerBy.setValidator(dateValidator);
        sampleToCustomerBy.addListener(Events.KeyPress, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
                int keyCode = ke.getKeyCode();
                if (!KeyUtil.isValidDateChar(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress()) {
                    ke.stopEvent();
                }
            }
        });
        left.add(sampleToCustomerBy, formData);

        finalArtworkAndPOToVendor.setFieldLabel("Final Artwork & PO Due to Vendor");
        finalArtworkAndPOToVendor.setPropertyEditor(new DateTimePropertyEditor("MM/dd/yyyy"));
        finalArtworkAndPOToVendor.setAutoValidate(true);
        finalArtworkAndPOToVendor.setAllowBlank(true);
        finalArtworkAndPOToVendor.setValidator(dateValidator);
        finalArtworkAndPOToVendor.addListener(Events.KeyPress, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
                int keyCode = ke.getKeyCode();
                if (!KeyUtil.isValidDateChar(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress()) {
                    ke.stopEvent();
                }
            }
        });
        left.add(finalArtworkAndPOToVendor, formData);

        pIMCompletedON.setFieldLabel("PIM Completed On");
        pIMCompletedON.setPropertyEditor(new DateTimePropertyEditor("MM/dd/yyyy"));
        pIMCompletedON.setAutoValidate(true);
        pIMCompletedON.setAllowBlank(true);
        pIMCompletedON.setValidator(dateValidator);
        pIMCompletedON.addListener(Events.KeyPress, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
                int keyCode = ke.getKeyCode();
                if (!KeyUtil.isValidDateChar(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress()) {
                    ke.stopEvent();
                }
            }
        });
        left.add(pIMCompletedON, formData);

        pricingAdminValidation.setFieldLabel("Pricing Admin Validation");
        pricingAdminValidation.setPropertyEditor(new DateTimePropertyEditor("MM/dd/yyyy"));
        pricingAdminValidation.setAutoValidate(true);
        pricingAdminValidation.setAllowBlank(true);
        pricingAdminValidation.setValidator(dateValidator);
        pricingAdminValidation.addListener(Events.KeyPress, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
                int keyCode = ke.getKeyCode();
                if (!KeyUtil.isValidDateChar(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress()) {
                    ke.stopEvent();
                }
            }
        });
        left.add(pricingAdminValidation, formData);

        qSIDocNumber.setFieldLabel("QSI Doc Number");
        qSIDocNumber.setAllowBlank(true);
        qSIDocNumber.setReadOnly(false);
        qSIDocNumber.setMinLength(2);
        qSIDocNumber.setMaxLength(15);
        right.add(qSIDocNumber, formData);

        projectLevel.setFieldLabel("Project Confidence Level");
        projectLevel.setPropertyEditorType(Short.class);
        projectLevel.setAllowBlank(true);
        projectLevel.setReadOnly(false);
        projectLevel.setMinValue(0);
        projectLevel.setMaxValue(100);
        right.add(projectLevel, formData);

        comments.setFieldLabel("Comments");
        comments.setMaxLength(200);
        right.add(comments, formData);

        initialRenderingAndEstQuote.setBoxLabel("Initial Rendering and Est. Quote");

        structureApproved.setBoxLabel("Structure Approved");

        orderIn.setBoxLabel("Orders Received");

        CheckBoxGroup checkGroup = new CheckBoxGroup();
        checkGroup.setLabelSeparator("  ");
        checkGroup.add(initialRenderingAndEstQuote);
        checkGroup.add(structureApproved);
        checkGroup.add(orderIn);
        right.add(checkGroup, formData);
//        right.add(structureApproved, formData);
//        right.add(orderIn, formData);

        main.add(left, new ColumnData(.5));
        main.add(right, new ColumnData(.5));
        formPanel.add(main);

        formPanel.addButton(new Button(RETURN_TO_SELECTION, listener));
        formPanel.addButton(btnSave);

        vp.add(formPanel);
    }

    public void reload() {
    	this.displayModel = displayTabs.getDisplayModel();
        if (customerMarketingModel == null) {
            customerMarketingModel = new CustomerMarketingDTO();
        }
        if (this.displayModel != null && this.displayModel.getDisplayId() != null) {
        	//AppService.App.getInstance().getDisplayDTO(displayModel, new Service.ReloadDisplay(displayModel));
            AppService.App.getInstance().getCustomerMarketingDTO(this.displayModel.getDisplayId(),
                    new Service.ReloadCustomerMarketing(customerMarketingModel));
            overrideDirty = false;
            updateView();
        }
    }

    
    
    private void updateHeaderView() {
        displayTabHeader.setDisplay(displayModel);
    }

    public DisplayTabs getDisplayTabs() {
        return displayTabs;
    }

    public void setDisplayTabs(DisplayTabs displayTabs) {
        this.displayTabs = displayTabs;
    }
    
    public Validator dateValidator = new Validator(){
        public String validate(Field<?> field, String value) {
            if(!value.matches(REGEX_DATE_PATTERN )) return "invalid date format: use " + datePattern;
            return null;
        }
    };
}
