package com.averydennison.dmm.app.client;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.averydennison.dmm.app.client.models.BooleanWrapper;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayDcAndPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayDcDTO;
import com.averydennison.dmm.app.client.models.DisplayPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.client.models.Folder;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.Validator;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello Date: Nov 17, 2009 Time: 9:07:09 AM
 */
public class PackoutDetailInfo extends LayoutContainer implements
		DirtyInterface {
	private DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
	private FormData formData;
	private VerticalPanel vp;

	private DisplayDTO displayModel;
	private ChangeListener displayListener;

	private final NumberField actualQty = new NumberField();

	private DisplayTabs displayTabs;

	BooleanWrapper dcMapNotifier = new BooleanWrapper();
	BooleanWrapper dpMapNotifier = new BooleanWrapper();
	BooleanWrapper dswMapNotifier = new BooleanWrapper();

	private Map<Long, DisplayDcDTO> dcMap = new HashMap<Long, DisplayDcDTO>();
	private Map<Long, DisplayPackoutDTO> dpMap = new HashMap<Long, DisplayPackoutDTO>();
	private Map<Long, DisplayShipWaveDTO> dswMap = new HashMap<Long, DisplayShipWaveDTO>();

	private FieldSet packoutDetailsFieldSet = new FieldSet();
	private FieldSet shippingWavesFieldSet = new FieldSet();
	PackoutDetailTreeGrid packoutDetailsTree;
	PackoutDetailInfoGrid packoutDetailInfoShippingWavesGrid;

	public static final String MESSAGE = "Packout Details Total Qty and Shipping Waves Total Qty do not equal Forecast Quantity.  Would you like to override the current forecast quantity?";
	private static final String PACKOUT_DETAILS_HEADER = "Packout Details";
	private static final String SHIPPING_WAVES_HEADER = "Shipping Waves";
	public static final String RETURN_TO_SELECTION = "Return to Selection";
	private static final String SAVE = "Save";
	private Button btnSave;
	private boolean overrideDirty = false;

	public PackoutDetailInfo() {
		formData = new FormData("95%");
		vp = new VerticalPanel();
		vp.setSpacing(10);
	}

	private void setPermissions() {
		if (!App.isAuthorized("PackoutDetailInfo.SAVE"))
			btnSave.disable();
	}

	public void overrideDirty() {
		this.overrideDirty = true;
	}

	public boolean isDirty() {
		if (overrideDirty == true)
			return false;
		Long actQty = (Long) actualQty.getValue();
		Long dcQty = packoutDetailsTree.getSumOfAllDCNodes();
		Long shipWaveQty = packoutDetailInfoShippingWavesGrid
				.getDisplayShippingWaveSum();
		if(!actQty.equals(shipWaveQty) || !dcQty.equals(shipWaveQty)){
			MessageBox.alert("Validation Error",
					"DC or Shipwave total quantity does not match actual quantity.", null);
		}
		if (displayModel != null && displayModel.getActualQty() == null
				&& actQty != null)
			return true;
		else if (displayModel != null && displayModel.getActualQty() != null
				&& !displayModel.getActualQty().equals(actQty))
			return true;
		if (packoutDetailsTree.isDirty())
			return true;
		if (packoutDetailsTree.isDisplayPackoutModified())
			return true;
		if (packoutDetailsTree.getStore().getModifiedRecords().size() > 0)
			return true;
		if (packoutDetailInfoShippingWavesGrid.getStore().getModifiedRecords()
				.size() > 0)
			return true;
		if (packoutDetailInfoShippingWavesGrid.getStore().getCount() > 0
				&& packoutDetailInfoShippingWavesGrid.isDirty())
			return true;
		return false;
	}

	@Override
	protected void onRender(Element parent, int index) {
		super.onRender(parent, index);
		setModelsAndView();
		createHeader();
		createForm();
		setPermissions();
		setReadOnly();
		add(vp);
	}

	private void setReadOnly() {
		Boolean b = this.getData("readOnly");
		if (b == null)
			return;
		if (b) {
			btnSave.disable();
			actualQty.disable();
		}
	}

	private void setModelsAndView() {
		displayModel = displayTabs.getDisplayModel();
		if (displayModel == null) {
			displayModel = new DisplayDTO();
		}
		updateView();
		displayListener = new ChangeListener() {
			public void modelChanged(ChangeEvent event) {
				displayModel = (DisplayDTO) event.getSource();
				if (displayModel != null && displayModel.getDisplayId() != null) {
					displayTabs.setDisplayModel(displayModel);
					displayTabs.setData("display", displayModel);
					updateView();
				}
			}
		};
		displayModel.addChangeListener(displayListener);
	}

	private void createHeader() {
		add(displayTabHeader);
	}

	private void createForm() {
		FormPanel formPanel = new FormPanel();
		formPanel.setHeaderVisible(false);
		formPanel.setFrame(true);
		//formPanel.setSize(935, -1);
		formPanel.setSize(1190, -1);
		formPanel.setLabelAlign(FormPanel.LabelAlign.TOP);
		formPanel.setButtonAlign(Style.HorizontalAlignment.CENTER);

		LayoutContainer main = new LayoutContainer();
		main.setLayout(new ColumnLayout());

		ContentPanel left = new ContentPanel();
		ContentPanel right = new ContentPanel();

		left.setHeaderVisible(false);
		right.setHeaderVisible(false);

		FormLayout leftLayout = new FormLayout();
		left.setStyleAttribute("paddingRight", "10px");
		leftLayout.setLabelAlign(FormPanel.LabelAlign.TOP);

		FormLayout rightLayout = new FormLayout();
		left.setStyleAttribute("paddingRight", "10px");
		rightLayout.setLabelAlign(FormPanel.LabelAlign.TOP);

		left.setStyleAttribute("paddingRight", "10px");
		left.setLayout(leftLayout);

		right.setStyleAttribute("paddingRight", "10px");
		right.setLayout(rightLayout);

		FormLayout displaySpecificationsFormLayout = new FormLayout();
		displaySpecificationsFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
		packoutDetailsFieldSet.setLayout(displaySpecificationsFormLayout);
		packoutDetailsFieldSet.setHeading(PACKOUT_DETAILS_HEADER);
		packoutDetailsFieldSet.setCollapsible(false);
		left.add(packoutDetailsFieldSet);

		FormLayout displayLogisticssFormLayout = new FormLayout();
		displayLogisticssFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
		shippingWavesFieldSet.setLayout(displayLogisticssFormLayout);
		shippingWavesFieldSet.setHeading("Shipping Waves");
		shippingWavesFieldSet.setCollapsible(false);
		right.add(shippingWavesFieldSet);

		Boolean b = this.getData("readOnly");
		if (b != null && b) {
			packoutDetailsTree = new PackoutDetailTreeGrid(displayModel, this,
					true);
		} else {
			packoutDetailsTree = new PackoutDetailTreeGrid(displayModel, this,
					false);
		}
		packoutDetailInfoShippingWavesGrid = new PackoutDetailInfoGrid(this);
		packoutDetailsFieldSet.add(packoutDetailsTree, formData);
		shippingWavesFieldSet.add(packoutDetailInfoShippingWavesGrid, formData);
		actualQty.setFieldLabel("Display Actual Quantity");
		actualQty.setAllowBlank(false);
		actualQty.setVisible(false);
		actualQty.setPropertyEditorType(Long.class);
		actualQty.setAutoValidate(true);
		actualQty.setValidator(new Validator() {
			public String validate(Field<?> field, String value) {
				if (Long.valueOf(value.trim()).equals(0L))
					return "invalid qty";
				return null;
			}
		});
		right.add(actualQty);

		SelectionListener<ButtonEvent> selectionListener = new SelectionListener<ButtonEvent>() {
			public void componentSelected(ButtonEvent ce) {
				Button btn = (Button) ce.getComponent();
				if (btn.getText().equals(SAVE)) {
					save(btn, null, null);
				} else if (btn.getText().equals(RETURN_TO_SELECTION)) {
					if (isDirty()) {
						Listener<MessageBoxEvent> cb = new Listener<MessageBoxEvent>() {
							public void handleEvent(MessageBoxEvent mbe) {
								String id = mbe.getButtonClicked().getItemId();
								if (Dialog.YES == id) {
									save(null, null, null);
								} else if (Dialog.NO == id) {
									clearModel();
									App.getVp().clear();
									App.getVp().add(
											new DisplayProjectSelection());
								} else {

								}
							}
						};
						String title = "You have unsaved information!";
						String msg = "This record has been modified. Do you wish to save before exiting?";
						MessageBox box = new MessageBox();
						box.setButtons(MessageBox.YESNOCANCEL);
						box.setIcon(MessageBox.QUESTION);
						box.setTitle("Save?");
						box.addCallback(cb);
						box.setTitle(title);
						box.setMessage(msg);
						box.show();
					} else {
						clearModel();
						App.getVp().clear();
						App.getVp().add(new DisplayProjectSelection());
					}
				}
			}
		};
		main.add(left, new ColumnData(.62));
		main.add(right, new ColumnData(.38));
		formPanel.add(main);
		btnSave = new Button(SAVE, selectionListener);
		formPanel
				.addButton(new Button("Return to Selection", selectionListener));
		formPanel.addButton(btnSave);
		vp.add(formPanel);
	}

	public void save(Button btn, TabPanel panel, TabItem tabItem) {
		// validate controls
		if (displayModel.getDisplayId() == null) {
			MessageBox.alert("Validation Error",
					"There is no display defined ", null);
			return;
		}
		if (!packoutDetailsTree.validateControls())
			return;
		if (!packoutDetailInfoShippingWavesGrid.validateControls())
			return;

		// execute business rules/validations
		Long actQty = (Long) actualQty.getValue();
		if (actQty == null || actQty.equals(0L)) {
			MessageBox.alert("Validation Error",
					"Actual quantity is required.", null);
			return;
		}

		// check for duplicate DC
		if (checkForDuplicateDC()) {
			MessageBox.alert("Validation Error",
					"There are duplicate DCs defined.", null);
			return;
		}

		// refresh quantities for validation
		packoutDetailsTree.getQuantities();
		packoutDetailInfoShippingWavesGrid.getQuantities();

		// if dcQty = shipWaveQty then override fcstQty
		Long dcQty = packoutDetailsTree.getSumOfAllDCNodes();
		Long shipWaveQty = packoutDetailInfoShippingWavesGrid
				.getDisplayShippingWaveSum();
		
		// if DC Qty > 0 then compare to fcstQty
		if (dcQty > 0L && !dcQty.equals(actQty)) {
			MessageBox
					.alert(
							"Validation Error",
							"DC total quantity does not match actual quantity.",
							null);
			return;
		}
		// if Ship Wave Qty > 0 then compare to fcstQty
		if (shipWaveQty > 0L && !shipWaveQty.equals(actQty)) {
			MessageBox
					.alert(
							"Validation Error",
							"Shipping Wave total quantity does not match actual quantity.",
							null);
			return;
		}

		// if DC Qty > 0 and Ship Wave Qty > 0 then compare to each other
		if (dcQty > 0L && shipWaveQty > 0L && !dcQty.equals(shipWaveQty)) {
			MessageBox.alert("Validation Error",
					"DC quantities do not match Shipping Wave quantities.",
					null);
			return;
		}

		if (dcQty > 0L && shipWaveQty > 0L && dcQty.equals(shipWaveQty)) {
			actQty = dcQty;
			actualQty.setValue(actQty);
		}
		
		// Make sure DC Qty = Packout Detail Qty
		String validationError = validateDcVsPackout();
		if (validationError.length() > 0) {
			MessageBox.alert("Validation Error", validationError, null);
			return;
		}

		// Make sure ShipWave Qty = Packout Detail Ship Wave Qty
		validationError = validateShipWaveVsPackout();
		if (validationError.length() > 0) {
			MessageBox.alert("Validation Error", validationError, null);
			return;
		}

		validationError = validateShipWaveVsPackoutandShipFromLoc();
		if (validationError.length() > 0) {
			MessageBox.alert("Validation Error", validationError, null);
			return;
		}

		// save data
		final MessageBox box = MessageBox.wait("Progress",
				"Saving Fulfillment data, please wait...", "Saving...");

		// -- actual qty
		displayModel.setActualQty(actQty);
		displayModel.setDtLastChanged(new Date());
		displayModel.setUserLastChanged(App.getUser().getUsername());

		// -- packout details
		List<DisplayDcAndPackoutDTO> displayDcAndPackoutList = packoutDetailsTree
				.getDisplayDcAndPackoutList();
		for (int i = 0; i < displayDcAndPackoutList.size(); i++) {
			DisplayDcAndPackoutDTO dapDTO = (DisplayDcAndPackoutDTO) displayDcAndPackoutList
					.get(i);
			dapDTO.setDtLastChanged(new Date());
			dapDTO.setUserLastChanged(App.getUser().getUsername());
			dapDTO.setPoDtLastChanged(new Date());
			dapDTO.setPoUserLastChanged(App.getUser().getUsername());
			if (dapDTO.getDtCreated() == null) {
				dapDTO.setDtCreated(new Date());
				dapDTO.setUserCreated(App.getUser().getUsername());
			}
			if (dapDTO.getPoDtCreated() == null) {
				dapDTO.setPoDtCreated(new Date());
				dapDTO.setPoUserCreated(App.getUser().getUsername());
			}
		}

		// -- shipping waves
		List<DisplayShipWaveDTO> displayShippingWaveList = packoutDetailInfoShippingWavesGrid
				.getStore().getModels();
		for (int i = 0; i < displayShippingWaveList.size(); i++) {
			DisplayShipWaveDTO swDTO = (DisplayShipWaveDTO) displayShippingWaveList
					.get(i);
			swDTO.setDtLastChanged(new Date());
			swDTO.setUserLastChanged(App.getUser().getUsername());
			if (swDTO.getDtCreated() == null) {
				swDTO.setDtCreated(new Date());
				swDTO.setUserCreated(App.getUser().getUsername());
			}
		}
		AppService.App.getInstance().savePackoutData(
				displayModel,
				displayDcAndPackoutList,
				displayShippingWaveList,
				new Service.SavePackoutData(displayModel, btn,
						displayTabHeader, this, box, panel, tabItem));
		if (btn == null) {
			clearModel();
			App.getVp().clear();
			App.getVp().add(new DisplayProjectSelection());
		} else if (btn.getText().equals("Yes") && panel != null
				&& tabItem != null) {

		}
		for (int i = 0; i < packoutDetailsTree.getDisplayDcAndPackoutList()
				.size(); i++) {
			packoutDetailsTree.getDisplayDcAndPackoutList().get(i).setDirty(
					false);
		}
		packoutDetailInfoShippingWavesGrid.setDisplayShipWaveModified(false);
		packoutDetailsTree.setDisplayPackoutModified(false);
	}

	private void overrideSaveOrUpdate(Button btn) {
		overrideActualQuantity(btn);
		saveDCNodes();
		saveDisplayShippingWaves();
		saveDisplayPackouts();
	}

	private void saveOrUpdate(Button btn) {
		saveActuQuantity(btn);
		saveDCNodes();
		saveDisplayShippingWaves();
		saveDisplayPackouts();
	}

	private void overrideActualQuantity(Button btn) {
		Long actualQuantity = packoutDetailsTree.getSumOfAllDCNodes();
		this.actualQty.setValue(actualQuantity);
		displayModel.setActualQty(actualQuantity);
		final MessageBox box = MessageBox.wait("Progress",
				"Saving actual qty, please wait...", "Saving...");
		AppService.App.getInstance().saveDisplayDTO(
				displayModel,
				new Service.SaveDisplayDTO(displayModel, displayTabHeader, btn,
						box));
	}

	private void saveActuQuantity(Button btn) {
		// Long actualForecastQuantity =
		// packoutDetailsTree.getSumOfAllDCNodes();
		Long longValue = Long.valueOf(actualQty.getRawValue());
		displayModel.setActualQty(longValue);
		final MessageBox box = MessageBox.wait("Progress",
				"Saving actual qty, please wait...", "Saving...");
		AppService.App.getInstance().saveDisplayDTO(
				displayModel,
				new Service.SaveDisplayDTO(displayModel, displayTabHeader, btn,
						box));
	}

	private void saveDCNodes() {
		packoutDetailsTree.saveDCNodes();
	}

	private void saveDisplayShippingWaves() {
		this.packoutDetailInfoShippingWavesGrid.saveDisplayShippingWaves();
	}

	private void saveDisplayPackouts() {
		packoutDetailsTree.saveDisplayPackouts();
	}

	private boolean validateControls() {
		if (!actualQty.validate())
			return false;
		if (!packoutDetailsTree.validateControls())
			return false;
		if (!packoutDetailInfoShippingWavesGrid.validateControls())
			return false;
		return true;
	}

	private boolean overrideForecastQty() {
		if (packoutDetailsTree.getSumOfAllDCNodes().equals(0L))
			return false;
		if (packoutDetailInfoShippingWavesGrid.getDisplayShippingWaveSum()
				.equals(0L))
			return false;
		return (packoutDetailsTree.getSumOfAllDCNodes()
				.equals(packoutDetailInfoShippingWavesGrid
						.getDisplayShippingWaveSum()))
				&& !checkSum1();
	}

	private boolean checkSum1() {
		if (packoutDetailsTree.getSumOfAllDCNodes().equals(0L))
			return true;
		return packoutDetailsTree.getSumOfAllDCNodes().equals(
				this.getActualForecastQuantity());
	}

	private boolean checkSum2() {
		if (packoutDetailInfoShippingWavesGrid.getDisplayShippingWaveSum()
				.equals(0L))
			return true;
		return packoutDetailInfoShippingWavesGrid.getDisplayShippingWaveSum()
				.equals(getActualForecastQuantity());
	}

	private boolean checkSum3() {
		return packoutDetailsTree.getSumOfAllDCNodes().equals(
				packoutDetailInfoShippingWavesGrid.getDisplayShippingWaveSum());
	}

	private boolean checkSum4() {
		return packoutDetailsTree
				.compareDCNodeTotalQtyAgainstPackoutDetailsQty();
	}

	private boolean checkSum5() {
		return packoutDetailsTree.crossValidatePackoutDetailsVsShipWaves();
	}

	private void updateView() {
		if (displayModel.getDisplayId() != null) {
			displayTabHeader.setDisplay(displayModel);
			actualQty.setValue(displayModel.getActualQty());
			//MessageBox.alert("PackoutDetailInfo.updateView()..1..", "displayModel.getActualQty() = "+ displayModel.getActualQty(), null);
		}
	}

	private void clearModel() {
		if (displayModel != null)
			displayModel.removeChangeListener(displayListener);
		displayModel = null;
		displayTabs.resetDisplayModel();
		setData("display", null);
		packoutDetailsTree.clearModel();
	}

	public void reload() {
		this.displayModel = displayTabs.getDisplayModel();
		if (this.displayModel != null
				&& this.displayModel.getDisplayId() != null) {
			AppService.App.getInstance().getDisplayDTO(displayModel,
					new Service.ReloadDisplay(displayModel));
			loadDisplayShippingWaves();
			this.packoutDetailsTree.reload();
			overrideDirty = false;
			updateView();
		}
	}

	public DisplayTabs getDisplayTabs() {
		return displayTabs;
	}

	public void setDisplayTabs(DisplayTabs displayTabs) {
		this.displayTabs = displayTabs;
	}

	public Map<Long, DisplayDcDTO> getDcMap() {
		return dcMap;
	}

	public void setDcMap(Map<Long, DisplayDcDTO> dcMap) {
		this.dcMap = dcMap;
	}

	public Long getActualForecastQuantity() {
		if (!this.actualQty.getRawValue().trim().equals(""))
			return Long.valueOf(this.actualQty.getRawValue());
		else
			return 0L;
	}

	public void setActualForecastQuantity(Long value) {
		this.actualQty.setValue(value);
	}

	public void loadDisplayShippingWaves() {
		AppService.App.getInstance().loadDisplayShippingWaveDTOList(
				this.displayModel,
				new Service.LoadDisplayShippingWaveDTOList(this));
	}

	public FieldSet getPackoutDetailsFieldSet() {
		return packoutDetailsFieldSet;
	}

	public void updatePackoutDetailsFieldSetHeading() {
		packoutDetailsTree.getQuantities();
		String heading = PACKOUT_DETAILS_HEADER + " - Total Qty ("
				+ this.packoutDetailsTree.getSumOfAllDCNodes().toString()
				+ ") ";
		packoutDetailsFieldSet.setHeading(heading);
	}

	public void updateShippingWavesFieldSetHeading() {
		this.packoutDetailInfoShippingWavesGrid.getQuantities();
		String heading = SHIPPING_WAVES_HEADER
				+ " - Total Qty ("
				+ this.packoutDetailInfoShippingWavesGrid
						.getDisplayShippingWaveSum().toString() + ") ";
		this.shippingWavesFieldSet.setHeading(heading);
	}

	public PackoutDetailInfoGrid getPackoutDetailInfoShippingWavesGrid() {
		return this.packoutDetailInfoShippingWavesGrid;
	}

	private Boolean checkForDuplicateDC() {
		HashMap<String, Long> dcQty = packoutDetailsTree.getDcQty();
		List<ModelData> l = packoutDetailsTree.getStore().getModels();

		if (dcQty.size() > 0) {
			Iterator<String> dcIter = dcQty.keySet().iterator();
			while (dcIter.hasNext()) {
				int count = 0;
				String dc = (String) dcIter.next();
				for (int i = 0; i < l.size(); i++) {
					ModelData item = (ModelData) l.get(i);
					if (item instanceof Folder) {
						if (dc.equals(((Folder) item).getDcCode()))
							count++;
					}
				}
				if (count > 1)
					return true;
			}
		}

		return false;
	}

	private String validateDcVsPackout() {
		String msg = "";
		HashMap<String, Long> dcQty = packoutDetailsTree.getDcQty();
		HashMap<String, Long> sumPackoutByDc = packoutDetailsTree
				.getSumPackoutByDc();

		if (dcQty.size() > 0 && sumPackoutByDc.size() > 0) {
			Iterator<String> dcIter = dcQty.keySet().iterator();
			while (dcIter.hasNext()) {
				String dc = (String) dcIter.next();
				Long dQty = (Long) dcQty.get(dc);
				Long packoutQty = (Long) sumPackoutByDc.get(dc);
				System.out.println("validateDcVsPackout(): dcQty = (" + dQty
						+ ") = packoutQty (" + packoutQty + ")");
				if (!dQty.equals(packoutQty))
					msg += "Packout Qty for DC " + dc
							+ " does not match DC qty. ";
			}
		}

		return msg;
	}

	private String validateShipWaveVsPackoutandShipFromLoc() {
		List<ModelData> lInner;
		List<ModelData> lOuter = packoutDetailsTree.getStore().getModels();
		Iterator<ModelData> iterInner;
		Iterator<ModelData> iterOuter = lOuter.iterator();
		String dc;
		Long swNum;
		String sFDc;
		String dcCode;
		String deleted;
		Long shipWave;
		String shipFromDcCode = null;
		Long shipWaveNum = null;
		int recOccurance = 0;
		String msg = "";
		ModelData itemOuter;
		ModelData item;
		while (iterOuter.hasNext()) {
			itemOuter = (ModelData) iterOuter.next();
			if (itemOuter instanceof DisplayDcAndPackoutDTO) {
				dc = ((DisplayDcAndPackoutDTO) itemOuter).getDcCode();
				swNum = ((DisplayDcAndPackoutDTO) itemOuter).getShipWaveNum();
				sFDc = ((DisplayDcAndPackoutDTO) itemOuter).getShipFromDcCode();
				recOccurance = 0;
				lInner = packoutDetailsTree.getStore().getModels();
				iterInner = lInner.iterator();
				while (iterInner.hasNext()) {
					item = (ModelData) iterInner.next();
					if (item instanceof DisplayDcAndPackoutDTO) {
						dcCode = ((DisplayDcAndPackoutDTO) item).getDcCode();
						deleted = ((DisplayDcAndPackoutDTO) item)
								.getDeleteFlg();
						if (dc.equals(dcCode)
								&& (deleted == null || deleted.equals("N"))) {
							shipWave = new Long(0);
							shipWaveNum = ((DisplayDcAndPackoutDTO) item)
									.getShipWaveNum();
							shipFromDcCode = ((DisplayDcAndPackoutDTO) item)
									.getShipFromDcCode();
							if (shipWaveNum != null)
								shipWave = new Long(shipWaveNum.intValue());
							if (swNum.equals(shipWave)
									&& sFDc.equals(shipFromDcCode)) {
								recOccurance++;
								if (recOccurance > 1) {
									msg = "Duplicate Packout Loc, Ship From Loc, Ship Wave combo exists ["
											+ dcCode
											+ ","
											+ shipFromDcCode
											+ "," + shipWave + "]";
								}
							}
						}
					}
				}
			}
		}
		return msg;
	}

	private String validateShipWaveVsPackout() {
		String msg = "";
		HashMap<Integer, Long> shipWaveQty = packoutDetailInfoShippingWavesGrid
				.getShipWaveQty();
		HashMap<Integer, Long> sumPackoutByShipWave = packoutDetailsTree
				.getSumPackoutByShipWave();

		if (shipWaveQty.size() > 0 && sumPackoutByShipWave.size() > 0) {
			Iterator<Integer> swIter = shipWaveQty.keySet().iterator();
			while (swIter.hasNext()) {
				Integer sw = (Integer) swIter.next();
				Long swQty = (Long) shipWaveQty.get(sw);
				Long packoutQty = (Long) sumPackoutByShipWave.get(sw);
				if (!swQty.equals(packoutQty))
					msg += "Packout Qty for Ship Wave Number " + sw
							+ " does not match Ship Wave Qty. ";
			}
		}
		return msg;
	}
}