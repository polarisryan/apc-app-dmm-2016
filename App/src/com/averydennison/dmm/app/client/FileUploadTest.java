package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;


import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FileUploadField;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.FormPanel.Encoding;
import com.extjs.gxt.ui.client.widget.form.FormPanel.Method;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.user.client.Element;

public class FileUploadTest extends LayoutContainer {

	@Override
	  protected void onRender(Element parent, int index) {
	    super.onRender(parent, index);
	    setStyleAttribute("margin", "10px");

	    final FormPanel panel = new FormPanel();
	    //panel.setHeading("Upload an Image or Rendering");
	    panel.setHeaderVisible(false);
	    panel.setFrame(false);
	    panel.setAction("imageUrlPost");
	    panel.setEncoding(Encoding.MULTIPART);
	    panel.setMethod(Method.POST);
	    panel.setButtonAlign(HorizontalAlignment.CENTER);
	    panel.setWidth(225);
	    
	    
	    LayoutContainer main = new LayoutContainer();
    	main.setLayout(new ColumnLayout());
    	
	    LayoutContainer middleRight = new LayoutContainer();
	    FormLayout leftLayout = new FormLayout();
        middleRight.setStyleAttribute("paddingRight","5px");
        leftLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        
	    FieldSet imageUploadData = new FieldSet();
        FormLayout displayCostDataFormLayout = new FormLayout();
        displayCostDataFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        imageUploadData.setLayout(displayCostDataFormLayout);
        imageUploadData.setHeading("Upload an Image or Rendering");
        middleRight.add(imageUploadData);

        

        
	    FileUploadField file = new FileUploadField();
	    file.setHideLabel(true);
	    //file.setFieldLabel("File");
	   // file.setWidth(width)
	    panel.add(file);
	    
	    
	    imageUploadData.add(panel, new FormData("95%"));
	    
	    middleRight.add(imageUploadData);
	    main.add(middleRight);//, new ColumnData(.335));
	    
	    Button btn = new Button("Reset");   
	    btn.addSelectionListener(new SelectionListener<ButtonEvent>() {   
	      @Override  
	      public void componentSelected(ButtonEvent ce) {   
	        panel.reset();   
	      }   
	    });   
	    panel.addButton(btn);   
	    
	    btn = new Button("Submit");
	    btn.addSelectionListener(new SelectionListener<ButtonEvent>() {
	    
	      @Override
	      public void componentSelected(ButtonEvent ce) {
	        if (!panel.isValid()) {
	          return;
	        }
	        
	        // normally would submit the form but for example no server set up to 
	        // handle the post
	        panel.submit();
	        MessageBox.info("Action", "You file was uploaded", null);
	      }
	    });
	    
	   
	    panel.addButton(btn);
	    add(main);
	    //add(main, new MarginData(10));
	    //add(panel);
	  }
}
