package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.AggregationRenderer;
import com.extjs.gxt.ui.client.widget.grid.AggregationRowConfig;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.SummaryType;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.table.NumberCellRenderer;
import com.google.gwt.i18n.client.NumberFormat;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.MessageBox;
/**
 * User: Spart Arguello
 * Date: Oct 27, 2009
 * Time: 8:10:46 AM
 */
public class CostAnalysisInfoGrid extends LayoutContainer {
	
    private static final ListStore<CostAnalysisKitComponentDTO> store = new ListStore<CostAnalysisKitComponentDTO>();
    public static final int PIM_COL_INDEX=5;
    public static final int PV_COL_INDEX=6;
    public static final int I2_COL_INDEX=7;
    public static final int PBL_COL_INDEX=8;
    private DisplayDTO displayDTO;
    Grid<CostAnalysisKitComponentDTO> grid;
    private double customerProgramPct;
    
    public DisplayDTO getDisplayDTO() {
		return displayDTO;
	}

	public void setDisplayDTO(DisplayDTO displayDTO) {
		this.displayDTO = displayDTO;
	}


	public double getCustomerProgramPct() {
		return customerProgramPct;
	}

	public void setCustomerProgramPct(double customerProgramPct) {
		this.customerProgramPct = customerProgramPct;
	}

	public ContentPanel createCostAnalysisInfoGrid() {
       /* setLayout(new FlowLayout(0));
        int x = 100;
        final NumberFormat number = NumberFormat.getFormat("0.00");
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        ColumnConfig column = new ColumnConfig();
        TextField<String> skuValue = new TextField<String>(); 
		CellEditor skuExceptionEditor =  new CellEditor(skuValue); 
		skuValue.disable();
        skuExceptionEditor.disable();
        column.setId(CostAnalysisKitComponentDTO.SKU);
        column.setEditor(skuExceptionEditor);
        column.setHeader("<center>SKU</center>");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x+15);
        configs.add(column);

        
        column = new ColumnConfig();
        TextField<String> chkDgtValue = new TextField<String>(); 
		CellEditor chkDgtEditor =  new CellEditor(chkDgtValue); 
		chkDgtValue.disable();
		chkDgtEditor.disable();
        column.setEditor(chkDgtEditor);
        column.setId(CostAnalysisKitComponentDTO.CHKDGT);
        column.setHeader("<center>Check<br>Digit</center>");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x/2);
        configs.add(column);

        column = new ColumnConfig();
        TextField<String> buDescValue = new TextField<String>(); 
		CellEditor buDescEditor =  new CellEditor(buDescValue); 
		buDescValue.disable();
		buDescEditor.disable();
        column.setEditor(buDescEditor);
        column.setId(CostAnalysisKitComponentDTO.BU_DESC);
        column.setHeader("<center>Business<br>Unit</center>");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x+50);
        configs.add(column);

        column = new ColumnConfig();
        TextField<String> prodNameValue = new TextField<String>(); 
		CellEditor prodNameEditor =  new CellEditor(prodNameValue); 
		prodNameValue.disable();
		prodNameEditor.disable();
		 column.setEditor(prodNameEditor);
        column.setId(CostAnalysisKitComponentDTO.PRODUCT_NAME);
        column.setHeader("<center>Description</center>");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x*2);
        configs.add(column);

      
		
        column = new ColumnConfig();
        //TextField<String> regCasePackValue = new TextField<String>();
        final NumberField regCasePackValue = new NumberField();
        regCasePackValue.setPropertyEditorType(Long.class);
		CellEditor regCasePackEditor =  new CellEditor(regCasePackValue); 
		regCasePackValue.disable();
		regCasePackEditor.disable();
		column.setEditor(regCasePackEditor);
        column.setId(CostAnalysisKitComponentDTO.REG_CASE_PACK);
        column.setHeader("<center>Retail<br>Packs Per<br>Inner Carton</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x -30);
        //column.setEditor(qtyRetailPacksPerEditor);
        configs.add(column);
        
        CheckColumnConfig pimCol = new CheckColumnConfig(CostAnalysisKitComponentDTO.SOURCE_PIM_FLG, "PIM", 55){
            protected String getCheckState(ModelData model, String property, int rowIndex,
                    int colIndex) {
                  return "-disabled";
                }
              };
        
        CheckBox pimValue = new CheckBox(); 
        final CellEditor checkBoxPimEditor = new CellEditor(pimValue);
        pimValue.disable();
        checkBoxPimEditor.disable();
        pimCol.setEditor(checkBoxPimEditor);
        //final CellEditor checkBoxPimEditor = new CellEditor(new CheckBox());
        checkBoxPimEditor.disable();
        checkBoxPimEditor.cancelEdit();
        //pimCol.setEditor(checkBoxPimEditor);
        pimCol.addListener(Events.CellDoubleClick, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
            	be.setCancelled(true);
            	checkBoxPimEditor.disable();
            }
        });
        pimCol.setWidth(x / 3);
        configs.add(pimCol);
        
        CheckColumnConfig pvCol = new CheckColumnConfig(CostAnalysisKitComponentDTO.PROMO_FLG, "PV", 55){
            protected String getCheckState(ModelData model, String property, int rowIndex,
                    int colIndex) {
                  return "-disabled";
                }
              };
        final CellEditor checkBoxPvEditor = new CellEditor(new CheckBox());
        checkBoxPvEditor.cancelEdit();
        pvCol.setEditor(checkBoxPvEditor);
        
              CheckBox promoValue = new CheckBox(); 
              final CellEditor checkBoxPromoditor = new CellEditor(promoValue);
              promoValue.disable();
              checkBoxPromoditor.disable();
              pvCol.setEditor(checkBoxPromoditor);
              
          
        pvCol.addListener(Events.CellMouseDown, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
            	be.setCancelled(true);
            	checkBoxPvEditor.disable();

            }
        });
        pvCol.setWidth(x / 3);
        configs.add(pvCol);
        
        CheckColumnConfig i2Col = new CheckColumnConfig(CostAnalysisKitComponentDTO.I2_FORECAST_FLG, "I2", 55);
        CellEditor checkBoxEditorI2 = new CellEditor(new CheckBox());
        checkBoxEditorI2.enable();
        i2Col.setEditor(checkBoxEditorI2);
        i2Col.setWidth(x / 3);
        configs.add(i2Col);
        
        CheckColumnConfig pbcBOM = new CheckColumnConfig(CostAnalysisKitComponentDTO.PLANT_BREAK_CASE_FLG, "PBC<br>BOM", 55){
            protected String getCheckState(ModelData model, String property, int rowIndex,
                    int colIndex) {
                  return "-disabled";
                }
              };
        final CellEditor checkBoxEditorPbc = new CellEditor(new CheckBox());
        checkBoxEditorPbc.cancelEdit();
        checkBoxEditorPbc.disableEvents(true);
        pbcBOM.setEditor(checkBoxEditorPbc);
        
        CheckBox plantBreakCaseValue = new CheckBox(); 
        final CellEditor checkBoxPlantBreakCaseditor = new CellEditor(plantBreakCaseValue);
        plantBreakCaseValue.disable();
        checkBoxPlantBreakCaseditor.disable();
        pbcBOM.setEditor(checkBoxPlantBreakCaseditor);
        
        pbcBOM.addListener(Events.CellMouseDown, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
            	be.setCancelled(true);
            	checkBoxEditorPbc.disable();
            }
        });
        pbcBOM.setWidth(x / 3);
        configs.add(pbcBOM);
        
       
        column = new ColumnConfig();
        TextField<String> pbcVersionValue = new TextField<String>(); 
		CellEditor pbcVersionValueEditor =  new CellEditor(pbcVersionValue); 
		pbcVersionValue.disable();
		pbcVersionValueEditor.disable();
		column.setEditor(pbcVersionValueEditor);
        column.setId(CostAnalysisKitComponentDTO.PBC_VERSION);
        column.setHeader("<center>PBC<br>Vers</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x-30);
        configs.add(column);
        
        final NumberField nQtyPerFacing = new NumberField(); 
        nQtyPerFacing.setPropertyEditorType(Long.class);
        nQtyPerFacing.addInputStyleName("text-element-no-underline");	
        nQtyPerFacing.setMaxLength(12);
		final CellEditor qtyPerFacingEditor =  new CellEditor(nQtyPerFacing); 
		
        column = new ColumnConfig();
        column.setId(CostAnalysisKitComponentDTO.QTY_PER_FACING);
        column.setHeader("<center>Qty<br>Per<br>Facing</center>");
        column.setWidth(x / 2);
        column.setAlignment(HorizontalAlignment.RIGHT);
        column.setEditor(qtyPerFacingEditor);
        configs.add(column);

        final NumberField nNoPerFacing = new NumberField(); 
        nNoPerFacing.setPropertyEditorType(Long.class);
       // final CurrencyField nNoPerFacing = new CurrencyField();
        nNoPerFacing.addInputStyleName("text-element-no-underline");	
        nNoPerFacing.setMaxLength(12);
		final CellEditor noPerFacingEditor =  new CellEditor(nNoPerFacing); 
		
        column = new ColumnConfig();
        column.setId(CostAnalysisKitComponentDTO.NUMBER_FACINGS);
        column.setHeader("<center>#<br>of<br>Facings</center>");
        column.setAlignment(HorizontalAlignment.RIGHT);
        column.setWidth(x / 2);
        column.setEditor(noPerFacingEditor);
        configs.add(column);

        column = new ColumnConfig();
        final NumberField qtyPerDisplayValue = new NumberField();
        //final CurrencyField qtyPerDisplayValue = new CurrencyField();
        qtyPerDisplayValue.addInputStyleName("text-element-no-underline");	
        qtyPerDisplayValue.setPropertyEditorType(Long.class);
		CellEditor qtyPerDisplayEditor =  new CellEditor(qtyPerDisplayValue); 
		qtyPerDisplayValue.disable();
		qtyPerDisplayEditor.disable();
		column.setEditor(qtyPerDisplayEditor);
        column.setId(CostAnalysisKitComponentDTO.QTY_PER_DISPLAY);
        column.setHeader("<center>Qty<br>Per<br>Display</center>");
        column.setAlignment(HorizontalAlignment.RIGHT);
        column.setWidth(x / 2);
        configs.add(column);

        column = new ColumnConfig();
        //TextField<String> inventoryReqValue = new TextField<String>(); 
       final NumberField inventoryReqValue = new NumberField();
       // final CurrencyField inventoryReqValue = new CurrencyField();
        inventoryReqValue.addInputStyleName("text-element-no-underline");	
        inventoryReqValue.setPropertyEditorType(Long.class);
		CellEditor inventoryReqEditor =  new CellEditor(inventoryReqValue); 
		inventoryReqValue.disable();
		inventoryReqEditor.disable();
		column.setEditor(inventoryReqEditor);
        column.setId(CostAnalysisKitComponentDTO.INVENTORY_REQUIREMENT);
        column.setHeader("<center>Inv<br>Req</center>");
        column.setAlignment(HorizontalAlignment.RIGHT);
        column.setWidth(x / 2);
        configs.add(column);
        
        final NumberField nPriceException = new NumberField(); 
        nPriceException.setPropertyEditorType(Double.class);
       // final CurrencyField nPriceException = new CurrencyField();
        nPriceException.addInputStyleName("text-element-no-underline");	
        nPriceException.setMaxLength(12);
		final CellEditor priceExceptionEditor =  new CellEditor(nPriceException); 
		
        column = new ColumnConfig();
        column.setId(CostAnalysisKitComponentDTO.PRICE_EXCEPTION);
        column.setHeader("<center>Price<br>Exception</center>");
        column.setAlignment(HorizontalAlignment.RIGHT);
        column.setEditor(priceExceptionEditor);
        column.setWidth(x);
        configs.add(column);
        
        column = new ColumnConfig();
        NumberField actualInvoicePerEaValue = new NumberField(); 
        actualInvoicePerEaValue.setPropertyEditorType(Double.class);
       // final CurrencyField actualInvoicePerEaValue = new CurrencyField();
        actualInvoicePerEaValue.addInputStyleName("text-element-no-underline");	
		CellEditor actualInvoicePerEaEditor =  new CellEditor(actualInvoicePerEaValue); 
		actualInvoicePerEaValue.disable();
		actualInvoicePerEaEditor.disable();
		column.setEditor(actualInvoicePerEaEditor);
        column.setId(CostAnalysisKitComponentDTO.ACTUAL_INVOICE_PER_EA);
        column.setHeader("<center>Actual<br>Invoice<br>Per Ea.$</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
      final  NumberField totActualInvoiceValue = new NumberField();
      //  final CurrencyField totActualInvoiceValue = new CurrencyField();
        totActualInvoiceValue.addInputStyleName("text-element-no-underline");	
        totActualInvoiceValue.setPropertyEditorType(Double.class);
		CellEditor totActualInvoiceEditor =  new CellEditor(totActualInvoiceValue); 
		totActualInvoiceValue.disable();
		totActualInvoiceEditor.disable();
		column.setEditor(totActualInvoiceEditor);
        column.setId(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE);
        column.setHeader("<center>Total<br>Actual<br>Invoice $</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
       final NumberField cogsUnitPriceValue = new NumberField();
       cogsUnitPriceValue.setPropertyEditorType(Double.class);

      //  final CurrencyField cogsUnitPriceValue = new CurrencyField();
        cogsUnitPriceValue.addInputStyleName("text-element-no-underline");		
		CellEditor cogsUnitPriceEditor =  new CellEditor(cogsUnitPriceValue); 
		cogsUnitPriceValue.disable();
		cogsUnitPriceEditor.disable();
		column.setEditor(cogsUnitPriceEditor);
        column.setId(CostAnalysisKitComponentDTO.COGS_UNIT_PRICE);
        column.setHeader("<center>COGS<br>Per<br>Ea.$</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        final NumberField totCogsUnitPriceValue = new NumberField();
        totCogsUnitPriceValue.setPropertyEditorType(Double.class);
        //TextField<String> totCogsUnitPriceValue = new TextField<String>();
      //  final CurrencyField totCogsUnitPriceValue = new CurrencyField();
        totCogsUnitPriceValue.addInputStyleName("text-element-no-underline");	
		CellEditor totCogsUnitPriceEditor =  new CellEditor(totCogsUnitPriceValue); 
		totCogsUnitPriceValue.disable();
		totCogsUnitPriceEditor.disable();
		column.setEditor(totCogsUnitPriceEditor);
        column.setId(CostAnalysisKitComponentDTO.TOTAL_COGS_UNIT_PRICE);
        column.setHeader("<center>Total<br>COGS$</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        //TextField<String> netVm$Value = new TextField<String>();
       // final CurrencyField netVm$Value = new CurrencyField();
        final NumberField netVm$Value = new NumberField();
        netVm$Value .setPropertyEditorType(Double.class);
        netVm$Value.addInputStyleName("text-element-no-underline");	
		CellEditor netVm$Editor =  new CellEditor(netVm$Value); 
		netVm$Value.disable();
		netVm$Editor.disable();
		column.setEditor(netVm$Editor);
        column.setId(CostAnalysisKitComponentDTO.NET_VM_$);
        column.setHeader("<center>Net<br>VM<br>$</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        //TextField<String> netVmPctValue = new TextField<String>();
       // final CurrencyField netVmPctValue = new CurrencyField();
        
        final NumberField netVmPctValue = new NumberField();
        netVmPctValue .setPropertyEditorType(Double.class);
        netVmPctValue.addInputStyleName("text-element-no-underline");	
		CellEditor netVmPctEditor =  new CellEditor(netVmPctValue); 
		netVmPctValue.disable();
		netVmPctEditor.disable();
		column.setEditor(netVmPctEditor);
        column.setId(CostAnalysisKitComponentDTO.NET_VM_PCT);
        column.setHeader("<center>Net<br>VM$</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        //TextField<String> benchMarkPerEachValue = new TextField<String>();
       // final CurrencyField benchMarkPerEachValue = new CurrencyField();
        final NumberField benchMarkPerEachValue = new NumberField();
        benchMarkPerEachValue .setPropertyEditorType(Double.class);
        benchMarkPerEachValue.addInputStyleName("text-element-no-underline");	
		CellEditor benchMarkPerEachEditor =  new CellEditor(benchMarkPerEachValue); 
		benchMarkPerEachValue.disable();
		benchMarkPerEachEditor.disable();
		column.setEditor(benchMarkPerEachEditor);
        column.setId(CostAnalysisKitComponentDTO.BENCH_MARK_PER_EACH);
        column.setHeader("<center>Benchmark<br>Per Each<br>$</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        //TextField<String> totBenchMarkValue = new TextField<String>();
      //  final CurrencyField totBenchMarkValue = new CurrencyField();
        final NumberField totBenchMarkValue = new NumberField();
        totBenchMarkValue .setPropertyEditorType(Double.class);
        totBenchMarkValue.addInputStyleName("text-element-no-underline");	
		CellEditor totBenchMarkEditor =  new CellEditor(totBenchMarkValue); 
		totBenchMarkValue.disable();
		totBenchMarkEditor.disable();
		column.setEditor(totBenchMarkEditor);
        column.setId(CostAnalysisKitComponentDTO.TOTAL_BENCH_MARK);
        column.setHeader("<center>Total<br>Benchmark<br>$</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
       // TextField<String> listPricePerEaValue = new TextField<String>();
      //  final CurrencyField listPricePerEaValue = new CurrencyField();
        final NumberField listPricePerEaValue = new NumberField();
        listPricePerEaValue .setPropertyEditorType(Double.class);
        listPricePerEaValue.addInputStyleName("text-element-no-underline");	
		CellEditor listPricePerEaEditor =  new CellEditor(listPricePerEaValue); 
		listPricePerEaValue.disable();
		listPricePerEaEditor.disable();
		column.setEditor(listPricePerEaEditor);
        column.setId(CostAnalysisKitComponentDTO.LIST_PRICE_PER_EA);
        column.setHeader("<center>Unit Price<br>Per Ea $</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        final NumberField tot_ListPriceValue = new NumberField();
        tot_ListPriceValue .setPropertyEditorType(Double.class);
        tot_ListPriceValue.addInputStyleName("text-element-no-underline");	
		CellEditor tot_ListPriceEditor =  new CellEditor(tot_ListPriceValue); 
		tot_ListPriceValue.disable();
		tot_ListPriceEditor.disable();
		column.setEditor(tot_ListPriceEditor);
        column.setId(CostAnalysisKitComponentDTO.TOTAL_LIST_PRICE);
        column.setHeader("<center>Total List<br>Price $</center>");
        column.setAlignment(Style.HorizontalAlignment.RIGHT);
        column.setWidth(x );
        configs.add(column);

        ColumnModel cm = new ColumnModel(configs);

        
        AggregationRowConfig<CostAnalysisKitComponentDTO> summary = new AggregationRowConfig<CostAnalysisKitComponentDTO>();   
		summary.setHtml(CostAnalysisKitComponentDTO.PBC_VERSION, "<b>Total</b>");   
		summary.setSummaryType(CostAnalysisKitComponentDTO.QTY_PER_FACING, SummaryType.SUM);   
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.QTY_PER_FACING, NumberFormat.getFormat(("#######0")));
		summary.setSummaryType(CostAnalysisKitComponentDTO.NUMBER_FACINGS, SummaryType.SUM);   
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.NUMBER_FACINGS, NumberFormat.getFormat(("#######0"))); 

		summary.setSummaryType(CostAnalysisKitComponentDTO.QTY_PER_DISPLAY, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.QTY_PER_DISPLAY, NumberFormat.getFormat(("#######0"))); 
	
		summary.setSummaryType(CostAnalysisKitComponentDTO.ACTUAL_INVOICE_PER_EA, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.ACTUAL_INVOICE_PER_EA, NumberFormat.getFormat(("$#,###,###0.00"))); 
		
		summary.setRenderer(CostAnalysisKitComponentDTO.ACTUAL_INVOICE_PER_EA, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			 		}
		});
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE, NumberFormat.getFormat(("$#,###,###0.00"))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.TOTAL_ACTUAL_INVOICE, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			 		}
		});
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.PRICE_EXCEPTION, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.PRICE_EXCEPTION, NumberFormat.getFormat(("$#,###,###0.00"))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.PRICE_EXCEPTION, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			 		}
		});
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.COGS_UNIT_PRICE, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.COGS_UNIT_PRICE, NumberFormat.getFormat(("$#,###,###0.00"))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.COGS_UNIT_PRICE, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			 		}
		});
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.TOTAL_COGS_UNIT_PRICE, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.TOTAL_COGS_UNIT_PRICE, NumberFormat.getFormat(("$#,###,###0.00"))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.TOTAL_COGS_UNIT_PRICE, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			 		}
		});
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.BENCH_MARK_PER_EACH, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.BENCH_MARK_PER_EACH, NumberFormat.getFormat(("$#,###,###0.00"))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.BENCH_MARK_PER_EACH, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			 		}
		});
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.TOTAL_BENCH_MARK, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.TOTAL_BENCH_MARK, NumberFormat.getFormat(("$#,###,###0.00"))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.TOTAL_BENCH_MARK, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			 		}
		});
		
		summary.setSummaryType(CostAnalysisKitComponentDTO.LIST_PRICE_PER_EA, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.LIST_PRICE_PER_EA, NumberFormat.getFormat(("$#,###,###0.00"))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.LIST_PRICE_PER_EA, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
			public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
				String result="";
				if(value!=null){
					result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
				}
				return result;
			 		}
		});

		summary.setSummaryType(CostAnalysisKitComponentDTO.TOTAL_LIST_PRICE, SummaryType.SUM); 
		summary.setSummaryFormat(CostAnalysisKitComponentDTO.TOTAL_LIST_PRICE, NumberFormat.getFormat(("$#,###,###0.00"))); 
		summary.setRenderer(CostAnalysisKitComponentDTO.TOTAL_LIST_PRICE, new AggregationRenderer<CostAnalysisKitComponentDTO>() {
				public Object render(Number value, int colIndex, Grid<CostAnalysisKitComponentDTO> grid, ListStore<CostAnalysisKitComponentDTO> store) {
					String result="";
					if(value!=null){
						result="<span style='font-weight:bold;' >" + number.format(value.doubleValue()) + "</span>";
					}
					return result;
				 		}
		});
		 
		cm.addAggregationRow(summary);
		loadCostAnalysisComponentInfo();
        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
        cp.setLayout(new FitLayout());
        cp.setSize(900, 300);
        cp.setFrame(false);
        cp.setHeight(300);
        grid = new Grid<CostAnalysisKitComponentDTO>(store, cm);
        GridSelectionModel<CostAnalysisKitComponentDTO> gm = new GridSelectionModel<CostAnalysisKitComponentDTO>();
        final CustomRowEditor<CostAnalysisKitComponentDTO> re = new CustomRowEditor<CostAnalysisKitComponentDTO>(this);
        re.setClicksToEdit(ClicksToEdit.ONE);
        re.setBodyStyleName("x-row-editor-body");
        grid.setColumnResize(false);
		grid.addPlugin(re); 
		grid.setSelectionModel(gm);
        grid.setStyleAttribute("borderTop", "none");
        grid.setBorders(true);
        
        re.addListener(Events.CellMouseDown, new Listener<GridEvent<ModelData>>() {
	       	public void handleEvent(GridEvent<ModelData> be) {
	       		int selectedRow = be.getRowIndex();
	       		int selectedCol = be.getColIndex();
	       		if((selectedRow > -1) && ( selectedCol==PIM_COL_INDEX || selectedCol==PV_COL_INDEX || selectedCol==PBL_COL_INDEX ) ){
	       			be.setCancelled(true);
	       		}
	       }
        });
        
        cp.add(grid);
        return cp;*/
		return null;
    }


    
 

  
    
    
    public void rejectChanges(){
		store.rejectChanges();
	}
    


	private CostAnalysisInfoGrid getSelectionGrid(){
		return this;
	}
	
	public Boolean validateControls(List<Record> mods) {
		for (int i = 0; i < mods.size(); i++) {
			CostAnalysisKitComponentDTO itemProj = (CostAnalysisKitComponentDTO)((Record)mods.get(i)).getModel();
			store.commitChanges();
			
		}
		return true;
	}

	
}
