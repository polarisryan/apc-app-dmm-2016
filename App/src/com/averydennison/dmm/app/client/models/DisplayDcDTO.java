package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Dec 1, 2009
 * Time: 4:39:31 PM
 */
public class DisplayDcDTO extends BaseModel {
    public static final String DISPLAY_DC_ID = "displayDcId";
    public static final String DISPLAY_ID = "displayId";
    public static final String LOCATION_ID = "locationId";
    public static final String QUANTITY = "quantity";
    public static final String DT_CREATED = "dtCreated";
    public static final String DT_LAST_CHANGED = "dtLastChanged";
    public static final String USER_CREATED = "userCreated";
    public static final String USER_LAST_CHANGED = "userLastChanged";

    public DisplayDcDTO() {
    }
    
    public static DisplayDcDTO displayDcDTO(DisplayDcAndPackoutDTO dap) {
    	DisplayDcDTO dcDTO = new DisplayDcDTO();
		dcDTO.setDisplayDcId(dap.getDisplayDcId());			
		dcDTO.setDisplayId(dap.getDisplayId());
		dcDTO.setLocationId(dap.getLocationId());
		dcDTO.setQuantity(dap.getDcQuantity());
		dcDTO.setDtCreated(dap.getDtCreated());
		dcDTO.setDtLastChanged(dap.getDtLastChanged());
		dcDTO.setUserCreated(dap.getUserCreated());
		dcDTO.setUserLastChanged(dap.getUserLastChanged());
		return dcDTO;
    }

    public Long getDisplayDcId() {
        return get(DISPLAY_DC_ID);
    }

    public void setDisplayDcId(Long displayDcId) {
        set(DISPLAY_DC_ID, displayDcId);
    }

    public Long getDisplayId() {
        return get(DISPLAY_ID);
    }

    public void setDisplayId(Long displayId) {
        set(DISPLAY_ID, displayId);
    }

    public Long getLocationId() {
        return get(LOCATION_ID);
    }

    public void setLocationId(Long locationId) {
        set(LOCATION_ID, locationId);
    }

    public Long getQuantity() {
        return get(QUANTITY);
    }

    public void setQuantity(Long quantity) {
        set(QUANTITY, quantity);
    }

    public Date getDtCreated() {
        return get(DT_CREATED);
    }

    public void setDtCreated(Date dtCreated) {
        set(DT_CREATED, dtCreated);
    }

    public Date getDtLastChanged() {
        return get(DT_LAST_CHANGED);
    }

    public void setDtLastChanged(Date dtLastChanged) {
        set(DT_LAST_CHANGED, dtLastChanged);
    }

    public String getUserCreated() {
        return get(USER_CREATED);
    }

    public void setUserCreated(String userCreated) {
        set(USER_CREATED, userCreated);
    }

    public String getUserLastChanged() {
        return get(USER_LAST_CHANGED);
    }

    public void setUserLastChanged(String userLastChanged) {
        set(USER_LAST_CHANGED, userLastChanged);
    }

    public String toString() {
        return "DisplayDc{" +
                "displayDcId=" + this.getDisplayDcId().toString() +
                ", displayId=" + this.getDisplayId().toString() +
                ", locationId=" + this.getLocationId().toString() +
                ", quantity=" + this.getQuantity().toString() +
                ", dtCreated=" + this.getDtCreated().toString() +
                ", dtLastChanged=" + this.getDtLastChanged().toString() +
                ", userCreated='" + this.getUserCreated() + '\'' +
                ", userLastChanged='" + this.getUserLastChanged() + '\'' +
//                ", locationByLocationId=" + locationByLocationId +
//                ", displayByDisplayId=" + displayByDisplayId +
//                ", displayPackoutsByDisplayDcId=" + displayPackoutsByDisplayDcId +
                '}';
    }
}
