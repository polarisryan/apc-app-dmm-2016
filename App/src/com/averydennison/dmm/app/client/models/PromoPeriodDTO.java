package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Nov 6, 2009
 * Time: 11:47:34 AM
 */
public class PromoPeriodDTO extends BaseModel {
    public static final String PROMO_PERIOD_ID = "promoPeriodId";
    public static final String PROMO_PERIOD_NAME = "promoPeriodName";
    public static final String PROMO_PERIOD_DESCRIPTION = "promoPeriodDescription";
    public static final String PROMO_DATE = "promoDate";

    public PromoPeriodDTO() {
    }

    public PromoPeriodDTO(Long promoPeriodId, String promoPeriodName, String promoPeriodDescription, Date promoDate) {
        set(PROMO_PERIOD_ID, promoPeriodId);
        set(PROMO_PERIOD_NAME, promoPeriodName);
        set(PROMO_PERIOD_DESCRIPTION, promoPeriodDescription);
        set(PROMO_DATE, promoDate);
    }

    public Long getPromoPeriodId() {
        return get(PROMO_PERIOD_ID);
    }

    public void setPromoPeriodId(Long promoPeriodId) {
        set(PROMO_PERIOD_ID, promoPeriodId);
    }

    public String getPromoPeriodName() {
        return get(PROMO_PERIOD_NAME);
    }

    public void setPromoPeriodName(String promoPeriodName) {
        set(PROMO_PERIOD_NAME, promoPeriodName);
    }

    public String getPromoPeriodDescription() {
        return get(PROMO_PERIOD_DESCRIPTION);
    }

    public void setPromoPeriodDescription(String promoPeriodDescription) {
        set(PROMO_PERIOD_DESCRIPTION, promoPeriodDescription);
    }

    public Date getPromoDate() {
        return get(PROMO_DATE);
    }

    public void setPromoDate(Date promoDate) {
        set(PROMO_DATE, promoDate);
    }
}
