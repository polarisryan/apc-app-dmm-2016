package com.averydennison.dmm.app.client.models;

import java.util.Date;
import java.util.List;

import com.averydennison.dmm.app.client.LookupData;
import com.averydennison.dmm.app.client.Service;
import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;

/**
 * User: Mari
 * Date: Jun 24, 2010
 * Time: 9:12:45 AM
 */
public class CostAnalysisKitComponentDTO extends BaseModel {
		private static final long serialVersionUID = 6643826035949658170L;
		public static final String DISPLAY_ID  ="displayId"; 
		public static final String SEQUENCE_NUM ="sequenceNum";
	  	public static final String KIT_COMPONENT_ID = "kitComponentId";
	    private static final String  DISPLAY_SCENARIO_ID="displayScenarioId";
	  	public static final String SKU = "sku";
	  	public static final String CHKDGT = "chkDgt";
	    public static final String BU_DESC = "buDesc";
	    public static final String BU_CODE = "buCode";
	  	public static final String PRODUCT_NAME = "productName";
	    public static final String REG_CASE_PACK = "regCasePack";
	    public static final String SOURCE_PIM_FLG = "sourcePimFlg";
	    public static final String PROMO_FLG = "promoFlg";
	    public static final String I2_FORECAST_FLG = "i2ForecastFlg";
	    public static final String PLANT_BREAK_CASE_FLG = "plantBreakCaseFlg";
	    public static final String PBC_VERSION = "pbcVersion";
	    public static final String QTY_PER_FACING = "qtyPerFacing";
	    public static final String NUMBER_FACINGS = "numberFacings";
	    public static final String QTY_PER_DISPLAY = "qtyPerDisplay";
	    public static final String INVENTORY_REQUIREMENT = "inventoryReq";
	    public static final String PRICE_EXCEPTION = "priceException";
	    public static final String ACTUAL_INVOICE_PER_EA = "actualInvoicePerEa";
	    public static final String TOTAL_ACTUAL_INVOICE = "totalActualInvoice";
	    public static final String TOTAL_ACTUAL_INVOICE_WPGRM = "totalActualInvoiceWpgrm";
	    public static final String COGS_UNIT_PRICE = "cogsUnitPrice";
	    public static final String TOTAL_COGS_UNIT_PRICE = "totalCogsUnitPrice";
	    public static final String NET_VM_$ = "netVmAmount";
	    public static final String NET_VM_PCT = "netVmPct";
	    public static final String NET_VM_$_BP = "netVmAmountBp";
	    public static final String NET_VM_PCT_BP = "netVmPctBp";
	    public static final String BENCH_MARK_PER_EACH = "benchMarkPerUnit";
	    public static final String TOTAL_BENCH_MARK = "totalBenchMark";
	    public static final String LIST_PRICE_PER_EA = "invoiceUnitPrice";
	    public static final String TOTAL_LIST_PRICE = "totalInvoiceUnitPrice";
	    public static final String PE_RECALC_DATE = "peReCalcDate";
	    public static final String EXCESS_INVT_DOLLAR = "excessInvtDollar";
	    public static final String EXCESS_INVT_PCT = "excessInvtPct";
	    public static final String OVERRIDE_EXC_PCT = "overrideExcPct";
	    public static final String INPOG_YN="inPogYn";
	    public static final String INVOICE_WITH_PROGRAM="invoiceWithProgram";
	    public static final String ESTIMATED_PROGRAM_PCT="estimatedProgramPct";
	   
	    
	    
	    public CostAnalysisKitComponentDTO() {
	    	
	    }

	    public CostAnalysisKitComponentDTO(String buDesc, String buCode, double invoiceUnitPrice, double netVmPct, double benchMarkPerUnit) {
	    	set(BU_DESC, buDesc);
	    	set(BU_CODE,buCode);
	    	set(LIST_PRICE_PER_EA, invoiceUnitPrice);
	    	set(NET_VM_PCT, netVmPct);
	    	set(BENCH_MARK_PER_EACH, benchMarkPerUnit);
	    }

	public CostAnalysisKitComponentDTO(Long kitComponentId,Long displayScenarioId,
	String  sku, String  chkDgt, String buDesc, String buCode,	String productName, Long regCasePack,
	Boolean sourcePimFlg, Boolean promoFlg, Boolean i2ForecastFlg, 	Boolean plantBreakCaseFlg,
	String pbcVersion, Long qtyPerFacing, 	Long numberFacings, Long qtyPerDisplay,
	Long inventoryReq, Double priceException, Double actualInvoicePerEa, Double totalActualInvoice, Double totalActualInvoiceWpgrm,
	Double cogsUnitPrice, Double totalCogsUnitPrice, Double netVmAmount, Double netVmPct,
	Double benchMarkPerUnit, Double totalBenchMark, Double invoiceUnitPrice, 
	Double totalInvoiceUnitPrice,Double excessInvtDollar, Double excessInvtPct, Double overrideExcPct, Double overrideExceptionPct,Double invoiceWithProgram, Double estimatedProgramPct,  Boolean inPogYn
	,Double netVmAmountBp, Double netVmPctBp )
	{
		set(KIT_COMPONENT_ID , kitComponentId);
		set(DISPLAY_SCENARIO_ID , displayScenarioId);
		set(SKU , sku);
		set(CHKDGT , chkDgt);
		set(BU_DESC , buDesc);
		set(BU_CODE , buCode);
		set(PRODUCT_NAME , productName);
		set(REG_CASE_PACK , regCasePack);
		set(SOURCE_PIM_FLG , sourcePimFlg);
		set(PROMO_FLG , promoFlg);
		set(I2_FORECAST_FLG , i2ForecastFlg);
		set(PLANT_BREAK_CASE_FLG , plantBreakCaseFlg);
		set(PBC_VERSION , pbcVersion);
		set(QTY_PER_FACING , qtyPerFacing);
		set(NUMBER_FACINGS , numberFacings);
		set(QTY_PER_DISPLAY , qtyPerDisplay);
		set(INVENTORY_REQUIREMENT , inventoryReq);
		set(PRICE_EXCEPTION , priceException);
		set(ACTUAL_INVOICE_PER_EA , actualInvoicePerEa);
		set(TOTAL_ACTUAL_INVOICE , totalActualInvoice);
		set(TOTAL_ACTUAL_INVOICE_WPGRM , totalActualInvoiceWpgrm);
		set(COGS_UNIT_PRICE , cogsUnitPrice);
		set(TOTAL_COGS_UNIT_PRICE , totalCogsUnitPrice);
		set(NET_VM_$ , netVmAmount);
		set(NET_VM_PCT , netVmPct);
		set(BENCH_MARK_PER_EACH , benchMarkPerUnit);
		set(TOTAL_BENCH_MARK , totalBenchMark);
		set(LIST_PRICE_PER_EA , invoiceUnitPrice);
		set(TOTAL_LIST_PRICE , totalInvoiceUnitPrice);
		set(EXCESS_INVT_PCT,excessInvtPct);
		set(EXCESS_INVT_DOLLAR,excessInvtDollar);
		set(INPOG_YN,inPogYn);
		set(OVERRIDE_EXC_PCT, overrideExcPct);
		set(INVOICE_WITH_PROGRAM,invoiceWithProgram);
		set(ESTIMATED_PROGRAM_PCT,estimatedProgramPct);
		set(NET_VM_$_BP , netVmAmountBp);
		set(NET_VM_PCT_BP , netVmPctBp);
	}

	public Double getEstimatedProgramPct() {
		return get(ESTIMATED_PROGRAM_PCT);
	}
	
	public void setEstimatedProgramPct(Double estimatedProgramPct) {
		set(ESTIMATED_PROGRAM_PCT,estimatedProgramPct);
	}
	
	public Double getInvoiceWithProgram() {
		return get(INVOICE_WITH_PROGRAM);
	}
	public void setInvoiceWithProgram(Double invoiceWithProgram) {
		set(INVOICE_WITH_PROGRAM,invoiceWithProgram);
	}
	
	public Double getExcessInvtDollar() {
		return get(EXCESS_INVT_DOLLAR);
	}
	public void setExcessInvtDollar(Double excessInvtDollar) {
		set(EXCESS_INVT_DOLLAR,excessInvtDollar);
	}
	
	public Double getExcessInvtPct() {
		return get(EXCESS_INVT_PCT);
	}
	public void setExcessInvtPct(Double excessInvtPct) {
		set(EXCESS_INVT_PCT,excessInvtPct);
	}
	public Double getOverrideExcPct() {
		return get(OVERRIDE_EXC_PCT);
	}
	public void setOverrideExcPct(Double overrideExcPct) {
		set(OVERRIDE_EXC_PCT,overrideExcPct);
	}
	public Boolean getInPogYn() {
		return get(INPOG_YN);
	}
	public void setInPogYn(Boolean inPogYn) {
		set(INPOG_YN,inPogYn);
	}

	public Long getDisplayId() {
		return get(DISPLAY_ID);
	}

	public void setDisplayId(Long displayId) {
		set(DISPLAY_ID, displayId);
	}
	
	public Long getSequenceNum() {
		return get(SEQUENCE_NUM);
	}

	public void setSequenceNum(Long displayId) {
		set(SEQUENCE_NUM, displayId);
	}
	
	public Long getKitComponentId() {
		return get(KIT_COMPONENT_ID);
	}

	public void setKitComponentId(Long kitComponentId) {
		set(KIT_COMPONENT_ID, kitComponentId);
	}

	public Long getDisplayScenarioId() {
		return get(DISPLAY_SCENARIO_ID);
	}

	public void setDisplayScenarioId(Long displayScenarioId) {
		set(DISPLAY_SCENARIO_ID, displayScenarioId);
	}

	public String getSku() {
		return get(SKU);
	}


	public void setSku(String sku) {
		set(SKU, sku);
	}


	public String getChkDgt() {
		return get(CHKDGT);
	}


	public void setChkDgt(String chkDgt) {
		set(CHKDGT,chkDgt);
	}


	public String getBuDesc() {
		return get(BU_DESC );
	}


	public void setBuDesc(String buDesc) {
		set(BU_DESC ,buDesc);
	}


	public String getBuCode() {
		return get(BU_CODE );
	}


	public void setBuCode(String buCode) {
		set(BU_CODE ,buCode);
	}
	
	public String getProductName() {
		return get(PRODUCT_NAME);
	}


	public void setProductName(String productName) {
		set(PRODUCT_NAME ,productName);
	}


	public Long getRegCasePack() {
		return get(REG_CASE_PACK);
	}


	public void setRegCasePack(Long regCasePack) {
		set(REG_CASE_PACK ,regCasePack);
	}


	public Boolean getSourcePimFlg() {
		return get(SOURCE_PIM_FLG);
	}


	public void setSourcePimFlg(Boolean sourcePimFlg) {
		set(SOURCE_PIM_FLG, sourcePimFlg);
	}


	public Boolean getPromoFlg() {
		return get(PROMO_FLG);
	}


	public void setPromoFlg(Boolean promoFlg) {
		set(PROMO_FLG , promoFlg);
	}


	public Boolean getI2ForecastFlg() {
		return get(I2_FORECAST_FLG);
	}


	public void setI2ForecastFlg(Boolean i2ForecastFlg) {
		set(I2_FORECAST_FLG,i2ForecastFlg);
	}


	public Boolean getPlantBreakCaseFlg() {
		return get(PLANT_BREAK_CASE_FLG);
	}


	public void setPlantBreakCaseFlg(Boolean plantBreakCaseFlg) {
		set(PLANT_BREAK_CASE_FLG ,plantBreakCaseFlg);
	}


	public String getPbcVersion() {
		return get(PBC_VERSION);
	}


	public void setPbcVersion(String pbcVersion) {
		set(PBC_VERSION ,pbcVersion);
	}


	public Long getQtyPerFacing() {
		return get(QTY_PER_FACING);
	}


	public void setQtyPerFacing(Long qtyPerFacing) {
		set(QTY_PER_FACING, qtyPerFacing);
	}


	public Long getNumberFacings() {
		return get(NUMBER_FACINGS);
	}


	public void setNumberFacings(Long numberFacings) {
		set(NUMBER_FACINGS ,numberFacings);
	}


	public Long getQtyPerDisplay() {
		return get(QTY_PER_DISPLAY);
	}


	public void setQtyPerDisplay(Long qtyPerDisplay) {
		set(QTY_PER_DISPLAY, qtyPerDisplay);
	}


	public Long getInventoryReq() {
		return get(INVENTORY_REQUIREMENT);
	}


	public void setInventoryReq(Long inventoryReq) {
		set(INVENTORY_REQUIREMENT, inventoryReq);
	}


	public Double getPriceException() {
		return get(PRICE_EXCEPTION);
	}


	public void setPriceException(Double priceException) {
		set(PRICE_EXCEPTION,priceException);
	}


	public Double getActualInvoicePerEa() {
		return get(ACTUAL_INVOICE_PER_EA);
	}


	public void setActualInvoicePerEa(Double actualInvoicePerEa) {
		set(ACTUAL_INVOICE_PER_EA,actualInvoicePerEa);
	}


	public Double getTotalActualInvoice() {
		return get(TOTAL_ACTUAL_INVOICE);
	}


	public void setTotalActualInvoice(Double totalActualInvoice) {
		set(TOTAL_ACTUAL_INVOICE, totalActualInvoice);
	}

	public Double getTotalActualInvoiceWpgrm() {
		return get(TOTAL_ACTUAL_INVOICE_WPGRM);
	}


	public void setTotalActualInvoiceWpgrm(Double totalActualInvoiceWpgrm) {
		set(TOTAL_ACTUAL_INVOICE_WPGRM, totalActualInvoiceWpgrm);
	}

	public Double getCogsUnitPrice() {
		return get(COGS_UNIT_PRICE);
	}


	public void setCogsUnitPrice(Double cogsUnitPrice) {
		set(COGS_UNIT_PRICE, cogsUnitPrice);
	}


	public Double getTotalCogsUnitPrice() {
		return get(TOTAL_COGS_UNIT_PRICE);
	}


	public void setTotalCogsUnitPrice(Double totalCogsUnitPrice) {
		set(TOTAL_COGS_UNIT_PRICE,totalCogsUnitPrice);
	}


	public Double getNetVmAmount() {
		return get(NET_VM_$);
	}


	public void setNetVmAmount(Double netVmAmount) {
		set(NET_VM_$, netVmAmount);
	}


	public Double getNetVmPct() {
		return get(NET_VM_PCT);
	}


	public void setNetVmPct(Double netVmPct) {
		set(NET_VM_PCT,netVmPct);
	}

	
	public Double getNetVmAmountBp() {
		return get(NET_VM_$_BP);
	}


	public void setNetVmAmountBp(Double netVmAmountBp) {
		set(NET_VM_$_BP, netVmAmountBp);
	}


	public Double getNetVmPctBp() {
		return get(NET_VM_PCT_BP);
	}

	public void setNetVmPctBp(Double netVmPctBp) {
		set(NET_VM_PCT_BP,netVmPctBp);
	}
	

	public Double getBenchMarkPerUnit() {
		return get(BENCH_MARK_PER_EACH);
	}


	public void setBenchMarkPerUnit(Double benchMarkPerUnit) {
		set(BENCH_MARK_PER_EACH,benchMarkPerUnit);
	}


	public Double getTotalBenchMark() {
		return get(TOTAL_BENCH_MARK);
	}


	public void setTotalBenchMark(Double totalBenchMark) {
		set(TOTAL_BENCH_MARK,totalBenchMark);
	}


	public Double getInvoiceUnitPrice() {
		return get(LIST_PRICE_PER_EA);
	}


	public void setInvoiceUnitPrice(Double invoiceUnitPrice) {
		set(LIST_PRICE_PER_EA,invoiceUnitPrice);
	}


	public Double getTotalInvoiceUnitPrice() {
		return get(TOTAL_LIST_PRICE);
	}


	public void setTotalInvoiceUnitPrice(Double totalInvoiceUnitPrice) {
		set(TOTAL_LIST_PRICE, totalInvoiceUnitPrice);
	}

	public Date getPeReCalcDate() {
		return get(PE_RECALC_DATE);
	}

	public void setPeReCalcDate(Date peReCalcDate) {
		set(PE_RECALC_DATE, peReCalcDate);
	}
	
	@Override
	public void addChangeListener(ChangeListener... listener) {
		super.addChangeListener(listener);
	}

	@Override
	public void addChangeListener(List<ChangeListener> listeners) {
		super.addChangeListener(listeners);
	}

	@Override
	protected void fireEvent(int type, Model item) {
		super.fireEvent(type, item);
	}

	@Override
	protected void fireEvent(int type) {
		super.fireEvent(type);
	}

	@Override
	public void notify(ChangeEvent evt) {
		super.notify(evt);
	}

	@Override
	protected void notifyPropertyChanged(String name, Object value,
			Object oldValue) {
		super.notifyPropertyChanged(name, value, oldValue);
	}

	@Override
	public void removeChangeListener(ChangeListener... listener) {
		super.removeChangeListener(listener);
	}

	@Override
	public void removeChangeListeners() {
		super.removeChangeListeners();
	}

	 public void updateCalculatedFields(Long actualQty, Double customerProgramPct) {
	 try{
	    	Long numberFacings = this.get(NUMBER_FACINGS);
	    	if(numberFacings==null) numberFacings=new Long(0);
	    	Long qtyPerFacing =  get(QTY_PER_FACING);
	    	if(qtyPerFacing==null) qtyPerFacing=new Long(0);
	    	Long qtyPerDisplay;
	    	Long inventoryRequirement;
	    	Double hundredPct=new Double(100.0);
	    	if (numberFacings != null && qtyPerFacing != null) {
	    		qtyPerDisplay = numberFacings * qtyPerFacing;
	    	}else{
	    		qtyPerDisplay = new Long(0);
	    	}
	    	setQtyPerDisplay(qtyPerDisplay); //Calculated : Quantity Per Display
	    	if (qtyPerDisplay != null && actualQty != null) {
	    		inventoryRequirement = qtyPerDisplay * actualQty;
	    	}
	    	else inventoryRequirement = new Long(0);
	    	setInventoryReq(inventoryRequirement); //Calculated : Inventory Requirement
	    	setChkDgt(getCheckSKUDigit(get(SKU).toString()));
	    	Double priceException = get(PRICE_EXCEPTION);
	    	if(priceException==null) priceException=new Double(0.0);
	    	Double listPricePerEach =  get(LIST_PRICE_PER_EA);
	    	if(listPricePerEach==null) listPricePerEach= new Double(0.0);
	    	Double cogsUnitPrice= get(COGS_UNIT_PRICE);
	    	if(cogsUnitPrice==null) cogsUnitPrice= new Double(0.0);
	    	Double benchMarkPerEach = get(BENCH_MARK_PER_EACH);
	    	if(benchMarkPerEach==null) benchMarkPerEach=new Double(0.0);
	    	Double totBenchMarkPerEach = benchMarkPerEach*qtyPerDisplay;
	    	setTotalBenchMark(roundToDecimals(totBenchMarkPerEach,2));
	    	Double actualInvoicePrice = calcLowestPrice(priceException,benchMarkPerEach);
	    	setActualInvoicePerEa(actualInvoicePrice);
	    	Double totalActualInvoice=actualInvoicePrice * qtyPerDisplay;
	    	setTotalActualInvoice(roundToDecimals(totalActualInvoice,2));
	    	
	    	Double totalCogsUnitPrice=cogsUnitPrice *  qtyPerDisplay;
	    	setTotalCogsUnitPrice(roundToDecimals(totalCogsUnitPrice,2));
	    	Double invoiceUnitPrice = get(LIST_PRICE_PER_EA);
	    	if(invoiceUnitPrice==null) invoiceUnitPrice=new Double(0.0);
	    	Double totInvoiceUnitPrice = invoiceUnitPrice*qtyPerDisplay;
	    	setTotalInvoiceUnitPrice(roundToDecimals(totInvoiceUnitPrice,2));
	    	
	    	Double excessInvtVal=get(EXCESS_INVT_PCT);
	    	if(excessInvtVal==null) excessInvtVal=new Double(0.0);
	    	Double excessInvtDollarVal=(((hundredPct-excessInvtVal)/hundredPct)*totalActualInvoice);
	    	setExcessInvtDollar(roundToDecimals(excessInvtDollarVal,2));
	    	Double overrideExceptionPctVal=get(OVERRIDE_EXC_PCT);
	    	if (overrideExceptionPctVal==null) overrideExceptionPctVal=0.00;
	    	Double estimatedProgramPctVal=get(ESTIMATED_PROGRAM_PCT);
	    	if (estimatedProgramPctVal==null) estimatedProgramPctVal=0.00;
	    	setEstimatedProgramPct(estimatedProgramPctVal);
	    	Double exceptionPctVal=new Double(0.0);
	    	if(overrideExceptionPctVal==null) overrideExceptionPctVal=new Double(0.0);
	    	if(overrideExceptionPctVal > new Double(0.0)){
	    		exceptionPctVal=get(OVERRIDE_EXC_PCT);
	    	}else{
	    		exceptionPctVal=estimatedProgramPctVal;
	    	}
	    	if (exceptionPctVal==null) exceptionPctVal=0.00;
	    	
	    	Double netVMAmountBp = totalActualInvoice - totalCogsUnitPrice;
	    	setNetVmAmountBp(roundToDecimals(netVMAmountBp,2));
	    	Double netVMPctBp= (netVMAmountBp / totalActualInvoice)*hundredPct;
	    	setNetVmPctBp(roundToDecimals(netVMPctBp,2));
	    	/*
	    			Code changed by Mari
	    			Date: 10-02-2012
	    			Description: Apply Program discount Estd. Program Disocunt or Override Exception to NetVmAmount and NetCmPct
	    			Replaced discPct with exceptionPctVal
	    			
	    	*/
	    	
	    	Double discPct=((hundredPct-exceptionPctVal)/hundredPct);
	    	Double netVMAmount =( totalActualInvoice * discPct)-totalCogsUnitPrice;
	    	setNetVmAmount(roundToDecimals(netVMAmount,2));
	    	Double netVMPct= (netVMAmount /( totalActualInvoice * discPct))*hundredPct;
	    	setNetVmPct(roundToDecimals(netVMPct,2));
	    	
	    	Double invoiceWithProgramVal=(((hundredPct-exceptionPctVal)/hundredPct)*actualInvoicePrice);
	    	setInvoiceWithProgram(roundToDecimals(invoiceWithProgramVal,2));
	    	
	    	Double totalActualInvoiceWpgrm=getInvoiceWithProgram() * qtyPerDisplay;
	    	setTotalActualInvoiceWpgrm(roundToDecimals(totalActualInvoiceWpgrm,2));
	    	
	    	Boolean inPogYnVal=get(INPOG_YN);
	    	if(inPogYnVal!=null){
	    		setInPogYn(inPogYnVal?Boolean.TRUE:Boolean.FALSE);
	    	}else{
	    		setInPogYn(Boolean.FALSE);
	    	}
		 }catch(Exception ex){
			 System.out.println("CostAnalysisKitComponentDTO:updateCalculatedFields Exception : "+ex.toString());
		 }
	 }
	 
	 public double calcLowestPrice(Double firstVal,Double secondVal) {
		 double actualInvoicePrice=0.00;
		 double zaroVal=0.00; 
		 firstVal=(firstVal==null)?0.00:firstVal;
		 secondVal=(secondVal==null)?0.00:secondVal;
		 if(firstVal.equals(zaroVal) && !(secondVal.equals(zaroVal)) ){
			 actualInvoicePrice=secondVal;
		 }else  if(secondVal.equals(zaroVal) && !(firstVal.equals(zaroVal)) ){
			 actualInvoicePrice=firstVal;
		 }else if(firstVal<secondVal){
			 actualInvoicePrice=firstVal;
		 }else{
			 actualInvoicePrice=secondVal;
		 }
		// setActualInvoicePerEa(actualInvoicePrice);
		 return actualInvoicePrice;
	 }
	 
	 public String getCheckSKUDigit(String sku) {
	        if (sku == null) return "";
	        //Check for leading zero.  If no leading zero add it.
	        if (!sku.startsWith("0")) {
	            sku = "0" + sku;
	        }
	        char[] ca = sku.toCharArray();
	        int odds = 0;
	        int evens = 0;
	        for (int i = 0; i <= 10; i++) {
	            int digit = Character.digit(ca[i], 10);
	            if (i % 2 == 0) {
	                odds += digit;
	            } else {
	                evens += digit;
	            }
	        }
	        int diff = (odds * 3) + evens;
	        String sd = String.valueOf(diff);
	        char[] cdiffs = sd.toCharArray();
	        int lastDigitOfDiff = Character.digit(cdiffs[cdiffs.length - 1], 10);

	        int checkDigit;
	        if (lastDigitOfDiff == 0) {
	            checkDigit = lastDigitOfDiff;
	        } else {
	            checkDigit = 10 - lastDigitOfDiff;
	        }
	       return Integer.toString(checkDigit);
	    }

	 		public static Double roundToDecimals(Double d, int c) {
	 			Double temp=(Double)((d*Math.pow(10,c)));
	 			temp= (Math.round(temp)/Math.pow(10,c));
	 			return temp;
	    	}
	 
	 
}
