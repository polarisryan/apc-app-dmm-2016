
package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.Model;
import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * User: Mari
 * Date: Dec 17, 2010
 * Time: 01:27:56 AM
 */
public class CorrugateDTO extends BaseModel {
    public static final String CORRUGATE_ID = "corrugateId";
    public static final String CREATE_DATE = "createDate";
  /*  public static final String CORRUGATE_DUE_DATE = "corrugateDueDate";
    public static final String DELIVERY_DATE = "actualDeliveryDate";*/
    public static final String CORRUGATE_COST = "corrugateCost";
    public static final String DISPLAY_ID="displayId";
    
    @Override
	public String toString() {
		/*return "CorrugateDTO [actualDeliveryDate=" + get(DELIVERY_DATE)
				+ ", corrugateCost=" + get(CORRUGATE_COST)
				+ ", displayId=" + get(DISPLAY_ID)
				+ ", corrugateDueDate=" + get(CORRUGATE_DUE_DATE) + ", corrugateId="
				+ get(CORRUGATE_ID) + ", createDate=" + get(CREATE_DATE) + "]";*/
    	return "CorrugateDTO [" 
		+ " corrugateCost=" + get(CORRUGATE_COST)
		+ ", displayId=" + get(DISPLAY_ID)
	    + ", corrugateId="
		+ get(CORRUGATE_ID) + ", createDate=" + get(CREATE_DATE) + "]";
	}

	public CorrugateDTO() {
    }

    public CorrugateDTO(Long corrugateId,Long displayId,  Date createDate, Date corrugateDueDate, Date actualDeliveryDate, Float corrugateCost ) {
        set(CORRUGATE_ID, corrugateId);
        set(CREATE_DATE, createDate);
     /*   set(CORRUGATE_DUE_DATE, corrugateDueDate);
        set(DELIVERY_DATE, actualDeliveryDate);*/
        set(CORRUGATE_COST, corrugateCost);
        set(DISPLAY_ID, displayId);
    }
    
    public Long getCorrugateId() {
		return get(CORRUGATE_ID);
	}

	public void setCorrugateId(Long corrugateId) {
		set(CORRUGATE_ID, corrugateId);
	}

	public Date getCreateDate() {
		return get(CREATE_DATE);
	}

	public void setCreateDate(Date createDate) {
		 set(CREATE_DATE, createDate);
	}

	/*public Date getCorrugateDueDate() {
		return  get(CORRUGATE_DUE_DATE);
	}

	public void setCorrugateDueDate(Date corrugateDueDate) {
		 set(CORRUGATE_DUE_DATE, corrugateDueDate);
	}

	public Date getDeliveryDate() {
		return  get(DELIVERY_DATE);
	}

	public void setDeliveryDate(Date deliveryDate) {
		 set(DELIVERY_DATE, deliveryDate);
	}*/
	
	public Float getCorrugateCost() {
		return  get(CORRUGATE_COST);
	}

	public void setCorrugateCost(Float corrugateCoste) {
		 set(CORRUGATE_COST, corrugateCoste);
	}
	
	public Long getDisplayId() {
		return get(DISPLAY_ID);
	}
	public void setDisplayId(Long displayId) {
		set(DISPLAY_ID,displayId);
	}
	
 	@Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    public void notify(ChangeEvent evt) {
        super.notify(evt);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value, Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }
    
    public boolean equalsCorrugateInfo(CorrugateDTO controlModel) {
		System.out.println("CorrugateDTO:equalsCorrugateInfo ..starts.. ");
		 DateTimeFormat fmt = DateTimeFormat.getFormat("MM/dd/yyyy");
       System.out.println("CorrugateDTO:equalsCorrugateInfo ..[getCreateDate()=.. "+getCreateDate() + "]");
        if (getCreateDate() == null) {
            if (controlModel.getCreateDate() != null)
                return false;
        } else if (controlModel.getCreateDate()==null){
        	return false;   
        } else if (!fmt.format(getCreateDate()).equals(fmt.format(controlModel.getCreateDate()))){
        	System.out.println("CorrugateDTO:equalsCorrugateInfo ..getCreateDate().NOT EQUALS.[ " + getCreateDate() + "," + controlModel.getCreateDate() +"]" );
            return false;
        }else{
        	System.out.println("CorrugateDTO:equalsCorrugateInfo ..getCreateDate().EQUALS.[ " + getCreateDate() + "," + controlModel.getCreateDate() +"]" );
        }
        /* System.out.println("CorrugateDTO:equalsCorrugateInfo ..1..[getCorrugateDueDate()=.. "+getCorrugateDueDate() + "]");
        if (getCorrugateDueDate() == null) {
            if (controlModel.getCorrugateDueDate() != null)
                return false;
        } else if (controlModel.getCorrugateDueDate()==null){
        	return false;   
        } else if (!fmt.format(getCorrugateDueDate()).equals(fmt.format(controlModel.getCorrugateDueDate()))){
        	System.out.println("CorrugateDTO:equalsCorrugateInfo ..getCreateDate().NOT EQUALS.[ " + getCorrugateDueDate() + "," + controlModel.getCorrugateDueDate() +"]" );
            return false;
        }else{
        	System.out.println("CorrugateDTO:equalsCorrugateInfo ..getCreateDate(). EQUALS.[ " + getCorrugateDueDate() + "," + controlModel.getCorrugateDueDate() +"]" );
        }
        System.out.println("CorrugateDTO:equalsCorrugateInfo ..getCorrugateDueDate()..2..[ " + getCorrugateDueDate() + "," + controlModel.getCorrugateDueDate() +"]" );
	    System.out.println("CorrugateDTO:equalsCorrugateInfo ..3..[getDeliveryDate()=.. "+getDeliveryDate() + "]");
	    */
        /*if (getDeliveryDate() == null) {
            if (controlModel.getDeliveryDate() != null)
                return false;
        } else if (controlModel.getDeliveryDate()==null){
        	return false;   
        } else if (!fmt.format(getDeliveryDate()).equals(fmt.format(controlModel.getDeliveryDate()))){
        	System.out.println("CorrugateDTO:equalsCorrugateInfo ..getCreateDate().NOT EQUALS.[ " + getDeliveryDate() + "," + controlModel.getDeliveryDate() +"]" );
            return false;
        }else{
        	System.out.println("CorrugateDTO:equalsCorrugateInfo ..getCreateDate(). EQUALS.[ " + getDeliveryDate() + "," + controlModel.getDeliveryDate() +"]" );
        }
	    System.out.println("CorrugateDTO:equalsCorrugateInfo ..getDeliveryDate()..[ " + getDeliveryDate() + "," + controlModel.getDeliveryDate() +"]" );
	    System.out.println("CorrugateDTO:equalsCorrugateInfo ..4..[getCorrugateCost()=.. "+getCorrugateCost() + "]");
	    */
	    if (getCorrugateCost() == null) {
            if (controlModel.getCorrugateCost() != null)
                return false;
        } else if (!getCorrugateCost().equals(controlModel.getCorrugateCost())){
        	System.out.println("CorrugateDTO:equalsCorrugateInfo ..getCorrugateCost().NOT EQUALS.[ " + getCorrugateCost() + "," + controlModel.getCorrugateCost() +"]" );
            return false;   
        }else{
        	System.out.println("CorrugateDTO:equalsCorrugateInfo ..getCorrugateCost(). EQUALS.[ " + getCorrugateCost() + "," + controlModel.getCorrugateCost() +"]" );
        }
	    System.out.println("CorrugateDTO:equalsCorrugateInfo ..ends.. ");
        return true;
    }
  }
