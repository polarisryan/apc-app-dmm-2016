package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Nov 6, 2009
 * Time: 8:22:27 AM
 */
public class DisplaySalesRepDTO extends BaseModel {
	private static final long serialVersionUID = -2429770719407404839L;
	public static final String DISPLAY_SALES_REP_ID = "displaySalesRepId";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String FULL_NAME = "fullName";
    public static final String REP_CODE = "repCode";
	public static final String COUNTRY_NAME = "countryName";
	public static final String COUNTRY_ID = "countryId";
	public static final String USER_TYPE_NAME = "userTypeName";
	public static final String USER_TYPE_ID = "userTypeId";
    public static String ACTIVE_FLG = "activeFlg";

    public DisplaySalesRepDTO() {
    }

    public DisplaySalesRepDTO(Long displaySalesRepId, String firstName, String lastName, String repCode) {
        set(DISPLAY_SALES_REP_ID, displaySalesRepId);
        set(FIRST_NAME, firstName);
        set(LAST_NAME, lastName);
        set(REP_CODE, repCode);
        set(FULL_NAME, firstName + " " + lastName);
    }

    public Long getDisplaySalesRepId() {
        return get(DISPLAY_SALES_REP_ID);
    }

    public void setDisplaySalesRepId(Long displaySalesRepId) {
        set(DISPLAY_SALES_REP_ID, displaySalesRepId);
    }


    public String getFirstName() {
        return get(FIRST_NAME);
    }

    public void setFirstName(String firstName) {
        set(FIRST_NAME, firstName);
    }


    public String getLastName() {
        return get(LAST_NAME);
    }

    public void setLastName(String lastName) {
        set(LAST_NAME, lastName);
    }

    public String getRepCode() {
        return get(REP_CODE);
    }

    public void setRepCode(String repCode) {
        set(REP_CODE, repCode);
    }

    public void setFullName(String fullname) {
        set(FULL_NAME, fullname);
    }

    public String getFullName() {
        return get(FULL_NAME);
    }
    
    public Boolean isActiveFlg() {
        return get(ACTIVE_FLG);
    }

    public void setActiveFlg(Boolean activeFlg) {
        set(ACTIVE_FLG, activeFlg);
    }
    
    public String getUserTypeName() {
        return get(USER_TYPE_NAME);
    }

    public void setUserTypeName(String userTypeName) {
        set(USER_TYPE_NAME, userTypeName);
    }
    
    public Long getUserTypeId() {
        return get(USER_TYPE_ID);
    }

    public void setUserTypeId(Long userTypeId) {
        set(USER_TYPE_ID, userTypeId);
    }
    
    public String getCountryName() {
        return get(COUNTRY_NAME);
    }

    public void setCountryName(String countryName) {
        set(COUNTRY_NAME, countryName);
    }
    
    public Long getCountryId() {
        return get(COUNTRY_ID);
    }

    public void setCountryId(Long countryId) {
        set(COUNTRY_ID, countryId);
    }    
    
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		DisplaySalesRepDTO object = (DisplaySalesRepDTO) o;

		if (getDisplaySalesRepId() != null ? !getDisplaySalesRepId().equals(
				object.getDisplaySalesRepId())
				: object.getDisplaySalesRepId() != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = getDisplaySalesRepId() != null ? getDisplaySalesRepId()
				.hashCode() : 0;
		return result;
	}

}
