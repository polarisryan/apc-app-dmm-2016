package com.averydennison.dmm.app.client.models;

import java.io.Serializable;

/**
 * User: Spart Arguello
 * Date: Oct 16, 2009
 * Time: 8:33:11 AM
 */
public class UserDTO implements Serializable {
    private Long id;
    private String username;
    private String password;
    private boolean isLoggedIn;
    private String[] authorities;
    private String[] permissions;
    
    public String[] getPermissions() {
		return permissions;
	}

	public void setPermissions(String[] permissions) {
		this.permissions = permissions;
	}

	public UserDTO() {
    }

    public String[] getAuthorities() {
		return authorities;
	}

	public void setAuthorities(String[] authorities) {
		this.authorities = authorities;
	}

	public UserDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserDTO(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }
    public String toString(){
    	String out = "";
    	out += "UserDTO Object\n";
    	out += "username: " + username + "\n";
    	out += "password: " + password + "\n";
    	for(int i=0;i<authorities.length;i++){
    		out += "Auth: " + authorities[i] + "\n";
    	}
    	for(int l=0;l<permissions.length;l++){
    		out += "Perm: " + permissions[l] + "\n";
    	}
    	return out;
    }
}