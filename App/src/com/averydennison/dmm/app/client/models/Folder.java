package com.averydennison.dmm.app.client.models;

import java.io.Serializable;

import com.extjs.gxt.ui.client.data.BaseTreeModel;

public class Folder extends BaseTreeModel implements Serializable {

	private static final long serialVersionUID = 3701565886459435329L;
	public static final String ID_KEY = "id";
	public static final String ROW_ID = "rowId";
	public static final String DC_CODE = "dcCode";
	public static final String PO_QUANTITY = "poQuantity";
	public static final String LOCATION_ID = "locationId";
	public static final String PO_QTY_BALANCE = "poQtyBalance";
	
	private static int ID = 0;

	
	public Folder(String name, Long qty, String rowId, Long locationId) {
		set(ID_KEY, ID++);
		set(ROW_ID, rowId);
		set(DC_CODE, name);
		set(PO_QUANTITY, qty);
		set(LOCATION_ID, locationId);
	}
	
	public Folder(String name, Long qty, String rowId, Long locationId, BaseTreeModel[] children) {
		this(name, qty, rowId, locationId);
		for (int i = 0; i < children.length; i++) {
			add(children[i]);
		}
	}
	
	public Integer getId() {
		return (Integer)get(ID_KEY);
	}

	public void setRowId(String rowId) {
		set(ROW_ID, rowId);
	}

	public String getRowId() {
		return (String)get(ROW_ID);
	}

	public String getDcCode() {
		return (String)get(DC_CODE);
	}

	public void setDcCode(String dcCode) {
		set(DC_CODE, dcCode);
	}

	// populate with DC Qty, but use PO Qty field so taht it appears in PO Qty column
	public Long getPoQuantity() {
		return (Long)get(PO_QUANTITY);
	}

	public void setPoQuantity(Long poQuantity) {
		set(PO_QUANTITY, poQuantity);
	}

	public Long getLocationId() {
		return (Long)get(LOCATION_ID);
	}

	public void setLocationid(Long locationId) {
		set(LOCATION_ID, locationId);
	}

	public Long getPoBalanceQty() {
		return (Long)get(PO_QTY_BALANCE);
	}

	public void setPoBalanceQty(Long poQty) {
		set(PO_QTY_BALANCE, poQty);
	}

	public String toString() {
		return getDcCode() + "," + getPoQuantity() + "," + getLocationId();
	}
}
