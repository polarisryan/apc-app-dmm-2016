package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Nov 5, 2009
 * Time: 3:09:37 PM
 */
public class StructureDTO extends BaseModel {
	private static final long serialVersionUID = 5313325889673455222L;
	public static String STRUCTURE_ID = "structureId";
    public static String STRUCTURE_NAME = "structureName";
    public static String STRUCTURE_DESC = "structureDesc";
    public static String ACTIVE_FLG = "activeFlg";

    public StructureDTO() {
    }

    public StructureDTO(Long structureId, String structureName, String structureDesc) {
        set(STRUCTURE_ID, structureId);
        set(STRUCTURE_NAME, structureName);
        set(STRUCTURE_DESC, structureDesc);
    }

    public Long getStructureId() {
        return get(STRUCTURE_ID);
    }

    public void setStructureId(Long structureId) {
        set(STRUCTURE_ID, structureId);
    }

    public String getStructureName() {
        return get(STRUCTURE_NAME);
    }

    public void setStructureName(String structureName) {
        set(STRUCTURE_NAME, structureName);
    }

    public String getStructureDesc() {
        return get(STRUCTURE_DESC);
    }

    public void setStructureDesc(String structureDesc) {
        set(STRUCTURE_DESC, structureDesc);
    }
    
    public Boolean isActiveFlg() {
        return get(ACTIVE_FLG);
    }

    public void setActiveFlg(Boolean activeFlg) {
        set(ACTIVE_FLG, activeFlg);
    }
    
    @Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		StructureDTO object = (StructureDTO) o;

		if (getStructureId() != null ? !getStructureId().equals(
				object.getStructureId())
				: object.getStructureId() != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = getStructureId() != null ? getStructureId()
				.hashCode() : 0;
		return result;
	}
}
