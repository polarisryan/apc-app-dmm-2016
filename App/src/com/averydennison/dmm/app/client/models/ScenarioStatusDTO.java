package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;


/**
 * User: Mari
 * Date: Oct 25, 2010
 * Time: 11:54:37 AM
 */
public class ScenarioStatusDTO extends BaseModel {
    private static final String  SCENARIO_STATUS_ID="scenarioStatusId";
	private static final String  NAME="name";
	private static final String  DESCRIPTION="description";
	private static final String  ACTIVE_FLG="activeFlg";
	 
	 public ScenarioStatusDTO(){
		 
	 }
	 
    public ScenarioStatusDTO(Long statusId, String name, String description, Boolean activeFlg) {
        set(SCENARIO_STATUS_ID, statusId);
        set(NAME, name);
        set(DESCRIPTION, description);
        set(ACTIVE_FLG, activeFlg);
    }

    public Long getScenarioStatusId() {
        return get(SCENARIO_STATUS_ID);
    }

    public void setScenarioStatusId(Long scenarioStatusId) {
        set(SCENARIO_STATUS_ID, scenarioStatusId);
    }

    public String getName() {
        return get(NAME);
    }

    public void setName(String name) {
        set(NAME, name);
    }

    public String getDescription() {
        return get(DESCRIPTION);
    }

    public void setDescription(String description) {
        set(DESCRIPTION, description);
    }
    
    public Boolean getActiveFlg() {
        return get(ACTIVE_FLG);
    }

    public void setActiveFlg(Boolean activeFlg) {
        set(ACTIVE_FLG, activeFlg);
    }
}
