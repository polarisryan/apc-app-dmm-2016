package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Oct 26, 2009
 * Time: 11:28:08 AM
 */
public class CorrugateComponent extends BaseModel {

    public CorrugateComponent() {
    }

    public CorrugateComponent(String manufacturer, String vendorPartNumber, String gloviaPartNumber, String description,
                              Integer qtyPerDisp, Integer totalQty, Date deliveryDate, Double unitCost, Double extCost,
                              Double ttlCost) {
        set("manufacturer", manufacturer);
        set("vendorPartNumber", vendorPartNumber);
        set("gloviaPartNumber", gloviaPartNumber);
        set("description", description);
        set("qtyPerDisp", qtyPerDisp);
        set("totalQty", totalQty);
        set("deliveryDate", deliveryDate);
        set("unitCost", unitCost);
        set("extCost", extCost);
        set("ttlCost", ttlCost);
    }

    public String getManufacturer() {
        return get("manufacturer");
    }

    public void setManufacturer(String manufacturer) {
        set("manufacturer", manufacturer);
    }

    public String getVendorPartNumber() {
        return get("vendorPartNumber");
    }

    public void setVendorPartNumber(String vendorPartNumber) {
        set("vendorPartNumber", vendorPartNumber);
    }

    public String getGloviaPartNumber() {
        return get("gloviaPartNumber");
    }

    public void setGloviaPartNumber(String gloviaPartNumber) {
        set("gloviaPartNumber", gloviaPartNumber);
    }

    public String getDescription() {
        return get("description");
    }

    public void setDescription(String description) {
        set("description", description);
    }

    public Integer getQtyPerDisp() {
        return get("qtyPerDisp");
    }

    public void setQtyPerDisp(Integer qtyPerDisp) {
        set("qtyPerDisp", qtyPerDisp);
    }

    public Integer getTotalQty() {
        return get("totalQty");
    }

    public void setTotalQty(Integer totalQty) {
        set("totalQty", totalQty);
    }

    public Date getDeliveryDate() {
        return get("deliveryDate");
    }

    public void setDeliveryDate(Date deliveryDate) {
        set("deliveryDate", deliveryDate);
    }

    public Double getUnitCost() {
        return get("unitCost");
    }

    public void setUnitCost(Double unitCost) {
        set("unitCost", unitCost);
    }

    public Double getExtCost() {
        return get("extCost");
    }

    public void setExtCost(Double extCost) {
        set("extCost", extCost);
    }

    public Double getTtlCost() {
        return get("ttlCost");
    }

    public void setTtlCost(Double ttlCost) {
        set("ttlCost", ttlCost);
    }
}
