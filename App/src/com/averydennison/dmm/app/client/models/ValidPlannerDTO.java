package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 3:09:37 PM
 */
public class ValidPlannerDTO extends BaseModel {
    public static String PLANNER_CODE = "plannerCode";
    public static String PLANNER_DESC = "plannerDesc";
    public static String ACTIVE_FLG = "activeFlg";

    public ValidPlannerDTO() {
    }

    public ValidPlannerDTO(String plannerCode, String plannerDesc, Boolean activeFlg) {
        set(PLANNER_CODE, plannerCode);
        set(PLANNER_DESC, plannerDesc);
        set(ACTIVE_FLG, activeFlg);
    }

    public String getPlannerCode() {
        return get(PLANNER_CODE);
    }

    public void setPlannerCode(String plannerCode) {
        set(PLANNER_CODE, plannerCode);
    }

    public String getPlannerDesc() {
        return get(PLANNER_DESC);
    }

    public void setPlannerDesc(String plannerDesc) {
        set(PLANNER_DESC, plannerDesc);
    }

    public Boolean isActiveFlg() {
        return get(ACTIVE_FLG);
    }

    public void setActiveFlg(Boolean isActiveFlg) {
        set(ACTIVE_FLG, isActiveFlg);
    }

    public String toString() {
    	return get(PLANNER_DESC);
    }
}
