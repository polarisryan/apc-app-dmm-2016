package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class CustomerCountryDTO extends BaseModel {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 8867752286450437783L;
	public static String CUSTOMER_ID = "customerId";
	 public static String CUSTOMER_NAME = "customerName";
	 public static String COUNTRY_ID = "countryId";
	 public static String COUNTRY_NAME = "countryName";
	 
	 public CustomerCountryDTO() {
	 }

	 public CustomerCountryDTO(Long customerId, String customerName, Long countryId, String countryName) {
	      set(CUSTOMER_ID, customerId);
	      set(CUSTOMER_NAME, customerName);
	      set(COUNTRY_ID, countryId);
	      set(COUNTRY_NAME, countryName);
	 }
	 
	 public Long getCustomerId() {
	     return get(CUSTOMER_ID);
	 }

	 public void setCustomerId(Long customerId) {
	     set(CUSTOMER_ID, customerId);
	 }

	 public String getCustomerName() {
	     return get(CUSTOMER_NAME);
	 }

	 public void setCustomerName(String customerName) {
	     set(CUSTOMER_NAME, customerName);
	 }

	 public Long getCountryId() {
	     return get(COUNTRY_ID);
	 }

	 public void setCountryId(Long countryId) {
	        set(COUNTRY_ID, countryId);
	 }

	 public String getCountryName() {
	     return get(COUNTRY_NAME);
	 }

	 public void setCountryName(String countryName) {
	        set(COUNTRY_NAME, countryName);
	 }
	 
	@Override
	public String toString() {
	     return "CustomerCountryDTO{" +
	         "customerId=" + get(CUSTOMER_ID) +
	         ", customerName='" + get(CUSTOMER_NAME) + '\'' +
	         ", countryId='" + get(COUNTRY_ID) + '\'' +
	         ", countryName='" + get(COUNTRY_NAME) + '\'' +
	         '}';
	    }
}
