package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 3:09:37 PM
 */
public class ValidBuDTO extends BaseModel {
    public static String BU_CODE = "buCode";
    public static String BU_DESC = "buDesc";
    public static String ACTIVE_FLG = "activeFlg";
    public static String PRODUCT_CATEGORY_CODE = "productCategoryCode";

    public ValidBuDTO() {
    }

    public ValidBuDTO(String buCode, String buDesc, Boolean activeFlg, String productCategoryCode) {
        set(BU_CODE, buCode);
        set(BU_DESC, buDesc);
        set(ACTIVE_FLG, activeFlg);
        set(PRODUCT_CATEGORY_CODE, productCategoryCode);
    }

    public String getBuCode() {
        return get(BU_CODE);
    }

    public void setBuCode(String buCode) {
        set(BU_CODE, buCode);
    }

    public String getBuDesc() {
        return get(BU_DESC);
    }

    public void setBuDesc(String buDesc) {
        set(BU_DESC, buDesc);
    }

    public Boolean isActiveFlg() {
        return get(ACTIVE_FLG);
    }

    public void setActiveFlg(Boolean isActiveFlg) {
        set(ACTIVE_FLG, isActiveFlg);
    }

    public Boolean isActiveOnFinanceFlg() {
        return get(PRODUCT_CATEGORY_CODE);
    }

    public void setProductCategoryCode(String productCategoryCode) {
        set(PRODUCT_CATEGORY_CODE, productCategoryCode);
    }

    public String toString() {
        return get(BU_DESC);
    }

}
