package com.averydennison.dmm.app.client.models;

import java.io.Serializable;

public class BooleanDTO implements Serializable {
	private Boolean value;

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}
	
}
