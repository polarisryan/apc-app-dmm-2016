package com.averydennison.dmm.app.client.models;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;

/**
 * User: Alvin Wong
 * Date: Jan 18, 2010
 * Time: 3:27:31 PM
 */
public class DisplayDcAndPackoutDTO extends BaseModel {

	private static final long serialVersionUID = 6643826035949658170L;
	public static final String DISPLAY_DC_ID = "displayDcId";
    public static final String DISPLAY_ID = "displayId";
    public static final String LOCATION_ID = "locationId";
    public static final String DC_CODE = "dcCode";
    public static final String SHIP_FROM_DCCODE = "shipFromDcCode";
    public static final String DC_QUANTITY = "dcQuantity";
    public static final String DT_CREATED = "dtCreated";
    public static final String DT_LAST_CHANGED = "dtLastChanged";
    public static final String USER_CREATED = "userCreated";
    public static final String USER_LAST_CHANGED = "userLastChanged";
    public static final String SHIP_WAVE_NUM = "shipWaveNum";
    public static final String MUST_ARRIVE_DATE = "mustArriveDate";
    public static final String DISPLAY_PACKOUT_ID = "displayPackoutId";
    public static final String DISPLAY_SHIP_WAVE_ID = "displayShipWaveId";
    public static final String SHIP_FROM_DATE = "shipFromDate";
    public static final String SHIP_FROM_LOCATIONID = "shipFromLocationId";
    public static final String SHIP_LOC_PRODUCT_DUE_DATE = "shipLocProductDueDate";
    public static final String PRODUCT_DUE_DATE = "productDueDate";
    public static final String CORRUGATE_DUE_DATE= "corrugateDueDate";
    public static final String DELIVERY_DATE= "deliveryDate";

    public static final String PO_QUANTITY = "poQuantity";
    public static final String PO_DT_CREATED = "poDtCreated";
    public static final String PO_DT_LAST_CHANGED = "poDtLastChanged";
    public static final String PO_USER_CREATED = "poUserCreated";
    public static final String PO_USER_LAST_CHANGED = "poUserLastChanged";

    public static final String PO_QTY_BALANCE = "poQtyBalance";
    public static final String DELETE_FLG = "deleteFlg";
    public static final String DIRTY = "dirty";
    public static final String ROW_ID = "rowId";
    public static final String TYPE = "type";
    
    public DisplayDcAndPackoutDTO() {
    	setDeleteFlg("N");
    }

    public static DisplayDcAndPackoutDTO displayDcAndPackoutDTO(DisplayDcAndPackoutDTO dap) {
    	DisplayDcAndPackoutDTO dapDTO = new DisplayDcAndPackoutDTO();
		dapDTO.setDisplayDcId(dap.getDisplayDcId());			
		dapDTO.setDisplayId(dap.getDisplayId());
		dapDTO.setLocationId(dap.getLocationId());
		dapDTO.setDcCode(dap.getDcCode());
		dapDTO.setDcQuantity(dap.getDcQuantity());
		dapDTO.setDtCreated(dap.getDtCreated());
		dapDTO.setDtLastChanged(dap.getDtLastChanged());
		dapDTO.setUserCreated(dap.getUserCreated());
		dapDTO.setUserLastChanged(dap.getUserLastChanged());
		dapDTO.setShipWaveNum(dap.getShipWaveNum());
		dapDTO.setMustArriveDate(dap.getMustArriveDate());
		dapDTO.setDisplayPackoutId(dap.getDisplayPackoutId());
		dapDTO.setDisplayShipWaveId(dap.getDisplayShipWaveId());
		dapDTO.setShipFromDate(dap.getShipFromDate());
		dapDTO.setCorrugateDueDate(dap.getCorrugateDueDate());
		dapDTO.setDeliveryDate(dap.getDeliveryDate());
		dapDTO.setShipLocProductDueDate(dap.getShipLocProductDueDate());
		dapDTO.setProductDueDate(dap.getProductDueDate());
		dapDTO.setPoQuantity(dap.getPoQuantity());
		dapDTO.setPoDtCreated(dap.getPoDtCreated());
		dapDTO.setPoDtLastChanged(dap.getPoDtLastChanged());
		dapDTO.setPoUserCreated(dap.getPoUserCreated());
		dapDTO.setPoUserLastChanged(dap.getPoUserLastChanged());
		dapDTO.setPoBalanceQty(dap.getPoBalanceQty());
		dapDTO.setShipFromLocationId(dap.getShipFromLocationId());
		dapDTO.setShipFromDcCode(dap.getShipFromDcCode());
		dapDTO.setDeleteFlg("N");    	
		return dapDTO;
    }
    
    public void updateDC(String dcCode, Long qty, Long locationId) {

    	if (getDcCode() == null) {
    		if (dcCode != null) setDirty(true);
    	}
        else if (!getDcCode().equals(dcCode))
        	setDirty(true);
        
    	if (getDcQuantity() == null) {
    		if (qty != null) setDirty(true);
    	}
        else if (!getDcQuantity().equals(qty))
        	setDirty(true);

    	if (getLocationId() == null) {
    		if (locationId != null) setDirty(true);
    	}
        else if (!getLocationId().equals(locationId))
        	setDirty(true);
        
		setDcCode(dcCode);
		setDcQuantity(qty);
		setLocationId(locationId);
    }
    
    public void updatePackout(Long shipWaveNum, Date shipLocProductDueDate, Date productDueDate, Date shipFromDate, Date mustArriveDate, Long shipWaveId, Long qty) {
    	if (getShipWaveNum() == null) {
    		if (shipWaveNum != null) setDirty(true);
    	}
        else if (!getShipWaveNum().equals(shipWaveNum))
        	setDirty(true);
    	
    	if (getShipLocProductDueDate() == null) {
    		if (shipLocProductDueDate != null) setDirty(true);
    	}
        else if (!getShipLocProductDueDate().equals(shipLocProductDueDate))
        	setDirty(true);
    	
    	if (getProductDueDate() == null) {
    		if (productDueDate != null) setDirty(true);
    	}
        else if (!getProductDueDate().equals(productDueDate))
        	setDirty(true);
    	
    	if (getShipFromDate() == null) {
    		if (shipFromDate != null) setDirty(true);
    	}
        else if (!getShipFromDate().equals(shipFromDate))
        	setDirty(true);
    	
    	if (getMustArriveDate() == null) {
    		if (mustArriveDate != null) setDirty(true);
    	}
        else if (!getMustArriveDate().equals(mustArriveDate))
        	setDirty(true);

    	if (getDisplayShipWaveId() == null) {
    		if (shipWaveId != null) setDirty(true);
    	}
        else if (!getDisplayShipWaveId().equals(shipWaveId))
        	setDirty(true);

    	if (getPoQuantity() == null) {
    		if (qty != null) setDirty(true);
    	}
        else if (!getPoQuantity().equals(qty))
        	setDirty(true);
    	
		setShipWaveNum(shipWaveNum);
		setShipLocProductDueDate(shipLocProductDueDate);
		setProductDueDate(productDueDate);
		setShipFromDate(shipFromDate);
		setMustArriveDate(mustArriveDate);
		setDisplayShipWaveId(shipWaveId);
		setPoQuantity(qty);
    	
    }
    
    public Long getDisplayDcId() {
        return get(DISPLAY_DC_ID);
    }

    public void setDisplayDcId(Long displayDcId) {
        set(DISPLAY_DC_ID, displayDcId);
    }

    public Long getDisplayId() {
        return get(DISPLAY_ID);
    }

    public void setDisplayId(Long displayId) {
        set(DISPLAY_ID, displayId);
    }

    public Long getLocationId() {
        return get(LOCATION_ID);
    }

    public void setLocationId(Long locationId) {
        set(LOCATION_ID, locationId);
    }

    public String getDcCode() {
        return get(DC_CODE);
    }

    public void setDcCode(String dcCode) {
        set(DC_CODE, dcCode);
    }

    public String getShipFromDcCode() {
        return get(SHIP_FROM_DCCODE);
    }

    public void setShipFromDcCode(String shipFromDcCode) {
        set(SHIP_FROM_DCCODE, shipFromDcCode);
    }
    
    public Long getDcQuantity() {
        return get(DC_QUANTITY);
    }

    public void setDcQuantity(Long dcQuantity) {
        set(DC_QUANTITY, dcQuantity);
    }

    public Date getDtCreated() {
        return get(DT_CREATED);
    }

    public void setDtCreated(Date dtCreated) {
        set(DT_CREATED, dtCreated);
    }

    public Date getDtLastChanged() {
        return get(DT_LAST_CHANGED);
    }

    public void setDtLastChanged(Date dtLastChanged) {
        set(DT_LAST_CHANGED, dtLastChanged);
    }

    public String getUserCreated() {
        return get(USER_CREATED);
    }

    public void setUserCreated(String userCreated) {
        set(USER_CREATED, userCreated);
    }

    public String getUserLastChanged() {
        return get(USER_LAST_CHANGED);
    }

    public void setUserLastChanged(String userLastChanged) {
        set(USER_LAST_CHANGED, userLastChanged);
    }

    public Long getShipWaveNum() {
        return get(SHIP_WAVE_NUM);
    }

    public void setShipWaveNum(Long shipWave) {
        set(SHIP_WAVE_NUM, shipWave);
    }

    public Long getShipFromLocationId() {
        return get(SHIP_FROM_LOCATIONID);
    }

    public void setShipFromLocationId(Long shipFromLocationId) {
        set(SHIP_FROM_LOCATIONID, shipFromLocationId);
    }
    
    public Date getMustArriveDate() {
        return get(MUST_ARRIVE_DATE);
    }

    public void setMustArriveDate(Date mustArriveDate) {
        set(MUST_ARRIVE_DATE, mustArriveDate);
    }
    
    public Long getDisplayPackoutId() {
        return get(DISPLAY_PACKOUT_ID);
    }

    public void setDisplayPackoutId(Long displayPackoutId) {
        set(DISPLAY_PACKOUT_ID, displayPackoutId);
    }

    public Long getDisplayShipWaveId() {
        return get(DISPLAY_SHIP_WAVE_ID);
    }

    public void setDisplayShipWaveId(Long displayShipWaveId) {
        set(DISPLAY_SHIP_WAVE_ID, displayShipWaveId);
    }

    public Date getShipFromDate() {
        return get(SHIP_FROM_DATE);
    }

    public void setShipFromDate(Date shipFromDate) {
        set(SHIP_FROM_DATE, shipFromDate);
    }

    public Date getShipLocProductDueDate() {
        return get(SHIP_LOC_PRODUCT_DUE_DATE);
    }

    public void setShipLocProductDueDate(Date shipLocProductDueDate) {
        set(SHIP_LOC_PRODUCT_DUE_DATE, shipLocProductDueDate);
    }
    
    public Date getProductDueDate() {
        return get(PRODUCT_DUE_DATE);
    }

    public void setProductDueDate(Date productDueDate) {
        set(PRODUCT_DUE_DATE, productDueDate);
    }

    public Date getCorrugateDueDate() {
        return get(CORRUGATE_DUE_DATE);
    }

    public void setCorrugateDueDate(Date corrugateDueDate) {
        set(CORRUGATE_DUE_DATE, corrugateDueDate);
    }
    
    public Date getDeliveryDate() {
        return get(DELIVERY_DATE);
    }

    public void setDeliveryDate(Date deliveryDate) {
        set(DELIVERY_DATE, deliveryDate);
    }
    
    public Long getPoQuantity() {
        return get(PO_QUANTITY);
    }

    public void setPoQuantity(Long poQuantity) {
        set(PO_QUANTITY, poQuantity);
    }
    
    public Date getPoDtCreated() {
        return get(PO_DT_CREATED);
    }

    public void setPoDtCreated(Date poDtCreated) {
        set(PO_DT_CREATED, poDtCreated);
    }

    public Date getPoDtLastChanged() {
        return get(PO_DT_LAST_CHANGED);
    }

    public void setPoDtLastChanged(Date poDtLastChanged) {
        set(PO_DT_LAST_CHANGED, poDtLastChanged);
    }

    public String getPoUserCreated() {
        return get(PO_USER_CREATED);
    }

    public void setPoUserCreated(String poUserCreated) {
        set(PO_USER_CREATED, poUserCreated);
    }

    public String getPoUserLastChanged() {
        return get(PO_USER_LAST_CHANGED);
    }

    public void setPoUserLastChanged(String poUserLastChanged) {
        set(PO_USER_LAST_CHANGED, poUserLastChanged);
    }

    public Long getPoBalanceQty() {
        return get(PO_QTY_BALANCE);
    }

    public void setPoBalanceQty(Long poQtyBalance) {
        set(PO_QTY_BALANCE, poQtyBalance);
    }

    public String getDeleteFlg() {
        return get(DELETE_FLG);
    }

    public void setDeleteFlg(String deleteFlg) {
    	if (deleteFlg != null && deleteFlg.equals("Y")) setDirty(true);
        set(DELETE_FLG, deleteFlg);
    }

    public String getType() {
        return get(TYPE);
    }

    public void setType(String type) {
        set(TYPE, type);
    }

    public String getRowId() {
        return get(ROW_ID);
    }

    public void setRowId(String rowId) {
        set(ROW_ID, rowId);
    }

    public Boolean isDirty() {
        return get(DIRTY);
    }

    public void setDirty(Boolean dirty) {
        set(DIRTY, dirty);
    }

    public String toString() {
        return "DisplayDc{" +
        		"type=" + this.getType() +
        		", delete=" + this.getDeleteFlg() +
                ", displayDcId=" + this.getDisplayDcId() +
                ", displayId=" + this.getDisplayId() +
                ", locationId=" + this.getLocationId() +
                ", dcCode=" + this.getDcCode() +
                ", dcQuantity=" + this.getDcQuantity() +
                ", dtCreated=" + this.getDtCreated() +
                ", dtLastChanged=" + this.getDtLastChanged() +
                ", userCreated='" + this.getUserCreated() + '\'' +
                ", userLastChanged='" + this.getUserLastChanged() + '\'' +
                ", shipWaveNum=" + this.getShipWaveNum() +
                ", mustArriveDate=" + this.getMustArriveDate() +
                ", displayPackoutId=" + this.getDisplayPackoutId() +
                ", displayShipWaveId=" + this.getDisplayShipWaveId() +
                ", shipFromDate=" + this.getShipFromDate() +
                ", shipLocProductDueDate=" + this.getShipLocProductDueDate() +
                ", productDueDate=" + this.getProductDueDate() +
                ", poQuantity=" + this.getPoQuantity() +
                ", poDtCreated=" + this.getPoDtCreated() +
                ", poDtLastChanged=" + this.getPoDtLastChanged() +
                ", poUserCreated='" + this.getPoUserCreated() + '\'' +
                ", poUserLastChanged='" + this.getPoUserLastChanged() + '\'' +
                '}';
    }

    @Override
    public void addChangeListener(ChangeListener... listener) {
        super.addChangeListener(listener);
    }

    @Override
    public void addChangeListener(List<ChangeListener> listeners) {
        super.addChangeListener(listeners);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    public void notify(ChangeEvent evt) {
        super.notify(evt);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value,
                                         Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }

    @Override
    public void removeChangeListener(ChangeListener... listener) {
        super.removeChangeListener(listener);
    }

    @Override
    public void removeChangeListeners() {
        super.removeChangeListeners();
    }

}
