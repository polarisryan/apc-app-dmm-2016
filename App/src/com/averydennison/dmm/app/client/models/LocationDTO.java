package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Nov 18, 2009
 * Time: 3:34:49 PM
 */
public class LocationDTO extends BaseModel {

    public static String LOCATION_ID = "locationId";
    public static String LOCATION_CODE = "locationCode";
    public static String LOCATION_NAME = "locationName";
    public static String ACTIVE_FLG = "activeFlg";

    public LocationDTO() {
    }

    public LocationDTO(Long locationId, String locationCode, String locationName, Boolean activeFlg) {
        set(LOCATION_ID, locationId);
        set(LOCATION_CODE, locationCode);
        set(LOCATION_NAME, locationName);
        set(ACTIVE_FLG, activeFlg);
    }
    
    public String getLocationCode() {
        return get(LOCATION_CODE);
    }

    public void setLocationCode(String locationCode) {
        set(LOCATION_CODE, locationCode);
    }

    public Long getLocationId() {
        return get(LOCATION_ID);
    }

    public void setLocationId(Long locationId) {
        set(LOCATION_ID, locationId);
    }

    public String getLocationName() {
        return get(LOCATION_NAME);
    }

    public void setLocationName(String locationName) {
        set(LOCATION_NAME, locationName);
    }

    public Boolean isActiveFlg() {
        return get(ACTIVE_FLG);
    }

    public void setActiveFlg(Boolean activeFlg) {
        set(ACTIVE_FLG, activeFlg);
    }

    public String toString() {
        return "Location{" +
                "locationId=" + this.getLocationId() +
                ", locationCode='" + getLocationCode() + '\'' +
                ", locationName='" + this.getLocationName() + '\'' +
                '}';
    }
}
