package com.averydennison.dmm.app.client.models;

import java.util.List;

/**
 * User: Marianandan Arockiasamy
 * Date: Jul 8, 2014
 * Time: 04:39:48 PM
 */

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;

public class ProductDetailDTO extends BaseModel {
    public static final String SKU_ID = "skuId";
    public static final String SKU = "sku";
    public static final String PRODUCT_NAME = "productName";
    public static final String BU_DESC = "buDesc";
    public static final String VERSION_NO = "versionNo";
    public static final String PROMO_FLG = "promoFlg";
    public static final String BREAK_CASE = "breakCase";
    public static final String LIST_PRICE = "listPrice";
    public static final String FULL_BURDEN_COST = "fullBurdenCost";
    public static final String INNERPACK_QTY = "innerpackQty";
    public static final String ILS_SKU_DESC = "ilsSkuDesc";
    public static final String BENCHMARK_PRICE = "benchMarkPrice";
    public static final String CAN_LIST_PRICE = "canListPrice";
    public static final String CAN_FULL_BURDEN_COST = "canFullBurdenCost";
    public static final String CAN_BENCHMARK_PRICE = "canBenchMarkPrice";
    
    public static final String CAN_MATERIAL_COST = "canMaterialCost";
    public static final String CAN_LABOR_COST = "canLaborCost";
    public static final String MATERIAL_COST = "materialCost";
    public static final String LABOR_COST = "laborCost";
    
    public ProductDetailDTO() {
    }

    public static ProductDetailDTO productDetailDTO(ProductDetailDTO pdDTO) {
        ProductDetailDTO newInstance = new ProductDetailDTO();
        
        newInstance.setSkuId(pdDTO.getSkuId());
        newInstance.setSku(pdDTO.getSku());
        newInstance.setProductName(pdDTO.getProductName());
        newInstance.setBuDesc(pdDTO.getBuDesc());
        newInstance.setVersionNo(pdDTO.getVersionNo());
        newInstance.setPromoFlg(pdDTO.isPromoFlg());
        newInstance.setBreakCase(pdDTO.getBreakCase());
        newInstance.setBenchMarkPrice(pdDTO.getBenchMarkPrice());
        newInstance.setListPrice(pdDTO.getListPrice());
        newInstance.setFullBurdenCost(pdDTO.getMaterialCost() + pdDTO.getLaborCost());
        newInstance.setInnerpackQty(pdDTO.getInnerpackQty());
        newInstance.setIlsSkuDesc(pdDTO.getIlsSkuDesc());
        newInstance.setCanBenchMarkPrice(pdDTO.getCanBenchMarkPrice());
        newInstance.setCanListPrice(pdDTO.getCanListPrice());
        
        newInstance.setCanFullBurdenCost(pdDTO.getCanMaterialCost() + pdDTO.getCanLaborCost());
        newInstance.setCanMaterialCost(pdDTO.getCanMaterialCost());
        newInstance.setCanLaborCost(pdDTO.getCanLaborCost());
        newInstance.setMaterialCost(pdDTO.getMaterialCost());
        newInstance.setLaborCost(pdDTO.getLaborCost());
        
        return newInstance;
    }

    public Long getSkuId() {
        return get(SKU_ID);
    }

    public void setSkuId(Long skuId) {
        set(SKU_ID, skuId);
    }

    public String getSku() {
    	return get(SKU);
    }

    public void setSku(String sku) {
        set(SKU, sku);
    }

    public String getProductName() {
    	return get(PRODUCT_NAME);
    }

    public void setProductName(String productName) {
        set(PRODUCT_NAME, productName);
    }

    public String getBuDesc() {
    	return get(BU_DESC);
    }

    public void setBuDesc(String buDesc) {
        set(BU_DESC, buDesc);
    }

    public String getVersionNo() {
    	return get(VERSION_NO);
    }

    public void setVersionNo(String versionNo) {
        set(VERSION_NO, versionNo);
    }

    public Boolean isPromoFlg() {
    	return get(PROMO_FLG);
    }

    public void setPromoFlg(Boolean promoFlg) {
        set(PROMO_FLG, promoFlg);
    }
    
    public String getBreakCase() {
    	return get(BREAK_CASE);
    }

    public void setBreakCase(String breakCase) {
        set(BREAK_CASE, breakCase);
    }

    public Float getBenchMarkPrice() {
    	return get(BENCHMARK_PRICE);
    }

    public void setBenchMarkPrice(Float benchMarkPrice) {
        set(BENCHMARK_PRICE, benchMarkPrice);
    }

    public Float getListPrice() {
    	return get(LIST_PRICE);
    }

    public void setListPrice(Float listPrice) {
        set(LIST_PRICE, listPrice);
    }

    public Float getFullBurdenCost() {
    	return get(FULL_BURDEN_COST);
    }

    public void setFullBurdenCost(Float fullBurdenCost) {
        set(FULL_BURDEN_COST, fullBurdenCost);
    }

    
    public Float getCanBenchMarkPrice() {
    	return get(CAN_BENCHMARK_PRICE);
    }

    public void setCanBenchMarkPrice(Float canBenchMarkPrice) {
        set(CAN_BENCHMARK_PRICE, canBenchMarkPrice);
    }

    public Float getCanListPrice() {
    	return get(CAN_LIST_PRICE);
    }

    public void setCanListPrice(Float canListPrice) {
        set(CAN_LIST_PRICE, canListPrice);
    }

    public Float getCanFullBurdenCost() {
    	return get(CAN_FULL_BURDEN_COST);
    }

    public void setCanFullBurdenCost(Float canFullBurdenCost) {
        set(CAN_FULL_BURDEN_COST, canFullBurdenCost);
    }
    public Long getInnerpackQty() {
        return get(INNERPACK_QTY);
    }

    public void setInnerpackQty(Long innerpackQty) {
        set(INNERPACK_QTY, innerpackQty);
    }

    public String getIlsSkuDesc() {
    	return get(ILS_SKU_DESC);
    }

    public void setIlsSkuDesc(String ilsSkuDesc) {
        set(ILS_SKU_DESC, ilsSkuDesc);
    }

    public Float getCanMaterialCost() {
    	return get(CAN_MATERIAL_COST);
    }

    public void setCanMaterialCost(Float canMaterialCost) {
        set(CAN_MATERIAL_COST, canMaterialCost);
    }
    
    
    public Float getCanLaborCost() {
    	return get(CAN_LABOR_COST);
    }

    public void setCanLaborCost(Float canLaborCost) {
        set(CAN_LABOR_COST, canLaborCost);
    }
    
    
    
    public Float getMaterialCost() {
    	return get(MATERIAL_COST);
    }

    public void setMaterialCost(Float materialCost) {
        set(MATERIAL_COST, materialCost);
    }
    
    
    public Float getLaborCost() {
    	return get(LABOR_COST);
    }

    public void setLaborCost(Float laborCost) {
        set(LABOR_COST, laborCost);
    }
    
    
    
    @Override
    public void addChangeListener(ChangeListener... listener) {
        super.addChangeListener(listener);
    }

    @Override
    public void addChangeListener(List<ChangeListener> listeners) {
        super.addChangeListener(listeners);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    public void notify(ChangeEvent evt) {
        super.notify(evt);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value,
                                         Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }

    @Override
    public void removeChangeListener(ChangeListener... listener) {
        super.removeChangeListener(listener);
    }

    @Override
    public void removeChangeListeners() {
        super.removeChangeListeners();
    }

}

