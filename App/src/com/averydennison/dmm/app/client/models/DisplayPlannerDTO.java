package com.averydennison.dmm.app.client.models;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;

public class DisplayPlannerDTO extends BaseModel {
    public static final String DISPLAY_PLANNER_ID = "displayPlannerId";
    public static final String KIT_COMPONENT_ID = "kitComponentId";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String INITIALS = "initials";
    public static final String PLANNER_CODE = "plannerCode";
    public static final String PLANT_CODE = "plantCode";
    public static final String LEAD_TIME = "leadTime";
    public static final String PLANT_OVERRIDE_FLG = "plantOverrideFlg";
    public static final String DISPLAY_DC_ID = "displayDcId";    
    public static final String DT_CREATED = "dtCreated";
    public static final String DT_LAST_CHANGED = "dtLastChanged";
    public static final String USER_CREATED = "userCreated";
    public static final String USER_LAST_CHANGED = "userLastChanged";
    public static final String DC_NAME = "dcName";
    public static final String DC_CODE = "dcCode";

    public DisplayPlannerDTO() {
    }

    public static DisplayPlannerDTO DisplayLogisticsInfoDTO(DisplayPlannerDTO dpDTO) {
        DisplayPlannerDTO newInstance = new DisplayPlannerDTO();
        newInstance.setDisplayPlannerId(dpDTO.getDisplayPlannerId());
        newInstance.setKitComponentId(dpDTO.getKitComponentId());
        newInstance.setFirstName(dpDTO.getFirstName());
        newInstance.setLastName(dpDTO.getLastName());
        newInstance.setInitials(dpDTO.getInitials());
        newInstance.setPlannerCode(dpDTO.getPlannerCode());
        newInstance.setPlantCode(dpDTO.getPlantCode());
        newInstance.setLeadTime(dpDTO.getLeadTime());
        newInstance.setPlantOverrideFlg(dpDTO.isPlantOverrideFlg());
        newInstance.setDisplayDcId(dpDTO.getDisplayDcId());
        newInstance.setDtCreated(dpDTO.getDtCreated());
        newInstance.setDtLastChanged(dpDTO.getDtLastChanged());
        newInstance.setUserCreated(dpDTO.getUserCreated());
        newInstance.setUserLastChanged(dpDTO.getUserLastChanged());
        newInstance.setDcName(dpDTO.getDcName());
        newInstance.setDcCode(dpDTO.getDcCode());
        return newInstance;
    }

    public Long getDisplayPlannerId() {
        return get(DISPLAY_PLANNER_ID);
    }

    public void setDisplayPlannerId(Long displayPlannerId) {
        set(DISPLAY_PLANNER_ID, displayPlannerId);
    }

    public Long getKitComponentId() {
        return get(KIT_COMPONENT_ID);
    }

    public void setKitComponentId(Long kitComponentId) {
        set(KIT_COMPONENT_ID, kitComponentId);
    }

    public String getFirstName() {
        return get(FIRST_NAME);
    }

    public void setFirstName(String firstName) {
        set(FIRST_NAME, firstName);
    }

    public void setLastName(String lastName) {
        set(LAST_NAME, lastName);
    }

    public String getLastName() {
        return get(LAST_NAME);
    }


    public String getInitials() {
        return get(INITIALS);
    }

    public void setInitials(String initials) {
        set(INITIALS, initials);
    }

    public String getPlannerCode() {
        return get(PLANNER_CODE);
    }

    public void setPlannerCode(String plannerCode) {
        set(PLANNER_CODE, plannerCode);
    }

    public String getPlantCode() {
        return get(PLANT_CODE);
    }

    public void setPlantCode(String plantCode) {
        set(PLANT_CODE, plantCode);
    }

    public String getLeadTime() {
        return get(LEAD_TIME);
    }

    public void setLeadTime(String leadTime) {
        set(LEAD_TIME, leadTime);
    }

    public Boolean isPlantOverrideFlg() {
        return get(PLANT_OVERRIDE_FLG);
    }

    public void setPlantOverrideFlg(Boolean plantOverrideFlg) {
        set(PLANT_OVERRIDE_FLG, plantOverrideFlg);
    }

    public Long getDisplayDcId() {
        return get(DISPLAY_DC_ID);
    }

    public void setDisplayDcId(Long displayDcId) {
        set(DISPLAY_DC_ID, displayDcId);
    }

    public Date getDtCreated() {
        return get(DT_CREATED);
    }

    public void setDtCreated(Date dtCreated) {
        set(DT_CREATED, dtCreated);
    }

    public Date getDtLastChanged() {
        return get(DT_LAST_CHANGED);
    }

    public void setDtLastChanged(Date dtLastChanged) {
        set(DT_LAST_CHANGED, dtLastChanged);
    }

    public String getUserCreated() {
        return get(USER_CREATED);
    }

    public void setUserCreated(String userCreated) {
        set(USER_CREATED, userCreated);
    }

    public String getUserLastChanged() {
        return get(USER_LAST_CHANGED);
    }

    public void setUserLastChanged(String userLastChanged) {
        set(USER_LAST_CHANGED, userLastChanged);
    }

    public String getDcName() {
        return get(DC_NAME);
    }

    public void setDcName(String dcName) {
        set(DC_NAME, dcName);
    }    
    
    public String getDcCode() {
        return get(DC_CODE);
    }

    public void setDcCode(String dcCode) {
        set(DC_CODE, dcCode);
    }    
    
    @Override
    public void addChangeListener(ChangeListener... listener) {
        super.addChangeListener(listener);
    }

    @Override
    public void addChangeListener(List<ChangeListener> listeners) {
        super.addChangeListener(listeners);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    public void notify(ChangeEvent evt) {
        super.notify(evt);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value,
                                         Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }

    @Override
    public void removeChangeListener(ChangeListener... listener) {
        super.removeChangeListener(listener);
    }

    @Override
    public void removeChangeListeners() {
        super.removeChangeListeners();
    }

}

