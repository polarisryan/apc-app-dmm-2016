package com.averydennison.dmm.app.client.models;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;

public class CorrugateComponentsDetailDTO extends BaseModel {
	public static final String DISPLAY_ID="displayId";
	public static final String  CORRUGATE_COMPONENT_ID="corrugateComponentId";
	public static final String  COUNTRY_ID="countryId";
	public static final String MANUFACTURER_ID="manufacturerId";
	public static final String  VENDOR_PART_NUM="vendorPartNum";
	public static final String  GLOVIA_PART_NUM="gloviaPartNum";
	public static final String  DESCRIPTION="description";
	public static final String  BOARD_TEST="boardTest";
	public static final String  MATERIAL="material";
	public static final String  QTY_PER_DISPLAY="qtyPerDisplay";
	public static final String  PIECE_PRICE="piecePrice";
	public static final String  TOTAL_COST="totalCost";
	public static final String  TOTAL_QTY="totalQty";
	public static final String  DT_CREATED="dtCreated";
	public static final String  DT_LAST_CHANGED="dtLastChanged";
	public static final String  USER_CREATED="userCreated";
	public static final String  USER_LAST_CHANGED="userLastChanged";
	public static final String  COUNTRY_NAME="countryName";
	public static final String  NAME="name";
	
	public CorrugateComponentsDetailDTO(){
		
	}
	public CorrugateComponentsDetailDTO( Long displayId,  Long  corrugateComponentId,
										 Long  countryId, Long  manufacturerId, String  vendorPartNum, String  gloviaPartNum,
										 String  description, String  boardTest, String  material, Long  qtyPerDisplay,
										 Double  piecePrice, Double  totalCost, Long  totalQty, Date  dtCreated,
										 Date  dtLastChanged, String  userCreated, String  userLastChanged, String  countryName,
										 String  name)
			{
			set(DISPLAY_ID , displayId);
			set(CORRUGATE_COMPONENT_ID , corrugateComponentId);
			set(COUNTRY_ID , countryId);
			set(MANUFACTURER_ID , manufacturerId);
			set(VENDOR_PART_NUM , vendorPartNum);
			set(GLOVIA_PART_NUM , gloviaPartNum);
			set(DESCRIPTION , description);
			set(BOARD_TEST , boardTest);
			set(MATERIAL , material);
			set(QTY_PER_DISPLAY , qtyPerDisplay);
			set(PIECE_PRICE , piecePrice);
			set(TOTAL_COST , totalCost);
			set(TOTAL_QTY , totalQty);
			set(DT_CREATED , dtCreated);
			set(DT_LAST_CHANGED , dtLastChanged);
			set(USER_CREATED , userCreated);
			set(USER_LAST_CHANGED , userLastChanged);
			set(COUNTRY_NAME , countryName);
			set(NAME , name);
			}
	public Long getDisplayId() {
		return get(DISPLAY_ID);
	}
	public void setDisplayId(Long displayId) {
		set(DISPLAY_ID , displayId);
	}
	public Long getCorrugateComponentId() {
		return get(CORRUGATE_COMPONENT_ID);
	}
	public void setCorrugateComponentId(Long corrugateComponentId) {
		set(CORRUGATE_COMPONENT_ID, corrugateComponentId);
	}
	public Long getCountryId() {
		return get(COUNTRY_ID);
	}
	public void setCountryId(Long countryId) {
		set(COUNTRY_ID , countryId);
	}
	public Long getManufacturerId() {
		return get(MANUFACTURER_ID);
	}
	public void setManufacturerId(Long manufacturerId) {
		set(MANUFACTURER_ID , manufacturerId);
	}
	public String getVendorPartNum() {
		return get(VENDOR_PART_NUM);
	}
	public void setVendorPartNum(String vendorPartNum) {
		set(VENDOR_PART_NUM, vendorPartNum);
	}
	public String getGloviaPartNum() {
		return get(GLOVIA_PART_NUM);
	}
	public void setGloviaPartNum(String gloviaPartNum) {
		set(GLOVIA_PART_NUM , gloviaPartNum);
	}
	public String getDescription() {
		return get(DESCRIPTION);
	}
	public void setDescription(String description) {
		set(DESCRIPTION, description);
	}
	public String getBoardTest() {
		return get(BOARD_TEST);
	}
	public void setBoardTest(String boardTest) {
		set(BOARD_TEST, boardTest);
	}
	public String getMaterial() {
		return get(MATERIAL);
	}
	public void setMaterial(String material) {
		set(MATERIAL , material);
	}
	public Long getQtyPerDisplay() {
		return get(QTY_PER_DISPLAY);
	}
	public void setQtyPerDisplay(Long qtyPerDisplay) {
		set(QTY_PER_DISPLAY , qtyPerDisplay);
	}
	public Double getPiecePrice() {
		return get(PIECE_PRICE);
	}
	public void setPiecePrice(Double piecePrice) {
		set(PIECE_PRICE , piecePrice);
	}
	public Double getTotalCost() {
		return get(TOTAL_COST);
	}
	public void setTotalCost(Double totalCost) {
		set(TOTAL_COST, totalCost);
	}
	public Long getTotalQty() {
		return get(TOTAL_QTY);
	}
	public void setTotalQty(Long totalQty) {
		set(TOTAL_QTY, totalQty);
	}
	public Date getDtCreated() {
		return get(DT_CREATED);
	}
	public void setDtCreated(Date dtCreated) {
		set(DT_CREATED, dtCreated);
	}
	public Date getDtLastChanged() {
		return get(DT_LAST_CHANGED);
	}
	public void setDtLastChanged(Date dtLastChanged) {
		set(DT_LAST_CHANGED, dtLastChanged);
	}
	public String getUserCreated() {
		return get(USER_CREATED);
	}
	public void setUserCreated(String userCreated) {
		set(USER_CREATED , userCreated);
	}
	public String getUserLastChanged() {
		return get(USER_LAST_CHANGED);
	}
	public void setUserLastChanged(String userLastChanged) {
		set(USER_LAST_CHANGED, userLastChanged);
	}
	public String getCountryName() {
		return get(COUNTRY_NAME);
	}
	public void setCountryName(String countryName) {
		set(COUNTRY_NAME, countryName);
	}
	public String getName() {
		return get(NAME);
	}
	public void setName(String name) {
		set(NAME , name);
	}
	
	@Override
	public void addChangeListener(ChangeListener... listener) {
		super.addChangeListener(listener);
	}

	@Override
	public void addChangeListener(List<ChangeListener> listeners) {
		super.addChangeListener(listeners);
	}

	@Override
	protected void fireEvent(int type, Model item) {
		super.fireEvent(type, item);
	}

	@Override
	protected void fireEvent(int type) {
		super.fireEvent(type);
	}

	@Override
	public void notify(ChangeEvent evt) {
		super.notify(evt);
	}

	@Override
	protected void notifyPropertyChanged(String name, Object value,
			Object oldValue) {
		super.notifyPropertyChanged(name, value, oldValue);
	}

	@Override
	public void removeChangeListener(ChangeListener... listener) {
		super.removeChangeListener(listener);
	}

	@Override
	public void removeChangeListeners() {
		super.removeChangeListeners();
	}

	 public void updateCalculatedFields(Long actualQty) {
		System.out.println("CorrugateComponentDetailDTO:updateCalculatedFields starts : ");
		 try{
			Long qtyPerDisplay = this.get(QTY_PER_DISPLAY);
	    	if(qtyPerDisplay==null) qtyPerDisplay=new Long(0);
	    	Double piecePrice = get(PIECE_PRICE);
	    	if(piecePrice==null) piecePrice=new Double(0.0);
	    	Double totCost=0.00;
	    	totCost=qtyPerDisplay * piecePrice;
	    	setTotalCost(totCost);
	    	Long locActualQty=actualQty;
	    	if(locActualQty==null) locActualQty=new Long(0);
	    	Long totQty=0L;
	    	totQty=qtyPerDisplay*locActualQty;
	    	setTotalQty(totQty);
	    	System.out.println("CorrugateComponentDetailDTO:updateCalculatedFields totQty : "+totQty);
	    }catch(Exception ex){
			 System.out.println("CorrugateComponentDetailDTO:updateCalculatedFields Exception : "+ex.toString());
		 }
	    System.out.println("CorrugateComponentDetailDTO:updateCalculatedFields ends : ");
	 }
}
