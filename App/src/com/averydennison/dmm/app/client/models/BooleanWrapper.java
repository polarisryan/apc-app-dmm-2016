package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.Model;

/**
 * User: Spart Arguello
 * Date: Dec 11, 2009
 * Time: 9:31:19 AM
 */
public class BooleanWrapper extends BaseModel {
    public static String NOTIFY = "notify";

    public BooleanWrapper() {
    }

    public Boolean isNotify() {
        return get(NOTIFY);
    }

    public void setNotify(Boolean notify) {
        set(NOTIFY, notify);
    }

    @Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value, Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }
}
