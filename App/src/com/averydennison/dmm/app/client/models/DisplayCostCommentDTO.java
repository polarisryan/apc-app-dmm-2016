package com.averydennison.dmm.app.client.models;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;

public class DisplayCostCommentDTO extends BaseModel{
	/**
	 * 
	 */
	/**
	 * 
	 */
	public static final String DISPLAY_COST_COMMENT_ID="displayCostCommentId";
	public static final String DISPLAY_COST_ID="displayCostId";
	public static final String CREATE_DATE="createDate";
	public static final String USER_NAME="userName";
	public static final String COST_COMMENT="costComment";
	public static final String DISPLAY_SCENARIO_ID="displayScenarioId";
	
	public DisplayCostCommentDTO() {
    }
	
	public Long getDisplayCostCommentId() {
		return get(DISPLAY_COST_COMMENT_ID);
	}
	public void setDisplayCostCommentId(Long displayCostCommentId) {
		set(DISPLAY_COST_COMMENT_ID,displayCostCommentId);
	}
	public Long getDisplayScenarioId() {
		return get(DISPLAY_SCENARIO_ID);
	}
	public void setDisplayScenarioId(Long displayScenarioId) {
		set(DISPLAY_SCENARIO_ID,displayScenarioId);
	}
	public Long getDisplayCostId() {
		return get(DISPLAY_COST_ID);
	}
	public void setDisplayCostId(Long displayCostId) {
		set(DISPLAY_COST_ID,displayCostId);
	}
	public String getCreateDate() {
		return get(CREATE_DATE);
	}
	public void setCreateDate(String createDate) {
		set(CREATE_DATE,createDate);
	}
	public String getUserName() {
		return get(USER_NAME);
	}
	public void setUserName(String userName) {
		set(USER_NAME,userName);
	}
	public String getCostComment() {
		return get(COST_COMMENT);
	}
	public void setCostComment(String costComment) {
		set(COST_COMMENT,costComment);
	}
	
	@Override
	public void addChangeListener(ChangeListener... listener) {
		super.addChangeListener(listener);
	}

	@Override
	public void addChangeListener(List<ChangeListener> listeners) {
		super.addChangeListener(listeners);
	}

	@Override
	protected void fireEvent(int type, Model item) {
		super.fireEvent(type, item);
	}

	@Override
	protected void fireEvent(int type) {
		super.fireEvent(type);
	}

	@Override
	public void notify(ChangeEvent evt) {
		super.notify(evt);
	}

	@Override
	protected void notifyPropertyChanged(String name, Object value,
			Object oldValue) {
		super.notifyPropertyChanged(name, value, oldValue);
	}

	@Override
	public void removeChangeListener(ChangeListener... listener) {
		super.removeChangeListener(listener);
	}

	@Override
	public void removeChangeListeners() {
		super.removeChangeListeners();
	}
}
