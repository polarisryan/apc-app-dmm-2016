package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello 
 * Date: Jan 13, 2010
 */
public class ManufacturerDTO extends BaseModel {
	public static final String MANUFACTURER_ID = "manufacturerId";
	public static final String NAME = "name";

	public ManufacturerDTO(){
		
	}
	  public ManufacturerDTO(Long manufacturerId, String name) {
	        set(MANUFACTURER_ID, manufacturerId);
	        set(NAME, name);
	    }
	  
	public Long getManufacturerId() {
		return get(MANUFACTURER_ID);
	}

	public void setManufacturerId(Long manufacturerId) {
		set(MANUFACTURER_ID, manufacturerId);
	}

	public String getName() {
		return get(NAME);
	}

	public void setName(String name) {
		set(NAME, name);
	}
}
