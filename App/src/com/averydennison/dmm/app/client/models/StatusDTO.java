package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Nov 4, 2009
 * Time: 10:23:56 AM
 */
public class StatusDTO extends BaseModel {
    public static final String STATUS_ID = "statusId";
    public static final String STATUS_NAME = "statusName";
    public static final String DESCRIPTION = "description";

    public StatusDTO() {
    }

    public StatusDTO(Long statusId, String statusName, String description) {
        set(STATUS_ID, statusId);
        set(STATUS_NAME, statusName);
        set(DESCRIPTION, description);
    }

    public Long getStatusId() {
        return get(STATUS_ID);
    }

    public void setStatusId(Long statusId) {
        set(STATUS_ID, statusId);
    }

    public String getStatusName() {
        return get(STATUS_NAME);
    }

    public void setStatusName(String statusName) {
        set(STATUS_NAME, statusName);
    }

    public String getDescription() {
        return get(DESCRIPTION);
    }

    public void setDescription(String description) {
        set(DESCRIPTION, description);
    }
}
