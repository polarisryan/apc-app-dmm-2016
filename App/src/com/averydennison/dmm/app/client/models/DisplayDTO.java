package com.averydennison.dmm.app.client.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.Model;

/**
 * User: Spart Arguello
 * Date: Oct 15, 2009
 * Time: 9:52:23 AM
 */
public class DisplayDTO extends BaseModel {
    public static final String DISPLAY_ID = "displayId";
    public static final String SKU = "sku";
    public static final String DESCRIPTION = "description";
    public static final String CUSTOMER_ID = "customerId";
    public static final String COUNTRY_ID = "countryId";
    public static final String STATUS_ID = "statusId";
    public static final String QTY_FORECAST = "qtyForecast";
    public static final String DISPLAY_SETUP_ID = "displaySetupId";
    public static final String DISPLAY_LOGISTICS_ID = "displayLogisticsId";
    public static final String PROMO_PERIOD_ID = "promoPeriodId";
    public static final String PROMO_YEAR = "promoYear";
    public static final String DISPLAY_SALES_REP_ID = "displaySalesRepId";
    public static final String DESTINATION = "destination";
    public static final String STRUCTURE_ID = "structureId";
    public static final String QSI_DOC_NUMBER = "qsiDocNumber";
    public static final String SKU_MIX_FINAL_FLG = "skuMixFinalFlg";
    public static final String MANUFACTURER_ID = "manufacturerId";
    public static final String DT_CREATED = "dtCreated";
    public static final String DT_LAST_CHANGED = "dtLastChanged";
    public static final String USER_CREATED = "userCreated";
    public static final String USER_LAST_CHANGED = "userLastChanged";
    public static final String DT_GONOGO = "dtGoNoGo";
    public static final String SHIP_DATE = "shipDate";
    public static final String DISPLAY_SALES_REP_BY_DISPLAY_SALES_REP_ID = "displaySalesRepByDisplaySalesRepId";
    public static final String DISPLAY_SALES_REP_BY_FULL_NAME = "displaySalesRepByFulName";
    public static final String STATUS_BY_STATUS_ID = "statusByStatusId";
    public static final String STATUS_BY_NAME = "statusByStatusName";
    public static final String CUSTOMER_BY_CUSTOMER_ID = "customerByCustomerId";
    public static final String COUNTRY_BY_COUNTRY_ID = "countryByCountryId";
    public static final String CUSTOMER_BY_NAME = "customerByName";
    public static final String COUNTRY_BY_NAME = "countryByName";
    public static final String STRUCTURE_BY_STRUCTURE_ID = "structureByStructureId";
    public static final String STRUCTURE_BY_NAME = "structureByName";
    public static final String DISPLAY_SHIP_WAVES_BY_DISPLAY_ID = "displayShipWavesByDisplayId";
    public static final String DISPLAY_SHIP_WAVES_BY_SHIP_DATE = "displayShipWavesByShipDate";
    public static final String PROMO_PERIOD_BY_PROMO_PERIOD_ID = "promoPeriodByPromoPeriodId";
    public static final String PROMO_PERIOD_BY_PROMO_PERIOD_NAME = "promoPeriodByPromoPeriodName";
    public static final String CUSTOMER_DISPLAY_NUMBER = "customerDisplayNumber";
    public static final String RYG_STATUS_FLG = "rygStatusFlg";
    public static final String PACKOUT_VENDOR_ID="packoutVendorId";
    public static final String ACT_QUANTITY="actualQty";
    public static final String CM_PROJECT_NUMBER = "cmProjectNumber";
    public static final String APPROVERS = "approvers";
    public DisplayDTO() {
    }

    public static DisplayDTO CopyDisplayDTO(DisplayDTO displayDTO) {
        DisplayDTO newInstance = new DisplayDTO();
        newInstance.setCustomerId(displayDTO.getCustomerId());
        newInstance.setCountryId(displayDTO.getCountryId());
        newInstance.setDescription(displayDTO.getDescription());
        newInstance.setDestination(displayDTO.getDestination());
        newInstance.setDisplayLogisticsId(displayDTO.getDisplayLogisticsId());
        newInstance.setDisplaySalesRepId(displayDTO.getDisplaySalesRepId());
        newInstance.setDtCreated(displayDTO.getDtCreated());
        newInstance.setDtLastChanged(displayDTO.getDtLastChanged());
        newInstance.setManufacturerId(displayDTO.getManufacturerId());
        newInstance.setPromoPeriodId(displayDTO.getPromoPeriodId());
        newInstance.setQtyForecast(displayDTO.getQtyForecast());
        newInstance.setQsiDocNumber(displayDTO.getQsiDocNumber());
        newInstance.setSku(displayDTO.getSku());
        newInstance.setStatusId(displayDTO.getStatusId());
        newInstance.setSkuMixFinalFlg(displayDTO.isSkuMixFinalFlg());
        newInstance.setStructureId(displayDTO.getStructureId());
        newInstance.setUserCreated(displayDTO.getUserCreated());
        newInstance.setUserLastChanged(displayDTO.getUserLastChanged());
        newInstance.setDisplaySalesRepByDisplaySalesRepId(displayDTO.getDisplaySalesRepByDisplaySalesRepId());
        newInstance.setStatusByStatusId(displayDTO.getStatusByStatusId());
        newInstance.setCustomerByCustomerId(displayDTO.getCustomerByCustomerId());
        newInstance.setCountryByCountryId(displayDTO.getCountryByCountryId());
        newInstance.setStructureByStructureId(displayDTO.getStructureByStructureId());
        newInstance.setPromoPeriodByPromoPeriodId(displayDTO.getPromoPeriodByPromoPeriodId());
        newInstance.setDtGoNoGo(displayDTO.getDtGoNoGo());
        newInstance.setApproverIds(displayDTO.getApproverIds());
        return newInstance;
    }

    public Long getDisplayId() {
        return get(DISPLAY_ID);
    }

    public void setDisplayId(Long displayId) {
        set(DISPLAY_ID, displayId);
    }

    

    public String getDescription() {
        return get(DESCRIPTION);
    }

    public void setDescription(String description) {
        set(DESCRIPTION, description);
    }

    public String getSku() {
        return get(SKU);
    }

    public void setSku(String sku) {
        set(SKU, sku);
    }
    
    public String getCustomerDisplayNumber() {
        return get(CUSTOMER_DISPLAY_NUMBER);
    }

    public void setCustomerDisplayNumber(String customerDisplayNumber) {
        set(CUSTOMER_DISPLAY_NUMBER, customerDisplayNumber);
    }
    
    public String getCmProjectNumber() {
        return get(CM_PROJECT_NUMBER);
    }

    public void setCmProjectNumber(String cmProjectNumber) {
        set(CM_PROJECT_NUMBER, cmProjectNumber);
    }
    
    public Long getCustomerId() {
        return get(CUSTOMER_ID);
    }

    public void setCustomerId(Long customerId) {
        set(CUSTOMER_ID, customerId);
    }

    public Long getCountryId() {
        return get(COUNTRY_ID);
    }

    public void setCountryId(Long countryId) {
        set(COUNTRY_ID, countryId);
    }
    
    public Long getStatusId() {
        return get(STATUS_ID);
    }

    public void setStatusId(Long statusId) {
        set(STATUS_ID, statusId);
    }


    public Long getQtyForecast() {
        return get(QTY_FORECAST);
    }

    public void setQtyForecast(Long qtyForecast) {
        set(QTY_FORECAST, qtyForecast);
    }


    public Long getDisplayLogisticsId() {
        return get(DISPLAY_LOGISTICS_ID);
    }

    public void setDisplayLogisticsId(Long displayLogisticsId) {
        set(DISPLAY_LOGISTICS_ID, displayLogisticsId);
    }

    public Long getDisplaySetupId() {
        return get(DISPLAY_SETUP_ID);
    }

    public void setDisplaySetupId(Long displaySetupId) {
        set(DISPLAY_SETUP_ID, displaySetupId);
    }

    public Long getPromoPeriodId() {
        return get(PROMO_PERIOD_ID);
    }

    public void setPromoPeriodId(Long promoPeriodId) {
        set(PROMO_PERIOD_ID, promoPeriodId);
    }


    public Long getDisplaySalesRepId() {
        return get(DISPLAY_SALES_REP_ID);
    }

    public void setDisplaySalesRepId(Long displaySalesRepId) {
        set(DISPLAY_SALES_REP_ID, displaySalesRepId);
    }

    public String getDestination() {
        return get(DESTINATION);
    }

    public void setDestination(String destination) {
        set(DESTINATION, destination);
    }

    public Long getStructureId() {
        return get(STRUCTURE_ID);
    }

    public void setStructureId(Long structureId) {
        set(STRUCTURE_ID, structureId);
    }


    public String getQsiDocNumber() {
        return get(QSI_DOC_NUMBER);
    }

    public void setQsiDocNumber(String qsiDocNumber) {
        set(QSI_DOC_NUMBER, qsiDocNumber);
    }

    public Boolean isSkuMixFinalFlg() {
        return get(SKU_MIX_FINAL_FLG);
    }

    public void setSkuMixFinalFlg(Boolean skuMixFinalFlg) {
        set(SKU_MIX_FINAL_FLG, skuMixFinalFlg);
    }

    public Long getManufacturerId() {
        return get(MANUFACTURER_ID);
    }

    public void setManufacturerId(Long manufacturerId) {
        set(MANUFACTURER_ID, manufacturerId);
    }

    
    public Long getPackoutVendorId() {
        return get(PACKOUT_VENDOR_ID);
    }

    public void setPackoutVendorId(Long packoutVendorId) {
        set(PACKOUT_VENDOR_ID, packoutVendorId);
    }
    
    public Long getActualQty() {
        return get(ACT_QUANTITY);
    }

    public void setActualQty(Long actualQty) {
        set(ACT_QUANTITY, actualQty);
    }
    
    public Date getDtCreated() {
        return get(DT_CREATED);
    }

    public void setDtCreated(Date dtCreated) {
        set(DT_CREATED, dtCreated);
    }


    public Date getDtLastChanged() {
        return get(DT_LAST_CHANGED);
    }

    public void setDtLastChanged(Date dtLastChanged) {
        set(DT_LAST_CHANGED, dtLastChanged);
    }

    public String getUserCreated() {
        return get(USER_CREATED);
    }

    public void setUserCreated(String userCreated) {
        set(USER_CREATED, userCreated);
    }


    public String getUserLastChanged() {
        return get(USER_LAST_CHANGED);
    }

    public void setUserLastChanged(String userLastChanged) {
        set(USER_LAST_CHANGED, userLastChanged);
    }

    public Date getShipDate() {
        return get(SHIP_DATE);
    }

    public void setShipDate(Date shipDate) {
        set(SHIP_DATE, shipDate);
    }

    public Date getDtGoNoGo() {
        return get(DT_GONOGO);
    }

    public void setDtGoNoGo(Date dtGoNoGo) {
        set(DT_GONOGO, dtGoNoGo);
    }

    public String getRygStatusFlg() {
        return get(RYG_STATUS_FLG);
    }

    public void setRygStatusFlg(String rygStatusFlg) {
        set(RYG_STATUS_FLG, rygStatusFlg);
    }
    
    @Override
    public String toString() {
        return "Display{" +
                "displayId=" + get(DISPLAY_ID) +
                ", sku='" + get(SKU) + '\'' +
                ", description='" + get(DESCRIPTION) + '\'' +
                ", customerId=" + get(CUSTOMER_ID) +
                ", countryId=" + get(COUNTRY_ID) +
                ", statusId=" + get(STATUS_ID) +
                ", qtyForecast=" + get(QTY_FORECAST) +
                ", displayLogisticsId=" + get(DISPLAY_LOGISTICS_ID) +
                ", promoPeriodId=" + get(PROMO_PERIOD_ID) +
                ", displaySalesRepId=" + get(DISPLAY_SALES_REP_ID) +
                ", destination='" + get(DESTINATION) + '\'' +
                ", structureId=" + get(STRUCTURE_ID) +
                ", qsiDocNumber='" + get(QSI_DOC_NUMBER) + '\'' +
                ", skuMixFinalFlg=" + get(SKU_MIX_FINAL_FLG) +
                ", manufacturerId=" + get(MANUFACTURER_ID) +
                ", dtGoNoGo=" + get(DT_GONOGO) +
                ", RYGStatus=" +get(RYG_STATUS_FLG) +
                ", dtCreated=" + get(DT_CREATED) +
                ", dtLastChanged=" + get(DT_LAST_CHANGED) +
                ", userCreated='" + get(USER_CREATED) + '\'' +
                ", userLastChanged='" + get(USER_LAST_CHANGED) + '\'' +
                ", packoutVendorId='" + get(PACKOUT_VENDOR_ID) + '\'' +
                ", qctQty='" + get(ACT_QUANTITY) + '\'' +
                '}';
    }

    public DisplaySalesRepDTO getDisplaySalesRepByDisplaySalesRepId() {
        return get(DISPLAY_SALES_REP_BY_DISPLAY_SALES_REP_ID);
    }

    public void setDisplaySalesRepByDisplaySalesRepId(DisplaySalesRepDTO displaySalesRepByDisplaySalesRepId) {
        if (displaySalesRepByDisplaySalesRepId != null) {
            set(DISPLAY_SALES_REP_BY_DISPLAY_SALES_REP_ID, displaySalesRepByDisplaySalesRepId);
            if (displaySalesRepByDisplaySalesRepId.getFullName() != null) {
                set(DISPLAY_SALES_REP_BY_FULL_NAME, displaySalesRepByDisplaySalesRepId.getFullName());
            }
        }
    }

    public StatusDTO getStatusByStatusId() {
        return get(STATUS_BY_STATUS_ID);
    }

    public void setStatusByStatusId(StatusDTO statusByStatusId) {
        if (statusByStatusId != null) {
            set(STATUS_BY_STATUS_ID, statusByStatusId);
            set(STATUS_BY_NAME, statusByStatusId.getStatusName());
        }
    }

    public CustomerDTO getCustomerByCustomerId() {
        return get(CUSTOMER_BY_CUSTOMER_ID);
    }

    public void setCustomerByCustomerId(CustomerDTO customerByCustomerId) {
        if (customerByCustomerId != null) {
            set(CUSTOMER_BY_CUSTOMER_ID, customerByCustomerId);
            set(CUSTOMER_BY_NAME, customerByCustomerId.getCustomerName());
        }
    }

    public CountryDTO getCountryByCountryId() {
        return get(COUNTRY_BY_COUNTRY_ID);
    }

    public void setCountryByCountryId(CountryDTO countryByCountryId) {
        if (countryByCountryId != null) {
            set(COUNTRY_BY_COUNTRY_ID, countryByCountryId);
            set(COUNTRY_BY_NAME, countryByCountryId.getCountryName());
        }
    }
    
    
    public StructureDTO getStructureByStructureId() {
        return get(STRUCTURE_BY_STRUCTURE_ID);
    }

    public void setStructureByStructureId(StructureDTO structureByStructureId) {
        if (structureByStructureId != null) {
            set(STRUCTURE_BY_STRUCTURE_ID, structureByStructureId);
            set(STRUCTURE_BY_NAME, structureByStructureId.getStructureName());
        }
    }

    public PromoPeriodDTO getPromoPeriodByPromoPeriodId() {
        return get(PROMO_PERIOD_BY_PROMO_PERIOD_ID);
    }

    public void setPromoPeriodByPromoPeriodId(PromoPeriodDTO promoPeriodByPromoPeriodId) {
        if (promoPeriodByPromoPeriodId != null) {
            set(PROMO_PERIOD_BY_PROMO_PERIOD_ID, promoPeriodByPromoPeriodId);
            set(PROMO_PERIOD_BY_PROMO_PERIOD_NAME, promoPeriodByPromoPeriodId.getPromoPeriodName());
        }
    }

    @Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    public void notify(ChangeEvent evt) {
        super.notify(evt);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value, Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }

	public void setPromoYear(Long promoYear) {
		set(PROMO_YEAR, promoYear);
	}
	
	public Long getPromoYear(){
		return get(PROMO_YEAR);
	}
	
	public boolean equalsCostAnalysisDisplayInfo(DisplayDTO controlModel) {
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..2.. getDisplaySalesRepId()="+getDisplaySalesRepId() + " controlModel.getDisplaySalesRepId()="+controlModel.getDisplaySalesRepId());

        if (getDisplaySalesRepId() == null) {
            if (controlModel.getDisplaySalesRepId() != null)
                return false;
        } else if (!getDisplaySalesRepId().equals(controlModel.getDisplaySalesRepId()))
            return false;
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..3.. getManufacturerId()="+getManufacturerId() + " controlModel.getManufacturerId()="+controlModel.getManufacturerId());

        if (getManufacturerId() == null) {
            if (controlModel.getManufacturerId() != null)
                return false;
        } else if (!getManufacturerId().equals(controlModel.getManufacturerId()))
            return false;
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..4.. getPromoPeriodId()="+getPromoPeriodId() + " controlModel.getPromoPeriodId()="+controlModel.getPromoPeriodId());

        if (getPromoPeriodId() == null) {
            if (controlModel.getPromoPeriodId() != null)
                return false;
        } else if (!getPromoPeriodId().equals(controlModel.getPromoPeriodId()))
            return false;
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..5.. getPromoYear()="+getPromoYear()+ " controlModel.getPromoYear()="+controlModel.getPromoYear());

        if (getPromoYear() == null) {
            if (controlModel.getPromoYear() != null)
                return false;
        } else if (!getPromoYear().equals(controlModel.getPromoYear()))
            return false;
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..6.. getSku()="+getSku()+ " controlModel.getSku()="+controlModel.getSku());

        if (getSku() == null) {
            if (controlModel.getSku() != null)
                return false;
        } else if (!getSku().equals(controlModel.getSku()))
            return false;
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..7.. getStatusId()="+getStatusId()+ " controlModel.getStatusId()="+controlModel.getStatusId());

        if (getStatusId() == null) {
            if (controlModel.getStatusId() != null)
                return false;
        } else if (!getStatusId().equals(controlModel.getStatusId()))
            return false;
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..8.. getQtyForecast()="+getQtyForecast()+ " controlModel.getQtyForecast()="+controlModel.getQtyForecast());

        if (getQtyForecast() == null) {
            if (controlModel.getQtyForecast() != null && controlModel.getQtyForecast() >0)
                return false;
        } else if (!getQtyForecast().equals(controlModel.getQtyForecast()))
            return false;
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..9.. getActualQty()="+getActualQty()+ " controlModel.getActualQty()="+controlModel.getActualQty());

        if (getActualQty() == null) {
            if (controlModel.getActualQty() != null && controlModel.getActualQty() >0)
                return false;
        } else if (!getActualQty().equals(controlModel.getActualQty()))
            return false;
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..10.. getPackoutVendorId()="+getPackoutVendorId()+ " controlModel.getPackoutVendorId()="+controlModel.getPackoutVendorId());
        if (getPackoutVendorId() == null) {
            if (controlModel.getPackoutVendorId() != null)
                return false;
        } else if (!getPackoutVendorId().equals(controlModel.getPackoutVendorId()))
            return false;
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..11.. Ends");
          return true;
    }
	
	public boolean equalsDisplayGeneralInfo(DisplayDTO controlModel) {
        if (getCustomerDisplayNumber() == null) {
            if (controlModel.getCustomerDisplayNumber() != null)
                return false;
        } else if (!getCustomerDisplayNumber().equals(controlModel.getCustomerDisplayNumber()))
            return false;
        if (getCmProjectNumber() == null) {
            if (controlModel.getCmProjectNumber() != null)
                return false;
        } else if (!getCmProjectNumber().equals(controlModel.getCmProjectNumber()))
            return false;
        if (getCustomerId() == null) {
            if (controlModel.getCustomerId() != null)
                return false;
        } else if (!getCustomerId().equals(controlModel.getCustomerId()))
            return false;
        if (getCountryId() == null) {
            if (controlModel.getCountryId() != null)
                return false;
        } else if (!getCountryId().equals(controlModel.getCountryId()))
            return false;
        if (getDescription() == null) {
            if (controlModel.getDescription() != null)
                return false;
        } else if (!getDescription().equals(controlModel.getDescription()))
            return false;

        if (getDisplaySalesRepId() == null) {
            if (controlModel.getDisplaySalesRepId() != null)
                return false;
        } else if (!getDisplaySalesRepId().equals(controlModel.getDisplaySalesRepId()))
            return false;

        if (getDisplaySetupId() == null) {
            if (controlModel.getDisplaySetupId() != null)
                return false;
        } else if (!getDisplaySetupId().equals(controlModel.getDisplaySetupId()))
            return false;

        if (getManufacturerId() == null) {
            if (controlModel.getManufacturerId() != null)
                return false;
        } else if (!getManufacturerId().equals(controlModel.getManufacturerId()))
            return false;

        if (getPromoPeriodId() == null) {
            if (controlModel.getPromoPeriodId() != null)
                return false;
        } else if (!getPromoPeriodId().equals(controlModel.getPromoPeriodId()))
            return false;

        if (getPromoYear() == null) {
            if (controlModel.getPromoYear() != null)
                return false;
        } else if (!getPromoYear().equals(controlModel.getPromoYear()))
            return false;

        if (getSku() == null) {
            if (controlModel.getSku() != null)
                return false;
        } else if (!getSku().equals(controlModel.getSku()))
            return false;

        if (getStatusId() == null) {
            if (controlModel.getStatusId() != null)
                return false;
        } else if (!getStatusId().equals(controlModel.getStatusId()))
            return false;
        if (getStructureId() == null) {
            if (controlModel.getStructureId() != null)
                return false;
        } else if (!getStructureId().equals(controlModel.getStructureId()))
            return false;

        if (getRygStatusFlg() == null) {
            if (controlModel.getRygStatusFlg() != null)
                return false;
        } else if (!getRygStatusFlg().equals(controlModel.getRygStatusFlg()))
            return false;

        if (getApproverIds() == null) {
            if (controlModel.getApproverIds() != null)
                return false;
        } else if (!getApproverIds().equals(controlModel.getApproverIds()))
            return false;
        
        return true;
    }

	public void setApproverIds(Set<Long> approvers) {
		set(APPROVERS, approvers);
	}
	
	public Set<Long> getApproverIds() {
		return get(APPROVERS);
	}

}
