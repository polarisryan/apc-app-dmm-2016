package com.averydennison.dmm.app.client.models;

import java.util.Date;
import java.util.List;



import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;
import com.google.gwt.i18n.client.DateTimeFormat;

public class DisplayCostDTO extends BaseModel {
	
    /**
     * User: Mari
     * Date: Jul 27, 2010
     * Time: 9.25 AM
	 */
	public static final String DISPLAY_COST_ID="displayCostId";
	public static final String DISPLAY_ID="displayId";
	public static final String DISPLAY_MATERIAL_COST="displayMaterialCost";
	public static final String FULLFILMENT_COST="fullfilmentCost";
	public static final String CTP_COST="ctpCost";
	public static final String DIECUTTING_COST="dieCuttingCost";
	public static final String PRINTING_PLATE_COST="printingPlateCost";
	public static final String TOT_ARTWORK_COST="totalArtworkCost";
	public static final String SHIP_TEST_COST="shipTestCost";
	public static final String  PALLET_COST="palletCost";
	public static final String  MISC_COST="miscCost";
	public static final String  CORRUGATE_COST="corrugateCost";
	public static final String  OVERAGE_SCRAP_PCT="overageScrapPct";
	public static final String  CUSTOMER_PROGRAM_PCT="customerProgramPct";
/*	public static final String  CM_PROJECT_NUM="cmProjectNum";
*/	public static final String  SSCID_PROJECT_NUM="sscidProjectNum";
	public static final String  COST_CENTER="costCenter";
	public static final String  DT_CREATED="dtCreated";
	public static final String  DT_LASTCHANGED="dtLastChanged";
	public static final String  USER_CREATED="userCreated";
	public static final String  USER_LAST_CHANGED="userLastChanged";
	public static final String  EXPEDITED_FREIGHT_COST="expeditedFreightCost";
	public static final String  FINES="fines";
	public static final String  CA_DATE="caDate";
	public static final String  DISPLAY_SCENARIO_ID="displayScenarioId";
	public static final String  MDFPCT="mdfPct";
	public static final String  DISCOUNTPCT="discountPct";
	public static final String  TRADE_SALES ="tradeSalesChanged";
	
    
	public Long getDisplayCostId() {
		return get(DISPLAY_COST_ID);
	}
	public void setDisplayCostId(Long displayCostId) {
		set(DISPLAY_COST_ID,displayCostId);
	}
	public Long getDisplayId() {
		return get(DISPLAY_ID);
	}
	public void setDisplayId(Long displayId) {
		set(DISPLAY_ID,displayId);
	}
	public Long getDisplayScenarioId() {
		return get(DISPLAY_SCENARIO_ID);
	}
	public void setDisplayScenarioId(Long displayScenarioId) {
		set(DISPLAY_SCENARIO_ID,displayScenarioId);
	}
	public Float getDisplayMaterialCost() {
		return get(DISPLAY_MATERIAL_COST);
	}
	public void setDisplayMaterialCost(Float displayMaterialCost) {
		set(DISPLAY_MATERIAL_COST,displayMaterialCost);
	}
	public Float getFullfilmentCost() {
		return get(FULLFILMENT_COST);
	}
	public void setFullfilmentCost(Float fullfilmentCost) {
		set(FULLFILMENT_COST,fullfilmentCost );
	}
	public Float getCtpCost() {
		return get(CTP_COST);
	}
	public void setCtpCost(Float ctpCost) {
		set(CTP_COST, ctpCost);
	}
	public Float getDieCuttingCost() {
		return get(DIECUTTING_COST);
	}
	public void setDieCuttingCost(Float dieCuttingCost) {
		set(DIECUTTING_COST,dieCuttingCost);
	}
	public Float getPrintingPlateCost() {
		return get(PRINTING_PLATE_COST);
	}
	public void setPrintingPlateCost(Float printingPlateCost) {
		set(PRINTING_PLATE_COST,printingPlateCost);
	}
	public Float getTotalArtworkCost() {
		return get(TOT_ARTWORK_COST);
	}
	public void setTotalArtworkCost(Float totalArtworkCost) {
		set(TOT_ARTWORK_COST,totalArtworkCost);
	}
	public Float getShipTestCost() {
		return get(SHIP_TEST_COST);
	}
	public void setShipTestCost(Float shipTestCost) {
		set(SHIP_TEST_COST, shipTestCost);
	}
	public Float getPalletCost() {
		return get(PALLET_COST);
	}
	public void setPalletCost(Float palletCost) {
		set(PALLET_COST, palletCost);
	}
	public Float getMiscCost() {
		return get(MISC_COST);
	}
	public void setMiscCost(Float miscCost) {
		set(MISC_COST, miscCost);
	}
	public Float getCorrugateCost() {
		return get(CORRUGATE_COST);
	}
	public void setCorrugateCost(Float corrugateCost) {
		set(CORRUGATE_COST, corrugateCost);
	}
	public Long getOverageScrapPct() {
		return get(OVERAGE_SCRAP_PCT);
	}
	public void setOverageScrapPct(Long overageScrapPct) {
		set(OVERAGE_SCRAP_PCT, overageScrapPct);
	}
	public Float getCustomerProgramPct() {
		return get(CUSTOMER_PROGRAM_PCT);
	}
	public void setCustomerProgramPct(Float customerProgramPct) {
		set(CUSTOMER_PROGRAM_PCT, customerProgramPct);
	}
	public String getSscidProjectNum() {
		return get(SSCID_PROJECT_NUM);
	}
	public void setSscidProjectNum(String sscidProjectNum) {
		set(SSCID_PROJECT_NUM,sscidProjectNum);
	}
	public String getCostCenter() {
		return get(COST_CENTER);
	}
	public void setCostCenter(String costCenter) {
		set(COST_CENTER, costCenter);
	}
	public Date getDtCreated() {
		return get(DT_CREATED);
	}
	public void setDtCreated(Date dtCreated) {
		set(DT_CREATED, dtCreated);
	}
	public Date getDtLastChanged() {
		return get(DT_LASTCHANGED);
	}
	public void setDtLastChanged(Date dtLastChanged) {
		set(DT_LASTCHANGED, dtLastChanged);
	}
	public String getUserCreated() {
		return get(USER_CREATED);
	}
	public void setUserCreated(String userCreated) {
		set(USER_CREATED,userCreated);
	}
	public String getUserLastChanged() {
		return get(USER_LAST_CHANGED);
	}
	public void setUserLastChanged(String userLastChanged) {
		set(USER_LAST_CHANGED, userLastChanged);
	}
	public Float getExpeditedFreightCost() {
		return get(EXPEDITED_FREIGHT_COST);
	}
	public void setExpeditedFreightCost(Float expeditedFreightCost) {
		set(EXPEDITED_FREIGHT_COST, expeditedFreightCost);
	}
	public Float getFines() {
		return get(FINES);
	}
	public void setFines(Float fines) {
		set(FINES, fines);
	}
	
	
	public Float getMdfPct() {
		return get(MDFPCT);
	}
	public void setMdfPct(Float mdfPct) {
		set(MDFPCT, mdfPct);
	}
	
	public Float getDiscountPct() {
		return get(DISCOUNTPCT);
	}
	public void setDiscountPct(Float discountPct) {
		set(DISCOUNTPCT, discountPct);
	}
	
	
	public Date getCaDate() {
		return get(CA_DATE);
	}
	public void setCaDate(Date caDate) {
		set(CA_DATE, caDate);
	}
	
	public Float getTradeSales() {
		return get(TRADE_SALES);
	}
	public void setTradeSales(Float tradeSales) {
		set(TRADE_SALES, tradeSales);
	}
	
	@Override
	public void addChangeListener(ChangeListener... listener) {
		super.addChangeListener(listener);
	}

	@Override
	public void addChangeListener(List<ChangeListener> listeners) {
		super.addChangeListener(listeners);
	}

	@Override
	protected void fireEvent(int type, Model item) {
		super.fireEvent(type, item);
	}

	@Override
	protected void fireEvent(int type) {
		super.fireEvent(type);
	}

	@Override
	public void notify(ChangeEvent evt) {
		super.notify(evt);
	}

	@Override
	protected void notifyPropertyChanged(String name, Object value,
			Object oldValue) {
		super.notifyPropertyChanged(name, value, oldValue);
	}

	@Override
	public void removeChangeListener(ChangeListener... listener) {
		super.removeChangeListener(listener);
	}

	@Override
	public void removeChangeListeners() {
		super.removeChangeListeners();
	}
	
	public boolean equalsDisplayCostInfo(DisplayCostDTO controlModel) {
	    System.out.println("equalsDisplayCostInfo..1..getFullfilmentCost()="+getFullfilmentCost()+ " controlModel.getFullfilmentCost()="+controlModel.getFullfilmentCost());
        if (getFullfilmentCost() == null) {
            if (controlModel.getFullfilmentCost() != null && controlModel.getFullfilmentCost() >0.0)
                return false;
        } else if (!getFullfilmentCost().equals(controlModel.getFullfilmentCost()))
            return false;
        System.out.println("equalsDisplayCostInfo..2..getCtpCost()="+getCtpCost() + " controlModel.getCtpCost()="+controlModel.getCtpCost());
        if (getCtpCost() == null) {
            if (controlModel.getCtpCost() != null && controlModel.getCtpCost()>0.0)
                return false;
        } else if (!getCtpCost().equals(controlModel.getCtpCost()))
            return false;
        System.out.println("equalsDisplayCostInfo..3..getDieCuttingCost()="+getDieCuttingCost()+ " controlModel.getDieCuttingCost()="+controlModel.getDieCuttingCost());
        if (getDieCuttingCost() == null) {
            if (controlModel.getDieCuttingCost() != null && controlModel.getDieCuttingCost()>0.0)
                return false;
        } else if (!getDieCuttingCost().equals(controlModel.getDieCuttingCost()))
            return false;
        System.out.println("equalsDisplayCostInfo..4..getDieCuttingCost()="+getDieCuttingCost() + " controlModel.getPrintingPlateCost()="+controlModel.getPrintingPlateCost());
        if (getPrintingPlateCost() == null) {
            if (controlModel.getPrintingPlateCost() != null && controlModel.getPrintingPlateCost()>0.0)
                return false;
        } else if (!getPrintingPlateCost().equals(controlModel.getPrintingPlateCost()))
            return false;
        System.out.println("equalsDisplayCostInfo..5..getTotalArtworkCost()="+getTotalArtworkCost() + " controlModel.getTotalArtworkCost()="+controlModel.getTotalArtworkCost());
        if (getTotalArtworkCost() == null) {
            if (controlModel.getTotalArtworkCost() != null && controlModel.getTotalArtworkCost()>0.0)
                return false;
        } else if (!getTotalArtworkCost().equals(controlModel.getTotalArtworkCost()))
            return false;
        System.out.println("equalsDisplayCostInfo..6..getShipTestCost()="+getShipTestCost() + " controlModel.getShipTestCost()="+controlModel.getShipTestCost());
        if (getShipTestCost() == null) {
            if (controlModel.getShipTestCost() != null && controlModel.getShipTestCost()>0.0)
                return false;
        } else if (!getShipTestCost().equals(controlModel.getShipTestCost()))
            return false;
        System.out.println("equalsDisplayCostInfo..7..getPalletCost()="+getPalletCost() + " controlModel.getPalletCost()="+controlModel.getPalletCost());
        if (getPalletCost() == null) {
            if (controlModel.getPalletCost() != null && controlModel.getPalletCost()>0.0)
                return false;
        } else if (!getPalletCost().equals(controlModel.getPalletCost()))
            return false;
        System.out.println("equalsDisplayCostInfo..8..getMiscCost()="+getMiscCost() + " controlModel.getMiscCost()="+controlModel.getMiscCost());
        if (getMiscCost() == null) {
            if (controlModel.getMiscCost() != null && controlModel.getMiscCost()>0.0)
                return false;
        } else if (!getMiscCost().equals(controlModel.getMiscCost()))
            return false;
        System.out.println("equalsDisplayCostInfo..9..getCorrugateCost()="+getCorrugateCost()+" controlModel.getCorrugateCost()="+controlModel.getCorrugateCost());
        if (getCorrugateCost() == null) {
            if (controlModel.getCorrugateCost() != null && controlModel.getCorrugateCost()>0.0)
                return false;
        } else if (!getCorrugateCost().equals(controlModel.getCorrugateCost()))
            return false;
        System.out.println("equalsDisplayCostInfo..10..getOverageScrapPct()="+getOverageScrapPct()+ " controlModel.getOverageScrapPct():"+controlModel.getOverageScrapPct());
        if (getOverageScrapPct() == null) {
            if (controlModel.getOverageScrapPct() != null && controlModel.getOverageScrapPct()>0.0)
                return false;
        } else if (!getOverageScrapPct().equals(controlModel.getOverageScrapPct()))
            return false;
        System.out.println("equalsDisplayCostInfo..11..getCustomerProgramPct()="+getCustomerProgramPct()+" controlModel.getCustomerProgramPct()="+controlModel.getCustomerProgramPct());
        if (getCustomerProgramPct() == null) {
            if (controlModel.getCustomerProgramPct() != null && controlModel.getCustomerProgramPct()>0.0)
                return false;
        } else if (!getCustomerProgramPct().equals(controlModel.getCustomerProgramPct()))
            return false;
        System.out.println("equalsDisplayCostInfo..12..");
        System.out.println("equalsDisplayCostInfo..12.1.getExpeditedFreightCost()="+getExpeditedFreightCost() + " controlModel.getExpeditedFreightCost()="+controlModel.getExpeditedFreightCost());
        if (getExpeditedFreightCost() == null) {
            if (controlModel.getExpeditedFreightCost() != null && controlModel.getExpeditedFreightCost()>0.0)
                return false;
        } else if (!getExpeditedFreightCost().equals(controlModel.getExpeditedFreightCost()))
            return false;
        System.out.println("equalsDisplayCostInfo..13..getFines()="+getFines()+" controlModel.getFines()="+controlModel.getFines());
        if (getFines() == null) {
            if (controlModel.getFines() != null && controlModel.getFines()>0.0)
                return false;
        } else if (!getFines().equals(controlModel.getFines()))
        	 return false;
        System.out.println("equalsDisplayCostInfo..14..");
      /*  if (getCmProjectNum() == null) {
            if (controlModel.getCmProjectNum() != null)
                return false;
        } else if (!getCmProjectNum().equals(controlModel.getCmProjectNum()))
        	return false;*/
        System.out.println("equalsDisplayCostInfo..15..getCaDate()="+getCaDate() + " controlModel.getCaDate()="+controlModel.getCaDate());
        DateTimeFormat fmt = DateTimeFormat.getFormat("MM/dd/yyyy");

        if (getCaDate() == null) {
            if (controlModel.getCaDate() != null)
                return false;
        } else if (controlModel.getCaDate()==null){
        	return false;   
        } else if (!fmt.format(getCaDate()).equals(fmt.format(controlModel.getCaDate())))
            return false;
        System.out.println("getCaDate() and controlModel.getCaDate() are EQUALS");
        return true;
	}
	
}
