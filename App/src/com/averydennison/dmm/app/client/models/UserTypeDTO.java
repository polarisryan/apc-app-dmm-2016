package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class UserTypeDTO extends BaseModel {
	private static final long serialVersionUID = -6428813964809644704L;
	public static String USER_TYPE_ID = "userTypeId";
	public static String USER_TYPE_NAME = "userTypeName";
	public static String ACTIVE = "active";

	public UserTypeDTO() {
	}

	public Long getUserTypeId() {
		return get(USER_TYPE_ID);
	}

	public void setUserTypeId(Long userTypeId) {
		set(USER_TYPE_ID, userTypeId);
	}

	public String getUserTypeName() {
		return get(USER_TYPE_NAME);
	}

	public void setUserTypeName(String userTypeName) {
		set(USER_TYPE_NAME, userTypeName);
	}

	public void setActive(Boolean activeFlg) {
		set(ACTIVE, activeFlg);
	}

	public Boolean isActive() {
		return get(ACTIVE);
	}

	@Override
	public String toString() {
		return "UserTypeDTO [getUserTypeId()=" + getUserTypeId()
				+ ", getUserTypeName()=" + getUserTypeName()
				+ ", isActive()=" + isActive() + "]";
	}

}