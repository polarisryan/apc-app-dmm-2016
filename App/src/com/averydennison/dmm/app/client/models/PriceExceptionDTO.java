package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class PriceExceptionDTO extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String OUT_PE = "out_pe";
	
	public static final String KIT_COMPONENT_ID="kitComponentId";
	
	
	public Long getKitComponentId() {
		return get(KIT_COMPONENT_ID);
	}

	public void setKitComponentId(Long kitComponentId) {
		set(KIT_COMPONENT_ID,kitComponentId);
	}

	public Double getOut_pe() {
		return get(OUT_PE);
	}

	public void setOut_pe(Double outPe) {
		set(OUT_PE, outPe);
	}

}
