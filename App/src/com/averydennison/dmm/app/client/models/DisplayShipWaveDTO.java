package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Nov 12, 2009
 * Time: 11:49:48 AM
 */
public class DisplayShipWaveDTO extends BaseModel {
    public static final String DISPLAY_SHIP_WAVE_ID = "displayShipWaveId";
    public static final String DISPLAY_ID = "displayId";
    public static final String SHIP_DATE = "shipDate";
    public static final String QUANTITY = "quantity";
    public static final String MUST_ARRIVE_DATE = "mustArriveDate";
    public static final String DT_CREATED = "dtCreated";
    public static final String DT_LAST_CHANGED = "dtLastChanged";
    public static final String USER_CREATED = "userCreated";
    public static final String USER_LAST_CHANGED = "userLastChanged";
    public static final String SHIP_WAVE_NUM = "shipWaveNum";
    public static final String DESTINATION_FLG = "destinationFlg";
    public static final String PO_QTY = "poQty";
    public static final String DELETE_FLG = "deleteFlg";
    public static final String DIRTY_FLG = "dirtyFlg";

    public DisplayShipWaveDTO() {
    }

    public Long getDisplayShipWaveId() {
        return get(DISPLAY_SHIP_WAVE_ID);
    }

    public void setDisplayShipWaveId(Long displayShipWaveId) {
        set(DISPLAY_SHIP_WAVE_ID, displayShipWaveId);
    }

    public Long getDisplayId() {
        return get(DISPLAY_ID);
    }

    public void setDisplayId(Long displayId) {
        set(DISPLAY_ID, displayId);
    }

    public Date getShipDate() {
        return get(SHIP_DATE);
    }

    public void setShipDate(Date shipDate) {
        set(SHIP_DATE, shipDate);
    }

    public Long getQuantity() {
        if (get(QUANTITY) instanceof String) return Long.valueOf((String) get(QUANTITY));
        return get(QUANTITY);
    }

    public void setQuantity(Long quantity) {
        set(QUANTITY, quantity);
    }

    public Date getMustArriveDate() {
        return get(MUST_ARRIVE_DATE);
    }

    public void setMustArriveDate(Date mustArriveDate) {
        set(MUST_ARRIVE_DATE, mustArriveDate);
    }

    public Date getDtCreated() {
        return get(DT_CREATED);
    }

    public void setDtCreated(Date dtCreated) {
        set(DT_CREATED, dtCreated);
    }

    public Date getDtLastChanged() {
        return get(DT_LAST_CHANGED);
    }

    public void setDtLastChanged(Date dtLastChanged) {
        set(DT_LAST_CHANGED, dtLastChanged);
    }

    public String getUserCreated() {
        return get(USER_CREATED);
    }

    public void setUserCreated(String userCreated) {
        set(USER_CREATED, userCreated);
    }

    public String getUserLastChanged() {
        return get(USER_LAST_CHANGED);
    }

    public void setUserLastChanged(String userLastChanged) {
        set(USER_LAST_CHANGED, userLastChanged);
    }

    public void setShipWave(Integer n) {
        set(SHIP_WAVE_NUM, n);
    }

    public Integer getShipWaveNum() {
        if (get(SHIP_WAVE_NUM) instanceof String) return Integer.valueOf((String) get(SHIP_WAVE_NUM));
        return get(SHIP_WAVE_NUM);
    }

    public Boolean isDestinationFlg() {
        return get(DESTINATION_FLG);
    }

    public void setDestinationFlg(Boolean destinationFlg) {
        set(DESTINATION_FLG, destinationFlg);
    }

    public Boolean isDeleteFlg() {
        return get(DELETE_FLG);
    }

    public void setDeleteFlg(Boolean deleteFlg) {
        set(DELETE_FLG, deleteFlg);
    }

    public Boolean isDirtyFlg() {
        return get(DIRTY_FLG);
    }

    public void setDirtyFlg(Boolean dirtyFlg) {
        set(DIRTY_FLG, dirtyFlg);
    }

    public Long getPoQty() {
        return get(PO_QTY);
    }

    public void setPoQty(Long poQty) {
        set(PO_QTY, poQty);
    }

    public String toString() {
        return "DisplayShipWave{" +
                //"displayShipWaveId=" + getDisplayShipWaveId().toString() +
                ", displayId=" + getDisplayId().toString() +
                //", mustArriveDate=" + getMustArriveDate().toString() +
                ", quantity=" + getQuantity().toString() +
                ", shipWaveNum=" + getShipWaveNum() +
                //", dtCreated=" + getDtCreated().toString() +
                // ", dtLastChanged=" + getDtLastChanged().toString() +
                // ", userCreated='" + getUserCreated() + '\'' +
                // ", userLastChanged='" + getUserLastChanged() + '\'' +
//                ", displayPackoutsByDisplayShipWaveId=" + getDisplayPackoutsByDisplayShipWaveId() +
//                ", displayByDisplayId=" + displayByDisplayId +
                '}';
    }
}
