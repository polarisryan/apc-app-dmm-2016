package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class CustomerTypeDTO extends BaseModel {
	private static final long serialVersionUID = -6418813964809644704L;
	public static String CUSTOMER_TYPE_ID = "customerTypeId";
	public static String CUSTOMER_TYPE_NAME = "customerTypeName";
	public static String ACTIVE = "active";

	public CustomerTypeDTO() {
	}

	public Long getCustomerTypeId() {
		return get(CUSTOMER_TYPE_ID);
	}

	public void setCustomerTypeId(Long customerTypeId) {
		set(CUSTOMER_TYPE_ID, customerTypeId);
	}

	public String getCustomerTypeName() {
		return get(CUSTOMER_TYPE_NAME);
	}

	public void setCustomerTypeName(String customerTypeName) {
		set(CUSTOMER_TYPE_NAME, customerTypeName);
	}

	public void setActive(Boolean activeFlg) {
		set(ACTIVE, activeFlg);
	}

	public Boolean isActive() {
		return get(ACTIVE);
	}

	@Override
	public String toString() {
		return "CustomerTypeDTO [getCustomerTypeId()=" + getCustomerTypeId()
				+ ", getCustomerTypeName()=" + getCustomerTypeName()
				+ ", isActive()=" + isActive() + "]";
	}

}