package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Mari
 * Date: Jan 10, 2011
 * Time: 9:48:28 AM
 */
public class CountryDTO extends BaseModel {

    public static String COUNTRY_ID = "countryId";
    public static String COUNTRY_CODE = "countryCode";
    public static String COUNTRY_NAME = "countryName";
    public static String ACTIVE_FLG = "activeFlg";
    public static String ACTIVE_COUNTRY_FLG = "activeCountryFlg";

    public CountryDTO() {
    }

    public CountryDTO(Long countryId, String countryCode, String countryName, Boolean activeFlg, Boolean activeCountryFlg) {
        set(COUNTRY_ID, countryId);
        set(COUNTRY_CODE, countryCode);
        set(COUNTRY_NAME, countryName);
        set(ACTIVE_FLG, activeFlg);
        set(ACTIVE_COUNTRY_FLG, activeCountryFlg);
    }
    
    public String getCountryCode() {
        return get(COUNTRY_CODE);
    }

    public void setCountryCode(String countryCode) {
        set(COUNTRY_CODE, countryCode);
    }

    public Long getCountryId() {
        return get(COUNTRY_ID);
    }

    public void setCountryId(Long countryId) {
        set(COUNTRY_ID, countryId);
    }

    public String getCountryName() {
        return get(COUNTRY_NAME);
    }

    public void setCountryName(String countryName) {
        set(COUNTRY_NAME, countryName);
    }

    public Boolean isActiveFlg() {
        return get(ACTIVE_FLG);
    }

    public void setActiveFlg(Boolean activeFlg) {
        set(ACTIVE_FLG, activeFlg);
    }

    public Boolean isActiveCountryFlg() {
        return get(ACTIVE_COUNTRY_FLG);
    }

    public void setActiveCountryFlg(Boolean activeCountryFlg) {
        set(ACTIVE_COUNTRY_FLG, activeCountryFlg);
    }
    
    public String toString() {
        return "country{" +
                "countryId=" + this.getCountryId() +
                ", countryCode='" + getCountryCode() + '\'' +
                ", activeFlg='" + isActiveFlg() + '\'' +
                ", activeCountryFlg='" + isActiveCountryFlg() + '\'' +
                ", countryName='" + this.getCountryName() + '\'' +
                '}';
    }
}
