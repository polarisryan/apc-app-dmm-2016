package com.averydennison.dmm.app.client.models;

import com.averydennison.dmm.app.server.persistence.Customer;
import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello Date: Nov 5, 2009 Time: 1:30:00 PM
 */
public class CustomerDTO extends BaseModel {
	private static final long serialVersionUID = -6418803964809644704L;
	public static String CUSTOMER_ID = "customerId";
	public static String CUSTOMER_NAME = "customerName";
	public static String CUSTOMER_DESCRIPTION = "customerDescription";
	public static String CUSTOMER_PARENT_ID = "customerParentId";
	public static String CUSTOMER_PARENT_NAME = "customerParentName";
	public static String CUSTOMER_TYPE_ID = "customerTypeId";
	public static String CUSTOMER_TYPE_NAME = "customerTypeName";
	public static String CUSTOMER_CODE = "customerCode";
	public static String CUSTOMER_COUNTRY_ID = "countryId";
	public static String CUSTOMER_COUNTRY_NAME = "countryName";
	public static String CUSTOMER_CMA_ID = "customerCMAId";
	public static String CUSTOMER_CMA_NAME = "customerCMAName";	
	public static String COMPANY_CODE = "companyCode";

	public static String ACTIVE = "active";

	public CustomerDTO() {
	}

	public CustomerDTO(Long customerId, String customerName,
			String customerDescription) {
		set(CUSTOMER_ID, customerId);
		set(CUSTOMER_NAME, customerName);
		set(CUSTOMER_DESCRIPTION, customerDescription);
	}

	public Long getCustomerId() {
		return get(CUSTOMER_ID);
	}

	public void setCustomerId(Long customerId) {
		set(CUSTOMER_ID, customerId);
	}

	public String getCustomerName() {
		return get(CUSTOMER_NAME);
	}

	public void setCustomerName(String customerName) {
		set(CUSTOMER_NAME, customerName);
	}

	public String getCustomerDescription() {
		return get(CUSTOMER_DESCRIPTION);
	}

	public void setCustomerDescription(String customerDescription) {
		set(CUSTOMER_DESCRIPTION, customerDescription);
	}

	public String getCustomerCode() {
		return get(CUSTOMER_CODE);
	}

	public void setCustomerCode(String customerCode) {
		set(CUSTOMER_CODE, customerCode);
	}

	public String getCompanyCode() {
		return get(COMPANY_CODE);
	}

	public void setCompanyCode(String companyCode) {
		set(COMPANY_CODE, companyCode);
	}
	
	public Long getCustomerParentId() {
		return get(CUSTOMER_PARENT_ID);
	}

	public void setCustomerParentId(Long customerParentId) {
		set(CUSTOMER_PARENT_ID, customerParentId);
	}

	public String getCustomerParentName() {
		return get(CUSTOMER_PARENT_NAME);
	}

	public void setCustomerParentName(String customerParentName) {
		set(CUSTOMER_PARENT_NAME, customerParentName);
	}

	public Long getCustomerTypeId() {
		return get(CUSTOMER_TYPE_ID);
	}

	public void setCustomerTypeId(Long customerTypeId) {
		set(CUSTOMER_TYPE_ID, customerTypeId);
	}

	public String getCustomerTypeName() {
		return get(CUSTOMER_TYPE_NAME);
	}

	public void setCustomerTypeName(String customerTypeName) {
		set(CUSTOMER_TYPE_NAME, customerTypeName);
	}

	public Long getCustomerCountryId() {
		return get(CUSTOMER_COUNTRY_ID);
	}

	public void setCustomerCountryId(Long countryId) {
		set(CUSTOMER_COUNTRY_ID, countryId);
	}

	public String getCustomerCountryName() {
		return get(CUSTOMER_COUNTRY_NAME);
	}

	public void setCustomerCountryName(String countryName) {
		set(CUSTOMER_COUNTRY_NAME, countryName);
	}
	
	public Long getCustomerCMAId() {
		return get(CUSTOMER_CMA_ID);
	}

	public void setCustomerCMAId(Long cmaId) {
		set(CUSTOMER_CMA_ID, cmaId);
	}

	public String getCustomerCMAName() {
		return get(CUSTOMER_CMA_NAME);
	}

	public void setCustomerCMAName(String cmaName) {
		set(CUSTOMER_CMA_NAME, cmaName);
	}
	
	public void setActive(Boolean activeFlg) {
		set(ACTIVE, activeFlg);
	}

	public Boolean isActive() {
		return get(ACTIVE);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CustomerDTO customer = (CustomerDTO) o;

		if (getCustomerId() != null ? !getCustomerId().equals(customer.getCustomerId())
				: customer.getCustomerId() != null)
			return false;

		return true;
	}
	
	@Override
	public int hashCode() {
		int result = getCustomerId() != null ? getCustomerId().hashCode() : 0;
		return result;
	}
	
	@Override
	public String toString() {
		return "Customer{" + "customerId=" + get(CUSTOMER_ID)
				+ ", customerName='" + get(CUSTOMER_NAME) + '\''
				+ ", customerDescription='" + get(CUSTOMER_DESCRIPTION) + '\''
				+ '}';
	}
}