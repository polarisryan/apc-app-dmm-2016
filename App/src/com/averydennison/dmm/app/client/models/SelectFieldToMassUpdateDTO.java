package com.averydennison.dmm.app.client.models;


public class SelectFieldToMassUpdateDTO {
	
	private boolean bCustomer;
	private boolean bStructure;
	private boolean bProjectManager;
	private boolean bStatus;
	private boolean bCorrugateMfgt;
	private boolean bPromoPeriod;
	private boolean bSkuMixFinal;
	private boolean bPriceAvailCompleteOnDt;
	private boolean bSampleToCustomerBy;
	private boolean bFinalArtworkPODueDt;
	private boolean bPIMCompletedonDt;
	private boolean bPricingAdminValidationDt;
	private boolean bProjectConfidenceLevel;
	private boolean bInitRenderingEstQuote;
	private boolean bStructApproved;
	private boolean bOrdersReceived;
	private boolean bComments;
	private int noOfFieldsForMassUpdate=4;
	
	public boolean isbCustomer() {
		return bCustomer;
	}
	public void setbCustomer(boolean bCustomer) {
		this.bCustomer = bCustomer;
	}
	public boolean isbStructure() {
		return bStructure;
	}
	public void setbStructure(boolean bStructure) {
		this.bStructure = bStructure;
	}
	public boolean isbProjectManager() {
		return bProjectManager;
	}
	public void setbProjectManager(boolean bProjectManager) {
		this.bProjectManager = bProjectManager;
	}
	public boolean isbStatus() {
		return bStatus;
	}
	public void setbStatus(boolean bStatus) {
		this.bStatus = bStatus;
	}
	public boolean isbCorrugateMfgt() {
		return bCorrugateMfgt;
	}
	public void setbCorrugateMfgt(boolean bCorrugateMfgt) {
		this.bCorrugateMfgt = bCorrugateMfgt;
	}
	public boolean isbPromoPeriod() {
		return bPromoPeriod;
	}
	public void setbPromoPeriod(boolean bPromoPeriod) {
		this.bPromoPeriod = bPromoPeriod;
	}
	public boolean isbSkuMixFinal() {
		return bSkuMixFinal;
	}
	public void setbSkuMixFinal(boolean bSkuMixFinal) {
		this.bSkuMixFinal = bSkuMixFinal;
	}
	public boolean isbPriceAvailCompleteOnDt() {
		return bPriceAvailCompleteOnDt;
	}
	public void setbPriceAvailCompleteOnDt(boolean bPriceAvailCompleteOnDt) {
		this.bPriceAvailCompleteOnDt = bPriceAvailCompleteOnDt;
	}
	public boolean isbSampleToCustomerBy() {
		return bSampleToCustomerBy;
	}
	public void setbSampleToCustomerBy(boolean bSampleToCustomerBy) {
		this.bSampleToCustomerBy = bSampleToCustomerBy;
	}
	public boolean isbFinalArtworkPODueDt() {
		return bFinalArtworkPODueDt;
	}
	public void setbFinalArtworkPODueDt(boolean bFinalArtworkPODueDt) {
		this.bFinalArtworkPODueDt = bFinalArtworkPODueDt;
	}
	public boolean isbPIMCompletedonDt() {
		return bPIMCompletedonDt;
	}
	public void setbPIMCompletedonDt(boolean bPIMCompletedonDt) {
		this.bPIMCompletedonDt = bPIMCompletedonDt;
	}
	public boolean isbPricingAdminValidationDt() {
		return bPricingAdminValidationDt;
	}
	public void setbPricingAdminValidationDt(boolean bPricingAdminValidationDt) {
		this.bPricingAdminValidationDt = bPricingAdminValidationDt;
	}
	public boolean isbProjectConfidenceLevel() {
		return bProjectConfidenceLevel;
	}
	public void setbProjectConfidenceLevel(boolean bProjectConfidenceLevel) {
		this.bProjectConfidenceLevel = bProjectConfidenceLevel;
	}
	public boolean isbInitRenderingEstQuote() {
		return bInitRenderingEstQuote;
	}
	public void setbInitRenderingEstQuote(boolean bInitRenderingEstQuote) {
		this.bInitRenderingEstQuote = bInitRenderingEstQuote;
	}
	public boolean isbStructApproved() {
		return bStructApproved;
	}
	public void setbStructApproved(boolean bStructApproved) {
		this.bStructApproved = bStructApproved;
	}
	public boolean isbOrdersReceived() {
		return bOrdersReceived;
	}
	public void setbOrdersReceived(boolean bOrdersReceived) {
		this.bOrdersReceived = bOrdersReceived;
	}
	public boolean isbComments() {
		return bComments;
	}
	public void setbComments(boolean bComments) {
		this.bComments = bComments;
	}
	
	public void setNoOfFieldsForMassUpdate(int noOfFieldsForMassUpdate){
		this.noOfFieldsForMassUpdate = noOfFieldsForMassUpdate;
	}
	
	public int getNoOfFieldsForMassUpdate(){
		return noOfFieldsForMassUpdate;
	}
	
	public void countNoOfFieldsForMassUpdate(SelectFieldToMassUpdateDTO selectFieldToMassUpdate){
		int iCnt=4;
		if(selectFieldToMassUpdate.isbCustomer())
			iCnt++;
		if(selectFieldToMassUpdate.isbStructure())
			iCnt++;
		if(selectFieldToMassUpdate.isbProjectManager())
			iCnt++;
		if(selectFieldToMassUpdate.isbStatus())
			iCnt++;
		if(selectFieldToMassUpdate.isbCorrugateMfgt())
			iCnt++;
		if(selectFieldToMassUpdate.isbPromoPeriod())
			iCnt++;
		if(selectFieldToMassUpdate.isbSkuMixFinal())
			iCnt++;
		if(selectFieldToMassUpdate.isbPriceAvailCompleteOnDt())
			iCnt++;
		if(selectFieldToMassUpdate.isbSampleToCustomerBy())
			iCnt++;
		if(selectFieldToMassUpdate.isbFinalArtworkPODueDt())
			iCnt++;
		if(selectFieldToMassUpdate.isbPIMCompletedonDt())
			iCnt++;
		if(selectFieldToMassUpdate.isbPricingAdminValidationDt())
			iCnt++;
		if(selectFieldToMassUpdate.isbProjectConfidenceLevel())
			iCnt++;
		if(selectFieldToMassUpdate.isbInitRenderingEstQuote())
			iCnt++;
		if(selectFieldToMassUpdate.isbStructApproved())
			iCnt++;
		if(selectFieldToMassUpdate.isbOrdersReceived())
			iCnt++;
		if(selectFieldToMassUpdate.isbComments())
			iCnt++;
		
		System.out.println("countNoOfFieldsForMassUpdate: count" + iCnt);
		setNoOfFieldsForMassUpdate(iCnt);
	}
	
	
	
	

}
