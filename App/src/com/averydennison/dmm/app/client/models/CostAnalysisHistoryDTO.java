package com.averydennison.dmm.app.client.models;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;

public class CostAnalysisHistoryDTO extends BaseModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1942781875582331811L;

	public static final String HISTORY="history";

	
	public CostAnalysisHistoryDTO() {
	       
	 }
	 public String getHistory() {
	        return get(HISTORY);
	    }

	 public void setHistory(String history) {
	        set(HISTORY, history);
	 }
	 
	 @Override
		public void addChangeListener(ChangeListener... listener) {
			super.addChangeListener(listener);
		}

		@Override
		public void addChangeListener(List<ChangeListener> listeners) {
			super.addChangeListener(listeners);
		}

		@Override
		protected void fireEvent(int type, Model item) {
			super.fireEvent(type, item);
		}

		@Override
		protected void fireEvent(int type) {
			super.fireEvent(type);
		}

		@Override
		public void notify(ChangeEvent evt) {
			super.notify(evt);
		}

		@Override
		protected void notifyPropertyChanged(String name, Object value,
				Object oldValue) {
			super.notifyPropertyChanged(name, value, oldValue);
		}

		@Override
		public void removeChangeListener(ChangeListener... listener) {
			super.removeChangeListener(listener);
		}

		@Override
		public void removeChangeListeners() {
			super.removeChangeListeners();
		}

}
