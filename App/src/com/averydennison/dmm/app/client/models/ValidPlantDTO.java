package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 3:09:37 PM
 */
public class ValidPlantDTO extends BaseModel {
    public static String PLANT_CODE = "plantCode";
    public static String PLANT_DESC = "plantDesc";
    public static String ACTIVE_FLG = "activeFlg";
    public static String ACTIVE_ON_FINANCE_FLG = "activeOnFinanceFlg";

    public ValidPlantDTO() {
    }

    public ValidPlantDTO(String plantCode, String plantDesc, Boolean activeFlg, Boolean activeOnFinanceFlg) {
        set(PLANT_CODE, plantCode);
        set(PLANT_DESC, plantDesc);
        set(ACTIVE_FLG, activeFlg);
        set(ACTIVE_ON_FINANCE_FLG, activeOnFinanceFlg);
    }

    public String getPlantCode() {
        return get(PLANT_CODE);
    }

    public void setPlantCode(String plantCode) {
        set(PLANT_CODE, plantCode);
    }

    public String getPlantDesc() {
        return get(PLANT_DESC);
    }

    public void setPlantDesc(String plantDesc) {
        set(PLANT_DESC, plantDesc);
    }

    public Boolean isActiveFlg() {
        return get(ACTIVE_FLG);
    }

    public void setActiveFlg(Boolean isActiveFlg) {
        set(ACTIVE_FLG, isActiveFlg);
    }

    public Boolean isActiveOnFinanceFlg() {
        return get(ACTIVE_ON_FINANCE_FLG);
    }

    public void setActiveOnFinanceFlg(Boolean isActiveOnFinanceFlg) {
        set(ACTIVE_ON_FINANCE_FLG, isActiveOnFinanceFlg);
    }

    public String toString() {
        return get(PLANT_DESC);
    }

}
