package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class CustomerPctDTO extends BaseModel {
	
	public static final String CUSTOMER_ID= "customerId";
	public static final String BINDERS="binders";
	public static final String CARDSPAPER="cardsPaper";
	public static final String DIVIDERS="dividers";
	public static final String LABELS= "labels";
	public static final String OANDP="oAndP";
	public static final String WRITINGINSTRUMENTS="writingInstruments";
	
	public Long getCustomerId() {
		return get(CUSTOMER_ID);
	}
	public void setCustomerId(Long customerId) {
		set(CUSTOMER_ID,customerId);
	}
	public Float getBinders() {
		return get(BINDERS);
	}
	public void setBinders(Float binders) {
		set(BINDERS, binders);
	}
	public Float getCardsPaper() {
		return get(CARDSPAPER);
	}
	public void setCardsPaper(Float cardsPaper) {
		set(CARDSPAPER,cardsPaper);
	}
	public Float getDividers() {
		return get(DIVIDERS);
	}
	public void setDividers(Float dividers) {
		set(DIVIDERS,dividers);
	}
	public Float getLabels() {
		return get(LABELS);
	}
	public void setLabels(Float labels) {
		set(LABELS,labels);
	}
	public Float getoAndP() {
		return get(OANDP);
	}
	public void setoAndP(Float oAndP) {
		set(OANDP,oAndP);
	}
	public Float getWritingInstruments() {
		return get(WRITINGINSTRUMENTS);
	}
	public void setWritingInstruments(Float writingInstruments) {
		set(WRITINGINSTRUMENTS,writingInstruments);
	}
}
