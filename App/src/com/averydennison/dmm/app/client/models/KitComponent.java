package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Oct 23, 2009
 * Time: 2:50:23 AM
 */
public class KitComponent extends BaseModel {

    public KitComponent() {
    }

    public KitComponent(String sku, String description, String bu, Integer vc, Boolean pv, String vcs, Boolean pbc,
                        Boolean i2, String plnr, String plant, String pi, Integer rcp, Integer invRqt, Integer tl,
                        Integer qpf, Integer f, Integer qps) {
        set("sku", sku);
        set("description", description);
        set("bu", bu);
        set("vc", vc);
        set("pv", pv);
        set("vcs", vcs);
        set("pbc", pbc);
        set("i2", i2);
        set("plnr", plnr);
        set("plant", plant);
        set("pi", pi);
        set("rcp", rcp);
        set("invRqt", invRqt);
        set("tl", tl);
        set("qpf", qpf);
        set("f", f);
        set("qps", qps);
    }

    public String getSku() {
        return get("sku");
    }

    public void setSku(String sku) {
        set("sku", sku);
    }

    public String getDescription() {
        return get("description");
    }

    public void setDescription(String description) {
        set("description", description);
    }

    public String getBu() {
        return get("bu");
    }

    public void setBu(String bu) {
        set("bu", bu);
    }

    public Integer getVc() {
        return get("vc");
    }

    public void setVc(Integer vc) {
        set("vc", vc);
    }

    public Boolean getPv() {
        return get("pv");
    }

    public void setPv(Boolean pv) {
        set("pv", pv);
    }

    public String getVcs() {
        return get("vcs");
    }

    public void setVcs(String vcs) {
        set("vcs", vcs);
    }

    public Boolean getPbc() {
        return get("pbc");
    }

    public void setPbc(Boolean pbc) {
        set("pbc", pbc);
    }

    public Boolean getI2() {
        return get("i2");
    }

    public void setI2(Boolean i2) {
        set("i2", i2);
    }

    public String getPlnr() {
        return get("plnr");
    }

    public void setPlnr(String plnr) {
        set("plnr", plnr);
    }

    public String getPlant() {
        return get("plant");
    }

    public void setPlant(String plant) {
        set("plant", plant);
    }

    public String getPl() {
        return get("pi");
    }

    public void setPl(String pi) {
        set("pi", pi);
    }

    public Integer getRcp() {
        return get("rcp");
    }

    public void setRcp(Integer rcp) {
        set("rcp", rcp);
    }

    public Integer getInvRqt() {
        return get("invRqt");
    }

    public void setInvRqt(Integer invRqt) {
        set("invRqt", invRqt);
    }

    public Integer getTl() {
        return get("tl");
    }

    public void setTl(Integer tl) {
        set("tl", tl);
    }

    public Integer getQpf() {
        return get("qpf");
    }

    public void setQpf(Integer qpf) {
        set("qpf", qpf);
    }

    public Integer getF() {
        return get("f");
    }

    public void setF(Integer f) {
        set("f", f);
    }

    public Integer getQps() {
        return get("qps");
    }

    public void setQps(Integer qps) {
        set("qps", qps);
    }
}