package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Nov 6, 2009
 * Time: 11:32:21 AM
 */
public class Destination extends BaseModel {
    public static final String NAME = "name";

    public Destination() {
    }

    public Destination(String name) {
        set(NAME, name);
    }

    public String getName() {
        return get(NAME);
    }

    public void setName(String name) {
        set(NAME, name);
    }
}
