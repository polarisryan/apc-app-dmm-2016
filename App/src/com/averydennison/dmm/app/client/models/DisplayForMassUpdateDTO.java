package com.averydennison.dmm.app.client.models;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.Model;
import com.google.gwt.i18n.client.DateTimeFormat;

public class DisplayForMassUpdateDTO extends BaseModel {
	
	private static final long serialVersionUID = 6643826035949658170L;

	public static final String DISPLAY_ID = "displayId";
    public static final String SKU = "sku";
    public static final String DESCRIPTION = "description";
    public static final String CUSTOMER_ID = "customerId";
    public static final String STATUS_ID = "statusId";
    public static final String PROMO_PERIOD_ID = "promoPeriodId";
    public static final String PROMO_YEAR = "promoYear";
    public static final String DISPLAY_SALES_REP_ID = "displaySalesRepId";
    public static final String SKU_MIX_FINAL_FLG = "skuMixFinalFlg";
    public static final String STRUCTURE_ID = "structureId";

    public static final String PRICE_AVAILABILITY_DATE = "priceAvailabilityDate";
    public static final String SENT_SAMPLE_DATE = "sentSampleDate";
    public static final String FINAL_ARTWORK_DATE = "finalArtworkDate";
    public static final String PIM_COMPLETED_DATE = "pimCompletedDate";
    public static final String PRICING_ADMIN_DATE = "pricingAdminDate";
    public static final String INITIAL_RENDERING = "initialRenderingFlg";
    public static final String STRUCTURE_APPROVED = "structureApprovedFlg";
    public static final String ORDER_IN = "orderInFlg";
    public static final String COMMENTS = "comments";
    
    public static final String CUSTOMER_NAME="customerName";
    public static final String STATUS_NAME="statusName";
    public static final String PROMO_PERIOD_NAME="promoPeriodName";
    public static final String FULL_NAME="fullName";
    public static final String STRUCTURE_NAME="structureName";
    public static final String DT_LAST_CHANGED = "dtLastChanged";
    public static final String DISPLAY_SALES_REP_BY_DISPLAY_SALES_REP_ID = "displaySalesRepByDisplaySalesRepId";
    public static final String DISPLAY_SALES_REP_BY_FULL_NAME = "displaySalesRepByFulName";
    public static final String STATUS_BY_STATUS_ID = "statusByStatusId";
    public static final String STATUS_BY_NAME = "statusByStatusName";
    public static final String CUSTOMER_BY_CUSTOMER_ID = "customerByCustomerId";
    public static final String CUSTOMER_BY_NAME = "customerByName";
    public static final String STRUCTURE_BY_STRUCTURE_ID = "structureByStructureId";
    public static final String STRUCTURE_BY_NAME = "structureByName";
    public static final String DISPLAY_SHIP_WAVES_BY_DISPLAY_ID = "displayShipWavesByDisplayId";
    public static final String DISPLAY_SHIP_WAVES_BY_SHIP_DATE = "displayShipWavesByShipDate";
    public static final String PROMO_PERIOD_BY_PROMO_PERIOD_ID = "promoPeriodByPromoPeriodId";
    public static final String PROMO_PERIOD_BY_PROMO_PERIOD_NAME = "promoPeriodByPromoPeriodName";
    public static final String CUSTOMER_DISPLAY_NUMBER = "customerDisplayNumber";
    public static final String PROJECT_CONFIDENCE_LEVEL="projectLevel";
    public static final String CORRUGATE_NAME="corrugateName";
    public static final String CORRUGATE_ID="corrugateId";
    
    public static final String SKU_MIX_FINAL_TF = "skuMixFinal";
    public static final String INITIAL_RENDERING_TF = "initialRendering";
    public static final String STRUCTURE_APPROVED_TF = "structureApproved";
    public static final String ORDERS_RECEIVED_TF = "ordersReceived";
    
    
    public DisplayForMassUpdateDTO() {
    	
    }

    public static DisplayForMassUpdateDTO CopyDisplayForMassUpdateDTO(DisplayForMassUpdateDTO displayForMassUpdateDTO) {
    	DisplayForMassUpdateDTO newInstance = new DisplayForMassUpdateDTO();
        newInstance.setSku(displayForMassUpdateDTO.getSku());
    	newInstance.setDescription(displayForMassUpdateDTO.getDescription());
    	newInstance.setCustomerId(displayForMassUpdateDTO.getCustomerId());
    	newInstance.setStatusId(displayForMassUpdateDTO.getStatusId());
    	newInstance.setPromoPeriodId(displayForMassUpdateDTO.getPromoPeriodId());
    	newInstance.setPromoYear(displayForMassUpdateDTO.getPromoYear());
    	newInstance.setSkuMixFinalFlg(displayForMassUpdateDTO.isSkuMixFinalFlg());
    	newInstance.setCorrugateId(displayForMassUpdateDTO.getCorrugateId());
        newInstance.setStructureId(displayForMassUpdateDTO.getStructureId());
        newInstance.setPriceAvailabilityDate(displayForMassUpdateDTO.getPriceAvailabilityDate());
        newInstance.setSentSampleDate(displayForMassUpdateDTO.getSentSampleDate());
        newInstance.setFinalArtworkDate(displayForMassUpdateDTO.getFinalArtworkDate());
        newInstance.setPimCompletedDate(displayForMassUpdateDTO.getPimCompletedDate());
        newInstance.setPricingAdminDate(displayForMassUpdateDTO.getPricingAdminDate());
        newInstance.setIntialRendering(displayForMassUpdateDTO.getInitialRendering());
        newInstance.setStructureApproved(displayForMassUpdateDTO.getStructureApproved());
        newInstance.setOrderIn(displayForMassUpdateDTO.getOrderIn());
        
        return newInstance;
        
    }

    public String getSku() {
        return get(SKU);
    }

    public void setSku(String sku) {
        set(SKU, sku);
    }

    public String getDescription() {
        return get(DESCRIPTION);
    }

    public void setDescription(String description) {
        set(DESCRIPTION, description);
    }

    public Long getCustomerId() {
        return get(CUSTOMER_ID);
    }

    public void setCustomerId(Long customerId) {
        set(CUSTOMER_ID, customerId);
    }

    public Long getStatusId() {
        return get(STATUS_ID);
    }

    public void setStatusId(Long statusId) {
        set(STATUS_ID, statusId);
    }

    public Long getPromoPeriodId() {
        return get(PROMO_PERIOD_ID);
    }

    public void setPromoPeriodId(Long promoPeriodId) {
        set(PROMO_PERIOD_ID, promoPeriodId);
    }

   
    public void setStructureId(Long structureId) {
        set(STRUCTURE_ID, structureId);
    }

    public Long getStructureId() {
        return get(STRUCTURE_ID);
    }

    public Long getDisplaySalesRepId() {
        return get(DISPLAY_SALES_REP_ID);
    }

    public void setDisplaySalesRepId(Long displaySalesRepId) {
        set(DISPLAY_SALES_REP_ID, displaySalesRepId);
    }

    public Short getProjectLevel() {
        return get(PROJECT_CONFIDENCE_LEVEL);
    }

    public void setProjectLevel(Short ProjectLevel) {
        set(PROJECT_CONFIDENCE_LEVEL, ProjectLevel);
    }
    
    public String isSkuMixFinalFlg() {
        return get(SKU_MIX_FINAL_FLG);
    }

    public void setSkuMixFinalFlg(String skuMixFinalFlg) {
        set(SKU_MIX_FINAL_FLG, skuMixFinalFlg);
    }

    public Long getDisplayId() {
        return get(DISPLAY_ID);
    }

    public void setDisplayId(Long displayId) {
        set(DISPLAY_ID, displayId);
    }

    public Date getPriceAvailabilityDate() {
        return get(PRICE_AVAILABILITY_DATE);
    }

    public void setPriceAvailabilityDate(Date priceAvailabilityDate) {
        set(PRICE_AVAILABILITY_DATE, priceAvailabilityDate);
    }

    public Date getSentSampleDate() {
        return get(SENT_SAMPLE_DATE);
    }

    public void setSentSampleDate(Date sentSampleDate) {
        set(SENT_SAMPLE_DATE, sentSampleDate);
    }

    public Date getFinalArtworkDate() {
        return get(FINAL_ARTWORK_DATE);
    }

    public void setFinalArtworkDate(Date finalArtworkDate) {
        set(FINAL_ARTWORK_DATE, finalArtworkDate);
    }

    public Date getPimCompletedDate() {
        return get(PIM_COMPLETED_DATE);
    }

    public void setPimCompletedDate(Date pimCompletedDate) {
        set(PIM_COMPLETED_DATE, pimCompletedDate);
    }

    public Date getPricingAdminDate() {
        return get(PRICING_ADMIN_DATE);
    }

    public void setPricingAdminDate(Date pricingAdminDate) {
        set(PRICING_ADMIN_DATE, pricingAdminDate);
    }

    public String getInitialRendering() {
        return get(INITIAL_RENDERING);
    }

    public void setIntialRendering(String initialRendering) {
        set(INITIAL_RENDERING, initialRendering);
    }

    public String getStructureApproved() {
        return get(STRUCTURE_APPROVED);
    }

    public void setStructureApproved(String structureApproved) {
        set(STRUCTURE_APPROVED, structureApproved);
    }

    public String getOrderIn() {
        return get(ORDER_IN);
    }

    public void setOrderIn(String orderIn) {
        set(ORDER_IN, orderIn);
    }

    public String getComments() {
        return get(COMMENTS);
    }

    public void setComments(String comments) {
        set(COMMENTS, comments);
    }

    public DisplaySalesRepDTO getDisplaySalesRepByDisplaySalesRepId() {
        return get(DISPLAY_SALES_REP_BY_DISPLAY_SALES_REP_ID);
    }

    public void setDisplaySalesRepByDisplaySalesRepId(DisplaySalesRepDTO displaySalesRepByDisplaySalesRepId) {
        if (displaySalesRepByDisplaySalesRepId != null) {
            set(DISPLAY_SALES_REP_BY_DISPLAY_SALES_REP_ID, displaySalesRepByDisplaySalesRepId);
            if (displaySalesRepByDisplaySalesRepId.getFullName() != null) {
                set(DISPLAY_SALES_REP_BY_FULL_NAME, displaySalesRepByDisplaySalesRepId.getFullName());
            }
        }
    }

    public StatusDTO getStatusByStatusId() {
        return get(STATUS_BY_STATUS_ID);
    }

    public void setStatusByStatusId(StatusDTO statusByStatusId) {
        if (statusByStatusId != null) {
            set(STATUS_BY_STATUS_ID, statusByStatusId);
            set(STATUS_BY_NAME, statusByStatusId.getStatusName());
        }
    }

    public CustomerDTO getCustomerByCustomerId() {
        return get(CUSTOMER_BY_CUSTOMER_ID);
    }

    public void setCustomerByCustomerId(CustomerDTO customerByCustomerId) {
        if (customerByCustomerId != null) {
            set(CUSTOMER_BY_CUSTOMER_ID, customerByCustomerId);
            set(CUSTOMER_BY_NAME, customerByCustomerId.getCustomerName());
        }
    }

    public StructureDTO getStructureByStructureId() {
        return get(STRUCTURE_BY_STRUCTURE_ID);
    }

    public void setStructureByStructureId(StructureDTO structureByStructureId) {
        if (structureByStructureId != null) {
            set(STRUCTURE_BY_STRUCTURE_ID, structureByStructureId);
            set(STRUCTURE_BY_NAME, structureByStructureId.getStructureName());
        }
    }

    public Collection<DisplayShipWaveDTO> getDisplayShipWavesByDisplayId() {
        return get(DISPLAY_SHIP_WAVES_BY_DISPLAY_ID);
    }

    public void setDisplayShipWavesByDisplayId(Collection<DisplayShipWaveDTO> displayShipWavesByDisplayId) {
        if (displayShipWavesByDisplayId != null) {
            set(DISPLAY_SHIP_WAVES_BY_DISPLAY_ID, displayShipWavesByDisplayId);
        }

        if (displayShipWavesByDisplayId != null && !displayShipWavesByDisplayId.isEmpty()) {
            Iterator i = displayShipWavesByDisplayId.iterator();
            if (i.hasNext()) {
                DisplayShipWaveDTO dsw = (DisplayShipWaveDTO) i.next();
                set(DISPLAY_SHIP_WAVES_BY_SHIP_DATE, dsw.getMustArriveDate());
            }
        }
    }

    public PromoPeriodDTO getPromoPeriodByPromoPeriodId() {
        return get(PROMO_PERIOD_BY_PROMO_PERIOD_ID);
    }

    public void setPromoPeriodByPromoPeriodId(PromoPeriodDTO promoPeriodByPromoPeriodId) {
        if (promoPeriodByPromoPeriodId != null) {
            set(PROMO_PERIOD_BY_PROMO_PERIOD_ID, promoPeriodByPromoPeriodId);
            set(PROMO_PERIOD_BY_PROMO_PERIOD_NAME, promoPeriodByPromoPeriodId.getPromoPeriodName());
        }
    }

    @Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    public void notify(ChangeEvent evt) {
        super.notify(evt);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value, Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }

	public void setPromoYear(Long promoYear) {
		set(PROMO_YEAR, promoYear);
	}
	
	public Long getPromoYear(){
		return get(PROMO_YEAR);
	}
	
    public Date getDtLastChanged() {
        return get(DT_LAST_CHANGED);
    }

    public void setDtLastChanged(Date dtLastChanged) {
        set(DT_LAST_CHANGED, dtLastChanged);
    }

	
	public String getCustomerName() {
		return get(CUSTOMER_NAME);
	}

	public void setCustomerName(String customerName) {
		set(CUSTOMER_NAME, customerName);
	}

	public String getStatusName() {
		return get(STATUS_NAME);
	}

	public void setStatusName(String statusName) {
		set(STATUS_NAME, statusName);
	}

	public String getPromoPeriodName() {
		return get(PROMO_PERIOD_NAME);
	}

	public void setPromoPeriodName(String promoPeriodName) {
		set(PROMO_PERIOD_NAME, promoPeriodName);
	}

	public String getFullName() {
		return get(FULL_NAME);
	}

	public void setFullName(String fullName) {
		set(FULL_NAME, fullName);
	}

	public String getStructureName() {
		return get(STRUCTURE_NAME);
	}

	public void setStructureName(String structureName) {
		set(STRUCTURE_NAME, structureName);
	}
	
	
	 public Long getCorrugateId() {
	        return get(CORRUGATE_ID);
	 }

	 public void setCorrugateId(Long corrugateId) {
	       set(CORRUGATE_ID, corrugateId);
	 }

	 public String getCorrugateName() {
	        return get(CORRUGATE_NAME);
	 }

	 public void setCorrugateName(String corrugateName) {
	       set(CORRUGATE_NAME, corrugateName);
	 }

	 public void setSkuMixFinalTF(Boolean skuMixFinal) {
	    set(SKU_MIX_FINAL_TF, skuMixFinal);
	 }
	 public void setInitialRenderingTF(Boolean initialRendering) {
		set(INITIAL_RENDERING_TF, initialRendering);		 
	 }
	 public void setStructureApprovedTF(Boolean structureApproved) {
		set(STRUCTURE_APPROVED_TF, structureApproved);		 
	 }
	 public void setOrdersReceivedTF(Boolean ordersReceived) {
		set(ORDERS_RECEIVED_TF, ordersReceived);		 
	 }
	 public Boolean isSkuMixFinalTF() {
		 return get(SKU_MIX_FINAL_TF);
	 }
	 public Boolean isInitialRenderingTF() {
		 return get(INITIAL_RENDERING_TF);
	 }
	 public Boolean isStrucureApprovedTF() {
		 return get(STRUCTURE_APPROVED_TF);
	 }
	 public Boolean isOrdersReceivedTF() {
		 return get(ORDERS_RECEIVED_TF);
	 }
	 
	public boolean equalsDisplayGeneralInfo(DisplayForMassUpdateDTO controlModel) {
        if (getCustomerId() == null) {
            if (controlModel.getCustomerId() != null)
                return false;
        } else if (!getCustomerId().equals(controlModel.getCustomerId()))
            return false;
        if (getDescription() == null) {
            if (controlModel.getDescription() != null)
                return false;
        } else if (!getDescription().equals(controlModel.getDescription()))
            return false;
        if (getDisplaySalesRepId() == null) {
            if (controlModel.getDisplaySalesRepId() != null)
                return false;
        } else if (!getDisplaySalesRepId().equals(controlModel.getDisplaySalesRepId()))
            return false;
        if (getPromoPeriodId() == null) {
            if (controlModel.getPromoPeriodId() != null)
                return false;
        } else if (!getPromoPeriodId().equals(controlModel.getPromoPeriodId()))
            return false;
        if (getPromoYear() == null) {
            if (controlModel.getPromoYear() != null)
                return false;
        } else if (!getPromoYear().equals(controlModel.getPromoYear()))
            return false;
        if (getSku() == null) {
            if (controlModel.getSku() != null)
                return false;
        } else if (!getSku().equals(controlModel.getSku()))
            return false;
        if (getStatusId() == null) {
            if (controlModel.getStatusId() != null)
                return false;
        } else if (!getStatusId().equals(controlModel.getStatusId()))
            return false;
        if (getStructureId() == null) {
            if (controlModel.getStructureId() != null)
                return false;
        } else if (!getStructureId().equals(controlModel.getStructureId()))
            return false;
        
        DateTimeFormat fmt = DateTimeFormat.getFormat("MM/dd/yyyy");
        if (getComments() == null) {
            if (controlModel.getComments() != null)
                return false;
        } else if (!getComments().equals(controlModel.getComments()))
            return false;
        if (getFinalArtworkDate() == null) {
            if (controlModel.getFinalArtworkDate() != null)
                return false;
        } else if (!fmt.format(getFinalArtworkDate()).equals(fmt.format(controlModel.getFinalArtworkDate())))
            return false;
        if (getInitialRendering() == null) {
            if (controlModel.getInitialRendering() != null)
                return false;
        } else if (!getInitialRendering().equals(controlModel.getInitialRendering()))
            return false;
        if (getOrderIn() == null) {
            if (controlModel.getOrderIn() != null)
                return false;
        } else if (!getOrderIn().equals(controlModel.getOrderIn()))
            return false;
        if (getPimCompletedDate() == null) {
            if (controlModel.getPimCompletedDate() != null)
                return false;
        } else if (!fmt.format(getPimCompletedDate()).equals(fmt.format(controlModel.getPimCompletedDate())))
            return false;
        if (getPriceAvailabilityDate() == null) {
            if (controlModel.getPriceAvailabilityDate() != null)
                return false;
        } else if (!fmt.format(getPriceAvailabilityDate()).equals(fmt.format(controlModel.getPriceAvailabilityDate())))
            return false;
        if (getSentSampleDate() == null) {
            if (controlModel.getSentSampleDate() != null)
                return false;
        } else if (!fmt.format(getSentSampleDate()).equals(fmt.format(controlModel.getSentSampleDate())))
            return false;
        if (getPricingAdminDate() == null) {
            if (controlModel.getPricingAdminDate() != null)
                return false;
        } else if (!fmt.format(getPricingAdminDate()).equals(fmt.format(controlModel.getPricingAdminDate())))
            return false;
        if (getStructureApproved() == null) {
            if (controlModel.getStructureApproved() != null)
                return false;
        } else if (!getStructureApproved().equals(controlModel.getStructureApproved()))
            return false;
        return true;
    }
}
