package com.averydennison.dmm.app.client.models;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;

public class DisplayLogisticsDTO extends BaseModel {
    public static final String DISPLAY_LOGISTICS_ID = "displayLogisticsId";
    public static final String DISPLAY_ID = "displayId";
    public static final String DISPLAY_HEIGHT = "displayHeight";
    public static final String DISPLAY_LENGTH = "displayLength";
    public static final String DISPLAY_WEIGHT = "displayWeight";
    public static final String DISPLAY_WIDTH = "displayWidth";
    public static final String DISPLAYS_PER_LAYER = "displaysPerLayer";
    public static final String DOUBLE_STACK_FLG = "doubleStackFlg";
    public static final String DT_CREATED = "dtCreated";
    public static final String DT_LAST_CHANGED = "dtLastChanged";
    public static final String LAYERS_PER_PALLET = "layersPerPallet";
    public static final String PALLETS_PER_TRAILER = "palletsPerTrailer";
    public static final String SERIAL_SHIP_FLG = "serialShipFlg";
    public static final String SPECIAL_LABEL_RQT = "specialLabelRqt";
    public static final String USER_CREATED = "userCreated";
    public static final String USER_LAST_CHANGED = "userLastChanged";

    public DisplayLogisticsDTO() {
    }

    public static DisplayLogisticsDTO DisplayLogisticsInfoDTO(DisplayLogisticsDTO dliDTO) {
        DisplayLogisticsDTO newInstance = new DisplayLogisticsDTO();
        newInstance.setDisplayId(dliDTO.getDisplayId());
        newInstance.setDisplayHeight(dliDTO.getDisplayHeight());
        newInstance.setDisplayLength(dliDTO.getDisplayLength());
        newInstance.setDisplayWidth(dliDTO.getDisplayWidth());
        newInstance.setDisplayWeight(dliDTO.getDisplayWeight());
        newInstance.setDisplayPerLayer(dliDTO.getDisplayPerLayer());
        newInstance.setLayersPerPallet(dliDTO.getLayersPerPallet());
        newInstance.setPalletsPerTrailer(dliDTO.getPalletsPerTrailer());
        newInstance.setDoubleStackFlg(dliDTO.isDoubleStackFlg());
        newInstance.setSpecialLabetRqt(dliDTO.getSpecialLabetRqt());
        newInstance.setSerialShipFlg(dliDTO.isSerialShipFlg());
        newInstance.setDtCreated(dliDTO.getDtCreated());
        newInstance.setDtLastChanged(dliDTO.getDtLastChanged());
        newInstance.setUserCreated(dliDTO.getUserCreated());
        newInstance.setUserLastChanged(dliDTO.getUserLastChanged());
        return newInstance;
    }

    public Long getDisplayLogisticsId() {
        return get(DISPLAY_LOGISTICS_ID);
    }

    public void setDisplayLogisticsId(Long displayLogisticsId) {
        set(DISPLAY_LOGISTICS_ID, displayLogisticsId);
    }

    public Long getDisplayId() {
        return get(DISPLAY_ID);
    }

    public void setDisplayId(Long displayId) {
        set(DISPLAY_ID, displayId);
    }

    public Float getDisplayHeight() {
        return get(DISPLAY_HEIGHT);
    }

    public void setDisplayHeight(Float displayHeight) {
        set(DISPLAY_HEIGHT, displayHeight);
    }

    public Float getDisplayLength() {
        return get(DISPLAY_LENGTH);
    }

    public void setDisplayLength(Float displayLength) {
        set(DISPLAY_LENGTH, displayLength);
    }

    public Float getDisplayWidth() {
        return get(DISPLAY_WIDTH);
    }

    public void setDisplayWidth(Float displayWidth) {
        set(DISPLAY_WIDTH, displayWidth);
    }

    public Float getDisplayWeight() {
        return get(DISPLAY_WEIGHT);
    }

    public void setDisplayWeight(Float displayWeight) {
        set(DISPLAY_WEIGHT, displayWeight);
    }

    public Long getDisplayPerLayer() {
        return get(DISPLAYS_PER_LAYER);
    }

    public void setDisplayPerLayer(Long displayPerLayer) {
        set(DISPLAYS_PER_LAYER, displayPerLayer);
    }

    public Long getLayersPerPallet() {
        return get(LAYERS_PER_PALLET);
    }

    public void setLayersPerPallet(Long layersPerPallet) {
        set(LAYERS_PER_PALLET, layersPerPallet);
    }

    public Long getPalletsPerTrailer() {
        return get(PALLETS_PER_TRAILER);
    }

    public void setPalletsPerTrailer(Long palletsPerTrailer) {
        set(PALLETS_PER_TRAILER, palletsPerTrailer);
    }

    public Boolean isDoubleStackFlg() {
        return get(DOUBLE_STACK_FLG);
    }

    public void setDoubleStackFlg(Boolean doubleStackFlg) {
        set(DOUBLE_STACK_FLG, doubleStackFlg);
    }

    public String getSpecialLabetRqt() {
        return get(SPECIAL_LABEL_RQT);
    }

    public void setSpecialLabetRqt(String specialLabetRqt) {
        set(SPECIAL_LABEL_RQT, specialLabetRqt);
    }

    public Boolean isSerialShipFlg() {
        return get(SERIAL_SHIP_FLG);
    }

    public void setSerialShipFlg(Boolean serialShipFlg) {
        set(SERIAL_SHIP_FLG, serialShipFlg);
    }

    public Date getDtCreated() {
        return get(DT_CREATED);
    }

    public void setDtCreated(Date dtCreated) {
        set(DT_CREATED, dtCreated);
    }

    public Date getDtLastChanged() {
        return get(DT_LAST_CHANGED);
    }

    public void setDtLastChanged(Date dtLastChanged) {
        set(DT_LAST_CHANGED, dtLastChanged);
    }

    public String getUserCreated() {
        return get(USER_CREATED);
    }

    public void setUserCreated(String userCreated) {
        set(USER_CREATED, userCreated);
    }

    public String getUserLastChanged() {
        return get(USER_LAST_CHANGED);
    }

    public void setUserLastChanged(String userLastChanged) {
        set(USER_LAST_CHANGED, userLastChanged);
    }

    @Override
    public void addChangeListener(ChangeListener... listener) {
        super.addChangeListener(listener);
    }

    @Override
    public void addChangeListener(List<ChangeListener> listeners) {
        super.addChangeListener(listeners);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    public void notify(ChangeEvent evt) {
        super.notify(evt);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value,
                                         Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }

    @Override
    public void removeChangeListener(ChangeListener... listener) {
        super.removeChangeListener(listener);
    }

    @Override
    public void removeChangeListeners() {
        super.removeChangeListeners();
    }
    
    public boolean equalsDisplayLogisticsInfo(DisplayLogisticsDTO other) {
        if (getDisplayHeight() == null) {
            if (other.getDisplayHeight() != null)
                return false;
        } else if (!getDisplayHeight().equals(other.getDisplayHeight()))
            return false;
        if (getDisplayLength() == null) {
            if (other.getDisplayLength() != null)
                return false;
        } else if (!getDisplayLength().equals(other.getDisplayLength()))
            return false;
        if (getDisplayPerLayer() == null) {
            if (other.getDisplayPerLayer() != null)
                return false;
        } else if (!getDisplayPerLayer().equals(other.getDisplayPerLayer()))
            return false;
        if (getDisplayWeight() == null) {
            if (other.getDisplayWeight() != null)
                return false;
        } else if (!getDisplayWeight().equals(other.getDisplayWeight()))
            return false;
        if (getDisplayWidth() == null) {
            if (other.getDisplayWidth() != null)
                return false;
        } else if (!getDisplayWidth().equals(other.getDisplayWidth()))
            return false;
        if (isDoubleStackFlg() == null) {
            if (other.isDoubleStackFlg() != null)
                return false;
        } else if (!isDoubleStackFlg().equals(other.isDoubleStackFlg()))
            return false;
        if (getLayersPerPallet() == null) {
            if (other.getLayersPerPallet() != null)
                return false;
        } else if (!getLayersPerPallet().equals(other.getLayersPerPallet()))
            return false;
        if (getPalletsPerTrailer() == null) {
            if (other.getPalletsPerTrailer() != null)
                return false;
        } else if (!getPalletsPerTrailer().equals(other.getPalletsPerTrailer()))
            return false;
        if (isSerialShipFlg() == null) {
            if (other.isSerialShipFlg() != null)
                return false;
        } else if (!isSerialShipFlg().equals(other.isSerialShipFlg()))
            return false;
        if (getSpecialLabetRqt() == null) {
            if (other.getSpecialLabetRqt() != null)
                return false;
        } else if (!getSpecialLabetRqt().equals(other.getSpecialLabetRqt()))
            return false;
        return true;
    }
}

