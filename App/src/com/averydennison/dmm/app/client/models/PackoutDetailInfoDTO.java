package com.averydennison.dmm.app.client.models;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * User: Spart Arguello
 * Date: Dec 28, 2009
 * Time: 1:59:03 PM
 */
public class PackoutDetailInfoDTO implements Serializable {
    Map<Long, DisplayDcDTO> dcMap;
    Map<Long, DisplayPackoutDTO> packoutMap;
    List<DisplayShipWaveDTO> shipWaves;

    public PackoutDetailInfoDTO() {
    }

    public Map<Long, DisplayDcDTO> getDcMap() {
        return dcMap;
    }

    public void setDcMap(Map<Long, DisplayDcDTO> dcMap) {
        this.dcMap = dcMap;
    }

    public Map<Long, DisplayPackoutDTO> getPackoutMap() {
        return packoutMap;
    }

    public void setPackoutMap(Map<Long, DisplayPackoutDTO> packoutMap) {
        this.packoutMap = packoutMap;
    }

    public List<DisplayShipWaveDTO> getShipWaves() {
        return shipWaves;
    }

    public void setShipWaves(List<DisplayShipWaveDTO> shipWaves) {
        this.shipWaves = shipWaves;
    }
}
