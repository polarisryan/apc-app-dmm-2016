package com.averydennison.dmm.app.client.models;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;

public class CostPriceDTO extends BaseModel {
    public static final String PRICE_DESC = "priceDesc";
    public static final String UNIT_PRICE = "unitPrice";
    public static final String TOTAL_PRICE = "totalPrice";

    public CostPriceDTO() {
    }

    public CostPriceDTO(String priceDesc, Float unitPrice, Long qtyPerDisplay) {
        Float totalPrice = 0f;
    	set(PRICE_DESC, priceDesc);
        set(UNIT_PRICE, unitPrice);
        if (qtyPerDisplay != null && unitPrice != null) 
        	totalPrice = unitPrice * qtyPerDisplay;
        set(TOTAL_PRICE, totalPrice);
    }
    
    public static CostPriceDTO costPriceDTO(CostPriceDTO cpDTO) {
        CostPriceDTO newInstance = new CostPriceDTO();
        
        newInstance.setPriceDesc(cpDTO.getPriceDesc());
        newInstance.setUnitPrice(cpDTO.getUnitPrice());
        newInstance.setTotalPrice(cpDTO.getTotalPrice());

        return newInstance;
    }

    public void updateTotal(Long qtyPerDisplay) {
        Float totalPrice = 0f;
    	Float unitPrice = get(UNIT_PRICE);
        if (qtyPerDisplay != null && unitPrice != null) 
        	totalPrice = unitPrice * qtyPerDisplay;
        set(TOTAL_PRICE, totalPrice);
    }
    
    public String getPriceDesc() {
        return get(PRICE_DESC);
    }

    public void setPriceDesc(String priceDesc) {
        set(PRICE_DESC, priceDesc);
    }

    public Float getUnitPrice() {
    	return get(UNIT_PRICE);
    }

    public void setUnitPrice(Float unitPrice) {
        set(UNIT_PRICE, unitPrice);
    }

    public Float getTotalPrice() {
    	return get(TOTAL_PRICE);
    }

    public void setTotalPrice(Float totalPrice) {
        set(TOTAL_PRICE, totalPrice);
    }

    
    @Override
    public void addChangeListener(ChangeListener... listener) {
        super.addChangeListener(listener);
    }

    @Override
    public void addChangeListener(List<ChangeListener> listeners) {
        super.addChangeListener(listeners);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    public void notify(ChangeEvent evt) {
        super.notify(evt);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value,
                                         Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }

    @Override
    public void removeChangeListener(ChangeListener... listener) {
        super.removeChangeListener(listener);
    }

    @Override
    public void removeChangeListeners() {
        super.removeChangeListeners();
    }

}

