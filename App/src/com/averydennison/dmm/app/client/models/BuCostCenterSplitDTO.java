package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class BuCostCenterSplitDTO extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String  BU_DESC="buDesc";
	public static final String  BU_CODE="buCode";
	public static final String  NET_SALES="netSales";
	public static final String  VM_PCT="VmPct";
	public static final String  PCT_OF_COST="pctOfCost";
	public static final String  BU_SPLIT="buSplit";
	public static final String  TOTAL_ACTUAL_INVOICE_PERBU="totalActualInvoicePerBu";
	public static final String  TOTAL_NET_VMAMOUNT_PERBU="totalNetVmAmountPerBu"; 
	public static final String  INVOICE_WITH_PROGRAM="invoiceWithProgram";
	public static final String  TOTAL_ACTUAL_INVOICE_WITH_PROGRAM="totalActualinvoiceWithProgram";
	public String getBuCode() {
		return get(BU_CODE);
	}
	public void setBuCode(String buCode) {
		set(BU_CODE,buCode);
	}
	public String getBuDesc() {
		return get(BU_DESC);
	}
	public void setBuDesc(String buDesc) {
		set(BU_DESC,buDesc);
	}
	public Double getNetSales() {
		return get(NET_SALES);
	}
	public void setNetSales(Double netSales) {
		set(NET_SALES,netSales);
	}
	public Double getVmPct() {
		return get(VM_PCT);
	}
	public void setVmPct(Double vmPct) {
		set(VM_PCT,vmPct);
	}
	public Double getPctOfCost() {
		return get(PCT_OF_COST);
	}
	public void setPctOfCost(Double pctOfCost) {
		set(PCT_OF_COST, pctOfCost);
	}
	public Double getBuSplit() {
		return get(BU_SPLIT);
	}
	public void setBuSplit(Double buSplit) {
		set(BU_SPLIT, buSplit);
	}
	public Double getTotalActualInvoicePerBu() {
		return get(TOTAL_ACTUAL_INVOICE_PERBU);
	}
	public void setTotalActualInvoicePerBu(Double totalActualInvoicePerBu) {
		set(TOTAL_ACTUAL_INVOICE_PERBU, totalActualInvoicePerBu);
	}
	public void setTotalActualInvoiceWithProgram(Double totalActualinvoiceWithProgram) {
		set(TOTAL_ACTUAL_INVOICE_WITH_PROGRAM, totalActualinvoiceWithProgram);
	}
	public Double getTotalActualInvoiceWithProgram() {
		return get(TOTAL_ACTUAL_INVOICE_WITH_PROGRAM);
	}
	
	public Double getTotalNetVmAmountPerBu() {
		return get(TOTAL_NET_VMAMOUNT_PERBU);
	}
	public void setTotalNetVmAmountPerBu(Double totalNetVmAmountPerBu) {
		set(TOTAL_NET_VMAMOUNT_PERBU, totalNetVmAmountPerBu);
	}
}
