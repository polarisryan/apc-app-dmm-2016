package com.averydennison.dmm.app.client.models;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;

public class KitComponentDTO extends BaseModel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String KIT_COMPONENT_ID = "kitComponentId";
    public static final String DISPLAY_ID = "displayId";
    public static final String SKU_ID = "skuId";
    public static final String QTY_PER_FACING = "qtyPerFacing";
    public static final String NUMBER_FACINGS = "numberFacings";
    public static final String PLANT_BREAK_CASE_FLG = "plantBreakCaseFlg";
    public static final String I2_FORECAST_FLG = "i2ForecastFlg";
    public static final String REG_CASE_PACK = "regCasePack";
    public static final String INVOICE_UNIT_PRICE = "invoiceUnitPrice";
    public static final String COGS_UNIT_PRICE = "cogsUnitPrice";
    public static final String SEQUENCE_NUM = "sequenceNum";
    public static final String SOURCE_PIM_FLG = "sourcePimFlg";
    public static final String SKU = "sku";
    public static final String PRODUCT_NAME = "productName";
    public static final String BU_DESC = "buDesc";
    public static final String VERSION_NO = "versionNo";
    public static final String PROMO_FLG = "promoFlg";
    public static final String BREAK_CASE = "breakCase";
    public static final String KIT_VERSION_CODE = "kitVersionCode";
    public static final String PBC_VERSION = "pbcVersion";
    public static final String FINISHED_GOODS_SKU = "finishedGoodsSku";
    public static final String FINISHED_GOODS_QTY = "finishedGoodsQty";
    public static final String DT_CREATED = "dtCreated";
    public static final String DT_LAST_CHANGED = "dtLastChanged";
    public static final String USER_CREATED = "userCreated";
    public static final String USER_LAST_CHANGED = "userLastChanged";
    public static final String QTY_PER_DISPLAY = "qtyPerDisplay";
    public static final String INVENTORY_REQUIREMENT = "inventoryReq";
    public static final String PRICE_EXCEPTION = "priceException";
    public static final String BENCHMARK_PER_UNIT = "benchMarkPerUnit";
    public static final String PE_RECALC_DATE="peReCalcDate";
    public static final String DISPLAY_SCENARIO_ID="displayScenarioId";
    public static final String EXCESS_INVT_DOLLAR = "excessInvtDollar";
    public static final String EXCESS_INVT_PCT="excessInvtPct";
    public static final String IN_PROG_Y_N="inProgYNId";
    public static final String EXCESS_INVT_TYPEID="excessInventoryTypeId";
    public static final String EXCESS_INVT_TYPENAME="excessInvtTypeName";
    public static final String OVERRIDE_EXCEPTION_PCT="overrideExceptionPct";
    public static final String PROG_PRCNT="progPrcnt";

    public KitComponentDTO() {
    }

    public static KitComponentDTO KitComponentDTO(KitComponentDTO kcDTO) {
        KitComponentDTO newInstance = new KitComponentDTO();
        newInstance.setKitComponentId(kcDTO.getKitComponentId());
        newInstance.setDisplayId(kcDTO.getDisplayId());
        newInstance.setSkuId(kcDTO.getSkuId());
        newInstance.setQtyPerFacing(kcDTO.getQtyPerFacing());
        newInstance.setNumberFacings(kcDTO.getNumberFacings());
        newInstance.setPlantBreakCaseFlg(kcDTO.isPlantBreakCaseFlg());
        newInstance.setI2ForecastFlg(kcDTO.isI2ForecastFlg());
        newInstance.setRegCasePack(kcDTO.getRegCasePack());
        newInstance.setInvoiceUnitPrice(kcDTO.getInvoiceUnitPrice());
        newInstance.setCogsUnitPrice(kcDTO.getCogsUnitPrice());
        newInstance.setSequenceNum(kcDTO.getSequenceNum());
        newInstance.setSourcePimFlg(kcDTO.isSourcePimFlg());
        newInstance.setSku(kcDTO.getSku());
        newInstance.setProductName(kcDTO.getProductName());
        newInstance.setBuDesc(kcDTO.getBuDesc());
        newInstance.setVersionNo(kcDTO.getVersionNo());
        newInstance.setPromoFlg(kcDTO.isPromoFlg());
        newInstance.setBreakCase(kcDTO.getBreakCase());
        newInstance.setKitVersionCode(kcDTO.getKitVersionCode());
        newInstance.setPbcVersion(kcDTO.getPbcVersion());
        newInstance.setFinishedGoodsSku(kcDTO.getFinishedGoodsSku());
        newInstance.setFinishedGoodsQty(kcDTO.getFinishedGoodsQty());
        newInstance.setDtCreated(kcDTO.getDtCreated());
        newInstance.setDtLastChanged(kcDTO.getDtLastChanged());
        newInstance.setUserCreated(kcDTO.getUserCreated());
        newInstance.setUserLastChanged(kcDTO.getUserLastChanged());
        newInstance.setPriceException(kcDTO.getPriceException());
        newInstance.setBenchMarkPerUnit(kcDTO.getBenchMarkPerUnit());
        
        newInstance.setExcessInvtDollar(kcDTO.getExcessInvtDollar());
        newInstance.setExcessInvtPct(kcDTO.getExcessInvtPct());
        newInstance.setInPogYn(kcDTO.getInPogYn());
        newInstance.setOverrideExceptionPct(kcDTO.getOverrideExceptionPct());
        newInstance.setProgPrcnt(kcDTO.getProgPrcnt());
        return newInstance;
    }

 
    public Long getKitComponentId() {
		return get(KIT_COMPONENT_ID);
	}

	public void setKitComponentId(Long kitComponentId) {
		set(KIT_COMPONENT_ID, kitComponentId);
	}

	public Long getDisplayId() {
		return get(DISPLAY_ID);
	}

	public void setDisplayId(Long displayId) {
		set(DISPLAY_ID, displayId);
	}

	public Long getSkuId() {
		return get(SKU_ID);
	}

	public void setSkuId(Long skuId) {
		set(SKU_ID, skuId);
	}

	public Long getQtyPerFacing() {
		return get(QTY_PER_FACING);
	}

	public void setQtyPerFacing(Long qtyPerFacing) {
		set(QTY_PER_FACING, qtyPerFacing);
	}

	public Long getNumberFacings() {
		return get(NUMBER_FACINGS);
	}

	public void setNumberFacings(Long numberFacings) {
		set(NUMBER_FACINGS, numberFacings);
	}

	public Boolean isPlantBreakCaseFlg() {
		return get(PLANT_BREAK_CASE_FLG);
	}

	public void setPlantBreakCaseFlg(Boolean plantBreakCaseFlg) {
		set(PLANT_BREAK_CASE_FLG, plantBreakCaseFlg);
	}

	public Boolean isI2ForecastFlg() {
		return get(I2_FORECAST_FLG);
	}

	public void setI2ForecastFlg(Boolean i2ForecastFlg) {
		set(I2_FORECAST_FLG, i2ForecastFlg);
	}

	public Long getRegCasePack() {
		return get(REG_CASE_PACK);
	}

	public void setRegCasePack(Long regCasePack) {
		set(REG_CASE_PACK, regCasePack);
	}

	public Float getInvoiceUnitPrice() {
		return get(INVOICE_UNIT_PRICE);
	}

	public void setInvoiceUnitPrice(Float invoiceUnitPrice) {
		set(INVOICE_UNIT_PRICE, invoiceUnitPrice);
	}

	public Float getBenchMarkPerUnit() {
		return get(BENCHMARK_PER_UNIT);
	}

	public void setBenchMarkPerUnit(Float benchMarkPerUnit) {
		set(BENCHMARK_PER_UNIT, benchMarkPerUnit);
	}
    
	public Float getExcessInvtDollar() {
		return get(EXCESS_INVT_DOLLAR);
	}

	public void setExcessInvtDollar(Float excessInvtDollar) {
		set(EXCESS_INVT_DOLLAR, excessInvtDollar);
	}
	
	public Float getExcessInvtPct() {
		return get(EXCESS_INVT_PCT);
	}
	
	public void setExcessInvtPct(Float excessInvtPct) {
		set(EXCESS_INVT_PCT, excessInvtPct);
	}

	public Boolean getInPogYn() {
		return get(IN_PROG_Y_N);
	}
	
	public void setInPogYn(Boolean InPogYn) {
		set(IN_PROG_Y_N, InPogYn);
	}
	
	
	public Long getSequenceNum() {
		return get(SEQUENCE_NUM);
	}

	public void setSequenceNum(Long sequenceNum) {
		set(SEQUENCE_NUM, sequenceNum);
	}

	public Boolean isSourcePimFlg() {
		return get(SOURCE_PIM_FLG);
	}

	public void setSourcePimFlg(Boolean sourcePimFlg) {
		set(SOURCE_PIM_FLG, sourcePimFlg);
	}

	public String getSku() {
		return get(SKU);
	}

	public void setSku(String sku) {
		set(SKU, sku);
	}

	public String getProductName() {
		return get(PRODUCT_NAME);
	}

	public void setProductName(String productName) {
		set(PRODUCT_NAME, productName);
	}

	public String getBuDesc() {
		return get(BU_DESC);
	}

	public void setBuDesc(String buDesc) {
		set(BU_DESC, buDesc);
	}

	public String getVersionNo() {
		return get(VERSION_NO);
	}

	public void setVersionNo(String versionNo) {
		set(VERSION_NO, versionNo);
	}

	public Boolean isPromoFlg() {
		return get(PROMO_FLG);
	}

	public void setPromoFlg(Boolean promoFlg) {
		set(PROMO_FLG, promoFlg);
	}

	public String getBreakCase() {
		return get(BREAK_CASE);
	}

	public void setBreakCase(String breakCase) {
		set(BREAK_CASE, breakCase);
	}

	public String getKitVersionCode() {
		return get(KIT_VERSION_CODE);
	}

	public void setKitVersionCode(String kitVersionCode) {
		set(KIT_VERSION_CODE, kitVersionCode);
	}

	public String getPbcVersion() {
		return get(PBC_VERSION);
	}

	public void setPbcVersion(String pbcVersion) {
		set(PBC_VERSION, pbcVersion);
	}

	public String getFinishedGoodsSku() {
		return get(FINISHED_GOODS_SKU);
	}

	public void setFinishedGoodsSku(String finishedGoodsSku) {
		set(FINISHED_GOODS_SKU, finishedGoodsSku);
	}

	public Long getFinishedGoodsQty() {
		return get(FINISHED_GOODS_QTY);
	}

	public void setFinishedGoodsQty(Long finishedGoodsQty) {
		set(FINISHED_GOODS_QTY, finishedGoodsQty);
	}

	public Long getQtyPerDisplay() {
		return get(QTY_PER_DISPLAY);
	}

	public void setQtyPerDisplay(Long qtyPerDisplay) {
		set(QTY_PER_DISPLAY, qtyPerDisplay);
	}
	
	public Date getPeReCalcDate() {
		return get(PE_RECALC_DATE);
	}

	public void setPeReCalcDate(Date peReCalcDate) {
		set(PE_RECALC_DATE, peReCalcDate);
	}	
	
	public Long getInventoryRequirement() {
		return get(INVENTORY_REQUIREMENT);
	}

	public void setInventoryRequirement(Long inventoryRequirement) {
		set(INVENTORY_REQUIREMENT, inventoryRequirement);
	}	
	
	public Long getDisplayScenarioId() {
		return get(DISPLAY_SCENARIO_ID);
	}
	public void setDisplayScenarioId(Long displayScenarioId) {
		set(DISPLAY_SCENARIO_ID,displayScenarioId);
	}
	public Float getPriceException() {
		return get(PRICE_EXCEPTION);
	}

	public void setPriceException(Float priceException) {
		set(PRICE_EXCEPTION, priceException);
	}
	
	public Float getCogsUnitPrice() {
		return get(COGS_UNIT_PRICE);
	}

	public void setCogsUnitPrice(Float cogsUnitPrice) {
		set(COGS_UNIT_PRICE, cogsUnitPrice);
	}
	
    public Long getExcessInventoryTypeId() {
		return get(EXCESS_INVT_TYPEID);
	}

	public void setExcessInventoryTypeId(Long excessInventoryTypeId) {
		set(EXCESS_INVT_TYPEID,excessInventoryTypeId);
	}

	public String getExcessInvtTypeName() {
		return EXCESS_INVT_TYPENAME;
	}

	public void setExcessInvtTypeName(String excessInvtTypeName) {
		set(EXCESS_INVT_TYPENAME , excessInvtTypeName);
	}

	public Float getOverrideExceptionPct() {
		return get(OVERRIDE_EXCEPTION_PCT);
	}

	public void setOverrideExceptionPct(Float overrideExceptionPct) {
		set(OVERRIDE_EXCEPTION_PCT, overrideExceptionPct);
	}
	
	public Float getProgPrcnt() {
		return get(PROG_PRCNT);
	}

	public void setProgPrcnt(Float progPrcnt) {
		set(PROG_PRCNT, progPrcnt);
	}
	
	public void updateCalculatedFields(Long actualQty) {
    	Long numberFacings = get(NUMBER_FACINGS);
    	Long qtyPerFacing = get(QTY_PER_FACING);
    	Long qtyPerDisplay;
    	Long inventoryRequirement;
    	if (numberFacings != null && qtyPerFacing != null) {
    		qtyPerDisplay = numberFacings * qtyPerFacing;
    	}
    	else qtyPerDisplay = null;
    	setQtyPerDisplay(qtyPerDisplay);
    	if (qtyPerDisplay != null && actualQty != null) {
    		inventoryRequirement = qtyPerDisplay * actualQty;
    	}
    	else inventoryRequirement = null;
    	setInventoryRequirement(inventoryRequirement);
    }
    
	public Date getDtCreated() {
		return get(DT_CREATED);
	}

	public void setDtCreated(Date dtCreated) {
		set(DT_CREATED, dtCreated);
	}

	public Date getDtLastChanged() {
		return get(DT_LAST_CHANGED);
	}

	public void setDtLastChanged(Date dtLastChanged) {
		set(DT_LAST_CHANGED, dtLastChanged);
	}

	public String getUserCreated() {
		return get(USER_CREATED);
	}

	public void setUserCreated(String userCreated) {
		set(USER_CREATED, userCreated);
	}

	public String getUserLastChanged() {
		return get(USER_LAST_CHANGED);
	}

	public void setUserLastChanged(String userLastChanged) {
		set(USER_LAST_CHANGED, userLastChanged);
	}  
	
	
    
    @Override
    public void addChangeListener(ChangeListener... listener) {
        super.addChangeListener(listener);
    }

    @Override
    public void addChangeListener(List<ChangeListener> listeners) {
        super.addChangeListener(listeners);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    public void notify(ChangeEvent evt) {
        super.notify(evt);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value,
                                         Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }

    @Override
    public void removeChangeListener(ChangeListener... listener) {
        super.removeChangeListener(listener);
    }

    @Override
    public void removeChangeListeners() {
        super.removeChangeListeners();
    }
    
    public boolean equalsKitComponentDetailControlModel(KitComponentDTO controlModel){
    	System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..starts..");
    	System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..getBreakCase()=.."+getBreakCase() + " controlModel.getBreakCase()="+controlModel.getBreakCase());
            if (getBreakCase() == null  ) {
                if (controlModel.getBreakCase() != null)
                    return false;
            } else if (!getBreakCase().equalsIgnoreCase("") && !getBreakCase().equals(controlModel.getBreakCase()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..2..");
            if (getBuDesc() == null) {
                if (controlModel.getBuDesc() != null)
                    return false;
            } else if (!getBuDesc().equals(controlModel.getBuDesc()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..3..");
           /* if (getCogsUnitPrice() == null) {
                if (controlModel.getCogsUnitPrice() != null)
                    return false;
                System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..4..");
            } else if (!getCogsUnitPrice().equals(controlModel.getCogsUnitPrice()))
                return false;*/
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..4..");
            if (getFinishedGoodsQty() == null) {
                if (controlModel.getFinishedGoodsQty() != null)
                    return false;
            } else if (!getFinishedGoodsQty().equals(controlModel.getFinishedGoodsQty()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..5..");
            if (getFinishedGoodsSku() == null) {
                if (controlModel.getFinishedGoodsSku() != null)
                    return false;
            } else if (!getFinishedGoodsSku().equals(controlModel.getFinishedGoodsSku()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..6..");
            if (isI2ForecastFlg() == null) {
                if (controlModel.isI2ForecastFlg() != null)
                    return false;
            } else if (!isI2ForecastFlg().equals(controlModel.isI2ForecastFlg()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..7..");
            /*if (getInvoiceUnitPrice() == null) {
                if (controlModel.getInvoiceUnitPrice() != null)
                    return false;
            } else if (!getInvoiceUnitPrice().equals(controlModel.getInvoiceUnitPrice()))
                return false;*/
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..8..");
            if (getKitVersionCode() == null) {
                if (controlModel.getKitVersionCode() != null)
                    return false;
            } else if (!getKitVersionCode().equals(controlModel.getKitVersionCode()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..9..");
            if (getNumberFacings() == null) {
                if (controlModel.getNumberFacings() != null)
                    return false;
            } else if (!getNumberFacings().equals(controlModel.getNumberFacings()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..10..");
            if (getPbcVersion() == null) {
                if (controlModel.getPbcVersion() != null)
                    return false;
            } else if (!getPbcVersion().equals(controlModel.getPbcVersion()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..11..");
            if (isPlantBreakCaseFlg() == null) {
                if (controlModel.isPlantBreakCaseFlg() != null)
                    return false;
            } else if (!isPlantBreakCaseFlg().equals(controlModel.isPlantBreakCaseFlg()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..12..");
            if (getProductName() == null) {
                if (controlModel.getProductName() != null)
                    return false;
            } else if (!getProductName().equals(controlModel.getProductName()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..13..");
            if (isPromoFlg() == null) {
                if (controlModel.isPromoFlg() != null)
                    return false;
            } else if ( !isPromoFlg().equals(controlModel.isPromoFlg()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..14..");
            if (getQtyPerFacing() == null) {
                if (controlModel.getQtyPerFacing() != null)
                    return false;
            } else if (!getQtyPerFacing().equals(controlModel.getQtyPerFacing()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..15..");
            if (getRegCasePack() == null) {
                if (controlModel.getRegCasePack() != null)
                    return false;
            } else if (!getRegCasePack().equals(controlModel.getRegCasePack()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..16..");
            if (getSku() == null) {
                if (controlModel.getSku() != null)
                    return false;
            } else if (!getSku().equals(controlModel.getSku()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..17..");
            if (getSkuId() == null) {
                if (controlModel.getSkuId() != null)
                    return false;
            } else if (!getSkuId().equals(controlModel.getSkuId()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..18..");
            if (isSourcePimFlg() == null) {
                if (controlModel.isSourcePimFlg() != null)
                    return false;
            } else if (!isSourcePimFlg().equals(controlModel.isSourcePimFlg()))
                return false;
            if (getInPogYn() == null) {
                if (controlModel.getInPogYn() != null)
                    return false;
            } else if (!getInPogYn().equals(controlModel.getInPogYn()))
                return false;
            if (getExcessInvtPct() == null) {
                if (controlModel.getExcessInvtPct() != null)
                    return false;
            } else if (!getExcessInvtPct().equals(controlModel.getExcessInvtPct()))
                return false;
            if (getProgPrcnt() == null) {
                if (controlModel.getProgPrcnt() != null)
                    return false;
            } else if (!getProgPrcnt().equals(controlModel.getProgPrcnt()))
                return false;
            System.out.println("KitComponentDTO:equalsKitComponentDetailControlModel() ..ends..");
            return true;
    }
}

