package com.averydennison.dmm.app.client.models;

import java.util.Date;
import com.extjs.gxt.ui.client.data.BaseModel;
public class DisplayScenarioDTO extends BaseModel {
	private static final String DISPLAY_SCENARIO_ID="displayScenarioId";
	private static final String DISPLAY_ID="displayId";
	private static final String SCENARIO_STATUS_ID="scenariodStatusId";
	private static final String SCENARIO_NUM="scenariodNum";
	private static final String DISPLAY_SALES_REP_ID="displaySalesRepId";
	private static final String PROMO_YEAR="promoYear";
	private static final String PROMO_PERIOD_ID="promoPeriodId";
	private static final String PACKOUT_VENDOR_ID="packoutVendorId";
	private static final String MANUFACTURE_ID="manufacturerId";
	private static final String SKU="sku";
	private static final String ACTUAL_QTY="actualQty";
	private static final String QTY_FORECAST="qtyForecast";
	private static final String SCENARIO_APPROVAL_FLG="scenarioApprovalFlg";
	private static final String DT_CREATED="dtCreated";
	private static final String DT_LAST_CHANGED="dtLastChanged";
	private static final String USER_CREATED="userCreated";
	private static final String USER_LAST_CHANGED="userLastChanged";
	
	public DisplayScenarioDTO(){
		
	}
	public DisplayScenarioDTO(Long displayScenarioId, Long displayId, Long scenariodStatusId, Long scenariodNum ,Long displaySalesRepId,
			Long promoYear,Long promoPeriodId, Long packoutVendorId, Long manufacturerId, Date dtCreated, Date dtLastChanged,
			String userCreated, String userLastChanged, String sku, Long actualQty, Long qtyForecast, String scenarioApprovalFlg ) {
	        set(DISPLAY_SCENARIO_ID, displayScenarioId);
	        set(DISPLAY_ID, displayId);
	        set(SCENARIO_STATUS_ID, scenariodStatusId);
	        set(SCENARIO_NUM, scenariodNum);
	        set(DISPLAY_SALES_REP_ID, displaySalesRepId);
	        set(PROMO_YEAR, promoYear);
	        set(PROMO_PERIOD_ID, promoPeriodId);
	        set(PACKOUT_VENDOR_ID, packoutVendorId);
	        set(MANUFACTURE_ID, manufacturerId);
	        set(SKU, sku);
	        set(DT_CREATED, dtCreated);
	        set(DT_LAST_CHANGED, dtLastChanged);
	        set(USER_CREATED, userCreated);
	        set(USER_LAST_CHANGED, userLastChanged);
	        set(ACTUAL_QTY, actualQty);
	        set(QTY_FORECAST, qtyForecast);
	        set(SCENARIO_APPROVAL_FLG,scenarioApprovalFlg);
	}
	public String getSku(){
		return get(SKU);
	}
	public void setSku(String sku) {
		set(SKU,sku);
	}
	public String getScenarioApprovalFlg(){
		return get(SCENARIO_APPROVAL_FLG);
	}
	public void setScenarioApprovalFlg(String scenarioApprovalFlg) {
		set(SCENARIO_APPROVAL_FLG,scenarioApprovalFlg);
	}
	public Long getDisplayScenarioId() {
		return get(DISPLAY_SCENARIO_ID);
	}
	public void setDisplayScenarioId(Long displayScenarioId) {
		set(DISPLAY_SCENARIO_ID,displayScenarioId);
	}
	public Long getDisplayId() {
		return get(DISPLAY_ID);
	}
	public void setDisplayId(Long displayId) {
		set(DISPLAY_ID, displayId);
	}
	public Long getScenariodStatusId() {
		return get(SCENARIO_STATUS_ID);
	}
	public void setScenariodStatusId(Long scenariodStatusId) {
		set(SCENARIO_STATUS_ID, scenariodStatusId);
	}
	public Long getActualQty() {
		return get(ACTUAL_QTY);
	}
	public void setActualQty(Long actualQty) {
		set(ACTUAL_QTY, actualQty);
	}
	public Long getQtyForecast() {
		return get(QTY_FORECAST);
	}
	public void setQtyForecast(Long qtyForecast) {
		set(QTY_FORECAST, qtyForecast);
	}
	public Long getScenarioNum() {
		return get(SCENARIO_NUM);
	}
	public void setScenarioNum(Long scenarioNum) {
		set(SCENARIO_NUM, scenarioNum);
	}
	public Long getDisplaySalesRepId() {
		return get(DISPLAY_SALES_REP_ID);
	}
	public void setDisplaySalesRepId(Long displaySalesRepId) {
		set(DISPLAY_SALES_REP_ID, displaySalesRepId);
	}
	public Long getPromoYear() {
		return get(PROMO_YEAR);
	}
	public void setPromoYear(Long promoYear) {
		set(PROMO_YEAR, promoYear);
	}
	public Long getPromoPeriodId() {
		return get(PROMO_PERIOD_ID);
	}
	public void setPromoPeriodId(Long promoPeriodId) {
		set(PROMO_PERIOD_ID, promoPeriodId);
	}
	public Long getPackoutVendorId() {
		return get(PACKOUT_VENDOR_ID);
	}
	public void setPackoutVendorId(Long packoutVendorId) {
		set(PACKOUT_VENDOR_ID, packoutVendorId);
	}
	public Long getManufacturerId() {
		return get(MANUFACTURE_ID);
	}
	public void setManufacturerId(Long manufacturerId) {
		set(MANUFACTURE_ID, manufacturerId);
	}
	public Date getDtCreated() {
		return get(DT_CREATED);
	}
	public void setDtCreated(Date dtCreated) {
		set(DT_CREATED, dtCreated);
	}
	public Date getDtLastChanged() {
		return get(DT_LAST_CHANGED);
	}
	public void setDtLastChanged(Date dtLastChanged) {
		set(DT_LAST_CHANGED,dtLastChanged);
	}
	public String getUserCreated() {
		return get(USER_CREATED);
	}
	public void setUserCreated(String userCreated) {
		set(USER_CREATED, userCreated);
	}
	public String getUserLastChanged() {
		return get(USER_LAST_CHANGED);
	}
	public void setUserLastChanged(String userLastChanged) {
		set(USER_LAST_CHANGED , userLastChanged);
	}
	
	public boolean equalsCostAnalysisDisplayScenarioInfo(DisplayDTO controlModel) {
		System.out.println("DisplayDTO:equalsCostAnalysisDisplayInfo ..2.. getDisplaySalesRepId()="+getDisplaySalesRepId() + " controlModel.getDisplaySalesRepId()="+controlModel.getDisplaySalesRepId());
        if (getDisplaySalesRepId() == null) {
            if (controlModel.getDisplaySalesRepId() != null)
                return false;
        } else if (!getDisplaySalesRepId().equals(controlModel.getDisplaySalesRepId()))
            return false;
		System.out.println("DisplayScenarioDTO:equalsCostAnalysisDisplayInfo ..3.. getManufacturerId()="+getManufacturerId() + " controlModel.getManufacturerId()="+controlModel.getManufacturerId());

        if (getManufacturerId() == null) {
            if (controlModel.getManufacturerId() != null)
                return false;
        } else if (!getManufacturerId().equals(controlModel.getManufacturerId()))
            return false;
		System.out.println("DisplayScenarioDTO:equalsCostAnalysisDisplayInfo ..4.. getPromoPeriodId()="+getPromoPeriodId() + " controlModel.getPromoPeriodId()="+controlModel.getPromoPeriodId());

        if (getPromoPeriodId() == null) {
            if (controlModel.getPromoPeriodId() != null)
                return false;
        } else if (!getPromoPeriodId().equals(controlModel.getPromoPeriodId()))
            return false;
		System.out.println("DisplayScenarioDTO:equalsCostAnalysisDisplayInfo ..5.. getPromoYear()="+getPromoYear()+ " controlModel.getPromoYear()="+controlModel.getPromoYear());

        if (getPromoYear() == null) {
            if (controlModel.getPromoYear() != null)
                return false;
        } else if (!getPromoYear().equals(controlModel.getPromoYear()))
            return false;
		System.out.println("DisplayScenarioDTO:equalsCostAnalysisDisplayInfo ..6.. getSku()="+getSku()+ " controlModel.getSku()="+controlModel.getSku());

        if (getSku() == null) {
            if (controlModel.getSku() != null)
                return false;
        } else if (!getSku().equals(controlModel.getSku()))
            return false;
		System.out.println("DisplayScenarioDTO:equalsCostAnalysisDisplayInfo ..7.. getStatusId()="+getScenariodStatusId()+ " controlModel.getStatusId()="+controlModel.getStatusId());

        if (getScenariodStatusId() == null) {
            if (controlModel.getStatusId() != null)
                return false;
        } else if (!getScenariodStatusId().equals(controlModel.getStatusId()))
            return false;
		System.out.println("DisplayScenarioDTO:equalsCostAnalysisDisplayInfo ..8.. getQtyForecast()="+getQtyForecast()+ " controlModel.getQtyForecast()="+controlModel.getQtyForecast());

        if (getQtyForecast() == null) {
            if (controlModel.getQtyForecast() != null && controlModel.getQtyForecast() >0)
                return false;
        } else if (!getQtyForecast().equals(controlModel.getQtyForecast()))
            return false;
		System.out.println("DisplayScenarioDTO:equalsCostAnalysisDisplayInfo ..9.. getActualQty()="+getActualQty()+ " controlModel.getActualQty()="+controlModel.getActualQty());

        if (getActualQty() == null) {
            if (controlModel.getActualQty() != null && controlModel.getActualQty() >0)
                return false;
        } else if (!getActualQty().equals(controlModel.getActualQty()))
            return false;
		System.out.println("DisplayScenarioDTO:equalsCostAnalysisDisplayInfo ..10.. getPackoutVendorId()="+getPackoutVendorId()+ " controlModel.getPackoutVendorId()="+controlModel.getPackoutVendorId());
        if (getPackoutVendorId() == null) {
            if (controlModel.getPackoutVendorId() != null)
                return false;
        } else if (!getPackoutVendorId().equals(controlModel.getPackoutVendorId()))
            return false;
		System.out.println("DisplayScenarioDTO:equalsCostAnalysisDisplayInfo ..11.. Ends");
          return true;
    }
}
