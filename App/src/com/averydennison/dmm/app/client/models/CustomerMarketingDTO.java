package com.averydennison.dmm.app.client.models;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.data.Model;
import com.google.gwt.i18n.client.DateTimeFormat;

public class CustomerMarketingDTO extends BaseModel {
    public static final String CUSTOMER_MARKETING_ID = "customerMarketingId";
    public static final String DISPLAY_ID = "displayId";
    public static final String PRICE_AVAILABILITY_DATE = "priceAvailabilityDate";
    public static final String SENT_SAMPLE_DATE = "sentSampleDate";
    public static final String FINAL_ARTWORK_DATE = "finalArtworkDate";
    public static final String PIM_COMPLETED_DATE = "pimCompletedDate";
    public static final String PRICING_ADMIN_DATE = "pricingAdminDate";
    public static final String INITIAL_RENDERING = "initialRenderingFlg";
    public static final String STRUCTURE_APPROVED = "structureApprovedFlg";
    public static final String ORDER_IN = "orderInFlg";
    public static final String PROJECT_LEVEL_PCT = "projectLevelPct";
    public static final String COMMENTS = "comments";
    public static final String DT_CREATED = "dtCreated";
    public static final String DT_LAST_CHANGED = "dtLastChanged";
    public static final String USER_CREATED = "userCreated";
    public static final String USER_LAST_CHANGED = "userLastChanged";

    public CustomerMarketingDTO() {
    }

    public static CustomerMarketingDTO CustomerMarketingDTO(CustomerMarketingDTO cmDTO) {
        CustomerMarketingDTO newInstance = new CustomerMarketingDTO();
        newInstance.setDisplayId(cmDTO.getDisplayId());
        newInstance.setCustomerMarketingId(cmDTO.getCustomerMarketingId());
        newInstance.setPriceAvailabilityDate(cmDTO.getPriceAvailabilityDate());
        newInstance.setSentSampleDate(cmDTO.getSentSampleDate());
        newInstance.setFinalArtworkDate(cmDTO.getFinalArtworkDate());
        newInstance.setPimCompletedDate(cmDTO.getPimCompletedDate());
        newInstance.setPriceAvailabilityDate(cmDTO.getPriceAvailabilityDate());
        newInstance.setIntialRendering(cmDTO.getInitialRendering());
        newInstance.setStructureApproved(cmDTO.getStructureApproved());
        newInstance.setOrderIn(cmDTO.getOrderIn());
        newInstance.setProjectLevelPct(cmDTO.getProjectLevelPct());
        newInstance.setComments(cmDTO.getComments());
        newInstance.setDtCreated(cmDTO.getDtCreated());
        newInstance.setDtLastChanged(cmDTO.getDtLastChanged());
        newInstance.setUserCreated(cmDTO.getUserCreated());
        newInstance.setUserLastChanged(cmDTO.getUserLastChanged());
        return newInstance;
    }

    public Long getDisplayId() {
        return get(DISPLAY_ID);
    }

    public void setDisplayId(Long displayId) {
        set(DISPLAY_ID, displayId);
    }

    public Long getCustomerMarketingId() {
        return get(CUSTOMER_MARKETING_ID);
    }

    public void setCustomerMarketingId(Long customerMarketingId) {
        set(CUSTOMER_MARKETING_ID, customerMarketingId);
    }

    public Date getPriceAvailabilityDate() {
        return get(PRICE_AVAILABILITY_DATE);
    }

    public void setPriceAvailabilityDate(Date priceAvailabilityDate) {
        set(PRICE_AVAILABILITY_DATE, priceAvailabilityDate);
    }

    public Date getSentSampleDate() {
        return get(SENT_SAMPLE_DATE);
    }

    public void setSentSampleDate(Date sentSampleDate) {
        set(SENT_SAMPLE_DATE, sentSampleDate);
    }

    public Date getFinalArtworkDate() {
        return get(FINAL_ARTWORK_DATE);
    }

    public void setFinalArtworkDate(Date finalArtworkDate) {
        set(FINAL_ARTWORK_DATE, finalArtworkDate);
    }

    public Date getPimCompletedDate() {
        return get(PIM_COMPLETED_DATE);
    }

    public void setPimCompletedDate(Date pimCompletedDate) {
        set(PIM_COMPLETED_DATE, pimCompletedDate);
    }

    public Date getPricingAdminDate() {
        return get(PRICING_ADMIN_DATE);
    }

    public void setPricingAdminDate(Date pricingAdminDate) {
        set(PRICING_ADMIN_DATE, pricingAdminDate);
    }

    public Boolean getInitialRendering() {
        return get(INITIAL_RENDERING);
    }

    public void setIntialRendering(Boolean initialRendering) {
        set(INITIAL_RENDERING, initialRendering);
    }

    public Boolean getStructureApproved() {
        return get(STRUCTURE_APPROVED);
    }

    public void setStructureApproved(Boolean structureApproved) {
        set(STRUCTURE_APPROVED, structureApproved);
    }

    public Boolean getOrderIn() {
        return get(ORDER_IN);
    }

    public void setOrderIn(Boolean orderIn) {
        set(ORDER_IN, orderIn);
    }

    public Short getProjectLevelPct() {
        return get(PROJECT_LEVEL_PCT);
    }

    public void setProjectLevelPct(Short projectLevelPct) {
        set(PROJECT_LEVEL_PCT, projectLevelPct);
    }

    public String getComments() {
        return get(COMMENTS);
    }

    public void setComments(String comments) {
        set(COMMENTS, comments);
    }

    public Date getDtCreated() {
        return get(DT_CREATED);
    }

    public void setDtCreated(Date dtCreated) {
        set(DT_CREATED, dtCreated);
    }

    public Date getDtLastChanged() {
        return get(DT_LAST_CHANGED);
    }

    public void setDtLastChanged(Date dtLastChanged) {
        set(DT_LAST_CHANGED, dtLastChanged);
    }

    public String getUserCreated() {
        return get(USER_CREATED);
    }

    public void setUserCreated(String userCreated) {
        set(USER_CREATED, userCreated);
    }

    public String getUserLastChanged() {
        return get(USER_LAST_CHANGED);
    }

    public void setUserLastChanged(String userLastChanged) {
        set(USER_LAST_CHANGED, userLastChanged);
    }

    @Override
    protected void fireEvent(int type, Model item) {
        super.fireEvent(type, item);
    }

    @Override
    protected void fireEvent(int type) {
        super.fireEvent(type);
    }

    @Override
    public void notify(ChangeEvent evt) {
        super.notify(evt);
    }

    @Override
    protected void notifyPropertyChanged(String name, Object value,
                                         Object oldValue) {
        super.notifyPropertyChanged(name, value, oldValue);
    }

    @Override
    public void addChangeListener(ChangeListener... listener) {
        // TODO Auto-generated method stub
        super.addChangeListener(listener);
    }

    @Override
    public void addChangeListener(List<ChangeListener> listeners) {
        // TODO Auto-generated method stub
        super.addChangeListener(listeners);
    }

    @Override
    public void removeChangeListener(ChangeListener... listener) {
        // TODO Auto-generated method stub
        super.removeChangeListener(listener);
    }

    @Override
    public void removeChangeListeners() {
        // TODO Auto-generated method stub
        super.removeChangeListeners();
    }
    public boolean equalsCustomerMarketingInfo(CustomerMarketingDTO controlModel) {
        DateTimeFormat fmt = DateTimeFormat.getFormat("MM/dd/yyyy");
        if (getComments() == null) {
            if (controlModel.getComments() != null)
                return false;
        } else if (controlModel.getComments()==null){
        	return false;   
        } else if (!getComments().equals(controlModel.getComments()))
            return false;
        if (getFinalArtworkDate() == null) {
            if (controlModel.getFinalArtworkDate() != null)
                return false;
        } else if (controlModel.getFinalArtworkDate()==null){
        	return false;   
        } else if (!fmt.format(getFinalArtworkDate()).equals(fmt.format(controlModel.getFinalArtworkDate())))
            return false;
        if (getInitialRendering() == null) {
            if (controlModel.getInitialRendering() != null)
                return false;
        } else if (controlModel.getInitialRendering()==null){
        	return false;   
        } else if (!getInitialRendering().equals(controlModel.getInitialRendering()))
            return false;
        if (getOrderIn() == null) {
            if (controlModel.getOrderIn() != null)
                return false;
        } else if (controlModel.getOrderIn()==null){
        	return false; 
        } else if (!getOrderIn().equals(controlModel.getOrderIn()))
            return false;
        if (getPimCompletedDate() == null) {
            if (controlModel.getPimCompletedDate() != null)
                return false;
        } else if (controlModel.getPimCompletedDate()==null){
        	return false;
        } else if (!fmt.format(getPimCompletedDate()).equals(fmt.format(controlModel.getPimCompletedDate())))
            return false;
        if (getPriceAvailabilityDate() == null) {
            if (controlModel.getPriceAvailabilityDate() != null)
                return false;
        } else if (controlModel.getPriceAvailabilityDate()==null){
        	return false;
        }else if( !fmt.format(getPriceAvailabilityDate()).equals(fmt.format(controlModel.getPriceAvailabilityDate()))){
        	return false;
        }
        if (getSentSampleDate() == null) {
            if (controlModel.getSentSampleDate() != null)
                return false;
        } else if (controlModel.getSentSampleDate()==null){
        	return false;
        } else if (!fmt.format(getSentSampleDate()).equals(fmt.format(controlModel.getSentSampleDate())))
            return false;
        if (getPricingAdminDate() == null) {
            if (controlModel.getPricingAdminDate() != null)
                return false;
        } else if (controlModel.getPricingAdminDate()==null){
        	return false;
        } else if (!fmt.format(getPricingAdminDate()).equals(fmt.format(controlModel.getPricingAdminDate())))
            return false;
        if (getProjectLevelPct() == null) {
            if (controlModel.getProjectLevelPct() != null)
                return false;
        } else if (controlModel.getProjectLevelPct()==null){
        	return false; 
        } else if (!getProjectLevelPct().equals(controlModel.getProjectLevelPct()))
            return false;
        if (getStructureApproved() == null) {
            if (controlModel.getStructureApproved() != null)
                return false;
        } else if (controlModel.getStructureApproved()==null){
        	return false; 
        } else if (!getStructureApproved().equals(controlModel.getStructureApproved()))
            return false;
        return true;
    }
}

