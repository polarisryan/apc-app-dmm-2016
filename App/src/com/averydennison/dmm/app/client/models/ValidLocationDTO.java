package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Alvin Wong
 * Date: Dec 18, 2009
 * Time: 3:09:37 PM
 */
public class ValidLocationDTO extends BaseModel {
    public static String DC_CODE = "dcCode";
    public static String DC_NAME = "dcName";
    public static String ACTIVE_FLG = "activeFlg";

    public ValidLocationDTO() {
    }

    public ValidLocationDTO(String dcCode, String dcName, Boolean activeFlg) {
        set(DC_CODE, dcCode);
        set(DC_NAME, dcName);
        set(ACTIVE_FLG, activeFlg);
    }

    public String getDcCode() {
        return get(DC_CODE);
    }

    public void setDcCode(String dcCode) {
        set(DC_CODE, dcCode);
    }

    public String getDcName() {
        return get(DC_NAME);
    }

    public void setDcName(String dcName) {
        set(DC_NAME, dcName);
    }

    public Boolean isActiveFlg() {
        return get(ACTIVE_FLG);
    }

    public void setActiveFlg(Boolean isActiveFlg) {
        set(ACTIVE_FLG, isActiveFlg);
    }

    public String toString() {
    	return get(DC_NAME);
    }
}
