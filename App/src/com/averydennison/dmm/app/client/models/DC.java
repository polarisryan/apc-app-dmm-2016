package com.averydennison.dmm.app.client.models;

import java.io.Serializable;

import com.extjs.gxt.ui.client.data.BaseTreeModel;

/**
 * User: Spart Arguello
 * Date: Nov 17, 2009
 * Time: 3:10:37 PM
 */
public class DC extends BaseTreeModel implements Serializable {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String QTY = "qty";
    public static final String DC = "dc";

    private static int id = 0;

    public DC() {
        set("id", id++);
    }

    public DC(String name, Long qty) {
        set(ID, id++);
        set(QTY, qty);
        set(NAME, name);
        set(DC, this);
    }

    public DC(String name, Long qty, BaseTreeModel[] children) {
        this(name, qty);
        for (int i = 0; i < children.length; i++) {
            add(children[i]);
        }
    }

    public Integer getId() {
        return (Integer) get("id");
    }

    public String getName() {
        return (String) get("name");
    }

    public Long getQty() {
        return get(QTY);
    }

    public void setQty(Long qty) {
        set(QTY, qty);
    }

    public String toString() {
        return getName() + " | " + getQty();
    }
}