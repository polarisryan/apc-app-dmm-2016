package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.Model;


public class DisplayCostImageDTO extends BaseModel {
	
	 public static final String IMAGE_ID	="imageId";
	 public static final String DISPLAY_COST_ID= "displayId";
	 public static final String DISPLAY_ID="DisplayCostId" ;
	 public static final String CREATE_DATE="createDate";
	 public static final String URL="url";
	 public static final String IMAGE_NAME="imageName";
	 public static final String DESCRIPTION="description";
	 public static final String DISPLAY_SCENARIO_ID="displayScenarioId";
	 
	 public Long getImageId() {
			return get(IMAGE_ID);
		}
	 public void setImageId(Long imageId) {
		 set(IMAGE_ID,imageId);
		}
		public Long getDisplayId() {
			return get(DISPLAY_ID);
		}
		public void setDisplayId(Long displayId) {
			set(DISPLAY_ID, displayId);
		}
		public Long getDisplayScenarioId() {
			return get(DISPLAY_SCENARIO_ID);
		}
		public void setDisplayScenarioId(Long displayScenarioId) {
			set(DISPLAY_SCENARIO_ID,displayScenarioId);
		}
		public Long getDisplayCostId() {
			return get(DISPLAY_COST_ID);
		}
		public void setDisplayCostId(Long displayCostId) {
			set(DISPLAY_COST_ID, displayCostId);
		}
		public Date getCreateDate() {
			return get(CREATE_DATE);
		}
		public void setCreateDate(Date createDate) {
			set(CREATE_DATE, createDate);
		}
		public String getUrl() {
			return get(URL);
		}
		public void setUrl(String url) {
			set(URL,url);
		}
		public String getImageName() {
			return get(IMAGE_NAME);
		}
		public void setImageName(String imageName) {
			set(IMAGE_NAME, imageName);
		}
		public String getDescription() {
			return get(DESCRIPTION);
		}
		public void setDescription(String description) {
			set(DESCRIPTION, description);
		}
		
		@Override
	    protected void fireEvent(int type) {
	        super.fireEvent(type);
	    }

	    @Override
	    public void notify(ChangeEvent evt) {
	        super.notify(evt);
	    }

	    @Override
	    protected void fireEvent(int type, Model item) {
	        super.fireEvent(type, item);
	    }

	    @Override
	    protected void notifyPropertyChanged(String name, Object value, Object oldValue) {
	        super.notifyPropertyChanged(name, value, oldValue);
	    }

}
