package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class ManufacturerCountryDTO extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8867752286450437783L;
	public static final String MANUFACTURER_ID = "manufacturerId";
	public static final String NAME = "name";
	public static String COUNTRY_ID = "countryId";
	public static String COUNTRY_NAME = "countryName";
	public static String ACTIVE = "active";

	public ManufacturerCountryDTO() {
	}

	public ManufacturerCountryDTO(Long manufacturerId, String name,
			Long countryId, String countryName) {
		set(MANUFACTURER_ID, manufacturerId);
		set(NAME, name);
		set(COUNTRY_ID, countryId);
		set(COUNTRY_NAME, countryName);
	}

	public Long getManufacturerId() {
		return get(MANUFACTURER_ID);
	}

	public void setManufacturerId(Long manufacturerId) {
		set(MANUFACTURER_ID, manufacturerId);
	}

	public String getName() {
		return get(NAME);
	}

	public void setName(String name) {
		set(NAME, name);
	}

	public Long getCountryId() {
		return get(COUNTRY_ID);
	}

	public void setCountryId(Long countryId) {
		set(COUNTRY_ID, countryId);
	}

	public String getCountryName() {
		return get(COUNTRY_NAME);
	}

	public void setCountryName(String countryName) {
		set(COUNTRY_NAME, countryName);
	}

	public void setActive(Boolean activeFlg) {
		set(ACTIVE, activeFlg);
	}

	public Boolean isActive() {
		return get(ACTIVE);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ManufacturerCountryDTO object = (ManufacturerCountryDTO) o;

		if (getManufacturerId() != null ? !getManufacturerId().equals(
				object.getManufacturerId()) : object.getManufacturerId() != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = getManufacturerId() != null ? getManufacturerId().hashCode() : 0;
		return result;
	}

	@Override
	public String toString() {
		return "ManufacturerCountryDTO{" + "manufacturerId="
				+ get(MANUFACTURER_ID) + ", name='" + get(NAME) + '\''
				+ ", countryId='" + get(COUNTRY_ID) + '\'' + ", countryName='"
				+ get(COUNTRY_NAME) + '\'' + '}';
	}

}
