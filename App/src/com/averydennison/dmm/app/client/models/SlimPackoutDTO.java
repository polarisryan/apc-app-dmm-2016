package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Dec 23, 2009
 * Time: 10:19:10 AM
 */
public class SlimPackoutDTO extends BaseModel {
    public static final String DC = "dc";
    public static final String SHIP_LOC_PRODUCT_DUE_DATE = "shipLocProductDueDate";
    public static final String PRODUCT_DUE_DATE = "productDueDate";
    public static final String MUST_ARRIVE_DATE = "mustArriveDate";
    public static final String SHIP_FROM_DATE = "shipFromDate";
	public static final String SHIP_FROM_LOCATIONID = "shipFromLocationId";
    public static final String SHIP_FROM_DCCODE = "shipFromDcCode";
    public static final String CORRUGATE_DUE_DATE = "corrugateDueDate";
    public static final String DELIVERY_DATE = "deliveryDate";

    

    public String getDc() {
        return get(DC);
    }

    public void setDc(String dc) {
        set(DC, dc);
    }

    public Date getShipLocProductDueDate() {
        return get(SHIP_LOC_PRODUCT_DUE_DATE);
    }

    public void setShipLocProductDueDate(Date shipLocProductDueDate) {
        set(SHIP_LOC_PRODUCT_DUE_DATE, shipLocProductDueDate);
    }
    
    public Date getProductDueDate() {
        return get(PRODUCT_DUE_DATE);
    }

    public void setProductDueDate(Date productDueDate) {
        set(PRODUCT_DUE_DATE, productDueDate);
    }

    public Date getMustArriveDate() {
        return get(MUST_ARRIVE_DATE);
    }

    public void setMustArriveDate(Date mustArriveDate) {
        set(MUST_ARRIVE_DATE, mustArriveDate);
    }

    public Date getShipFromDate() {
        return get(SHIP_FROM_DATE);
    }

    public void setShipFromDate(Date shipFromDate) {
        set(SHIP_FROM_DATE, shipFromDate);
    }
    
    public Date getCorrugateDueDate() {
        return get(CORRUGATE_DUE_DATE);
    }

    public void setCorrugateDueDate(Date corrugateDueDate) {
        set(CORRUGATE_DUE_DATE, corrugateDueDate);
    }

	public Long getShipFromLocationId() {
		return get(SHIP_FROM_LOCATIONID);
	}

	public void setShipFromLocationId(Long shipFromLocationId) {
		set(SHIP_FROM_LOCATIONID, shipFromLocationId);
	}

	public String getShipFromDcCode() {
		return get(SHIP_FROM_DCCODE);
	}

	public void setShipFromDcCode(String shipFromDcCode) {
		set(SHIP_FROM_DCCODE, shipFromDcCode);
	}
    
	 public Date getDeliveryDate() {
	        return get(DELIVERY_DATE);
	    }

	    public void setDeliveryDate(Date deliveryDueDate) {
	        set(DELIVERY_DATE, deliveryDueDate);
	    }
    
}
