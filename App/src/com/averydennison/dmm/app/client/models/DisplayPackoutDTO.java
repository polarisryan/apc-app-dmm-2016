package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Spart Arguello
 * Date: Dec 1, 2009
 * Time: 5:06:21 PM
 */
public class DisplayPackoutDTO extends BaseModel {
	private static final long serialVersionUID = -14782498996791227L;
	public static final String DISPLAY_PACKOUT_ID = "displayPackoutId";
    public static final String DISPLAY_SHIP_WAVE_ID = "displayShipWaveId";
    public static final String DISPLAY_DC_ID = "displayDcId";
    public static final String SHIP_LOC_PRODUCT_DUE_DATE = "shipLocProductDueDate";
    public static final String PRODUCT_DUE_DATE = "productDueDate";
    public static final String QUANTITY = "quantity";
    public static final String DT_CREATED = "dtCreated";
    public static final String DT_LAST_CHANGED = "dtLastChanged";
    public static final String USER_CREATED = "userCreated";
    public static final String USER_LAST_CHANGED = "userLastChanged";
    public static final String SHIP_WAVE_NUM = "shipWaveNum";
    public static final String MUST_ARRIVE_DATE = "mustArriveDate";
    public static final String SHIP_FROM_DATE = "shipFromDate";
    public static final String SHIP_FROM_LOCATIONID = "shipFromLocationId";
    public static final String CORRUGATE_DUE_DATE = "corrugateDueDate";
    public static final String DELIVERY_DATE = "deliveryDate";

    public DisplayPackoutDTO() {
    }

    public static DisplayPackoutDTO displayPackoutDTO(DisplayDcAndPackoutDTO dap) {
    	DisplayPackoutDTO displayPackoutDTO = new DisplayPackoutDTO();
		displayPackoutDTO.setDisplayPackoutId(dap.getDisplayPackoutId());
    	displayPackoutDTO.setDisplayDcId(dap.getDisplayDcId());			
		displayPackoutDTO.setDisplayShipWaveId(dap.getDisplayShipWaveId());
		displayPackoutDTO.setShipWaveNum(new Integer(dap.getShipWaveNum().intValue()));
		displayPackoutDTO.setMustArriveDate(dap.getMustArriveDate());
		displayPackoutDTO.setShipFromDate(dap.getShipFromDate());
		displayPackoutDTO.setCorrugateDueDate(dap.getCorrugateDueDate());
		displayPackoutDTO.setDeliveryDate(dap.getDeliveryDate());
		displayPackoutDTO.setShipLocProductDueDate(dap.getShipLocProductDueDate());
		displayPackoutDTO.setProductDueDate(dap.getProductDueDate());
		displayPackoutDTO.setQuantity(dap.getPoQuantity());
		displayPackoutDTO.setDtCreated(dap.getPoDtCreated());
		displayPackoutDTO.setDtLastChanged(dap.getPoDtLastChanged());
		displayPackoutDTO.setUserCreated(dap.getPoUserCreated());
		displayPackoutDTO.setUserLastChanged(dap.getPoUserLastChanged());
		displayPackoutDTO.setShipFromLocationId(dap.getShipFromLocationId());
		return displayPackoutDTO;
    }

    public Long getDisplayPackoutId() {
        return get(DISPLAY_PACKOUT_ID);
    }

    public void setDisplayPackoutId(Long displayPackoutId) {
        set(DISPLAY_PACKOUT_ID, displayPackoutId);
    }

    public Long getDisplayShipWaveId() {
        return get(DISPLAY_SHIP_WAVE_ID);
    }

    public void setDisplayShipWaveId(Long displayShipWaveId) {
        set(DISPLAY_SHIP_WAVE_ID, displayShipWaveId);
    }

    public Long getDisplayDcId() {
        return get(DISPLAY_DC_ID);
    }

    public void setDisplayDcId(Long displayDcId) {
        set(DISPLAY_DC_ID, displayDcId);
    }
    
    public Date getShipLocProductDueDate() {
        return get(SHIP_LOC_PRODUCT_DUE_DATE);
    }

    public void setShipLocProductDueDate(Date shipLocProductDueDate) {
        set(SHIP_LOC_PRODUCT_DUE_DATE, shipLocProductDueDate);
    }

    public Date getProductDueDate() {
        return get(PRODUCT_DUE_DATE);
    }

    public void setProductDueDate(Date productDueDate) {
        set(PRODUCT_DUE_DATE, productDueDate);
    }

    public Date getCorrugateDueDate() {
        return get(CORRUGATE_DUE_DATE);
    }

    public void setCorrugateDueDate(Date corrugateDueDate) {
        set(CORRUGATE_DUE_DATE, corrugateDueDate);
    }
    
    public Date getDeliveryDate() {
        return get(DELIVERY_DATE);
    }

    public void setDeliveryDate(Date deliveryDueDate) {
        set(DELIVERY_DATE, deliveryDueDate);
    }
    
    public Long getQuantity() {
        if (get(QUANTITY) instanceof String) return Long.valueOf((String) get(QUANTITY));
        return get(QUANTITY);
    }

    public void setQuantity(Long quantity) {
        set(QUANTITY, quantity);
    }

    public Date getDtCreated() {
        return get(DT_CREATED);
    }

    public void setDtCreated(Date dtCreated) {
        set(DT_CREATED, dtCreated);
    }

    public Date getDtLastChanged() {
        return get(DT_LAST_CHANGED);
    }

    public void setDtLastChanged(Date dtLastChanged) {
        set(DT_LAST_CHANGED, dtLastChanged);
    }

    public String getUserCreated() {
        return get(USER_CREATED);
    }

    public void setUserCreated(String userCreated) {
        set(USER_CREATED, userCreated);
    }

    public String getUserLastChanged() {
        return get(USER_LAST_CHANGED);
    }

    public void setUserLastChanged(String userLastChanged) {
        set(USER_LAST_CHANGED, userLastChanged);
    }

    public Integer getShipWaveNum() {
        return get(SHIP_WAVE_NUM);
    }

    public void setShipWaveNum(Integer shipWave) {
        set(SHIP_WAVE_NUM, shipWave);
    }

    public Date getMustArriveDate() {
        return get(MUST_ARRIVE_DATE);
    }

    public void setMustArriveDate(Date mustArriveDate) {
        set(MUST_ARRIVE_DATE, mustArriveDate);
    }

    public Date getShipFromDate() {
        return get(SHIP_FROM_DATE);
    }

    public void setShipFromDate(Date shipFromDate) {
        set(SHIP_FROM_DATE, shipFromDate);
    }
    
    public Long getShipFromLocationId() {
        return get(SHIP_FROM_LOCATIONID);
    }

    public void setShipFromLocationId(Long shipFromLocationId) {
        set(SHIP_FROM_LOCATIONID, shipFromLocationId);
    }
    
    public String toString() {
        return "DisplayPackout{" +
                "displayPackoutId=" + this.getDisplayPackoutId().toString() +
                ", displayShipWaveId=" + this.getDisplayShipWaveId().toString() +
                ", displayDcId=" + this.getDisplayDcId().toString() +
                ", productDueDate=" + this.getProductDueDate().toString() +
                ", quantity=" + this.getQuantity().toString() +
                ", dtCreated=" + this.getDtCreated().toString() +
                ", dtLastChanged=" + this.getDtLastChanged().toString() +
                ", userCreated='" + this.getUserCreated() + '\'' +
                ", userLastChanged='" + this.getUserLastChanged() + '\'' +
                '}';
    }
}