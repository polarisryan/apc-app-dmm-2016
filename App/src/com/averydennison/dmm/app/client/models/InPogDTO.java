package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class InPogDTO extends BaseModel {
    public static final String INPOG_YN_ID = "inPogYorNId";
    public static final String INPOG_YN_NAME = "inPogYorName";
    
    public InPogDTO() {
    }

    public InPogDTO(Boolean inPogYorNId, String inPogYorName) {
        set(INPOG_YN_ID, inPogYorNId);
        set(INPOG_YN_NAME, inPogYorName);
    }

    public Boolean getInPogYorNId() {
        return get(INPOG_YN_ID);
    }

    public void setInPogYorNId(Boolean inPogYorNId) {
        set(INPOG_YN_ID, inPogYorNId);
    }

    public String getInPogYorName() {
        return get(INPOG_YN_NAME);
    }

    public void setInPogYorName(String inPogYorName) {
        set(INPOG_YN_NAME, inPogYorName);
    }
}
