package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModel;

public class ProgramBuDTO extends BaseModel {
	
	private static final long serialVersionUID = 2328708395663850872L;
	
	public static final String PROGRAM_BU_ID="programBuId";
	public static final String DISPLAY_ID="displayId";
	public static final String BU_CODE="buCode";
	public static final String BU_DESC="buDesc";
	public static final String PROGRAM_PCT="programPct";
	public static final String DT_CREATED="dtCreated";
	public static final String DT_LASTCHANGED="dtLastChanged";
	public static final String USER_CREATED="userCreated";
	public static final String USER_LAST_CHANGED="userLastChanged";
    
	public Long getProgramBuId() {
		return get(PROGRAM_BU_ID);
	}
	public void setProgramBuId(Long programBuId) {
		set(PROGRAM_BU_ID, programBuId);
	}
	public Long getDisplayId() {
		return get(DISPLAY_ID);
	}
	public void setDisplayId(Long displayId) {
		set(DISPLAY_ID,displayId);
	}
	public String getBuCode() {
		return get(BU_CODE);
	}
	public void setBuCode(String buCode) {
		set(BU_CODE,buCode);
	}
	public String getBuDesc() {
		return get(BU_DESC);
	}
	public void setBuDesc(String buDesc) {
		set(BU_DESC,buDesc);
	}
	public Double getProgramPct() {
		return get(PROGRAM_PCT);
	}
	public void setProgramPct(Double programPct) {
		set(PROGRAM_PCT,programPct);
	}
	public Date getDtCreated() {
		return get(DT_CREATED);
	}
	public void setDtCreated(Date dtCreated) {
		set(DT_CREATED, dtCreated);
	}
	public Date getDtLastChanged() {
		return get(DT_LASTCHANGED);
	}
	public void setDtLastChanged(Date dtLastChanged) {
		set(DT_LASTCHANGED, dtLastChanged);
	}
	public String getUserCreated() {
		return get(USER_CREATED);
	}
	public void setUserCreated(String userCreated) {
		set(USER_CREATED,userCreated);
	}
	public String getUserLastChanged() {
		return get(USER_LAST_CHANGED);
	}
	public void setUserLastChanged(String userLastChanged) {
		set(USER_LAST_CHANGED, userLastChanged);
	}
}	
	
