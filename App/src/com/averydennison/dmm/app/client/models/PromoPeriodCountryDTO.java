package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class PromoPeriodCountryDTO extends BaseModel {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 8867752286450437783L;
    public static final String PROMO_PERIOD_ID = "promoPeriodId";
    public static final String PROMO_PERIOD_NAME = "promoPeriodName";
	 public static String COUNTRY_ID = "countryId";
	 public static String COUNTRY_NAME = "countryName";
	 
	 public PromoPeriodCountryDTO() {
	 }

	 public PromoPeriodCountryDTO(Long promoPeriodId, String promoPeriodName, Long countryId, String countryName) {
	      set(PROMO_PERIOD_ID, promoPeriodId);
	      set(PROMO_PERIOD_NAME, promoPeriodName);
	      set(COUNTRY_ID, countryId);
	      set(COUNTRY_NAME, countryName);
	 }
	 
	 public Long getPromoPeriodId() {
	     return get(PROMO_PERIOD_ID);
	 }

	 public void setPromoPeriodId(Long promoPeriodId) {
	     set(PROMO_PERIOD_ID, promoPeriodId);
	 }

	 public String getPromoPeriodName() {
	     return get(PROMO_PERIOD_NAME);
	 }

	 public void setPromoPeriodName(String promoPeriodName) {
	     set(PROMO_PERIOD_NAME, promoPeriodName);
	 }

	 public Long getCountryId() {
	     return get(COUNTRY_ID);
	 }

	 public void setCountryId(Long countryId) {
	        set(COUNTRY_ID, countryId);
	 }

	 public String getCountryName() {
	     return get(COUNTRY_NAME);
	 }

	 public void setCountryName(String countryName) {
	        set(COUNTRY_NAME, countryName);
	 }
	 
	@Override
	public String toString() {
	     return "ManufacturerCountryDTO{" +
	         "promoPeriodId=" + get(PROMO_PERIOD_ID) +
	         ", promoPeriodname='" + get(PROMO_PERIOD_NAME) + '\'' +
	         ", countryId='" + get(COUNTRY_ID) + '\'' +
	         ", countryName='" + get(COUNTRY_NAME) + '\'' +
	         '}';
	    }
}


