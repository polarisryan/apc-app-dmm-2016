package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Mari
 * Date: Jun 24, 2010
 * Time: 9:12:45 AM
 */
public class CostAnalysis extends BaseModel {

	
	 public static final String UPC = "upc";
	 public static final String  CHKDGT = "chkDgt";
  	 public static final String VERS = "vers";
  	 public static final String SKU = "sku";
  	 public static final String BU = "bU";
  	 public static final String DESC = "desc";
  	 public static final String QPD  = "qPD";
  	 public static final String F = "f";
  	 
  	public static final String RETAIL_PACKS_PIC = "retailPacks";
  	public static final String PIM = "pim";
  	public static final String PV = "pv";
  	public static final String I2 = "i2";
  	public static final String PBCBOM="pbcBom";
  	 public static final String INVUNIT = "invUnit";
  	 public static final String INVTH = "invTH";
  	 public static final String COGSUNIT = "cOGSUnit";
  	 public static final String COGSTH="cOGSTH";
  	 public static final String VMMONEY ="vMMoney";
  	 public static final String VMPERCENTAGE = "vMPercentage";
  	 public static final String BENCHUNIT ="benchUnit";
  	 public static final String BENCHTOT="benchTtl";
  	 public static final String LISTUNIT = "listUnit";
  	 public static final String LISTTOT = "listTtl";
  	 
    public CostAnalysis(String bU, double invUnit, double vMPercentage, double benchUnit) {
    	 set(BU, bU);
         set(INVUNIT, invUnit);
         set(VMPERCENTAGE, vMPercentage);
         set(BENCHUNIT, benchUnit);
    }

   

  /*  public CostAnalysis(String upc, 
    		Long chkDgt, String vers, String sku, String bU, String desc, Long qPD, Long f, Double invUnit,  Double invTH, Double cOGSUnit,
    		Double cOGSTH,Double vMMoney, String vMPercentage,Double benchUnit, Double beachTtl, Double listUnit, Double listTtl   ){
    	 set("upc",upc);
    	 set("chkDgt",chkDgt);
    	 set("vers",vers);
    	 set("sku",sku);
    	 set(BU,bU);
    	 set("desc",desc);
    	 set("qPD",qPD);
    	 set("f",f);
    	 set(INVUNIT,invUnit);
    	 set("invTH",invTH);
    	 set("cOGSUnit",cOGSUnit);
    	 set("cOGSTH",cOGSTH);
    	 set("vMMoney",vMMoney);
    	 set("vMPercentage",vMPercentage);
    	 set("benchUnit",benchUnit);
    	 set("beachTtl",beachTtl);
    	 set("listUnit",listUnit);
    	 set("listTtl",listTtl);
	}*/
    
   public CostAnalysis(String upc, int chkDgt, String vers, String sku,
			String bU, String desc, int qPD, int f, double invUnit, double invTH,
			double cOGSUnit, double cOGSTH, double vMMoney, String vMPercentage, double benchUnit, double beachTtl,
			double listUnit, double listTtl , int retailPacks,boolean pim, boolean pv, boolean i2,  boolean pbcBom) {
	   
	 set(UPC,upc);
  	 set(CHKDGT,chkDgt);
  	 set(VERS,vers);
  	 set(SKU,sku);
  	 set(BU,bU);
  	 set(DESC,desc);
  	 set(QPD,qPD);
  	 set(F,f);
  	 set(INVUNIT,invUnit);
  	 set(INVTH,invTH);
  	 set(COGSUNIT,cOGSUnit);
  	 set(COGSTH,cOGSTH);
  	 set(VMMONEY,vMMoney);
  	 set(VMPERCENTAGE,vMPercentage);
  	 set(BENCHUNIT,benchUnit);
  	 set(BENCHTOT,beachTtl);
  	 set(LISTUNIT,listUnit);
  	 set(LISTTOT,listTtl);
  	 set(RETAIL_PACKS_PIC, retailPacks);
  	 set(PIM,pim);
  	 set(PV,pv);
  	 set(I2,i2);
  	 set(PBCBOM, pbcBom);
		// TODO Auto-generated constructor stub
	}

   public String getUpc() {
		return get(UPC);
	}

	public void setUpc(String upc) {
		set(UPC, upc);
	}

	public Long getChkDgt() {
		return get(CHKDGT);
	}

	public void setChkDgt(Long chkDgt) {
		set(CHKDGT,chkDgt);
	}

	public String getVers() {
		return get(VERS);
	}

	public void setVers(String vers) {
		set(VERS,vers);;
	}

	public String getSku() {
		return get(SKU);
	}

	public void setSku(String sku) {
		set(SKU,sku);
	}

	public String getbU() {
		return get(BU);
	}

	public void setbU(String bU) {
		set(BU, bU);
	}

	public String getDesc() {
		return get(DESC);
	}

	public void setDesc(String desc) {
		set(DESC, desc);
	}

	public Long getqPD() {
		return get(QPD);
	}

	public void setqPD(Long qPD) {
		set(QPD,qPD);
	}

	public Long getF() {
		return get(F);
	}

	public void setF(Long f) {
		set(F, f);
	}

	public Double getInvUnit() {
		return get(INVUNIT);
	}

	public void setInvUnit(Double invUnit) {
		set(INVUNIT, invUnit);
	}

	public Integer getInvTH() {
		return get(INVTH);
	}

	public void setInvTH(Double invTH) {
		set(INVTH,invTH);
	}

	public Double getcOGSUnit() {
		return get(COGSUNIT);
	}

	public void setcOGSUnit(Double cOGSUnit) {
		set(COGSUNIT,cOGSUnit);
	}

	public Double getcOGSTH() {
		return get(COGSTH);
	}

	public void setcOGSTH(Double cOGSTH) {
		set(COGSTH,cOGSTH);
	}

	public Double getvMMoney() {
		return get(VMMONEY);
	}

	public void setvMMoney(Double vMMoney) {
		set(VMMONEY,vMMoney);
	}

	public String getvMPercentage() {
		return get(VMPERCENTAGE);
	}

	public void setvMPercentage(String vMPercentage) {
		set(VMPERCENTAGE,vMPercentage);
	}

	public Double getBenchUnit() {
		return get(BENCHUNIT);
	}

	public void setBenchUnit(Double benchUnit) {
		set(BENCHUNIT,benchUnit);
	}

	public Double getBeachTtl() {
		return get(BENCHTOT);
	}

	public void setBeachTtl(Double beachTtl) {
		set(BENCHTOT,beachTtl);
	}

	public Double getListUnit() {
		return get(LISTUNIT);
	}

	public void setListUnit(Double listUnit) {
		set(LISTUNIT,listUnit);
	}

	public Boolean getPim() {
		return get(PIM);
	}

	public void setPim(Boolean pim) {
		set(PIM,pim);
	}
	
	public Boolean getPv() {
		return get(PV);
	}

	public void setPv(Boolean pv) {
		set(PV,pv);
	}
	
	public Boolean getI2() {
		return get(I2);
	}

	public void setI2(Boolean i2) {
		set(PV,i2);
	}
	
	public Boolean getPbcBom() {
		return get(PBCBOM);
	}

	public void setPbcBom(Boolean pbcBom) {
		set(PBCBOM,pbcBom);
	}

}
