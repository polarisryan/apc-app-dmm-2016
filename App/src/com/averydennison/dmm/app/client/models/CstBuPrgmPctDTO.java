package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class CstBuPrgmPctDTO extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String CSTBUPRG_PCT_ID="cstBuPrgmPctId";
	public static final String CUSTOMER_ID="customerId";
	public static final String BU_CODE="buCode";
	public static final String ACTIVE_FLG="activeFlg";
	public static final String PROGRAM_PCT="programPct";
	public static final String FYEAR="fyear";
	
	 public CstBuPrgmPctDTO() {
	 }
	 
	 public CstBuPrgmPctDTO(Long cstBuPrgmPctId ,Long customerId, String buCode,Boolean activeFlg,Float programPct,Long fYear   ) {
		 set(CSTBUPRG_PCT_ID, cstBuPrgmPctId);
		 set(CUSTOMER_ID, customerId);
		 set(BU_CODE,buCode);
		 set(ACTIVE_FLG, activeFlg);
		 set(PROGRAM_PCT,programPct);
		 set(FYEAR, fYear);
	 }
	 
	 
	public void setCstBuPrgmPctId(Long cstBuPrgmPctId ){
		set(CSTBUPRG_PCT_ID, cstBuPrgmPctId);
	}
	
	public Long getCstBuPrgmPctId() {
		return get(CSTBUPRG_PCT_ID);
	}
	
	public void setCustomerId(Long customerId ){
		set(CUSTOMER_ID, customerId);
	}
	
	public Long getCustomerId() {
		return get(CUSTOMER_ID);
	}
	
	public String getBuCode() {
		return get(BU_CODE);
	}
	public void setBuCode(String buCode) {
		set(BU_CODE,buCode);
	}
	
	public void setActiveFlg(Boolean activeFlg) {
		set(ACTIVE_FLG, activeFlg);
	}

	public Boolean isActiveFlg() {
		return get(ACTIVE_FLG);
	}
	
	public Double getProgramPct() {
		return get(PROGRAM_PCT);
	}
	public void setProgramPct(Double programPct) {
		set(PROGRAM_PCT,programPct);
	}
	
	public void setFyear(Long fYear ){
		set(FYEAR, fYear);
	}
	
	public Long getFyear() {
		return get(FYEAR);
	}

	@Override
	public String toString() {
		return "CstBuPrgmPctDTO [getBuCode()=" + getBuCode()
				+ ", getCstBuPrgmPctId()=" + getCstBuPrgmPctId()
				+ ", getCustomerId()=" + getCustomerId() + ", getFyear()="
				+ getFyear() + ", getProgramPct()=" + getProgramPct()
				+ ", isActiveFlg()=" + isActiveFlg() + "]";
	}
}
