package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseModel;

/**
 * User: Mari
 * Date: May 02, 2011
 * Time: 08:41:16 AM
 */
public class CalculationSummaryDTO extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5569719833520658303L;
	public static final String CALCULATION_SUMMARY_ID="calculationSummaryId";
	public static final String DISPLAY_ID= "displayId";
	public static final String DISPLAY_SCENARIO_ID="displayScenarioId";
	public static final String FORECAST_QTY= "foreCastQty";		
	public static final String ACTUAL_QTY= "actualQty";			
	public static final String  SUM_CORRUGATE_COST= "sumCorrugateCost";	
	public static final String  SUM_FULFILLMENT_COST="sumFulFillmentCost";	
	public static final String  SUM_OTHER_COST="sumOtherCost";			
	public static final String SUM_TOT_DISP_COST_NOSHIPPING="sumTotDispCostNoShipping";		
	public static final String SUM_TOT_DISPLAY_COST_SHIPPING="sumTotDisplayCostShipping";
	public static final String SHIPPING_COST="shippingCost";
	public static final String SUM_TRADE_SALES="sumTradeSales";
	public static final String CUST_PROG_DISC="custProgDisc";
	public static final String SUM_NET_SALES_BP="sumNetSalesBp";
	public static final String SUM_NET_SALES_AP="sumNetSalesAp";
	public static final String SUM_COGS="sumCogs";
	public static final String SUM_PRODVAR_MARGIN="sumProdVarMargin";
	public static final String SUM_DISPLAY_COST="sumDisplayCost";
	public static final String SUM_VM_DOLLAR_BP="sumVmDollarBp";
	public static final String SUM_VM_DOLLAR_AP="sumVmDollarAp";
	public static final String SUM_VM_PCT_BP="sumVmPctBp";
	public static final String SUM_VM_PCT_AP="sumVmPctAp";
	public static final String VM_DOLLAR_HURDLE_RED="vmDollarHurdleRed";
	public static final String VM_DOLLAR_HURDLE_GREEN="vmDollarHurdleGreen";
	public static final String VM_PCT_HURDLE_RED="vmPctHurdleRed";
	public static final String VM_PCT_HURDLE_GREEN="vmPctHurdleGreen";
	public static final String STATUS_ID="statusId";
	public static final String PROMO_PERIOD_ID="promoPeriodId";
	public static final String CUSTOMER_ID="customerId";
	public static final String SKU="sku";
	public static final String DESCRIPTION="description";
	public static final String AVERY_SKU="averySku";
	public static final String QTY_PER_FACING="qtyPerFacing";
	public static final String NUMBER_FACINGS="numberFacing";
	public static final String QTY_PER_DISPLAY="qtyPerDisplay";
	public static final String SUM_TOT_ACTUAL_INVOICE="sumTotActualInvoice";
	public static final String HURDLES="hurdles";
	public static final String EXCESS_INVT_VMDOLLAR="excessInvtVmDollar";		
	public static final String EXCESS_INVT_VMPCT="excessInvtVmPct";	
	public static final String CORRUGATE_COST_PER_UNIT="corrugateCostPerUnit";		
	public static final String LABOR_COST_PER_UNIT="laborCostPerUnit";	
	public static final String MDF_PCT="mdfPct";	
	public static final String DISCOUNT_PCT="discountPct";	

	public CalculationSummaryDTO() {
	    	
	}
	    
	public CalculationSummaryDTO(Long calculationSummaryId,Long displayId,Long displayScenarioId, Long foreCastQty, Long actualQty,			
		Float sumCorrugateCost,	Float sumFulFillmentCost, Float sumOtherCost, Float sumTotDispCostNoShipping, Float sumTotDisplayCostShipping,			
		Float shippingCost,	Float sumTradeSales, Float custProgDisc,Float sumNetSalesBp,Float sumNetSalesAp, Float sumCogs,			
		Float sumProdVarMargin,Float sumDisplayCost, Float sumVmDollarBp,Float sumVmDollarAp, Float sumVmPctBp, Float sumVmPctAp,			
		Float vmDollarHurdleRed, Float vmDollarHurdleGreen, Float vmPctHurdleRed, Float vmPctHurdleGreen, Long statusId, Long promoPeriodId,			
		Long customerId, String sku, String description, String averySku, Long qtyPerFacing, Long numberFacing, Long qtyPerDisplay,			
		Float sumTotActualInvoice,	String hurdles, Float excessInvtVmDollar, Float excessInvtVmPct, Float corrugateCostPerUnit, Float laborCostPerUnit
		, Float mdfPct, Float discountPct)
	    {
			set(CALCULATION_SUMMARY_ID,calculationSummaryId);
			set(DISPLAY_ID, displayId);
			set(DISPLAY_SCENARIO_ID,displayScenarioId);
			set(FORECAST_QTY,foreCastQty);		
			set(ACTUAL_QTY, actualQty);			
			set(SUM_CORRUGATE_COST,sumCorrugateCost);	
			set(SUM_FULFILLMENT_COST,sumFulFillmentCost);	
			set(SUM_OTHER_COST,sumOtherCost);			
			set(SUM_TOT_DISP_COST_NOSHIPPING,sumTotDispCostNoShipping);		
			set(SUM_TOT_DISPLAY_COST_SHIPPING,sumTotDisplayCostShipping);
			set(SHIPPING_COST,shippingCost);
			set(SUM_TRADE_SALES,sumTradeSales);
			set(CUST_PROG_DISC,custProgDisc);
			set(SUM_NET_SALES_BP,sumNetSalesBp);
			set(SUM_NET_SALES_AP,sumNetSalesAp);
			set(SUM_COGS,sumCogs);
			set(SUM_PRODVAR_MARGIN,sumProdVarMargin);
			set(SUM_DISPLAY_COST,sumDisplayCost);
			set(SUM_VM_DOLLAR_BP,sumVmDollarBp);
			set(SUM_VM_DOLLAR_AP,sumVmDollarAp);
			set(SUM_VM_PCT_BP,sumVmPctBp);
			set(SUM_VM_PCT_AP,sumVmPctAp);
			set(VM_DOLLAR_HURDLE_RED,vmDollarHurdleRed);
			set(VM_DOLLAR_HURDLE_GREEN,vmDollarHurdleGreen);
			set(VM_PCT_HURDLE_RED,vmPctHurdleRed);
			set(VM_PCT_HURDLE_GREEN,vmPctHurdleGreen);
			set(STATUS_ID,statusId);
			set(PROMO_PERIOD_ID,promoPeriodId);
			set(CUSTOMER_ID,customerId);
			set(SKU,sku);
			set(DESCRIPTION,description);
			set(AVERY_SKU,averySku);
			set(QTY_PER_FACING,qtyPerFacing);
			set(NUMBER_FACINGS,numberFacing);
			set(QTY_PER_DISPLAY,qtyPerDisplay);
			set(SUM_TOT_ACTUAL_INVOICE,sumTotActualInvoice);
			set(HURDLES,hurdles);
			set(EXCESS_INVT_VMDOLLAR,excessInvtVmDollar);
			set(EXCESS_INVT_VMPCT,excessInvtVmPct);
			set(CORRUGATE_COST_PER_UNIT,corrugateCostPerUnit);
			set(LABOR_COST_PER_UNIT,laborCostPerUnit);
			set(MDF_PCT,mdfPct);
			set(DISCOUNT_PCT,discountPct);
	}
	
	public Long getCalculationSummaryId() {
		return get(CALCULATION_SUMMARY_ID);
	}
	public void setCalculationSummaryId(Long calculationSummaryId) {
		set(CALCULATION_SUMMARY_ID, calculationSummaryId);
	}
	
	public Long getDisplayId() {
		return get(DISPLAY_ID);
	}
	public void setDisplayId(Long displayId) {
		set(DISPLAY_ID, displayId);
	}
	public Long getDisplayScenarioId() {
		return get(DISPLAY_SCENARIO_ID);
	}
	public void setDisplayScenarioId(Long displayScenarioId) {
		set(DISPLAY_SCENARIO_ID, displayScenarioId);
	}
	public Long getForeCastQty() {
		return get(FORECAST_QTY);
	}
	public void setForeCastQty(Long foreCastQty) {
		set(FORECAST_QTY, foreCastQty);
	}
	public Long getActualQty() {
		return get(ACTUAL_QTY);
	}
	public void setActualQty(Long actualQty) {
		set(ACTUAL_QTY, actualQty);
	}
	public Float getSumCorrugateCost() {
		return get(SUM_CORRUGATE_COST);
	}
	public void setSumCorrugateCost(Float sumCorrugateCost) {
		set(SUM_CORRUGATE_COST, sumCorrugateCost);
	}
	public Float getSumFulFillmentCost() {
		return get(SUM_FULFILLMENT_COST);
	}
	public void setSumFulFillmentCost(Float sumFulFillmentCost) {
		set(SUM_FULFILLMENT_COST, sumFulFillmentCost);
	}
	public Float getSumOtherCost() {
		return get(SUM_OTHER_COST);
	}
	public void setSumOtherCost(Float sumOtherCost) {
		set(SUM_OTHER_COST, sumOtherCost);
	}
	public Float getSumTotDispCostNoShipping() {
		return get(SUM_TOT_DISP_COST_NOSHIPPING);
	}
	public void setSumTotDispCostNoShipping(Float sumTotDispCostNoShipping) {
		set(SUM_TOT_DISP_COST_NOSHIPPING, sumTotDispCostNoShipping);
	}
	public Float getSumTotDisplayCostShipping() {
		return get(SUM_TOT_DISPLAY_COST_SHIPPING);
	}
	public void setSumTotDisplayCostShipping(Float sumTotDisplayCostShipping) {
		set(SUM_TOT_DISPLAY_COST_SHIPPING, sumTotDisplayCostShipping);
	}
	public Float getShippingCost() {
		return get(SHIPPING_COST);
	}
	public void setShippingCost(Float shippingCost) {
		set(SHIPPING_COST, shippingCost);
	}
	public Float getSumTradeSales() {
		return get(SUM_TRADE_SALES);
	}
	public void setSumTradeSales(Float sumTradeSales) {
		set(SUM_TRADE_SALES, sumTradeSales);
	}
	public Float getCustProgDisc() {
		return get(CUST_PROG_DISC);
	}
	public void setCustProgDisc(Float custProgDisc) {
		set(CUST_PROG_DISC, custProgDisc);
	}
	public Float getSumNetSalesBp() {
		return get(SUM_NET_SALES_BP);
	}
	public void setSumNetSalesBp(Float sumNetSalesBp) {
		set(SUM_NET_SALES_BP, sumNetSalesBp);
	}
	public Float getSumNetSalesAp() {
		return get(SUM_NET_SALES_AP);
	}
	public void setSumNetSalesAp(Float sumNetSalesAp) {
		set(SUM_NET_SALES_AP, sumNetSalesAp);
	}
	public Float getSumCogs() {
		return get(SUM_COGS);
	}
	public void setSumCogs(Float sumCogs) {
		set(SUM_COGS, sumCogs);
	}
	public Float getSumProdVarMargin() {
		return get(SUM_PRODVAR_MARGIN);
	}
	public void setSumProdVarMargin(Float sumProdVarMargin) {
		set(SUM_PRODVAR_MARGIN, sumProdVarMargin);
	}
	public Float getSumDisplayCost() {
		return get(SUM_DISPLAY_COST);
	}
	public void setSumDisplayCost(Float sumDisplayCost) {
		set(SUM_DISPLAY_COST,sumDisplayCost);
	}
	public Float getSumVmDollarBp() {
		return get(SUM_VM_DOLLAR_BP);
	}
	public void setSumVmDollarBp(Float sumVmDollarBp) {
		set(SUM_VM_DOLLAR_BP, sumVmDollarBp);
	}
	public Float getSumVmDollarAp() {
		return get(SUM_VM_DOLLAR_AP);
	}
	public void setSumVmDollarAp(Float sumVmDollarAp) {
		set(SUM_VM_DOLLAR_AP, sumVmDollarAp);
	}
	public Float getSumVmPctBp() {
		return get(SUM_VM_PCT_BP);
	}
	public void setSumVmPctBp(Float sumVmPctBp) {
		set(SUM_VM_PCT_BP,sumVmPctBp);
	}
	public Float getSumVmPctAp() {
		return get(SUM_VM_PCT_AP);
	}
	public void setSumVmPctAp(Float sumVmPctAp) {
		set(SUM_VM_PCT_AP, sumVmPctAp);;
	}
	public Float getVmDollarHurdleRed() {
		return get(VM_DOLLAR_HURDLE_RED);
	}
	public void setVmDollarHurdleRed(Float vmDollarHurdleRed) {
		set(VM_DOLLAR_HURDLE_RED, vmDollarHurdleRed);
	}
	public Float getVmDollarHurdleGreen() {
		return get(VM_DOLLAR_HURDLE_GREEN);
	}
	public void setVmDollarHurdleGreen(Float vmDollarHurdleGreen) {
		set(VM_DOLLAR_HURDLE_GREEN, vmDollarHurdleGreen);
	}
	public Float getVmPctHurdleRed() {
		return get(VM_PCT_HURDLE_RED);
	}
	public void setVmPctHurdleRed(Float vmPctHurdleRed) {
		set(VM_PCT_HURDLE_RED, vmPctHurdleRed);
	}
	public Float getVmPctHurdleGreen() {
		return get(VM_PCT_HURDLE_GREEN);
	}
	public void setVmPctHurdleGreen(Float vmPctHurdleGreen) {
		set(VM_PCT_HURDLE_GREEN, vmPctHurdleGreen);
	}
	public Long getStatusId() {
		return get(STATUS_ID);
	}
	public void setStatusId(Long statusId) {
		set(STATUS_ID, statusId);
	}
	public Long getPromoPeriodId() {
		return get(PROMO_PERIOD_ID);
	}
	public void setPromoPeriodId(Long promoPeriodId) {
		set(PROMO_PERIOD_ID, promoPeriodId);
	}
	public Long getCustomerId() {
		return get(CUSTOMER_ID);
	}
	public void setCustomerId(Long customerId) {
		set(CUSTOMER_ID, customerId);
	}
	public String getSku() {
		return get(SKU);
	}
	public void setSku(String sku) {
		set(SKU, sku);
	}
	public String getDescription() {
		return get(DESCRIPTION);
	}
	public void setDescription(String description) {
		set(DESCRIPTION, description);
	}
	public String getAverySku() {
		return get(AVERY_SKU);
	}
	public void setAverySku(String averySku) {
		set(AVERY_SKU, averySku);
	}
	public Long getQtyPerFacing() {
		return get(QTY_PER_FACING);
	}
	public void setQtyPerFacing(Long qtyPerFacing) {
		set(QTY_PER_FACING, qtyPerFacing);
	}
	public Long getNumberFacing() {
		return get(NUMBER_FACINGS);
	}
	public void setNumberFacing(Long numberFacing) {
		set(NUMBER_FACINGS, numberFacing);
	}
	public Long getQtyPerDisplay() {
		return get(QTY_PER_DISPLAY);
	}
	public void setQtyPerDisplay(Long qtyPerDisplay) {
		set(QTY_PER_DISPLAY, qtyPerDisplay);
	}
	public Float getSumTotActualInvoice() {
		return get(SUM_TOT_ACTUAL_INVOICE);
	}
	public void setSumTotActualInvoice(Float sumTotActualInvoice) {
		set(SUM_TOT_ACTUAL_INVOICE, sumTotActualInvoice);
	}
	public String getHurdles() {
		return get(HURDLES);
	}
	public void setHurdles(String hurdles) {
		set(HURDLES, hurdles);
	}
	
	public void setExcessInvtVmDollar(Float excessInvtVmDollar) {
		set(EXCESS_INVT_VMDOLLAR, excessInvtVmDollar);
	}
	public Float getExcessInvtVmDollar() {
		return get(EXCESS_INVT_VMDOLLAR);
	}
	public void setExcessInvtVmPct(Float excessInvtVmPct) {
		set(EXCESS_INVT_VMPCT, excessInvtVmPct);
	}
	public Float getExcessInvtVmPct() {
		return get(EXCESS_INVT_VMPCT);
	}
	public Float getCorrugateCostPerUnit() {
		return get(CORRUGATE_COST_PER_UNIT);
	}
	
	public void setCorrugateCostPerUnit(Float corrugateCostPerUnit) {
		set(CORRUGATE_COST_PER_UNIT, corrugateCostPerUnit);
	}
	
	public Float getLaborCostPerUnit() {
		return get(LABOR_COST_PER_UNIT);
	}
	
	public void setLaborCostPerUnit(Float laborCostPerUnit) {
		set(LABOR_COST_PER_UNIT, laborCostPerUnit);
	}
	
	
	public Float getMdfPct() {
		return get(MDF_PCT);
	}
	
	public void setMdfPct(Float mdfPct) {
		set(MDF_PCT, mdfPct);
	}
	
	public Float getDiscountPct() {
		return get(DISCOUNT_PCT);
	}
	
	public void setDiscountPct(Float discountPct) {
		set(DISCOUNT_PCT, discountPct);
	}
	
	
}
