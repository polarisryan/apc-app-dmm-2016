package com.averydennison.dmm.app.client.models;

import com.extjs.gxt.ui.client.data.BaseModel;

public class LocationCountryDTO extends BaseModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6743467510667908682L;
	public static String LOCATION_ID = "locationId";
	public static String LOCATION_CODE = "locationCode";
	public static String LOCATION_NAME = "locationName";
	public static String ACTIVE_FLG = "activeFlg";
	public static String COUNTRY_ID = "countryId";
	public static String COUNTRY_NAME = "countryName";

	public LocationCountryDTO() {

	}

	public LocationCountryDTO(Long locationId, String locationCode,
			String locationName, Boolean activeFlg, Long countryId,
			String countryName) {
		set(LOCATION_ID, locationId);
		set(LOCATION_CODE, locationCode);
		set(LOCATION_NAME, locationName);
		set(ACTIVE_FLG, activeFlg);
		set(COUNTRY_ID, countryId);
		set(COUNTRY_NAME, countryName);
	}

	public String getLocationCode() {
		return get(LOCATION_CODE);
	}

	public void setLocationCode(String locationCode) {
		set(LOCATION_CODE, locationCode);
	}

	public Long getLocationId() {
		return get(LOCATION_ID);
	}

	public void setLocationId(Long locationId) {
		set(LOCATION_ID, locationId);
	}

	public String getLocationName() {
		return get(LOCATION_NAME);
	}

	public void setLocationName(String locationName) {
		set(LOCATION_NAME, locationName);
	}

	public Boolean isActiveFlg() {
		return get(ACTIVE_FLG);
	}

	public void setActiveFlg(Boolean activeFlg) {
		set(ACTIVE_FLG, activeFlg);
	}

	public Long getCountryId() {
		return get(COUNTRY_ID);
	}

	public void setCountryId(Long countryId) {
		set(COUNTRY_ID, countryId);
	}

	public String getCountryName() {
		return get(COUNTRY_NAME);
	}

	public void setCountryName(String countryName) {
		set(COUNTRY_NAME, countryName);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		LocationCountryDTO object = (LocationCountryDTO) o;

		if (getLocationId() != null ? !getLocationId().equals(
				object.getLocationId())
				: object.getLocationId() != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = getLocationId() != null ? getLocationId()
				.hashCode() : 0;
		return result;
	}

	public String toString() {
		return "Location{" + "locationId=" + this.getLocationId()
				+ ", locationCode='" + getLocationCode() + '\''
				+ ", locationName='" + this.getLocationName() + '\''
				+ ", countryId='" + this.getCountryId() + '\''
				+ ", countryName='" + this.getCountryName() + '\'' + '}';
	}
}
