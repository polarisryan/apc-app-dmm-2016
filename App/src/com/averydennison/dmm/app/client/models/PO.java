package com.averydennison.dmm.app.client.models;

import java.util.Date;

import com.extjs.gxt.ui.client.data.BaseTreeModel;

/**
 * User: Spart Arguello
 * Date: Nov 17, 2009
 * Time: 3:36:53 PM
 */
public class PO extends BaseTreeModel {
    public static final String TEST_NAME = "name";
    public static final String SHIP_WAVE_NUM = "shipWaveNum";
    public static final String MUST_ARRIVE_DATE = "mustArriveDate";
    public static final String SHIP_FROM_DATE = "shipFromDate";
    public static final String PRODUCT_DUE_DATE = "productDueDate";
    public static final String QUANTITY = "quantity";

    public PO() {
    }

    public PO(Integer shipWave, Date mustArriveDate, Date shipFromDate, Date packoutDueDate, Long quantity) {
        set(SHIP_WAVE_NUM, shipWave);
        set(MUST_ARRIVE_DATE, mustArriveDate);
        set(SHIP_FROM_DATE, shipFromDate);
        set(PRODUCT_DUE_DATE, packoutDueDate);
        set(QUANTITY, quantity);
        set(TEST_NAME, shipWave.toString());
    }

    public Integer getShipWaveNum() {
        return get(SHIP_WAVE_NUM);
    }

    public void setShipWaveNum(Integer shipWave) {
        set(SHIP_WAVE_NUM, shipWave);
    }

    public Date getMustArriveDate() {
        return get(MUST_ARRIVE_DATE);
    }

    public void setMustArriveDate(Date mustArriveDate) {
        set(MUST_ARRIVE_DATE, mustArriveDate);
    }

    public Date getShipFromDate() {
        return get(SHIP_FROM_DATE);
    }

    public void setShipFromDate(Date shipFromDate) {
        set(SHIP_FROM_DATE, shipFromDate);
    }

    public Date getPackoutDueDate() {
        return get(PRODUCT_DUE_DATE);
    }

    public void setPackoutDueDate(Date packoutDueDate) {
        set(PRODUCT_DUE_DATE, packoutDueDate);
    }

    public Long getQuantity() {
        return get(QUANTITY);
    }

    public void setQuantity(Long quantity) {
        set(QUANTITY, quantity);
    }
}
