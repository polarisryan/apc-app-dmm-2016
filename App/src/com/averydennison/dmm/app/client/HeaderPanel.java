package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;

/**
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 4:02:06 PM
 */
public class HeaderPanel extends LayoutContainer {
	public static final String ABOUT = "About";
	public static final String LOGOUT = "Logout";
	public static final String ADMIN = "Admin";
	public static final String PROJECTS = "Projects";
	
    public HeaderPanel() {
    	this(null);
    }
	
    public HeaderPanel(String activePage) {
        setLayout(new FlowLayout(10));

        ToolBar toolBar = new ToolBar();
        //toolBar.setSize(935, -1);
        toolBar.setSize(1230, -1);

        SelectionListener listener = new SelectionListener<ComponentEvent>() {

			@Override
			public void componentSelected(ComponentEvent ce) {
				Button button = (Button) ce.getComponent();
                if (button.getText().equals(ABOUT)) {
                	String msg = "";
        			msg += "<b>Avery Dennison Corp.</b><br/>";
        			msg += "<b>DMMS v.1</b><br/>";
        			msg += "<br/>";
        			msg += "User Id: " + App.getUser().getUsername() + "<br/>";
        			String[] auth = App.getUser().getAuthorities();
        			if(auth.length!=0 || auth !=null){
        				for (int i=0; i<auth.length; i++){
        					msg += "Authorities: " + auth[i] + "<br/>";
        			    }
        			}else{
        				msg += "Authorities: \n";
        			}
        			MessageBox.info("Display Marketing Management System", msg, null);
                }else if(button.getText().equals(LOGOUT)) {
                	Window.open(App.getServletPath() + "/j_spring_security_logout", "_self", ""); 
                }else if(button.getText().equals(ADMIN)) {
                	App.getVp().clear();
                    App.getVp().add(new DisplayAdminArea());
                	final MessageBox box = MessageBox.wait("Progress",
                    		"Loading Admin screen, please wait...", "Loading...");
                	Timer t = new Timer() {
                	    public void run() {
    			              box.close();
    			        }
    			    };
    			    t.schedule(1000);                    
                } else if(button.getText().equals(PROJECTS)) {
                	App.getVp().clear();
                    App.getVp().add(new DisplayProjectSelection());
                	final MessageBox box = MessageBox.wait("Progress",
                    		"Loading projects, please wait...", "Loading...");
                	Timer t = new Timer() {
                	    public void run() {
    			              box.close();
    			        }
    			    };
    			    t.schedule(1000);                    
                }
			}
        	
        };
        
        Button admin = new Button(ADMIN, listener);
        Button projects = new Button(PROJECTS, listener);
        Button report = new Button("Report");
        Button logout = new Button(LOGOUT, listener);
        Button about = new Button(ABOUT, listener);
        if(ADMIN.equals(activePage)){
        	admin.setEnabled(false);
        } else if(PROJECTS.equals(activePage)){
        	projects.setEnabled(false);
        }
        
        LabelToolItem logo = new LabelToolItem(App.IMAGE_STRING);

        toolBar.add(logo);
        toolBar.add(new SeparatorToolItem());
        if(App.isAuthorized("HeaderPanel.ADMIN")){
	        toolBar.add(admin);
	        toolBar.add(new SeparatorToolItem());
        }
        
//        toolBar.add(report);
//        toolBar.add(new SeparatorToolItem());
        toolBar.add(projects);
        toolBar.add(new SeparatorToolItem());
        
        toolBar.add(logout);
        toolBar.add(new SeparatorToolItem());

//        toolBar.add(about);

        toolBar.setBorders(true);
        add(toolBar);
    }
}
