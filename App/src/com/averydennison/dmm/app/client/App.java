package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.averydennison.dmm.app.client.models.UserDTO;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class App implements EntryPoint {
/*    public static final String IMAGE_STRING = "<img src=\"images/icons/200x78_logo_en_us.png\" width=\"200\" height=\"78\" border=\"2\" alt=\"Avery Dennison\" />";
*/	
/*	public static final String IMAGE_STRING = "<img src=\"images/icons/200x78_logo_en_us.png\" width=\"200\" height=\"78\" border=\"2\" alt=\"Avery Dennison\" />";
*/	
	public static final String IMAGE_STRING = "<img src=\"images/icons/200x78_logo_en_us.png\" width=\"200\" height=\"78\" border=\"2\" alt=\"Avery Dennison\" />";
	private static VerticalPanel vp = new VerticalPanel();
    private static UserDTO userDTO;
    private static LoginForm loginForm = new LoginForm();
    private static DisplayProjectSelection displayProjectSelection = new DisplayProjectSelection();
    private static HeaderPanel headerPanel = new HeaderPanel();
    private static FilterPanel filterPanel = new FilterPanel();
    private static SelectionGrid selectionGrid = new SelectionGrid();//TODO remove
    private static SelectionButtons selectionButtons = new SelectionButtons();
    private static DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
    private static DisplayTabs displayTabs = new DisplayTabs();

    public static UserDTO getUser() {
        return userDTO;
    }

    public static void setUser(UserDTO user) {
        userDTO = user;
    }

    public static DisplayTabs getDisplayTabs() {
		return displayTabs;
	}

	public static void setDisplayTabs(DisplayTabs displayTabs) {
		App.displayTabs = displayTabs;
	}

	public static boolean isAuthorized(String buttonKey){
	boolean isAuth = false;
    	if(userDTO!=null){
        	String[] cred = userDTO.getPermissions();
        	for(int i=0;i<cred.length;i++){
        		if(cred[i].equals(buttonKey)){
        			isAuth = true;
        		}
        	}
    	}else{
    		System.out.println("UserDTO is null");
    	}
    	return isAuth;
   }
    
    public static LoginForm getLoginForm() {
        return loginForm;
    }

    public static void setLoginForm(LoginForm loginForm) {
        App.loginForm = loginForm;
    }
    
    public static HeaderPanel getHeaderPanel() {
        return headerPanel;
    }

    public static void setHeaderPanel(HeaderPanel headerPanel) {
        App.headerPanel = headerPanel;
    }

    public static FilterPanel getFilterPanel() {
        return filterPanel;
    }

    public static void setFilterPanel(FilterPanel filterPanel) {
        App.filterPanel = filterPanel;
    }

    public static SelectionGrid getSelectionGrid() {
        return selectionGrid;
    }

    public static void setSelectionGrid(SelectionGrid selectionGrid) {
        App.selectionGrid = selectionGrid;
    }

    public static SelectionButtons getSelectionButtons() {
        return selectionButtons;
    }

    public static void setSelectionButtons(SelectionButtons selectionButtons) {
        App.selectionButtons = selectionButtons;
    }

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        AppServiceAsync createServiceOnLoad = AppService.App.getInstance();
        Service.getPromoPeriodMap();
        AppService.App.getInstance().getUserInfo(new Service.LoadUserInfo());
        AppService.App.getInstance().getServletPath(new Service.GetServletPath());
    }
    
    public static void loadApp(){
    	vp.add(new DisplayProjectSelection());
        RootPanel.get("ea").add(vp);
    }

    public static VerticalPanel getVp() {
        return vp;
    }

    public static void setVp(VerticalPanel vp) {
        App.vp = vp;
    }
    
    private static String filters;
    public static String getFilters(){
        return filters;
    }
    public static void setFilters(String filters){
        App.filters = filters;
    }
    
    private static CustomerCountryDTO customerFilter;

    public static CustomerCountryDTO getCustomerFilter() {
        return App.customerFilter;
    }

    public static void setCustomerFilter(CustomerCountryDTO customerFilter) {
        App.customerFilter = customerFilter;
    } 
    
    private static StructureDTO structureFilter;

    public static StructureDTO getStructureFilter() {
        return structureFilter;
    }

    public static void setStructureFilter(StructureDTO structureFilter) {
        App.structureFilter = structureFilter;
    }
    
    private static DisplaySalesRepDTO managerFilter;

    public static DisplaySalesRepDTO getManagerFilter() {
        return managerFilter;
    }

    public static void setManagerFilter(DisplaySalesRepDTO managerFilter) {
        App.managerFilter = managerFilter;
    }
    
    private static StatusDTO statusFilter;

    public static StatusDTO getStatusFilter() {
        return statusFilter;
    }

    public static void setStatusFilter(StatusDTO statusFilter) {
        App.statusFilter = statusFilter;
    }
    
    private static CountryDTO countryFilter;

    public static CountryDTO getCountryFilter() {
        return countryFilter;
    }

    public static void setCountryFilter(CountryDTO countryFilter) {
    	App.countryFilter = countryFilter;
    }
    
    private static String skuFilter;

    public static String getSkuFilter() {
        return skuFilter;
    }

    public static void setSkuFilter(String skuFilter) {
        App.skuFilter = skuFilter;
    }
    
    private static PromoPeriodCountryDTO promoPeriodFilter;

	public static PromoPeriodCountryDTO getPromoPeriodFilter() {
		return promoPeriodFilter;
	}

	public static void setPromoPeriodFilter(PromoPeriodCountryDTO promoPeriodFilter) {
		App.promoPeriodFilter = promoPeriodFilter;
	}

	private static String servletPath;

    public static String getServletPath() {
        return servletPath;
    }

    public static void setServletPath(String servletPath) {
        App.servletPath = servletPath;
    }
        
}
