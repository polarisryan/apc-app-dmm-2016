package com.averydennison.dmm.app.client;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.averydennison.dmm.app.client.models.BooleanDTO;
import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.averydennison.dmm.app.client.util.Constants;
import com.averydennison.dmm.app.client.util.KeyUtil;
import com.averydennison.dmm.app.client.util.StringUtil;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeEventSource;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.StoreEvent;
import com.extjs.gxt.ui.client.store.StoreListener;
import com.extjs.gxt.ui.client.widget.CheckBoxListView;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.Radio;
import com.extjs.gxt.ui.client.widget.form.RadioGroup;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 11:13:56 AM
 * TODO - externalize labelling and titling
 * TODO - this was a quick layout so once we start processing data through here, we may need to adjust
 * TODO - reorganize this class
 */
public class DisplayGeneralInfo extends LayoutContainer implements DirtyInterface {
    public static final String SAVE = "Save";
    public static final String COPY = "Copy";
    public static final String RETURN_TO_SELECTION = "Return to Selection";
    private DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
    private DisplayTabs displayTabs;
    private FormData formData;
    private VerticalPanel vp;
    private final TextField<String> sku = new TextField<String>();
    private final TextField<String> skuCheckDigit = new TextField<String>();
    private final ComboBox<StatusDTO> status = new ComboBox<StatusDTO>();
    private final ComboBox<StructureDTO> structure = new ComboBox<StructureDTO>();
    private final ComboBox<DisplaySalesRepDTO> projectManager = new ComboBox<DisplaySalesRepDTO>();
    private ComboBox<CustomerCountryDTO> customerCountry = new ComboBox<CustomerCountryDTO>();
    private ComboBox<ManufacturerCountryDTO> manufacturerCountry = new ComboBox<ManufacturerCountryDTO>();
    private ComboBox<CountryDTO> country = new ComboBox<CountryDTO>();
    private CheckBoxListView<DisplaySalesRepDTO> approvers = new CheckBoxListView<DisplaySalesRepDTO>();
    private ListStore<DisplaySalesRepDTO> approversStore = new ListStore<DisplaySalesRepDTO>();
    private final TextField<String> approver = new TextField<String>();
    private ComboBox<PromoPeriodCountryDTO> promoPeriodCountry = new ComboBox<PromoPeriodCountryDTO>();
    private final ComboBox<StatusDTO> rygStatusFlg = new ComboBox<StatusDTO>();
    private final TextField<Long> promoYear = new TextField<Long>();
    private final TextArea description = new TextArea();
    private RadioGroup radioGroup = new RadioGroup();
    private Radio kitRadio = new Radio();
    private Radio newUPC = new Radio();
    private Radio newProductionVersion = new Radio();
    private BooleanDTO isDuplicate;
    private final TextField<String> customerDisplayNumber = new TextField<String>();
    private final TextField<String> cmProjectNumber = new TextField<String>();
    public static Long DISPLAY_SETUP_KIT = 1L;
    public static Long DISPLAY_SETUP_FG_NEW_UPC = 2L;
    public static Long DISPLAY_SETUP_FG_NEW_PROD_VER = 3L;
    private ListStore<PromoPeriodCountryDTO> promoPeriodsCountryStore=new ListStore<PromoPeriodCountryDTO>();
    private ListStore<CustomerCountryDTO> customersCountryStore = new ListStore<CustomerCountryDTO>();
	private ListStore<ManufacturerCountryDTO> manufacturersCountryStore = new ListStore<ManufacturerCountryDTO>();

   
    private SelectionListener listener = new SelectionListener<ComponentEvent>() {
        /* (non-Javadoc)
         * @see com.extjs.gxt.ui.client.event.SelectionListener#componentSelected(com.extjs.gxt.ui.client.event.ComponentEvent)
         */
        public void componentSelected(ComponentEvent ce) {
            Button btn = (Button) ce.getComponent();
             if (btn.getText().equals(SAVE) && isDirty()) {
            	if (sku.getValue() != null && sku.getValue().length() >= 10) {
               		skuCheckDigit.setValue(getCheckSKUDigit(sku.getValue()));
               		setDuplicateSKU();
               	}else{
               		save(btn, null, null);
               	}
            } else if (btn.getText().equals(RETURN_TO_SELECTION)) {
            	if(isDirty()){
            		String title = "You have unsaved information!";
            		String msg = "Would you like to save?"; 		           			
            		MessageBox box = new MessageBox();
                    box.setButtons(MessageBox.YESNOCANCEL);
                    box.setIcon(MessageBox.QUESTION);
                    box.setTitle("Save Changes?");
                    box.addCallback(l);
                    box.setTitle(title);
                    box.setMessage(msg);
                    box.show();
            	}else{
            		forwardToDisplayProjectSelection();
            	}                
            }
        }
    };
    private final Button btnCopy = new Button(COPY, listener);
    
    private void forwardToDisplayProjectSelection(){
    	clearModel();
        App.getVp().clear();
        App.getVp().add(new DisplayProjectSelection());
    }
    private final Listener<MessageBoxEvent> l = new Listener<MessageBoxEvent>() {
        public void handleEvent(MessageBoxEvent mbe) {
            Button btn = mbe.getButtonClicked();
            if (btn.getText().equals("Yes")) {
              if (sku.getValue() != null && sku.getValue().length() >= 10) {
              		skuCheckDigit.setValue(getCheckSKUDigit(sku.getValue()));
              }
              	save(btn, null, null);
              	forwardToDisplayProjectSelection();
            }
            if (btn.getText().equals("No")) {
            	forwardToDisplayProjectSelection();
            }
            if (btn.getText().equals("Cancel")) {
            }
        }
    };
    
    public void save(Button btn, TabPanel panel, TabItem tabItem){
    	if (!sku.validate()) return;
        if (!status.validate()) return;
        if (!customerCountry.validate()) return;
        if (!manufacturerCountry.validate()) return;
        if (!structure.validate()) return;
        if (!projectManager.validate()) return;
        if (!promoPeriodCountry.validate()) return;
        if(!promoYear.validate()) {
        	MessageBox.alert("Promo Year Required", "Please enter Promo Year", null);
        	return;
        }
        if(!country.validate()) {
        	MessageBox.alert("Country Required", "Please enter country", null);
        	return;
        }
        if (!description.validate()) return;
        if(!customerDisplayNumber.validate()) return;
        if(!rygStatusFlg.validate()) return;
        model=getDisplayTabs().getDisplayModel();
        /*
         * By 			Marianandan Arockiasamy
         * Date 		8/5/2014
         * Description 	Office Depot has acquired Office Max. So there will be situation either enter SKU or not still allow saving the project.
         */
      /*  if (officeDepot() && !SKU() && country.getValue().getCountryName().equals(Constants.DISPLAY_COUNTRY_USA)) {
            MessageBox.alert("SKU not included", "Office Depot orders do not include SKU", null);
            return;
        }else*/ 
        if (staplesCanada() && !SKU() && country.getValue().getCountryName().equals(Constants.DISPLAY_COUNTRY_CAN)) {
                MessageBox.alert("SKU not included", "Staples Canada orders do not include SKU", null);
                return;
        } else if (isNewProductionVersion() && !SKU()) {
            MessageBox.alert("SKU not included", "SKU not included in Finished Goods - New Production Version displays", null);
            return;
        } else if (!isNewProductionVersion() && !officeDepot() && !officeDepotNorth() && country.getValue().getCountryName().equals(Constants.DISPLAY_COUNTRY_USA) && SKU() && !permanentFixture()) {
            MessageBox.alert("SKU Required", "Please enter SKU", null);
            return;
        } else if (!isNewProductionVersion() && !staplesCanada() && country.getValue().getCountryName().equals(Constants.DISPLAY_COUNTRY_CAN) && SKU() && !permanentFixture()) {
            MessageBox.alert("SKU Required", "Please enter SKU", null);
            return;
        } else if (model != null && isDirty()) {
        	skuCheckDigit.setValue(getCheckSKUDigit(sku.getValue()));
        	model.setSku(sku.getValue());
            model.setStatusId(status.getValue().getStatusId());
            if(customerCountry.getValue()!=null){
            	model.setCustomerId(customerCountry.getValue().getCustomerId());
           }
            model.setStructureId(structure.getValue().getStructureId());
            model.setDisplaySalesRepId(projectManager.getValue().getDisplaySalesRepId());
            if(manufacturerCountry.getValue()!=null) {
            	model.setManufacturerId(manufacturerCountry.getValue().getManufacturerId());
            }
            if(country.getValue()!=null) model.setCountryId(country.getValue().getCountryId());
            if(customerDisplayNumber.getValue()!=null) {
                model.setCustomerDisplayNumber(StringUtil.format(customerDisplayNumber.getValue()));
                customerDisplayNumber.setValue(StringUtil.format(customerDisplayNumber.getValue()));
            } else{
            	  model.setCustomerDisplayNumber(null);
                  customerDisplayNumber.clear();
            }
            if(cmProjectNumber.getValue()!=null) {
                model.setCmProjectNumber(StringUtil.format(cmProjectNumber.getValue()));
                cmProjectNumber.setValue(StringUtil.format(cmProjectNumber.getValue()));
            } else{
            	 model.setCmProjectNumber(null);
            	 cmProjectNumber.clear();
            }
            if(rygStatusFlg.getValue()!=null) {
            	model.setRygStatusFlg(rygStatusFlg.getValue().getStatusName());
            }
        	model.setApproverIds(getApproverIds());
            model.setPromoPeriodId(promoPeriodCountry.getValue().getPromoPeriodId());
            model.setPromoYear(getDefaultPromoYear());
            model.setDescription(StringUtil.format(description.getValue()));
            description.setValue(StringUtil.format(description.getValue()));
            model.setDisplaySetupId(getDisplaySetupId());
            model.setDtLastChanged(new Date());  
            model.setUserLastChanged(App.getUser().getUsername());
            model.setDtLastChanged(new Date());
            model.setUserLastChanged(App.getUser().getUsername());
        	if (model.getDisplayId() == null) {
                model.setDtCreated(new Date());
                model.setUserCreated(App.getUser().getUsername());
            }
            final MessageBox box = MessageBox.wait("Progress",
                    "Saving display general info, please wait...", "Saving...");
            AppService.App.getInstance().saveDisplayDTO(model,
            		new Service.SaveDisplayDTOAndLoadScenario(model, displayTabHeader,getDisplayTabs(), btn, box,panel,tabItem));
            if (applyKitVerSetup || applyFGDisplayRef){
           	 final MessageBox boxKit = MessageBox.wait("Progress",
                        "Saving kit component details, please wait...", "Saving...");
           	AppService.App.getInstance().switchOnDisplayTypesInKitComponent(model,	applyKitVerSetup, applyFGDisplayRef,
           		new Service.SwitchOnDisplayTypesInKitComponent( btn, boxKit));
           }
       }
        System.out.println("save display general info ..ends..");
    }
    
    private Set<Long> getApproverIds() {
		HashSet<Long> result = new HashSet<Long>();
		for(DisplaySalesRepDTO app: approvers.getChecked()){
			result.add(app.getDisplaySalesRepId());
		}
		return result;
	}

	private Long getDefaultPromoYear(){
    	if(promoYear.getRawValue()!=null && promoYear.getRawValue().length()>0) 
    		return Long.valueOf(promoYear.getRawValue());
    	else
    		return null;
    }
    
    private DisplayGeneralInfo getDisplayGeneralInfo() {
        return this;
    }

    private final Button actionButton = new Button(SAVE, listener);
    private DisplayDTO model;
    private ChangeListener modelListener;
  //  private ListStore<ManufacturerCountryDTO> manufacturerCountryStore = new ListStore<ManufacturerCountryDTO>();
    private ListStore<StatusDTO> rygStatusStore = new ListStore<StatusDTO>();
    private boolean applyKitVerSetup=false;
    private boolean applyFGDisplayRef=false;

    public Button getActionButton(){
    	return this.actionButton;
    }

    public DisplayGeneralInfo() {
        formData = new FormData("95%");
        vp = new VerticalPanel();
        vp.setSpacing(10);
    }
    
    private void setPermissions(){
    	if(!App.isAuthorized("DisplayGeneralInfo.SAVE")) actionButton.disable();
    }

    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        setModelsAndView();
        createHeader();
        createForm();
        setPermissions();
        setReadOnly();
        add(vp);
    }

    private void setModelsAndView() {
        model = displayTabs.getDisplayModel();
        if (model == null) {
            model = new DisplayDTO();
        }
        updateView();
        setReadOnly();
        setActionButton();
        modelListener = new ChangeListener() {
            public void modelChanged(com.extjs.gxt.ui.client.data.ChangeEvent event) {
                model = (DisplayDTO) event.getSource();
                if (model != null && model.getDisplayId() != null) {
                    updateView();
                }
            }
        };
        model.addChangeListener(modelListener);
    }

    private void clearModel() {
        if (model != null) model.removeChangeListener(modelListener);
        model = null;
        displayTabs.resetDisplayModel();
        setData("display", null);
    }

    private void setReadOnly() {
        Boolean b = this.getData("readOnly");
        if (b == null) return;
        sku.setReadOnly(b);
        status.setReadOnly(b);
        customerCountry.setReadOnly(b);
        structure.setReadOnly(b);
        projectManager.setReadOnly(b);
        promoPeriodCountry.setReadOnly(b);
        promoYear.setReadOnly(b);
        description.setReadOnly(b);
        manufacturerCountry.setReadOnly(b);
        country.setReadOnly(b);
        approvers.setEnabled(!b);
        rygStatusFlg.setReadOnly(b);
        radioGroup.setReadOnly(b);
        cmProjectNumber.setReadOnly(b);
        customerDisplayNumber.setReadOnly(b);
        if(b){
        	kitRadio.removeAllListeners();
        	newUPC.removeAllListeners();
        	newProductionVersion.removeAllListeners();
        }
        if (b) actionButton.disable();
    }

    public boolean checkDuplicateSkuExists(){
    	System.out.println("DisplayGeneralInfo:checkDuplicateSkuExists starts: " );
    	if(isDuplicate !=null){
    		System.out.println(" displayTabs.displayGeneralInfo.isDuplicate " + isDuplicate.getValue());
    	}else{
    		System.out.println(" displayTabs.displayGeneralInfo.isDuplicate NULL " );
    	}
    	return  isDuplicate.getValue();
    }
    
    private void setActionButton() {
      /*
    	 * By:Mari
    	 * Date:01/19/2010
    	 * Description: Implementation of display copy feature
    	 * By default copy all the records associalted to display, including 
    	 * general info, fulfillment, kit component, cust marketing, disp attributes, etc.,
    	 */  
    	Boolean b = this.getData("copy");
        if (b == null) return;
        if (b) {
            model = DisplayDTO.CopyDisplayDTO(model);
            actionButton.enable();
            description.setReadOnly(false);
            status.setValue(Service.getStatusMap().get(1L));
        }
     }

    private void setEmptyText() {
        sku.setEmptyText("Enter SKU...");
        sku.focus();
        skuCheckDigit.setEmptyText("SKU must be entered above...");
        status.setEmptyText("Select a status");
        customerCountry.setEmptyText("Select a customer");
        structure.setEmptyText("Select a structure");
        projectManager.setEmptyText("Select Project Manager...");
        manufacturerCountry.setEmptyText("Select Manufacturer...");
        country.setEmptyText("Select Country...");
        rygStatusFlg.setEmptyText("Select RYG Status...");
        promoPeriodCountry.setEmptyText("Select Promo Period...");
        promoYear.setEmptyText("Enter Promo Year...");
        cmProjectNumber.setEmptyText("CM Project #...");
        customerDisplayNumber.setEmptyText("Customer Display #...");
        kitRadio.setValue(true);
    }
       private void updateView() {
        if (model != null && model.getDisplayId() != null) {
            displayTabHeader.setDisplay(model);
            sku.setValue(model.getSku());
            skuCheckDigit.setValue(getCheckSKUDigit(model.getSku()));
            status.setValue(Service.getStatusMap().get(model.getStatusId()));
            customerCountry.setValue(Service.getCustomerCountryMap().get(model.getCustomerId()));
            structure.setValue(Service.getStructureMap().get(model.getStructureId()));
            manufacturerCountry.setValue(Service.getManufacturerCountryMap().get(model.getManufacturerId()));
            country.setValue(Service.getActiveCountryFlagMap().get(model.getCountryId()));
            if(country.getValue() != null){
            	approversStore.filter(DisplaySalesRepDTO.COUNTRY_NAME, Service.getCountryMap().get(country.getValue().getCountryId()).getCountryName());
            } else {
            	approversStore.clearFilters();
			}
            projectManager.setValue(Service.getProjectManagerMap().get(model.getDisplaySalesRepId()));
            promoPeriodCountry.setValue(Service.getPromoPeriodCountryMap().get(model.getPromoPeriodId()));
            promoYear.setValue(model.getPromoYear());
            description.setValue(model.getDescription());
            customerDisplayNumber.setValue(model.getCustomerDisplayNumber());
            cmProjectNumber.setValue(model.getCmProjectNumber());
            checkApprovers(model);
            if (model.getDisplaySetupId() != null) setDisplaySetupId(model.getDisplaySetupId());
            else kitRadio.setValue(true);
        } else {
        	Double currYear=(System.currentTimeMillis()/1000/3600/24/365.25 +1970);
            promoYear.setValue(model.getPromoYear()==null ?currYear.longValue(): model.getPromoYear());
            setEmptyText();
        }
    }

	public void checkApprovers(DisplayDTO modelDTO) {
		if(modelDTO.getApproverIds() != null){
			for(DisplaySalesRepDTO approverDTO: approversStore.getModels()){
				approvers.setChecked(approverDTO, modelDTO.getApproverIds().contains(approverDTO.getDisplaySalesRepId()));
			}
		}
	}
    
    private void createHeader() {
        add(displayTabHeader);
    }

    private void createForm() {
        FormPanel formPanel = new FormPanel();
        formPanel.setHeaderVisible(false);
        formPanel.setFrame(true);
        formPanel.setSize(1190, 435);
        formPanel.setLabelAlign(FormPanel.LabelAlign.TOP);
        formPanel.setButtonAlign(Style.HorizontalAlignment.CENTER);
        LayoutContainer main = new LayoutContainer();
        main.setLayout(new ColumnLayout());
        LayoutContainer left = new LayoutContainer();
        left.setStyleAttribute("paddingRight", "10px");
        FormLayout layout = new FormLayout();
        layout.setLabelAlign(FormPanel.LabelAlign.TOP);
        left.setLayout(layout);
        LayoutContainer middle = new LayoutContainer();
        middle.setStyleAttribute("paddingRight", "10px");
        FormLayout middleLayout = new FormLayout();
        middleLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        middle.setLayout(middleLayout);
        LabelField spaceLabel = new LabelField();
		spaceLabel.setLabelSeparator(" ");
		spaceLabel.setFieldLabel(" ");
		spaceLabel.setWidth(85);
		
        LayoutContainer skuLayoutContainer = new LayoutContainer();
        skuLayoutContainer.setStyleAttribute("paddingRight", "10px");
        layout = new FormLayout();
        layout.setLabelAlign(FormPanel.LabelAlign.TOP);
        skuLayoutContainer.setLayout(layout);
        
        LayoutContainer right = new LayoutContainer();
        right.setStyleAttribute("paddingLeft", "10px");
        layout = new FormLayout();
        layout.setLabelAlign(FormPanel.LabelAlign.TOP);
        right.setLayout(layout);
        
        sku.setFieldLabel("SKU");
        sku.setAllowBlank(true);
        sku.setMinLength(15);
        sku.setMaxLength(15);
        sku.addListener(Events.KeyDown, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
                int keyCode = ke.getKeyCode();
                if (!KeyUtil.isDigit(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress() && !ke.isSpecialKey()) {
                    ke.stopEvent();
                }
            }
        });
        left.add(sku, formData);
        skuCheckDigit.setFieldLabel("SKU Check Digit");
        skuCheckDigit.setAllowBlank(true);
        skuCheckDigit.setReadOnly(true);
        skuCheckDigit.setEnabled(false);
        skuCheckDigit.setMinLength(1);
        skuCheckDigit.setMaxLength(15);
        skuCheckDigit.setWidth(17);
        skuLayoutContainer.add(skuCheckDigit);
        left.add(skuLayoutContainer);
//        middle.add(spaceLabel, formData);
        
        AppService.App.getInstance().getApproverDTOs(new Service.SetProjectManagerStore(approversStore));
        approvers.setStore(approversStore);  
        approvers.setDisplayProperty(DisplaySalesRepDTO.FULL_NAME);
        approvers.setWidth(200);
        approvers.setHeight(200);
        approvers.setStyleAttribute("margin-top", "-7px");
        approvers.setStyleAttribute("margin-bottom", "10px");
        middle.add(new LabelField(){{setFieldLabel("Approvers:");}});
        middle.add(approvers);
        
        approver.setFieldLabel("Approver");
        approver.setAllowBlank(true);
        approver.setReadOnly(true);
        middle.add(approver);

        ListStore<StatusDTO> statuses = new ListStore<StatusDTO>();
        Service service = new Service();
        service.setStatusStore(statuses);
        ListStore<StructureDTO> structures = new ListStore<StructureDTO>();
        AppService.App.getInstance().getStructureDTOs(new Service.SetStructuresStore(structures));

        ListStore<DisplaySalesRepDTO> displaySalesReps = new ListStore<DisplaySalesRepDTO>();
        AppService.App.getInstance().getDisplaySalesRepDTOs(new Service.SetProjectManagerStore(displaySalesReps));
        
        AppService.App.getInstance().getRYGStatusDTOs(new Service.LoadRYGStatusStore(this));
        
        ListStore<CountryDTO> countries = new ListStore<CountryDTO>();
        AppService.App.getInstance().getCountryFlagDTOs(new Service.SetCountryStore(countries));
        ListStore<LocationCountryDTO> locationsCountry = new ListStore<LocationCountryDTO>();
        AppService.App.getInstance().getLocationCountryDTOs(new Service.SetLocationCountryStore(this.model.getCountryId()==null?null:model.getCountryId(),locationsCountry));
        //service.setCustomerCountryStore(this.model.getCountryId()==null?null:model.getCountryId(),customersCountryStore);
        AppService.App.getInstance().getCustomerCountryDTOs(new Service.SetCustomerCountryStore(this.model.getCountryId()==null?null:model.getCountryId(),customersCountryStore));
        AppService.App.getInstance().getPromoPeriodCountryDTOs(new Service.SetPromoPeriodsCountryStore( this.model.getCountryId()==null?null:model.getCountryId(), promoPeriodsCountryStore));
    	AppService.App.getInstance().getManufacturerCountryDTOs(new Service.SetManufacturersCountryStore(this.model.getCountryId()==null?null:model.getCountryId(),manufacturersCountryStore));
        
    	country.setFieldLabel("Country");
        country.setDisplayField(CountryDTO.COUNTRY_NAME);
        country.setStore(countries);
        //country.setWidth(15);
        country.setWidth(10);
        country.setAllowBlank(false);
        country.setEditable(false);
        country.setTriggerAction(ComboBox.TriggerAction.ALL);
        left.add(country,  formData);
        middle.add(spaceLabel, formData);
        
        status.setFieldLabel("Status");
        status.setDisplayField(StatusDTO.STATUS_NAME);
        //status.setWidth(15);
        status.setWidth(10);
        status.setStore(statuses);
        status.setAllowBlank(false);
        status.setEditable(false);
        status.setTriggerAction(ComboBox.TriggerAction.ALL);
        left.add(status, formData);
        middle.add(spaceLabel, formData);
        
        customerCountry.setFieldLabel("Customer");
        customerCountry.setDisplayField(CustomerCountryDTO.CUSTOMER_NAME);
        //customerCountry.setWidth(15);
        customerCountry.setWidth(10);
        customerCountry.setStore(customersCountryStore);
        customerCountry.setAllowBlank(false);
        customerCountry.setEditable(false);
        customerCountry.setTriggerAction(ComboBox.TriggerAction.ALL);
        left.add(customerCountry, formData);
        middle.add(spaceLabel, formData);

        structure.setFieldLabel("Structure Type");
        structure.setDisplayField(StructureDTO.STRUCTURE_NAME);
        //structure.setWidth(15);
        structure.setWidth(10);
        structure.setStore(structures);
        structure.setAllowBlank(false);
        structure.setEditable(false);
        structure.setTriggerAction(ComboBox.TriggerAction.ALL);
        left.add(structure, formData);
        middle.add(spaceLabel, formData);
        projectManager.setFieldLabel("Project Manager");
        projectManager.setDisplayField(DisplaySalesRepDTO.FULL_NAME);
        //projectManager.setWidth(15);
        projectManager.setWidth(10);
        projectManager.setStore(displaySalesReps);
        projectManager.setAllowBlank(false);
        projectManager.setEditable(false);
        projectManager.setTriggerAction(ComboBox.TriggerAction.ALL);
        left.add(projectManager, formData);
        middle.add(spaceLabel, formData);
        
        

        manufacturerCountry.setFieldLabel("Corrugate Vendor");
        manufacturerCountry.setDisplayField(ManufacturerCountryDTO.NAME);
        manufacturerCountry.setStore(manufacturersCountryStore);
        //manufacturerCountry.setWidth(15);
        manufacturerCountry.setWidth(10);
        manufacturerCountry.setAllowBlank(false);
        manufacturerCountry.setEditable(false);
        manufacturerCountry.setTriggerAction(ComboBox.TriggerAction.ALL);
        left.add(manufacturerCountry,  formData);
        middle.add(spaceLabel, formData);
        
        rygStatusFlg.setFieldLabel("RYG Status");
	    rygStatusFlg.setDisplayField(StatusDTO.DESCRIPTION);
	    rygStatusFlg.setValueField(StatusDTO.DESCRIPTION);
	    rygStatusFlg.setStore(rygStatusStore);
	   // rygStatusFlg.setWidth(15);
	    rygStatusFlg.setWidth(8);
	    rygStatusFlg.setAllowBlank(false);
	    rygStatusFlg.setEditable(false);
	    rygStatusFlg.setTriggerAction(ComboBox.TriggerAction.ALL);
        left.add(rygStatusFlg, formData);
        middle.add(spaceLabel, formData);
     
        promoPeriodCountry.setFieldLabel("Promo Period");
        promoPeriodCountry.setDisplayField(PromoPeriodCountryDTO.PROMO_PERIOD_NAME);
        promoPeriodCountry.setWidth(100);
        promoPeriodCountry.setStore(promoPeriodsCountryStore);
        promoPeriodCountry.setAllowBlank(false);
        promoPeriodCountry.setEditable(false);
        promoPeriodCountry.setTriggerAction(ComboBox.TriggerAction.ALL);
        
        right.add(promoPeriodCountry, formData);
        promoYear.setFieldLabel("Promo Year");
        promoYear.setWidth(100);
        promoYear.setAllowBlank(false);
        promoYear.setMinLength(4);
        promoYear.setMaxLength(4);
        promoYear.addListener(Events.KeyDown, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
                int keyCode = ke.getKeyCode();
                if (!KeyUtil.isDigit(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress()) {
                    ke.stopEvent();
                }
            }
        });
        right.add(promoYear, formData);
        description.setAllowBlank(false);
        description.setMinLength(1);
        description.setMaxLength(200);
        description.setFieldLabel("Description");
        right.add(description, formData);
        
        customerDisplayNumber.setAllowBlank(true);
        customerDisplayNumber.setAutoValidate(true);
        customerDisplayNumber.setMinLength(1);
        customerDisplayNumber.setMaxLength(30);
        customerDisplayNumber.setFieldLabel("Customer Display #");
        right.add(customerDisplayNumber, formData);
        kitRadio.setBoxLabel("Kit");
        newUPC.setBoxLabel("Finished Goods - New UPC");
        newProductionVersion.setBoxLabel("Finished Goods - New Production Version");
    	model=getDisplayTabs().getDisplayModel();
            if (model.getDisplaySetupId()!=null) {
	        	newUPC.addListener(Events.OnClick, new Listener<FieldEvent>() {
	             	String title = "Change Display Setup Type";
	                public void handleEvent(FieldEvent be) {
		                if (model.getDisplaySetupId().equals(Long.valueOf(DISPLAY_SETUP_KIT)) ) {
		                	 String msg = "Changing from Kit to Finished Goods - New UPC will delete Kit Component data";      
		                     MessageBox box = new MessageBox();
		                     box.setButtons(MessageBox.YESNO);
		                     box.setIcon(MessageBox.QUESTION);
		                     box.setTitle(title);
		                     box.addCallback(new Listener<MessageBoxEvent>() {
	                			 public void handleEvent(MessageBoxEvent mbe) {
	                				 Button btn = mbe.getButtonClicked();
	                				 if (btn.getText().equals("Yes")) {
	                					 newUPC.setValue(true);
	                					 applyKitVerSetup=false;
	                					 applyFGDisplayRef=true;
	                				 }else{
	                					 kitRadio.setValue(true);
	                					 applyKitVerSetup=false;
	                					 applyFGDisplayRef=false;
	                				 }
	                			 }
	                		 });
		                     box.setTitle(title);
		                     box.setMessage(msg);
		                     box.show();
		                }else if (model.getDisplaySetupId().equals(Long.valueOf(DISPLAY_SETUP_FG_NEW_PROD_VER))){
		                	 String msg = "Changing from Finished Goods - New Product Version to FG - New UPC will delete Kit Component data";                             
		                     MessageBox box = new MessageBox();
		                     box.setButtons(MessageBox.YESNO);
		                     box.setIcon(MessageBox.QUESTION);
		                     box.setTitle(title);
		                     box.addCallback(new Listener<MessageBoxEvent>() {
	                			 public void handleEvent(MessageBoxEvent mbe) {
	                				 Button btn = mbe.getButtonClicked();
	                				 if (btn.getText().equals("Yes")) {
	                					 applyKitVerSetup=false;
	                					 applyFGDisplayRef=true;
	                				 }else{
	                					 newProductionVersion.setValue(true);
	                					 applyKitVerSetup=false;
	                					 applyFGDisplayRef=false;
	                				 }
	                			 }
	                		 });
		                     box.setTitle(title);
		                     box.setMessage(msg);
		                     box.show();
		                }
		            }
		        });
	        			kitRadio.addListener(Events.OnClick, new Listener<FieldEvent>() {
	                 	String title = "Change Display Setup Type";
	    	            public void handleEvent(FieldEvent be) {
	    	                if (model.getDisplaySetupId().equals(Long.valueOf(DISPLAY_SETUP_FG_NEW_PROD_VER)) ) {
	    	                	 String msg = "Changing from Finished Goods - New Production Version to Kit will delete FG Display reference data";                             
	    	                     MessageBox box = new MessageBox();
	    	                     box.setButtons(MessageBox.YESNO);
	    	                     box.setIcon(MessageBox.QUESTION);
	    	                     box.setTitle(title);
	    	                     box.addCallback(new Listener<MessageBoxEvent>() {
	                    			 public void handleEvent(MessageBoxEvent mbe) {
	                    				 Button btn = mbe.getButtonClicked();
	                    				 if (btn.getText().equals("Yes")) {
	                    					 applyKitVerSetup=true;
	                    					 applyFGDisplayRef=false;
	                    				 }else{
	                    					 newProductionVersion.setValue(true);
	                    					 applyKitVerSetup=false;
	                    					 applyFGDisplayRef=false;
	                    				 }
	                    			 }
	                    		 });
	    	                     box.setTitle(title);
	    	                     box.setMessage(msg);
	    	                     box.show();
	    	                }else if (model.getDisplaySetupId().equals(Long.valueOf(DISPLAY_SETUP_FG_NEW_UPC))){
	    	                	 String msg = "Changing from Finished Goods - New UPC to kit will delete FG Display Reference data";                             
	    	                     MessageBox box = new MessageBox();
	    	                     box.setButtons(MessageBox.YESNO);
	    	                     box.setIcon(MessageBox.QUESTION);
	    	                     box.setTitle(title);
	    	                     box.addCallback(new Listener<MessageBoxEvent>() {
	                    			 public void handleEvent(MessageBoxEvent mbe) {
	                    				 Button btn = mbe.getButtonClicked();
	                    				 if (btn.getText().equals("Yes")) {
	                    				     applyKitVerSetup=true;
	                    					 applyFGDisplayRef=false;
	                    				 }else{
	                    					 newUPC.setValue(true);
	                    					 applyKitVerSetup=false;
	                    					 applyFGDisplayRef=false;
	                    				 }
	                    			 }
	                    		 });
	    	                     box.setTitle(title);
	    	                     box.setMessage(msg);
	    	                     box.show();
	    	                }
	    	            }
	    	        });
	        		newProductionVersion.addListener(Events.OnClick, new Listener<FieldEvent>() {
	             	String title = "Change Display Setup Type";
		            public void handleEvent(FieldEvent be) {
		                if (model.getDisplaySetupId().equals(Long.valueOf(DISPLAY_SETUP_KIT)) ) {
		                	 String msg = "Changing from Kit to Finished Goods - New Production Version will delete FG Display reference data";                             
		                     MessageBox box = new MessageBox();
		                     box.setButtons(MessageBox.YESNO);
		                     box.setIcon(MessageBox.QUESTION);
		                     box.setTitle(title);
		                     box.addCallback(new Listener<MessageBoxEvent>() {
	                			 public void handleEvent(MessageBoxEvent mbe) {
	                				 Button btn = mbe.getButtonClicked();
	                				 if (btn.getText().equals("Yes")) {
	                					 applyKitVerSetup=true;
	                					 applyFGDisplayRef=false;
	                				 }else{
	                					 kitRadio.setValue(true);
	                					 applyKitVerSetup=false;
	                					 applyFGDisplayRef=false;
	                				 }
	                			 }
	                		 });
		                     box.setTitle(title);
		                     box.setMessage(msg);
		                     box.show();
		                }else if (model.getDisplaySetupId().equals(Long.valueOf(DISPLAY_SETUP_FG_NEW_UPC))){
		                	 String msg = "Changing from Finished Goods - New UPC to FG - New Product Version will delete FG Display reference data";                             
		                     MessageBox box = new MessageBox();
		                     box.setButtons(MessageBox.YESNO);
		                     box.setIcon(MessageBox.QUESTION);
		                     box.setTitle(title);
		                     box.addCallback(new Listener<MessageBoxEvent>() {
	                			 public void handleEvent(MessageBoxEvent mbe) {
	                				 Button btn = mbe.getButtonClicked();
	                				 if (btn.getText().equals("Yes")) {
	                					 newUPC.setValue(false);
	                					 applyKitVerSetup=true;
	                					 applyFGDisplayRef=false;
	                				 }else{
	                					 applyKitVerSetup=false;
	                					 applyFGDisplayRef=false;
	                					 newUPC.setValue(true);
	                				 }
	                			 }
	                		 });
		                     box.setTitle(title);
		                     box.setMessage(msg);
		                     box.show();
		                }
		            }
		        });
	        }
        radioGroup.add(kitRadio);
        radioGroup.add(newUPC);
        radioGroup.add(newProductionVersion);
        FieldSet displaySetupFieldSet = new FieldSet();
        displaySetupFieldSet.setHeading("Display Setup Type");
        displaySetupFieldSet.add(kitRadio);
        displaySetupFieldSet.add(newUPC);
        displaySetupFieldSet.add(newProductionVersion);
        right.add(displaySetupFieldSet, formData);
        
        cmProjectNumber.setAllowBlank(true);
        cmProjectNumber.setAutoValidate(true);
        cmProjectNumber.setMinLength(1);
        cmProjectNumber.setMaxLength(40);
        cmProjectNumber.setFieldLabel("CM Project #");
        right.add(cmProjectNumber, formData);
        main.add(left, new ColumnData(.20));
        main.add(middle, new ColumnData(.30));
        main.add(right, new ColumnData(.50));
        formPanel.add(main);
        country.addListener(Events.Select, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
            	customerCountry.setValue(null);
            	manufacturerCountry.setValue(null);
            	promoPeriodCountry.setValue(null);
            	if(country.getValue() != null){
                    approversStore.filter(DisplaySalesRepDTO.COUNTRY_NAME, Service.getCountryMap().get(country.getValue().getCountryId()).getCountryName());
            	}
            }
        });
        
        
        promoPeriodCountry.addListener(Events.Expand, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
            	if(country.getValue()!=null){
            		promoPeriodsCountryStore.filter(PromoPeriodCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(country.getValue().getCountryId()).getCountryName());
            	}
            	promoPeriodCountry.setEnabled(true);
            }
        });
        
        manufacturerCountry.addListener(Events.Expand, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
            	if(country.getValue()!=null){
            		manufacturersCountryStore.filter(ManufacturerCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(country.getValue().getCountryId()).getCountryName());
            	}
            	manufacturerCountry.setEnabled(true);
            }
        });
        customerCountry.addListener(Events.Expand, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent ke) {
            	if(country.getValue()!=null){
                    customersCountryStore.filter(CustomerCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(country.getValue().getCountryId()).getCountryName());
            	}
            	customerCountry.setEnabled(true);
            }
        });
        
       /* customerCountry.addSelectionChangedListener(new SelectionChangedListener<CustomerCountryDTO>() {
            public void selectionChanged(SelectionChangedEvent<CustomerCountryDTO> se) {
            	customerCountry.setValue(se.getSelectedItem());
            }
        });
        
        manufacturerCountry.addSelectionChangedListener(new SelectionChangedListener<ManufacturerCountryDTO>() {
            public void selectionChanged(SelectionChangedEvent<ManufacturerCountryDTO> se) {
            	manufacturerCountry.setValue(se.getSelectedItem());
            }
        });*/
        
        formPanel.addButton(new Button(RETURN_TO_SELECTION, listener));
        formPanel.addButton(actionButton);
        vp.add(formPanel);
    }

    public String getCheckSKUDigit(String sku) {
        if (sku == null) return "";
        //Check for leading zero.  If no leading zero add it.
        if (!sku.startsWith("0")) {
            sku = "0" + sku;
        }
        char[] ca = sku.toCharArray();
        int odds = 0;
        int evens = 0;
        for (int i = 0; i <= 10; i++) {
            int digit = Character.digit(ca[i], 10);
            if (i % 2 == 0) {
                odds += digit;
            } else {
                evens += digit;
            }
        }
        int diff = (odds * 3) + evens;
        String sd = String.valueOf(diff);
        char[] cdiffs = sd.toCharArray();
        int lastDigitOfDiff = Character.digit(cdiffs[cdiffs.length - 1], 10);

        int checkDigit;
        if (lastDigitOfDiff == 0) {
            checkDigit = lastDigitOfDiff;
        } else {
            checkDigit = 10 - lastDigitOfDiff;
        }

        return Integer.toString(checkDigit);
    }

    public void reload() {
        this.model = displayTabs.getDisplayModel();
        overrideDirty = false;
        if (this.model != null && this.model.getDisplayId() != null) {
            AppService.App.getInstance().getDisplayDTO(this.model, new Service.ReloadDisplay(this.model));
        }
        updateView();
        btnCopy.disable();
    }

    public DisplayTabs getDisplayTabs() {
        return displayTabs;
    }

    public void setDisplayTabs(DisplayTabs displayTabs) {
        this.displayTabs = displayTabs;
        this.displayTabs.onComponentEvent(new ComponentEvent(this){});       
    }
    
    private boolean permanentFixture(){
    	return structure.getValue().getStructureId().equals(Long.valueOf(19));
    }
    private boolean officeDepot() {
        return customerCountry.getValue().getCustomerName().trim().equalsIgnoreCase("Office Depot");
    }
    private boolean officeDepotNorth() {
        return customerCountry.getValue().getCustomerName().trim().equalsIgnoreCase("Office Depot North");
    }
    
    private boolean staplesCanada() {
        return customerCountry.getValue().getCustomerName().trim().equalsIgnoreCase("Staples Canada");
    }
    private boolean SKU() {
        return sku.getValue() == null || sku.getValue().trim().equals("");
    }

    private boolean isNewProductionVersion() {
        return newProductionVersion.getValue();
    }

    private Long getDisplaySetupId() {
        if (kitRadio.getValue()) return DISPLAY_SETUP_KIT ;
        if (newUPC.getValue()) return DISPLAY_SETUP_FG_NEW_UPC ;
        return DISPLAY_SETUP_FG_NEW_PROD_VER; //newProductionVersion
    }

    private void setDisplaySetupId(Long displaySetupId) {
        if (displaySetupId.equals(Long.valueOf(DISPLAY_SETUP_KIT))) kitRadio.setValue(true);
        if (displaySetupId.equals(Long.valueOf(DISPLAY_SETUP_FG_NEW_UPC))) newUPC.setValue(true);
        if (displaySetupId.equals(Long.valueOf(DISPLAY_SETUP_FG_NEW_PROD_VER ))) newProductionVersion.setValue(true);
    }
    
    public void overrideDirty() {
        overrideDirty = true;
    }
    private boolean overrideDirty = false;
    public boolean isDirty() {
        if(overrideDirty==true) return false;
    	return !different();
    }
	private boolean different(){
	    return this.model.equalsDisplayGeneralInfo(getCurrentModelFromControls()); 
    }

	private DisplayDTO getCurrentModelFromControls(){
	    DisplayDTO controlModel = new DisplayDTO();
	    if(sku.getValue()!=null) controlModel.setSku(sku.getValue());
	    if(status.getValue()!=null) controlModel.setStatusId(status.getValue().getStatusId());
	    if(customerCountry.getValue()!=null) {
	    	controlModel.setCustomerId(customerCountry.getValue().getCustomerId());
/*	    	customerCountry.setValue(customerCountry.getValue());
*/	    }
	    if(structure.getValue()!=null) controlModel.setStructureId(structure.getValue().getStructureId());
	    if(projectManager.getValue()!=null) controlModel.setDisplaySalesRepId(projectManager.getValue().getDisplaySalesRepId());
	    if(manufacturerCountry.getValue()!=null){
	    	controlModel.setManufacturerId(manufacturerCountry.getValue().getManufacturerId());
/*	    	manufacturerCountry.setValue(manufacturerCountry.getValue());
*/	    }
	    if(rygStatusFlg.getValue()!=null) controlModel.setRygStatusFlg(rygStatusFlg.getValue().getStatusName());
	    if(promoPeriodCountry.getValue()!=null) controlModel.setPromoPeriodId(promoPeriodCountry.getValue().getPromoPeriodId());
	    if(getDefaultPromoYear()!=null) controlModel.setPromoYear(getDefaultPromoYear());
	    if(description.getValue()!=null) controlModel.setDescription(description.getValue());
	    if(customerDisplayNumber.getValue()!=null){
	    	controlModel.setCustomerDisplayNumber(customerDisplayNumber.getValue());
	    }else{
	    	controlModel.setCustomerDisplayNumber(null);
	    }
	    if(this.model.getQtyForecast()!=null) controlModel.setQtyForecast(this.model.getQtyForecast());
	    if(cmProjectNumber.getValue()!=null) {
	    	controlModel.setCmProjectNumber(cmProjectNumber.getValue());
	    }else{
	    	controlModel.setCmProjectNumber(null);
	    }
	    boolean radiosDirty = kitRadio.isDirty() || newUPC.isDirty() || newProductionVersion.isDirty();
	    if(this.model.getDisplaySetupId()!=null) controlModel.setDisplaySetupId(getDisplaySetupId());
	    else if(this.model.getDisplaySetupId()==null && radiosDirty) controlModel.setDisplaySetupId(getDisplaySetupId());
	    if(country.getValue()!=null) controlModel.setCountryId(country.getValue().getCountryId());
	    controlModel.setApproverIds(getApproverIds());
	    return controlModel;
	}
	
	private void setDuplicateSKU(){
         AppService.App.getInstance().isDuplicateSKU(sku.getValue(),
         		new Service.IsDuplicateSKU(this));
	}
	
	public void setDuplicateSKU(BooleanDTO isDuplicate){
		this.isDuplicate = isDuplicate;
	}
	
	public DisplayDTO getDisplayModel(){
	    return this.model;
	}

    public ComboBox<ManufacturerCountryDTO> getManufacturerCountry() {
        return manufacturerCountry;
    }
    
    public ComboBox<StatusDTO> getRYGStatusFlg() {
        return rygStatusFlg;
    }
    
    public ComboBox<CustomerCountryDTO> getCustomerCountry() {
        return customerCountry;
    }
    

    public TextField<Long> getPromoYear() {
		return promoYear;
	}

    public ListStore<ManufacturerCountryDTO> getManufacturersCountryStore() {
        return manufacturersCountryStore;
    }
    
    public void setManufacturersCountryStore( ListStore<ManufacturerCountryDTO> manufacturersCountryStore ) {
    	this.manufacturersCountryStore=manufacturersCountryStore  ;
    }
    
    public ListStore<CustomerCountryDTO> getCustomersCountryStore() {
        return customersCountryStore;
    }
    
    public void setCustomersCountryStore( ListStore<CustomerCountryDTO> customersCountryStore ) {
    	this.customersCountryStore=customersCountryStore  ;
    }
    
    
    public ListStore<PromoPeriodCountryDTO> getPromoPeriodsCountry(){
    	return promoPeriodsCountryStore;
    }
    
    public void setPromoPeriodsCountry(ListStore<PromoPeriodCountryDTO> promoPeriodsCountry ){
    	this.promoPeriodsCountryStore=promoPeriodsCountry;
    }
    
	public ListStore<StatusDTO> getRYGStatusStore() {
        return rygStatusStore;
    }
    
    public void setSkuFocus(){
		sku.focus();
	}
    
    public void copyDisplayGeneralInfoRecord(){
    	      final MessageBox box = MessageBox.wait("Progress",
                 "Copying display and other associated records. Please wait...", "Copying...");
       AppService.App.getInstance().copyDisplayDTO(model, App.getUser().getUsername(),
    	                new Service.SaveCopiedDisplayDTO(getDisplayGeneralInfo(),model, displayTabHeader, btnCopy, box));  
       model = displayTabs.getDisplayModel();
    }
}