package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.google.gwt.user.client.Element;
import com.gwtext.client.widgets.MessageBox;

/**
 * User: Spart Arguello
 * Date: Dec 21, 2009
 * Time: 2:04:13 PM
 */
public class DisplayTabHeaderV2 extends LayoutContainer {

    private VerticalPanel vp;
    private DisplayDTO display;
    private TextField<String> skuTextField = new TextField<String>();
    private TextField<String> desc = new TextField<String>();
    private TextField<String> structureTextField = new TextField<String>();
    private TextField<String> custTextField = new TextField<String>();
    private DateField goNoGoTextField = new DateField();
    private TextField<String> statusTextField = new TextField<String>();
    private TextField<String> actualQtyTextField = new TextField<String>();
    private CheckBox skuMixFinalCheckBox = new CheckBox();
    private TextField<String> displayIdTextField = new TextField<String>();
    private TextField<String> countryTextField = new TextField<String>();
    private TextField<String> cmProjectNoTextField = new TextField<String>();
    private TextField<String> promoPeriodTextField = new TextField<String>();
    
    private SlimDisplayPackoutGrid slimDisplayPackoutGrid = new SlimDisplayPackoutGrid();

    public DisplayTabHeaderV2() {
        vp = new VerticalPanel();
        vp.setSpacing(10);
    }

    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        createHeader();
        add(vp);
    }

    private void createHeader() {

        HorizontalPanel hp1 = new HorizontalPanel();
        HorizontalPanel hp2 = new HorizontalPanel();
        HorizontalPanel hp3 = new HorizontalPanel();
        HorizontalPanel hp4 = new HorizontalPanel();
        HorizontalPanel hp5 = new HorizontalPanel();
        HorizontalPanel hp6 = new HorizontalPanel();
        HorizontalPanel hp7 = new HorizontalPanel();
        HorizontalPanel hp8 = new HorizontalPanel();
        HorizontalPanel hp9 = new HorizontalPanel();
        HorizontalPanel hp10 = new HorizontalPanel();
        HorizontalPanel hp11 = new HorizontalPanel();
        HorizontalPanel hp12 = new HorizontalPanel();
        HorizontalPanel hp13 = new HorizontalPanel();
        
        VerticalPanel vp1 = new VerticalPanel();
        VerticalPanel vp2 = new VerticalPanel();
		VerticalPanel vp3 = new VerticalPanel();
        VerticalPanel vp4 = new VerticalPanel();
        
        hp1.setSpacing(2);
        hp2.setSpacing(2);
        hp3.setSpacing(2);
        hp4.setSpacing(2);
        hp5.setSpacing(2);
        hp6.setSpacing(2);
        hp7.setSpacing(2);
        hp8.setSpacing(2);
        hp9.setSpacing(2);
        hp10.setSpacing(2);
        hp11.setSpacing(2);
        hp12.setSpacing(2);
        hp13.setSpacing(2);
        
        vp1.setSpacing(2);
        vp2.setSpacing(2);
        vp3.setSpacing(2);
        vp4.setSpacing(2);

        ContentPanel panel = new ContentPanel();
        panel.setHeaderVisible(false);
        panel.setLayout(new ColumnLayout());
        panel.setSize(1190, 140);
        panel.setFrame(true);
        panel.setCollapsible(true);

        LabelField skuLabel = new LabelField("SKU:");
        skuLabel.setReadOnly(true);
        skuLabel.setWidth(50);
        hp1.add(skuLabel);

        skuTextField.setWidth(120);
        skuTextField.setFieldLabel("skuTextField");
        skuTextField.setAllowBlank(true);
        skuTextField.setReadOnly(true);
        skuTextField.setEnabled(false);
        skuTextField.setMinLength(15);
        skuTextField.setMaxLength(15);
        hp1.add(skuTextField);
        vp1.add(hp1);

        LabelField custLabel = new LabelField("CST:");
        custLabel.setFieldLabel("customerLabel");
        custLabel.setWidth(50);
        custLabel.setReadOnly(true);
        hp2.add(custLabel);

        custTextField.setWidth(120);
        custTextField.setFieldLabel("custTextField");
        custTextField.setAllowBlank(true);
        custTextField.setReadOnly(true);
        custTextField.setEnabled(false);
        custTextField.setMinLength(15);
        custTextField.setMaxLength(15);
        hp2.add(custTextField);
        vp1.add(hp2);
        
        LabelField displayIdLabel = new LabelField("Disp#:");
        displayIdLabel.setFieldLabel("displayIdLabel");
        displayIdLabel.setWidth(50);
        displayIdLabel.setReadOnly(true);
        hp9.add(displayIdLabel);

        displayIdTextField.setWidth(50);
        displayIdTextField.setFieldLabel("displayIdTextField");
        displayIdTextField.setAllowBlank(true);
        displayIdTextField.setReadOnly(true);
        displayIdTextField.setEnabled(false);
        displayIdTextField.setMinLength(15);
        displayIdTextField.setMaxLength(15);
        hp9.add(displayIdTextField);
        vp1.add(hp9);

        LabelField countryLabel = new LabelField("Country:");
        countryLabel.setFieldLabel("countryLabel");
        countryLabel.setWidth(50);
        countryLabel.setReadOnly(true);
        hp13.add(countryLabel);

        countryTextField.setWidth(120);
        countryTextField.setFieldLabel("countryTextField");
        countryTextField.setAllowBlank(true);
        countryTextField.setReadOnly(true);
        countryTextField.setEnabled(false);
        countryTextField.setMinLength(15);
        countryTextField.setMaxLength(15);
        hp13.add(countryTextField);
        vp1.add(hp13);
        
        desc.setWidth(175);
        desc.setFieldLabel("Description");
        desc.setAllowBlank(true);
        desc.setReadOnly(true);
        desc.setEnabled(false);
        hp3.add(desc);
        vp2.add(hp3);

        LabelField structureLabel = new LabelField("Struct:");
        structureLabel.setFieldLabel("structureLabel");
        structureLabel.setWidth(50);
        structureLabel.setReadOnly(true);
        hp4.add(structureLabel);

        structureTextField.setWidth(125);
        structureTextField.setFieldLabel("structureTextField");
        structureTextField.setAllowBlank(true);
        structureTextField.setReadOnly(true);
        structureTextField.setEnabled(false);
        structureTextField.setMinLength(15);
        structureTextField.setMaxLength(15);
        hp4.add(structureTextField);
        vp2.add(hp4);

        
        LabelField statusLabel = new LabelField("Status:");
        statusLabel.setFieldLabel("statusLabel");
        statusLabel.setWidth(60);
        statusLabel.setReadOnly(true);
        hp5.add(statusLabel);

        statusTextField.setWidth(64);
        statusTextField.setFieldLabel("statusTextField");
        statusTextField.setAllowBlank(true);
        statusTextField.setReadOnly(true);
        statusTextField.setEnabled(false);
        statusTextField.setMinLength(15);
        statusTextField.setMaxLength(15);
        hp5.add(statusTextField);
        vp3.add(hp5);

        LabelField actualQtyLabel = new LabelField("Actual Qty:");
        actualQtyLabel.setFieldLabel("actualQtyLabel");
        actualQtyLabel.setWidth(60);
        actualQtyLabel.setReadOnly(true);
        hp6.add(actualQtyLabel);

        actualQtyTextField.setWidth(54);
        actualQtyTextField.setFieldLabel("actualQtyTextField");
        actualQtyTextField.setAllowBlank(true);
        actualQtyTextField.setReadOnly(true);
        actualQtyTextField.setEnabled(false);
        actualQtyTextField.setMinLength(15);
        actualQtyTextField.setMaxLength(15);
        hp6.add(actualQtyTextField);
        vp3.add(hp6);

        LabelField goNoGoLabel = new LabelField("Go/No Go:");
        goNoGoLabel.setFieldLabel("goNoGoLabel");
        goNoGoLabel.setWidth(65);
        goNoGoLabel.setReadOnly(true);
        hp7.add(goNoGoLabel);

        goNoGoTextField.setWidth(75);
        goNoGoTextField.setFieldLabel("goNoGoTextField");
        goNoGoTextField.setAllowBlank(true);
        goNoGoTextField.setReadOnly(true);
        goNoGoTextField.setEnabled(false);
        hp7.add(goNoGoTextField);
        vp4.add(hp7);

        skuMixFinalCheckBox.setBoxLabel("SKU Mix Final");
        skuMixFinalCheckBox.setValue(false);
        skuMixFinalCheckBox.setWidth(100);
        skuMixFinalCheckBox.setReadOnly(true);
        skuMixFinalCheckBox.setEnabled(false);
        hp8.add(skuMixFinalCheckBox);
        vp4.add(hp8);
        
        LabelField cmProjectNoLabel = new LabelField("CM Project#:");
        cmProjectNoLabel.setFieldLabel("cmProjectNoLabel");
        cmProjectNoLabel.setWidth(75);
        cmProjectNoLabel.setReadOnly(true);
        hp10.add(cmProjectNoLabel);

        cmProjectNoTextField.setWidth(100);
        cmProjectNoTextField.setFieldLabel("cmProjectNoTextField");
        cmProjectNoTextField.setAllowBlank(true);
        //cmProjectNoTextField.addInputStyleName("text-element-no-underline");	
        cmProjectNoTextField.setReadOnly(true);
        cmProjectNoTextField.setEnabled(false);
        cmProjectNoTextField.setMinLength(15);
        cmProjectNoTextField.setMaxLength(15);
        hp10.add(cmProjectNoTextField);
        vp2.add(hp10);
      
        LabelField promoPeriodLabel = new LabelField("Promo Period:");
        promoPeriodLabel.setFieldLabel("promoPeriodLabel");
        promoPeriodLabel.setWidth(115);
        promoPeriodLabel.setStyleName("text-element-no-underline");	
        promoPeriodLabel.setReadOnly(true);
        hp11.add(promoPeriodLabel);
        vp3.add(hp11);
        
        promoPeriodTextField.setWidth(140);
        promoPeriodTextField.setFieldLabel("promoPeriodTextField");
        //promoPeriodTextField.addInputStyleName("text-element-no-underline");	
        promoPeriodTextField.setAllowBlank(true);
        promoPeriodTextField.setReadOnly(true);
        promoPeriodTextField.setEnabled(false);
        hp12.add(promoPeriodTextField);
        vp4.add(hp12);
        
       /* panel.add(vp1, new ColumnData(160));
        panel.add(vp2, new ColumnData(185));
        panel.add(vp3, new ColumnData(145));
        panel.add(vp4, new ColumnData(145));*/
        panel.add(vp1, new ColumnData(185));
        panel.add(vp2, new ColumnData(185));
        panel.add(vp3, new ColumnData(145));
        panel.add(vp4, new ColumnData(145));
        panel.add(slimDisplayPackoutGrid, new ColumnData(350));
        vp.add(panel);
    }

    public void setDisplay(DisplayDTO display) {
        this.display = display;
        updateView();
    }

    public void setSkuMixFinal(Boolean mix) {
        skuMixFinalCheckBox.setValue(mix);
    }

   
   /* public String getCmProjectNo() {
		return cmProjectNo;
	}

	public void setCmProjectNo(String cmProjectNo) {
		this.cmProjectNo = cmProjectNo;
	}*/

	private void updateView() {
        if (display != null && display.getDisplayId() != null) {
            skuTextField.setEmptyText(display.getSku());
            custTextField.setEmptyText(Service.getCustomerCountryMap().get(display.getCustomerId()).getCustomerName());
            desc.setEmptyText(display.getDescription());
            structureTextField.setEmptyText(Service.getStructureMap().get(display.getStructureId()).getStructureName());
            goNoGoTextField.setValue(display.getDtGoNoGo());
            statusTextField.setEmptyText(Service.getStatusMap().get(display.getStatusId()).getStatusName());
            if (display.getActualQty() != null){
                actualQtyTextField.setEmptyText(display.getActualQty().toString());
            }
            skuMixFinalCheckBox.setValue(display.isSkuMixFinalFlg());
            displayIdTextField.setEmptyText(display.getDisplayId().toString());
            if(display.getCmProjectNumber()!=null){
            	cmProjectNoTextField.setEmptyText(display.getCmProjectNumber());
            }
            promoPeriodTextField.setEmptyText(Service.getPromoPeriodCountryMap().get(display.getPromoPeriodId()).getPromoPeriodName());
           if(display.getCountryId()!=null && Service.getActiveCountryFlagMap().size()>0) {
        	   countryTextField.setEmptyText(Service.getActiveCountryFlagMap().get(display.getCountryId()).getCountryName());
           }else{
        	   System.out.println("Error in setting CountryId in DisplayTabHeaderV2");
           }
            loadSlimDisplayPackoutGrid(display);
        }
    }

    public void loadSlimDisplayPackoutGrid(DisplayDTO display) {
        slimDisplayPackoutGrid.load(display);
    }
}
