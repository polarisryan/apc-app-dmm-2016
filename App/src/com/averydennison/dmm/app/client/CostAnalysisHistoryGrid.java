package com.averydennison.dmm.app.client;
import java.util.ArrayList;
import java.util.List;
import com.averydennison.dmm.app.client.models.CostAnalysisHistoryDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
public class CostAnalysisHistoryGrid extends LayoutContainer {
	private static String DISPLAY_COST_TABLE = "display_cost";
	ListStore<CostAnalysisHistoryDTO> store = new ListStore<CostAnalysisHistoryDTO>();
	public Grid<CostAnalysisHistoryDTO> historyGrid;
	ContentPanel cpHistoryPanel;
	public ContentPanel createHistoryForm(Long displayId, Long displayScenarioId){
		System.out.println("CostAnalysisHistory:createHistoryForm starts");
		cpHistoryPanel = new ContentPanel();
		try{
		setLayout(new FlowLayout(5));
		setStyleAttribute("paddingLeft","5px");
		setStyleAttribute("paddingRight","5px");
		setStyleAttribute("margin", "2px");
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
		ColumnConfig column = new ColumnConfig();
		column.setId("history");
		column.setHeader("History");
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setWidth(430);
		column.setFixed(true);
		configs.add(column);
		reloadHistory(displayId,displayScenarioId);
		ColumnModel cm = new ColumnModel(configs);
		cpHistoryPanel.setBodyBorder(false);
		cpHistoryPanel.setHeaderVisible(false);
		cpHistoryPanel.setButtonAlign(HorizontalAlignment.CENTER);
		cpHistoryPanel.setLayout(new FitLayout());
		cpHistoryPanel.setSize(460, 75);
		historyGrid = new Grid<CostAnalysisHistoryDTO>(store, cm);
		historyGrid.setStyleAttribute("borderTop", "none");
		historyGrid.setStyleName("nowrapgrid");
		historyGrid.setBorders(true);
		historyGrid.setHideHeaders(true);
		historyGrid.setSelectionModel(new GridSelectionModel<CostAnalysisHistoryDTO>());
		historyGrid.setAutoWidth(true);
		cpHistoryPanel.add(historyGrid);
		}catch(Exception ex){
			System.out.println("=======CostAnalysisHistoryGrid:createHistoryForm error "+ ex.toString());
		}
		System.out.println("CostAnalysisHistory:createHistoryForm ends");
		return cpHistoryPanel;
	}
	public void reloadHistory(Long displayId, Long displayScenarioId){
		AppService.App.getInstance().getUserChangeLogDTO(displayId,displayScenarioId,DISPLAY_COST_TABLE,new Service.ReloadCostAnalysisHistory(store,this));
	}
}
