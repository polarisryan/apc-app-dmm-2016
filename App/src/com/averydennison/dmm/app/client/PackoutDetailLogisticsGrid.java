package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.KitComponent;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;

/**
 * User: Spart Arguello
 * Date: Oct 27, 2009
 * Time: 11:43:40 AM
 */
public class PackoutDetailLogisticsGrid extends LayoutContainer {
    public PackoutDetailLogisticsGrid() {
        setLayout(new FlowLayout(10));

        int x = 100;

        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        ColumnConfig column = new ColumnConfig();
        column.setId("packOutLocation");
        column.setHeader("Pack Out Location");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("productDueAtPackOut");
        column.setHeader("Product Due @ PackOut");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x + 50);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("shipFromLocation");
        column.setHeader("Ship From Location");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x + 25);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("shipFromDt");
        column.setHeader("Ship From Dt");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("qty");
        column.setHeader("Qty");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x / 2);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("numberShipToLocs");
        column.setHeader("# Ship To Locs");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        //TODO call server
        ListStore<KitComponent> store = new ListStore<KitComponent>();
        store.add(PackoutDetailLogisticsGrid.getCostPricing());

        ColumnModel cm = new ColumnModel(configs);

        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        //cp.setHeading("Main Displays");
        cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
        cp.setLayout(new FitLayout());
        cp.setSize(650, 150);

        Grid<KitComponent> grid = new Grid<KitComponent>(store, cm);
        grid.setColumnResize(true);
        grid.setStyleAttribute("borderTop", "none");
        grid.setStripeRows(true);
        grid.setBorders(true);
        cp.add(grid);

        add(cp);
    }

    public static List<KitComponent> getCostPricing() {
        List<KitComponent> list = new ArrayList<KitComponent>();
        for (int i = 0; i < 2; i++) {
            list.add(new KitComponent());
        }
        return list;
    }
}
