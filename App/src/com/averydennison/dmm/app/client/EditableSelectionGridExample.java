package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.SimpleComboBox;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * User: Spart Arguello
 * Date: Oct 15, 2009
 * Time: 4:49:58 PM
 */
public class EditableSelectionGridExample extends LayoutContainer {
    public static final String DATE_PATTERN = "MM/dd/y";
    private int displayCount = 1;

    public EditableSelectionGridExample() {
        setLayout(new FlowLayout(10));

        final SimpleComboBox<String> customerComboBox = new SimpleComboBox<String>();
        customerComboBox.setTriggerAction(ComboBox.TriggerAction.ALL);
        customerComboBox.add("Office Depot");
        customerComboBox.add("Office Max");
        customerComboBox.add("Staples");

        CellEditor CustomerEditor = new CellEditor(customerComboBox) {
            @Override
            public Object preProcessValue(Object value) {
                if (value == null) {
                    return value;
                }
                return customerComboBox.findModel(value.toString());
            }

            @Override
            public Object postProcessValue(Object value) {
                if (value == null) {
                    return value;
                }
                return ((ModelData) value).get("value");
            }
        };

        final SimpleComboBox<String> statusComboBox = new SimpleComboBox<String>();
        statusComboBox.setTriggerAction(ComboBox.TriggerAction.ALL);
        statusComboBox.add("Proposed");
        statusComboBox.add("Approved");
        statusComboBox.add("Cancelled");
        statusComboBox.add("Completed");

        CellEditor statusEditor = new CellEditor(statusComboBox) {
            @Override
            public Object preProcessValue(Object value) {
                if (value == null) {
                    return value;
                }
                return statusComboBox.findModel(value.toString());
            }

            @Override
            public Object postProcessValue(Object value) {
                if (value == null) {
                    return value;
                }
                return ((ModelData) value).get("value");
            }
        };

        final SimpleComboBox<String> projectManagerComboBox = new SimpleComboBox<String>();
        projectManagerComboBox.setTriggerAction(ComboBox.TriggerAction.ALL);
        projectManagerComboBox.add("McIver Robert");
        projectManagerComboBox.add("Johnson Kara");

        CellEditor projectManagerEditor = new CellEditor(projectManagerComboBox) {
            @Override
            public Object preProcessValue(Object value) {
                if (value == null) {
                    return value;
                }
                return projectManagerComboBox.findModel(value.toString());
            }

            @Override
            public Object postProcessValue(Object value) {
                if (value == null) {
                    return value;
                }
                return ((ModelData) value).get("value");
            }
        };

        final SimpleComboBox<String> structureComboBox = new SimpleComboBox<String>();
        structureComboBox.setTriggerAction(ComboBox.TriggerAction.ALL);
        structureComboBox.add("8-Facing Octagon");
        structureComboBox.add("Half-Pallet");
        structureComboBox.add("Tray");

        CellEditor structureEditor = new CellEditor(structureComboBox) {
            @Override
            public Object preProcessValue(Object value) {
                if (value == null) {
                    return value;
                }
                return structureComboBox.findModel(value.toString());
            }

            @Override
            public Object postProcessValue(Object value) {
                if (value == null) {
                    return value;
                }
                return ((ModelData) value).get("value");
            }
        };

        final SimpleComboBox<String> destinationComboBox = new SimpleComboBox<String>();
        destinationComboBox.setTriggerAction(ComboBox.TriggerAction.ALL);
        destinationComboBox.add("DOM");
        destinationComboBox.add("OS");

        CellEditor destinationEditor = new CellEditor(destinationComboBox) {
            @Override
            public Object preProcessValue(Object value) {
                if (value == null) {
                    return value;
                }
                return destinationComboBox.findModel(value.toString());
            }

            @Override
            public Object postProcessValue(Object value) {
                if (value == null) {
                    return value;
                }
                return ((ModelData) value).get("value");
            }
        };

        int x = 100;
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        ColumnConfig column = new ColumnConfig();
        //column.setId(DisplayDTO.CUSTOMER);
        column.setHeader("Customer");

        column.setEditor(CustomerEditor);
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x - 25);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.SKU);
        column.setHeader("SKU");

        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.DESCRIPTION);
        column.setHeader("Description");

        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x * 3);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.DESTINATION);
        column.setHeader("Dest");

        column.setEditor(destinationEditor);
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x / 2);
        configs.add(column);

        column = new ColumnConfig();
        //column.setId(DisplayDTO.STRUCTURE);
        column.setHeader("Structure");

        column.setEditor(structureEditor);
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);


        column = new ColumnConfig();
//        column.setId(DisplayDTO.PROJECT_MANAGER);
        column.setHeader("Project Manager");

        column.setEditor(projectManagerEditor);
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x - 20);
        configs.add(column);

        column = new ColumnConfig();
//        column.setId(DisplayDTO.STATUS);
        column.setHeader("Status");

        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x / 2);
        column.setEditor(statusEditor);
        configs.add(column);

        DateField dateField = new DateField();
        dateField.getPropertyEditor().setFormat(DateTimeFormat.getFormat(DATE_PATTERN));

        column = new ColumnConfig();
//        column.setId(DisplayDTO.SHIP_DATE);
        column.setHeader("Ship Date");

        column.setWidth(x / 2);
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setDateTimeFormat(DateTimeFormat.getFormat(DATE_PATTERN));
        column.setEditor(new CellEditor(dateField));
        configs.add(column);

        column = new ColumnConfig();
//        column.setId(DisplayDTO.UPDATE_DATE);
        column.setHeader("Last Upd Date");

        column.setWidth(x / 2);
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        configs.add(column);

        //TODO call server
        final ListStore<DisplayDTO> store = new ListStore<DisplayDTO>();
        //store.add(SelectionGrid.getDisplays());
        store.commitChanges();

        ColumnModel cm = new ColumnModel(configs);

        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
        cp.setFrame(true);
        cp.setLayout(new FitLayout());
        cp.setSize(900, 300);

        final EditorGrid<DisplayDTO> grid = new EditorGrid<DisplayDTO>(store, cm);
        grid.setColumnResize(true);
        grid.setStyleAttribute("borderTop", "none");
        grid.setStripeRows(true);
        grid.setBorders(true);
        cp.add(grid);

        SelectionListener listener = new SelectionListener<ComponentEvent>() {
            public void componentSelected(ComponentEvent ce) {
                Button btn = (Button) ce.getComponent();
                //Info.display("Click Event", "The '{0}' button was clicked.", btn.getText());
            }
        };

        SelectionListener create = new SelectionListener<ComponentEvent>() {
            public void componentSelected(ComponentEvent ce) {
                DisplayDTO d = new DisplayDTO();
//                d.setCustomer("customer" + displayCount++);
                grid.stopEditing();
                store.insert(d, 0);
                grid.startEditing(0, 0);
            }
        };

        SelectionListener create2 = new SelectionListener<ComponentEvent>() {
            public void componentSelected(ComponentEvent ce) {
                App.getVp().clear();
                App.getVp().add(new DisplayTabs());
            }
        };

        ButtonBar buttonBar = new ButtonBar();
        buttonBar.setSize(850, -1);
        buttonBar.setSpacing(10);
        buttonBar.add(new FillToolItem());
        buttonBar.add(new Button("View", listener));
        buttonBar.add(new Button("Edit", listener));
        buttonBar.add(new Button("Create", create2));
        buttonBar.add(new Button("Copy", listener));

        add(cp);

        add(buttonBar);
    }

    public static List<DisplayDTO> getDisplays() {
        List<DisplayDTO> list = new ArrayList<DisplayDTO>();
        for (int i = 0; i < 5; i++) {
            list.add(new DisplayDTO());
        }
        return list;
    }
}
