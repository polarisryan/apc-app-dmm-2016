package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.CustomerMarketingDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayForMassUpdateDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.SelectFieldToMassUpdateDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.i18n.client.DateTimeFormat;

public class MassUpdatePanel extends LayoutContainer {
	public static final String ALL = "All";
	private CustomerCountryDTO selectedCustomer;
	private StructureDTO selectedStructure;
	private DisplaySalesRepDTO selectedProjectManager;
    private StatusDTO selectedStatus;
    private PromoPeriodCountryDTO selectedPromoPeriod;
    private ManufacturerCountryDTO selectedCorrugate;
    private boolean selectedSkuMixFinal;
    private boolean selectedInitialRendering;
    private boolean selectedStructureApproved;
    private boolean selectedOrdersReceived;
    private Date selectedPriceAvailCompleteOnDt;
    private Date selectedFinalArtworkPODueDt;
    private Date selectedSampleToCustomerBy;
    private Date selectedPricingAdminValidationDt;
    private Date selectedPIMCompletedonDt;
    private String selectedComments;
    private Short selectedProjectConfidenceLevel;
     
    private final EditorGrid<DisplayForMassUpdateDTO> grid;
    private ComboBox<CustomerCountryDTO> customerCombo = new ComboBox<CustomerCountryDTO>();
    private ComboBox<StructureDTO> structureCombo = new ComboBox<StructureDTO>();
    private ComboBox<DisplaySalesRepDTO> projectManagerCombo = new ComboBox<DisplaySalesRepDTO>();
    private ComboBox<StatusDTO> statusCombo = new ComboBox<StatusDTO>();
    private ComboBox<ManufacturerCountryDTO> corrugateCombo = new ComboBox<ManufacturerCountryDTO>();
    private ComboBox<PromoPeriodCountryDTO> promoPeriodCombo = new ComboBox<PromoPeriodCountryDTO>();
    private DateField dateFieldFinalArtwork = new DateField();
    private DateField dateFieldPriceAvail = new DateField();
    private DateField dateFieldSampleToCustomer=new DateField();
    private DateField dateFieldPIMCompletedonDt=new DateField();
   private DateField  dateFieldPricingAdminValidationDt=new DateField();
   private TextField<String> txtFieldComments = new TextField<String>();
   private NumberField numFieldPCLevel = new NumberField();
   
    LabelField newValuesLabel = new LabelField();
    private ListStore<CustomerCountryDTO> customers = new ListStore<CustomerCountryDTO>();
    private ListStore<StructureDTO> structures = new ListStore<StructureDTO>();
    private ListStore<DisplaySalesRepDTO> projectManagers = new ListStore<DisplaySalesRepDTO>();
    private ListStore<StatusDTO> statuses = new ListStore<StatusDTO>();
    private ListStore<ManufacturerCountryDTO> corrugates = new ListStore<ManufacturerCountryDTO>();
    private ListStore<PromoPeriodCountryDTO> promoPeriods = new ListStore<PromoPeriodCountryDTO>();
    
    private CheckBox skuFinalCheckBox =  new CheckBox();;
    
    private CellEditor statusEditor;
    private CellEditor customerEditor;
    private CellEditor projectManagerEditor;
    private CellEditor structureEditor;
    private CellEditor corrugateEditor;
    private CellEditor promoPeriodEditor;
    
    public static final String DATE_PATTERN = "MM/dd/y";
    List<ColumnConfig> configs = new ArrayList<ColumnConfig>();  
	ColumnConfig column = new ColumnConfig();
	private CellEditor checkBoxEditor;
	CheckColumnConfig checkColumn ;
	private boolean fieldsSelectedFlag=false;
	private SelectFieldToMassUpdateDTO selectFieldMassUpdate;
    
    public MassUpdatePanel( MassUpdateGrid massUpdateGrid,MassUpdatePanel masterUpdatePanel,SelectFieldToMassUpdateDTO selectFieldMassUpdate ){
    	setLayout(new FlowLayout(0));
    	configs = new ArrayList<ColumnConfig>();  
    	final int x=125;
    	this.selectFieldMassUpdate=selectFieldMassUpdate;
    	column = new ColumnConfig();
        column.setId(DisplayDTO.DISPLAY_ID);
        column.setHeader("ID");
        column.setSortable(false);
        column.setMenuDisabled(true);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x - 25);
        configs.add(column);
        
        column = new ColumnConfig();
        column.setId(DisplayDTO.CUSTOMER_BY_NAME);
        column.setHeader("Customer");
        column.setSortable(false);
        column.setMenuDisabled(true);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x - 25);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.SKU);
        column.setHeader("SKU");
        column.setSortable(false);
        column.setMenuDisabled(true);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x-25);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.DESCRIPTION);
        column.setHeader("Description");
        column.setAlignment(HorizontalAlignment.RIGHT);
        column.setWidth(x*2);
        column.setSortable(false);
        column.setFixed(true);
        column.setMenuDisabled(true);
	        if(selectFieldMassUpdate.getNoOfFieldsForMassUpdate()>4){
	        	 newValuesLabel = new LabelField();
	             newValuesLabel.setLabelSeparator(" :");
	             newValuesLabel.setFieldLabel("newValues");
	             //column.setEditor(new CellEditor(newValuesLabel)); 
	             newValuesLabel.setReadOnly(true);
	             column.setRenderer(new GridCellRenderer() {
	         		@Override
	     			public Object render(ModelData model, String property,
	     					ColumnData config, int rowIndex, int colIndex,
	     					ListStore store, Grid grid) {
	         			newValuesLabel.setText("New Values: ");
	         			return  newValuesLabel;
	     			}
	             });
	        }
	    //column.addListener(Events.CellClick, null);
	    configs.add(column);
        //1. Customer
        	if(selectFieldMassUpdate.isbCustomer()){
		    	column = new ColumnConfig();
		        column.setHeader("Customer");
		        column.setAlignment(Style.HorizontalAlignment.LEFT);
		        column.setWidth(x);
                customers = new ListStore<CustomerCountryDTO>();
    	        AppService.App.getInstance().getCustomerCountryDTOs(new Service.SetCustomerCountryStore(null, customers));
    	        customerCombo = new ComboBox<CustomerCountryDTO>();
    	        customerCombo.setEditable(false);
    	        setSelectedCustomer(masterUpdatePanel.getSelectedCustomer());
    	        column.setSortable(false);
    	        column.setMenuDisabled(true);
    	         
    	         customerCombo.setFieldLabel("Customer");
		         customerCombo.setDisplayField(CustomerCountryDTO.CUSTOMER_NAME);
		         customerCombo.setValueField(CustomerCountryDTO.CUSTOMER_NAME);
		         customerCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
		         column.setEditor(customerEditor);
		         column.setId(CustomerCountryDTO.CUSTOMER_ID);
		         customerCombo.setEmptyText("Customer...");
	    	     customerCombo.setValue(getSelectedCustomer());
		         customerCombo.setStore(customers);
		         customerCombo.setWidth(x-15);
		         customerCombo.setTypeAhead(true);
		         customerCombo.setSelectOnFocus(true);
		         customerCombo.setSelection(customers.getModels());
		         column.setRenderer(new GridCellRenderer() {
	            		@Override
						public Object render(ModelData model, String property,
								ColumnData config, int rowIndex, int colIndex,
								ListStore store, Grid grid) {
	            			setFieldsSelectedFlag(true);
	            			customerCombo.setValue(getSelectedCustomer());
	 	    	            validateSelectedFields();
	            			return  customerCombo;
						}
	                });
	   	        configs.add(column);
        }
        //2. Structure
	        if(selectFieldMassUpdate.isbStructure()){
	        	 column = new ColumnConfig();
	             column.setHeader("Structure");
	             column.setAlignment(Style.HorizontalAlignment.LEFT);
	             column.setWidth(x);
	             structures = new ListStore<StructureDTO>();
	  	         AppService.App.getInstance().getStructureDTOs(new Service.SetStructuresStore(structures));
	  	         structureCombo = new ComboBox<StructureDTO>();
	  	         column.setSortable(false);
	  	         column.setMenuDisabled(true);
	  	         setSelectedStructure(masterUpdatePanel.getSelectedStructure());
	  	         structureCombo.setEditable(false);
		       	 structureCombo.setValue(getSelectedStructure());
	  	         structureEditor = new CellEditor(structureCombo) {  
	  	    	       @Override  
	  	    	       public Object preProcessValue(Object value) {  
	  	    	         if (value == null) {
	  	                     return null;
	  	                   }
	  	                   String valueAttr = ((ComboBox<StructureDTO> )structureCombo).getValueField();
	  	                   if (valueAttr == null)
	  	                     valueAttr = "value";
	  	                   return ((ComboBox<StructureDTO> )structureCombo).getStore().findModel(valueAttr,
	  	                		   value);
	  	    	       }  
	  	    	       @Override  
	  	    	       public Object postProcessValue(Object value) {  
	  	    	       if (value == null) {
	    	                return null;
	    	              }
	  	    	    	  String valueAttr = ((ComboBox<StructureDTO>)structureCombo).getValueField();
	    	              if (valueAttr == null)
	    	                valueAttr = "value";
	    	             setFieldsSelectedFlag(true);
	    	             setSelectedStructure((StructureDTO)value) ;
	    	             validateSelectedFields();
	    	              return ((StructureDTO)value).get(valueAttr);
	  	    	       }  
	  	    	     }; 
		       	structureCombo.setFieldLabel("Structure");
	  	    	structureCombo.setDisplayField(StructureDTO.STRUCTURE_NAME);
	  	    	structureCombo.setValueField(StructureDTO.STRUCTURE_NAME);
	  	    	structureCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
	  	    	structureCombo.setWidth(x-15);
	  	    	column.setEditor(structureEditor);
	       	    column.setId(StructureDTO.STRUCTURE_ID);
	      	    structureCombo.setEmptyText("Structure...");
	  	    	structureCombo.setStore(structures);
	  	    	structureCombo.setTypeAhead(true);
	  	    	structureCombo.setSelectOnFocus(true);
	  	    	structureCombo.setSelection(structures.getModels());
	  	    	column.setRenderer(new GridCellRenderer() {
	        		@Override
					public Object render(ModelData model, String property,
							ColumnData config, int rowIndex, int colIndex,
							ListStore store, Grid grid) {
	        			structureCombo.setValue(getSelectedStructure());
	    	             validateSelectedFields();

	        			return  structureCombo;
					}
	            });
	  	    	
	  	        configs.add(column);
	        }
        // 3. Project Manager
        	if(selectFieldMassUpdate.isbProjectManager()){
                 column = new ColumnConfig();
   	             column.setHeader("ProjectManager");
   	             column.setAlignment(Style.HorizontalAlignment.LEFT);
   	             column.setWidth(x);
   	             projectManagers = new ListStore<DisplaySalesRepDTO>();
   	 	         AppService.App.getInstance().getDisplaySalesRepDTOs(new Service.SetProjectManagerStore(projectManagers));
   	 	         projectManagerCombo = new ComboBox<DisplaySalesRepDTO>();
   	 	         projectManagerCombo.setEditable(false);
   	 	         column.setSortable(false);
   	 	         column.setMenuDisabled(true);
   	 	         setSelectedProjectManager(masterUpdatePanel.getSelectedProjectManager());
   	 	         projectManagerEditor = new CellEditor(projectManagerCombo) {  
   	 	    	       @Override  
   	 	    	       public Object preProcessValue(Object value) {  
   	 	    	         if (value == null) {
   	 	                     return null;
   	 	                   }
   	 	                   String valueAttr = ((ComboBox<DisplaySalesRepDTO> )projectManagerCombo).getValueField();
   	 	                   if (valueAttr == null)
   	 	                     valueAttr = "value";
   	 	                   return ((ComboBox<DisplaySalesRepDTO> )projectManagerCombo).getStore().findModel(valueAttr,
   	 	                		   value);
   	 	    	       }  
   	 	    	       @Override  
   	 	    	       public Object postProcessValue(Object value) {  
   	 	    	         if (value == null) {
   	 	    	                return null;
   	 	    	              }
   	 	    	              String valueAttr = ((ComboBox<DisplaySalesRepDTO>)projectManagerCombo).getValueField();
   	 	    	              if (valueAttr == null)
   	 	    	                valueAttr = "value";
   	 	    	              setFieldsSelectedFlag(true);
   	 	    	              setSelectedProjectManager((DisplaySalesRepDTO)value);
   	 	    	             validateSelectedFields();
   	 	    	              return ((DisplaySalesRepDTO)value).get(valueAttr);
   	 	    	       }  
   	 	    	     };  
   	 	    	     projectManagerCombo.setFieldLabel("Project Manager");
   	 	    	     projectManagerCombo.setDisplayField(DisplaySalesRepDTO.FULL_NAME);
   	 	    	     projectManagerCombo.setValueField(DisplaySalesRepDTO.FULL_NAME);
   	 	    	     projectManagerCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
   	 	    	     column.setEditor(projectManagerEditor);
   	 	    	     column.setId(DisplaySalesRepDTO.DISPLAY_SALES_REP_ID);
   	 	    	     projectManagerCombo.setValue(getSelectedProjectManager());
   	 	    	     projectManagerCombo.setEmptyText("Project Manager...");
   	 	    	     projectManagerCombo.setStore(projectManagers);
   	 	    	     projectManagerCombo.setWidth(x-15);
   	 	    	     projectManagerCombo.setTypeAhead(true);
   	 	    	     projectManagerCombo.setSelectOnFocus(true);
   	 	    	     projectManagerCombo.setSelection(projectManagers.getModels());
   	 	    	     column.setRenderer(new GridCellRenderer() {
   	 	            		@Override
   	 						public Object render(ModelData model, String property,
   	 								ColumnData config, int rowIndex, int colIndex,
   	 								ListStore store, Grid grid) {
   	 	            			projectManagerCombo.setValue(getSelectedProjectManager());
   	 	            			return  projectManagerCombo;
   	 						}
   	 	                });
   	 	    	  
   	 	    	     configs.add(column);
        		}
        		// 4. Status
             	if(selectFieldMassUpdate.isbStatus()){
		    	     column = new ColumnConfig();
		             column.setHeader("Status");
		             column.setAlignment(Style.HorizontalAlignment.LEFT);
		             column.setWidth(x);
		             statuses = new ListStore<StatusDTO>();
		  	         AppService.App.getInstance().getStatusDTOs(new Service.SetStatusStore(statuses));
		  	         statusCombo = new ComboBox<StatusDTO>();
		  	         statusCombo.setEditable(false);
		  	         column.setSortable(false);
		  	         column.setMenuDisabled(true);
		  	         setSelectedStatus(masterUpdatePanel.getSelectedStatus());
		  	         statusEditor = new CellEditor(statusCombo) {  
		  	    	       @Override  
		  	    	       public Object preProcessValue(Object value) {  
		  	    	         if (value == null) {
		  	                     return null;
		  	    	           }
		  	                   String valueAttr = ((ComboBox<StatusDTO> )statusCombo).getValueField();
		  	                   if (valueAttr == null)
		  	                     valueAttr = "value";
		  	                   return ((ComboBox<StatusDTO> )statusCombo).getStore().findModel(valueAttr,
		  	                		   value);
		  	    	       }  
		  	    	       @Override  
		  	    	       public Object postProcessValue(Object value) {  
		  	    	    	 if (value == null) {
		    	                return null;
		    	              }
		    	              String valueAttr = ((ComboBox<StatusDTO>)statusCombo).getValueField();
		    	              if (valueAttr == null)
		    	                valueAttr = "value";
		    	              setSelectedStatus((StatusDTO)value);
		    	              validateSelectedFields();
		    	              return ((StatusDTO)value).get(valueAttr);
		  	    	       }  
		  	    	};  
		  	    	statusCombo.setFieldLabel("Status");
		  	    	statusCombo.setDisplayField(StatusDTO.STATUS_NAME);
		  	    	statusCombo.setValueField(StatusDTO.STATUS_NAME);
		  	    	statusCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
		  	        column.setEditor(statusEditor);
		  	        column.setId(StatusDTO.STATUS_ID);
		  	        statusCombo.setEmptyText("Status...");
		  	        statusCombo.setStore(statuses);
		  	        statusCombo.setWidth(x-15);
		  	        statusCombo.setValue(getSelectedStatus());
			    	statusCombo.setTypeAhead(true);
			    	statusCombo.setSelectOnFocus(true);
			    	statusCombo.setSelection(statuses.getModels());
			    	column.setRenderer(new GridCellRenderer() {
		        		@Override
						public Object render(ModelData model, String property,
								ColumnData config, int rowIndex, int colIndex,
								ListStore store, Grid grid) {
							statusCombo.setValue(getSelectedStatus());
	 	    	             validateSelectedFields();

		        			return  statusCombo;
						}
		            });
		  	        configs.add(column);
             	}
               //5. Corrugate Mfg.
             	if(selectFieldMassUpdate.isbCorrugateMfgt()){
	            	 column = new ColumnConfig();
	                 column.setHeader("Corrugate Mfg");
	                 column.setAlignment(Style.HorizontalAlignment.LEFT);
	                 column.setWidth(x);
	                /* column.setSortable(false);*/
	                 column.setMenuDisabled(true);
	                 corrugates = new ListStore<ManufacturerCountryDTO>();
	          	     AppService.App.getInstance().getManufacturerCountryDTOs(new Service.SetManufacturersCountryStore(null, corrugates));
	          	     corrugateCombo = new ComboBox<ManufacturerCountryDTO>();
	          	     corrugateCombo.setEditable(false);
	          	     setSelectedCorrugate(masterUpdatePanel.getSelectedCorrugate());
	          	     corrugateEditor = new CellEditor(corrugateCombo) {  
	  	    	       @Override  
	  	    	       public Object preProcessValue(Object value) {  
	  	    	         if (value == null) {
	  	                     return null;
	  	                   }
	  	                   String valueAttr = ((ComboBox<ManufacturerCountryDTO> )corrugateCombo).getValueField();
	  	                   if (valueAttr == null)
	  	                     valueAttr = "value";
	  	                   return ((ComboBox<ManufacturerCountryDTO> )corrugateCombo).getStore().findModel(valueAttr,
	  	                		   value);
	  	    	       }  
	  	    	       @Override  
	  	    	       public Object postProcessValue(Object value) {  
	  	    	    	 if (value == null) {
	    	                return null;
	    	              }
	    	              String valueAttr = ((ComboBox<ManufacturerCountryDTO>)corrugateCombo).getValueField();
	    	              if (valueAttr == null)
	    	                valueAttr = "value";
	    	              setFieldsSelectedFlag(true);
	    	              setSelectedCorrugate((ManufacturerCountryDTO)value);
	    	              validateSelectedFields();
	    	              return ((ManufacturerCountryDTO)value).get(valueAttr);
	  	    	       }  
	  	    	     };  
	  	    	   corrugateCombo.setFieldLabel("Corrugate Mfg");
	  	    	   corrugateCombo.setDisplayField(ManufacturerCountryDTO.NAME);
	  	    	   corrugateCombo.setValueField(ManufacturerCountryDTO.NAME);
	  	    	   corrugateCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
	  	    	   column.setEditor(corrugateEditor);
	  	    	   column.setId(ManufacturerCountryDTO.MANUFACTURER_ID);
	  	    	   corrugateCombo.setValue(getSelectedCorrugate());
	  	    	   corrugateCombo.setEmptyText("Corrugate...");
	  	    	   corrugateCombo.setStore(corrugates);
	  	    	   corrugateCombo.setWidth(x-15);
	  	    	   corrugateCombo.setTypeAhead(true);
	  	    	   corrugateCombo.setSelectOnFocus(true);
	  	    	   corrugateCombo.setSelection(corrugates.getModels());
	  	    	   column.setRenderer(new GridCellRenderer() {
	        		@Override
					public Object render(ModelData model, String property,
							ColumnData config, int rowIndex, int colIndex,
							ListStore store, Grid grid) {
	        			corrugateCombo.setValue(getSelectedCorrugate());
	        			return  corrugateCombo;
					}
	  	    	   });
	  	    	   configs.add(column);
             	}
             //6. Promo Period
               if(selectFieldMassUpdate.isbPromoPeriod()){
                    column = new ColumnConfig();
                    column.setHeader("Promo Period");
                    column.setAlignment(Style.HorizontalAlignment.LEFT);
                    column.setWidth(x);
                    /*column.setSortable(false);*/
                    column.setMenuDisabled(true);
                    promoPeriods = new ListStore<PromoPeriodCountryDTO>();
                    AppService.App.getInstance().getPromoPeriodCountryDTOs(new Service.SetPromoPeriodsCountryStore(null,promoPeriods));
                    promoPeriodCombo = new ComboBox<PromoPeriodCountryDTO>();
                    promoPeriodCombo.setEditable(false);
                    setSelectedPromoPeriod(masterUpdatePanel.getSelectedPromoPeriod());
         	        promoPeriodEditor = new CellEditor(promoPeriodCombo) {  
     	    	       @Override  
     	    	       public Object preProcessValue(Object value) {  
     	    	         if (value == null) {
     	                     return null;
     	                   }
     	                   String valueAttr = ((ComboBox<PromoPeriodCountryDTO> )promoPeriodCombo).getValueField();
     	                   if (valueAttr == null)
     	                     valueAttr = "value";
     	                   return ((ComboBox<PromoPeriodCountryDTO> )promoPeriodCombo).getStore().findModel(valueAttr,
     	                		   value);
     	    	       }  
     	    	       @Override  
     	    	       public Object postProcessValue(Object value) {  
     	    	       if (value == null) {
	    	                return null;
	    	              }
     	    	    	  String valueAttr = ((ComboBox<PromoPeriodCountryDTO>)promoPeriodCombo).getValueField();
	    	              if (valueAttr == null)
	    	                valueAttr = "value";
	    	             setSelectedPromoPeriod((PromoPeriodCountryDTO)value) ;
	    	             validateSelectedFields();
	    	              return ((PromoPeriodCountryDTO)value).get(valueAttr);
     	    	       }  
         	    	};  
             	    promoPeriodCombo.setFieldLabel("Promo Period");
             	    promoPeriodCombo.setDisplayField(PromoPeriodCountryDTO.PROMO_PERIOD_NAME);
             	    promoPeriodCombo.setValueField(PromoPeriodCountryDTO.PROMO_PERIOD_NAME);
             	    promoPeriodCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
             	    promoPeriodCombo.setWidth(x-15);
             	    column.setEditor(promoPeriodEditor);
                  	column.setId(PromoPeriodCountryDTO.PROMO_PERIOD_ID);
                  	promoPeriodCombo.setValue(getSelectedPromoPeriod());
                  	promoPeriodCombo.setEmptyText("Promo Period...");
                  	promoPeriodCombo.setStore(promoPeriods);
                  	promoPeriodCombo.setTypeAhead(true);
                  	promoPeriodCombo.setSelectOnFocus(true);
                  	promoPeriodCombo.setSelection(promoPeriods.getModels());
             	    
   	    	    	column.setRenderer(new GridCellRenderer() {
   	            	@Override
					public Object render(ModelData model, String property,
							ColumnData config, int rowIndex, int colIndex,
							ListStore store, Grid grid) {
            			promoPeriodCombo.setValue(getSelectedPromoPeriod());
	    	             validateSelectedFields();

            			return  promoPeriodCombo;
					}
   	                });
   	         	    configs.add(column);
             }
             final ListStore<DisplayForMassUpdateDTO> displays = new ListStore<DisplayForMassUpdateDTO>(); 
             DisplayForMassUpdateDTO model = new DisplayForMassUpdateDTO();
    	     model.setDescription("  ");
    	     displays.add(model);
    	     ColumnModel cm = new ColumnModel(configs);  
    	     ContentPanel cp = new ContentPanel();  
    	     cp.setHeading("DPS - Mass Update Screen");
    	     cp.setFrame(true);
    	     int colAddlCnt=(selectFieldMassUpdate.getNoOfFieldsForMassUpdate()-4);
    	     if(selectFieldMassUpdate.getNoOfFieldsForMassUpdate()<=4){
    	    	 cp.setSize(935, 90);
    	     }else if(selectFieldMassUpdate.getNoOfFieldsForMassUpdate()>4){
    	    	 cp.setSize(935 + (colAddlCnt * x), 90);
    	     }
    	     cp.setButtonAlign(HorizontalAlignment.CENTER);
    	     cp.setLayout(new FitLayout());
    	     grid = new EditorGrid<DisplayForMassUpdateDTO>(displays, cm);
    	     grid.setStyleAttribute("borderTop", "none");
  	         grid.setColumnResize(false);
  	         grid.setBorders(true);
  	         grid.setSelectionModel(new GridSelectionModel<DisplayForMassUpdateDTO>());
        	 grid.setAutoWidth(true);
        	
        	 //7. SKU Mix Final
        	 if(selectFieldMassUpdate.isbSkuMixFinal() ){
             	setSelectedSkuMixFinal(masterUpdatePanel.isSelectedSkuMixFinal());
               	checkColumn = new CheckColumnConfig(DisplayDTO.SKU_MIX_FINAL_FLG, "SKU Mix Final", 50){
     	            @Override
     	            protected String getCheckState(ModelData model, String property, int rowIndex, int colIndex)
     	            {
 	            	  	Boolean value = model.get(property);
 	            	    StringBuilder sb = new StringBuilder();
 	            	    if (value != null && value) {
 	            	        sb.append("-on");
 	            	        setSelectedSkuMixFinal(true);
 	            	    }else if(value==null && isSelectedSkuMixFinal()){
 	            	    	sb.append("-on");
 	            	        setSelectedSkuMixFinal(true);
 	            	    }else{
 	            	    	setSelectedSkuMixFinal(false);
 	            	    	sb.append("-off");
 	            	    }
 	            	    validateSelectedFields();
 	            	    return sb.toString();
     	            }
     	           protected void onMouseDown(GridEvent<ModelData> ge) {
     	        	  String cls = ge.getTarget().getClassName();
     	        	  if (cls != null && cls.indexOf("x-grid3-cc-" + getId()) != -1 && cls.indexOf("disabled") == -1) {
     	        	  ge.stopEvent();
     	        	  int index = grid.getView().findRowIndex(ge.getTarget());
     	        	  ModelData m = grid.getStore().getAt(index);
     	        	  Record r = grid.getStore().getRecord(m);
     	        	  boolean b;
  	        		  if(m.get(getDataIndex())==null){
  	        			  b=false;
  	        		  }else{
  	        			 b = (Boolean) m.get(getDataIndex());  
  	        		  }
     	        	  r.setValid(getDataIndex(), true);
     	        	  r.set(getDataIndex(), !b);
     	        	  }
     	        	}
               		}
               	;  
             	checkColumn.setId(DisplayDTO.SKU_MIX_FINAL_FLG);
             	checkColumn.setHeader("SKU Mix Final");
             	checkColumn.setWidth(x-15);
             	checkColumn.setSortable(false);
             	checkColumn.setMenuDisabled(true);
             	checkColumn.setAlignment(Style.HorizontalAlignment.CENTER);
             	checkBoxEditor = new CellEditor(skuFinalCheckBox);
             	checkColumn.setEditor(checkBoxEditor);
             	configs.add(checkColumn);
             	grid.addPlugin(checkColumn);
             }
        	 //8. Price Avail Complete On Dt.
        	 if(selectFieldMassUpdate.isbPriceAvailCompleteOnDt() ){
         		dateFieldPriceAvail.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
                 column = new ColumnConfig();  
                 column.setId(CustomerMarketingDTO.PRICE_AVAILABILITY_DATE);  
                 column.setHeader("Price Avail Complete On Dt.");  
                 column.setWidth(x);
                 column.setSortable(false);
                 column.setMenuDisabled(true);
                 dateFieldPriceAvail.setValue(masterUpdatePanel.getSelectedPriceAvailCompleteOnDt());
                 dateFieldPriceAvail.setEditable(false);
                 dateFieldPriceAvail.setWidth(x-15);
                 setSelectedPriceAvailCompleteOnDt(masterUpdatePanel.getSelectedPriceAvailCompleteOnDt());
                 CellEditor dateEditor=new CellEditor(dateFieldPriceAvail)
                 {  
           	       @Override  
           	       public Object preProcessValue(Object value) {
           	         if (value == null) {
                            return null;
                          }
                         Date valueAttr = ((Date )value);
                         setSelectedPriceAvailCompleteOnDt(valueAttr) ;
                          return value;
           	       }  
           	       @Override  
           	       public Object postProcessValue(Object value) {
           	       if (value == null) {
         	                return null;
         	              }
           	     Date valueAttr = ((Date )value);
           	     validateSelectedFields();
           	     setSelectedPriceAvailCompleteOnDt(valueAttr) ;
           	     dateFieldPriceAvail.setValue(valueAttr);
           	     return value;
           	   }  
           	};  
           		column.setEditor(dateEditor);
                column.setRenderer(new GridCellRenderer() {
        		@Override
 				public Object render(ModelData model, String property,
 						ColumnData config, int rowIndex, int colIndex,
 						ListStore store, Grid grid) {
        			dateFieldPriceAvail.setValue(getSelectedPriceAvailCompleteOnDt());
    	             validateSelectedFields();

        			return dateFieldPriceAvail;
        		 }
                 });
                 
                 
                 configs.add(column);
             }
        	//9. Sample To Customer By
             if(selectFieldMassUpdate.isbSampleToCustomerBy() ){
            	 dateFieldSampleToCustomer.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
                 column = new ColumnConfig();  
                 column.setId(CustomerMarketingDTO.SENT_SAMPLE_DATE);  
                 column.setHeader("Sample To Customer By");
                 column.setSortable(false);
                 column.setMenuDisabled(true);
                 column.setWidth(x);  
                 dateFieldSampleToCustomer.setValue(masterUpdatePanel.getSelectedSampleToCustomerBy());
                 dateFieldSampleToCustomer.setEditable(false);
                 dateFieldSampleToCustomer.setWidth(x-15);
                 setSelectedSampleToCustomerBy(masterUpdatePanel.getSelectedSampleToCustomerBy());
                 CellEditor dateEditor=new CellEditor(dateFieldSampleToCustomer)
                 {  
           	       @Override  
           	       public Object preProcessValue(Object value) {
           	         if (value == null) {
                            return null;
                          }
                         Date valueAttr = ((Date )value);
                         setSelectedSampleToCustomerBy(valueAttr) ;
                          return value;
           	       }  
           	       @Override  
           	       public Object postProcessValue(Object value) {
           	       if (value == null) {
         	                return null;
         	              }
           	     Date valueAttr = ((Date )value);
           	     validateSelectedFields();
           	     setSelectedSampleToCustomerBy(valueAttr) ;
           	     dateFieldSampleToCustomer.setValue(valueAttr);
           	     return value;
           	   }  
           	};  
           		column.setEditor(dateEditor);
                column.setRenderer(new GridCellRenderer() {
        		@Override
 				public Object render(ModelData model, String property,
 						ColumnData config, int rowIndex, int colIndex,
 						ListStore store, Grid grid) {
        				dateFieldSampleToCustomer.setValue(getSelectedSampleToCustomerBy());
	    	             validateSelectedFields();

        				return dateFieldSampleToCustomer;
        		}
            	});
                 configs.add(column);
           	 }
             //10. Final Artwork & PO Due Dt.
             if(selectFieldMassUpdate.isbFinalArtworkPODueDt() ){
                     dateFieldFinalArtwork.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
                     column = new ColumnConfig();  
                     column.setId(CustomerMarketingDTO.FINAL_ARTWORK_DATE);  
                     column.setHeader("Final Artwork & PO Due Dt.");  
                     column.setWidth(x);
                     column.setSortable(false);
                     column.setMenuDisabled(true);
                     dateFieldFinalArtwork.setValue(masterUpdatePanel.getSelectedFinalArtworkPODueDt());
                     dateFieldFinalArtwork.setEditable(false);
                     dateFieldFinalArtwork.setWidth(x-15);
                     setSelectedFinalArtworkPODueDt(masterUpdatePanel.getSelectedFinalArtworkPODueDt());
                     CellEditor dateEditor=new CellEditor(dateFieldFinalArtwork)
                     {  
       	    	       @Override  
       	    	       public Object preProcessValue(Object value) {
       	    	         if (value == null) {
       	                     return null;
       	                   }
       	                  Date valueAttr = ((Date )value);
       	                  setSelectedFinalArtworkPODueDt(valueAttr) ;
       	                   return value;
       	    	       }  
       	    	       @Override  
       	    	       public Object postProcessValue(Object value) {
       	    	       if (value == null) {
  	    	                return null;
  	    	              }
       	    	       validateSelectedFields();
       	    	       Date valueAttr = ((Date )value);
       	    	       setSelectedFinalArtworkPODueDt(valueAttr) ;
       	    	       dateFieldFinalArtwork.setValue(valueAttr);
       	    	       return value;
       	    	       }  
                     };  
       	    	 column.setEditor(dateEditor);
	             column.setRenderer(new GridCellRenderer() {
	            		@Override
						public Object render(ModelData model, String property,
								ColumnData config, int rowIndex, int colIndex,
								ListStore store, Grid grid) {
	            			dateFieldFinalArtwork.setValue(getSelectedFinalArtworkPODueDt());
	 	    	             validateSelectedFields();

	            			return dateFieldFinalArtwork;
	            	}
	                });
	             configs.add(column);
                 }
             
             //11. PIM Completed on Dt.
             if(selectFieldMassUpdate.isbPIMCompletedonDt() ){
            	 dateFieldPIMCompletedonDt.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
                 column = new ColumnConfig();  
                 column.setId(CustomerMarketingDTO.PIM_COMPLETED_DATE);  
                 column.setHeader("PIM Completed on Dt.");  
                 column.setWidth(x);
                 column.setSortable(false);
                 column.setMenuDisabled(true);
                 dateFieldPIMCompletedonDt.setValue(masterUpdatePanel.getSelectedPIMCompletedonDt());
                 dateFieldPIMCompletedonDt.setWidth(x-15);
                 dateFieldPIMCompletedonDt.setEditable(false);
                 setSelectedPIMCompletedonDt(masterUpdatePanel.getSelectedPIMCompletedonDt());
                 CellEditor dateEditor=new CellEditor(dateFieldPIMCompletedonDt)
                 {  
           	       @Override  
           	       public Object preProcessValue(Object value) {
           	         if (value == null) {
                            return null;
                          }
                         Date valueAttr = ((Date )value);
                         setSelectedPIMCompletedonDt(valueAttr) ;
                          return value;
           	       }  
           	       @Override  
           	       public Object postProcessValue(Object value) {
          	       if (value == null) {
         	                return null;
         	              }
          	     validateSelectedFields(); 
           	     Date valueAttr = ((Date )value);
           	     setSelectedPIMCompletedonDt(valueAttr) ;
           	     dateFieldPIMCompletedonDt.setValue(valueAttr);
           	     return value;
           	       }  
                 };  
           		column.setEditor(dateEditor);
                column.setRenderer(new GridCellRenderer() {
        		@Override
 				public Object render(ModelData model, String property,
 						ColumnData config, int rowIndex, int colIndex,
 						ListStore store, Grid grid) {
        				dateFieldPIMCompletedonDt.setValue(getSelectedPIMCompletedonDt());
	    	             validateSelectedFields();

        				return dateFieldPIMCompletedonDt;
        		}
            	});
                configs.add(column);
             }
             
             // 12. Pricing Admin Validation Dt.
             if(selectFieldMassUpdate.isbPricingAdminValidationDt() ){
                 	dateFieldPricingAdminValidationDt.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
                     column = new ColumnConfig();  
                     column.setId(CustomerMarketingDTO.PRICING_ADMIN_DATE);  
                     column.setHeader("Pricing Admin Validation Dt.");  
                     column.setWidth(x);
                     column.setSortable(false);
                     column.setMenuDisabled(true);
                     dateFieldPricingAdminValidationDt.setValue(masterUpdatePanel.getSelectedPricingAdminValidationDt());
                     dateFieldPricingAdminValidationDt.setEditable(false);
                     setSelectedPricingAdminValidationDt(masterUpdatePanel.getSelectedPricingAdminValidationDt());
                     dateFieldPricingAdminValidationDt.setWidth(x-15);
                     CellEditor dateEditor=new CellEditor(dateFieldPricingAdminValidationDt)
                     {  
               	       @Override  
               	       public Object preProcessValue(Object value) {
               	         if (value == null) {
                                return null;
                              }
                             Date valueAttr = ((Date )value);
                             setSelectedPricingAdminValidationDt(valueAttr) ;
                              return value;
               	       }  
               	       @Override  
               	       public Object postProcessValue(Object value) {
               	       if (value == null) {
             	                return null;
             	              }
               	     Date valueAttr = ((Date )value);
               	     setSelectedPricingAdminValidationDt(valueAttr) ;
               	     validateSelectedFields();
               	     dateFieldPricingAdminValidationDt.setValue(valueAttr);
               	     return value;
               	   }  
               	};  
               		column.setEditor(dateEditor);
                    column.setRenderer(new GridCellRenderer() {
            		@Override
     				public Object render(ModelData model, String property,
     					ColumnData config, int rowIndex, int colIndex,
     					ListStore store, Grid grid) {
        				dateFieldPricingAdminValidationDt.setValue(getSelectedPricingAdminValidationDt());
	    	             validateSelectedFields();

        				return dateFieldPricingAdminValidationDt;
            		}
                	});
                    configs.add(column);
                 }
             // 13. Project Confidence Level
             if(selectFieldMassUpdate.isbProjectConfidenceLevel()){
             	numFieldPCLevel.setValue(masterUpdatePanel.getSelectedProjectConfidenceLevel());
             	numFieldPCLevel.setMinValue(0);
             	numFieldPCLevel.setMaxValue(100);
             	numFieldPCLevel.setAutoHeight(true);
             	numFieldPCLevel.setMaxLength(3);
             	numFieldPCLevel.setMinLength(0);
             	numFieldPCLevel.setWidth(x-15);
             	numFieldPCLevel.setAutoValidate(true);
             	numFieldPCLevel.setSelectOnFocus(true);
             	numFieldPCLevel.setHeight(25);
             	numFieldPCLevel.setToolTip("field size is from 0 to 100 digits");
             	setSelectedProjectConfidenceLevel(masterUpdatePanel.getSelectedProjectConfidenceLevel());
            	configs.add((makeNumberField(CustomerMarketingDTO.PROJECT_LEVEL_PCT, "Project Confidence Level")));
             }
             
          // 14. Init Rendering & Est Quote (Y/N)
	            if(selectFieldMassUpdate.isbInitRenderingEstQuote() ){
	         	    setSelectedInitialRendering(masterUpdatePanel.isSelectedInitialRendering());
	              	checkColumn = new CheckColumnConfig(CustomerMarketingDTO.INITIAL_RENDERING, "Init Rendering & Est Quote (Y/N)", 50){
	    	            @Override
	    	            protected String getCheckState(ModelData model, String property, int rowIndex, int colIndex)
	    	            {
		            	  	Boolean value = model.get(property);
		            	    StringBuilder sb = new StringBuilder();
		            	    if (value != null && value) {
		            	        sb.append("-on");
		            	     setSelectedInitialRendering(true);
		            	    }else if(value==null && isSelectedInitialRendering()){
		            	    	sb.append("-on");
		            	    	setSelectedInitialRendering(true);
		            	    }else{
		            	    	setSelectedInitialRendering(false);
		            	    	sb.append("-off");
		            	    }
		            	    validateSelectedFields();
		            	    return sb.toString();
	    	            }
	    	            protected void onMouseDown(GridEvent<ModelData> ge) {
	       	        	  String cls = ge.getTarget().getClassName();
	       	        	  if (cls != null && cls.indexOf("x-grid3-cc-" + getId()) != -1 && cls.indexOf("disabled") == -1) {
	       	        	  ge.stopEvent();
	       	        	  int index = grid.getView().findRowIndex(ge.getTarget());
	       	        	  ModelData m = grid.getStore().getAt(index);
	       	        		  Record r = grid.getStore().getRecord(m);
	       	        		  boolean b;
	       	        		  if(m.get(getDataIndex())==null){
	       	        			  b=false;
	       	        		  }else{
	       	        			 b = (Boolean) m.get(getDataIndex());  
	       	        		  }
	       	        		  r.setValid(getDataIndex(), true);
	       	        		  r.set(getDataIndex(), !b);
	       	        	  }
	       	        	}
	    	        };  
	            	checkColumn.setId(CustomerMarketingDTO.INITIAL_RENDERING);
	            	checkColumn.setHeader("Init Rendering & Est Quote (Y/N)");
	            	checkColumn.setAlignment(Style.HorizontalAlignment.CENTER);
	            	checkColumn.setSortable(false);
	            	checkColumn.setMenuDisabled(true);
	            	checkBoxEditor = new CellEditor(new CheckBox());
	            	checkColumn.setWidth(x);
	            	checkColumn.setEditor(checkBoxEditor);
	            	configs.add(checkColumn);
	            	grid.addPlugin(checkColumn);
	            }
            // 15. Struct Approved (Y/N)
            	if(selectFieldMassUpdate.isbStructApproved() ){
	             	setSelectedStructureApproved(masterUpdatePanel.isSelectedStructureApproved());
	              	checkColumn = new CheckColumnConfig(CustomerMarketingDTO.STRUCTURE_APPROVED, "Struct Approved (Y/N)", 50){
	    	            @Override
	    	            protected String getCheckState(ModelData model, String property, int rowIndex, int colIndex)
	    	            {
	    	            	  	Boolean value = model.get(property);
	    	            	    StringBuilder sb = new StringBuilder();
	    	            	    if (value != null && value) {
	    	            	        sb.append("-on");
	    	            	     setSelectedStructureApproved(true);
	    	            	    }else if(value==null && isSelectedStructureApproved()){
	    	            	    	sb.append("-on");
	    	            	    	setSelectedStructureApproved(true);
	    	            	    }else{
	    	            	    	setSelectedStructureApproved(false);
	    	            	    	sb.append("-off");
	    	            	    }
	    	            	    validateSelectedFields();
	    	            	    return sb.toString();
	    	            }
	    	            protected void onMouseDown(GridEvent<ModelData> ge) {
	       	        	  String cls = ge.getTarget().getClassName();
	       	        	  if (cls != null && cls.indexOf("x-grid3-cc-" + getId()) != -1 && cls.indexOf("disabled") == -1) {
	       	        	  ge.stopEvent();
	       	        	  int index = grid.getView().findRowIndex(ge.getTarget());
	       	        	  ModelData m = grid.getStore().getAt(index);
	       	        	  Record r = grid.getStore().getRecord(m);
	       	        	boolean b;
	 	        		  if(m.get(getDataIndex())==null){
	 	        			  b=false;
	 	        		  }else{
	 	        			 b = (Boolean) m.get(getDataIndex());  
	 	        		  }
	       	        	  r.setValid(getDataIndex(), true);
	       	        	  r.set(getDataIndex(), !b);
	       	        	  }
	       	        	}
	    	        };  
	            	checkColumn.setId(CustomerMarketingDTO.STRUCTURE_APPROVED);
	            	checkColumn.setHeader("Struct Approved (Y/N)");
	            	checkColumn.setAlignment(Style.HorizontalAlignment.CENTER);
	            	checkColumn.setWidth(x);
	            	checkColumn.setSortable(false);
	            	checkColumn.setMenuDisabled(true);
	            	checkBoxEditor = new CellEditor(new CheckBox());
	            	checkColumn.setEditor(checkBoxEditor);
	            	checkColumn.addListener(Events.OnMouseDown, new Listener<GridEvent<ModelData>>() {
	      	       	public void handleEvent(GridEvent<ModelData> be) {
		      	       	if(be!=null){	
		      	       		int selectedRow = be.getRowIndex();
		      	       		int selectedCol = be.getColIndex();
		      	       	}
	      	       	}
	            	});
	            	configs.add(checkColumn);
	            	grid.addPlugin(checkColumn);
            	}
            // 16. Orders Received (Y/N)
            	if(selectFieldMassUpdate.isbOrdersReceived() ){
             	    setSelectedOrdersReceived(masterUpdatePanel.isSelectedOrdersReceived());
                    checkColumn = new CheckColumnConfig(CustomerMarketingDTO.ORDER_IN, "Orders Received (Y/N)", 50){
    	            @Override
    	            protected String getCheckState(ModelData model, String property, int rowIndex, int colIndex)
    	            {
    	            	  	Boolean value = model.get(property);
    	            	    StringBuilder sb = new StringBuilder();
    	            	    if (value != null && value) {
    	            	        sb.append("-on");
    	            	        setSelectedOrdersReceived(true);
    	            	    }else if(value==null && isSelectedOrdersReceived()){
    	            	    	sb.append("-on");
    	            	    	setSelectedOrdersReceived(true);
    	            	    }else{
    	            	    	setSelectedOrdersReceived(false);
    	            	    	sb.append("-off");
    	            	    }
    	            	    validateSelectedFields();
    	            	    return sb.toString();
    	            }
	    	        protected void onMouseDown(GridEvent<ModelData> ge) {
	       	        	  String cls = ge.getTarget().getClassName();
	       	        	  if (cls != null && cls.indexOf("x-grid3-cc-" + getId()) != -1 && cls.indexOf("disabled") == -1) {
	       	        	  ge.stopEvent();
	       	        	  int index = grid.getView().findRowIndex(ge.getTarget());
	       	        	  ModelData m = grid.getStore().getAt(index);
	       	        	  Record r = grid.getStore().getRecord(m);
	       	        	  boolean b;
	 	        		  if(m.get(getDataIndex())==null){
	 	        			  b=false;
	 	        		  }else{
	 	        			 b = (Boolean) m.get(getDataIndex());  
	 	        		  }
	       	        	  r.setValid(getDataIndex(), true);
	       	        	  r.set(getDataIndex(), !b);
	       	        	  }
	       	        	}
	                };  
	            	checkColumn.setId(CustomerMarketingDTO.ORDER_IN);
	            	checkColumn.setHeader("Orders Received (Y/N)");
	            	checkColumn.setAlignment(Style.HorizontalAlignment.CENTER);
	            	checkColumn.setSortable(false);
	            	checkColumn.setWidth(x);
	            	checkColumn.setMenuDisabled(true);
	            	checkBoxEditor = new CellEditor(new CheckBox());
	            	checkColumn.setEditor(checkBoxEditor);
	            	configs.add(checkColumn);
	            	grid.addPlugin(checkColumn);
                }
            // 16. Orders Received (Y/N)
	            if(selectFieldMassUpdate.isbComments() ){
	            	txtFieldComments.setWidth(x-15);
	            	txtFieldComments.setAutoHeight(true);
	            	txtFieldComments.setHeight(50);
	            	txtFieldComments.setMinLength(0);
	            	txtFieldComments.setAutoValidate(true);
	            	txtFieldComments.setSelectOnFocus(true);
	            	txtFieldComments.setMaxLength(200);
	            	txtFieldComments.setValue(masterUpdatePanel.getSelectedComments());
	            	txtFieldComments.setToolTip("field size is from 0 to 200 characters");
	            	setSelectedComments(masterUpdatePanel.getSelectedComments());
                	configs.add((makeTextField(CustomerMarketingDTO.COMMENTS, "Comments")));
	            }
	            this.setFieldsSelectedFlag(masterUpdatePanel.isFieldsSelectedFlag());
        	 cp.add(grid); 
    	     add(cp);
    }
    
    public MassUpdatePanel() {
        setLayout(new FlowLayout(0));
    	configs = new ArrayList<ColumnConfig>();  
    	column = new ColumnConfig();  
    	int x=125;

    	column = new ColumnConfig();
        column.setId(DisplayDTO.DISPLAY_ID);
        column.setHeader("ID");
        column.setSortable(false);
        column.setMenuDisabled(true);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x - 25);
        configs.add(column);
        
        column = new ColumnConfig();
        column.setId(DisplayDTO.CUSTOMER_BY_NAME);
        column.setHeader("Customer");
        column.setSortable(false);
        column.setMenuDisabled(true);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x - 25);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.SKU);
        column.setHeader("SKU");
        column.setSortable(false);
        column.setMenuDisabled(true);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x-25);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.DESCRIPTION);
        column.setHeader("Description");
        column.setMenuDisabled(true);
        column.setSortable(false);
        column.setAlignment(HorizontalAlignment.RIGHT);
        column.setWidth(x*2);
        newValuesLabel = new LabelField();
        newValuesLabel.setLabelSeparator(" :");
        newValuesLabel.setFieldLabel("newValues");
        column.setEditor(new CellEditor(newValuesLabel));  
        column.setRenderer(new GridCellRenderer() {
    		@Override
			public Object render(ModelData model, String property,
					ColumnData config, int rowIndex, int colIndex,
					ListStore store, Grid grid) {
    			newValuesLabel.setText("");
    			return  newValuesLabel;
			}
        });
    	configs.add(column);
    	final ListStore<DisplayForMassUpdateDTO> displays = new ListStore<DisplayForMassUpdateDTO>(); 
        DisplayForMassUpdateDTO model = new DisplayForMassUpdateDTO();
	    model.setDescription("  ");
	    displays.add(model);
	    ColumnModel cm = new ColumnModel(configs);  
	    ContentPanel cp = new ContentPanel();  
	    cp.setHeading("DPS - Mass Update Screen");
	    cp.setFrame(true);  
	    cp.setSize(935, 10);
	    cp.setButtonAlign(HorizontalAlignment.CENTER);
	    cp.setLayout(new FitLayout());
	    grid = new EditorGrid<DisplayForMassUpdateDTO>(displays, cm);
	    grid.setStyleAttribute("borderTop", "none");
	    grid.setBorders(true);
	    grid.setSelectionModel(new GridSelectionModel<DisplayForMassUpdateDTO>());
	    grid.setAutoWidth(true);
	    cp.add(grid); 
    	add(cp);
      }
    
    public CustomerCountryDTO getSelectedCustomer() {
        return selectedCustomer;
    }

    public void setSelectedCustomer(CustomerCountryDTO selectedCustomer) {
        this.selectedCustomer = selectedCustomer;
    }

    public StructureDTO getSelectedStructure() {
        return selectedStructure;
    }

    public void setSelectedStructure(StructureDTO selectedStructure) {
        this.selectedStructure = selectedStructure;
    }

    public DisplaySalesRepDTO getSelectedProjectManager() {
        return selectedProjectManager;
    }

    public void setSelectedProjectManager(DisplaySalesRepDTO selectedProjectManager) {
        this.selectedProjectManager = selectedProjectManager;
    }

    public StatusDTO getSelectedStatus() {
        return selectedStatus;
    }

    public void setSelectedStatus(StatusDTO selectedStatus) {
        this.selectedStatus = selectedStatus;
    }

	public PromoPeriodCountryDTO getSelectedPromoPeriod() {
		return selectedPromoPeriod;
	}

	public void setSelectedPromoPeriod(PromoPeriodCountryDTO selectedPromoPeriod) {
		this.selectedPromoPeriod = selectedPromoPeriod;
	}
	
	public Date getSelectedPricingAdminValidationDt() {
		return selectedPricingAdminValidationDt;
	}

	public void setSelectedPricingAdminValidationDt(
			Date selectedPricingAdminValidationDt) {
		this.selectedPricingAdminValidationDt = selectedPricingAdminValidationDt;
	}

	public boolean isSelectedSkuMixFinal() {
		return selectedSkuMixFinal;
	}

	public void setSelectedSkuMixFinal(boolean selectedSkuMixFinal) {
		this.selectedSkuMixFinal = selectedSkuMixFinal;
	}
    
	public boolean isSelectedInitialRendering() {
		return selectedInitialRendering;
	}

	public void setSelectedInitialRendering(boolean selectedInitialRendering) {
		this.selectedInitialRendering = selectedInitialRendering;
	}
	
	public boolean isSelectedStructureApproved() {
		return selectedStructureApproved;
	}

	public void setSelectedStructureApproved(boolean selectedStructureApproved) {
		this.selectedStructureApproved = selectedStructureApproved;
	}

	public boolean isSelectedOrdersReceived() {
		return selectedOrdersReceived;
	}

	public void setSelectedOrdersReceived(boolean selectedOrdersReceived) {
		this.selectedOrdersReceived = selectedOrdersReceived;
	}

	public Date getSelectedPriceAvailCompleteOnDt() {
		return selectedPriceAvailCompleteOnDt;
	}

	public void setSelectedPriceAvailCompleteOnDt(Date selectedPriceAvailCompleteOnDt) {
		this.selectedPriceAvailCompleteOnDt = selectedPriceAvailCompleteOnDt;
	}
	
	public Date getSelectedFinalArtworkPODueDt() {
		return selectedFinalArtworkPODueDt;
	}

	public void setSelectedFinalArtworkPODueDt(Date selectedFinalArtworkPODueDt) {
		this.selectedFinalArtworkPODueDt = selectedFinalArtworkPODueDt;
	}
	
	
	public Date getSelectedSampleToCustomerBy() {
		return selectedSampleToCustomerBy;
	}

	public void setSelectedSampleToCustomerBy(Date selectedSampleToCustomerBy) {
		this.selectedSampleToCustomerBy = selectedSampleToCustomerBy;
	}

	public Date getSelectedPIMCompletedonDt() {
		return selectedPIMCompletedonDt;
	}

	public void setSelectedPIMCompletedonDt(Date selectedPIMCompletedonDt) {
		this.selectedPIMCompletedonDt = selectedPIMCompletedonDt;
	}
	

	public String getSelectedComments() {
		return selectedComments;
	}

	public void setSelectedComments(String selectedComments) {
		this.selectedComments = selectedComments;
	}

	public Short getSelectedProjectConfidenceLevel() {
		return selectedProjectConfidenceLevel;
	}

	public void setSelectedProjectConfidenceLevel(
			Short selectedProjectConfidenceLevel) {
		this.selectedProjectConfidenceLevel = selectedProjectConfidenceLevel;
	}
	
	public ManufacturerCountryDTO getSelectedCorrugate() {
		return selectedCorrugate;
	}

	public void setSelectedCorrugate(ManufacturerCountryDTO selectedCorrugate) {
		this.selectedCorrugate = selectedCorrugate;
	}

	public boolean isFieldsSelectedFlag() {
		return fieldsSelectedFlag;
	}

	public void setFieldsSelectedFlag(boolean fieldsSelectedFlag) {
		this.fieldsSelectedFlag = fieldsSelectedFlag;
	}

	public  ColumnConfig makeCheckBox(String id, String name)
    {
	    final CheckColumnConfig checkColumn = new CheckColumnConfig(id, name, 75);
        checkColumn.setId(id);
        checkColumn.setHeader(name);
        column.setSortable(false);
        checkColumn.setAlignment(Style.HorizontalAlignment.CENTER);
        CellEditor checkBoxEditor = new CellEditor(new CheckBox());    
 	    checkColumn.setEditor(checkBoxEditor);
 	    grid.addPlugin(checkColumn);
        return checkColumn;  
    }

	protected ColumnConfig makeDateField(String id, String name)
    {
        DateField dateField = new DateField();  
        dateField.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
        ColumnConfig column = new ColumnConfig();  
        column.setId(id); 
        column.setSortable(false);
        column.setHeader(name);  
        column.setWidth(125);  
        column.setEditor(new CellEditor(dateField)); 
        return column;
	}

	 protected ColumnConfig makeTextField(String id, String name)
	 {
       ColumnConfig column = new ColumnConfig();  
       column.setId(id);  
       column.setHeader(name);  
       column.setSortable(false);
       column.setWidth(125);
       if(id.equals(CustomerMarketingDTO.COMMENTS)){
    	   CellEditor commentEditor = new CellEditor(txtFieldComments)
    	   {  
    		   @Override  
      	      public Object preProcessValue(Object value) {
      	    	 if (value == null) {
      	        	 	setSelectedComments(null) ;
      	        	 	return null;
                     }
                    String valueAttr = ((String )value);
                    setSelectedComments(valueAttr) ;
                    return value;
      	       }  
      	       @Override  
      	       public Object postProcessValue(Object value) {
      	       if (value == null) {
      	    	 		setSelectedComments(null) ;
    	                return null;
    	       }
      	     setFieldsSelectedFlag(true);
      	     String valueAttr = ((String )value);
      	     setSelectedComments(valueAttr) ;
      	     return value;
      	   }  
      	}; 
      	column.setEditor(commentEditor);
      	column.setRenderer(new GridCellRenderer() {
      		@Override
				public Object render(ModelData model, String property,
						ColumnData config, int rowIndex, int colIndex,
						ListStore store, Grid grid) {
      					txtFieldComments.setValue(getSelectedComments());
      					validateSelectedFields();
      					return txtFieldComments;
      		}
          	});
       }
       return column;
	 }

	 protected ColumnConfig makeNumberField(String id, String name)
    {
	   ColumnConfig column = new ColumnConfig();  
	   column.setId(id);  
	   column.setHeader(name);  
	   column.setSortable(false);
	   column.setWidth(125);
	   if(id.equals(CustomerMarketingDTO.PROJECT_LEVEL_PCT)){
		   numFieldPCLevel.setPropertyEditorType(Short.class);
		   CellEditor pcLevelEditor = new CellEditor(numFieldPCLevel)
    	   {  
			    @Override  
      	      public Object preProcessValue(Object value) {
      	         if (value == null) {
      	        	 setSelectedProjectConfidenceLevel(null) ;
      	        	 return null;
                 }
                 Short valueAttr = ((Short )value);
                 setSelectedProjectConfidenceLevel(valueAttr) ;
                 return value;
      	       }  
      	       @Override  
      	       public Object postProcessValue(Object value) {
      	       if (value == null) {
      	    	 		setSelectedProjectConfidenceLevel(null) ;
    	                return null;
    	       }
      	     setFieldsSelectedFlag(true);
      	     Short valueAttr = ((Short )value);
      	     setSelectedProjectConfidenceLevel(valueAttr) ;
      	     numFieldPCLevel.setValue(valueAttr);
      	     return value;
      	   }  
      	}; 
      	column.setEditor(pcLevelEditor);
      	column.setRenderer(new GridCellRenderer() {
      		@Override
				public Object render(ModelData model, String property,
						ColumnData config, int rowIndex, int colIndex,
						ListStore store, Grid grid) {
      				 	numFieldPCLevel.setValue(getSelectedProjectConfidenceLevel());
      				 	validateSelectedFields();
      				 	return numFieldPCLevel;
      		}
          	});
	   } 
	   return column;
    }
	 
	public void validateSelectedFields(){
		 if(selectFieldMassUpdate.getNoOfFieldsForMassUpdate()>4){
			 if(selectFieldMassUpdate.isbCustomer()){
				 if(getSelectedCustomer()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbStructure()){
				 if(getSelectedStructure()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbProjectManager()){
				 if(getSelectedProjectManager()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbStatus()){
				 if(getSelectedStatus()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbCorrugateMfgt()){
				 if(getSelectedCorrugate()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbPromoPeriod()){
				 if(getSelectedPromoPeriod()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbPriceAvailCompleteOnDt()){
				 if(getSelectedPriceAvailCompleteOnDt()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbSampleToCustomerBy()){
				 if(getSelectedSampleToCustomerBy()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbFinalArtworkPODueDt()){
				 if(getSelectedFinalArtworkPODueDt()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbPIMCompletedonDt()){
				 if(getSelectedPIMCompletedonDt()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbPricingAdminValidationDt()){
				 if(getSelectedPricingAdminValidationDt()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbProjectConfidenceLevel()){
				 if(getSelectedProjectConfidenceLevel()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 if(selectFieldMassUpdate.isbComments() ){
				 if(getSelectedComments()==null){
					 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
					 return;
				 }
			 }
			 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().enable();
		 }else{
			 ((MassUpdateGrid)getParent()).getBtnApplyUpdates().disable();
		 }
	}
}
