package com.averydennison.dmm.app.client.admin;

import com.averydennison.dmm.app.client.AppService;
import com.averydennison.dmm.app.client.Service;
import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.CustomerTypeDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.SimpleComboBox;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.google.gwt.user.client.Element;

public class CustomerForm extends AbstractAdminForm<CustomerDTO>{

	protected LabelField id = new LabelField();
	protected TextField<String> name = new TextField<String>();
	protected ComboBox<CustomerTypeDTO> customerType = new ComboBox<CustomerTypeDTO>();
	protected ComboBox<CustomerDTO> parent = new ComboBox<CustomerDTO>();
	protected ComboBox<DisplaySalesRepDTO> cma = new ComboBox<DisplaySalesRepDTO>();
	protected TextField<String> customerCode = new TextField<String>();
	protected TextField<String> companyCode = new TextField<String>();
	protected ComboBox<CountryDTO> country = new ComboBox<CountryDTO>();
	protected SimpleComboBox<String> active = new SimpleComboBox<String>();
	
	public CustomerForm() {
		this.id.setFieldLabel("ID:");
		this.id.setLabelStyle(getRequirityFieldStyle(100, 12, false));
	    populateTextFieldWithRequiredData(this.name, "Customer Name", true, 25);
	    
	    populateComboBoxWithRequiredData(this.customerType, "Customer Type", false, null);
	    this.customerType.setDisplayField(CustomerTypeDTO.CUSTOMER_TYPE_NAME);
	    AppService.App.getInstance().getCustomerTypeDTOs(new Service.SetModelStore<CustomerTypeDTO>(this.customerType.getStore()));
	    
	    populateComboBoxWithRequiredData(this.parent, "Parent", false, null);
	    this.parent.setDisplayField(CustomerDTO.CUSTOMER_NAME);

	    populateComboBoxWithRequiredData(this.cma, "CMA", false, null);
	    this.cma.setDisplayField(DisplaySalesRepDTO.FULL_NAME);

	    populateTextFieldWithRequiredData(this.customerCode, "Customer Code", false, 8);
	    populateTextFieldWithRequiredData(this.companyCode, "Company Code", false, 3);
	    
	    populateComboBoxWithRequiredData(this.country, "Country", false, null);
	    this.country.setDisplayField(CountryDTO.COUNTRY_NAME);
	    AppService.App.getInstance().getCountryFlagDTOs(new Service.SetCountryStore(this.country.getStore()));
	    
	    this.active.setFieldLabel("Active?");
	    this.active.setTriggerAction(TriggerAction.ALL);
	    this.active.add(YES);
	    this.active.add(NO);
	    this.active.setAllowBlank(true);
	    this.active.setEditable(false); 
	    this.active.setLabelStyle(getRequirityFieldStyle(100, 12, false));
	}
	
	@Override
	protected void onRender(Element target, int index) {
		super.onRender(target, index);
		this.setLayout(getFormLayout(120,180));
		
		this.add(this.id);
		this.add(this.name);
		this.add(this.customerType);
		this.add(this.parent);
		this.add(this.customerCode);
		this.add(this.companyCode);
		this.add(this.cma);
		this.add(this.country);
		this.add(this.active);
	}

	public void init() {
		active.setValue(active.getStore().getAt(0));
	}

	@Override
	public void updateDTO(CustomerDTO dto) {
		dto.setCustomerName(name.getValue());
		dto.setCustomerTypeId(customerType.getValue() != null ? customerType.getValue().getCustomerTypeId() : null);
		dto.setCustomerTypeName(customerType.getValue() != null ? customerType.getValue().getCustomerTypeName() : null);
		dto.setCustomerParentId(parent.getValue() != null && parent.getValue().getCustomerId() != 0 ? parent.getValue().getCustomerId() : null);
		dto.setCustomerParentName(parent.getValue() != null && parent.getValue().getCustomerId() != 0 ? parent.getValue().getCustomerName() : null);
		dto.setCustomerCode(customerCode.getValue());
		dto.setCompanyCode(companyCode.getValue());
		dto.setCustomerCountryId(country.getValue() != null ? country.getValue().getCountryId() : null);
		dto.setCustomerCountryName(country.getValue() != null ? country.getValue().getCountryName() : null);
		dto.setCustomerCMAId(cma.getValue() != null ? cma.getValue().getDisplaySalesRepId() : null);
		dto.setCustomerCMAName(cma.getValue() != null ? cma.getValue().getFullName() : null);		
		dto.setActive(YES.equals(active.getSimpleValue()) ? true : false);
	}

	@Override
	public void setDTO(CustomerDTO dto) {
		id.setValue(dto.getCustomerId());
		name.setValue(dto.getCustomerName());
		customerType.setValue(getCustomerTypeById(dto.getCustomerTypeId()));
		parent.setValue(getCustomerParentById(dto.getCustomerParentId()));
		if (parent.getValue() == null) parent.setValue(getCustomerParentById(0l));
		customerCode.setValue(dto.getCustomerCode());
		companyCode.setValue(dto.getCompanyCode());
		country.setValue(getCountryById(dto.getCustomerCountryId(), country.getStore()));
		active.setValue(active.getStore().getAt(dto.isActive()? 0 : 1));
		AppService.App.getInstance().getDisplaySalesRepDTOs(new Service.SetComboboxValues<DisplaySalesRepDTO>(this.cma,	dto.getCustomerCMAId(), DisplaySalesRepDTO.DISPLAY_SALES_REP_ID));
	}
	
	private CustomerTypeDTO getCustomerTypeById(Long customerTypeId) {
		for(CustomerTypeDTO customerTypeDTO: customerType.getStore().getModels()){
			if(customerTypeDTO.getCustomerTypeId().equals(customerTypeId)){
				return customerTypeDTO;
			}
		}
		return null;
	}
	
	private CustomerDTO getCustomerParentById(Long customerParentId) {
		for(CustomerDTO customerDTO: parent.getStore().getModels()){
			if(customerDTO.getCustomerId().equals(customerParentId)){
				return customerDTO;
			}
		}
		return null;
	}

}
