package com.averydennison.dmm.app.client.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.averydennison.dmm.app.client.App;
import com.averydennison.dmm.app.client.AppService;
import com.averydennison.dmm.app.client.Service;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

public class CorrugateVendors  extends AbstractAdminPanel<ManufacturerCountryDTO> {

	public CorrugateVendorsWindow window = new CorrugateVendorsWindow();
	
	public CorrugateVendors() {
		grid.getView().setEmptyText("No available corrugate vendors");
		gridWrapper.setHeading("Corrugate Vendors");
		this.window.addListener(Events.Submit, new Listener<BaseEvent>() {
			@Override
			public void handleEvent(BaseEvent be) {
				if(be.getSource() instanceof ManufacturerCountryDTO){
					ManufacturerCountryDTO dto = ((ManufacturerCountryDTO)be.getSource());
					AppService.App.getInstance().saveManufacturerCountryDTO(dto, new Service.SaveAdminDTO<ManufacturerCountryDTO>(CorrugateVendors.this));
				}else{
					System.err.println("Wrong data type for add item window submit event");
				}
			}
		});
	}
	
	protected List<ColumnConfig> getColumnConfigs() {
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

	    ColumnConfig column = new ColumnConfig(ManufacturerCountryDTO.MANUFACTURER_ID, "ID", 50);
        column.setAlignment(HorizontalAlignment.LEFT);
        configs.add(column);

	    column = new ColumnConfig(ManufacturerCountryDTO.NAME, "Corrugate Vendor", 150);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
	    
	    column = new ColumnConfig(ManufacturerCountryDTO.COUNTRY_NAME, "Country", 100);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
	    
	    column = new ColumnConfig(ManufacturerCountryDTO.ACTIVE, "Active?", 100);
	    column.setRenderer(new GridCellRenderer<ManufacturerCountryDTO>() {
			@Override
			public Object render(ManufacturerCountryDTO model, String property,	ColumnData config, int rowIndex, int colIndex,
					ListStore<ManufacturerCountryDTO> store, Grid<ManufacturerCountryDTO> grid) {
				
				return model.isActive() ? "Yes" : "No";
			}
		});
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);

		return configs;
	}

	public void setData(List<ManufacturerCountryDTO> manufacturers) {
		grid.getStore().removeAll();
		grid.getStore().add(manufacturers);
	}

	@Override
	protected void deleteSelectedItem(ManufacturerCountryDTO model) {
		btnDelete.disable();
        AppService.App.getInstance().deleteManufacturerCountryDTO(model, new Service.DeleteAdminDTO<ManufacturerCountryDTO>(CorrugateVendors.this, model));
	}

	@Override
	public void finishDelete(ManufacturerCountryDTO model, boolean isSuccess) {
		btnDelete.enable();
		if(isSuccess){
			grid.getStore().remove(model);
		}		
	}

	@Override
	public void finishEdit(ManufacturerCountryDTO model) {
		if(grid.getStore().contains(model)){
			grid.getStore().update(model);
		} else {
			grid.getStore().add(model);
			grid.getSelectionModel().setSelection(Arrays.asList(model));
			grid.getView().focusRow(grid.getStore().indexOf(model));
		}
		window.closeWindow();
	}

	@Override
	protected void showAddWindow() {
		window.makeNew();
		window.show();
	}

	@Override
	protected void showEditWindow(ManufacturerCountryDTO model) {
		window.setDTO(model);
		window.show();
	}

	@Override
	protected void setPermissions() {
    	if(!App.isAuthorized("CorrugateVendors.CREATE")) btnCreate.disable();
    	if(!App.isAuthorized("CorrugateVendors.EDIT")) btnEdit.disable();
    	if(!App.isAuthorized("CorrugateVendors.DELETE")) btnDelete.disable();
	}

	@Override
	protected boolean isButtonPermitted(Button button) {
		if(button.getText().equals(CREATE)) return App.isAuthorized("CorrugateVendors.CREATE");
		if(button.getText().equals(EDIT)) return App.isAuthorized("CorrugateVendors.EDIT");
		if(button.getText().equals(DELETE)) return App.isAuthorized("CorrugateVendors.DELETE");
		return false;
	}

}
