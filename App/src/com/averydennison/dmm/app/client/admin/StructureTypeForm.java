package com.averydennison.dmm.app.client.admin;

import com.averydennison.dmm.app.client.models.StructureDTO;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.SimpleComboBox;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.google.gwt.user.client.Element;

public class StructureTypeForm extends AbstractAdminForm<StructureDTO>{

	protected LabelField id = new LabelField();
	protected TextField<String> name = new TextField<String>();
	protected SimpleComboBox<String> active = new SimpleComboBox<String>();
	
	public StructureTypeForm() {
		this.id.setFieldLabel("ID:");
		this.id.setLabelStyle(getRequirityFieldStyle(100, 12, false));
	    populateTextFieldWithRequiredData(this.name, "Structure Type", true, 50);
	    
	    this.active.setFieldLabel("Active?");
	    this.active.setTriggerAction(TriggerAction.ALL);
	    this.active.add(YES);
	    this.active.add(NO);
	    this.active.setAllowBlank(true);
	    this.active.setEditable(false); 
	    this.active.setLabelStyle(getRequirityFieldStyle(100, 12, false));
	}
	
	@Override
	protected void onRender(Element target, int index) {
		super.onRender(target, index);
		this.setLayout(getFormLayout(120,180));
		
		this.add(this.id);
		this.add(this.name);
		this.add(this.active);
	}

	public void init() {
		active.setValue(active.getStore().getAt(0));
	}

	@Override
	public void updateDTO(StructureDTO dto) {
		dto.setStructureName(name.getValue());
		dto.setActiveFlg(YES.equals(active.getSimpleValue()) ? true : false);
	}

	@Override
	public void setDTO(StructureDTO dto) {
		id.setValue(dto.getStructureId());
		name.setValue(dto.getStructureName());
		active.setValue(active.getStore().getAt(dto.isActiveFlg()? 0 : 1));
	}

}
