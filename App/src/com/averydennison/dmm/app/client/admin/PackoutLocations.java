package com.averydennison.dmm.app.client.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.averydennison.dmm.app.client.App;
import com.averydennison.dmm.app.client.AppService;
import com.averydennison.dmm.app.client.Service;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

public class PackoutLocations extends AbstractAdminPanel<LocationCountryDTO> {

	public PackoutLocationsWindow window = new PackoutLocationsWindow();
	
	public PackoutLocations() {
		grid.getView().setEmptyText("No available packout locations");
		gridWrapper.setHeading("Packout Locations");
		this.window.addListener(Events.Submit, new Listener<BaseEvent>() {
			@Override
			public void handleEvent(BaseEvent be) {
				if(be.getSource() instanceof LocationCountryDTO){
					LocationCountryDTO dto = ((LocationCountryDTO)be.getSource());
					AppService.App.getInstance().saveLocationCountryDTO(dto, new Service.SaveAdminDTO<LocationCountryDTO>(PackoutLocations.this));
				}else{
					System.err.println("Wrong data type for add item window submit event");
				}
			}
		});
	}
	
	protected List<ColumnConfig> getColumnConfigs() {
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

	    ColumnConfig column = new ColumnConfig(LocationCountryDTO.LOCATION_ID, "ID", 50);
        column.setAlignment(HorizontalAlignment.LEFT);
        configs.add(column);

	    column = new ColumnConfig(LocationCountryDTO.LOCATION_CODE, "DC Code", 100);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
        
	    column = new ColumnConfig(LocationCountryDTO.LOCATION_NAME, "DC Name", 200);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
	    
	    column = new ColumnConfig(LocationCountryDTO.COUNTRY_NAME, "Country", 100);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
	    
	    column = new ColumnConfig(LocationCountryDTO.ACTIVE_FLG, "Active?", 100);
	    column.setRenderer(new GridCellRenderer<LocationCountryDTO>() {
			@Override
			public Object render(LocationCountryDTO model, String property,	ColumnData config, int rowIndex, int colIndex,
					ListStore<LocationCountryDTO> store, Grid<LocationCountryDTO> grid) {
				
				return model.isActiveFlg() ? "Yes" : "No";
			}
		});
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);

		return configs;
	}

	public void setData(List<LocationCountryDTO> models) {
		grid.getStore().removeAll();
		grid.getStore().add(models);
	}

	@Override
	protected void deleteSelectedItem(LocationCountryDTO model) {
		btnDelete.disable();
        AppService.App.getInstance().deleteLocationCountryDTO(model, new Service.DeleteAdminDTO<LocationCountryDTO>(PackoutLocations.this, model));
	}

	@Override
	public void finishDelete(LocationCountryDTO model, boolean isSuccess) {
		btnDelete.enable();
		if(isSuccess){
			grid.getStore().remove(model);
		}		
	}

	@Override
	public void finishEdit(LocationCountryDTO model) {
		if(grid.getStore().contains(model)){
			grid.getStore().update(model);
		} else {
			grid.getStore().add(model);
			grid.getSelectionModel().setSelection(Arrays.asList(model));
			grid.getView().focusRow(grid.getStore().indexOf(model));
		}
		window.closeWindow();
	}

	@Override
	protected void showAddWindow() {
		window.makeNew();
		window.show();
	}

	@Override
	protected void showEditWindow(LocationCountryDTO model) {
		window.setDTO(model);
		window.show();
	}

	protected void setPermissions() {
    	if(!App.isAuthorized("PackoutLocations.CREATE")) btnCreate.disable();
    	if(!App.isAuthorized("PackoutLocations.EDIT")) btnEdit.disable();
    	if(!App.isAuthorized("PackoutLocations.DELETE")) btnDelete.disable();
	}

	protected boolean isButtonPermitted(Button button) {
		if(button.getText().equals(CREATE)) return App.isAuthorized("PackoutLocations.CREATE");
		if(button.getText().equals(EDIT)) return App.isAuthorized("PackoutLocations.EDIT");
		if(button.getText().equals(DELETE)) return App.isAuthorized("PackoutLocations.DELETE");
		return false;
	}
	
}
