package com.averydennison.dmm.app.client.admin;

import com.averydennison.dmm.app.client.AppService;
import com.averydennison.dmm.app.client.Service;
import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.SimpleComboBox;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.google.gwt.user.client.Element;

public class PackoutLocationForm extends AbstractAdminForm<LocationCountryDTO>{

	protected LabelField id = new LabelField();
	protected TextField<String> code = new TextField<String>();
	protected TextField<String> name = new TextField<String>();
	protected ComboBox<CountryDTO> country = new ComboBox<CountryDTO>();
	protected SimpleComboBox<String> active = new SimpleComboBox<String>();
	
	public PackoutLocationForm() {
		this.id.setFieldLabel("ID:");
		this.id.setLabelStyle(getRequirityFieldStyle(100, 12, false));
		populateTextFieldWithRequiredData(this.code, "DC Code", false, 2);
	    populateTextFieldWithRequiredData(this.name, "DC Name", true, 50);
	    
	    populateComboBoxWithRequiredData(this.country, "Country", false, null);
	    this.country.setDisplayField(CountryDTO.COUNTRY_NAME);
	    AppService.App.getInstance().getCountryFlagDTOs(new Service.SetCountryStore(this.country.getStore()));
	    
	    this.active.setFieldLabel("Active?");
	    this.active.setTriggerAction(TriggerAction.ALL);
	    this.active.add(YES);
	    this.active.add(NO);
	    this.active.setAllowBlank(true);
	    this.active.setEditable(false); 
	    this.active.setLabelStyle(getRequirityFieldStyle(100, 12, false));
	}
	
	@Override
	protected void onRender(Element target, int index) {
		super.onRender(target, index);
		this.setLayout(getFormLayout(120,180));
		
		this.add(this.id);
		this.add(this.name);
		this.add(this.code);
		this.add(this.country);
		this.add(this.active);
	}

	public void init() {
		active.setValue(active.getStore().getAt(0));
	}

	@Override
	public void updateDTO(LocationCountryDTO dto) {
		dto.setLocationCode(code.getValue());
		dto.setLocationName(name.getValue());
		dto.setCountryId(country.getValue() != null ? country.getValue().getCountryId() : null);
		dto.setCountryName(country.getValue() != null ? country.getValue().getCountryName() : null);
		dto.setActiveFlg(YES.equals(active.getSimpleValue()) ? true : false);
	}

	@Override
	public void setDTO(LocationCountryDTO dto) {
		id.setValue(dto.getLocationId());
		code.setValue(dto.getLocationCode());
		name.setValue(dto.getLocationName());
		country.setValue(getCountryById(dto.getCountryId(), country.getStore()));
		active.setValue(active.getStore().getAt(dto.isActiveFlg()? 0 : 1));
	}

}
