package com.averydennison.dmm.app.client.admin;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FormButtonBinding;
import com.google.gwt.user.client.Element;

public abstract class AbstractAdminWindow<T extends BaseModel> extends Window{
	
	protected AbstractAdminForm<T> form;
	protected Button submitButton = new Button("Save");
	protected Button cancelButton = new Button("Cancel");
	protected T dto;

	public AbstractAdminWindow(){
		this.buildForm();
	    this.setModal(true);
	    this.setClosable(false);
	    this.setResizable(false);
        this.setWidth(350);

        this.submitButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
			public void componentSelected(ButtonEvent ce) {
				submitForm();	
			}
		});
        this.cancelButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
			public void componentSelected(ButtonEvent ce) {
				cancelForm();
			}
		});
        FormButtonBinding binding = new FormButtonBinding(form);  
	    binding.addButton(this.submitButton);
        this.addButton(this.submitButton);
        this.addButton(this.cancelButton);
	}
	
	protected abstract void buildForm();
	protected abstract T makeDTOInstance();

	@Override
	protected void onRender(Element parent, int pos) {
		super.onRender(parent, pos);
		this.add(this.form);
	}

	public void setDTO(T dto) {
		this.dto = dto;
		init();
		this.form.setDTO(this.dto);
	}

	public void makeNew() {
		reset();
		this.dto = makeDTOInstance();
		init();
	}

	private void init() {
		form.init();
	}

	private void updateDTO() {
		this.form.updateDTO(this.dto);
	}
	
	private void submitForm() {
		updateDTO();
		fireEvent(Events.Submit, new BaseEvent(this.dto));
	}

	protected void cancelForm() {
		hide();	
		reset();
	}
	
	public void closeWindow(){
		cancelForm();
	}
	
	public void reset(){
		form.clear();
	}

}
