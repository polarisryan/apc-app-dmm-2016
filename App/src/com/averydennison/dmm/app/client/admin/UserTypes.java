package com.averydennison.dmm.app.client.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.averydennison.dmm.app.client.App;
import com.averydennison.dmm.app.client.AppService;
import com.averydennison.dmm.app.client.Service;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

public class UserTypes extends AbstractAdminPanel<DisplaySalesRepDTO> {

	public UserTypesWindow window = new UserTypesWindow();
	
	public UserTypes() {
		grid.getView().setEmptyText("No available user types");
		gridWrapper.setHeading("User Types");
		this.window.addListener(Events.Submit, new Listener<BaseEvent>() {
			@Override
			public void handleEvent(BaseEvent be) {
				if(be.getSource() instanceof DisplaySalesRepDTO){
					DisplaySalesRepDTO dto = ((DisplaySalesRepDTO)be.getSource());
					AppService.App.getInstance().saveDisplaySalesRepDTO(dto, new Service.SaveAdminDTO<DisplaySalesRepDTO>(UserTypes.this));
				}else{
					System.err.println("Wrong data type for add item window submit event");
				}
			}
		});
	}
	
	protected List<ColumnConfig> getColumnConfigs() {
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

	    ColumnConfig column = new ColumnConfig(DisplaySalesRepDTO.DISPLAY_SALES_REP_ID, "ID", 50);
        column.setAlignment(HorizontalAlignment.LEFT);
        configs.add(column);

	    column = new ColumnConfig(DisplaySalesRepDTO.FULL_NAME, "Name", 200);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
   
	    column = new ColumnConfig(DisplaySalesRepDTO.USER_TYPE_NAME, "Type", 100);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);

	    column = new ColumnConfig(DisplaySalesRepDTO.COUNTRY_NAME, "Country", 100);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);	    
	    
	    column = new ColumnConfig(DisplaySalesRepDTO.ACTIVE_FLG, "Active?", 100);
	    column.setRenderer(new GridCellRenderer<DisplaySalesRepDTO>() {
			@Override
			public Object render(DisplaySalesRepDTO model, String property,	ColumnData config, int rowIndex, int colIndex,
					ListStore<DisplaySalesRepDTO> store, Grid<DisplaySalesRepDTO> grid) {
				
				return model.isActiveFlg() ? "Yes" : "No";
			}
		});
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);

		return configs;
	}

	public void setData(List<DisplaySalesRepDTO> models) {
		grid.getStore().removeAll();
		grid.getStore().add(models);
	}

	@Override
	protected void deleteSelectedItem(DisplaySalesRepDTO model) {
		btnDelete.disable();
        AppService.App.getInstance().deleteDisplaySalesRepDTO(model, new Service.DeleteAdminDTO<DisplaySalesRepDTO>(UserTypes.this, model));
	}

	@Override
	public void finishDelete(DisplaySalesRepDTO model, boolean isSuccess) {
		btnDelete.enable();
		if(isSuccess){
			grid.getStore().remove(model);
		}		
	}

	@Override
	public void finishEdit(DisplaySalesRepDTO model) {
		if(grid.getStore().contains(model)){
			grid.getStore().update(model);
		} else {
			grid.getStore().add(model);
			grid.getSelectionModel().setSelection(Arrays.asList(model));
			grid.getView().focusRow(grid.getStore().indexOf(model));
		}
		window.closeWindow();
	}

	@Override
	protected void showAddWindow() {
		window.makeNew();
		window.show();
	}

	@Override
	protected void showEditWindow(DisplaySalesRepDTO model) {
		window.setDTO(model);
		window.show();
	}

	protected void setPermissions() {
    	if(!App.isAuthorized("UserTypes.CREATE")) btnCreate.disable();
    	if(!App.isAuthorized("UserTypes.EDIT")) btnEdit.disable();
    	if(!App.isAuthorized("UserTypes.DELETE")) btnDelete.disable();
	}
	

	protected boolean isButtonPermitted(Button button) {
		if(button.getText().equals(CREATE)) return App.isAuthorized("UserTypes.CREATE");
		if(button.getText().equals(EDIT)) return App.isAuthorized("UserTypes.EDIT");
		if(button.getText().equals(DELETE)) return App.isAuthorized("UserTypes.DELETE");
		return false;
	}
}
