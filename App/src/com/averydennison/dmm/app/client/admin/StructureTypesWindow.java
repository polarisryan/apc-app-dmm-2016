package com.averydennison.dmm.app.client.admin;

import com.averydennison.dmm.app.client.models.StructureDTO;

public class StructureTypesWindow extends AbstractAdminWindow<StructureDTO>{
	
	public StructureTypesWindow(){
	    this.setHeading("Structure Type");
	}
	
	protected void buildForm() {
		this.form = new StructureTypeForm();
	}
	
	public void setDTO(StructureDTO dto) {
		super.setDTO(dto);
		setFocusWidget(((StructureTypeForm)this.form).name);
	}

	public void makeNew() {
		super.makeNew();
		setFocusWidget(((StructureTypeForm)this.form).name);
	}
	
	@Override
	protected StructureDTO makeDTOInstance() {
		return new StructureDTO();
	}

}
