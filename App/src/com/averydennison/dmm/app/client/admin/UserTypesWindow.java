package com.averydennison.dmm.app.client.admin;

import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;

public class UserTypesWindow extends AbstractAdminWindow<DisplaySalesRepDTO>{
	
	public UserTypesWindow(){
	    this.setHeading("User Type");
	}
	
	protected void buildForm() {
		this.form = new UserTypeForm();
	}
	
	public void setDTO(DisplaySalesRepDTO dto) {
		super.setDTO(dto);
		setFocusWidget(((UserTypeForm)this.form).firstName);
	}

	public void makeNew() {
		super.makeNew();
		setFocusWidget(((UserTypeForm)this.form).firstName);
	}
	
	@Override
	protected DisplaySalesRepDTO makeDTOInstance() {
		return new DisplaySalesRepDTO();
	}

}
