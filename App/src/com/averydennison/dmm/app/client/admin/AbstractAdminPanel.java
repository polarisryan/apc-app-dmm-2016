package com.averydennison.dmm.app.client.admin;

import java.util.List;

import com.averydennison.dmm.app.client.util.GeneralComponentBinding;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.GridView;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;

public abstract class AbstractAdminPanel<T extends BaseModel> extends LayoutContainer{
	
	protected Grid<T> grid;
	protected ContentPanel gridWrapper;
    public static final String CREATE = "Create";
    public static final String EDIT = "Edit";
    public static final String DELETE = "Delete";
    protected Button btnCreate;
    protected Button btnEdit;
    protected Button btnDelete;
    
    protected SelectionListener<ButtonEvent> buttonListener = new SelectionListener<ButtonEvent>() {
        public void componentSelected(ButtonEvent ce) {
            final Button button = ce.getButton();
            if (button.getText().equals(CREATE)) {
            	showAddWindow();
            } else if (button.getText().equals(EDIT)) {
            	showEditWindow(grid.getSelectionModel().getSelectedItem());
            } else if (button.getText().equals(DELETE)){
            	
            	MessageBox box = new MessageBox();
                box.setButtons(MessageBox.YESNO);
                box.setIcon(MessageBox.QUESTION);
                box.setTitle("Save Changes?");
                box.addCallback(new Listener<MessageBoxEvent>() {
                    public void handleEvent(MessageBoxEvent mbe) {
                        Button btn = mbe.getButtonClicked();
                        if (btn.getText().equals("Yes")) {
                        	deleteSelectedItem(grid.getSelectionModel().getSelectedItem());
                        }
                        if (btn.getText().equals("No")) {
                        }
                       
                    }
                });
                box.setTitle("Delete?");
                box.setMessage("This will delete the selected item permanently. Are you sure you would like to proceed?");
                box.show();
            }
        }
    };
	
	public AbstractAdminPanel(){
		grid = createGrid();
		gridWrapper = createGridWrapper();
	    gridWrapper.add(grid);
	    this.add(gridWrapper);
	   
	    btnEdit = new Button(EDIT, buttonListener);
        btnCreate = new Button(CREATE, buttonListener);
        btnDelete = new Button(DELETE, buttonListener);
        
        gridWrapper.addButton(btnEdit);
        gridWrapper.addButton(btnCreate);
//        gridWrapper.addButton(btnDelete);
        GeneralComponentBinding selectionBinding = new GeneralComponentBinding(grid) {
			protected boolean isButtonEnabled(Button button) {
				return grid.getSelectionModel().getSelectedItems().size() == 1 && isButtonPermitted(button);
			}
		};
		selectionBinding.addButton(btnEdit);
		selectionBinding.addButton(btnDelete);
		setPermissions();
	}

	protected Grid<T> createGrid() {
		Grid<T> grid = new EditorGrid<T>(new ListStore<T>(), new ColumnModel(getColumnConfigs()));
	    GridView view = new GridView();
	    grid.setView(view);
//	    grid.setAutoExpandColumn("name");
	    grid.setBorders(true);
	    grid.setSelectionModel(new GridSelectionModel<T>());
	    grid.setHeight(157);
        grid.setColumnResize(true);
        grid.setStyleAttribute("borderTop", "none");
        grid.setStripeRows(true);
		return grid;
	}
	
	protected ContentPanel createGridWrapper() {
		ContentPanel gridWrapper = new ContentPanel();
		gridWrapper.setBodyBorder(false);
	    gridWrapper.setButtonAlign(HorizontalAlignment.CENTER);
	    gridWrapper.setLayout(new FitLayout());
	    gridWrapper.setStyleAttribute("margin-left", "10px");
	    gridWrapper.setStyleAttribute("margin-top", "10px");
	    gridWrapper.setButtonAlign(HorizontalAlignment.CENTER);
	    gridWrapper.setFrame(true);
	    gridWrapper.setSize(1210, 500);
	    return gridWrapper;
	}
	
	protected abstract List<ColumnConfig> getColumnConfigs();
	protected abstract void showAddWindow();
	protected abstract void showEditWindow(T model);
	protected abstract void deleteSelectedItem(T model);
	protected abstract void setPermissions();
	protected abstract boolean isButtonPermitted(Button button);
	
	public abstract void setData(List<T> models);
	public abstract void finishEdit(T model);
	public abstract void finishDelete(T model, boolean isSuccess);
	
}
