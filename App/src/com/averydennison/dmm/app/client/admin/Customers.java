package com.averydennison.dmm.app.client.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.averydennison.dmm.app.client.App;
import com.averydennison.dmm.app.client.AppService;
import com.averydennison.dmm.app.client.Service;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

public class Customers extends AbstractAdminPanel<CustomerDTO> {

    public CustomersWindow customersWindow = new CustomersWindow();
    
	public Customers() {
		grid.getView().setEmptyText("No available customers");
		gridWrapper.setHeading("Customers");
		
		this.customersWindow.addListener(Events.Submit, new Listener<BaseEvent>() {
			@Override
			public void handleEvent(BaseEvent be) {
				if(be.getSource() instanceof CustomerDTO){
					CustomerDTO dto = ((CustomerDTO)be.getSource());
					AppService.App.getInstance().saveCustomerDTO(dto, new Service.SaveAdminDTO<CustomerDTO>(Customers.this));
				}else{
					System.err.println("Wrong data type for add item window submit event");
				}
			}
		});
	}

	protected List<ColumnConfig> getColumnConfigs() {
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

	    ColumnConfig column = new ColumnConfig(CustomerDTO.CUSTOMER_ID, "ID", 50);
        column.setAlignment(HorizontalAlignment.LEFT);
        configs.add(column);

	    column = new ColumnConfig(CustomerDTO.CUSTOMER_NAME, "Customer Name", 150);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
	    
	    column = new ColumnConfig(CustomerDTO.CUSTOMER_TYPE_NAME, "Customer Type", 125);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
	    
	    column = new ColumnConfig(CustomerDTO.CUSTOMER_PARENT_NAME, "Parent", 125);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
	    
	    column = new ColumnConfig(CustomerDTO.CUSTOMER_CODE, "Customer Code", 100);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
	    
	    column = new ColumnConfig(CustomerDTO.COMPANY_CODE, "Company Code", 100);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);

	    column = new ColumnConfig(CustomerDTO.CUSTOMER_CMA_NAME, "CMA", 150);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);

	    column = new ColumnConfig(CustomerDTO.CUSTOMER_COUNTRY_NAME, "Country", 100);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
	    
	    column = new ColumnConfig(CustomerDTO.ACTIVE, "Active?", 100);
	    column.setRenderer(new GridCellRenderer<CustomerDTO>() {
			public Object render(CustomerDTO model, String property, ColumnData config, int rowIndex, int colIndex,
					ListStore<CustomerDTO> store, Grid<CustomerDTO> grid) {
				return model.isActive() ? "Yes" : "No";
			}
		});
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);

		return configs;
	}
	
	protected void showAddWindow() {
		customersWindow.makeNew();
		customersWindow.setParentCustomers(grid.getStore().getModels());
		customersWindow.show();
	}
	
	protected void showEditWindow(final CustomerDTO customer) {
		customersWindow.setParentCustomers(grid.getStore().getModels());
		customersWindow.setDTO(customer);
		customersWindow.show();
	}
	
	public void setData(List<CustomerDTO> models) {
		grid.getStore().removeAll();
		grid.getStore().add(models);
	}
	
	public void finishEdit(CustomerDTO result){
		if(grid.getStore().contains(result)){
			grid.getStore().update(result);
		} else {
			grid.getStore().add(result);
			grid.getSelectionModel().setSelection(Arrays.asList(result));
			grid.getView().focusRow(grid.getStore().indexOf(result));
		}
		customersWindow.closeWindow();
	}

	public void finishDelete(CustomerDTO model, boolean isSuccess) {
		btnDelete.enable();
		if(isSuccess){
			grid.getStore().remove(model);
		}
	}

	protected void deleteSelectedItem(CustomerDTO model) {
		btnDelete.disable();
        AppService.App.getInstance().deleteCustomerDTO(model, new Service.DeleteAdminDTO<CustomerDTO>(Customers.this, model));
	}

	protected void setPermissions() {
    	if(!App.isAuthorized("Customers.CREATE")) btnCreate.disable();
    	if(!App.isAuthorized("Customers.EDIT")) btnEdit.disable();
    	if(!App.isAuthorized("Customers.DELETE")) btnDelete.disable();
	}

	protected boolean isButtonPermitted(Button button) {
		if(button.getText().equals(CREATE)) return App.isAuthorized("Customers.CREATE");
		if(button.getText().equals(EDIT)) return App.isAuthorized("Customers.EDIT");
		if(button.getText().equals(DELETE)) return App.isAuthorized("Customers.DELETE");
		return false;
	}
}
