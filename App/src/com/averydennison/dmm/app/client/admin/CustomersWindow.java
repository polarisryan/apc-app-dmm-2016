package com.averydennison.dmm.app.client.admin;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.averydennison.dmm.app.client.models.CustomerDTO;

public class CustomersWindow extends AbstractAdminWindow<CustomerDTO>{
	
	public CustomersWindow(){
	    this.setHeading("Customer");
	}
	
	protected void buildForm() {
		this.form = new CustomerForm();
	}
	
	public void setDTO(CustomerDTO dto) {
		super.setDTO(dto);
		setFocusWidget(((CustomerForm)this.form).name);
	}

	public void makeNew() {
		super.makeNew();
		setFocusWidget(((CustomerForm)this.form).name);
	}
	
	@Override
	protected CustomerDTO makeDTOInstance() {
		return new CustomerDTO();
	}

	public void setParentCustomers(List<CustomerDTO> models) {
		CustomerForm customerForm = (CustomerForm)this.form;
		customerForm.parent.getStore().removeAll();
		Collections.sort(models, new Comparator<CustomerDTO>(){
			public int compare(CustomerDTO o1, CustomerDTO o2) {
				return o1.getCustomerName().compareTo(o2.getCustomerName());
			}
		});
		customerForm.parent.getStore().add(new CustomerDTO(0l, "None", null));
		customerForm.parent.getStore().add(models);
		customerForm.parent.setValue(new CustomerDTO(0l, "None", null));
	}

}
