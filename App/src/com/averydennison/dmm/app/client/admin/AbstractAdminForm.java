package com.averydennison.dmm.app.client.admin;

import com.averydennison.dmm.app.client.models.CountryDTO;
import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;

public abstract class AbstractAdminForm<T extends BaseModel> extends FormPanel{
	
	protected static final String MESSAGE_TARGET = "tooltip";
    public static final String YES = "Yes";
    public static final String NO = "No";
    
	protected class ExtendedTextField extends TextField<String>{
		public El getLabel(){
			return findLabelElement();
		}		
	}
	
	public AbstractAdminForm() {
	    this.setHeaderVisible(false);
	    this.setBorders(false);
		this.setBodyBorder(false);
	}
	
	protected void populateTextFieldWithRequiredData(TextField<?> field, String labelTitle, boolean required, Integer maxLength) {
		field.setFieldLabel(labelTitle);
		field.setMessageTarget("tooltip");
		field.setAllowBlank(!required);
		field.setLabelStyle(getRequirityFieldStyle(100, 12, false));
		if(maxLength != null) {
			field.setMaxLength(maxLength);
			field.setAutoValidate(true);
		}
	}
	
	protected <D extends BaseModel> void populateComboBoxWithRequiredData(ComboBox<D> field, String labelTitle, boolean required, Integer maxLength) {
		populateTextFieldWithRequiredData(field, labelTitle, required, maxLength);
		field.setEditable(false);
		field.setDisplayField("name");
		field.setValueField("id");
		field.setStore(new ListStore<D>());
		field.setTriggerAction(TriggerAction.ALL);
	}
	
	protected FormLayout getFormLayout(int labelWidth, int fieldWidth) {
		FormLayout layout = new FormLayout();  
		layout.setLabelWidth(labelWidth);  
		layout.setDefaultWidth(fieldWidth);
		return layout;
	}
	
	protected String getRequirityFieldStyle(int labelWidth, int labelLeftPadding, boolean required) {
		StringBuilder builder = new StringBuilder();
		builder.append("padding-left: " + labelLeftPadding + "px;");
		builder.append("white-space: nowrap;");
		if(required){
			builder.append("background: url(../images/asterisk_v10px.png) no-repeat 0px 5px;");
		}
		builder.append("width: ").append((labelWidth - labelLeftPadding)).append("px;");
		return builder.toString();
	}

	protected CountryDTO getCountryById(Long countryId, Store<CountryDTO> store) {
		for(CountryDTO countryDTO: store.getModels()){
			if(countryDTO.getCountryId().equals(countryId)){
				return countryDTO;
			}
		}
		return null;
	}
	
	public abstract void init();
	public abstract void updateDTO(T dto);
	public abstract void setDTO(T dto);
	
}
