package com.averydennison.dmm.app.client.admin;

import com.averydennison.dmm.app.client.models.LocationCountryDTO;

public class PackoutLocationsWindow extends AbstractAdminWindow<LocationCountryDTO>{
	
	public PackoutLocationsWindow(){
	    this.setHeading("Packout Location");
	}
	
	protected void buildForm() {
		this.form = new PackoutLocationForm();
	}
	
	public void setDTO(LocationCountryDTO dto) {
		super.setDTO(dto);
		setFocusWidget(((PackoutLocationForm)this.form).name);
	}

	public void makeNew() {
		super.makeNew();
		setFocusWidget(((PackoutLocationForm)this.form).name);
	}
	
	@Override
	protected LocationCountryDTO makeDTOInstance() {
		return new LocationCountryDTO();
	}

}
