package com.averydennison.dmm.app.client.admin;

import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;

public class CorrugateVendorsWindow extends AbstractAdminWindow<ManufacturerCountryDTO>{
	
	public CorrugateVendorsWindow(){
	    this.setHeading("Corrugate Vendor");
	}
	
	protected void buildForm() {
		this.form = new CorrugateVendorForm();
	}
	
	public void setDTO(ManufacturerCountryDTO dto) {
		super.setDTO(dto);
		setFocusWidget(((CorrugateVendorForm)this.form).name);
	}

	public void makeNew() {
		super.makeNew();
		setFocusWidget(((CorrugateVendorForm)this.form).name);
	}
	
	@Override
	protected ManufacturerCountryDTO makeDTOInstance() {
		return new ManufacturerCountryDTO();
	}

}
