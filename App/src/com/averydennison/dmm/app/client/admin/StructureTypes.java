package com.averydennison.dmm.app.client.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.averydennison.dmm.app.client.App;
import com.averydennison.dmm.app.client.AppService;
import com.averydennison.dmm.app.client.Service;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

public class StructureTypes extends AbstractAdminPanel<StructureDTO> {

	public StructureTypesWindow window = new StructureTypesWindow();
	
	public StructureTypes() {
		grid.getView().setEmptyText("No available structure types");
		gridWrapper.setHeading("Structure Types");
		this.window.addListener(Events.Submit, new Listener<BaseEvent>() {
			@Override
			public void handleEvent(BaseEvent be) {
				if(be.getSource() instanceof StructureDTO){
					StructureDTO dto = ((StructureDTO)be.getSource());
					AppService.App.getInstance().saveStructureDTO(dto, new Service.SaveAdminDTO<StructureDTO>(StructureTypes.this));
				}else{
					System.err.println("Wrong data type for add item window submit event");
				}
			}
		});
	}
	
	protected List<ColumnConfig> getColumnConfigs() {
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

	    ColumnConfig column = new ColumnConfig(StructureDTO.STRUCTURE_ID, "ID", 50);
        column.setAlignment(HorizontalAlignment.LEFT);
        configs.add(column);

	    column = new ColumnConfig(StructureDTO.STRUCTURE_NAME, "Structure Type", 200);
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);
	    
	    column = new ColumnConfig(StructureDTO.ACTIVE_FLG, "Active?", 100);
	    column.setRenderer(new GridCellRenderer<StructureDTO>() {
			@Override
			public Object render(StructureDTO model, String property,	ColumnData config, int rowIndex, int colIndex,
					ListStore<StructureDTO> store, Grid<StructureDTO> grid) {
				
				return model.isActiveFlg() ? "Yes" : "No";
			}
		});
	    column.setAlignment(HorizontalAlignment.LEFT);
	    configs.add(column);

		return configs;
	}

	public void setData(List<StructureDTO> models) {
		grid.getStore().removeAll();
		grid.getStore().add(models);
	}

	@Override
	protected void deleteSelectedItem(StructureDTO model) {
		btnDelete.disable();
        AppService.App.getInstance().deleteStructureDTO(model, new Service.DeleteAdminDTO<StructureDTO>(StructureTypes.this, model));
	}

	@Override
	public void finishDelete(StructureDTO model, boolean isSuccess) {
		btnDelete.enable();
		if(isSuccess){
			grid.getStore().remove(model);
		}		
	}

	@Override
	public void finishEdit(StructureDTO model) {
		if(grid.getStore().contains(model)){
			grid.getStore().update(model);
		} else {
			grid.getStore().add(model);
			grid.getSelectionModel().setSelection(Arrays.asList(model));
			grid.getView().focusRow(grid.getStore().indexOf(model));
		}
		window.closeWindow();
	}

	@Override
	protected void showAddWindow() {
		window.makeNew();
		window.show();
	}

	@Override
	protected void showEditWindow(StructureDTO model) {
		window.setDTO(model);
		window.show();
	}

	protected void setPermissions() {
    	if(!App.isAuthorized("StructureTypes.CREATE")) btnCreate.disable();
    	if(!App.isAuthorized("StructureTypes.EDIT")) btnEdit.disable();
    	if(!App.isAuthorized("StructureTypes.DELETE")) btnDelete.disable();
	}
	
	protected boolean isButtonPermitted(Button button) {
		if(button.getText().equals(CREATE)) return App.isAuthorized("StructureTypes.CREATE");
		if(button.getText().equals(EDIT)) return App.isAuthorized("StructureTypes.EDIT");
		if(button.getText().equals(DELETE)) return App.isAuthorized("StructureTypes.DELETE");
		return false;
	}
}
