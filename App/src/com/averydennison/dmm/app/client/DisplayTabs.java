package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.TabPanelEvent;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.user.client.Timer;

/**
 * User: Spart Arguello Date: Oct 16, 2009 Time: 11:16:36 AM
 */
public class DisplayTabs extends LayoutContainer {
	public static final String DISPLAY_GENERAL_INFO = "Display General Info";
	public static final String PACKOUT_DETAIL = "Fulfillment";
	public static final String KIT_COMPONENT_INFO = "Kit Component Info";
	public static final String CUSTOMER_MARKETING_INFO = "Customer Marketing";
	public static final String DISPLAY_LOGISTICS_INFO = "Display Attributes";
	public static final String CORRUGATE_COMPONENT_INFO = "Corrugate Components";
	public static final String COST_ANALYSIS_INFO = "Cost Analysis";

	private DisplayDTO displayModel = new DisplayDTO();
	private TabPanel panel = new TabPanel();
	private TabItem displayGeneralInfoTab = new TabItem(DISPLAY_GENERAL_INFO);
	private TabItem packoutDetailInfoTab = new TabItem(PACKOUT_DETAIL);
	private TabItem kitComponentInfoTab = new TabItem(KIT_COMPONENT_INFO);
	private TabItem customerMarketingInfoTab = new TabItem(CUSTOMER_MARKETING_INFO);
	private TabItem displayLogisticsInfoTab = new TabItem(DISPLAY_LOGISTICS_INFO);
	private TabItem corrugateComponentInfoTab = new TabItem(CORRUGATE_COMPONENT_INFO);
	private TabItem costAnalysisInfoTab = new TabItem(COST_ANALYSIS_INFO);
	private static ChangeListener displayListener;
	DisplayGeneralInfo displayGeneralInfo = new DisplayGeneralInfo();
	PackoutDetailInfo packoutDetailInfo = new PackoutDetailInfo();
	KitComponentInfo kitComponentInfo = new KitComponentInfo();
	CustomerMarketingInfo customerMarketingInfo = new CustomerMarketingInfo();
	DisplayLogisticsInfo displayLogisticsInfo = new DisplayLogisticsInfo();
	CorrugateComponentInfo corrugateComponentInfo = new CorrugateComponentInfo();
	CostAnalysisInfo costAnalysisInfo = new CostAnalysisInfo();

	public DisplayTabs() {
		setup();
		setPermissions();
		App.setDisplayTabs(this);
	}

	public DisplayTabs(DisplayDTO displayDTO) {
		setData("display", displayDTO);
		setup();
		setPermissions();
		App.setDisplayTabs(this);
	}

	public DisplayTabs(DisplayDTO displayDTO, Boolean readOnly, Boolean copy) {
		displayModel = displayDTO;
		setData("display", displayDTO);
		setData("readOnly", readOnly);
		setData("copy", copy);
		setup();
		setPermissions();
		App.setDisplayTabs(this);
	}

	public DisplayTabs(DisplayDTO displayDTO, Boolean readOnly,
			String selectedTab, Boolean copy) {
		displayModel = displayDTO;
		setData("display", displayDTO);
		setData("readOnly", readOnly);
		setData("copy", copy);
		setup();
		setPermissions();
		if (selectedTab.equals(DISPLAY_GENERAL_INFO)) {
			panel.setSelection(displayGeneralInfoTab);
		} else if (selectedTab.equals(PACKOUT_DETAIL)) {
			panel.setSelection(packoutDetailInfoTab);
		} else if (selectedTab.equals(KIT_COMPONENT_INFO)) {
			panel.setSelection(kitComponentInfoTab);
		} else if (selectedTab.equals(CUSTOMER_MARKETING_INFO)) {
			panel.setSelection(customerMarketingInfoTab);
		} else if (selectedTab.equals(DISPLAY_LOGISTICS_INFO)) {
			panel.setSelection(displayLogisticsInfoTab);
		} else if (selectedTab.equals(CORRUGATE_COMPONENT_INFO)) {
			panel.setSelection(corrugateComponentInfoTab);
		} else if (selectedTab.equals(COST_ANALYSIS_INFO)) {
			panel.setSelection(costAnalysisInfoTab);
		}
		App.setDisplayTabs(this);
	}

	private void setPermissions() {

	}

	private TabItem getSelectedTab() {
		return this.panel.getSelectedItem();
	}

	private TabItem nextTab;

	public TabItem getNextTab() {
		return nextTab;
	}

	public void setNextTab(TabItem nextTab) {
		this.nextTab = nextTab;
	}

	private void setup() {
		System.out.println("DisplayTabs:setup starts");
		VerticalPanel vp = new VerticalPanel();
		vp.setSpacing(10);
		panel.setPlain(true);
		panel.setAutoSelect(true);
		panel.setSize(1210, 600);
		panel.addListener(Events.BeforeSelect, new Listener<TabPanelEvent>() {
			public void handleEvent(TabPanelEvent tpe) {
				if (tpe.getItem() != null)
					setNextTab(tpe.getItem());
			}
		});
		panel.addListener(Events.BeforeSelect, new Listener<BaseEvent>() {
			public void handleEvent(BaseEvent be) {
				try {
					if (be.getSource() instanceof TabPanel) {
						TabPanel tp = (TabPanel) be.getSource();
						if (tp.getSelectedItem() != null) {
							if (getSelectedTab().getText().equals(
									tp.getSelectedItem().getText())) {
								TabItem ti = tp.getSelectedItem();
								Boolean bDisplayReadOnly;
								if (getData("readOnly") == null)
									bDisplayReadOnly = Boolean.FALSE;
								else
									bDisplayReadOnly = getData("readOnly");
								if (ti.getWidget(0) instanceof DisplayGeneralInfo) {
									if (bDisplayReadOnly) {
										displayGeneralInfo.overrideDirty();
									}
									if (displayGeneralInfo.isDirty()) {
										be.setCancelled(true);
										String title = "You have unsaved information!";
										String msg = "Would you like to save?";
										MessageBox box = new MessageBox();
										box.setButtons(MessageBox.YESNOCANCEL);
										box.setIcon(MessageBox.QUESTION);
										box.setTitle("Save Changes?");
										box
												.addCallback(new Listener<MessageBoxEvent>() {
													public void handleEvent(
															MessageBoxEvent mbe) {
														Button btn = mbe
																.getButtonClicked();
														if (btn.getText()
																.equals("Yes")) {
															displayGeneralInfo
																	.save(
																			btn,
																			panel,
																			nextTab);
														}
														if (btn.getText()
																.equals("No")) {
															displayGeneralInfo
																	.overrideDirty();
															panel
																	.setSelection(getNextTab());
														}
														if (btn
																.getText()
																.equals(
																		"Cancel")) {
														}
													}
												});
										box.setTitle(title);
										box.setMessage(msg);
										box.show();
									}
								}
								if (ti.getWidget(0) instanceof KitComponentInfo) {
									if (bDisplayReadOnly) {
										kitComponentInfo.overrideDirty();
									}
									if (kitComponentInfo.isDirty()) {
										be.setCancelled(true);
										String title = "You have unsaved information!";
										String msg = "Would you like to save?";
										final MessageBox kitBox = new MessageBox();
										kitBox
												.setButtons(MessageBox.YESNOCANCEL);
										kitBox.setIcon(MessageBox.QUESTION);
										kitBox.setTitle("Save Changes?");
										kitBox
												.addCallback(new Listener<MessageBoxEvent>() {
													public void handleEvent(
															MessageBoxEvent mbe) {
														Button btn = mbe
																.getButtonClicked();
														if (btn.getText()
																.equals("Yes")) {
															kitComponentInfo
																	.getKitComponentInfoGrid()
																	.save(
																			btn,
																			panel,
																			nextTab);
														}
														if (btn.getText()
																.equals("No")) {
															kitComponentInfo
																	.overrideDirty();
															panel
																	.setSelection(getNextTab());
														}
														if (btn
																.getText()
																.equals(
																		"Cancel")) {
														}
													}
												});
										kitBox.setTitle(title);
										kitBox.setMessage(msg);
										kitBox.show();
									}
								}
								if (ti.getWidget(0) instanceof CostAnalysisInfo) {
									if (bDisplayReadOnly) {
										costAnalysisInfo.overrideDirty();
									}
								}
								if (ti.getWidget(0) instanceof PackoutDetailInfo) {
									if (bDisplayReadOnly) {
										packoutDetailInfo.overrideDirty();
									}
									if (packoutDetailInfo.isDirty()) {
										be.setCancelled(true);
										String title = "You have unsaved information!";
										String msg = "Would you like to save?";
										MessageBox packoutBox = new MessageBox();
										packoutBox
												.setButtons(MessageBox.YESNOCANCEL);
										packoutBox.setIcon(MessageBox.QUESTION);
										packoutBox.setTitle("Save Changes?");
										packoutBox
												.addCallback(new Listener<MessageBoxEvent>() {
													public void handleEvent(
															MessageBoxEvent mbe) {
														Button btn = mbe
																.getButtonClicked();
														if (btn.getText()
																.equals("Yes")) {
															packoutDetailInfo
																	.save(
																			btn,
																			panel,
																			getNextTab());
														}
														if (btn.getText()
																.equals("No")) {
															packoutDetailInfo
																	.overrideDirty();
															panel
																	.setSelection(getNextTab());
														}
														if (btn
																.getText()
																.equals(
																		"Cancel")) {

														} else {
															for (int i = 0; i < packoutDetailInfo.packoutDetailsTree
																	.getDisplayDcAndPackoutList()
																	.size(); i++) {
																packoutDetailInfo.packoutDetailsTree
																		.getDisplayDcAndPackoutList()
																		.get(i)
																		.setDirty(
																				false);
															}
															packoutDetailInfo.packoutDetailInfoShippingWavesGrid
																	.setDisplayShipWaveModified(false);
															packoutDetailInfo.packoutDetailsTree
																	.setDisplayPackoutModified(false);
														}
													}
												});
										packoutBox.setTitle(title);
										packoutBox.setMessage(msg);
										packoutBox.show();
									}
								}
								if (ti.getWidget(0) instanceof CustomerMarketingInfo) {
									if (bDisplayReadOnly) {
										customerMarketingInfo.overrideDirty();
									}
									if (customerMarketingInfo.isDirty()) {
										be.setCancelled(true);
										String title = "You have unsaved information!";
										String msg = "Would you like to save?";
										MessageBox box = new MessageBox();
										box.setButtons(MessageBox.YESNOCANCEL);
										box.setIcon(MessageBox.QUESTION);
										box.setTitle("Save Changes?");
										box
												.addCallback(new Listener<MessageBoxEvent>() {
													public void handleEvent(
															MessageBoxEvent mbe) {
														Button btn = mbe
																.getButtonClicked();
														if (btn.getText()
																.equals("Yes")) {
															customerMarketingInfo
																	.save(
																			btn,
																			panel,
																			nextTab);
															panel
																	.setSelection(getNextTab());
														}
														if (btn.getText()
																.equals("No")) {
															customerMarketingInfo
																	.overrideDirty();
															panel
																	.setSelection(getNextTab());
														}
														if (btn
																.getText()
																.equals(
																		"Cancel")) {
														}
													}
												});
										box.setTitle(title);
										box.setMessage(msg);
										box.show();
									}
								}
								if (ti.getWidget(0) instanceof DisplayLogisticsInfo) {
									if (bDisplayReadOnly) {
										displayLogisticsInfo.overrideDirty();
									}
									if (displayLogisticsInfo.isDirty()) {
										be.setCancelled(true);
										String title = "You have unsaved information!";
										String msg = "Would you like to save?";
										MessageBox box = new MessageBox();
										box.setButtons(MessageBox.YESNOCANCEL);
										box.setIcon(MessageBox.QUESTION);
										box.setTitle("Save Changes?");
										box
												.addCallback(new Listener<MessageBoxEvent>() {
													public void handleEvent(
															MessageBoxEvent mbe) {
														Button btn = mbe
																.getButtonClicked();
														if (btn.getText()
																.equals("Yes")) {
															displayLogisticsInfo
																	.save(
																			btn,
																			panel,
																			nextTab);
															panel
																	.setSelection(getNextTab());
														}
														if (btn.getText()
																.equals("No")) {
															displayLogisticsInfo
																	.overrideDirty();
															panel
																	.setSelection(getNextTab());
														}
														if (btn
																.getText()
																.equals(
																		"Cancel")) {
														}
													}
												});
										box.setTitle(title);
										box.setMessage(msg);
										box.show();
									}
								}
								if (ti.getWidget(0) instanceof CorrugateComponentInfo) {
									if (bDisplayReadOnly) {
										corrugateComponentInfo.corrCompEditorGrid.overrideDirty();
									}
									if (corrugateComponentInfo.corrCompEditorGrid.isDirty()) {
										be.setCancelled(true);
										String title = "You have unsaved information!";
										String msg = "Would you like to save?";
										MessageBox box = new MessageBox();
										box.setButtons(MessageBox.YESNOCANCEL);
										box.setIcon(MessageBox.QUESTION);
										box.setTitle("Save Changes?");
										box
												.addCallback(new Listener<MessageBoxEvent>() {
													public void handleEvent(
															MessageBoxEvent mbe) {
														Button btn = mbe
																.getButtonClicked();
														if (btn.getText()
																.equals("Yes")) {
															corrugateComponentInfo
																	.corrCompEditorGrid.save(
																			btn,
																			panel,
																			nextTab);
															panel
																	.setSelection(getNextTab());
														}
														if (btn.getText()
																.equals("No")) {
															corrugateComponentInfo
																	.corrCompEditorGrid.overrideDirty();
															panel
																	.setSelection(getNextTab());
														}
														if (btn
																.getText()
																.equals(
																		"Cancel")) {
														}
													}
												});
										box.setTitle(title);
										box.setMessage(msg);
										box.show();
									}
								}
							}
						}
					}
				} catch (Exception e) {
					System.out
							.println("..DisplayTabs:panel.addListener(Events.BeforeSelect, new Listener<BaseEvent>() Error.... :"
									+ e.toString());
					// Mari: to resolve non-working of tab click event Dt:
					// 09-03-2010
				}
			}
		});
		if (this.getData("display") != null) {
			displayModel = (DisplayDTO) this.getData("display");
		}
		displayListener = new ChangeListener() {
			public void modelChanged(
					com.extjs.gxt.ui.client.data.ChangeEvent event) {
				displayModel = (DisplayDTO) event.getSource();
				if (displayModel != null && displayModel.getDisplayId() != null) {
					setData("display", displayModel);
				}
			}
		};
		if (displayModel == null) {
			displayModel = new DisplayDTO();
		}
		displayModel.addChangeListener(displayListener);
		/*
		 * Tab:1 Display General Info
		 */
		if (this.getData("display") != null) {
			displayGeneralInfo.setData("display", this.getData("display"));
		}
		if (this.getData("readOnly") != null) {
			displayGeneralInfo.setData("readOnly", this.getData("readOnly"));
		}
		if (this.getData("copy") != null) {
			displayGeneralInfo.setData("copy", this.getData("copy"));
		}
		displayGeneralInfo.setDisplayTabs(this);
		displayGeneralInfoTab.addListener(Events.Select,
				new Listener<ComponentEvent>() {
					public void handleEvent(ComponentEvent be) {
						final MessageBox box = MessageBox.wait("Progress",
								"Loading display general info, please wait...",
								"Loading...");
						Timer t = new Timer() {
							public void run() {
								box.close();
							}
						};
						panel.setSize(1210, 650);
						panel.setResizeTabs(false);
						t.schedule(500);
						displayGeneralInfo.reload();
						Boolean b = displayGeneralInfo.getData("copy");
						if (b == null)
							return;
						if (b) {
							displayGeneralInfo.copyDisplayGeneralInfoRecord();
						}
					}
				});
		displayGeneralInfoTab.add(displayGeneralInfo);
		panel.add(displayGeneralInfoTab);

		/*
		 * Tab:2 Cost Analysis Info
		 */
		// setup cost analysis tab
		/*
		 * By: MA Date : 01/15/2010
		 */
		costAnalysisInfo.setDisplayTabs(this);
		if (this.getData("display") != null) {
			costAnalysisInfo.setData("display", this.getData("display"));
			if (displayModel.getDisplayId() != null
					&& displayModel.getDisplayId() > 0)
				AppService.App.getInstance().getDisplayScenarioDTO(
						displayModel.getDisplayId(),
						new Service.LoadDisplayScenarioInfo(costAnalysisInfo));
		}
		if (this.getData("readOnly") != null) {
			costAnalysisInfo.setData("readOnly", this.getData("readOnly"));
		}
		if (this.getData("copy") != null) {
			costAnalysisInfo.setData("copy", this.getData("copy"));
		}
		costAnalysisInfoTab.addListener(Events.Select,
				new Listener<ComponentEvent>() {
					public void handleEvent(ComponentEvent be) {
						final MessageBox box = MessageBox.wait("Progress",
								"Loading cost analysis info, please wait...",
								"Loading...");
						Timer t = new Timer() {
							public void run() {
								box.close();
							}
						};
						panel.setSize(1620, 1500);
						panel.setResizeTabs(false);
						t.schedule(1500);
						costAnalysisInfo.reload();
					}
				});
		costAnalysisInfoTab.add(costAnalysisInfo);
		panel.add(costAnalysisInfoTab);
		

		/*
		 * Tab:3 Fullfilment Tab (Tab)
		 */
		// setup packout detail tab
		packoutDetailInfo.setDisplayTabs(this);
		/*
		 * By: MA Date : 01/15/2010
		 */
		if (this.getData("display") != null) {
			packoutDetailInfo.setData("display", this.getData("display"));
		}
		if (this.getData("readOnly") != null) {
			packoutDetailInfo.setData("readOnly", this.getData("readOnly"));
		}
		if (this.getData("copy") != null) {
			packoutDetailInfo.setData("copy", this.getData("copy"));
		}
		packoutDetailInfoTab.addListener(Events.Select,
				new Listener<ComponentEvent>() {
					public void handleEvent(ComponentEvent be) {
						final MessageBox box = MessageBox.wait("Progress",
								"Loading packout detail info, please wait...",
								"Loading...");
						Timer t = new Timer() {
							public void run() {
								box.close();
							}
						};
						t.schedule(500);
						panel.setSize(1210, 617);
						panel.setResizeTabs(false);
						packoutDetailInfo.reload();
					}
				});
		packoutDetailInfoTab.add(packoutDetailInfo);
		panel.add(packoutDetailInfoTab);
		/*
		 * Tab:4 Customer Marketing Info
		 */
		// setup customer marketing tab
		customerMarketingInfo.setDisplayTabs(this);
		if (displayModel != null) {
			AppService.App.getInstance().getCustomerMarketingDTO(
					displayModel.getDisplayId(),
					new Service.LoadCustomerMarketing(customerMarketingInfo));
		}
		if (this.getData("readOnly") != null) {
			customerMarketingInfo.setData("readOnly", this.getData("readOnly"));
		}
		if (this.getData("display") != null) {
			customerMarketingInfo.setData("display", this.getData("display"));
			AppService.App.getInstance().getCustomerMarketingDTO(
					((DisplayDTO) this.getData("display")).getDisplayId(),
					new Service.LoadCustomerMarketing(customerMarketingInfo));
		}
		customerMarketingInfoTab.addListener(Events.Select,
				new Listener<ComponentEvent>() {
					public void handleEvent(ComponentEvent be) {
						final MessageBox box = MessageBox
								.wait(
										"Progress",
										"Loading customer marketing info, please wait...",
										"Loading...");
						Timer t = new Timer() {
							public void run() {
								box.close();
							}
						};
						panel.setSize(1210, 550);
						panel.setResizeTabs(false);
						t.schedule(500);
						customerMarketingInfo.reload();
					}
				});
		customerMarketingInfoTab.add(customerMarketingInfo);
		panel.add(customerMarketingInfoTab);
		/*
		 * Tab:5 Display Logistics Info
		 */
		// setup display logistics tab
		displayLogisticsInfo.setDisplayTabs(this);
		if (displayModel != null) {
			displayLogisticsInfo.setData("display", this.getData("display"));
			if (displayModel.getDisplayId() != null
					&& displayModel.getDisplayId() > 0)
				AppService.App.getInstance().getDisplayLogisticsInfoDTO(
						displayModel.getDisplayId(),
						new Service.LoadDisplayLogistics(displayLogisticsInfo));
		}
		/*
		 * By: MA Date : 01/15/2010
		 */
		if (this.getData("readOnly") != null) {
			System.out.println("displayLogisticsInfo:readOnly "
					+ this.getData("readOnly"));
			displayLogisticsInfo.setData("readOnly", this.getData("readOnly"));
		}
		if (this.getData("copy") != null
				&& displayModel.getDisplayLogisticsId() != null) {
			displayLogisticsInfo.setData("copy", this.getData("copy"));
		}
		displayLogisticsInfoTab.addListener(Events.Select,
				new Listener<ComponentEvent>() {
					public void handleEvent(ComponentEvent be) {
						final MessageBox box = MessageBox
								.wait(
										"Progress",
										"Loading display logistics info, please wait...",
										"Loading...");
						Timer t = new Timer() {
							public void run() {
								box.close();
							}
						};
						t.schedule(500);
						panel.setSize(1210, 600);
						panel.setResizeTabs(false);
						displayLogisticsInfo.reload();
					}
				});
		displayLogisticsInfoTab.add(displayLogisticsInfo);
		panel.add(displayLogisticsInfoTab);
		/*
		 * Tab:6 Corrugate Component Info
		 */
		// setup corrugate component tab
		/*
		 * By: MA Date : 01/15/2010
		 */
		if (this.getData("display") != null) {
			corrugateComponentInfo.setData("display", this.getData("display"));
		}
		if (this.getData("readOnly") != null) {
			System.out.println("corrugateComponentInfo:readOnly "
					+ this.getData("readOnly"));
			corrugateComponentInfo
					.setData("readOnly", this.getData("readOnly"));
		}
		if (this.getData("copy") != null) {
			corrugateComponentInfo.setData("copy", this.getData("copy"));
		}
		corrugateComponentInfo.setDisplayTabs(this);
		corrugateComponentInfoTab.addListener(Events.Select,
				new Listener<ComponentEvent>() {
					public void handleEvent(ComponentEvent be) {
						final MessageBox box = MessageBox
								.wait(
										"Progress",
										"Loading corrugate component info, please wait...",
										"Loading...");
						Timer t = new Timer() {
							public void run() {
								box.close();
							}
						};
						t.schedule(500);
						panel.setSize(1210, 600);
						panel.setResizeTabs(false);
						corrugateComponentInfo.reload();
					}
				});
		corrugateComponentInfoTab.add(corrugateComponentInfo);
		panel.add(corrugateComponentInfoTab);
		vp.add(panel);
		add(vp);
	}

	public void reloadScenarios(Long displayScenarioNum){
		  panel.setSelection(costAnalysisInfoTab);
		  costAnalysisInfo.setDisplayTabs(this); if (this.getData("display") != null) { costAnalysisInfo.setData("display", this.getData("display"));
		  if (displayModel.getDisplayId() != null && displayModel.getDisplayId() > 0)
		  AppService.App.getInstance().getDisplayScenarioDTO
		  (displayModel.getDisplayId(),new Service.ReLoadDisplayScenarioInfo(costAnalysisInfo,displayScenarioNum)); }
	}

	public void setDisplayModel(DisplayDTO display) {
		if (displayModel != null && displayModel.getDisplayId() != null) {
			this.displayModel = display;
			updateViews();
		}
	}

	public void reloadScenarios(){
		if (displayModel.getDisplayId() != null
				&& displayModel.getDisplayId() > 0)
			AppService.App.getInstance().getDisplayScenarioDTO(
					displayModel.getDisplayId(),
					new Service.LoadDisplayScenarioInfoForKitComponentInfo(kitComponentInfo, costAnalysisInfo));
		panel.setSelection(kitComponentInfoTab);
	}
	
	public void reloadCopiedScenarios(DisplayDTO copiedDisplayModel){
		  System.out.println("DisplayTabs:reloadCopiedScenarios Starts..");
		  if (copiedDisplayModel != null) {
			  costAnalysisInfo.setData("display", copiedDisplayModel);
			  if (displayModel.getDisplayId() != null && displayModel.getDisplayId() > 0){
				  System.out.println("DisplayTabs:reloadCopiedScenarios ...Success..");
				  AppService.App.getInstance().getDisplayScenarioDTO
				  (displayModel.getDisplayId(),new Service.LoadDisplayScenarioInfo(costAnalysisInfo));
			  }else{
				  System.out.println("DisplayTabs:reloadCopiedScenarios ...Failed..");
			  }
		  }
		  System.out.println("DisplayTabs:reloadCopiedScenarios ends..");
	}
	
	public void resetDisplayModel() {
		displayModel.removeChangeListener(displayListener);
		displayModel.setDisplayId(null);
		displayModel.setCustomerByCustomerId(null);
		displayModel.setCustomerId(null);
		displayModel.setDescription(null);
	}

	public DisplayDTO getDisplayModel() {
		return this.displayModel;
	}

	private static void updateViews() {

	}

}