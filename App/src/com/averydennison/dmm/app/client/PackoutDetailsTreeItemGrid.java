package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.dnd.DND;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.event.DNDListener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Nov 17, 2009
 * Time: 2:45:30 PM
 */
public class PackoutDetailsTreeItemGrid extends LayoutContainer implements ValidationHelper {
    private PackoutDetailsTreeItemGridDropTarget target;
    private ListStore<DisplayPackoutDTO> store = new ListStore<DisplayPackoutDTO>();
    private EditorGrid<DisplayPackoutDTO> editorGrid;

    private DCNode dcNode;

    public PackoutDetailsTreeItemGrid(DCNode dcNode) {
        this.dcNode = dcNode;
    }

    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);

        setLayout(new FlowLayout(10));

        int x = 100;

        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

//        RowNumberer rowNumberer = new RowNumberer();
//        rowNumberer.setHeader("Shipping Wave");

//        configs.add(rowNumberer);

        ColumnConfig shipWave = new ColumnConfig();
        shipWave.setId(DisplayPackoutDTO.SHIP_WAVE_NUM);
        shipWave.setHeader("Ship Wave");

        shipWave.setWidth(x / 2);
        shipWave.setFixed(true);
        shipWave.setResizable(false);
        shipWave.setSortable(false);
        shipWave.setMenuDisabled(true);
        TextField<Integer> text = new TextField<Integer>();
        text.setAllowBlank(false);
        text.setEnabled(false);
        
        Boolean bDisplayReadOnly;
        if(getData("readOnly")==null)
        	bDisplayReadOnly=Boolean.FALSE;
        else 
        	bDisplayReadOnly=getData("readOnly");
        
        if(!bDisplayReadOnly)
        	shipWave.setEditor(new CellEditor(text));
        configs.add(shipWave);

        DateField dateField = new DateField();
        dateField.getPropertyEditor().setFormat(DateTimeFormat.getShortDateFormat());
        ColumnConfig mustArrive = new ColumnConfig();
        mustArrive.setId(DisplayPackoutDTO.MUST_ARRIVE_DATE);
        mustArrive.setHeader("Must Arrive by Date");

        mustArrive.setWidth(x - 15);
        mustArrive.setSortable(false);
        mustArrive.setMenuDisabled(false);
        mustArrive.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        DateField mustArriveDateField = new DateField();
        mustArriveDateField.setEnabled(false);
        mustArriveDateField.setEditable(false);
        if(!bDisplayReadOnly)
        	mustArrive.setEditor(new CellEditor(mustArriveDateField));
        configs.add(mustArrive);

        ColumnConfig shipFrom = new ColumnConfig();
        shipFrom.setId(DisplayPackoutDTO.SHIP_FROM_DATE);
        shipFrom.setHeader("Ship Date");

        shipFrom.setWidth(x - 15);
        shipFrom.setSortable(false);
        shipFrom.setMenuDisabled(true);
        shipFrom.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        DateField shipFromDateField = new DateField();
        shipFromDateField.setEditable(false);
        if(!bDisplayReadOnly)
        	shipFrom.setEditor(new CellEditor(shipFromDateField));
        configs.add(shipFrom);

        ColumnConfig prdDue = new ColumnConfig();
        prdDue.setId(DisplayPackoutDTO.PRODUCT_DUE_DATE);
        prdDue.setHeader("Prod. Due at Packout");

        prdDue.setWidth(x - 15);
        prdDue.setSortable(false);
        prdDue.setMenuDisabled(true);
        prdDue.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        DateField packoutDueDateField = new DateField();
        packoutDueDateField.setEditable(false);
        if(!bDisplayReadOnly)
        	prdDue.setEditor(new CellEditor(packoutDueDateField));
        configs.add(prdDue);

        ColumnConfig qtyColumn = new ColumnConfig();
        qtyColumn.setId(DisplayPackoutDTO.QUANTITY);
        qtyColumn.setHeader("Qty");

        qtyColumn.setAlignment(Style.HorizontalAlignment.LEFT);
        qtyColumn.setWidth(x - 15);
        qtyColumn.setSortable(false);
        qtyColumn.setMenuDisabled(true);
        configs.add(qtyColumn);

//        final TextField<Long> qtyTextField = new TextField<Long>();
//        qtyTextField.setMaxLength(10);
//
//        qtyTextField.setAutoValidate(true);
//        qty.addListener(Events.KeyUp, new Listener<FieldEvent>() {
//            public void handleEvent(FieldEvent be) {
//                if(qty.validate()){
//                   qty.setValue(Long.valueOf(qty.getValue()));
//                }
//            }
//        });
//        qtyTextField.addListener(Events.KeyDown, new Listener<FieldEvent>() {
//
//            public void handleEvent(FieldEvent ke) {
//                int keyCode = ke.getKeyCode();
//                if (!KeyUtil.isDigit(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress()) {
//                    ke.stopEvent();
//                }
//            }
//        });
        TextField<Long> qtyTextField = new TextField<Long>();
        if(!bDisplayReadOnly)
        	qtyColumn.setEditor(new CellEditor(qtyTextField));

        ColumnModel cm = new ColumnModel(configs);

        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setButtonAlign(HorizontalAlignment.CENTER);
        cp.setLayout(new FitLayout());
        cp.setFrame(true);
        cp.setSize(400, 200);

        GridSelectionModel<DisplayPackoutDTO> gridSelectionModel = new GridSelectionModel<DisplayPackoutDTO>();

        editorGrid = new EditorGrid<DisplayPackoutDTO>(store, cm);
        editorGrid.setBorders(true);
        editorGrid.setSize(400, 200);
        editorGrid.setTrackMouseOver(false);
//        editorGrid.addPlugin(rowNumberer);
        editorGrid.setSelectionModel(gridSelectionModel);

        target = new PackoutDetailsTreeItemGridDropTarget(editorGrid);
        target.addDNDListener(new DNDListener() {
            public void dragDrop(DNDEvent e) {
                getEditorGrid().stopEditing();
                DisplayShipWaveDTO displayShipWaveDTO = ((DisplayShipWaveDTO) ((List) e.getData()).get(0));
                if (displayShipWaveDTO.getDisplayShipWaveId() == null) {
                    MessageBox.alert("Save display ship waves", "Display Ship waves should be saved before assigning for packout", null);
                    return;
                }
                DisplayPackoutDTO displayPackoutDTO = new DisplayPackoutDTO();
                displayPackoutDTO.setDisplayShipWaveId(displayShipWaveDTO.getDisplayShipWaveId());
                displayPackoutDTO.setDisplayDcId(getDcNode().getDcModel().getDisplayDcId());
                displayPackoutDTO.setShipWaveNum(displayShipWaveDTO.getShipWaveNum());
                displayPackoutDTO.setMustArriveDate(displayShipWaveDTO.getMustArriveDate());
                displayPackoutDTO.setProductDueDate(new Date());
                displayPackoutDTO.setShipFromDate(new Date());
                displayPackoutDTO.setQuantity(getDcNode().getQty());
                store.add(displayPackoutDTO);
            }
        });
        target.setOperation(DND.Operation.COPY);
        target.setFeedback(DND.Feedback.APPEND);
        target.setAllowSelfAsSource(false);

        cp.add(editorGrid);
//        SelectionListener add = new SelectionListener<ComponentEvent>() {
//            public void componentSelected(ComponentEvent ce) {
//                getEditorGrid().stopEditing();
//                editorGrid.stopEditing();
//
//                DisplayPackoutDTO displayPackoutDTO = new DisplayPackoutDTO();
//                displayPackoutDTO.setShipWaveNum(store.getCount() + 1);
//                displayPackoutDTO.setMustArriveDate(new Date());
//                displayPackoutDTO.setShipFromDate(new Date());
//                displayPackoutDTO.setProductDueDate(new Date());
//                displayPackoutDTO.setQuantity(0L);
//
//                store.add(displayPackoutDTO);
//
//                editorGrid.startEditing(store.getCount() - 1, 1);
//            }
//        };
        SelectionListener delete = new SelectionListener<ComponentEvent>() {
            public void componentSelected(ComponentEvent ce) {
                editorGrid.stopEditing();
                store.remove(editorGrid.getSelectionModel().getSelectedItem());
            }
        };
//        cp.addButton(new Button("Add", add));
        cp.addButton(new Button("Delete", delete));
        add(cp);
    }

    public boolean validateControls() {
        return true;
    }

    public Store getStore() {
        return store;
    }

    public EditorGrid getEditorGrid() {
        return editorGrid;
    }

    public DCNode getDcNode() {
        return dcNode;
    }

    public Long getPackoutDetailsTotalQty() {
        Long totalQuantity = new Long(0);
        this.editorGrid.stopEditing();
        this.store.commitChanges();
        List<DisplayPackoutDTO> list = this.store.getModels();

        for (int i = 0; i < list.size(); i++) {
            totalQuantity += list.get(i).getQuantity();
        }
        return totalQuantity;
    }

    public boolean checkTotalQty() {
        if (getPackoutDetailsTotalQty().equals(0L)) return true;
        return getPackoutDetailsTotalQty().equals(dcNode.getQty());
    }

    public Long getQty() {
        return getPackoutDetailsTotalQty();
    }
}