package com.averydennison.dmm.app.client;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.averydennison.dmm.app.client.Service.LoadProgramBu;
import com.averydennison.dmm.app.client.Service.SetProjectManagerStore;
import com.averydennison.dmm.app.client.Service.UpdateProgramBu;
import com.averydennison.dmm.app.client.models.BooleanDTO;
import com.averydennison.dmm.app.client.models.BuCostCenterSplitDTO;
import com.averydennison.dmm.app.client.models.CalculationSummaryDTO;
import com.averydennison.dmm.app.client.models.CorrugateComponentsDetailDTO;
import com.averydennison.dmm.app.client.models.CorrugateDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisHistoryDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.averydennison.dmm.app.client.models.CostPriceDTO;
import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.CstBuPrgmPctDTO;
import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.CustomerMarketingDTO;
import com.averydennison.dmm.app.client.models.CustomerTypeDTO;
import com.averydennison.dmm.app.client.models.DisplayCostCommentDTO;
import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.client.models.DisplayCostImageDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayDcAndPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayDcDTO;
import com.averydennison.dmm.app.client.models.DisplayForMassUpdateDTO;
import com.averydennison.dmm.app.client.models.DisplayLogisticsDTO;
import com.averydennison.dmm.app.client.models.DisplayPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayPlannerDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.client.models.InPogDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.LocationDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerDTO;
import com.averydennison.dmm.app.client.models.ProductDetailDTO;
import com.averydennison.dmm.app.client.models.ProgramBuDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodDTO;
import com.averydennison.dmm.app.client.models.SlimPackoutDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.averydennison.dmm.app.client.models.UserDTO;
import com.averydennison.dmm.app.client.models.UserTypeDTO;
import com.averydennison.dmm.app.client.models.ValidBuDTO;
import com.averydennison.dmm.app.client.models.ValidLocationDTO;
import com.averydennison.dmm.app.client.models.ValidPlannerDTO;
import com.averydennison.dmm.app.client.models.ValidPlantDTO;
import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * User: Spart Arguello
 * Date: Oct 13, 2009
 * Time: 2:14:17 PM
 */
public interface AppServiceAsync {

    void getMessage(String msg, AsyncCallback<String> async);

    void getDisplays(AsyncCallback<List<DisplayDTO>> async);
    
    void getSelectedDisplaysForMassUpdate(List<Long> parameters , AsyncCallback<List<DisplayForMassUpdateDTO>> async);
    
    void massUpdateSelectedItems(String userName,  List<Long> parameters, DisplayForMassUpdateDTO displayMassUpdateDTO,AsyncCallback<List<DisplayForMassUpdateDTO>> async);

    void getStatusDTOs(AsyncCallback<List<StatusDTO>> async);

    void getCustomerDTOs(AsyncCallback<List<CustomerDTO>> async);
    
    void getCustomerCountryDTOs(AsyncCallback<List<CustomerCountryDTO>> async);

    void getStructureDTOs(AsyncCallback<List<StructureDTO>> async);

    void getDisplaySalesRepDTOs(AsyncCallback<List<DisplaySalesRepDTO>> async);

    void getPromoPeriodDTOs(AsyncCallback<List<PromoPeriodDTO>> async);
    
    void getPromoPeriodCountryDTOs(AsyncCallback<List<PromoPeriodCountryDTO>> async);

    void saveDisplayDTO(DisplayDTO display, AsyncCallback<DisplayDTO> async);
    
    void copyDisplayDTO(DisplayDTO display, String user, AsyncCallback<DisplayDTO> async);

    void saveCopiedDisplayDTO(DisplayDTO display, AsyncCallback<DisplayDTO> async);

    void getStatusDTOMap(AsyncCallback<Map<Long, StatusDTO>> async);
    
    void getLocationCountryDTOMap(AsyncCallback<Map<Long, LocationCountryDTO>> async);
    void getLocationDTOMap(AsyncCallback<Map<Long, LocationDTO>> async);

    void getCustomerCountryDTOMap(AsyncCallback<Map<Long, CustomerCountryDTO>> async);
    void getCustomerDTOMap(AsyncCallback<Map<Long, CustomerDTO>> async);

    void getDisplayDTO(DisplayDTO displayDTO, AsyncCallback<DisplayDTO> async);

    void getCustomerMarketingDTO(Long displayId, AsyncCallback<CustomerMarketingDTO> async);

    void getPromoPeriodMapDTOs(AsyncCallback<Map<Long, PromoPeriodDTO>> async);
    void getPromoPeriodCountryMapDTOs(AsyncCallback<Map<Long, PromoPeriodCountryDTO>> async);
    
    void getLocationMapDTOs(AsyncCallback<Map<Long, LocationDTO>> async);
    void getLocationCountryMapDTOs(AsyncCallback<Map<Long, LocationCountryDTO>> async);
    
    void getManufacturerMapDTOs(AsyncCallback<Map<Long, ManufacturerDTO>> async);
    void getManufacturerCountryMapDTOs(AsyncCallback<Map<Long, ManufacturerCountryDTO>> async);

    void getDisplayLogisticsInfoDTO(Long displayId, AsyncCallback<DisplayLogisticsDTO> async);

    void saveDisplayLogisticsInfoDTO(DisplayLogisticsDTO displayLogistics, DisplayDTO display, AsyncCallback<DisplayLogisticsDTO> async);

    void saveCustomerMarketingDTO(CustomerMarketingDTO customerMarketing, DisplayDTO display, AsyncCallback<CustomerMarketingDTO> async);

    void loadLocationDTOs(AsyncCallback<List<LocationDTO>> async);
    void loadLocationCountryDTOs(AsyncCallback<List<LocationCountryDTO>> async);

    void findDisplayDcByDisplay(DisplayDTO display, AsyncCallback<Map<Long, DisplayDcDTO>> async);

    void saveDisplayDcDTOList(List<DisplayDcDTO> displayDcDTOList, AsyncCallback<Map<Long, DisplayDcDTO>> async);

    void saveDisplayShipingWaveDTOList(List<DisplayShipWaveDTO> displayShipWaveDTOList, AsyncCallback<Map<Long, DisplayShipWaveDTO>> async);

    void loadDisplayShippingWaveDTOMap(DisplayDTO display, AsyncCallback<Map<Long, DisplayShipWaveDTO>> async);

    void saveDisplayPackoutDTOList(List<DisplayPackoutDTO> list, AsyncCallback<List<DisplayPackoutDTO>> async);

    void loadDisplayPackouts(DisplayDcDTO displayDcDTO, AsyncCallback<Map<Long, DisplayPackoutDTO>> async);

    void getKitComponents(DisplayDTO display, DisplayScenarioDTO displayScenarioDTO, AsyncCallback<List<KitComponentDTO>> async);

    void getKitComponentDTO(Long kitComponentId, DisplayDTO display,DisplayScenarioDTO displayScenarioDTO, AsyncCallback<KitComponentDTO> async);

    void saveKitComponentDTO(KitComponentDTO kitComponent, DisplayDTO display, List<DisplayPlannerDTO> store, AsyncCallback<KitComponentDTO> async);
    
    void saveKitComponentDTOList(List<KitComponentDTO> list, AsyncCallback<List<KitComponentDTO>> async);
    
    void deleteKitComponent(KitComponentDTO kitComponent, AsyncCallback<Boolean> async);

    void getDisplayPlanners(KitComponentDTO kitComponent, DisplayDTO display, AsyncCallback<List<DisplayPlannerDTO>> async);

    void getDisplayPlanner(KitComponentDTO kitComponent, DisplayPlannerDTO dpDTO, AsyncCallback<DisplayPlannerDTO> async);

    void getProductDetail(String sCountryName, KitComponentDTO kitComponent, AsyncCallback<List<CostPriceDTO>> async);

    void getProductDetailForFGSKU(String sCountryName, KitComponentDTO kitComponent, AsyncCallback<List<CostPriceDTO>> async);

    void savePackoutData(DisplayDTO displayModel, List<DisplayDcAndPackoutDTO>displayDcAndPackoutList, List<DisplayShipWaveDTO>displayShippingWaveList, AsyncCallback<DisplayDTO> async);

    void getValidPlantDTOs(AsyncCallback<List<ValidPlantDTO>> async);

    void getValidPlannerDTOs(AsyncCallback<List<ValidPlannerDTO>> async);

    void getValidLocationDTOs(AsyncCallback<List<ValidLocationDTO>> async);

    void getValidBuDTOs(AsyncCallback<List<ValidBuDTO>> async);

    void getProductDetail( String sku, AsyncCallback<List<ProductDetailDTO>> async);

    void loadDisplayPackoutList(DisplayDcDTO displayDcDTO, AsyncCallback<List<DisplayPackoutDTO>> async);

    void loadDisplayDcAndPackouts(DisplayDTO display, AsyncCallback<List<DisplayDcAndPackoutDTO>> async);
    
    void saveDisplayShipingWaveDTOListTest(List<DisplayShipWaveDTO> displayShipWaveDTOList, AsyncCallback<List<DisplayShipWaveDTO>> async);

    void loadDisplayShippingWaveDTOList(DisplayDTO displayDTO, AsyncCallback<List<DisplayShipWaveDTO>> async);

    void loadSlimDisplayPackoutGrid(DisplayDTO displayDTO, AsyncCallback<List<SlimPackoutDTO>> async);

	void getUserInfo(AsyncCallback<UserDTO> callback);

	void isDuplicateSKU(String sku, AsyncCallback<BooleanDTO> callback);

	void getManufacturerDTOs(AsyncCallback<List<ManufacturerDTO>> callback);
	void getManufacturerCountryDTOs(AsyncCallback<List<ManufacturerCountryDTO>> callback);
	void getAllManufacturerCountryDTOs(AsyncCallback<List<ManufacturerCountryDTO>> callback);
	
	void getLocationDTOs(AsyncCallback<List<LocationDTO>> callback);
	void getLocationCountryDTOs(AsyncCallback<List<LocationCountryDTO>> callback);
	
	void getRYGStatusDTOs(AsyncCallback<List<StatusDTO>> callback);

    void setFilters(Map<String, BaseModelData> filters, AsyncCallback<Void> callback);

    void getServletPath(AsyncCallback<String> callback);

    void callDisplayDeleteStoredProcedure(Long id, String username, AsyncCallback<Void> callback);
    
    void deleteDisplayShipWaves(DisplayShipWaveDTO displayShipWaveDTO, AsyncCallback<Boolean> callback);

    void switchOnDisplayTypesInKitComponent(DisplayDTO model,	boolean applyKitVerSetup, boolean applyFGDisplayRef, AsyncCallback<List<KitComponentDTO>> callback);

	void getCostAnalysisKitComponentInfo(DisplayScenarioDTO displayScenarioDTO, Double customerProgramPct, AsyncCallback<List<CostAnalysisKitComponentDTO>> callback);

	void saveCostAnalysisKitComponentInfo(List<CostAnalysisKitComponentDTO> costAnalysisDTOList, Long qtyForecast, Double custProgramPct,AsyncCallback<List<CostAnalysisKitComponentDTO>> callback);

	void getDisplayCostDTOs(DisplayDTO displayDTO, AsyncCallback<List<DisplayCostDTO>> callback);

	void saveDisplayCostDTO(DisplayCostDTO displayCostModel,AsyncCallback<DisplayCostDTO> callback);
	
	void deleteCostAnalysisKitComponent(CostAnalysisKitComponentDTO costAnalysisKitComponent,AsyncCallback<Boolean> callback);

	void getDisplayCostCommentDTO(Long displayScenarioId, AsyncCallback<List<DisplayCostCommentDTO>> callback);

	void saveDisplayCostCommentDTO(DisplayCostCommentDTO displayCostCommentDTO, AsyncCallback<DisplayCostCommentDTO> callback);
	
	void getUserChangeLogDTO(Long displayId, Long displayScenarioId, String tableName, AsyncCallback<List<CostAnalysisHistoryDTO>> callback);

	void saveILSPriceException(Long kitComponentId, Date PeReCalcDate, AsyncCallback<Void> callback);

	void loadBUCostCenterSplitGroup(Double mdfPctVal,Double discountPctVal,Boolean bDefaultProgramDiscount, Long customerId,Long foreCastQtyVal, Double totalDisplaysOrdered,Double totalActualInvoiceTotCostVal,Double totalDisplayCostsVal, Double customerProgramPercentageVal, 
			List<CostAnalysisKitComponentDTO> costAnalysisKitComponentStore, AsyncCallback<List<BuCostCenterSplitDTO>> callback);

	void reloadDisplayCostImages(Long displayId, Long displayCostId, AsyncCallback<List<DisplayCostImageDTO>> callback);

	void saveCostAnalysisImage(DisplayCostImageDTO displayCostImageDTO, AsyncCallback<List<DisplayCostImageDTO>> callback);

	void deleteCostAnalysisImage(DisplayCostImageDTO displayCostImageDTO, AsyncCallback<List<DisplayCostImageDTO>> callback);

	void reCalculateILSPriceException(DisplayDTO displayDTO, Date reCalcDate, Double customerPct,DisplayScenarioDTO displayScenarioDTO, AsyncCallback<List<CostAnalysisKitComponentDTO>> callback);

	void saveDisplayScenarioDTO(DisplayScenarioDTO displayScenarioModel, AsyncCallback<DisplayScenarioDTO> callback);
	
	void copyDisplayScenarioDTO(DisplayScenarioDTO displayScenarioModel, String userName, boolean copyComments, AsyncCallback<DisplayScenarioDTO> callback);
	
	void getDisplayScenarioDTO(Long displayId, AsyncCallback<List<DisplayScenarioDTO>> callback);
	void getCountryDTOs(AsyncCallback<List<CountryDTO>> async);
	void getCountryFlagDTOs(AsyncCallback<List<CountryDTO>> async);
	void getCorrugateComponentInfo(DisplayDTO displayDTO, AsyncCallback<List<CorrugateComponentsDetailDTO>> async );
	void saveCorrugateComponentInfo(List<CorrugateComponentsDetailDTO> corrCompDTOList,DisplayDTO displayDTO, AsyncCallback<List<CorrugateComponentsDetailDTO>> async );
	void callCorrugateComponentDelete(CorrugateComponentsDetailDTO corrugateComponentsDetailDTO, AsyncCallback<Boolean> callback);
	void getCorrugateDTO(DisplayDTO displayDTO,AsyncCallback<CorrugateDTO> callback);
	void saveCorrugateDTO(CorrugateDTO corrugateDTO, AsyncCallback<CorrugateDTO> callback)  ;
	void saveCalculationSummaryDTO(CalculationSummaryDTO calculationSummaryDTO, AsyncCallback<CalculationSummaryDTO> callback)  ;
	void getInPogMapDTOs(AsyncCallback<Map<Boolean, InPogDTO>> async);
	void getInPogDTOs(AsyncCallback<List<InPogDTO>> callback);
	void getCountryMapDTOs(AsyncCallback<Map<Long, CountryDTO>> async);
	void getActiveCountryFlagMapDTOs(AsyncCallback<Map<Long, CountryDTO>> async);
	void saveManufacturerCountryDTO(ManufacturerCountryDTO manufacturerCountryDTO,	AsyncCallback<ManufacturerCountryDTO> callback);
	void deleteManufacturerCountryDTO(ManufacturerCountryDTO manufacturerCountryDTO, AsyncCallback<Void> callback);
	void getAllCustomerDTOs(AsyncCallback<List<CustomerDTO>> callback);
	void getCustomerTypeDTOs(AsyncCallback<List<CustomerTypeDTO>> callback);
	void saveCustomerDTO(CustomerDTO customerDTO, AsyncCallback<CustomerDTO> callback);
	void deleteCustomerDTO(CustomerDTO customerDTO, AsyncCallback<Void> callback);
	void getAllLocationCountryDTOs(AsyncCallback<List<LocationCountryDTO>> callback);
	void saveLocationCountryDTO(LocationCountryDTO locationCountryDTO, AsyncCallback<LocationCountryDTO> callback);
	void deleteLocationCountryDTO(LocationCountryDTO model, AsyncCallback<Void> callback);
	void getAllStructureDTOs(AsyncCallback<List<StructureDTO>> callback);
	void saveStructureDTO(StructureDTO dto,	AsyncCallback<StructureDTO> callback);
	void deleteStructureDTO(StructureDTO model, AsyncCallback<Void> callback);
	void getAllDisplaySalesRepDTOs(AsyncCallback<List<DisplaySalesRepDTO>> callback);
	void saveDisplaySalesRepDTO(DisplaySalesRepDTO dto, AsyncCallback<DisplaySalesRepDTO> callback);
	void deleteDisplaySalesRepDTO(DisplaySalesRepDTO model, AsyncCallback<Void> callback);
	void getUserTypeDTOs(AsyncCallback<List<UserTypeDTO>> callback);
	void getApproverDTOs(AsyncCallback<List<DisplaySalesRepDTO>> async);
	void loadProgramBus(Long displayId, AsyncCallback<List<ProgramBuDTO>> async);
	void saveProgramBus(List<ProgramBuDTO> programList,	AsyncCallback<List<ProgramBuDTO>> async);
	void saveProgramUploadedBus(List<ProgramBuDTO> programList,DisplayDTO displayDTO,	AsyncCallback<List<ProgramBuDTO>> async);
	void loadCstBuPrgmPcts(Long customerId, AsyncCallback<List<CstBuPrgmPctDTO>> async);
	void saveProgramUploadBusForDisplays(List<DisplayDTO> displayDTOs, AsyncCallback<Void> async);
		
	
}
