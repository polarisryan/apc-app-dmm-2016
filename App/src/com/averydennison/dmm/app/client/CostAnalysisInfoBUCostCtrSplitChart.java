package com.averydennison.dmm.app.client;

import com.extjs.gxt.charts.client.Chart;
import com.extjs.gxt.charts.client.event.ChartEvent;
import com.extjs.gxt.charts.client.event.ChartListener;
import com.extjs.gxt.charts.client.model.ChartModel;
import com.extjs.gxt.charts.client.model.Legend;
import com.extjs.gxt.charts.client.model.charts.PieChart;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.MarginData;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Oct 26, 2009
 * Time: 4:05:18 PM
 */
public class CostAnalysisInfoBUCostCtrSplitChart extends LayoutContainer {
    private ChartListener listener = new ChartListener() {

        public void chartClick(ChartEvent ce) {
            //Info.display("Chart Clicked", "You selected {0}.", "" + ce.getValue());
        }
    };

    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        ContentPanel cp = new ContentPanel();
        cp.setHeading("");
        cp.setFrame(true);
        cp.setSize(225, 200);
        cp.setLayout(new FitLayout());
        String url = "chart/open-flash-chart.swf";
        final Chart chart = new Chart(url);
        chart.setBorders(true);
        chart.setChartModel(getPieChartData());
        cp.add(chart);
        add(cp, new MarginData(10));
       // add(cp);
    }

    private ChartModel getPieChartData() {
        ChartModel cm = new ChartModel(" ",
                "font-size: 14px; font-family: Verdana; text-align: center;");
        cm.setBackgroundColour("#fffff5");
        Legend lg = new Legend(Legend.Position.RIGHT, true);
        lg.setPadding(10);
        cm.setLegend(lg);

        PieChart pie = new PieChart();
        pie.setAlpha(0.5f);
        pie.setNoLabels(true);

        pie.setColours("#ff0000", "#0000cc", "#0000ff", "#ff9900", "#ff00ff");
        pie.addSlices(new PieChart.Slice(43, "0 INDEX"));
        pie.addSlices(new PieChart.Slice((100 - 43), "1 INDEX"));
        pie.addChartListener(listener);

        cm.addChartConfig(pie);
        return cm;
    }
}