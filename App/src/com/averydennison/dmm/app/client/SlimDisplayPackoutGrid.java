package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.SlimPackoutDTO;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Dec 21, 2009
 * Time: 1:38:34 PM
 */
public class SlimDisplayPackoutGrid extends LayoutContainer {
    private ListStore<SlimPackoutDTO> store = new ListStore<SlimPackoutDTO>();
    private Grid<SlimPackoutDTO> grid;

    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        setLayout(new FlowLayout(5));
        int x = 115;
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        ColumnConfig dcColumn = new ColumnConfig();
        dcColumn.setId(SlimPackoutDTO.DC);
        dcColumn.setHeader("PackLoc");
        dcColumn.setWidth(x / 2);
        dcColumn.setFixed(true);
        dcColumn.setResizable(false);
        dcColumn.setSortable(true);
        dcColumn.setMenuDisabled(true);
        configs.add(dcColumn);

        ColumnConfig shipFromLocColumn = new ColumnConfig();
        shipFromLocColumn.setId(SlimPackoutDTO.SHIP_FROM_DCCODE);
        shipFromLocColumn.setHeader("ShipLoc");
        shipFromLocColumn.setWidth(x / 2);
        shipFromLocColumn.setFixed(true);
        shipFromLocColumn.setResizable(false);
        shipFromLocColumn.setSortable(true);
        shipFromLocColumn.setMenuDisabled(true);
        configs.add(shipFromLocColumn);
        
        ColumnConfig corDue = new ColumnConfig();
        corDue.setId(SlimPackoutDTO.CORRUGATE_DUE_DATE);
        corDue.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        corDue.setHeader("Corr Due");
        corDue.setWidth(x - 35);
        corDue.setSortable(true);
        corDue.setMenuDisabled(true);
        configs.add(corDue);
        
        ColumnConfig deliDue = new ColumnConfig();
        deliDue.setId(SlimPackoutDTO.DELIVERY_DATE);
        deliDue.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        deliDue.setHeader("Deliv Dt");
        deliDue.setWidth(x - 35);
        deliDue.setSortable(true);
        deliDue.setMenuDisabled(true);
        configs.add(deliDue);
        
        ColumnConfig prdDue = new ColumnConfig();
        prdDue.setId(SlimPackoutDTO.PRODUCT_DUE_DATE);
        prdDue.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        prdDue.setHeader("PackLocPrd Due");

        prdDue.setWidth(x - 15);
        prdDue.setSortable(true);
        prdDue.setMenuDisabled(true);
        configs.add(prdDue);

        ColumnConfig shipLocDue = new ColumnConfig();
        shipLocDue.setId(SlimPackoutDTO.SHIP_LOC_PRODUCT_DUE_DATE);
        shipLocDue.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        shipLocDue.setHeader("ShipLocPrd Due");

        shipLocDue.setWidth(x - 15);
        shipLocDue.setSortable(true);
        shipLocDue.setMenuDisabled(true);
        configs.add(shipLocDue);
        
        ColumnConfig shipFrom = new ColumnConfig();
        shipFrom.setId(SlimPackoutDTO.SHIP_FROM_DATE);
        shipFrom.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        shipFrom.setHeader("Ship Dt");

        shipFrom.setWidth(x - 35);
        shipFrom.setSortable(true);
        shipFrom.setMenuDisabled(true);
        configs.add(shipFrom);

        ColumnConfig mustArrive = new ColumnConfig();
        mustArrive.setId(SlimPackoutDTO.MUST_ARRIVE_DATE);
        mustArrive.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        mustArrive.setHeader("Arrive Dt");

        mustArrive.setWidth(x - 35);
        mustArrive.setMenuDisabled(true);
        configs.add(mustArrive);

        ColumnModel cm = new ColumnModel(configs);

        grid = new Grid<SlimPackoutDTO>(store, cm);
        grid.setBorders(true);
        //grid.setSize(240, 85);
        grid.setSize(380, 85);
        grid.setTrackMouseOver(false);
        add(grid);
    }

    public ListStore<SlimPackoutDTO> getStore() {
        return store;
    }

    public void setStore(ListStore<SlimPackoutDTO> store) {
        this.store = store;
    }

    public Grid<SlimPackoutDTO> getGrid() {
        return grid;
    }

    public void setGrid(Grid<SlimPackoutDTO> grid) {
        this.grid = grid;
    }

    public void load(DisplayDTO display) {
        if (display != null && display.getDisplayId() != null) {
            AppService.App.getInstance().loadSlimDisplayPackoutGrid(display, new Service.LoadSlimDisplayPackoutGrid(this));
        }
    }
}
