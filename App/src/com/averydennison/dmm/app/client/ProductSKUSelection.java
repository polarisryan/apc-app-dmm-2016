package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;

/**
 * User: Spart Arguello
 * Date: Oct 30, 2009
 * Time: 4:50:35 PM
 */
public class ProductSKUSelection extends LayoutContainer {

    public ProductSKUSelection() {
        setLayout(new FlowLayout(10));

        final Window window = new Window();
        window.setSize(500, 300);
        window.setPlain(true);
        window.setHeading("Product SKU Selection");
        window.setLayout(new FitLayout());
        
        DisplayDTO display = new DisplayDTO();

        FormPanel panel = new FormPanel();
        panel.setLabelAlign(FormPanel.LabelAlign.TOP);
        TextField<String> skuSearchString = new TextField<String>();
        skuSearchString.setFieldLabel("SKU Search String");
        panel.add(skuSearchString);
        panel.add(new ProductSKUSelectionResultsGrid(null,window, display, null, 0L,null));
        window.add(panel, new FitData(4));

        Button btn = new Button("Product SKU Selection");
        btn.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce) {
                window.show();
            }
        });
        add(btn);
    }
}
