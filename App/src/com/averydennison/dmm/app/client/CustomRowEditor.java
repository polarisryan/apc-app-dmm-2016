package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.RowEditor;
import com.extjs.gxt.ui.client.widget.layout.TableLayout;
import com.google.gwt.user.client.Element;

public class CustomRowEditor<M extends ModelData> extends RowEditor{
    String m_buttonId = null; 
    DisplayCostDataGroup selectionGrid;

    public void setButtonId(String id) {
	m_buttonId = id;
    }
    
    public DisplayCostDataGroup getSelectionGrid() {
		return selectionGrid;
	}

	public void setSelectionGrid(DisplayCostDataGroup selectionGrid) {
		this.selectionGrid = selectionGrid;
	}

	public CustomRowEditor(DisplayCostDataGroup selectionGrid){
		this.selectionGrid=selectionGrid;
	}
	
	@Override
    protected void onRender(Element target, int index) {
        super.onRender(target, index);
        btns.removeAll();
        btns.removeAllListeners();
	    btns.addStyleName("x-btns");
        btns.setLayout(new TableLayout(2));
	    Button saveBtn = new Button(getMessages().getSaveText(), new SelectionListener<ButtonEvent>() {
	      @Override
	      public void componentSelected(ButtonEvent ce) {
	        stopEditing(true);
	        selectionGrid.save(null);
	      }

	    });
	    if(m_buttonId != null)
		saveBtn.setId(m_buttonId + "_SAVE");
	    saveBtn.setMinWidth(getMinButtonWidth());
	    
	    btns.add(saveBtn);
	    Button cancelBtn = new Button(getMessages().getCancelText(), new SelectionListener<ButtonEvent>() {
	      @Override
	      public void componentSelected(ButtonEvent ce) {
	        stopEditing(false);
	        selectionGrid.rejectChanges();
	      }

	    });
	    if(m_buttonId != null)
		cancelBtn.setId(m_buttonId + "_CANCEL");
	    cancelBtn.setMinWidth(getMinButtonWidth());
	    
	    btns.add(cancelBtn);
	    btns.render(getElement("bwrap"));
	    btns.layout();
	  }
	
	}
