package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.google.gwt.user.client.ui.KeyboardListener;

public class XNumberField extends NumberField {
	private int decimalPrecision;
	private int wholeNumberPrecision;
	public int getWholeNumberPrecision() {
		return wholeNumberPrecision;
	}
	public void setWholeNumberPrecision(int wholeNumberPrecision) {
		this.wholeNumberPrecision = wholeNumberPrecision;
	}
	public int getDecimalPrecision() {
        return decimalPrecision;
    }
    public void setDecimalPrecision(int decimalPrecision) {
        this.decimalPrecision = decimalPrecision;
    }
    @Override
    protected void onKeyPress(FieldEvent fe) {
      super.onKeyPress(fe);
      if (!getAllowDecimals() || decimalPrecision <= 0) {
          // Nothing to do as decimals are not allowed.
    	  return;
      }
      char key = (char) fe.getKeyCode();
      String rawValue = getRawValue();
      String latestKey = String.valueOf(key);
      // Attempt to only allow 1 non-digit in the value (ie. '.' or ',').
      // Add some bogus value to the end so we are able to split the string properly if the latestKey is a non-digit.
      // Otherwise we won't get the right number of tokens.
      String splitString = rawValue + latestKey + "9";
      String[] tokens = splitString.split("\\D"); // Split the string on non-digits.
      // Attempt to see if there is already an allowed non-digit within the rawValue, as we only want to have 1.
      if (tokens.length > 2 && rawValue.indexOf(key) != -1) {
          // Stop the 2nd non-digit key from being allowed.
          fe.stopEvent();
          return;
      }
      // Allow special keys to be used.
      if (fe.isSpecialKey(fe.getKeyCode()) || fe.getKeyCode() == KeyboardListener.KEY_BACKSPACE
              || fe.getKeyCode() == KeyboardListener.KEY_DELETE || fe.isControlKey()) {
          return;
      }
      int cursorPos = getCursorPos();
      int rawValueLength = rawValue.length();
      // Preview what the value is going to look like, and determine if we should allow it.
      final StringBuilder sb = new StringBuilder(rawValue);
      sb.insert(cursorPos, latestKey); // Add latestKey in the right position.
      String previewValue = sb.toString();
      // Determine if there is a decimal and that the length of it doesn't exceed 'decimalPrecision'.
      if (previewValue.length() > 0) {
          tokens = previewValue.split("\\D"); // Split the string on non-digits.
          if (tokens.length > 1) {
              // A decimal was found.
              String decimal = tokens[1];
              if (decimal.length() > decimalPrecision) {
                  // The decimal length is longer than we want.
                  if (rawValueLength > cursorPos) { // Optional check.
                      // The inserted value is not at the end. Allow the value, but remove the last character
                      // to keep the proper length.
                      String newValue = previewValue.substring(0, previewValue.length() - 1);
                      setRawValue(newValue); // This moves the cursor to the end of the field.
                      setCursorPos(cursorPos + 1); // Move the cursor back to the next position.
                  }
                  fe.stopEvent();
              }
          }
          if (tokens.length > 0) {
          String wholeNumber = tokens[0];
          	if (wholeNumber.length() > wholeNumberPrecision) {
        	  fe.stopEvent();
          	}
          }
      }


   
    }
   
}