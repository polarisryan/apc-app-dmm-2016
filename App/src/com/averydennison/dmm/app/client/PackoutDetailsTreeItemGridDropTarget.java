package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.dnd.GridDropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.widget.grid.Grid;

/**
 * User: Spart Arguello
 * Date: Dec 15, 2009
 * Time: 3:33:14 PM
 */
public class PackoutDetailsTreeItemGridDropTarget extends GridDropTarget {
    /**
     * Creates a new drop target instance.
     *
     * @param grid the target grid
     */
    public PackoutDetailsTreeItemGridDropTarget(Grid grid) {
        super(grid);
    }

    @Override
    protected void onDragDrop(DNDEvent e) {
    }
}
