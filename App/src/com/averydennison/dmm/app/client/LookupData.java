package com.averydennison.dmm.app.client;

import java.util.List;

import com.averydennison.dmm.app.client.models.ProgramBuDTO;
import com.averydennison.dmm.app.client.models.ValidBuDTO;
import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class LookupData {

	private static ListStore<ProgramBuDTO> programBuList = new ListStore<ProgramBuDTO>();
	private static ListStore<ValidBuDTO> validBuList = new ListStore<ValidBuDTO>();
	public static void initialize(Long displayId) {
		if(programBuList.getModels().size() == 0) populateProgramBu(displayId);
		if(validBuList.getModels().size() == 0) populateValidBu();
	}
	
	public static ListStore<ValidBuDTO> getValidBuList() {
		if(validBuList.getModels().size() == 0) populateValidBu();
		return validBuList;
	}
	
	public static void populateValidBu() {
		AppService.App.getInstance().getValidBuDTOs( new AsyncCallback<List<ValidBuDTO>>() {
			@Override
			public void onFailure(Throwable caught) {
				MessageBox.alert("Failed to load Dim Year lookup data", caught.getMessage(), null);
			}
			@Override
			public void onSuccess(List<ValidBuDTO> result) {
				validBuList.removeAll();
				validBuList.add(result);
				validBuList.sort("fyear", SortDir.ASC);
			}
	    });
	}
	
	public static ListStore<ProgramBuDTO> getProgramBuList(Long displayId) {
		if(programBuList.getModels().size() == 0) populateProgramBu(displayId);
		return programBuList;
	}

	public static void populateProgramBu(Long displayId) {
		AppService.App.getInstance().loadProgramBus(displayId, new AsyncCallback<List<ProgramBuDTO>>() {
			@Override
			public void onFailure(Throwable caught) {
				MessageBox.alert("Failed to load Dim Year lookup data", caught.getMessage(), null);
			}
			
			@Override
			public void onSuccess(List<ProgramBuDTO> result) {
				programBuList.removeAll();
				programBuList.add(result);
				programBuList.sort("fyear", SortDir.ASC);
			}
	    });
	}
}
