package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.CostAnalysis;
import com.averydennison.dmm.app.client.models.CostPriceDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.table.NumberCellRenderer;
import com.google.gwt.i18n.client.NumberFormat;

/**
 * User: Spart Arguello
 * Date: Oct 27, 2009
 * Time: 10:11:39 AM
 */
public class KitComponentDetailCostPricingGrid extends LayoutContainer {
	private KitComponentDTO kitComponentModel;
	private Grid<CostPriceDTO> grid;
	private DisplayDTO displayModel;
	
	public DisplayDTO getDisplayModel() {
		return displayModel;
	}

	public void setDisplayModel(DisplayDTO displayModel) {
		this.displayModel = displayModel;
	}

	public void update(Long qty) {
		int size = grid.getStore().getModels().size();
		for (int i = 0; i < size; i++) {
			grid.getStore().getModels().get(i).updateTotal(qty);
		}
		grid.getStore().commitChanges();
		grid.getView().refresh(true);
	}
	
	public void updateCostForFGSKU( KitComponentDTO kitComponent){
		 store = new ListStore<CostPriceDTO>();
	        if (kitComponentModel.getFinishedGoodsSku() != null) {
		   	    System.out.println("KitComponentDetailCostPricingGrid: loading kit component costs for sku_id=" + kitComponentModel.getFinishedGoodsSku());
		   	 sCountryName=displayModel.getCountryByCountryId()==null?null:displayModel.getCountryByCountryId().getCountryName();
		   	 //AppService.App.getInstance().getProductDetailForFGSKU(getDisplayModel().getCountryByCountryId()==null?"":getDisplayModel().getCountryByCountryId().getCountryName(),kitComponentModel, new Service.LoadProductDetailForFGSKU(grid));
		   	AppService.App.getInstance().getProductDetailForFGSKU(sCountryName,kitComponentModel, new Service.LoadProductDetailForFGSKU(grid));
	        }
	}
	private String sCountryName;
	private ListStore<CostPriceDTO> store;
    public KitComponentDetailCostPricingGrid(DisplayDTO displayModel, KitComponentDTO kitComponent) {
    	this.kitComponentModel = kitComponent;
    	this.displayModel=displayModel;
        setLayout(new FlowLayout(0));

        int x = 100;

        final NumberFormat currency = NumberFormat.getCurrencyFormat();
        final NumberFormat number = NumberFormat.getFormat("0.00");
        final NumberCellRenderer<Grid<CostAnalysis>> numberRenderer = new NumberCellRenderer<Grid<CostAnalysis>>(
                currency);
        
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        ColumnConfig column = new ColumnConfig();
        column.setId(CostPriceDTO.PRICE_DESC);
        column.setHeader(" ");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x - 40);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(CostPriceDTO.UNIT_PRICE);
        column.setHeader("Unit");
        column.setWidth(x - 30);
        column.setAlignment(HorizontalAlignment.RIGHT);
        column.setNumberFormat(number);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(CostPriceDTO.TOTAL_PRICE);
        column.setHeader("Total");
        column.setWidth(x - 20);
        column.setAlignment(HorizontalAlignment.RIGHT);
        column.setNumberFormat(number);
        configs.add(column);


        // get data
        store = new ListStore<CostPriceDTO>();
        sCountryName=displayModel.getCountryByCountryId()==null?null:displayModel.getCountryByCountryId().getCountryName();
        if (kitComponentModel.getSkuId() != null) {
	   	//  MessageBox.alert("KitComponentDetailCostPricingGrid","KitComponentDetailCostPricingGrid:..1.. loading kit component costs for sku_id=" + kitComponentModel.getSkuId(),null);
	   	// MessageBox.alert("KitComponentDetailCostPricingGrid","KitComponentDetailCostPricingGrid:..2.. countName=" + sCountryName,null);
	   	    AppService.App.getInstance().getProductDetail(sCountryName,kitComponentModel, new Service.LoadKitComponentCost(store));	
        }
        
        ColumnModel cm = new ColumnModel(configs);

        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setHeaderVisible(false);
        cp.setLayout(new FitLayout());
        cp.setSize(215, 110);

        grid = new Grid<CostPriceDTO>(store, cm);
        grid.setColumnResize(true);
        grid.setStyleAttribute("borderTop", "none");
        grid.setStripeRows(true);
        grid.setBorders(true);
        cp.add(grid, new FitData(0));

        add(cp);
    }

}
