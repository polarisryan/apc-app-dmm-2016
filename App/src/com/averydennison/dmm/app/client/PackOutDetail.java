package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Oct 27, 2009
 * Time: 11:29:29 AM
 */
public class PackOutDetail extends LayoutContainer {
    private FormData formData;
    private VerticalPanel vp;

    public PackOutDetail() {
        formData = new FormData("95%");
        vp = new VerticalPanel();
        vp.setSpacing(10);
    }

    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        createHeader();
        createForm();
        add(vp);
    }

    private void createHeader() {
        add(new HeaderPanel());
        add(new DisplayTabHeaderV2());
    }

    private void createForm() {

        FormPanel formPanel = new FormPanel();
        formPanel.setFrame(true);
        formPanel.setSize(935, -1);
        formPanel.setLabelAlign(FormPanel.LabelAlign.TOP);
        formPanel.setButtonAlign(Style.HorizontalAlignment.CENTER);

        LayoutContainer main = new LayoutContainer();
        main.setLayout(new ColumnLayout());

        LayoutContainer left = new LayoutContainer();
        LayoutContainer center = new LayoutContainer();
        LayoutContainer right = new LayoutContainer();

        FormLayout leftLayout = new FormLayout();
        left.setStyleAttribute("paddingRight", "10px");
        leftLayout.setLabelAlign(FormPanel.LabelAlign.TOP);

        FormLayout centerLayout = new FormLayout();
        left.setStyleAttribute("paddingRight", "10px");
        centerLayout.setLabelAlign(FormPanel.LabelAlign.TOP);

        FormLayout rightLayout = new FormLayout();
        rightLayout.setLabelAlign(FormPanel.LabelAlign.TOP);

        left.setStyleAttribute("paddingRight", "10px");
        left.setLayout(leftLayout);

        center.setStyleAttribute("paddingRight", "10px");
        center.setLayout(centerLayout);

        right.setStyleAttribute("paddingLeft", "10px");
        right.setLayout(rightLayout);

        FieldSet shippingWave = new FieldSet();
        FormLayout shippingWaveFormLayout = new FormLayout();
        shippingWaveFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        shippingWave.setLayout(shippingWaveFormLayout);
        shippingWave.setHeading("shippingWave");
        shippingWave.setCollapsible(true);
        left.add(shippingWave);

        FieldSet logistics = new FieldSet();
        FormLayout bUCostCtrSplitFormLayout = new FormLayout();
        bUCostCtrSplitFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        logistics.setLayout(bUCostCtrSplitFormLayout);
        logistics.setHeading("Logistics");
        logistics.setCollapsible(true);
        left.add(logistics);

        HorizontalPanel hp = new HorizontalPanel();
        hp.setSpacing(10);
        VerticalPanel waveButtons = new VerticalPanel();
        waveButtons.setSpacing(10);

        waveButtons.add(new Button("Previous Wave"));
        waveButtons.add(new Button("Next Wave"));
        hp.add(new PackOutDetailShippingWaveGrid());
        hp.add(waveButtons);
        shippingWave.add(hp);

        VerticalPanel logisticsVerticalPanel = new VerticalPanel();
        logisticsVerticalPanel.setSpacing(10);
        logisticsVerticalPanel.add(new PackoutDetailLogisticsGrid());
        ButtonBar buttonBar = new ButtonBar();
        buttonBar.setSpacing(10);
        buttonBar.add(new FillToolItem());
        buttonBar.add(new Button("Add", listener));
        buttonBar.add(new Button("Delete", listener));
        logisticsVerticalPanel.add(buttonBar);
        logistics.add(logisticsVerticalPanel);

        main.add(left);
        //main.add(right, new ColumnData(.65));
        formPanel.add(main);

        formPanel.addButton(new Button("Return", listener));
        formPanel.addButton(new Button("Save", listener));

        vp.add(formPanel);
    }

    SelectionListener listener = new SelectionListener<ComponentEvent>() {
        public void componentSelected(ComponentEvent ce) {
            Button btn = (Button) ce.getComponent();
            //Info.display("Click Event", "The '{0}' button was clicked.", btn.getText());
        }
    };
}
