package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayForMassUpdateDTO;
import com.averydennison.dmm.app.client.models.SelectFieldToMassUpdateDTO;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * User: Mari
 * Date: Feb 3, 2010
 * Time: 4:10:00 PM
 */

public class MassUpdateGrid extends LayoutContainer {
	public static final String CANCEL = "Cancel";
	public static final String SELECT_FIELDS= "Select Fields";
	public static final String APPLY_UPDATES = "Apply Updates";
	public static final String OK ="OK";
	private Button btnCancel;
	private Button btnSelectFields;
	public Button btnApplyUpdates;
	private SelectionGrid selectionGrid;
	private Window window = new Window();
	private static final ListStore<DisplayForMassUpdateDTO> displays = new ListStore<DisplayForMassUpdateDTO>();
	private SelectFieldToMassUpdateDTO selectFieldToMassUpdate ;
	final MassUpdatePanel massUpdatePanel ;
	private EditorGrid<DisplayDTO> displayGrid;
	private CheckBox priceAvailCheckBox = new CheckBox();
	private CheckBox structureCheckBox = new CheckBox();
	private CheckBox sampleToCustomersCheckBox = new CheckBox();
	private CheckBox statusCheckBox = new CheckBox();
	private CheckBox projectManagerCheckBox = new CheckBox();
	private CheckBox finalArtworkCheckBox = new CheckBox();
	private CheckBox pimCompletedOnCheckBox = new CheckBox();
	private CheckBox corrugateMfgtCheckBox = new CheckBox();
	private CheckBox pricingAdminValidationCheckBox = new CheckBox();
	private CheckBox promoPeriodMfgtCheckBox = new CheckBox();
	private CheckBox projectConfidenceLevelCheckBox = new CheckBox();
	private CheckBox initRenderingEstimateCheckBox = new CheckBox();
	private CheckBox skuMixFinalCheckBox = new CheckBox();
	private CheckBox structuredApprovedCheckBox = new CheckBox();
	private CheckBox orderReceivedCheckBox = new CheckBox();
	private CheckBox commentsCheckBox = new CheckBox();
    private Boolean bCheckBoxClicked; 
    List<Long> parameters = new ArrayList<Long>();   
	
	 public MassUpdateGrid(EditorGrid<DisplayDTO> displayGrid, SelectionGrid selectionGrid) {
		 	this.selectionGrid=selectionGrid;
	        this.selectFieldToMassUpdate=selectionGrid.getSelectFieldToMassUpdate();
	        this.massUpdatePanel=selectionGrid.getMassUpdatePanel();
	        this.displayGrid=displayGrid;
	        setLayout(new FlowLayout(10));
	        int x = 125;
	        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
	        ColumnConfig column = new ColumnConfig();
	        column = new ColumnConfig();
	        column.setId(DisplayForMassUpdateDTO.DISPLAY_ID);
	        column.setHeader("ID");
	        
	        column.setAlignment(HorizontalAlignment.LEFT);
	        column.setWidth(x - 25);
	        configs.add(column);
	        
	        column = new ColumnConfig();
	        column.setId(DisplayForMassUpdateDTO.CUSTOMER_NAME);
	        column.setHeader("Customer");

	        column.setAlignment(HorizontalAlignment.LEFT);
	        column.setWidth(x - 25);
	        configs.add(column);

	        column = new ColumnConfig();
	        column.setId(DisplayForMassUpdateDTO.SKU);
	        column.setHeader("SKU");

	        column.setAlignment(HorizontalAlignment.LEFT);
	        column.setWidth(x-25);
	        configs.add(column);

	        column = new ColumnConfig();
	        column.setId(DisplayForMassUpdateDTO.DESCRIPTION);
	        column.setHeader("Description");
	        column.setAlignment(HorizontalAlignment.LEFT);
	        column.setWidth(x*2);
	        configs.add(column);
	       
	       //1. Customer
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbCustomer()){
	            column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.CUSTOMER_NAME);
		        column.setHeader("Customer");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //2. Structure
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbStructure()){
		        column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.STRUCTURE_NAME);
		        column.setHeader("Structure");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //3. Project Manager
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbProjectManager()){
		        column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.FULL_NAME);
		        column.setHeader("Project Manager");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //4. Status
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbStatus()){
		        column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.STATUS_NAME);
		        column.setHeader("Status");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //5. Corrugate Mfgt.
	        if(selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbCorrugateMfgt()){
	            column = new ColumnConfig();
	            column.setId(DisplayForMassUpdateDTO.CORRUGATE_NAME);//To be changed
	            column.setHeader("Corrugate Mfgt");
	            column.setAlignment(HorizontalAlignment.LEFT);
	            column.setWidth(x);
		        configs.add(column);
	        }
	        //6. Promo Period
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbPromoPeriod()){
		        column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.PROMO_PERIOD_NAME);
		        column.setHeader("Promo Period");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //7. SKU Mix Final
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbSkuMixFinal()){
		        column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.SKU_MIX_FINAL_FLG);
		        column.setHeader("SKU Mix Final");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //8. Price Avail Complete
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbPriceAvailCompleteOnDt()){
		        column = new ColumnConfig();
		        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
		        column.setId(DisplayForMassUpdateDTO.PRICE_AVAILABILITY_DATE);
		        column.setHeader("Price Availability Date");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //9. Sample To Customer By
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbSampleToCustomerBy()){
		        column = new ColumnConfig();
		        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
		        column.setId(DisplayForMassUpdateDTO.SENT_SAMPLE_DATE);
		        column.setHeader("Sample To Customer By");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	       //10. Final Artwork PO Due Date
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbFinalArtworkPODueDt()){
	            column = new ColumnConfig();
	            column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
		        column.setId(DisplayForMassUpdateDTO.FINAL_ARTWORK_DATE);
		        column.setHeader("Final Artwork PO Due Date");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //11. PIM Completed Date
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbPIMCompletedonDt()){
		        column = new ColumnConfig();
		        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
		        column.setId(DisplayForMassUpdateDTO.PIM_COMPLETED_DATE);
		        column.setHeader("Pim Completed Date");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //12. Pricing Admin Date 
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbPricingAdminValidationDt()){
		        column = new ColumnConfig();
		        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
		        column.setId(DisplayForMassUpdateDTO.PRICING_ADMIN_DATE);
		        column.setHeader("Pricing Admin Date");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //13. Project Confidence Level
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbProjectConfidenceLevel()){
		        column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.PROJECT_CONFIDENCE_LEVEL);
		        column.setHeader("Project Confidence Level");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	      //14. Initial Rendering
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbInitRenderingEstQuote()){
		        column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.INITIAL_RENDERING);
		        column.setHeader("Initial Rendering");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //15. Structure Approved
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbStructApproved()){
		        column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.STRUCTURE_APPROVED);
		        column.setHeader("Structure Approved");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //16. Order Received
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbOrdersReceived()){
		        column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.ORDER_IN);
		        column.setHeader("Order Received");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        //17. Comments
	        if (selectFieldToMassUpdate!=null && selectFieldToMassUpdate.isbComments()){
	            column = new ColumnConfig();
		        column.setId(DisplayForMassUpdateDTO.COMMENTS);
		        column.setHeader("Comments");
		        column.setAlignment(HorizontalAlignment.LEFT);
		        column.setWidth(x);
		        configs.add(column);
	        }
	        ColumnModel cm = new ColumnModel(configs);
	        List<DisplayDTO> displayDTOList= displayGrid.getSelectionModel().getSelectedItems();
	        if(displayDTOList!=null && displayDTOList.size()>0){
	        	for(DisplayDTO displayDTO:displayDTOList){
	        		parameters.add(displayDTO.getDisplayId());
	        	}
	        }
	        loadSelectedDisplays(parameters);
	        ContentPanel cp = new ContentPanel();
	        cp.setHeading("DPS - Mass Update Screen");
	        cp.setHeaderVisible(false);
	        cp.setBodyBorder(false);
	        cp.setButtonAlign(HorizontalAlignment.CENTER);
	        cp.setFrame(true);
	        cp.setLayout(new FitLayout());
	        int colAddlCnt=(selectFieldToMassUpdate.getNoOfFieldsForMassUpdate()-4);
   	     	if(selectFieldToMassUpdate.getNoOfFieldsForMassUpdate()<=4){
   	     		cp.setSize(935, 350);
   	     	}else if(selectFieldToMassUpdate.getNoOfFieldsForMassUpdate()>4){
   	     		cp.setSize(935 + (colAddlCnt * x), 350);
   	     	}
	        final Grid<DisplayForMassUpdateDTO> grid = new Grid<DisplayForMassUpdateDTO>(displays, cm);
	        grid.setStyleAttribute("borderTop", "none");
  	        grid.setStripeRows(true);
  	        grid.setBorders(true);
  	        grid.setHideHeaders(true);
  	        cp.add(this.massUpdatePanel);
	        cp.add(grid);
	        SelectionListener listener = new SelectionListener<ComponentEvent>() {
	            public void componentSelected(ComponentEvent ce) {
	            	Button button = (Button) ce.getComponent();
	                if (button.getText().equals(CANCEL)) {
	                	if(selectFieldToMassUpdate.getNoOfFieldsForMassUpdate()>4){
	                		 String msg = "This will cancel your mass update and return you to the selection screen. \n" +
                     		"Are you sure you would like to proceed?";                             
	                		 MessageBox box = new MessageBox();
	                		 box.setButtons(MessageBox.YESNO);
	                		 box.setIcon(MessageBox.QUESTION);
	                		 box.setTitle("Cancel?");
	                		 box.addCallback(new Listener<MessageBoxEvent>() {
	                			 public void handleEvent(MessageBoxEvent mbe) {
	                				 Button btn = mbe.getButtonClicked();
	                				 if (btn.getText().equals("Yes")) {
	                					 forwardToDisplayProjectSelection();  
	                				 }
	                			 }
	                		 });
	                		 box.setMessage(msg);
	                         box.show();
	                	}else{
	                		 forwardToDisplayProjectSelection();  
	                	}
	                } else if (button.getText().equals(SELECT_FIELDS)) {
	                    window.show();
	                } else if (button.getText().equals(APPLY_UPDATES)) {
                        String msg = "This process will update all selected displays \n" +
                        		"with the fields/values you have chosen. \n" +
                        		"Are you sure you would like to proceed?";                             
                        MessageBox box = new MessageBox();
                        box.setButtons(MessageBox.YESNO);
                        box.setIcon(MessageBox.QUESTION);
                        box.setTitle("Apply Masss Update?");
                        box.addCallback(new Listener<MessageBoxEvent>() {
                            public void handleEvent(MessageBoxEvent mbe) {
                                Button btn = mbe.getButtonClicked();
                                if (btn.getText().equals("Yes")) {
                                	final MessageBox box = MessageBox.wait("Progress",
                                             "The mass update process is in progress. Please wait...", "Mass updating...");
                                 	DisplayForMassUpdateDTO displayMassUpdateDTO = new DisplayForMassUpdateDTO();
                                	if(selectFieldToMassUpdate.isbStructure() && massUpdatePanel.getSelectedStructure()!=null)
                	        			displayMassUpdateDTO.setStructureId(massUpdatePanel.getSelectedStructure().getStructureId());
                	        		if(selectFieldToMassUpdate.isbProjectManager() && massUpdatePanel.getSelectedProjectManager()!=null)
                	        			displayMassUpdateDTO.setDisplaySalesRepId(massUpdatePanel.getSelectedProjectManager().getDisplaySalesRepId());
                	        		if(selectFieldToMassUpdate.isbStatus() && massUpdatePanel.getSelectedStatus()!=null)
                	        			displayMassUpdateDTO.setStatusId(massUpdatePanel.getSelectedStatus().getStatusId());
                	        		if(selectFieldToMassUpdate.isbCorrugateMfgt() && massUpdatePanel.getSelectedCorrugate()!=null)
                	        			displayMassUpdateDTO.setCorrugateId(massUpdatePanel.getSelectedCorrugate().getManufacturerId());
                	        		if(selectFieldToMassUpdate.isbPromoPeriod() && massUpdatePanel.getSelectedPromoPeriod()!=null)
                	        			displayMassUpdateDTO.setPromoPeriodId(massUpdatePanel.getSelectedPromoPeriod().getPromoPeriodId());
                	        		if(selectFieldToMassUpdate.isbSkuMixFinal())
                	        			displayMassUpdateDTO.setSkuMixFinalFlg(massUpdatePanel.isSelectedSkuMixFinal()?"Y":"N");
                	        		if(selectFieldToMassUpdate.isbPriceAvailCompleteOnDt() && massUpdatePanel.getSelectedPriceAvailCompleteOnDt()!=null)
                	        			displayMassUpdateDTO.setPriceAvailabilityDate(massUpdatePanel.getSelectedPriceAvailCompleteOnDt());
                	        		if(selectFieldToMassUpdate.isbSampleToCustomerBy() && massUpdatePanel.getSelectedSampleToCustomerBy()!=null)
                	        			displayMassUpdateDTO.setSentSampleDate(massUpdatePanel.getSelectedSampleToCustomerBy());
                	        		if(selectFieldToMassUpdate.isbFinalArtworkPODueDt() && massUpdatePanel.getSelectedFinalArtworkPODueDt()!=null)
                	        			displayMassUpdateDTO.setFinalArtworkDate(massUpdatePanel.getSelectedFinalArtworkPODueDt());
                	        		if(selectFieldToMassUpdate.isbPIMCompletedonDt() && massUpdatePanel.getSelectedPIMCompletedonDt()!=null)
                	        			displayMassUpdateDTO.setPimCompletedDate(massUpdatePanel.getSelectedPIMCompletedonDt());
                	        		if(selectFieldToMassUpdate.isbPricingAdminValidationDt() && massUpdatePanel.getSelectedPricingAdminValidationDt()!=null)
                	        			displayMassUpdateDTO.setPricingAdminDate(massUpdatePanel.getSelectedPricingAdminValidationDt());
                	        		if(selectFieldToMassUpdate.isbProjectConfidenceLevel() &&  massUpdatePanel.getSelectedProjectConfidenceLevel()!=null)
                	        			displayMassUpdateDTO.setProjectLevel(massUpdatePanel.getSelectedProjectConfidenceLevel());
                	        		if(selectFieldToMassUpdate.isbInitRenderingEstQuote())
                	        			displayMassUpdateDTO.setIntialRendering(massUpdatePanel.isSelectedInitialRendering()?"Y":"N");
                	        		if(selectFieldToMassUpdate.isbStructApproved())
                	        			displayMassUpdateDTO.setStructureApproved(massUpdatePanel.isSelectedStructureApproved()?"Y":"N");
                	        		if(selectFieldToMassUpdate.isbOrdersReceived())
                	        			displayMassUpdateDTO.setOrderIn(massUpdatePanel.isSelectedOrdersReceived()?"Y":"N");
                	        		if(selectFieldToMassUpdate.isbComments())
                	        			displayMassUpdateDTO.setComments(massUpdatePanel.getSelectedComments()==null?"":massUpdatePanel.getSelectedComments());
                	        		AppService.App.getInstance().massUpdateSelectedItems(App.getUser().getUsername(),parameters,displayMassUpdateDTO,  new Service.SaveMassUpdateRecords(displays,  btnApplyUpdates, box));
                                }
                                if (btn.getText().equals("No")) {
                                }
                                if (btn.getText().equals("Cancel")) {                                            
                                }
                            }});
                        box.setMessage(msg);
                        box.show();
                }
	            }
	        };
	        setupSelectionDialog(displayGrid,  selectionGrid);
	        ButtonBar buttonBar = new ButtonBar();
	        buttonBar.setSize(850, -1);
	        buttonBar.setSpacing(10);
	        btnCancel = new Button(CANCEL, listener);
	        btnSelectFields = new Button(SELECT_FIELDS, listener);
	        btnApplyUpdates = new Button(APPLY_UPDATES, listener);
	        btnApplyUpdates.disable();
	        buttonBar.add(new FillToolItem());
	        buttonBar.add(btnCancel);
	        buttonBar.add(btnSelectFields);
	        buttonBar.add(btnApplyUpdates);
	        setPermissions();
	        add(this.massUpdatePanel);
	        add(cp);
	        add(buttonBar);
	    }
	 
	    private MassUpdateGrid getMassUpdateGrid(){
	        return this;
	    }
	    
	    private SelectionGrid getSelectionGrid(){
	        return this.selectionGrid;
	    }
	    
	    
	    private SelectFieldToMassUpdateDTO getSelectFieldToMassUpdateDTO(){
	        return this.selectFieldToMassUpdate;
	    }
	    
	    public EditorGrid<DisplayDTO> getDisplayGrid(){
	        return this.displayGrid;
	    }
	    
	    public MassUpdatePanel getMassUpdatePanel() {
			return massUpdatePanel;
		}

	    public Button getBtnApplyUpdates() {
			return btnApplyUpdates;
		}

		public void setBtnApplyUpdates(Button btnApplyUpdates) {
			this.btnApplyUpdates = btnApplyUpdates;
		}

		private void setPermissions(){
	    	//if(!App.isAuthorized("MassUpdateGrid.CANCEL")) btnCancel.disable();
	    	//if(!App.isAuthorized("MassUpdateGrid.SELECT_FIELDS")) btnSelectFields.disable();
	    	//if(!App.isAuthorized("MassUpdateGrid.APPLY_UPDATES")) btnApplyUpdates.disable();
	    }
	    
	    public void loadSelectedDisplays(List<Long> parameters){
	        MessageBox box = MessageBox.wait("Progress", "loading selected projects for mass update, please wait...", "Loading...");
	        AppService.App.getInstance().getSelectedDisplaysForMassUpdate(parameters, new Service.LoadSelectedDisplaysForMassUpdate(displays, box));
	    }
	    private void forwardToDisplayProjectSelection(){
	        App.getVp().clear();
	        App.getVp().add(new DisplayProjectSelection());
	    }
	    
	    private void setupSelectionDialog(EditorGrid<DisplayDTO> displayGrid, SelectionGrid selectionGrid) {
	        window.setSize(550, 475);
	        window.setPlain(true);
	        window.setResizable(false);
	        window.setHeading("Select Fields to Update");
	        window.setLayout(new FitLayout());
	        VerticalPanel vp1 = new VerticalPanel();
	        VerticalPanel vp2 = new VerticalPanel();
	        HorizontalPanel hp1 = new HorizontalPanel();
	        HorizontalPanel hp2 = new HorizontalPanel();
	        HorizontalPanel hp3 = new HorizontalPanel();
	        hp1.setSpacing(2);
	        hp2.setSpacing(2);
	        hp3.setSpacing(2);
	        vp1.setSpacing(5);
	        vp2.setSpacing(5);
	        ContentPanel panel = new ContentPanel();
	        panel.setHeaderVisible(false);
	        panel.setLayout(new ColumnLayout());
	        panel.setSize(550, -1);
	        panel.setFrame(true);
	        panel.setCollapsible(true);
	        panel.setButtonAlign(HorizontalAlignment.CENTER);
	        LabelField genralInfoTabLabel = new LabelField("Display General Info");
	        genralInfoTabLabel.setFieldLabel("genralInfoTabLabel");
	        genralInfoTabLabel.setWidth(250);
	        hp1.add(genralInfoTabLabel);
	        vp1.add(hp1);
	        LabelField customerMarketingLabel = new LabelField("Customer Marketing Info");
	        customerMarketingLabel.setFieldLabel("customerMarketingLabel");
	        customerMarketingLabel.setWidth(250);
	        hp1.add(customerMarketingLabel);
	        vp2.add(hp1);
	        //1. Customer, 2.Price Avail Complete On Dt.
	        structureCheckBox.setBoxLabel("Structure");
	        structureCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbStructure());
	        structureCheckBox.setWidth(250);
	        structureCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = structureCheckBox.getValue();
	                selectFieldToMassUpdate.setbStructure(bCheckBoxClicked);
	            }
	        });
	        hp2.add(structureCheckBox);
	        vp1.add(hp2);
	        
	        priceAvailCheckBox.setBoxLabel("Price Avail Complete On Dt.");
	        priceAvailCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbPriceAvailCompleteOnDt());
	        priceAvailCheckBox.setWidth(250);
	        priceAvailCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = priceAvailCheckBox.getValue();
	                selectFieldToMassUpdate.setbPriceAvailCompleteOnDt(bCheckBoxClicked);
	            }
	        });
	        hp2.add(priceAvailCheckBox);
	        vp2.add(hp2);
	        //3. Structure, 4. Sample To Customer By
	        projectManagerCheckBox.setBoxLabel("Project Manager");
	        projectManagerCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbProjectManager());
	        projectManagerCheckBox.setWidth(250);
	        projectManagerCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = projectManagerCheckBox.getValue();
	                selectFieldToMassUpdate.setbProjectManager(bCheckBoxClicked);
	            }
	        });
	        /*hp4.add(projectManagerCheckBox);
	        vp1.add(hp4);*/
	        hp3.add(projectManagerCheckBox);
	        vp1.add(hp3);
	        
	        sampleToCustomersCheckBox.setBoxLabel("Sample To Customer By");
	        sampleToCustomersCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbSampleToCustomerBy());
	        sampleToCustomersCheckBox.setWidth(250);
	        sampleToCustomersCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = sampleToCustomersCheckBox.getValue();
	                selectFieldToMassUpdate.setbSampleToCustomerBy(bCheckBoxClicked);
	            }
	        });
	        hp3.add(sampleToCustomersCheckBox);
	        vp2.add(hp3);
	        
	        //5. Project Manager, 6. Final Artwork & PO Due Dt.
	        HorizontalPanel hp4 = new HorizontalPanel();
	        hp4.setSpacing(2);
	        statusCheckBox.setBoxLabel("Status");
	        statusCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbStatus());
	        statusCheckBox.setWidth(250);
	        statusCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = statusCheckBox.getValue();
	                selectFieldToMassUpdate.setbStatus(bCheckBoxClicked) ;
	            }
	        });
	       /* hp5.add(statusCheckBox);
	        vp1.add(hp5);*/
	        hp4.add(statusCheckBox);
	        vp1.add(hp4);
	        
	        finalArtworkCheckBox.setBoxLabel("Final Artwork & PO Due Dt.");
	        finalArtworkCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbFinalArtworkPODueDt());
	        finalArtworkCheckBox.setWidth(250);
	        finalArtworkCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = finalArtworkCheckBox.getValue();
	                selectFieldToMassUpdate.setbFinalArtworkPODueDt(bCheckBoxClicked) ;
	            }
	        });
	        hp4.add(finalArtworkCheckBox);
	        vp2.add(hp4);
	        
	        //7.Status, 8. PIM Completed on Dt
	        HorizontalPanel hp5 = new HorizontalPanel();
	        hp5.setSpacing(2);
	        
	        corrugateMfgtCheckBox.setBoxLabel("Corrugate Mfg");
	        corrugateMfgtCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbCorrugateMfgt());
	        corrugateMfgtCheckBox.setWidth(250);
	        corrugateMfgtCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = corrugateMfgtCheckBox.getValue();
	                selectFieldToMassUpdate.setbCorrugateMfgt(bCheckBoxClicked);  ;
	            }
	        });
	        /*hp6.add(corrugateMfgtCheckBox);
	        vp1.add(hp6);*/
	        hp5.add(corrugateMfgtCheckBox);
	        vp1.add(hp5);
	        
	        pimCompletedOnCheckBox.setBoxLabel("PIM Completed on Dt.");
	        pimCompletedOnCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbPIMCompletedonDt());
	        pimCompletedOnCheckBox.setWidth(250);
	        pimCompletedOnCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = pimCompletedOnCheckBox.getValue();
	                selectFieldToMassUpdate.setbPIMCompletedonDt(bCheckBoxClicked) ;
	            }
	        });
	        hp5.add(pimCompletedOnCheckBox);
	        vp2.add(hp5);
	        
	        //9. Corrugate Mfgt, 10. Pricing Admin Validation Dt
	        HorizontalPanel hp6 = new HorizontalPanel();
	        hp6.setSpacing(2);
	        
	        promoPeriodMfgtCheckBox.setBoxLabel("Promo Period");
	        promoPeriodMfgtCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbPromoPeriod());
	        promoPeriodMfgtCheckBox.setWidth(250);
	        promoPeriodMfgtCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = promoPeriodMfgtCheckBox.getValue();
	                selectFieldToMassUpdate.setbPromoPeriod(bCheckBoxClicked) ;
	            }
	        });
	        /*hp7.add(promoPeriodMfgtCheckBox);
	        vp1.add(hp7);*/
	        hp6.add(promoPeriodMfgtCheckBox);
	        vp1.add(hp6);
	        
	        pricingAdminValidationCheckBox.setBoxLabel("Pricing Admin Validation Dt.");
	        pricingAdminValidationCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbPricingAdminValidationDt());
	        pricingAdminValidationCheckBox.setWidth(250);
	        pricingAdminValidationCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = pricingAdminValidationCheckBox.getValue();
	                selectFieldToMassUpdate.setbPricingAdminValidationDt(bCheckBoxClicked) ;
	            }
	        });
	        hp6.add(pricingAdminValidationCheckBox);
	        vp2.add(hp6);
	        HorizontalPanel hp7 = new HorizontalPanel();

	        //11. Promo Period, 12. Project Confidence Level
	        hp7.setSpacing(2);
	        skuMixFinalCheckBox.setBoxLabel("SKU Mix Final");
	        skuMixFinalCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbSkuMixFinal());
	        skuMixFinalCheckBox.setWidth(250);
	        skuMixFinalCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked= skuMixFinalCheckBox.getValue();
	                selectFieldToMassUpdate.setbSkuMixFinal(bCheckBoxClicked) ;
	            }
	        });
	        /*hp8.add(skuMixFinalCheckBox);
	        vp1.add(hp8);*/
	        hp7.add(skuMixFinalCheckBox);
	        vp1.add(hp7);
	        
	        projectConfidenceLevelCheckBox.setBoxLabel("Project Confidence Level");
	        projectConfidenceLevelCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbProjectConfidenceLevel());
	        projectConfidenceLevelCheckBox.setWidth(250);
	        projectConfidenceLevelCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = projectConfidenceLevelCheckBox.getValue();
	                selectFieldToMassUpdate.setbProjectConfidenceLevel(bCheckBoxClicked) ;
	            }
	        });
	        hp7.add(projectConfidenceLevelCheckBox);
	        vp2.add(hp7);
	        HorizontalPanel hp8 = new HorizontalPanel();

	        //13. SKU Mix Final, 14. Init Rendering & Est Quote (Y/N)
	        hp8.setSpacing(2);
	        LabelField emptyLabel = new LabelField("");
	        emptyLabel.setHideLabel(true);
	        emptyLabel.setWidth(252);
	        hp8.add(emptyLabel);
	        vp1.add(hp8);
	        
	        initRenderingEstimateCheckBox.setBoxLabel("Init Rendering & Est Quote (Y/N)");
	        initRenderingEstimateCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbInitRenderingEstQuote());
	        initRenderingEstimateCheckBox.setWidth(250);
	        initRenderingEstimateCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked= initRenderingEstimateCheckBox.getValue();
	                selectFieldToMassUpdate.setbInitRenderingEstQuote(bCheckBoxClicked) ;
	            }
	        });
	        hp8.add(initRenderingEstimateCheckBox);
	        vp2.add(hp8);
	        HorizontalPanel hp9 = new HorizontalPanel();
	        //				,15 Struct Approved (Y/N)
	        hp9.setSpacing(2);
	        emptyLabel = new LabelField("");
	        emptyLabel.setHideLabel(true);
	        emptyLabel.setWidth(252);
	        hp9.add(emptyLabel);
	        vp1.add(hp9);
	        
	        structuredApprovedCheckBox.setBoxLabel("Struct Approved (Y/N)");
	        structuredApprovedCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbStructApproved());
	        structuredApprovedCheckBox.setWidth(250);
	        structuredApprovedCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = structuredApprovedCheckBox.getValue();
	                selectFieldToMassUpdate.setbStructApproved(bCheckBoxClicked);
	            }
	        });
	        hp9.add(structuredApprovedCheckBox);
	        vp2.add(hp9);

	        HorizontalPanel hp10 = new HorizontalPanel();
	        //				,16. Orders Received (Y/N)
	        hp10.setSpacing(2);
	       
	        emptyLabel = new LabelField("");
	        emptyLabel.setHideLabel(true);
	        emptyLabel.setWidth(252);
	        hp10.add(emptyLabel);
	        vp1.add(hp10);
	        orderReceivedCheckBox.setBoxLabel("Orders Received (Y/N)");
	        orderReceivedCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbOrdersReceived());
	        orderReceivedCheckBox.setWidth(250);
	        orderReceivedCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = orderReceivedCheckBox.getValue();
	                selectFieldToMassUpdate.setbOrdersReceived(bCheckBoxClicked) ;
	            }
	        });
	        hp10.add(orderReceivedCheckBox);
	        vp2.add(hp10);
	        
	        HorizontalPanel hp11 = new HorizontalPanel();
	        //				,17. Comments
	        hp11.setSpacing(2);
	        emptyLabel = new LabelField("");
	        emptyLabel.setHideLabel(true);
	        emptyLabel.setWidth(252);
	        hp11.add(emptyLabel);
	        vp1.add(hp11);
	        commentsCheckBox.setBoxLabel("Comments");
	        commentsCheckBox.setValue(selectionGrid.getSelectFieldToMassUpdate().isbComments());
	        commentsCheckBox.setWidth(250);
	        commentsCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	            	bCheckBoxClicked = commentsCheckBox.getValue();
	                selectFieldToMassUpdate.setbComments(bCheckBoxClicked); ;
	            }
	        });
	        hp11.add(commentsCheckBox);
	        vp2.add(hp11);
	        ButtonBar buttonBar = new ButtonBar();
	        buttonBar.setSize(850, -1);
	        buttonBar.setSpacing(2);
	        SelectionListener listn = new SelectionListener<ComponentEvent>() {
	            public void componentSelected(ComponentEvent ce) {
	                Button button = (Button) ce.getComponent();
	                if (button.getText().equals(OK)) {
	                	selectFieldToMassUpdate.countNoOfFieldsForMassUpdate(selectFieldToMassUpdate);
	                	getMassUpdateGrid().getSelectionGrid().setSelectFieldToMassUpdate(selectFieldToMassUpdate);
	                	getMassUpdateGrid().getSelectionGrid().setMassUpdatePanel(massUpdatePanel);
	                	 App.getVp().clear();
                		 App.getVp().add(new MassUpdateSelection(getDisplayGrid(),getSelectionGrid())); 
                		 window.close();
	                }
	            }
	        };
	        HorizontalPanel hp12 = new HorizontalPanel();
	        hp12.setSpacing(2);
	        emptyLabel = new LabelField("");
	        emptyLabel.setHideLabel(true);
	        emptyLabel.setWidth(255);
	        hp12.add(emptyLabel);
	        vp1.add(hp12);
	        Button btnOK = new Button(OK, listn);
	        btnOK.setWidth(80);
	        panel.add(vp1, new ColumnData(300));
	        panel.add(vp2, new ColumnData(300));
	        panel.addButton(btnOK);
	        window.add(panel, new FitData(2));
	    }
}
