package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.averydennison.dmm.app.client.models.CorrugateComponent;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * User: Spart Arguello
 * Date: Oct 26, 2009
 * Time: 11:25:43 AM
 */
public class CorrugateComponentInfoGrid extends LayoutContainer {
    public CorrugateComponentInfoGrid(DisplayDTO displayDTO) {
        setLayout(new FlowLayout(10));
        int x = 120;
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        ColumnConfig column = new ColumnConfig();
        column.setId("vendorPartNumber");
        column.setHeader("Vendor Part #");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("gloviaPartNumber");
        column.setHeader("Glovia Part #");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("description");
        column.setHeader("Description");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("qtyPerDisp");
        column.setHeader("Qty Per Disp");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("totalQty");
        column.setHeader("Total Qty");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("deliveryDate");
        column.setHeader("Delivery Date");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("unitCost");
        column.setHeader("Unit Cost");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("extCost");
        column.setHeader("Ext Cost");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId("ttlCost");
        column.setHeader("Ttl Cost");
        column.setAlignment(Style.HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        //TODO call server
        ListStore<CorrugateComponent> store = new ListStore<CorrugateComponent>();
        store.add(CorrugateComponentInfoGrid.getCorrugateComponents());

        ColumnModel cm = new ColumnModel(configs);

        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setHeaderVisible(false);
        cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
        cp.setLayout(new FitLayout());
        //cp.setSize(935, 300);
        cp.setSize(1190,300);
        

        Grid<CorrugateComponent> grid = new Grid<CorrugateComponent>(store, cm);
        grid.setColumnResize(true);
        grid.setStyleAttribute("borderTop", "none");
        grid.setStripeRows(true);
        //grid.setAutoExpandColumn("customer");
        grid.setBorders(true);
        cp.add(grid);

        add(cp);
    }

    public static List<CorrugateComponent> getCorrugateComponents() {
        List<CorrugateComponent> list = new ArrayList<CorrugateComponent>();
        for (int i = 0; i < 25; i++) {
            list.add(new CorrugateComponent("IP", "FF SPEC#6877487", "181097-FM", "Shipper", 9, 13050, new Date(),
                    1.00, 9.00, 117450.00));
        }
        return list;
    }
}
