package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayForMassUpdateDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.SelectFieldToMassUpdateDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.GridView;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Element;

/**
 * User: Mari
 * Date: Feb 3, 2010
 * Time: 4:10:00 PM
 */

public class MassUpdate extends LayoutContainer {
	public static final String CANCEL = "Cancel";
	public static final String SELECT_FIELDS= "Select Fields";
	public static final String APPLY_UPDATES = "Apply Updates";

    private static final ListStore<DisplayForMassUpdateDTO> dataRow = new ListStore<DisplayForMassUpdateDTO>(); 
	private static final ListStore<DisplayForMassUpdateDTO> displays = new ListStore<DisplayForMassUpdateDTO>();

	ListStore<StatusDTO> statuses = new ListStore<StatusDTO>();
    ListStore<StructureDTO> structures = new ListStore<StructureDTO>();
    ListStore<PromoPeriodCountryDTO> promoPeriods = new ListStore<PromoPeriodCountryDTO>();
    ListStore<ManufacturerCountryDTO> corrugates = new ListStore<ManufacturerCountryDTO>();
    ListStore<DisplaySalesRepDTO> projectManagers = new ListStore<DisplaySalesRepDTO>();

	private SelectFieldToMassUpdateDTO selectFieldToMassUpdate ;
	private DisplayForMassUpdateDTO massUpdateData;

	private EditorGrid<DisplayForMassUpdateDTO> dataGrid;
	private Grid<DisplayForMassUpdateDTO> displaysGrid;
	private GridView dataGridView;
	private int lastDataCol = 0;
	private int lastHpos = 0;
	private ContentPanel cp;

	private List<Long> displayIdList = new ArrayList<Long>();   
    
    // selection dialog
    private MassUpdateFieldSelection massUpdateFieldSelection;

    
	public MassUpdate(List<Long> displayIdList) {
		this.selectFieldToMassUpdate=  new SelectFieldToMassUpdateDTO();
		this.massUpdateData=new DisplayForMassUpdateDTO();
        this.displayIdList = displayIdList;
        setLayout(new FlowLayout(10));
        setMassUpdateData();
	}
	
	// button listener for page
	private SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
        public void componentSelected(ButtonEvent ce) {
        	Button button = (Button) ce.getComponent();
            if (button.getText().equals(CANCEL)) {
            	if (massUpdateFieldSelection.selectedFieldCount() > 0) {
            		 String msg = "This will cancel your mass update and return you to the selection screen. \n" +
             		"Are you sure you would like to proceed?";                             
            		 MessageBox box = new MessageBox();
            		 box.setButtons(MessageBox.YESNO);
            		 box.setIcon(MessageBox.QUESTION);
            		 box.setTitle("Cancel?");
            		 box.addCallback(new Listener<MessageBoxEvent>() {
            			 public void handleEvent(MessageBoxEvent mbe) {
            				 Button btn = mbe.getButtonClicked();
            				 if (btn.getText().equals("Yes")) {
            					 forwardToDisplayProjectSelection();  
            				 }
            			 }
            		 });
            		 box.setMessage(msg);
                     box.show();
            	} else {
            		 forwardToDisplayProjectSelection();  
            	}
            } else if (button.getText().equals(SELECT_FIELDS)) {
				if (massUpdateFieldSelection == null) massUpdateFieldSelection = new MassUpdateFieldSelection(dataGrid, displaysGrid, selectFieldToMassUpdate);
				massUpdateFieldSelection.show();
				
            } else if (button.getText().equals(APPLY_UPDATES)) {
            	selectFieldToMassUpdate = massUpdateFieldSelection.getSelectFieldToMassUpdateDTO();
            	massUpdateData = getMassUpdateData();
            	String msg = "This process will update all selected displays \n" +
                		"with the fields/values you have chosen. \n" +
                		"Are you sure you would like to proceed?";                             
                MessageBox box = new MessageBox();
                box.setButtons(MessageBox.YESNO);
                box.setIcon(MessageBox.QUESTION);
                box.setTitle("Apply Masss Update?");
                box.addCallback(new Listener<MessageBoxEvent>() {
                    public void handleEvent(MessageBoxEvent mbe) {
                        Button btn = mbe.getButtonClicked();
                        if (btn.getText().equals("Yes")) {
                        	final MessageBox box = MessageBox.wait("Progress",
                                     "The mass update process is in progress. Please wait...", "Mass updating...");
                         	DisplayForMassUpdateDTO displayMassUpdateDTO = new DisplayForMassUpdateDTO();
                        	if(selectFieldToMassUpdate.isbStructure() && massUpdateData.getStructureId()!=null)
        	        			displayMassUpdateDTO.setStructureId(massUpdateData.getStructureId());
        	        		if(selectFieldToMassUpdate.isbProjectManager() && massUpdateData.getDisplaySalesRepId()!=null)
        	        			displayMassUpdateDTO.setDisplaySalesRepId(massUpdateData.getDisplaySalesRepId());
        	        		if(selectFieldToMassUpdate.isbStatus() && massUpdateData.getStatusId()!=null)
        	        			displayMassUpdateDTO.setStatusId(massUpdateData.getStatusId());
        	        		if(selectFieldToMassUpdate.isbCorrugateMfgt() && massUpdateData.getCorrugateId()!=null)
        	        			displayMassUpdateDTO.setCorrugateId(massUpdateData.getCorrugateId());
        	        		if(selectFieldToMassUpdate.isbPromoPeriod() && massUpdateData.getPromoPeriodId()!=null)
        	        			displayMassUpdateDTO.setPromoPeriodId(massUpdateData.getPromoPeriodId());
        	        		if(selectFieldToMassUpdate.isbSkuMixFinal())
        	        			displayMassUpdateDTO.setSkuMixFinalFlg(massUpdateData.isSkuMixFinalFlg());
        	        		if(selectFieldToMassUpdate.isbPriceAvailCompleteOnDt() && massUpdateData.getPriceAvailabilityDate()!=null)
        	        			displayMassUpdateDTO.setPriceAvailabilityDate(massUpdateData.getPriceAvailabilityDate());
        	        		if(selectFieldToMassUpdate.isbSampleToCustomerBy() && massUpdateData.getSentSampleDate()!=null)
        	        			displayMassUpdateDTO.setSentSampleDate(massUpdateData.getSentSampleDate());
        	        		if(selectFieldToMassUpdate.isbFinalArtworkPODueDt() && massUpdateData.getFinalArtworkDate()!=null)
        	        			displayMassUpdateDTO.setFinalArtworkDate(massUpdateData.getFinalArtworkDate());
        	        		if(selectFieldToMassUpdate.isbPIMCompletedonDt() && massUpdateData.getPimCompletedDate()!=null)
        	        			displayMassUpdateDTO.setPimCompletedDate(massUpdateData.getPimCompletedDate());
        	        		if(selectFieldToMassUpdate.isbPricingAdminValidationDt() && massUpdateData.getPricingAdminDate()!=null)
        	        			displayMassUpdateDTO.setPricingAdminDate(massUpdateData.getPricingAdminDate());
        	        		if(selectFieldToMassUpdate.isbProjectConfidenceLevel() &&  massUpdateData.getProjectLevel()!=null)
        	        			displayMassUpdateDTO.setProjectLevel(massUpdateData.getProjectLevel());
        	        		if(selectFieldToMassUpdate.isbInitRenderingEstQuote())
        	        			displayMassUpdateDTO.setIntialRendering(massUpdateData.getInitialRendering());
        	        		if(selectFieldToMassUpdate.isbStructApproved())
        	        			displayMassUpdateDTO.setStructureApproved(massUpdateData.getStructureApproved());
        	        		if(selectFieldToMassUpdate.isbOrdersReceived())
        	        			displayMassUpdateDTO.setOrderIn(massUpdateData.getOrderIn());
        	        		if(selectFieldToMassUpdate.isbComments())
        	        			displayMassUpdateDTO.setComments(massUpdateData.getComments()==null ? "" : massUpdateData.getComments());
        	        		AppService.App.getInstance().massUpdateSelectedItems(App.getUser().getUsername(),displayIdList, displayMassUpdateDTO,  new Service.SaveMassUpdateRecords(displays,  btnApplyUpdates, box));
                        }
                        if (btn.getText().equals("No")) {
                        }
                        if (btn.getText().equals("Cancel")) {                                            
                        }
                    }});
                box.setMessage(msg);
                box.show();
        }
        }
	};

	private final Button btnCancel = new Button(CANCEL, listener);
	private final Button btnSelectFields = new Button(SELECT_FIELDS, listener);
	private final Button btnApplyUpdates = new Button(APPLY_UPDATES, listener);

	@Override
	protected void onRender(Element parent, int index) {
		super.onRender(parent, index);

		createForm();

		massUpdateFieldSelection = new MassUpdateFieldSelection(dataGrid, displaysGrid, selectFieldToMassUpdate);

		loadSelectedDisplays(displayIdList);
	}

   	public void createForm() {        
        int x = 125;
        ColumnConfig column;
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        List<ColumnConfig> dataColConfigs = new ArrayList<ColumnConfig>();

        // get data for dropdowns
        AppService.App.getInstance().getStructureDTOs(new Service.SetStructuresStore(structures));
        AppService.App.getInstance().getDisplaySalesRepDTOs(new Service.SetProjectManagerStore(projectManagers));
        AppService.App.getInstance().getStatusDTOs(new Service.SetStatusStore(statuses));
        AppService.App.getInstance().getManufacturerCountryDTOs(new Service.SetManufacturersCountryStore(null,corrugates));
        AppService.App.getInstance().getPromoPeriodCountryDTOs(new Service.SetPromoPeriodsCountryStore(null,promoPeriods));
	                 
        // Data Grid
        // =================================================
    	column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.DISPLAY_ID);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("ID");
        column.setSortable(false);
        column.setMenuDisabled(true);
        column.setWidth(x - 25);
        dataColConfigs.add(column);
        
        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.CUSTOMER_NAME);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Customer");
        column.setSortable(false);
        column.setMenuDisabled(true);
        column.setWidth(x - 25);
        dataColConfigs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.SKU);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("SKU");
        column.setSortable(false);
        column.setMenuDisabled(true);
        column.setWidth(x-25);
        dataColConfigs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.DESCRIPTION);
        column.setAlignment(HorizontalAlignment.RIGHT);
        column.setHeader("Description");
        column.setWidth(x*2);
        column.setSortable(false);
        column.setFixed(true);
        column.setMenuDisabled(true);
        dataColConfigs.add(column);
        
        // Structure
		final ComboBox<StructureDTO> structureCombo = new ComboBox<StructureDTO>();
		structureCombo.setFieldLabel("Structure");
		structureCombo.setEmptyText("Structure...");
		structureCombo.setSelection(structures.getModels());
		structureCombo.setDisplayField(StructureDTO.STRUCTURE_NAME);
		structureCombo.setValueField(StructureDTO.STRUCTURE_NAME);
		structureCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
		structureCombo.setWidth(x);
		structureCombo.setStore(structures);
		structureCombo.setEditable(false);
		structureCombo.setTypeAhead(true);
		structureCombo.setSelectOnFocus(true);

		CellEditor structureEditor = new CellEditor(structureCombo) {  
			@Override  
			public Object preProcessValue(Object value) {  
				if (value == null) {
					return null;
				}
				String valueAttr = ((ComboBox<StructureDTO> )structureCombo).getValueField();
				if (valueAttr == null) valueAttr = "value";
				return ((ComboBox<StructureDTO> )structureCombo).getStore().findModel(valueAttr, value);
			}  
			@Override  
			public Object postProcessValue(Object value) {  
				if (value == null) {
					return null;
				}
				String valueAttr = ((ComboBox<StructureDTO>)structureCombo).getValueField();
				if (valueAttr == null) valueAttr = "value";
				return ((StructureDTO)value).get(valueAttr);
			}  
		}; 

		column = new ColumnConfig();
		column.setHeader("Structure");
		column.setId(DisplayForMassUpdateDTO.STRUCTURE_NAME);
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setEditor(structureEditor);
		column.setWidth(x);
		column.setSortable(false);
		column.setMenuDisabled(true);
		column.setHidden(!selectFieldToMassUpdate.isbStructure());
		dataColConfigs.add(column);
  	
        // Project Manager
	    final ComboBox<DisplaySalesRepDTO> projectManagerCombo = new ComboBox<DisplaySalesRepDTO>();
	    projectManagerCombo.setFieldLabel("Project Manager");
	    projectManagerCombo.setEmptyText("Project Manager...");
	    projectManagerCombo.setStore(projectManagers);
	    projectManagerCombo.setDisplayField(DisplaySalesRepDTO.FULL_NAME);
	    projectManagerCombo.setValueField(DisplaySalesRepDTO.FULL_NAME);
	    projectManagerCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
	    projectManagerCombo.setWidth(x-15);
	    projectManagerCombo.setEditable(false);
	    projectManagerCombo.setTypeAhead(true);
	    projectManagerCombo.setSelectOnFocus(true);
	    projectManagerCombo.setSelection(projectManagers.getModels());
		
	    CellEditor projectManagerEditor = new CellEditor(projectManagerCombo) {  
			@Override  
			public Object preProcessValue(Object value) {  
				if (value == null) {
			         return null;
				}
				String valueAttr = ((ComboBox<DisplaySalesRepDTO> )projectManagerCombo).getValueField();
				if (valueAttr == null) valueAttr = "value";
				return ((ComboBox<DisplaySalesRepDTO> )projectManagerCombo).getStore().findModel(valueAttr, value);
			}  
			@Override  
			public Object postProcessValue(Object value) {  
				if (value == null) {
			    	return null;
				}
				String valueAttr = ((ComboBox<DisplaySalesRepDTO>)projectManagerCombo).getValueField();
				if (valueAttr == null) valueAttr = "value";
				return ((DisplaySalesRepDTO)value).get(valueAttr);
			}  
	    };  

		column = new ColumnConfig();
		column.setHeader("ProjectManager");
		column.setId(DisplayForMassUpdateDTO.FULL_NAME);
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setEditor(projectManagerEditor);
		column.setWidth(x);
		column.setSortable(false);
		column.setMenuDisabled(true);
		column.setHidden(!selectFieldToMassUpdate.isbProjectManager());
		dataColConfigs.add(column);

		// Status
	    final ComboBox<StatusDTO> statusCombo = new ComboBox<StatusDTO>();
		statusCombo.setFieldLabel("Status");
		statusCombo.setEmptyText("Status...");
		statusCombo.setSelection(statuses.getModels());
		statusCombo.setDisplayField(StatusDTO.STATUS_NAME);
		statusCombo.setValueField(StatusDTO.STATUS_NAME);
		statusCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
		statusCombo.setStore(statuses);
		statusCombo.setWidth(x-15);
		statusCombo.setTypeAhead(true);
		statusCombo.setSelectOnFocus(true);
	    	
		CellEditor statusEditor = new CellEditor(statusCombo) {  
			@Override  
			public Object preProcessValue(Object value) {  
				if (value == null) {
					return null;
				}
				String valueAttr = ((ComboBox<StatusDTO> )statusCombo).getValueField();
				if (valueAttr == null) valueAttr = "value";
				return ((ComboBox<StatusDTO> )statusCombo).getStore().findModel(valueAttr, value);
			}  
			@Override  
			public Object postProcessValue(Object value) {  
				if (value == null) {
					return null;
				}
				String valueAttr = ((ComboBox<StatusDTO>)statusCombo).getValueField();
				if (valueAttr == null) valueAttr = "value";
				return ((StatusDTO)value).get(valueAttr);
			}  
		};
		
		column = new ColumnConfig();
		column.setHeader("Status");
		column.setId(DisplayForMassUpdateDTO.STATUS_NAME);
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setEditor(statusEditor);
		column.setWidth(x);
		column.setSortable(false);
		column.setMenuDisabled(true);
		column.setHidden(!selectFieldToMassUpdate.isbStatus());		
		dataColConfigs.add(column);
		
		// Corrugate Mfg
	    final ComboBox<ManufacturerCountryDTO> corrugateCombo = new ComboBox<ManufacturerCountryDTO>();
		corrugateCombo.setFieldLabel("Corrugate Mfg");
		corrugateCombo.setEmptyText("Corrugate...");
		corrugateCombo.setSelection(corrugates.getModels());
		corrugateCombo.setDisplayField(ManufacturerCountryDTO.NAME);
		corrugateCombo.setValueField(ManufacturerCountryDTO.NAME);
		corrugateCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
		corrugateCombo.setStore(corrugates);
		corrugateCombo.setWidth(x-15);
		corrugateCombo.setEditable(false);
		corrugateCombo.setTypeAhead(true);
		corrugateCombo.setSelectOnFocus(true);

		CellEditor corrugateEditor = new CellEditor(corrugateCombo) {  
			@Override  
			public Object preProcessValue(Object value) {  
			if (value == null) {
				return null;
			}
			String valueAttr = ((ComboBox<ManufacturerCountryDTO> )corrugateCombo).getValueField();
			if (valueAttr == null) valueAttr = "value";
			return ((ComboBox<ManufacturerCountryDTO> )corrugateCombo).getStore().findModel(valueAttr, value);
			}  
			@Override  
			public Object postProcessValue(Object value) {  
				if (value == null) {
					return null;
				}
				String valueAttr = ((ComboBox<ManufacturerCountryDTO>)corrugateCombo).getValueField();
				if (valueAttr == null) valueAttr = "value";
				validateSelectedFields();
				return ((ManufacturerCountryDTO)value).get(valueAttr);
			}  
		};  

		
		column = new ColumnConfig();
		column.setHeader("Corrugate Mfg");
		column.setId(DisplayForMassUpdateDTO.CORRUGATE_NAME);
		column.setAlignment(Style.HorizontalAlignment.LEFT);
		column.setEditor(corrugateEditor);
		column.setWidth(x);
		column.setSortable(false);
		column.setMenuDisabled(true);
		column.setHidden(!selectFieldToMassUpdate.isbCorrugateMfgt());
		dataColConfigs.add(column);
		
		// Promo Period
	    final ComboBox<PromoPeriodCountryDTO> promoPeriodCombo = new ComboBox<PromoPeriodCountryDTO>();
 	    promoPeriodCombo.setFieldLabel("Promo Period");
      	promoPeriodCombo.setEmptyText("Promo Period...");
      	promoPeriodCombo.setSelection(promoPeriods.getModels());
 	    promoPeriodCombo.setDisplayField(PromoPeriodCountryDTO.PROMO_PERIOD_NAME);
 	    promoPeriodCombo.setValueField(PromoPeriodCountryDTO.PROMO_PERIOD_NAME);
 	    promoPeriodCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
      	promoPeriodCombo.setStore(promoPeriods);
 	    promoPeriodCombo.setWidth(x-15);
        promoPeriodCombo.setEditable(false);
      	promoPeriodCombo.setTypeAhead(true);
      	promoPeriodCombo.setSelectOnFocus(true);

        CellEditor promoPeriodEditor = new CellEditor(promoPeriodCombo) { 
  	       @Override  
  	       public Object preProcessValue(Object value) {  
  	    	   if (value == null) {
  	    		   return null;
  	    	   }
  	    	   String valueAttr = ((ComboBox<PromoPeriodCountryDTO> )promoPeriodCombo).getValueField();
  	    	   if (valueAttr == null) valueAttr = "value";
  	    	   return ((ComboBox<PromoPeriodCountryDTO> )promoPeriodCombo).getStore().findModel(valueAttr, value);
  	       }  
  	       @Override  
  	       public Object postProcessValue(Object value) {  
  	    	   if (value == null) {
  	    		   return null;
  	    	   }
  	    	   String valueAttr = ((ComboBox<PromoPeriodCountryDTO>)promoPeriodCombo).getValueField();
  	    	   if (valueAttr == null) valueAttr = "value";
  	    	   return ((PromoPeriodCountryDTO)value).get(valueAttr);
  	       }  
 	    };  
 	    
      	column = new ColumnConfig();
        column.setHeader("Promo Period");
      	column.setId(DisplayForMassUpdateDTO.PROMO_PERIOD_NAME);
        column.setAlignment(Style.HorizontalAlignment.LEFT);
 	    column.setEditor(promoPeriodEditor);
        column.setWidth(x);
        column.setSortable(false);
        column.setMenuDisabled(true);
		column.setHidden(!selectFieldToMassUpdate.isbPromoPeriod());
		dataColConfigs.add(column);

		// SKU Mix Final
     	CheckColumnConfig checkColumnSKUMix = new CheckColumnConfig(DisplayForMassUpdateDTO.SKU_MIX_FINAL_TF, "SKU Mix Final", 50);
     	checkColumnSKUMix.setId(DisplayForMassUpdateDTO.SKU_MIX_FINAL_TF);
     	checkColumnSKUMix.setHeader("SKU Mix Final");
     	checkColumnSKUMix.setWidth(x-15);
     	checkColumnSKUMix.setSortable(false);
     	checkColumnSKUMix.setMenuDisabled(true);
     	checkColumnSKUMix.setAlignment(Style.HorizontalAlignment.CENTER);
     	checkColumnSKUMix.setEditor(new CellEditor(new CheckBox()));
     	checkColumnSKUMix.setHidden(!selectFieldToMassUpdate.isbSkuMixFinal());
     	dataColConfigs.add(checkColumnSKUMix);


     	// Price Avail on Complete Dt
        final DateField dateFieldPriceAvail = new DateField();
 		dateFieldPriceAvail.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
        dateFieldPriceAvail.setEditable(false);
        dateFieldPriceAvail.setWidth(x-15);

        column = new ColumnConfig();  
        column.setHeader("Price Avail Complete On Dt.");  
        column.setId(DisplayForMassUpdateDTO.PRICE_AVAILABILITY_DATE);  
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
		column.setEditor(new CellEditor(dateFieldPriceAvail));
        column.setWidth(x);
        column.setSortable(false);
        column.setMenuDisabled(true);
        column.setHidden(!selectFieldToMassUpdate.isbPriceAvailCompleteOnDt());
  		dataColConfigs.add(column);

  		// Sample to Customer By
  	    final DateField dateFieldSampleToCustomer=new DateField();
		dateFieldSampleToCustomer.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
		dateFieldSampleToCustomer.setEditable(false);
		dateFieldSampleToCustomer.setWidth(x-15);
	
		column = new ColumnConfig();  
		column.setHeader("Sample To Customer By");
		column.setId(DisplayForMassUpdateDTO.SENT_SAMPLE_DATE);  
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
		column.setEditor(new CellEditor(dateFieldSampleToCustomer));
		column.setWidth(x);  
		column.setSortable(false);
		column.setMenuDisabled(true);
		column.setHidden(!selectFieldToMassUpdate.isbSampleToCustomerBy());
		dataColConfigs.add(column);
  		
  	    // Final Artwork and PO Due Dt
  	    final DateField dateFieldFinalArtwork=new DateField();		
        dateFieldFinalArtwork.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
        dateFieldFinalArtwork.setEditable(false);
        dateFieldFinalArtwork.setWidth(x-15);
                
        column = new ColumnConfig();  
        column.setHeader("Final Artwork & PO Due Dt.");  
        column.setId(DisplayForMassUpdateDTO.FINAL_ARTWORK_DATE);  
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        column.setEditor(new CellEditor(dateFieldFinalArtwork));
        column.setWidth(x);
        column.setSortable(false);
        column.setMenuDisabled(true);
		column.setHidden(!selectFieldToMassUpdate.isbFinalArtworkPODueDt());
		dataColConfigs.add(column);
  	    
		// PIM Completed on Dt
  	    final DateField dateFieldPIMCompletedonDt = new DateField();		
  	    dateFieldPIMCompletedonDt.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
		dateFieldPIMCompletedonDt.setEditable(false);
		dateFieldPIMCompletedonDt.setWidth(x-15);

		column = new ColumnConfig();  
		column.setHeader("PIM Completed on Dt.");  
		column.setId(DisplayForMassUpdateDTO.PIM_COMPLETED_DATE);  
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
		column.setEditor(new CellEditor(dateFieldPIMCompletedonDt));
		column.setWidth(x);
		column.setSortable(false);
		column.setMenuDisabled(true);
		column.setHidden(!selectFieldToMassUpdate.isbPIMCompletedonDt());
		dataColConfigs.add(column);

		// Pricing Admin Validation
  	    final DateField dateFieldPricingAdminValidationDt = new DateField();		
     	dateFieldPricingAdminValidationDt.getPropertyEditor().setFormat(DateTimeFormat.getFormat("MM/dd/y"));  
        dateFieldPricingAdminValidationDt.setEditable(false);
        dateFieldPricingAdminValidationDt.setWidth(x-15);
        
        column = new ColumnConfig();  
        column.setHeader("Pricing Admin Validation Dt.");  
        column.setId(DisplayForMassUpdateDTO.PRICING_ADMIN_DATE);  
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
  		column.setEditor(new CellEditor(dateFieldPricingAdminValidationDt));
        column.setWidth(x);
        column.setSortable(false);
        column.setMenuDisabled(true);
		column.setHidden(!selectFieldToMassUpdate.isbPricingAdminValidationDt());
		dataColConfigs.add(column);
		
		// Project Confidence Level
		final NumberField numFieldPCLevel = new NumberField();
     	numFieldPCLevel.setMinValue(0);
     	numFieldPCLevel.setMaxValue(100);
     	numFieldPCLevel.setAutoHeight(true);
     	numFieldPCLevel.setMaxLength(3);
     	numFieldPCLevel.setMinLength(0);
     	numFieldPCLevel.setWidth(x-15);
     	numFieldPCLevel.setAutoValidate(true);
     	numFieldPCLevel.setSelectOnFocus(true);
     	numFieldPCLevel.setHeight(25);
     	numFieldPCLevel.setToolTip("field size is from 0 to 100 digits");
     	numFieldPCLevel.setPropertyEditorType(Short.class);

     	column = new ColumnConfig();  
     	column.setHeader("Project Confidence Level");  
     	column.setId(DisplayForMassUpdateDTO.PROJECT_CONFIDENCE_LEVEL);  
     	column.setEditor(new CellEditor(numFieldPCLevel));
     	column.setSortable(false);
     	column.setWidth(125);
		column.setHidden(!selectFieldToMassUpdate.isbProjectConfidenceLevel());
		dataColConfigs.add(column);

        // Init Rendering & Est Quote (Y/N)
		CheckColumnConfig checkColumnInit = new CheckColumnConfig(DisplayForMassUpdateDTO.INITIAL_RENDERING_TF, "Init Rendering & Est Quote (Y/N)", 50);
		checkColumnInit.setId(DisplayForMassUpdateDTO.INITIAL_RENDERING_TF);
		checkColumnInit.setHeader("Init Rendering & Est Quote (Y/N)");
		checkColumnInit.setAlignment(Style.HorizontalAlignment.CENTER);
		checkColumnInit.setSortable(false);
		checkColumnInit.setMenuDisabled(true);
		checkColumnInit.setWidth(x);
		checkColumnInit.setEditor(new CellEditor(new CheckBox()));
		checkColumnInit.setHidden(!selectFieldToMassUpdate.isbInitRenderingEstQuote());
		dataColConfigs.add(checkColumnInit);

        // Struct Approved (Y/N)
		CheckColumnConfig checkColumnStru = new CheckColumnConfig(DisplayForMassUpdateDTO.STRUCTURE_APPROVED_TF, "Struct Approved (Y/N)", 50);
		checkColumnStru.setId(DisplayForMassUpdateDTO.STRUCTURE_APPROVED_TF);
		checkColumnStru.setHeader("Struct Approved (Y/N)");
		checkColumnStru.setAlignment(Style.HorizontalAlignment.CENTER);
		checkColumnStru.setWidth(x);
		checkColumnStru.setSortable(false);
		checkColumnStru.setMenuDisabled(true);
		checkColumnStru.setEditor(new CellEditor(new CheckBox()));
		checkColumnStru.setHidden(!selectFieldToMassUpdate.isbStructApproved());
		dataColConfigs.add(checkColumnStru);
	
		
        // Orders Received (Y/N)
		CheckColumnConfig checkColumnOrdRec = new CheckColumnConfig(DisplayForMassUpdateDTO.ORDERS_RECEIVED_TF, "Orders Received (Y/N)", 50);
		checkColumnOrdRec.setId(DisplayForMassUpdateDTO.ORDERS_RECEIVED_TF);
		checkColumnOrdRec.setHeader("Orders Received (Y/N)");
		checkColumnOrdRec.setAlignment(Style.HorizontalAlignment.CENTER);
		checkColumnOrdRec.setSortable(false);
		checkColumnOrdRec.setWidth(x);
		checkColumnOrdRec.setMenuDisabled(true);
		checkColumnOrdRec.setEditor(new CellEditor(new CheckBox()));
		checkColumnOrdRec.setHidden(!selectFieldToMassUpdate.isbOrdersReceived());
		dataColConfigs.add(checkColumnOrdRec);
		

		// Comments
		final TextField<String> txtFieldComments = new TextField<String>();
    	txtFieldComments.setWidth(x-15);
    	txtFieldComments.setAutoHeight(true);
    	txtFieldComments.setHeight(50);
    	txtFieldComments.setMinLength(0);
    	txtFieldComments.setAutoValidate(true);
    	txtFieldComments.setSelectOnFocus(true);
    	txtFieldComments.setMaxLength(200);
    	txtFieldComments.setToolTip("field size is from 0 to 200 characters");

        column = new ColumnConfig();  
        column.setHeader("Comments");  
        column.setId(DisplayForMassUpdateDTO.COMMENTS);  
        column.setEditor(new CellEditor(txtFieldComments));
        column.setSortable(false);
        column.setWidth(125);
    	column.setHidden(!selectFieldToMassUpdate.isbComments());
		dataColConfigs.add(column);		
		
		// create data grid
        ColumnModel dataCm = new ColumnModel(dataColConfigs);
        GridSelectionModel<DisplayForMassUpdateDTO> gs = new GridSelectionModel<DisplayForMassUpdateDTO>();
        gs.setMoveEditorOnEnter(true);
        gs.selectNext(true);
        gs.selectPrevious(true);

	    dataGrid = new EditorGrid<DisplayForMassUpdateDTO>(dataRow, dataCm);
	    dataGrid.setStyleAttribute("borderTop", "none");
	    dataGrid.setColumnResize(true);
	    dataGrid.setBorders(true);
	    dataGrid.setSelectionModel(gs);
	    dataGrid.setWidth(2600);
	    dataGrid.setHeight(45);
	    dataGrid.setClicksToEdit(ClicksToEdit.ONE);
		dataGridView = dataGrid.getView();
		dataGridView.focusRow(0);
		
     	dataGrid.addPlugin(checkColumnSKUMix);
		dataGrid.addPlugin(checkColumnInit);
		dataGrid.addPlugin(checkColumnStru);
		dataGrid.addPlugin(checkColumnOrdRec);

		 dataGrid.addListener(Events.AfterEdit, new Listener<GridEvent<ModelData>>() {
		    	public void handleEvent(GridEvent<ModelData> be) {
		    		validateSelectedFields();
		    		lastDataCol = be.getColIndex();
		    		cp.setHScrollPosition(lastHpos);
		    	}
		    });

		    dataGrid.addListener(Events.OnMouseDown, new Listener<GridEvent<ModelData>>() {
		    	public void handleEvent(GridEvent<ModelData> be) {
		    		int hscrollPos= cp.getHScrollPosition();
		    		int colPos = massUpdateFieldSelection.selectedColumnPos(be.getColIndex());
		    		if (colPos > 7 && hscrollPos == 0) hscrollPos = lastHpos;
		    		lastHpos = hscrollPos;
		    	}
		    });

		    dataGrid.addListener(Events.OnClick, new Listener<GridEvent<ModelData>>() {
		    	public void handleEvent(GridEvent<ModelData> be) {
		    		cp.setHScrollPosition(lastHpos);
		    	}
		    });


	    	
        
        // Display Grid
        // =================================================
        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.DISPLAY_ID);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("ID");        
        column.setWidth(x - 25);
        configs.add(column);
        
        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.CUSTOMER_NAME);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Customer");
        column.setWidth(x - 25);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.SKU);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("SKU");
        column.setWidth(x-25);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.DESCRIPTION);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Description");
        column.setWidth(x*2);
        configs.add(column);
       
        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.STRUCTURE_NAME);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Structure");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbStructure());
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.FULL_NAME);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Project Manager");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbProjectManager());
        configs.add(column);
        
        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.STATUS_NAME);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Status");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbStatus());
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.CORRUGATE_NAME);//To be changed
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Corrugate Mfgt");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbCorrugateMfgt());
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.PROMO_PERIOD_NAME);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Promo Period");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbPromoPeriod());
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.SKU_MIX_FINAL_FLG);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("SKU Mix Final");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbSkuMixFinal());
        configs.add(column);

        column = new ColumnConfig();
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        column.setId(DisplayForMassUpdateDTO.PRICE_AVAILABILITY_DATE);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Price Availability Date");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbPriceAvailCompleteOnDt());
        configs.add(column);

        column = new ColumnConfig();
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        column.setId(DisplayForMassUpdateDTO.SENT_SAMPLE_DATE);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Sample To Customer By");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbSampleToCustomerBy());
        configs.add(column);

        column = new ColumnConfig();
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        column.setId(DisplayForMassUpdateDTO.FINAL_ARTWORK_DATE);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Final Artwork PO Due Date");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbFinalArtworkPODueDt());
        configs.add(column);

        column = new ColumnConfig();
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        column.setId(DisplayForMassUpdateDTO.PIM_COMPLETED_DATE);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Pim Completed Date");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbPIMCompletedonDt());
        configs.add(column);

        column = new ColumnConfig();
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        column.setId(DisplayForMassUpdateDTO.PRICING_ADMIN_DATE);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Pricing Admin Date");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbPricingAdminValidationDt());
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.PROJECT_CONFIDENCE_LEVEL);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Project Confidence Level");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbProjectConfidenceLevel());
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.INITIAL_RENDERING);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Initial Rendering");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbInitRenderingEstQuote());
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.STRUCTURE_APPROVED);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Structure Approved");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbStructApproved());
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.ORDER_IN);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Order Received");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbOrdersReceived());
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayForMassUpdateDTO.COMMENTS);
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setHeader("Comments");
        column.setWidth(x);
        column.setHidden(!selectFieldToMassUpdate.isbComments());
        configs.add(column);

        ColumnModel cm = new ColumnModel(configs);

     	displaysGrid = new Grid<DisplayForMassUpdateDTO>(displays, cm);
        displaysGrid.setStyleAttribute("borderTop", "none");
        displaysGrid.setStripeRows(true);
        displaysGrid.setHideHeaders(true);
	    displaysGrid.setBorders(true);
	    displaysGrid.setAutoHeight(true);
	    displaysGrid.setWidth(2600);
	    displaysGrid.setTabIndex(0);
	    
	    
        cp = new ContentPanel();
        cp.setHeading("DPS - Mass Update Screen");
        cp.setHeaderVisible(true);
        cp.setBodyBorder(false);
        cp.setButtonAlign(HorizontalAlignment.CENTER);
        cp.setFrame(true);
 		cp.setSize(1190, 450); 
        cp.add(dataGrid);
        cp.add(displaysGrid);
        cp.setScrollMode(Scroll.AUTO);        	      

        ButtonBar buttonBar = new ButtonBar();
        buttonBar.setSize(1050, -1);
        buttonBar.setSpacing(10);
        btnApplyUpdates.disable();
        buttonBar.add(new FillToolItem());
        buttonBar.add(btnCancel);
        buttonBar.add(btnSelectFields);
        buttonBar.add(btnApplyUpdates);

        setPermissions();

        add(cp);
        add(buttonBar);
    }
 
	private void setPermissions(){
    	//if(!App.isAuthorized("MassUpdateGrid.CANCEL")) btnCancel.disable();
    	//if(!App.isAuthorized("MassUpdateGrid.SELECT_FIELDS")) btnSelectFields.disable();
    	//if(!App.isAuthorized("MassUpdateGrid.APPLY_UPDATES")) btnApplyUpdates.disable();
    }
    
    public void loadSelectedDisplays(List<Long> parameters){
        MessageBox box = MessageBox.wait("Progress", "loading selected projects for mass update, please wait...", "Loading...");
        AppService.App.getInstance().getSelectedDisplaysForMassUpdate(parameters, new Service.LoadSelectedDisplaysForMassUpdate(displays, box));
    }

    private void forwardToDisplayProjectSelection(){
        App.getVp().clear();
        App.getVp().add(new DisplayProjectSelection());
    }
	    
	public void validateSelectedFields() {
		 if (dataRow.getAt(0) != null) {
			 if(selectFieldToMassUpdate.isbStructure()){
				 if (dataRow.getAt(0).getStructureName() == null) {
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbProjectManager()){
				 if(dataRow.getAt(0).getFullName()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbStatus()){
				 if(dataRow.getAt(0).getStatusName()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbCorrugateMfgt()){
				 if(dataRow.getAt(0).getCorrugateName()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbPromoPeriod()){
				 if(dataRow.getAt(0).getPromoPeriodName()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbPriceAvailCompleteOnDt()){
				 if(dataRow.getAt(0).getPriceAvailabilityDate()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbSampleToCustomerBy()){
				 if(dataRow.getAt(0).getSentSampleDate()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbFinalArtworkPODueDt()){
				 if(dataRow.getAt(0).getFinalArtworkDate()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbPIMCompletedonDt()){
				 if(dataRow.getAt(0).getPimCompletedDate()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbPricingAdminValidationDt()){
				 if(dataRow.getAt(0).getPricingAdminDate()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbProjectConfidenceLevel()){
				 if(dataRow.getAt(0).getProjectLevel()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 if(selectFieldToMassUpdate.isbComments() ){
				 if(dataRow.getAt(0).getComments()==null){
					 btnApplyUpdates.disable();
					 return;
				 }
			 }
			 btnApplyUpdates.enable();
		 }else{
			 btnApplyUpdates.disable();
		 }
	}
	
	private void setMassUpdateData() {
        DisplayForMassUpdateDTO model = new DisplayForMassUpdateDTO();
	    model.setDescription("  Set Values:");
	    if (massUpdateData.getStructureName() != null) model.setStructureName(massUpdateData.getStructureName());
	    model.setFullName(massUpdateData.getFullName());
	    model.setStatusName(massUpdateData.getStatusName());
	    model.setCorrugateName(massUpdateData.getCorrugateName());
	    model.setPromoPeriodName(massUpdateData.getPromoPeriodName());
	    model.setSkuMixFinalFlg(massUpdateData.isSkuMixFinalFlg());
	    model.setSkuMixFinalTF(massUpdateData.isSkuMixFinalTF() == null ? false : massUpdateData.isSkuMixFinalTF());
	    model.setPriceAvailabilityDate(massUpdateData.getPriceAvailabilityDate());
	    model.setSentSampleDate(massUpdateData.getSentSampleDate());
	    model.setFinalArtworkDate(massUpdateData.getFinalArtworkDate());
	    model.setPimCompletedDate(massUpdateData.getPimCompletedDate());
	    model.setPricingAdminDate(massUpdateData.getPricingAdminDate());
	    model.setProjectLevel(massUpdateData.getProjectLevel());
	    model.setIntialRendering(massUpdateData.getInitialRendering());
	    model.setInitialRenderingTF(massUpdateData.isInitialRenderingTF() == null ? false : massUpdateData.isInitialRenderingTF());
	    model.setStructureApproved(massUpdateData.getStructureApproved());
	    model.setStructureApprovedTF(massUpdateData.isStrucureApprovedTF() == null ? false : massUpdateData.isStrucureApprovedTF());
	    model.setOrderIn(massUpdateData.getOrderIn());
	    model.setOrdersReceivedTF(massUpdateData.isOrdersReceivedTF() == null ? false : massUpdateData.isOrdersReceivedTF());
	    model.setComments(massUpdateData.getComments());	    
	    dataRow.add(model);
	}
	
	private DisplayForMassUpdateDTO getMassUpdateData() {
		DisplayForMassUpdateDTO data = new DisplayForMassUpdateDTO();
		
		if (dataRow.getModels().size() > 0) {
			data = dataRow.getModels().get(0);
			data.setSkuMixFinalFlg(data.isSkuMixFinalTF()!= null && data.isSkuMixFinalTF() ? "Y" : "N");
			data.setIntialRendering(data.isInitialRenderingTF()!= null && data.isInitialRenderingTF() ? "Y" : "N");
			data.setStructureApproved(data.isStrucureApprovedTF()!=null && data.isStrucureApprovedTF() ? "Y" : "N");
			data.setOrderIn(data.isOrdersReceivedTF()!=null && data.isOrdersReceivedTF() ? "Y" : "N");
			if (data.getStructureName() != null)
				data.setStructureId( (structures.findModel(StructureDTO.STRUCTURE_NAME, data.getStructureName())).getStructureId() );
			if (data.getFullName() != null)
				data.setDisplaySalesRepId((projectManagers.findModel(DisplaySalesRepDTO.FULL_NAME, data.getFullName())).getDisplaySalesRepId());
			if (data.getStatusName() != null)
				data.setStatusId((statuses.findModel(StatusDTO.STATUS_NAME, data.getStatusName())).getStatusId());
			if (data.getCorrugateName() != null)
				data.setCorrugateId((corrugates.findModel(ManufacturerCountryDTO.NAME, data.getCorrugateName())).getManufacturerId());
			if (data.getPromoPeriodName() != null)
				data.setPromoPeriodId((promoPeriods.findModel(PromoPeriodCountryDTO.PROMO_PERIOD_NAME, data.getPromoPeriodName())).getPromoPeriodId());
		
		}

		return data;			
	}
}
