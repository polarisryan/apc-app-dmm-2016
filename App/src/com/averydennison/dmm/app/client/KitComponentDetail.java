package com.averydennison.dmm.app.client;

import java.util.Date;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayPlannerDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.models.InPogDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.averydennison.dmm.app.client.models.ValidBuDTO;
import com.averydennison.dmm.app.client.util.KeyUtil;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.Orientation;
import com.extjs.gxt.ui.client.data.ChangeEvent;
import com.extjs.gxt.ui.client.data.ChangeListener;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.Radio;
import com.extjs.gxt.ui.client.widget.form.RadioGroup;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Timer;

/**
 * User: Spart Arguello
 * Date: Oct 27, 2009
 * Time: 9:46:11 AM
 */
public class KitComponentDetail extends LayoutContainer implements DirtyInterface{
    private VerticalPanel vp;
    public static final String PREVIOUS = "Previous";
    public static final String NEXT = "Next";
    public static final String SAVE = "Save";
    public static final String RETURN = "Return";
    public static final String RETURN_TO_SELECTION = "Return to Selection";

    public static final String PRODUCTION_VERSION = "Production Version";
    public static final String BREAK_CASE = "Break Case";
    public static final String PLANT_BREAK_CASE = "Plant Break Case";

    public static Long DISPLAY_SETUP_KIT = 1L;
    public static Long DISPLAY_SETUP_FG_NEW_UPC = 2L;
    public static Long DISPLAY_SETUP_FG_NEW_PROD_VER = 3L;
    public static Long KIT_COMP_DEFAULT_QTY_ONE = 1L;

    
    private DisplayTabHeaderV2 displayTabHeader = new DisplayTabHeaderV2();
    private KitComponentDTO kitComponentModel;
    private DisplayDTO displayModel;
    private DisplayScenarioDTO displayScenarioModel;
    private List<Long> ids;
    private Long selectedIdx;
    private Boolean readOnly;
    private Boolean sourceFromCostAnalysis;
    private Date peReCalcDate;
    private ChangeListener displayListener;
    private ChangeListener kitComponentListener;

    // production sku
    private final TextField<String> componentSku = new TextField<String>();
    private final TextField<String> description = new TextField<String>();
    private final ComboBox<ValidBuDTO> buDesc = new ComboBox<ValidBuDTO>();
    private final NumberField regCasePack = new NumberField();
    private final CheckBox promoFlg = new CheckBox();
    private final CheckBox i2ForecastFlg = new CheckBox();
    private final CheckBox srcPimFlg = new CheckBox();

    // display qty
    private final NumberField qtyPerFacing = new NumberField();
    private final NumberField numFacings = new NumberField();
    private final NumberField qtyPerDisplay = new NumberField();
    private final NumberField inventoryRequirement = new NumberField();

    // kit version code setup
    private final Radio productionVersionRadio = new Radio();
    private final Radio breakCaseRadio = new Radio();
    private final Radio plantBreakCaseRadio = new Radio();
    private final RadioGroup radioGrp = new RadioGroup();
    private final TextField<String> kitVersionCode = new TextField<String>();
    private final CheckBox plantBreakCaseFlg = new CheckBox();
    private final TextField<String> pbcVersion = new TextField<String>();

    // finished goods display reference sku
    private final TextField<String> finishedGoodsSku = new TextField<String>();
    private final NumberField finishedGoodsQty = new NumberField();

    private KitComponentInfoPlannerGrid plannerGrid;
    private KitComponentDetailCostPricingGrid costGrid;
   /* private DisplayCostDataGroup objDisplayCostDataGroup;*/
    
    private final NumberField excessInvtPct = new NumberField();
    private final ComboBox<InPogDTO> inPogYn = new ComboBox<InPogDTO>();
    final Double pctMaxValue=9999.99d;
    final Double pctFixedValue=100.00d;
    private ListStore<InPogDTO> inPogYorNStore ;
    
   /* public DisplayDTO getDisplayModel() {
		return displayModel;
	}

	public void setDisplayModel(DisplayDTO displayModel) {
		this.displayModel = displayModel;
	}*/

	public KitComponentDetail() {
        vp = new VerticalPanel();
        vp.setSpacing(10);
    }

    public DisplayScenarioDTO getDisplayScenarioModel() {
		return displayScenarioModel;
	}

	public void setDisplayScenarioModel(DisplayScenarioDTO displayScenarioModel) {
		this.displayScenarioModel = displayScenarioModel;
	}

	public KitComponentDetail(KitComponentDTO kitComponent, DisplayDTO display, DisplayScenarioDTO displayScenarioModel, List<Long> ids, Long selectedIdx, Boolean readOnly, Boolean sourceFromCostAnalysis, Date peReCalcDate/*, DisplayCostDataGroup objDisplayCostDataGroup*/) {
    	vp = new VerticalPanel();
        vp.setSpacing(10);
        this.kitComponentModel = kitComponent;
        this.displayModel = display;
        this.displayScenarioModel=displayScenarioModel;
        /*this.objDisplayCostDataGroup=objDisplayCostDataGroup;*/
        this.ids = ids;
        this.selectedIdx = selectedIdx;
        this.readOnly = readOnly;
        this.sourceFromCostAnalysis=sourceFromCostAnalysis;
        this.peReCalcDate=peReCalcDate;
        if (displayModel.getDisplaySetupId() == null)
            displayModel.setDisplaySetupId(DISPLAY_SETUP_KIT);
        loadDefaultScenario(displayModel);
    	}

    private void setPermissions(){
    	if(!App.isAuthorized("KitComponentDetail.SAVE")) btnSave.disable();
    }
    public Radio getRadio1() {
        return productionVersionRadio;
    }

    public Radio getRadio2() {
        return breakCaseRadio;
    }

    public Radio getRadio3() {
        return plantBreakCaseRadio;
    }

    
    public void saveDefaultDisplayCost(DisplayScenarioDTO displayCostScenarioDTO){
    	DisplayCostDTO displaCostDTO = new DisplayCostDTO();
    	displaCostDTO.setDisplayId(displayCostScenarioDTO.getDisplayId());
    	displaCostDTO.setDisplayScenarioId(displayCostScenarioDTO.getDisplayScenarioId());
    	displaCostDTO.setDtCreated(displayCostScenarioDTO.getDtCreated());
    	displaCostDTO.setUserCreated(displayCostScenarioDTO.getUserCreated());
    	AppService.App.getInstance().saveDisplayCostDTO(displaCostDTO, new Service.SaveDefaultDisplayCostDTO(displaCostDTO));
    }
    public void saveDefaultScenario( DisplayScenarioDTO displayScenarioDTO){
    	setDisplayScenarioModel(displayScenarioDTO);
    	save();
    }
    
    public void save() {
/*    try{*/
    	if (displayModel.getDisplayId() == null) {
   		 	MessageBox.alert("Validation Error", "There is no display defined " , null);
   		 	return;
    	}
        if (!componentSku.validate()) return;
        if (!description.validate()) return;
        if (!buDesc.validate()) return;
        if (!regCasePack.validate()) return;
        if (!promoFlg.validate()) return;
        if (!i2ForecastFlg.validate()) return;
        if (!srcPimFlg.validate()) return;
        if (!qtyPerFacing.validate()) return;

        if (!numFacings.validate()) return;
        if (!qtyPerDisplay.validate()) return;
        if (!inventoryRequirement.validate()) return;
        if (!productionVersionRadio.validate()) return;
        if (!breakCaseRadio.validate()) return;
        if (!plantBreakCaseRadio.validate()) return;
        if (!kitVersionCode.validate()) return;
        if (!plantBreakCaseFlg.validate()) return;
        if (!pbcVersion.validate()) return;
        if (!finishedGoodsSku.validate()) return;
        if (!finishedGoodsQty.validate()) return;
        if (!excessInvtPct.validate()) return;
        if (!inPogYn.validate()) return;
        String breakCase = "";
        String validationErrorMsg = "";
        if (productionVersionRadio.getValue()) breakCase = PRODUCTION_VERSION;
        else if (breakCaseRadio.getValue()) breakCase = BREAK_CASE;
        else if (plantBreakCaseRadio.getValue()) breakCase = PLANT_BREAK_CASE;

        // validate Component SKU Info section
        if (componentSku.getValue() == null)
            validationErrorMsg += "Please enter a SKU.\n";                    	
        if (description.getValue() == null)
            validationErrorMsg += "Please enter a Description.\n";                    	
    	if (buDesc.getValue() == null)
            validationErrorMsg += "Please select a BU.\n";                    	
		if (regCasePack.getValue() == null)
            validationErrorMsg += "Please enter Ret Pk/In Ctn Qty.\n";                    	

        // validate Kit Version Code section
        if (displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_KIT) || displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_PROD_VER)) {
        	if (breakCase.equals("")) {
                validationErrorMsg += "Please specify a Kit Version Code setup.\n";                    	
            }
            else if (breakCase.equals(PRODUCTION_VERSION)) {
                if (kitVersionCode.getValue() == null)
                    validationErrorMsg += "Kit Version Code is required for Production Version SKUs.\n";
            } else if (breakCase.equals(BREAK_CASE)) {
                if (kitVersionCode.getValue() == null)
                    validationErrorMsg += "Kit Version Code is required for Break Case Version SKUs.\n";
                else
                	System.out.println("KitComponentDetail: Break Case Validation: kitVersionCode=" + kitVersionCode.getValue());
            } else if (breakCase.equals(PLANT_BREAK_CASE)) {
                if (kitVersionCode.getValue() == null)
                    validationErrorMsg += "Kit Version Code is required for Plant Break Case Version SKUs.\n";
                if (plantBreakCaseFlg.getValue() != null && pbcVersion.getValue() == null)
                    validationErrorMsg += "PBC Version is required for Plant Break Case Version SKUs.\n";
            }
        }
        // validate Finished Goods
        if (displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)) {
            if (finishedGoodsSku.getValue() == null || finishedGoodsQty.getValue() == null) {
                validationErrorMsg += "The Finished Goods Display Reference SKU section must be " +
                        "completed for Finished Goods - New UPC Displays.";
            }else if(finishedGoodsSku.getValue().length() < 15){
            	validationErrorMsg += "The Finished Goods Display Reference SKU must be a 15 degits data.";
            }
        }
        if (validationErrorMsg.length() > 0) {
            messageBox("Validation Error", validationErrorMsg);
            return;
        }
        kitComponentModel.setDisplayId(displayModel.getDisplayId());
        kitComponentModel.setQtyPerFacing((Long) qtyPerFacing.getValue());
        kitComponentModel.setNumberFacings((Long) numFacings.getValue());
        kitComponentModel.setPlantBreakCaseFlg(plantBreakCaseFlg.getValue());
        kitComponentModel.setI2ForecastFlg(i2ForecastFlg.getValue());
        kitComponentModel.setRegCasePack((Long) regCasePack.getValue());
        kitComponentModel.setSourcePimFlg(srcPimFlg.getValue());
        kitComponentModel.setSku(componentSku.getValue());
        kitComponentModel.setProductName(description.getValue());
        kitComponentModel.setBuDesc(buDesc.getValue().getBuDesc());
        kitComponentModel.setPromoFlg(promoFlg.getValue());
        kitComponentModel.setBreakCase(breakCase);
        kitComponentModel.setKitVersionCode(kitVersionCode.getValue());
        kitComponentModel.setPbcVersion(pbcVersion.getValue());
        kitComponentModel.setFinishedGoodsSku(finishedGoodsSku.getValue());
        kitComponentModel.setFinishedGoodsQty((Long) finishedGoodsQty.getValue());
        kitComponentModel.setUserLastChanged(App.getUser().getUsername());
        if(getDisplayScenarioModel()!=null){
        	kitComponentModel.setDisplayScenarioId(getDisplayScenarioModel().getDisplayScenarioId());
        }else{
        	kitComponentModel.setDisplayScenarioId(null);
        }
        kitComponentModel.setPeReCalcDate(peReCalcDate);
        kitComponentModel.setDtLastChanged(new Date());
        if (kitComponentModel.getKitComponentId() == null) {
            kitComponentModel.setUserCreated(App.getUser().getUsername());
            kitComponentModel.setDtCreated(new Date());
        }
        kitComponentModel.setInPogYn(inPogYn.getValue().getInPogYorNId()==null?Boolean.FALSE:inPogYn.getValue().getInPogYorNId());
        Double excessInvtPctVal = (Double) (excessInvtPct.getValue()==null?0.00:excessInvtPct.getValue().doubleValue());
        kitComponentModel.setExcessInvtPct(excessInvtPctVal.floatValue());
       
        // show progress bar
        final MessageBox progressBar = MessageBox.wait("Progress",
                "Saving kit component info, please wait...", "Saving...");
        // save kit component info
        List<Record> mods = plannerGrid.getStore().getModifiedRecords();
        for (int i = 0; i < mods.size(); i++) {
        	DisplayPlannerDTO dpDTO = (DisplayPlannerDTO)((Record)mods.get(i)).getModel();
        	dpDTO.setDtLastChanged(new Date());
        	dpDTO.setUserLastChanged(App.getUser().getUsername());
        	if (dpDTO.getDtCreated() == null) {
        		dpDTO.setDtCreated(new Date());
        		dpDTO.setUserCreated(App.getUser().getUsername());
        	}
        }
        plannerGrid.setKitComponentInfoPlannerGridModified(false);
        List<DisplayPlannerDTO> store = plannerGrid.getStore().getModels();
       // AppService.App.getInstance().saveKitComponentDTO(kitComponentModel, displayModel, store, new Service.SaveKitComponentDTO(kitComponentModel, progressBar, plannerGrid, btnSave,getDisplayScenarioModel(),objDisplayCostDataGroup.getCostAnalysisInfo()));
        AppService.App.getInstance().saveKitComponentDTO(kitComponentModel, displayModel, store, new Service.SaveKitComponentDTO(kitComponentModel, progressBar, plannerGrid, btnSave));
        if(!sourceFromCostAnalysis){
        	AppService.App.getInstance().saveDisplayDTO(displayModel,
        		new Service.SaveDisplayDTO(displayModel, displayTabHeader, null, null));
        }
   /* }catch(Exception ex){
    	MessageBox.alert("KitComponentDetail:save", "..Error.."+ex.toString() , null);
    }*/
    }
    
    private SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
        public void componentSelected(ButtonEvent ce) {
            Button btn = (Button) ce.getComponent();
            if (btn.getText().equals(SAVE)) {
            	if(getDisplayScenarioModel()!=null){
            		save();
            	}else{
            		attachDefaultScenario();
            	}
            } else if (btn.getText().equals(RETURN_TO_SELECTION)) {
                if(isDirty()){
                    String title = "You have unsaved information!";
                    String msg = "Would you like to save?";                             
                    MessageBox box = new MessageBox();
                    box.setButtons(MessageBox.YESNOCANCEL);
                    box.setIcon(MessageBox.QUESTION);
                    box.setTitle("Save Changes?");
                    box.addCallback(l);
                    box.setTitle(title);
                    box.setMessage(msg);
                    box.show();
                }else{
                    forwardToDisplayProjectSelection();
                }
            }  else if (btn.getText().equals(PREVIOUS)) {
            	 
            	if (isDirty()) {
                	if (kitComponentModel.getKitComponentId() == null) {
                        MessageBox.alert("New Item", "Please save this item before switching...", null);
                	}
                	else {
	                	Listener<MessageBoxEvent>cb = new Listener<MessageBoxEvent>() {
	                		public void handleEvent(MessageBoxEvent mbe) {
	                			String id = mbe.getButtonClicked().getItemId();
	                			if (Dialog.YES == id){
	                				if(getDisplayScenarioModel()!=null){
	                					save();
	                				}else{
	                					attachDefaultScenario();
	                				}
	                			}
	        	            	// move previous
	        	                if (selectedIdx > 0L) {
	        	                    selectedIdx--;
	        	                    final MessageBox prevProgressBar = MessageBox.wait("Progress", "Loading previous item...", "Loading...");
	        	                    AppService.App.getInstance().getKitComponentDTO((Long) ids.get(selectedIdx.intValue()), displayModel,displayScenarioModel,
	        	                            new Service.LoadKitComponent(displayModel,displayScenarioModel, ids, selectedIdx, App.getVp(), btnSave, readOnly, prevProgressBar,sourceFromCostAnalysis,peReCalcDate));
	        	                }
	                		}
	                	};
	                	MessageBox.confirm("Save?", "This record has been modified. Do you wish to save before exiting?", cb);
                	}
            	}
            	else {
	            	// move previous
	                if (selectedIdx > 0L) {
	                    selectedIdx--;
	                    final MessageBox prevProgressBar = MessageBox.wait("Progress", "Loading previous item...", "Loading...");
	                    AppService.App.getInstance().getKitComponentDTO((Long) ids.get(selectedIdx.intValue()), displayModel,displayScenarioModel,
	                            new Service.LoadKitComponent(displayModel, displayScenarioModel,ids, selectedIdx, App.getVp(), btnSave, readOnly, prevProgressBar,sourceFromCostAnalysis,peReCalcDate));
	                }
            	}
            } else if (btn.getText().equals(NEXT)) {
            	if (isDirty()) {
                	if (kitComponentModel.getKitComponentId() == null) {
                        MessageBox.alert("New Item", "Please save this item before switching...", null);
                	}
                	else {
	                	Listener<MessageBoxEvent>cb = new Listener<MessageBoxEvent>() {
	                		public void handleEvent(MessageBoxEvent mbe) {
	                			String id = mbe.getButtonClicked().getItemId();
	                			if (Dialog.YES == id) {
	                				if(getDisplayScenarioModel()!=null){
	                					save();
	                				}else{
	                					attachDefaultScenario();
	                				}
	                			}
	        	            	// move next
	        	                if (selectedIdx < ids.size()) {
	        	                    selectedIdx++;
	        	                    final MessageBox nextProgressBar = MessageBox.wait("Progress", "Loading next item...", "Loading...");
	        	                    AppService.App.getInstance().getKitComponentDTO((Long) ids.get(selectedIdx.intValue()), displayModel,displayScenarioModel,
	        	                            new Service.LoadKitComponent(displayModel,displayScenarioModel, ids, selectedIdx, App.getVp(), btnSave, readOnly, nextProgressBar,sourceFromCostAnalysis,peReCalcDate));
	        	                }
	                		}
	                	};
	                	MessageBox.confirm("Save?", "This record has been modified. Do you wish to save before exiting?", cb);
                	}
            	}
            	else {
	            	// move next
	                if (selectedIdx < ids.size()) {
	                    selectedIdx++;
	                    final MessageBox nextProgressBar = MessageBox.wait("Progress", "Loading next item...", "Loading...");
	                    AppService.App.getInstance().getKitComponentDTO((Long) ids.get(selectedIdx.intValue()), displayModel,displayScenarioModel,
	                            new Service.LoadKitComponent(displayModel,displayScenarioModel, ids, selectedIdx, App.getVp(), btnSave, readOnly, nextProgressBar,sourceFromCostAnalysis,peReCalcDate));
	                }
            	}
                plannerGrid.setKitComponentInfoPlannerGridModified(false);
            } else if (btn.getText().equals(RETURN)) {

                if(isDirty()){
                    String title = "You have unsaved information!";
                    String msg = "Would you like to save?";                             
                    MessageBox box = new MessageBox();
                    box.setButtons(MessageBox.YESNOCANCEL);
                    box.setIcon(MessageBox.QUESTION);
                    box.setTitle("Save Changes?");
                    box.addCallback(l2);
                    box.setTitle(title);
                    box.setMessage(msg);
                    box.show();
                }else{
                    forwardToKitComponentInfo();
                }
                plannerGrid.setKitComponentInfoPlannerGridModified(false);
            }
        }
    };
    private final Listener<MessageBoxEvent> l = new Listener<MessageBoxEvent>() {
        public void handleEvent(MessageBoxEvent mbe) {
            Button btn = mbe.getButtonClicked();
            if (btn.getText().equals("Yes")) {
                save();
                final MessageBox box = MessageBox.wait("Progress",
                		"Loading price exception from ILS, please wait...", "Loading...");
                	Timer t = new Timer() {
                	    public void run() {
    			              box.close();
    			              forwardToDisplayProjectSelection();
    			        }
    			    };
    			    t.schedule(9000);
    	    }
            if (btn.getText().equals("No")) {
                forwardToDisplayProjectSelection();
                plannerGrid.setKitComponentInfoPlannerGridModified(false);

            }
            if (btn.getText().equals("Cancel")) {
            }
        }
    };
    private final Listener<MessageBoxEvent> l2 = new Listener<MessageBoxEvent>() {
        public void handleEvent(MessageBoxEvent mbe) {
            Button btn = mbe.getButtonClicked();
            if (btn.getText().equals("Yes")) {
                if(getDisplayScenarioModel()!=null){
					save();
				}else{
					attachDefaultScenario();
				}
                
                final MessageBox box = MessageBox.wait("Progress",
                		"Loading price exception from ILS, please wait...", "Loading...");
                	Timer t = new Timer() {
                	    public void run() {
    			              box.close();
    			              forwardToKitComponentInfo();
    			        }
    			    };
    			    t.schedule(9000);
            }
            if (btn.getText().equals("No")) {
                forwardToKitComponentInfo();
                plannerGrid.setKitComponentInfoPlannerGridModified(false);

            }
            if (btn.getText().equals("Cancel")) {
            }
        }
    };
    private void forwardToDisplayProjectSelection(){
        clearModel();
        App.getVp().clear();
        App.getVp().add(new DisplayProjectSelection());
    }
    private void forwardToKitComponentInfo(){
        clearModel();
      App.getVp().clear();
      DisplayTabs displayTabs=null;
      if(!sourceFromCostAnalysis){
     // displayTabs = new DisplayTabs(displayModel, readOnly, DisplayTabs.KIT_COMPONENT_INFO, false);
    	  displayTabs=App.getDisplayTabs();
    	  displayTabs.reloadScenarios();
      }else{
    	 // displayTabs = new DisplayTabs(displayModel, readOnly, DisplayTabs.COST_ANALYSIS_INFO, false);
    	  /*
    	   * Restore the DisplayTabs screen
    	   * Date : 11/17/2010
    	   * By : Mari
    	   */
    	  displayTabs=App.getDisplayTabs();
    	  displayTabs.reloadScenarios(getDisplayScenarioModel().getScenarioNum());
      }
      App.getVp().add(displayTabs);
    }
    private final Button btnSave = new Button(SAVE, listener);
    private final Button btnPrevious = new Button(PREVIOUS, listener);
    private final Button btnNext = new Button(NEXT, listener);
    private final Button btnReturnToSelection = new Button(RETURN_TO_SELECTION, listener);
    private final Button btnReturn = new Button(RETURN, listener);

    private void clearModel() {
        productionVersionRadio.removeAllListeners();
        breakCaseRadio.removeAllListeners();
        plantBreakCaseRadio.removeAllListeners();
        kitVersionCode.removeAllListeners();
        pbcVersion.removeAllListeners();
        qtyPerFacing.removeAllListeners();
        numFacings.removeAllListeners();
        if (displayModel != null) displayModel.removeChangeListener(displayListener);
        if (kitComponentModel != null) kitComponentModel.removeChangeListener(kitComponentListener);
        kitComponentModel = null;
    }

    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);
        createHeader();
        createForm();
        setModelsAndView();
        setPermissions();
        add(vp);
    }

    private void createHeader() {
    	add(new HeaderPanel());
        add(displayTabHeader);
    }

    private void setModelsAndView() {
        if (displayModel.getDisplayId() != null) {
            displayTabHeader.setDisplay(displayModel);
        }
        loadDataElements();
        setReadOnly();
        kitComponentListener = new ChangeListener() {
            public void modelChanged(com.extjs.gxt.ui.client.data.ChangeEvent event) {
            	System.out.println("KitComponentDetail.listener: event type=" + event.getType());
            	System.out.println("KitComponentDetail.listener: event type..1.." ); 
            	if ((KitComponentDTO) event.getSource() != null) {
            		System.out.println("KitComponentDetail.listener: event type..2.." );
            		kitComponentModel = (KitComponentDTO) event.getSource();
            		System.out.println("KitComponentDetail.listener: event type..3.." );
            		if (kitComponentModel.getKitComponentId() != null && !ids.contains(kitComponentModel.getKitComponentId())) {
            			System.out.println("KitComponentDetail.listener: event type..4.." );
            			ids.add(kitComponentModel.getKitComponentId());
            			System.out.println("KitComponentDetail.listener: event type..5.." );
            		}
            	}
            	System.out.println("KitComponentDetail.listener: event type..ends.." );
            }
        };
        kitComponentModel.addChangeListener(kitComponentListener);
        displayListener = new ChangeListener() {
            public void modelChanged(ChangeEvent event) {
            	System.out.println("KitComponentDetail.listener: event type..6.." );
                displayModel = (DisplayDTO) event.getSource();
                System.out.println("KitComponentDetail.listener: event type..7.." );
                if (displayModel != null && displayModel.getDisplayId() != null) {
                	System.out.println("KitComponentDetail.listener: event type..8.." );
                	displayTabHeader.setDisplay(displayModel);
                	System.out.println("KitComponentDetail.listener: event type..9.." );
                }
            }
        };
        displayModel.addChangeListener(displayListener);

    }

    private void setReadOnly() {
        if (readOnly == null) return;
        if (readOnly) {
            componentSku.setReadOnly(readOnly);
            description.setReadOnly(readOnly);
            buDesc.setReadOnly(readOnly);
            regCasePack.setReadOnly(readOnly);
            promoFlg.setReadOnly(readOnly);
            i2ForecastFlg.setReadOnly(readOnly);
            qtyPerFacing.setReadOnly(readOnly);
            numFacings.setReadOnly(readOnly);
            qtyPerDisplay.setReadOnly(readOnly);
            productionVersionRadio.setReadOnly(readOnly);
            breakCaseRadio.setReadOnly(readOnly);
            plantBreakCaseRadio.setReadOnly(readOnly);
            kitVersionCode.setReadOnly(readOnly);
            plantBreakCaseFlg.setReadOnly(readOnly);
            pbcVersion.setReadOnly(readOnly);
            finishedGoodsSku.setReadOnly(readOnly);
            finishedGoodsQty.setReadOnly(readOnly);
            btnSave.disable();
        }
        srcPimFlg.setReadOnly(true);
        qtyPerDisplay.setReadOnly(true);
        inventoryRequirement.setReadOnly(true);
    }

    private void loadDefaultScenario(DisplayDTO displayDTO){
    	if (displayDTO.getDisplayId() != null
				&& displayDTO.getDisplayId() > 0){
    	AppService.App.getInstance().getDisplayScenarioDTO(
    			displayDTO.getDisplayId(),
				new Service.LoadDisplayScenarioInfoForKitComponent(this));
    	}
		
    }
    
    private void attachDefaultScenario(){
    	if (displayModel.getDisplayId() == null) {
   		 	//MessageBox.alert("Validation Error", "There is no display defined " , null);
   		 	return;
    	}else{
    		//MessageBox.alert("Validation ", "attachDefaultScenario displayId="+displayModel.getDisplayId() , null);
    	}
    	List<DisplayScenarioDTO> displayScenarioDTOs=this.getData("displayScenarioDTOs");
    	DisplayScenarioDTO displayScenarioDTO=null;
    	if(displayScenarioDTOs!=null && displayScenarioDTOs.size()>0){
    		displayScenarioDTO=displayScenarioDTOs.get(0);
    		displayScenarioDTO.setDisplayId(displayModel.getDisplayId());
    		AppService.App.getInstance().saveDisplayScenarioDTO( displayScenarioDTO, new Service.SaveWithDefaultScenario( displayScenarioDTO,getKitComponentDetail()));
    	}else{
    		displayScenarioDTO =new  DisplayScenarioDTO();
			displayScenarioDTO.setScenarioNum(new Long(1));
			displayScenarioDTO.setDisplayId(displayModel.getDisplayId());
			displayScenarioDTO.setDtCreated(new Date());
			displayScenarioDTO.setDisplayScenarioId(null);
			displayScenarioDTO.setUserCreated(App.getUser().getUsername());
			AppService.App.getInstance().saveDisplayScenarioDTO( displayScenarioDTO, new Service.SaveWithDefaultScenarioAndCost( displayScenarioDTO,getKitComponentDetail()));
    	}
    	
    }
    
    private void loadDataElements() {
        componentSku.setValue(kitComponentModel.getSku());
        description.setValue(kitComponentModel.getProductName());
        if (kitComponentModel.getBuDesc() != null) {
        	ValidBuDTO buDTO = new ValidBuDTO();
        	buDTO.setBuDesc(kitComponentModel.getBuDesc());
            buDesc.setValue(buDTO);
        }        
        regCasePack.setValue(kitComponentModel.getRegCasePack());
        if(kitComponentModel.isPromoFlg()!=null){
            promoFlg.setValue(kitComponentModel.isPromoFlg());
            promoFlg.setOriginalValue(kitComponentModel.isPromoFlg());
        }
        if(kitComponentModel.isI2ForecastFlg()!=null){
            i2ForecastFlg.setValue(kitComponentModel.isI2ForecastFlg());
            i2ForecastFlg.setOriginalValue(kitComponentModel.isI2ForecastFlg());
        }
        if(kitComponentModel.isSourcePimFlg()!=null){
            srcPimFlg.setValue(kitComponentModel.isSourcePimFlg());
            srcPimFlg.setOriginalValue(kitComponentModel.isSourcePimFlg());
        }
        if(displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)){
        	//Default qty as 1
        	if(kitComponentModel.getQtyPerFacing()==null || kitComponentModel.getQtyPerFacing().equals(0L)){
        		kitComponentModel.setQtyPerFacing(KIT_COMP_DEFAULT_QTY_ONE);
        	}
        	if(kitComponentModel.getNumberFacings()==null || kitComponentModel.getNumberFacings().equals(0L)){
        		kitComponentModel.setNumberFacings(KIT_COMP_DEFAULT_QTY_ONE);
        	}
        	if(kitComponentModel.getQtyPerFacing()==null || kitComponentModel.getNumberFacings()==null || 
        			kitComponentModel.getQtyPerFacing().equals(0L) || kitComponentModel.getNumberFacings().equals(0L) ){
        		kitComponentModel.setQtyPerDisplay(null);
        	}else{
        		kitComponentModel.setQtyPerDisplay(kitComponentModel.getQtyPerFacing() * kitComponentModel.getNumberFacings());
        	}
        	if(kitComponentModel.getQtyPerDisplay()==null ||  displayModel.getActualQty()==null || 
        			kitComponentModel.getQtyPerDisplay().equals(0L)||  displayModel.getActualQty().equals(0L)){
        		kitComponentModel.setInventoryRequirement(null);	
        	}else{
        		if(getDisplayScenarioModel()!=null){
        			kitComponentModel.setInventoryRequirement(kitComponentModel.getQtyPerDisplay() * getDisplayScenarioModel().getActualQty());
        		}else{
        			kitComponentModel.setInventoryRequirement(kitComponentModel.getQtyPerDisplay() * displayModel.getActualQty());
        		}
        	}
        }
       	qtyPerFacing.setValue(kitComponentModel.getQtyPerFacing());
        numFacings.setValue(kitComponentModel.getNumberFacings());
        qtyPerDisplay.setValue(kitComponentModel.getQtyPerDisplay());
        inventoryRequirement.setValue(kitComponentModel.getInventoryRequirement());
        if (kitComponentModel.getBreakCase() != null) {
            if (kitComponentModel.getBreakCase().equals(PRODUCTION_VERSION)) {
                productionVersionRadio.setValue(Boolean.TRUE);
                productionVersionRadio.setOriginalValue(Boolean.TRUE);
            } else if (kitComponentModel.getBreakCase().equals(BREAK_CASE)) {
                breakCaseRadio.setValue(true);
                breakCaseRadio.setOriginalValue(true);
            } else if (kitComponentModel.getBreakCase().equals(PLANT_BREAK_CASE)) {
                plantBreakCaseRadio.setValue(true);
                plantBreakCaseRadio.setOriginalValue(true);
            }else{
            	  productionVersionRadio.setValue(Boolean.TRUE);
                  productionVersionRadio.setOriginalValue(Boolean.TRUE);
                  kitComponentModel.setBreakCase(PRODUCTION_VERSION);
            }
        }else{
        	if(!displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)){
        		productionVersionRadio.setValue(Boolean.TRUE);
        		productionVersionRadio.setOriginalValue(Boolean.TRUE);
        		kitComponentModel.setBreakCase(PRODUCTION_VERSION);
        	 }else{
        		productionVersionRadio.setValue(Boolean.FALSE);
         		productionVersionRadio.setOriginalValue(Boolean.FALSE);
         		kitComponentModel.setBreakCase(null);
        	 }
        }
        kitVersionCode.setValue(kitComponentModel.getKitVersionCode());
        if(kitComponentModel.isPlantBreakCaseFlg()!=null){
            plantBreakCaseFlg.setValue(kitComponentModel.isPlantBreakCaseFlg());
            plantBreakCaseFlg.setOriginalValue(kitComponentModel.isPlantBreakCaseFlg());
        }
        pbcVersion.setValue(kitComponentModel.getPbcVersion());
        finishedGoodsSku.setValue(kitComponentModel.getFinishedGoodsSku());
        finishedGoodsQty.setValue(kitComponentModel.getFinishedGoodsQty());

        // set access based on source
        if (kitComponentModel.isSourcePimFlg()) {
            componentSku.setReadOnly(true);
            description.setReadOnly(true);
            buDesc.setReadOnly(true);
            regCasePack.setReadOnly(true);
            promoFlg.setReadOnly(true);
            srcPimFlg.setReadOnly(true);
        }

        // setup kit version code section
        if (displayModel.getDisplaySetupId() != null) {
            if (displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_KIT) || displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_PROD_VER)) {
            	if (kitComponentModel.getBreakCase() != null) {
            		System.out.println("-------Break Case is " + kitComponentModel.getBreakCase());
            	}
            	if (kitComponentModel.getBreakCase() == null || kitComponentModel.getBreakCase().equals(PRODUCTION_VERSION)) {
                	String s = componentSku.getValue();
                	if (s == null) s = kitComponentModel.getVersionNo();
                	if (s != null && s.length()==15) s = s.substring(10);
                	if (s == null) s = "";
                    kitVersionCode.setValue(s);
                    kitVersionCode.setReadOnly(true);
                    plantBreakCaseFlg.setValue(false);
                    plantBreakCaseFlg.setReadOnly(true);
                    pbcVersion.setValue("");
                    pbcVersion.setReadOnly(true);            		
            	}
            	else if (kitComponentModel.getBreakCase().equals(BREAK_CASE)) {
                    kitVersionCode.setReadOnly(false);            		
                    plantBreakCaseFlg.setValue(false);
                    plantBreakCaseFlg.setReadOnly(true);
                    pbcVersion.setValue("");
                    pbcVersion.setReadOnly(true);            		
            	}
            	else if (kitComponentModel.getBreakCase().equals(PLANT_BREAK_CASE)) {
                	String s = componentSku.getValue();
                	if (s == null) s = kitComponentModel.getVersionNo();
                	if (s.length()==15) s = s.substring(10);
                    kitVersionCode.setValue(s);
                    kitVersionCode.setReadOnly(true);
            	}
            }
        }

        // setup finished goods display reference section
        if (displayModel.getDisplaySetupId() != null && displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)) {
            finishedGoodsSku.setReadOnly(false);
            finishedGoodsQty.setReadOnly(false);
            kitVersionCode.setReadOnly(true);
            plantBreakCaseFlg.setReadOnly(true);
            pbcVersion.setReadOnly(true);
            productionVersionRadio.setReadOnly(true);
            productionVersionRadio.setValue(Boolean.FALSE);
     		productionVersionRadio.setOriginalValue(Boolean.FALSE);
            breakCaseRadio.setReadOnly(true);
            plantBreakCaseRadio.setReadOnly(true);
        } else {
            finishedGoodsSku.setReadOnly(true);
            finishedGoodsQty.setReadOnly(true);
        }
        updateCostForFGSKU();
        if(kitComponentModel.getExcessInvtPct()!=null){
        	excessInvtPct.setValue(kitComponentModel.getExcessInvtPct());
        }else{
        	/*
        	 * By : Mari
        	 * Date : 09-15-2011
        	 * Description: Default it 100% when it is NULL
        	 */
        	excessInvtPct.setValue(100f);
        }
        
        System.out.println("+++KitComponentDetail: kitComponentModel.getInPogYn()..1..: "+kitComponentModel.getInPogYn());
        System.out.println("+++KitComponentDetail: Service.getInPogYnMap().size()..2..: "+Service.getInPogYnMap().size());
        if(kitComponentModel.getInPogYn()!=null){
        	if(kitComponentModel.getInPogYn().equals(Boolean.TRUE)){
        		inPogYn.setValue(new InPogDTO(Boolean.TRUE,"Yes"));
        	}else{
        		inPogYn.setValue(new InPogDTO(Boolean.FALSE,"No"));
        	}
        }else{
        	inPogYn.setValue(new InPogDTO(Boolean.FALSE,"No"));
        }
        //inPogYn.setValue(Service.getInPogYnMap().get(kitComponentModel.getInPogYn()));
        // set buttons
        if (selectedIdx <= 0) btnPrevious.disable();
        if (selectedIdx >= (ids.size() - 1)) btnNext.disable();
    }
    
    private void updateCostForFGSKU(){
    	/*
         * Description : Load finished Goods Pricing SKU when DisplaySetupId is DISPLAY_SETUP_FG_NEW_UPC
         * Date        : 01-28-2011
         * BY          : Mari
         * 
         */
        if (finishedGoodsSku.getValue() != null) {
        	if(displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)){
            	String s = finishedGoodsSku.getValue();
            	if (s == null) s = finishedGoodsSku.getValue();
            	if (s != null && s.length()==15){ 
            		kitComponentModel.setFinishedGoodsSku(s);
            		costGrid.updateCostForFGSKU(kitComponentModel);
            	}
            }
        }
    }
    private void calculateQtys(Long actualQty) {
    	Long numberFacings = (Long)(numFacings.getValue());
    	Long qtyPerFace = (Long)(qtyPerFacing.getValue());
    	Long qtyPerDisp;
    	Long invReq;
    	if (numberFacings != null && qtyPerFace != null) {
    		qtyPerDisp = numberFacings * qtyPerFace;
    	}
    	else qtyPerDisp = null;
    	kitComponentModel.setQtyPerDisplay(qtyPerDisp);
    	qtyPerDisplay.setValue(qtyPerDisp);
    	if (qtyPerDisp != null && actualQty != null) {
    		invReq = qtyPerDisp * actualQty;
    	}
    	else invReq = null;
    	kitComponentModel.setInventoryRequirement(invReq);
    	inventoryRequirement.setValue(invReq);

    }

    private void createForm() {
    	// load dropdown data
        ListStore<ValidBuDTO> bus = new ListStore<ValidBuDTO>();
        AppService.App.getInstance().getValidBuDTOs(new Service.SetValidBuStore(bus));

        FormPanel formPanel = new FormPanel();
        formPanel.setFrame(true);
        //formPanel.setSize(935, -1);
        formPanel.setSize(1190, -1);
        formPanel.setLabelAlign(FormPanel.LabelAlign.LEFT);
        formPanel.setButtonAlign(Style.HorizontalAlignment.CENTER);

        // setup layout containers
        LayoutContainer top = new LayoutContainer();
        top.setLayout(new ColumnLayout());
        LayoutContainer middle = new LayoutContainer();
        middle.setLayout(new ColumnLayout());
        LayoutContainer bottom = new LayoutContainer();
        bottom.setLayout(new ColumnLayout());

        LayoutContainer topLeft = new LayoutContainer();
        LayoutContainer topMiddle = new LayoutContainer();
        LayoutContainer topRight = new LayoutContainer();
        LayoutContainer middleLeft = new LayoutContainer();
        LayoutContainer middleRight = new LayoutContainer();
        LayoutContainer bottomLeft = new LayoutContainer();
        LayoutContainer bottomRight = new LayoutContainer();

        // setup form layouts
        FormLayout topLeftFormLayout = new FormLayout();
        topLeftFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        topLeft.setStyleAttribute("paddingRight", "10px");
        topLeft.setLayout(topLeftFormLayout);
        
        FormLayout topMiddleFormLayout = new FormLayout();
        topMiddleFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        topMiddle.setStyleAttribute("paddingRight", "10px");
        topMiddle.setLayout(topMiddleFormLayout);

        FormLayout topRightFormLayout = new FormLayout();
        topRightFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        //topRight.setStyleAttribute("paddingRight", "10px");
        topRight.setLayout(topRightFormLayout);

        FormLayout middleLeftFormLayout = new FormLayout();
        middleLeftFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        middleLeft.setStyleAttribute("paddingRight", "10px");
        middleLeft.setLayout(middleLeftFormLayout);

        FormLayout middleRightFormLayout = new FormLayout();
        middleRightFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        //middleRight.setStyleAttribute("paddingRight", "10px");
        middleRight.setLayout(middleRightFormLayout);

        FormLayout bottomLeftFormLayout = new FormLayout();
        bottomLeftFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        bottomLeft.setStyleAttribute("paddingRight", "10px");
        bottomLeft.setLayout(bottomLeftFormLayout);

        //top.add(topLeft, new ColumnData(620));
        //top.add(topRight, new ColumnData(280));
        top.add(topLeft, new ColumnData(400));
        top.add(topMiddle, new ColumnData(270));
        top.add(topRight, new ColumnData(330));
        formPanel.add(top);

        //middle.add(middleLeft, new ColumnData(620));
        //middle.add(middleRight, new ColumnData(280));
        middle.add(middleLeft, new ColumnData(670));
        middle.add(middleRight, new ColumnData(330));
        formPanel.add(middle);

        //middle.add(bottomLeft, new ColumnData(640));
        //middle.add(bottomRight, new ColumnData(260));
        middle.add(bottomLeft, new ColumnData(690));
        middle.add(bottomRight, new ColumnData(310));
        formPanel.add(bottom);

        // setup group boxes
        FieldSet productionSkuGrp = new FieldSet();
        FormLayout productionSkuFormLayout = new FormLayout();
        productionSkuFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        productionSkuGrp.setLayout(productionSkuFormLayout);
        productionSkuGrp.setHeading("Production SKU");
        productionSkuGrp.setCollapsible(false);
        topLeft.add(productionSkuGrp);
        
        FieldSet excessInvtGrp = new FieldSet();
        FormLayout excessInvtFormLayout = new FormLayout();
        excessInvtFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        excessInvtGrp.setLayout(excessInvtFormLayout);
        excessInvtGrp.setHeading("Excess Inventory");
        excessInvtGrp.setCollapsible(false);
        topMiddle.add(excessInvtGrp);

        FieldSet displayQtyGrp = new FieldSet();
        FormLayout displayQtyFormLayout = new FormLayout();
        displayQtyFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        displayQtyGrp.setLayout(displayQtyFormLayout);
        displayQtyGrp.setHeading("Display Quantity");
        displayQtyGrp.setCollapsible(false);
        topRight.add(displayQtyGrp);

        FieldSet kitVersionCodeGrp = new FieldSet();
        FormLayout kitVersionCodeFormLayout = new FormLayout();
        kitVersionCodeFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        kitVersionCodeGrp.setLayout(kitVersionCodeFormLayout);
        kitVersionCodeGrp.setHeading("Kit Version Code Setup");
        kitVersionCodeGrp.setCollapsible(false);
        middleLeft.add(kitVersionCodeGrp);

        FieldSet finishedGoodsGrp = new FieldSet();
        FormLayout finishedGoodsFormLayout = new FormLayout();
        finishedGoodsFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        finishedGoodsGrp.setLayout(finishedGoodsFormLayout);
        finishedGoodsGrp.setHeading("Finished Goods Display Reference SKU");
        finishedGoodsGrp.setCollapsible(false);
        middleRight.add(finishedGoodsGrp);

        FieldSet plannerGrp = new FieldSet();
        FormLayout plannerFormLayout = new FormLayout();
        plannerFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        plannerGrp.setLayout(plannerFormLayout);
        plannerGrp.setHeading("Planning");
        plannerGrp.setCollapsible(false);
        bottomLeft.add(plannerGrp);

        FieldSet pricingCostGrp = new FieldSet();
        FormLayout pricingCostFormLayout = new FormLayout();
        pricingCostFormLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
        pricingCostGrp.setLayout(pricingCostFormLayout);
        pricingCostGrp.setHeading("Pricing/Cost");
        pricingCostGrp.setCollapsible(false);
        bottomRight.add(pricingCostGrp);


        // production sku fields
        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setHeaderVisible(false);
        cp.setLayout(new RowLayout());
        //cp.setSize(500, 110);
        //cp.setSize(700, 110);
        cp.setSize(400, 110);
        HorizontalPanel hp1 = new HorizontalPanel();
        HorizontalPanel hp2 = new HorizontalPanel();
        HorizontalPanel hp3 = new HorizontalPanel();
        HorizontalPanel hp4 = new HorizontalPanel();
        hp1.setSpacing(2);
        hp2.setSpacing(2);
        hp3.setSpacing(2);
        hp4.setSpacing(2);
        cp.add(hp1);
        cp.add(hp2);
        cp.add(hp3);
        cp.add(hp4);
        
        LabelField componentSkuLabel = new LabelField("SKU:");
        componentSkuLabel.setWidth(80);
        hp1.add(componentSkuLabel);
        componentSku.setFieldLabel("Comp SKU");
        componentSku.setAllowBlank(true);
        componentSku.setMinLength(15);
        componentSku.setMaxLength(15);
       // componentSku.setWidth(120);
        componentSku.setWidth(120);
        componentSku.addListener(Events.KeyUp, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                if (componentSku.getValue() != null) {
                	if(!displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)){
	                	String s = componentSku.getValue();
	                	if (s == null) s = componentSku.getValue();
	                	if (s != null && s.length()==15){ 
	                		kitVersionCode.setValue(s.substring(10));
	                	}
	                    kitVersionCode.setReadOnly(true);
	                }
                }
            }
        });
        hp1.add(componentSku);

        HorizontalPanel spacer1 = new HorizontalPanel();
       // spacer1.setWidth(150);
        spacer1.setWidth(70);
        hp1.add(spacer1);

        promoFlg.setBoxLabel("Promo Version");
       // promoFlg.setWidth(150);
        promoFlg.setWidth(110);
        hp1.add(promoFlg);

        LabelField descriptionLabel = new LabelField("Desc:");
        descriptionLabel.setWidth(80);
        hp2.add(descriptionLabel);
        description.setFieldLabel("Desc");
        description.setAllowBlank(true);
        description.setWidth(250);
        //description.setWidth(230);
        hp2.add(description);

        LabelField buDescLabel = new LabelField("BU:");
       // buDescLabel.setWidth(100);
        buDescLabel.setWidth(80);
        hp3.add(buDescLabel);
        buDesc.setFieldLabel("Bus Unit");
        buDesc.setDisplayField(ValidBuDTO.BU_DESC);
        buDesc.setAllowBlank(true);
        buDesc.setStore(bus);
        buDesc.setEditable(false);
        buDesc.setTriggerAction(ComboBox.TriggerAction.ALL);
        hp3.add(buDesc);

        HorizontalPanel spacer2 = new HorizontalPanel();
        //spacer2.setWidth(125);
        //spacer2.setWidth(100);
        spacer2.setWidth(45);
        hp3.add(spacer2);

        i2ForecastFlg.setBoxLabel("I2 Forecast");
        //i2ForecastFlg.setWidth(150);
        i2ForecastFlg.setWidth(110);
        hp3.add(i2ForecastFlg);

        LabelField regCaseLabel = new LabelField("Ret Pk/In Ctn:");
        //regCaseLabel.setWidth(100);
        regCaseLabel.setWidth(80);
        hp4.add(regCaseLabel);
        regCasePack.setFieldLabel("Ret Pk/In Ctn");
        regCasePack.setPropertyEditorType(Long.class);
        regCasePack.setAllowBlank(true);
        hp4.add(regCasePack);

        HorizontalPanel spacer3 = new HorizontalPanel();
        //spacer3.setWidth(150);
       // spacer3.setWidth(125);//Mari: to resolve space issue for gxt 2.2 version Dt: 09-03-2010
        
       // spacer3.setWidth(100);
        spacer3.setWidth(45);
        hp4.add(spacer3);

        srcPimFlg.setBoxLabel("Source = PIM");
        //srcPimFlg.setWidth(150);
        srcPimFlg.setWidth(110);
        hp4.add(srcPimFlg);

        productionSkuGrp.add(cp);

        
        // production sku fields
        ContentPanel cpExcessInvt = new ContentPanel();
        cpExcessInvt.setBodyBorder(false);
        cpExcessInvt.setHeaderVisible(false);
        cpExcessInvt.setLayout(new RowLayout());
        //cp.setSize(500, 110);
        //cp.setSize(700, 110);
        cpExcessInvt.setSize(280, 110);
        HorizontalPanel hpExcessInvt1 = new HorizontalPanel();
        HorizontalPanel hpExcessInvt2 = new HorizontalPanel();
       // HorizontalPanel hpExcessInvt3 = new HorizontalPanel();
        hpExcessInvt1.setSpacing(2);
        hpExcessInvt2.setSpacing(2);
       // hpExcessInvt3.setSpacing(2);
        cpExcessInvt.add(hpExcessInvt1);
        cpExcessInvt.add(hpExcessInvt2);
        
        LabelField excessInvtPctLabel = new LabelField("Excess Inventory%:");
        excessInvtPctLabel.setWidth(120);
        hpExcessInvt1.add(excessInvtPctLabel);
		
        excessInvtPct.setFieldLabel("Excess Inventory%");
        excessInvtPct.setAllowBlank(true);
        excessInvtPct.setPropertyEditorType(Double.class);
        excessInvtPct.setAllowDecimals(true);
        excessInvtPct.setFormat(NumberFormat.getFormat("###,###,##0.00"));
        excessInvtPct.addInputStyleName("text-element-no-underline");
        excessInvtPct.setMaxLength(15);
        excessInvtPct.setWidth(50);
        excessInvtPct.addListener(Events.KeyDown, new Listener<FieldEvent>() {
		public void handleEvent(FieldEvent ke) {
				int keyCode = ke.getKeyCode();
				Double dExcessInvtPctVal = (Double) (excessInvtPct.getValue()==null?0:excessInvtPct.getValue().doubleValue());
				if(dExcessInvtPctVal >= pctMaxValue){
					if (KeyUtil.isDigit(keyCode)){
						ke.stopEvent();
					}
					
				}
			}
		});
        hpExcessInvt1.add(excessInvtPct);
        
        LabelField inPogYnLabel = new LabelField("In POG Y/N:");
        inPogYnLabel.setWidth(120);
        hpExcessInvt2.add(inPogYnLabel);
        inPogYorNStore = new ListStore<InPogDTO>();
		AppService.App.getInstance().getInPogDTOs(new Service.SetInPogStore(inPogYorNStore));
		inPogYn.setDisplayField(InPogDTO.INPOG_YN_NAME);
		inPogYn.setValueField(InPogDTO.INPOG_YN_NAME);
		inPogYn.setStore(inPogYorNStore);
		inPogYn.setAllowBlank(false);
		inPogYn.setWidth(80);
		inPogYn.setEditable(false);
		inPogYn.setTriggerAction(ComboBox.TriggerAction.ALL);
	    hpExcessInvt2.add(inPogYn);
		
        excessInvtGrp.add(cpExcessInvt);
        // display quantity fields
        ContentPanel dqcp = new ContentPanel();
        dqcp.setBodyBorder(false);
        dqcp.setHeaderVisible(false);
        dqcp.setLayout(new RowLayout());
        dqcp.setSize(240, 110);
        HorizontalPanel dqhp1 = new HorizontalPanel();
        HorizontalPanel dqhp2 = new HorizontalPanel();
        HorizontalPanel dqhp3 = new HorizontalPanel();
        HorizontalPanel dqhp4 = new HorizontalPanel();
        dqhp1.setSpacing(2);
        dqhp2.setSpacing(2);
        dqhp3.setSpacing(2);
        dqhp4.setSpacing(2);

        LabelField qtyPerFacingLabel = new LabelField("Qty Per Facing:");
        qtyPerFacingLabel.setWidth(100);
        dqhp1.add(qtyPerFacingLabel);
        qtyPerFacing.setFieldLabel("Qty Per Facing");
        qtyPerFacing.setPropertyEditorType(Long.class);
        qtyPerFacing.setAllowBlank(true);
        qtyPerFacing.setWidth(60);
        qtyPerFacing.addListener(Events.KeyUp, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                if (qtyPerFacing.getValue() != null) {
                	if(getDisplayScenarioModel()==null){
                		calculateQtys(displayModel.getActualQty());
                	}else{
                		calculateQtys(getDisplayScenarioModel().getActualQty());
                	}
                    	costGrid.update(kitComponentModel.getQtyPerDisplay());
                }else{
                	kitComponentModel.setQtyPerDisplay(null);
                	kitComponentModel.setInventoryRequirement(null);
                }
            }
        });
        dqhp1.add(qtyPerFacing);

        LabelField numFacingsLabel = new LabelField("# of Facings:");
        numFacingsLabel.setWidth(100);
        dqhp2.add(numFacingsLabel);
        numFacings.setFieldLabel("# of Facings");
        numFacings.setPropertyEditorType(Long.class);
        numFacings.setAllowBlank(true);
        numFacings.setWidth(60);
        numFacings.addListener(Events.KeyUp, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                if (numFacings.getValue() != null) {
                	if(getDisplayScenarioModel()==null){
                		calculateQtys(displayModel.getActualQty());
                	}else{
                		calculateQtys(getDisplayScenarioModel().getActualQty());
                	}
                    costGrid.update(kitComponentModel.getQtyPerDisplay());
                }else{
                	kitComponentModel.setQtyPerDisplay(null);
                	kitComponentModel.setInventoryRequirement(null);
                }
            }
        });
        dqhp2.add(numFacings);

        LabelField qtyPerDisplayLabel = new LabelField("Qty Per Display:");
        qtyPerDisplayLabel.setWidth(100);
        dqhp3.add(qtyPerDisplayLabel);
        qtyPerDisplay.setFieldLabel("Qty Per Display");
        qtyPerDisplay.setPropertyEditorType(Long.class);
        qtyPerDisplay.setAllowBlank(true);
        qtyPerDisplay.setWidth(60);
        dqhp3.add(qtyPerDisplay);

        LabelField inventoryRequirementLabel = new LabelField("Inventory Req:");
        inventoryRequirementLabel.setWidth(100);
        dqhp4.add(inventoryRequirementLabel);
        inventoryRequirement.setFieldLabel("Inventory Req");
        inventoryRequirement.setPropertyEditorType(Long.class);
        inventoryRequirement.setAllowBlank(true);
        inventoryRequirement.setWidth(100);
        dqhp4.add(inventoryRequirement);

        dqcp.add(dqhp1);
        dqcp.add(dqhp2);
        dqcp.add(dqhp3);
        dqcp.add(dqhp4);
        displayQtyGrp.add(dqcp);

        // pricing cost fields
        costGrid = new KitComponentDetailCostPricingGrid(displayModel, kitComponentModel);
        pricingCostGrp.add(costGrid);

        // kit version code fields
        ContentPanel kvcp = new ContentPanel();
        kvcp.setBodyBorder(false);
        kvcp.setHeaderVisible(false);
        kvcp.setLayout(new ColumnLayout());
        kvcp.setSize(560, 90);
        VerticalPanel kvvp1 = new VerticalPanel();
        VerticalPanel kvvp2 = new VerticalPanel();
        HorizontalPanel kvhp0 = new HorizontalPanel();
        HorizontalPanel kvhp1 = new HorizontalPanel();
        HorizontalPanel kvhp2 = new HorizontalPanel();
        HorizontalPanel kvhp3 = new HorizontalPanel();
        kvhp1.setSpacing(2);
        kvhp2.setSpacing(2);
        kvhp3.setSpacing(2);

        productionVersionRadio.setBoxLabel("Production Version");
        productionVersionRadio.setWidth(175);
        
        if (!displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)) {
	        productionVersionRadio.addListener(Events.OnClick, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	                if (productionVersionRadio.getValue() != null) {
	                	String s = componentSku.getValue();
	                	if (s == null) s = kitComponentModel.getVersionNo();
	                	if (s.length()==15) s = s.substring(10);
	                    kitVersionCode.setValue(s);
	                    kitVersionCode.setReadOnly(true);
	                    plantBreakCaseFlg.setValue(false);
	                    plantBreakCaseFlg.setReadOnly(true);
	                    pbcVersion.setValue("");
	                    pbcVersion.setReadOnly(true);
	                }
	            }
	        });
        }
        
        
        breakCaseRadio.setBoxLabel("Break Case Version");
        breakCaseRadio.setWidth(175);
        if (!displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)) {
	        breakCaseRadio.addListener(Events.OnClick, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {
	                if (breakCaseRadio.getValue() != null) {
	                    kitVersionCode.setValue("");
	                    kitVersionCode.setReadOnly(false);
	                    plantBreakCaseFlg.setValue(false);
	                    plantBreakCaseFlg.setReadOnly(true);
	                    pbcVersion.setValue("");
	                    pbcVersion.setReadOnly(true);
	                }
	            }
	        });
        }

        plantBreakCaseRadio.setBoxLabel("Plant Break Case");
        plantBreakCaseRadio.setWidth(175);
        if (!displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)) {
	        plantBreakCaseRadio.addListener(Events.OnClick, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent be) {

	                if (plantBreakCaseRadio.getValue() != null) {
	                	String s = componentSku.getValue();
	                	if (s == null) s = kitComponentModel.getVersionNo();
	                	if (s.length()==15) s = s.substring(10);
	                    kitVersionCode.setValue(s);
	                    kitVersionCode.setReadOnly(true);
	                    plantBreakCaseFlg.setReadOnly(false);
	                    pbcVersion.setReadOnly(false);
	                }
	            }
	        });
        }

        radioGrp.add(productionVersionRadio);
        radioGrp.add(breakCaseRadio);
        radioGrp.add(plantBreakCaseRadio);
        radioGrp.setOrientation(Orientation.VERTICAL);
      
     

        LabelField kitVersionCodeLabel = new LabelField("Kit Version Code:");
        kitVersionCodeLabel.setWidth(110);
        kvhp1.add(kitVersionCodeLabel);
        kitVersionCode.setFieldLabel("Kit Version Code");
        kitVersionCode.setAllowBlank(true);
        kitVersionCode.setMaxLength(5);
        kitVersionCode.setWidth(100);
        kitVersionCode.addListener(Events.OnBlur, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
            	String s = kitVersionCode.getValue();
            	if (s != null) {
            		if (s.length() < 5) {
            			while (s.length() < 5) s = "0" + s;
            			kitVersionCode.setValue(s);
            		}
            	}
            	
            }
        });
        kvhp1.add(kitVersionCode);

        kvhp2.setHeight(28);

        plantBreakCaseFlg.setBoxLabel("Plant Break Case BOM");
        plantBreakCaseFlg.setWidth(180);
        kvhp3.add(plantBreakCaseFlg);

        LabelField pbcVersionCodeLabel = new LabelField("PBC Version:");
        pbcVersionCodeLabel.setWidth(90);
        kvhp3.add(pbcVersionCodeLabel);

        pbcVersion.setFieldLabel("PBC Version");
        pbcVersion.setAllowBlank(true);
        pbcVersion.setMaxLength(5);
        pbcVersion.setWidth(100);
        pbcVersion.addListener(Events.OnBlur, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
            	String s = pbcVersion.getValue();
            	if (s != null) {
            		if (s.length() < 5) {
            			while (s.length() < 5) s = "0" + s;
            			pbcVersion.setValue(s);
            		}
            	}
            	
            }
        });
        kvhp3.add(pbcVersion);

        kvvp2.add(kvhp1);
        kvvp2.add(kvhp2);
        kvvp2.add(kvhp3);
        kvcp.add(radioGrp, new ColumnData(175));
        kvcp.add(kvvp2, new ColumnData(325));
        kitVersionCodeGrp.add(kvcp);

        // finished goods fields       
        ContentPanel fgCt = new ContentPanel();
        fgCt.setBodyBorder(false);
        fgCt.setHeaderVisible(false);
        fgCt.setLayout(new RowLayout());
        fgCt.setSize(240, 90);
        HorizontalPanel fghp1 = new HorizontalPanel();
        HorizontalPanel fghp2 = new HorizontalPanel();
        HorizontalPanel fghp3 = new HorizontalPanel();
        fghp1.setSpacing(2);
        fghp2.setSpacing(2);
        fghp3.setSpacing(2);
        fgCt.add(fghp1);
        fgCt.add(fghp2);
        fgCt.add(fghp3);

        LabelField finishedGoodsSkuLabel = new LabelField("Consists of SKU:");
        finishedGoodsSkuLabel.setWidth(100);
        fghp1.add(finishedGoodsSkuLabel);
        finishedGoodsSku.setFieldLabel("Consists of SKU");
        finishedGoodsSku.setAllowBlank(true);
        finishedGoodsSku.setMaxLength(15);
        finishedGoodsSku.setWidth(120);
        finishedGoodsSku.addListener(Events.KeyUp, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
            	 if (finishedGoodsSku.getValue() != null) {
                 	if(displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)){
                     	String s = finishedGoodsSku.getValue();
                     	if (s == null) s = finishedGoodsSku.getValue();
                     	if (s != null && s.length()==15){ 
                     		kitComponentModel.setFinishedGoodsSku(s);
                     		costGrid.updateCostForFGSKU(kitComponentModel);
                     	}
                     }
                 }
            }
        });
        finishedGoodsSku.addListener(Events.OnBlur, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
            	 if (finishedGoodsSku.getValue() != null) {
                 	if(displayModel.getDisplaySetupId().equals(DISPLAY_SETUP_FG_NEW_UPC)){
                     	String s = finishedGoodsSku.getValue();
                     	if (s == null) s = finishedGoodsSku.getValue();
                     	if (s != null && s.length()==15){ 
                     		kitComponentModel.setFinishedGoodsSku(s);
                     		costGrid.updateCostForFGSKU(kitComponentModel);
                     	}
                     }
                 }
            }
        });
        fghp1.add(finishedGoodsSku);
        
        LabelField finishedGoodsQtyLabel = new LabelField("Qty:");
        finishedGoodsQtyLabel.setWidth(100);
        fghp2.add(finishedGoodsQtyLabel);
        finishedGoodsQty.setFieldLabel("Qty");
        finishedGoodsQty.setPropertyEditorType(Long.class);
        finishedGoodsQty.setAllowBlank(true);
        finishedGoodsQty.setWidth(60);
        fghp2.add(finishedGoodsQty);

        LabelField finishedGoodsLabel = new LabelField("* Required for Finished Goods - New UPC Displays");
        finishedGoodsLabel.setStyleAttribute("font-size", "11px");
        fghp3.add(finishedGoodsLabel);

        finishedGoodsGrp.add(fgCt);

        // planner grid
        plannerGrid = new KitComponentInfoPlannerGrid(kitComponentModel, displayModel, readOnly);
        plannerGrp.add(plannerGrid);

        // add buttons
        formPanel.addButton(btnPrevious);
        formPanel.addButton(btnNext);
        formPanel.addButton(btnReturnToSelection);
        formPanel.addButton(btnReturn);
        formPanel.addButton(btnSave);
        
        vp.add(formPanel);

    }

    private void messageBox(String title, String msg) {
        Listener<MessageBoxEvent> cb;
        cb = new Listener<MessageBoxEvent>() {
            public void handleEvent(MessageBoxEvent mbe) {

            }
        };
        MessageBox.alert(title, msg, cb);
    }
    private boolean overrideDirty = false;

    public void overrideDirty() {
        this.overrideDirty = true;
    }

    public void save(Button btn, TabPanel panel, TabItem item) {
    }

    public boolean isDirty() {
    if(overrideDirty==true) return false;        
        return (!kitComponentModel.equalsKitComponentDetailControlModel(getControlModel()) || (plannerGrid.getStore().getModifiedRecords().size()>0) || plannerGrid.isKitComponentInfoPlannerGridModified() );
    }
    private KitComponentDTO getControlModel(){
        KitComponentDTO controlModel = new KitComponentDTO();
        if (componentSku.getValue() != null) controlModel.setSku(componentSku.getValue());
        if (description.getValue() != null) controlModel.setProductName(description.getValue());
        if (buDesc.getValue() != null) controlModel.setBuDesc(buDesc.getValue().getBuDesc());
        if (regCasePack.getValue()!=null) controlModel.setRegCasePack(regCasePack.getValue().longValue());
        if(promoFlg.isDirty() || kitComponentModel.isPromoFlg()!=null) controlModel.setPromoFlg(promoFlg.getValue());
        if (i2ForecastFlg.isDirty() || kitComponentModel.isI2ForecastFlg()!=null) controlModel.setI2ForecastFlg(i2ForecastFlg.getValue());
        if(qtyPerFacing.getValue()!=null) controlModel.setQtyPerFacing(qtyPerFacing.getValue().longValue());
        if(numFacings.getValue()!=null) controlModel.setNumberFacings(numFacings.getValue().longValue());
        if(productionVersionRadio.isDirty() || breakCaseRadio.isDirty() || plantBreakCaseRadio.isDirty() || kitComponentModel.getBreakCase()!=null){
            if(productionVersionRadio.getValue()) controlModel.setBreakCase(PRODUCTION_VERSION);
            else if(breakCaseRadio.getValue()) controlModel.setBreakCase(BREAK_CASE);
            else if(plantBreakCaseRadio.getValue()) controlModel.setBreakCase(PLANT_BREAK_CASE);
        }
        if(kitComponentModel.getSkuId()!=null) {
        	controlModel.setSkuId(kitComponentModel.getSkuId());
        }
        if (plantBreakCaseFlg.isDirty() || kitComponentModel.isPlantBreakCaseFlg()!=null) controlModel.setPlantBreakCaseFlg(plantBreakCaseFlg.getValue());
        if (kitVersionCode.getValue() != null) controlModel.setKitVersionCode(kitVersionCode.getValue());
        if (pbcVersion.getValue() != null) controlModel.setPbcVersion(pbcVersion.getValue());
        if (finishedGoodsSku.getValue() != null) controlModel.setFinishedGoodsSku(finishedGoodsSku.getValue());
        if(finishedGoodsQty.getValue() != null) controlModel.setFinishedGoodsQty(finishedGoodsQty.getValue().longValue());
        if(kitComponentModel.isSourcePimFlg()!=null || srcPimFlg.isDirty()){
            controlModel.setSourcePimFlg(srcPimFlg.getValue());
        }
        if(inPogYn.isDirty() || inPogYn.getValue()!=null) controlModel.setInPogYn(inPogYn.getValue().getInPogYorNId()); 
        if(excessInvtPct.isDirty() || excessInvtPct.getValue()!=null) controlModel.setExcessInvtPct(excessInvtPct.getValue().floatValue());
        return controlModel;
    }
    public KitComponentDetail getKitComponentDetail(){
    	return this;
    }
}
