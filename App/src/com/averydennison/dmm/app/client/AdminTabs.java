package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.admin.CorrugateVendors;
import com.averydennison.dmm.app.client.admin.Customers;
import com.averydennison.dmm.app.client.admin.PackoutLocations;
import com.averydennison.dmm.app.client.admin.StructureTypes;
import com.averydennison.dmm.app.client.admin.UserTypes;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.LocationCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Timer;

public class AdminTabs extends LayoutContainer {
	public static final String CORRUGATE_VENDOR = "Corrugate Vendor";
	public static final String CUSTOMER = "Customer";
	public static final String PACKOUT_LOCATION = "Packout Location";
	public static final String STRUCTURE_TYPE = "Structure Type";
	public static final String USER_TYPE = "User Type";
	
	private TabPanel panel = new TabPanel();
	private TabItem corrugateVendorTab = new TabItem(CORRUGATE_VENDOR);
	private TabItem customerTab = new TabItem(CUSTOMER);
	private TabItem packoutLocationTab = new TabItem(PACKOUT_LOCATION);
	private TabItem structureTypeTab = new TabItem(STRUCTURE_TYPE);
	private TabItem userTypeTab = new TabItem(USER_TYPE);
	
	CorrugateVendors corrugateVendor = new CorrugateVendors();
	Customers customer = new Customers();
	PackoutLocations packoutLocation = new PackoutLocations();
	StructureTypes structureType = new StructureTypes();
	UserTypes userType = new UserTypes();

	public AdminTabs() {
		setup();
		setPermissions();
	}

	private void setPermissions() {
	}

	private void setup() {
		System.out.println("AdminTabs:setup starts");
		VerticalPanel vp = new VerticalPanel();
		vp.setSpacing(10);
		panel.setPlain(true);
		panel.setAutoSelect(true);
		panel.setSize(1230, 550);

		corrugateVendorTab.add(corrugateVendor);
		corrugateVendorTab.addListener(Events.Select, new Listener<BaseEvent>() {
			public void handleEvent(BaseEvent be) {
				final MessageBox box = MessageBox.wait("Progress",
						"Loading corrugate vendors, please wait...",
						"Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}
				};
				t.schedule(500);
				AppService.App.getInstance().getAllManufacturerCountryDTOs(new Service.LoadAdminData<ManufacturerCountryDTO>(corrugateVendor));
			}
		});
		panel.add(corrugateVendorTab);

		customerTab.add(customer);
		customerTab.addListener(Events.Select, new Listener<BaseEvent>() {
			public void handleEvent(BaseEvent be) {
				final MessageBox box = MessageBox.wait("Progress",
						"Loading customers, please wait...",
						"Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}
				};
				t.schedule(500);
				AppService.App.getInstance().getAllCustomerDTOs(new Service.LoadAdminData<CustomerDTO>(customer));
			}
		});		
		panel.add(customerTab);

		packoutLocationTab.add(packoutLocation);
		packoutLocationTab.addListener(Events.Select, new Listener<BaseEvent>() {
			public void handleEvent(BaseEvent be) {
				final MessageBox box = MessageBox.wait("Progress",
						"Loading packout locations, please wait...",
						"Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}
				};
				t.schedule(500);
				AppService.App.getInstance().getAllLocationCountryDTOs(new Service.LoadAdminData<LocationCountryDTO>(packoutLocation));
			}
		});		
		panel.add(packoutLocationTab);
		
		structureTypeTab.add(structureType);
		structureTypeTab.addListener(Events.Select, new Listener<BaseEvent>() {
			public void handleEvent(BaseEvent be) {
				final MessageBox box = MessageBox.wait("Progress",
						"Loading structure types, please wait...",
						"Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}
				};
				t.schedule(500);
				AppService.App.getInstance().getAllStructureDTOs(new Service.LoadAdminData<StructureDTO>(structureType));
			}
		});		
		panel.add(structureTypeTab);
		
		userTypeTab.add(userType);
		userTypeTab.addListener(Events.Select, new Listener<BaseEvent>() {
			public void handleEvent(BaseEvent be) {
				final MessageBox box = MessageBox.wait("Progress",
						"Loading user types, please wait...",
						"Loading...");
				Timer t = new Timer() {
					public void run() {
						box.close();
					}
				};
				t.schedule(500);
				AppService.App.getInstance().getAllDisplaySalesRepDTOs(new Service.LoadAdminData<DisplaySalesRepDTO>(userType));
			}
		});		
		panel.add(userTypeTab);
		
		vp.add(panel);
		add(vp);
	}
	
	@Override
	protected void onRender(Element parent, int index) {
		super.onRender(parent, index);
		panel.setSelection(corrugateVendorTab);
	}

}