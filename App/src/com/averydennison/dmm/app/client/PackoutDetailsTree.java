package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.averydennison.dmm.app.client.models.DisplayDcDTO;
import com.averydennison.dmm.app.client.models.DisplayPackoutDTO;
import com.averydennison.dmm.app.client.models.DisplayShipWaveDTO;
import com.averydennison.dmm.app.client.models.LocationDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;

/**
 * User: Spart Arguello
 * Date: Nov 20, 2009
 * Time: 1:19:28 PM
 */
public class PackoutDetailsTree extends LayoutContainer implements ValidationHelper {
    public static String ADD = "Add";
    public static String DELETE = "Delete";

    private Tree tree;

    private SelectionListener listener = new SelectionListener<ComponentEvent>() {
        public void componentSelected(ComponentEvent ce) {
            Button btn = (Button) ce.getComponent();
            if (btn.getText().equals(ADD)) {
                DCNode dcNode = new DCNode(getLocationList());
                TreeItem item = tree.addItem(dcNode);
                item.setState(false, false);
                //todo uncomment
//                TreeItem child = new TreeItem(new PackoutDetailsTreeItemGrid(dcNode));
//                item.addItem(child);
            } else if (btn.getText().equals(DELETE)) {
                //TreeItem item = tree.getSelectedItem();
                //TreeItem child = item.getChild(0);
//                PackoutDetailsTreeItemGrid g = (PackoutDetailsTreeItemGrid) child.getWidget();
//                child.removeItems();
                ////Info.display(item.toString(), child.toString());
            }
        }
    };

    private PackoutDetailInfo packoutDetailInfo;

    public PackoutDetailsTree(PackoutDetailInfo packoutDetailInfo) {
        this.packoutDetailInfo = packoutDetailInfo;
        AppService.App.getInstance().loadLocationDTOs(new Service.LoadLocationList(this));
    }


    @Override
    protected void onRender(Element parent, int index) {
        super.onRender(parent, index);

        setLayout(new FlowLayout(10));

        if (tree == null) {
            tree = new Tree();
        }
//        tree.addOpenHandler(new OpenHandler<TreeItem>() {
//            public void onOpen(OpenEvent<TreeItem> treeItemOpenEvent) {
//                if (packoutDetailInfo.packoutDetailInfoShippingWavesGrid.getStore().getCount() > 0) {
//                    TreeItem treeItem = treeItemOpenEvent.getTarget();
//                    TreeItem child = treeItem.getChild(0);
//                    if (child.getWidget() instanceof PackoutDetailsTreeItemGrid) {
//                        PackoutDetailsTreeItemGrid grid = (PackoutDetailsTreeItemGrid) child.getWidget();
//                        if (grid == null) {
//                            treeItemOpenEvent.getTarget().setState(false);
//                            return;
//                        }
//                        if (grid.getDcNode() == null) {
//                            treeItemOpenEvent.getTarget().setState(false);
//                            return;
//                        }
//                        if (grid.getDcNode().getDcModel() == null) {
//                            treeItemOpenEvent.getTarget().setState(false);
//                            return;
//                        }
//                        if (grid.getDcNode().getDcModel().getDisplayDcId() == null) {
//                            treeItemOpenEvent.getTarget().setState(false);
//                            return;
//                        }
//
//                        if (grid != null && grid.getDcNode() != null && grid.getDcNode().getDcModel().getDisplayDcId() != null) {
//                            treeItemOpenEvent.getTarget().setState(true);
//                            grid.getEditorGrid().stopEditing();
//                            AppService.App.getInstance().
//                                    loadDisplayPackoutList(grid.getDcNode().getDcModel(),
//                                            new Service.LoadDisplayPackoutDTOList(grid));
//                        }
//
//                    }
//                } else {
//                    treeItemOpenEvent.getTarget().setState(false);
//                }
//            }
//        });
//        tree.addCloseHandler(new CloseHandler<TreeItem>() {
//            public void onClose(CloseEvent<TreeItem> treeItemCloseEvent) {
//                TreeItem treeItem = treeItemCloseEvent.getTarget();
//                TreeItem child = treeItem.getChild(0);
//                if (child.getWidget() instanceof PackoutDetailsTreeItemGrid) {
//                    PackoutDetailsTreeItemGrid grid = (PackoutDetailsTreeItemGrid) child.getWidget();
//                    grid.getEditorGrid().stopEditing();
//                    //grid.getStore().commitChanges();
//                }
//            }
//        });
        ContentPanel cp = new ContentPanel();
        cp.setHeaderVisible(false);
        cp.setBodyBorder(false);
        cp.setButtonAlign(Style.HorizontalAlignment.CENTER);
        cp.setLayout(new FitLayout());
        cp.setFrame(true);
        cp.setSize(500, 400);
        ScrollPanel sp = new ScrollPanel(tree);
        cp.addButton(new Button("Add", listener));
        //cp.addButton(new Button("Delete", listener));
        cp.add(sp);

        add(cp);
    }

    public void reload() {

        if (packoutDetailInfo.getDisplayTabs().getDisplayModel() != null &&
                packoutDetailInfo.getDisplayTabs().getDisplayModel().getDisplayId() != null) {
            AppService.App.getInstance().findDisplayDcByDisplay(
                    packoutDetailInfo.getDisplayTabs().getDisplayModel(),
                    new Service.LoadDisplayDcDTO(packoutDetailInfo, packoutDetailInfo.getDcMap()));
        }
    }

    public void loadDCNodes() {
        if (packoutDetailInfo.getDcMap().size() > 0) {
            if (tree.treeItemIterator().hasNext()) tree.removeItems();
            tree.setFocus(true);
            Iterator<Long> ki = packoutDetailInfo.getDcMap().keySet().iterator();
            while (ki.hasNext()) {
                DCNode node = new DCNode(getLocationList());
                DisplayDcDTO dcModel = packoutDetailInfo.getDcMap().get(ki.next());
                node.setDcModel(dcModel);
                LocationDTO location = this.getLocationMap().get(dcModel.getLocationId());
                node.setLocation(location);
               
                //tree.addItem(node);

                TreeItem parent = tree.addItem(node);
                PackoutDetailsTreeItemGrid grid = new PackoutDetailsTreeItemGrid(node);
                TreeItem child = new TreeItem(grid);
                parent.addItem(child);

                grid.getEditorGrid().stopEditing();
                AppService.App.getInstance().
                        loadDisplayPackoutList(grid.getDcNode().getDcModel(),
                                new Service.LoadDisplayPackoutDTOList(grid));
//                parent.setState(true);
            }
        }
    }

    public boolean validateControls() {
        //just making sure the DC Nodes have the right kind of data
        Iterator<TreeItem> i = tree.treeItemIterator();

        while (i.hasNext()) {
            TreeItem ti = i.next();
            if (ti.getWidget() instanceof DCNode) {
                DCNode dcNode = (DCNode) ti.getWidget();
                if (!dcNode.validateControls()) return false;
            }//todo check packout detail
        }
        return true;
    }

    public Long getSumOfAllDCNodes() {

        Iterator<TreeItem> i = tree.treeItemIterator();
        Long sum = new Long(0);
        while (i.hasNext()) {
            TreeItem ti = i.next();
            if (ti.getWidget() instanceof DCNode) {
                DCNode dcNode = (DCNode) ti.getWidget();
                //dcNode.disableControls();
                //assuming the DC quantity nodes entered reasonable quantitiies based on forecast quantity max
                sum = sum + dcNode.getQty();
            }
        }
        return sum;
    }

    public void saveDCNodes() {
        Iterator<TreeItem> i = tree.treeItemIterator();
        List<DisplayDcDTO> list = new ArrayList<DisplayDcDTO>();
        while (i.hasNext()) {
            TreeItem ti = i.next();
            if (ti.getWidget() instanceof DCNode) {
                DCNode node = (DCNode) ti.getWidget();
                if (node.validateControls()) {
                    if (node.getDcModel() == null) {
                        DisplayDcDTO dto = new DisplayDcDTO();
                        dto.setDisplayId(packoutDetailInfo.getDisplayTabs().getDisplayModel().getDisplayId());
                        dto.setLocationId(node.getLocationId());
                        dto.setQuantity(node.getQty());
                        list.add(dto);
                    } else {
                        DisplayDcDTO dto = node.getDcModel();
                        dto.setLocationId(node.getLocationId());
                        dto.setQuantity(node.getQty());
                        list.add(dto);
                    }
                }
            }
        }
        final MessageBox box = MessageBox.wait("Progress",
                "Saving DC data, please wait...", "Saving...");

        AppService.App.getInstance().saveDisplayDcDTOList(list,
                new Service.SaveDisplayDcDTOList(packoutDetailInfo, packoutDetailInfo.getDcMap(), box));
    }

    private List<LocationDTO> locationList;

    public void setLocationList(List<LocationDTO> list) {
        this.locationList = list;
    }

    public List<LocationDTO> getLocationList() {
        return locationList;
    }

    private Map<Long, LocationDTO> locationMap;

    public Map<Long, LocationDTO> getLocationMap() {
        return locationMap;
    }

    public void setLocationMap(Map<Long, LocationDTO> locationMap) {
        this.locationMap = locationMap;
    }

    public void saveDisplayPackouts() {
        Iterator<TreeItem> i = tree.treeItemIterator();
//        List<DisplayPackoutDTO> list = new ArrayList<DisplayPackoutDTO>();
        while (i.hasNext()) {
            TreeItem ti = i.next();
            if (ti.getWidget() instanceof PackoutDetailsTreeItemGrid) {
                PackoutDetailsTreeItemGrid node = (PackoutDetailsTreeItemGrid) ti.getWidget();
                if (node.validateControls()) {
                    if (node.getStore().getModels().size() > 0) {
//                        list.addAll(node.getStore().getModels());
                        if (validateData(node.getStore().getModels())) {
                            final MessageBox box = MessageBox.wait("Progress",
                                    "Saving packout details data, please wait...", "Saving...");
                            AppService.App.getInstance().saveDisplayPackoutDTOList(node.getStore().getModels(),
                                    new Service.SaveDisplayPackoutDTOList(node, box));
                        }
                    }
                }
            }
        }
//        for (int j = 0; j < list.size(); j++) {
//            //Info.display("", list.get(j).getDisplayDcId().toString());
//        }

//        final MessageBox box = MessageBox.wait("Progress",
//                "Saving DC data, please wait...", "Saving...");

//        AppService.App.getInstance().saveDisplayPackoutDTOList(list,
//                new Service.SaveDisplayPackoutDTOList(packoutDetailInfo));
//        if (list.size() > 0) {
//
//        }
    }

    private boolean validateData(List<DisplayPackoutDTO> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDisplayShipWaveId() == null || list.get(i).getDisplayDcId() == null) {
                Window.alert(list.get(i).toString());
                return false;
            }
        }
        return true;
    }

    public boolean compareDCNodeTotalQtyAgainstPackoutDetailsQty() {
        boolean value = false;
        Iterator<TreeItem> i = tree.treeItemIterator();
        while (i.hasNext()) {
            TreeItem ti = i.next();
            if (ti.getWidget() instanceof PackoutDetailsTreeItemGrid) {
                PackoutDetailsTreeItemGrid singlePackoutDetailsGrid = (PackoutDetailsTreeItemGrid) ti.getWidget();
                Long packoutDetailsTotalQty = singlePackoutDetailsGrid.getQty();
                if (singlePackoutDetailsGrid.getStore().getModels().size() == 0) value = true;
                else if (!packoutDetailsTotalQty.equals(singlePackoutDetailsGrid.getDcNode().getQty())) return false;
            }
        }
        if (value == true) return true;
        return true;
    }

    public boolean crossValidatePackoutDetailsVsShipWaves() {
        Map<Long, DisplayShipWaveDTO> shipWaveMap = new HashMap<Long, DisplayShipWaveDTO>();
        Map<Long, Long> currentTotalQtyMap = new HashMap<Long, Long>();
        List<DisplayShipWaveDTO> list =
                packoutDetailInfo.packoutDetailInfoShippingWavesGrid.getStore().getModels();
        for (int index = 0; index < list.size(); index++) {
            shipWaveMap.put(list.get(index).getDisplayShipWaveId(), list.get(index));
        }

        Iterator<TreeItem> treeItemIterator = tree.treeItemIterator();
        while (treeItemIterator.hasNext()) {
            TreeItem ti = treeItemIterator.next();
            if (ti.getWidget() instanceof PackoutDetailsTreeItemGrid) {
                PackoutDetailsTreeItemGrid singlePackoutDetailsGrid = (PackoutDetailsTreeItemGrid) ti.getWidget();
                List<DisplayPackoutDTO> packoutList = singlePackoutDetailsGrid.getStore().getModels();
                for (int i = 0; i < packoutList.size(); i++) {
                    DisplayPackoutDTO packout = packoutList.get(i);

                    Long qty = 0L;
                    if (currentTotalQtyMap.get(packout.getDisplayShipWaveId()) == null) {
                        qty = packout.getQuantity();
                    } else if (currentTotalQtyMap.get(packout.getDisplayShipWaveId()) != null) {
                        qty = currentTotalQtyMap.get(packout.getDisplayShipWaveId()) + packout.getQuantity();
                    }
                    currentTotalQtyMap.put(packout.getDisplayShipWaveId(), qty);
                }
            }
        }
        //all or none final check on client side
        Iterator<Long> ki = currentTotalQtyMap.keySet().iterator();
        while (ki.hasNext()) {
            Long key = ki.next();
            if (!shipWaveMap.get(key).getQuantity().equals(currentTotalQtyMap.get(key))) return false;
        }
        return true;
    }
}