package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.VerticalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.google.gwt.user.client.Element;

/**
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 11:13:56 AM
 * TODO - externalize labelling and titling
 */
public class LoginForm extends LayoutContainer {

    private VerticalPanel vp;
    private FormData formData = new FormData("-20");

    private TextField<String> username = new TextField<String>();
    private TextField<String> password = new TextField<String>();

    public LoginForm() {
        vp = new VerticalPanel();
        vp.setSpacing(10);
    }

    public String getUsername() {
        return username.getValue();
    }

    public String getPassword() {
        return password.getValue();
    }

    @Override
    protected void onRender(Element element, int i) {
        super.onRender(element, i);
        createForm();
        add(vp);
    }

    private void createForm() {
        FormPanel fp = new FormPanel();
        //fp.setIconStyle("icon-form");//TODO get 16px x 16px
        fp.setHeading(App.IMAGE_STRING + "&nbsp;&nbsp;Display Marketing System");
        //fp.setHeading("Display Marketing System");
        fp.setFrame(true);
        fp.setWidth(350);

        username.setFieldLabel("User Name");
        username.setEmptyText("Enter your user name");
        username.setAllowBlank(false);
        username.setMinLength(4);
        username.setMaxLength(40);
        fp.add(username, formData);


        password.setFieldLabel("Password");
        password.setEmptyText("Enter your password");
        password.setAllowBlank(true); //for now?
        password.setPassword(true);
        password.setMinLength(4);
        password.setMaxLength(40);
        fp.add(password, formData);

        Button b = new Button("Login");
        SelectionListener listener = new SelectionListener<ComponentEvent>() {
            public void componentSelected(ComponentEvent ce) {
                Button btn = (Button) ce.getComponent();
                final String x = getUsername();
                final String y = getPassword();

                AppServiceAsync callback = AppService.App.getInstance();

                ////Info.display(x, y);
                ////Info.display("Click Event", "The '{0}' button was clicked.", btn.getText());
                //TODO call spring security through here
            }
        };
        b.addSelectionListener(listener);
        b.setEnabled(true);

        fp.addButton(b);

        vp.add(fp);
    }

}
