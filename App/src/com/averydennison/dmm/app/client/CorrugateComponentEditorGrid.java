package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.List;

import com.averydennison.dmm.app.client.models.CorrugateComponentsDetailDTO;
import com.averydennison.dmm.app.client.models.CostAnalysisKitComponentDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.ManufacturerDTO;
import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.util.Constants;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.Orientation;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.grid.AggregationRenderer;
import com.extjs.gxt.ui.client.widget.grid.AggregationRowConfig;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.SummaryType;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.google.gwt.i18n.client.NumberFormat;

/**
 * User: Mari Date: Jan 04, 2011 Time: 11:25:16 AM
 * 
 * @param <E>
 */
public class CorrugateComponentEditorGrid extends LayoutContainer implements
		DirtyInterface {
	private Button btnAddComp;
	private Button btnDeleteComp;
	private Button btnCopyComp;
	private Button btnSaveCorr;
	private Button btnReturnSelection;
	private ComboBox<CountryDTO> countryCombo = new ComboBox<CountryDTO>();
	private ListStore<CountryDTO> countries;
	private ComboBox<ManufacturerCountryDTO> manufacturerCountryCombo = new ComboBox<ManufacturerCountryDTO>();
	private ListStore<ManufacturerCountryDTO> manufacturersCountry;
	private Grid<CorrugateComponentsDetailDTO> componentGrid;
	private DisplayDTO displayDTO;
	private static final ListStore<CorrugateComponentsDetailDTO> corrCompStore = new ListStore<CorrugateComponentsDetailDTO>();
	GridSelectionModel<CorrugateComponentsDetailDTO> gm;
	CustomCorrRowEditor<CorrugateComponentsDetailDTO> re;
	CorrugateInfoGroup corrugateInfoGroup;

	public DisplayDTO getDisplayDTO() {
		return displayDTO;
	}

	public void setDisplayDTO(DisplayDTO displayDTO) {
		this.displayDTO = displayDTO;
	}

	public CorrugateInfoGroup getCorrugateInfoGroup() {
		return corrugateInfoGroup;
	}

	public void setCorrugateInfoGroup(CorrugateInfoGroup corrugateInfoGroup) {
		this.corrugateInfoGroup = corrugateInfoGroup;
	}

	public void enableDisableButtons(){
		btnAddComp.disable();
		btnDeleteComp.disable();
		btnCopyComp.disable();
		btnSaveCorr.disable();	
	}

	public ContentPanel createForm(Boolean readOnly) {
		final NumberFormat number = NumberFormat.getFormat("0.00");
		int x = 150;
		overrideDirty = false;
		manufacturersCountry = new ListStore<ManufacturerCountryDTO>();
		AppService.App.getInstance().getManufacturerCountryDTOs(
				new Service.SetManufacturersCountryStore(getDisplayDTO().getCountryId(),manufacturersCountry));
		manufacturerCountryCombo = new ComboBox<ManufacturerCountryDTO>();
		manufacturerCountryCombo.setEditable(false);
		manufacturerCountryCombo.setWidth(x);
		manufacturerCountryCombo.setDisplayField(ManufacturerCountryDTO.NAME);
		manufacturerCountryCombo.setValueField(ManufacturerCountryDTO.NAME);
		manufacturerCountryCombo.setStore(manufacturersCountry);
		manufacturerCountryCombo.setTriggerAction(ComboBox.TriggerAction.ALL);
		CellEditor vendorEditor = new CellEditor(manufacturerCountryCombo) {
			@Override
			public Object preProcessValue(Object value) {
				if (value == null) {
					return value;
				}
				String valueAttr = ((ComboBox<ManufacturerCountryDTO>) manufacturerCountryCombo)
						.getValueField();
				if (valueAttr == null)
					valueAttr = "value";
				return ((ComboBox<ManufacturerCountryDTO>) manufacturerCountryCombo)
						.getStore().findModel(valueAttr, value);
			}

			@Override
			public Object postProcessValue(Object value) {
				if (value == null) {
					return value;
				}
				String valueAttr = ((ComboBox<ManufacturerCountryDTO>) manufacturerCountryCombo)
						.getValueField();
				if (valueAttr == null)
					valueAttr = "value";
				return ((ModelData) value).get(valueAttr);
			}
		};

		countries = new ListStore<CountryDTO>();
		AppService.App.getInstance().getCountryDTOs(
				new Service.SetCountryStore(countries));
		countryCombo = new ComboBox<CountryDTO>();
		countryCombo.setEditable(false);
		// battleFieldCombo.setAllowBlank(true);
		countryCombo.setWidth(x);
		countryCombo.setDisplayField(CountryDTO.COUNTRY_NAME);
		countryCombo.setValueField(CountryDTO.COUNTRY_NAME);
		countryCombo.setTriggerAction(TriggerAction.ALL);
		countryCombo.setStore(countries);
		CellEditor countryEditor = new CellEditor(countryCombo) {
			@Override
			public Object preProcessValue(Object value) {
				if (value == null) {
					return value;
				}
				String valueAttr = ((ComboBox<CountryDTO>) countryCombo)
						.getValueField();
				if (valueAttr == null)
					valueAttr = "value";
				return ((ComboBox<CountryDTO>) countryCombo).getStore()
						.findModel(valueAttr, value);
			}

			@Override
			public Object postProcessValue(Object value) {
				if (value == null) {
					return value;
				}
				String valueAttr = ((ComboBox<CountryDTO>) countryCombo)
						.getValueField();
				if (valueAttr == null)
					valueAttr = "value";
				return ((ModelData) value).get(valueAttr);
			}
		};
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
		ColumnConfig column = new ColumnConfig();
		column = new ColumnConfig();
		column.setSortable(true);
		column.setMenuDisabled(false);
		column.setEditor(vendorEditor);
		if(readOnly){
			manufacturerCountryCombo.disable();
			vendorEditor.disable();
		}
		column.setId(CorrugateComponentsDetailDTO.NAME);
		column.setHeader(Constants.VENDOR_NAME);
		column.setAlignment(HorizontalAlignment.LEFT);
		column.setWidth(x - 25);
		configs.add(column);

		column = new ColumnConfig();
		TextField<String> vendorPartNum = new TextField<String>();
		vendorPartNum.setMinLength(1);
		vendorPartNum.setMaxLength(50);
		if(readOnly){
			vendorPartNum.disable();
		}
		column.setEditor(new CellEditor(vendorPartNum));
		column.setId(CorrugateComponentsDetailDTO.VENDOR_PART_NUM);
		column.setHeader(Constants.VENDOR_PART_NO);
		column.setAlignment(HorizontalAlignment.LEFT);
		column.setWidth(x - 50);
		configs.add(column);
		
		column = new ColumnConfig();
		TextField<String> gloviaPartNum = new TextField<String>();
		if(readOnly){
			gloviaPartNum.disable();
		}
		gloviaPartNum.setMinLength(1);
		gloviaPartNum.setMaxLength(50);
		column.setEditor(new CellEditor(gloviaPartNum));
		column.setId(CorrugateComponentsDetailDTO.GLOVIA_PART_NUM);
		column.setHeader(Constants.GLOVIA_PART_NO);
		column.setAlignment(HorizontalAlignment.LEFT);
		column.setWidth(x - 50);
		configs.add(column);

		column = new ColumnConfig();
		column.setId(CorrugateComponentsDetailDTO.DESCRIPTION);
		column.setSortable(true);
		column.setMenuDisabled(false);
		TextField<String> textProjectName = new TextField<String>();
		if(readOnly){
			textProjectName.disable();
		}
		textProjectName.setMinLength(1);
		textProjectName.setMaxLength(500);
		column.setEditor(new CellEditor(textProjectName));
		column.setHeader(Constants.PART_DESCRIPTION);
		column.setAlignment(HorizontalAlignment.LEFT);
		column.setWidth(x + 20);
		configs.add(column);

		column = new ColumnConfig();
		column.setSortable(true);
		column.setMenuDisabled(false);
		TextField<String> textBoardTest = new TextField<String>();
		if(readOnly){
			textBoardTest.disable();
		}
		textBoardTest.setMinLength(1);
		textBoardTest.setMaxLength(30);
		column.setEditor(new CellEditor(textBoardTest));
		column.setId(CorrugateComponentsDetailDTO.BOARD_TEST);
		column.setHeader(Constants.BOARD_TEST);
		column.setAlignment(HorizontalAlignment.LEFT);
		column.setWidth(x - 50);
		configs.add(column);

		column = new ColumnConfig();
		column.setSortable(true);
		column.setMenuDisabled(false);
		TextField<String> textMaterial = new TextField<String>();
		if(readOnly){
			textMaterial.disable();
		}
		textMaterial.setMinLength(1);
		textMaterial.setMaxLength(30);
		column.setEditor(new CellEditor(textMaterial));
		column.setId(CorrugateComponentsDetailDTO.MATERIAL);
		column.setHeader(Constants.MATERIAL);
		column.setAlignment(HorizontalAlignment.LEFT);
		column.setWidth(x - 35);
		configs.add(column);

		column = new ColumnConfig();
		column.setSortable(true);
		column.setMenuDisabled(false);
		column.setEditor(countryEditor);
		if(readOnly){
			countryCombo.disable();
			countryEditor.disable();
		}
		column.setId(CorrugateComponentsDetailDTO.COUNTRY_NAME);
		column.setHeader(Constants.COUNTRY_OF_ORIGIN);
		column.setAlignment(HorizontalAlignment.LEFT);
		column.setWidth(x - 50);
		configs.add(column);

		column = new ColumnConfig();
		column.setSortable(true);
		column.setMenuDisabled(false);

		NumberField nQtyPerDisplay = new NumberField();
		nQtyPerDisplay.setPropertyEditorType(Long.class);
		nQtyPerDisplay.addInputStyleName("text-element-no-underline");
		nQtyPerDisplay.setMaxLength(10);
		CellEditor qtyPerDisplayEditor = new CellEditor(nQtyPerDisplay);
		if(readOnly){
			qtyPerDisplayEditor.disable();
			nQtyPerDisplay.disable();
		}
		column.setEditor(qtyPerDisplayEditor);
		column.setId(CorrugateComponentsDetailDTO.QTY_PER_DISPLAY);
		column.setHeader(Constants.NUMBER_PER_DISPLAY);
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x / 2);
		configs.add(column);

		column = new ColumnConfig();
		NumberField numberPiecePrice = new NumberField();
		numberPiecePrice.setPropertyEditorType(Double.class);
		numberPiecePrice.setMaxLength(12);
		numberPiecePrice.setAllowDecimals(true);
		numberPiecePrice.addInputStyleName("text-element-no-underline");
		CellEditor numberPiecePriceEditor = new CellEditor(numberPiecePrice);
		if(readOnly){
			numberPiecePrice.disable();
			numberPiecePriceEditor.disable();
		}
		column.setEditor(numberPiecePriceEditor);
		column.setId(CorrugateComponentsDetailDTO.PIECE_PRICE);
		column.setHeader(Constants.PIECE_PRICE);
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x / 2);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property) == null ? "0.00" : number
						.format(Double.parseDouble(model.get(property)
								.toString()));
			}
		});
		configs.add(column);

		column = new ColumnConfig();

		NumberField numberTotalCost = new NumberField();
		numberTotalCost.setPropertyEditorType(Double.class);
		numberTotalCost.setAllowDecimals(true);
		numberTotalCost.addInputStyleName("text-element-no-underline");
		CellEditor numberTotalCostEditor = new CellEditor(numberTotalCost);
		if(readOnly){
			numberTotalCost.disable();
			numberTotalCostEditor.disable();
		}
		numberTotalCost.disable();
		numberTotalCostEditor.disable();
		column.setEditor(numberTotalCostEditor);
		column.setId(CorrugateComponentsDetailDTO.TOTAL_COST);
		column.setHeader(Constants.TOTAL_COST);
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth((x / 2) + 10);
		column.setRenderer(new GridCellRenderer() {
			@Override
			public Object render(ModelData model, String property,
					com.extjs.gxt.ui.client.widget.grid.ColumnData config,
					int rowIndex, int colIndex, ListStore store, Grid grid) {
				return model.get(property) == null ? "0.00" : number
						.format(Double.parseDouble(model.get(property)
								.toString()));
			}
		});
		configs.add(column);

		column = new ColumnConfig();
		column.setSortable(true);
		column.setMenuDisabled(false);
		NumberField numberTotalQty = new NumberField();
		numberTotalQty.setPropertyEditorType(Long.class);
		numberTotalQty.addInputStyleName("text-element-no-underline");
		numberTotalQty.setMaxLength(10);
		CellEditor numberTotalQtyEditor = new CellEditor(numberTotalQty);
		if(readOnly){
			numberTotalQty.disable();
			numberTotalQtyEditor.disable();
		}
		numberTotalQty.disable();
		numberTotalQtyEditor.disable();
		column.setEditor(numberTotalQtyEditor);
		column.setId(CorrugateComponentsDetailDTO.TOTAL_QTY);
		column.setHeader(Constants.TOTAL_QUANTITY);
		column.setAlignment(HorizontalAlignment.RIGHT);
		column.setWidth(x / 2);
		configs.add(column);

		ColumnModel cm = new ColumnModel(configs);
		AggregationRowConfig<CorrugateComponentsDetailDTO> summary = new AggregationRowConfig<CorrugateComponentsDetailDTO>();
		summary.setHtml(CorrugateComponentsDetailDTO.COUNTRY_NAME,
				Constants.TOTAL);
		// with summary type and format for QTY_PER_DISPLAY
		summary.setSummaryType(CorrugateComponentsDetailDTO.QTY_PER_DISPLAY,
				SummaryType.SUM);
		summary.setSummaryFormat(CorrugateComponentsDetailDTO.QTY_PER_DISPLAY,
				NumberFormat.getFormat(("#######0")));
		summary.setRenderer(CorrugateComponentsDetailDTO.QTY_PER_DISPLAY,
				new AggregationRenderer<CorrugateComponentsDetailDTO>() {
					public Object render(Number value, int colIndex,
							Grid<CorrugateComponentsDetailDTO> grid,
							ListStore<CorrugateComponentsDetailDTO> store) {
						String result = "";
						if (value != null) {
							result = "<span style='font-weight:bold;' >"
									+ number.format(value.longValue())
									+ "</span>";
						}
						return result;
					}
				});
		// with summary type and format for TOTAL_COST
		summary.setSummaryType(CorrugateComponentsDetailDTO.TOTAL_COST,
				SummaryType.SUM);
		summary.setSummaryFormat(CorrugateComponentsDetailDTO.TOTAL_COST,
				NumberFormat.getFormat(("#######0.00")));
		summary.setRenderer(CorrugateComponentsDetailDTO.TOTAL_COST,
				new AggregationRenderer<CorrugateComponentsDetailDTO>() {
					public Object render(Number value, int colIndex,
							Grid<CorrugateComponentsDetailDTO> grid,
							ListStore<CorrugateComponentsDetailDTO> store) {
						String result = "";
						if (value != null) {
							result = "<span style='font-weight:bold;' >"
									+ number.format(value.doubleValue())
									+ "</span>";
						}
						return result;
					}
				});
		// with summary type and format for TOTAL_QTY
		summary.setSummaryType(CorrugateComponentsDetailDTO.TOTAL_QTY,
				SummaryType.SUM);
		summary.setSummaryFormat(CorrugateComponentsDetailDTO.TOTAL_QTY,
				NumberFormat.getFormat(("#######0")));
		summary.setRenderer(CorrugateComponentsDetailDTO.TOTAL_QTY,
				new AggregationRenderer<CorrugateComponentsDetailDTO>() {
					public Object render(Number value, int colIndex,
							Grid<CorrugateComponentsDetailDTO> grid,
							ListStore<CorrugateComponentsDetailDTO> store) {
						String result = "";
						if (value != null) {
							result = "<span style='font-weight:bold;' >"
									+ number.format(value.longValue())
									+ "</span>";
						}
						return result;
					}
				});
		cm.addAggregationRow(summary);
		loadCorrugateComponents();
		ContentPanel corrPanel = new ContentPanel();
		corrPanel.setBodyBorder(false);
		corrPanel.setLayout(new RowLayout(Orientation.VERTICAL));
		corrPanel.setFrame(false);
		corrPanel.setHeaderVisible(false);
		corrPanel.setSize(1130, 280);
		gm = new GridSelectionModel<CorrugateComponentsDetailDTO>();
		re = new CustomCorrRowEditor<CorrugateComponentsDetailDTO>(this);
		componentGrid = new Grid<CorrugateComponentsDetailDTO>(corrCompStore,
				cm);
		componentGrid.setColumnResize(true);
		// componentGrid.setSize(1130, 235);
		componentGrid.setSize(1130, 252);
		componentGrid.setStyleAttribute("borderTop", "none");
		componentGrid.setStripeRows(true);
		re.setBodyStyleName("x-row-editor-body");
		componentGrid.addPlugin(re);
		componentGrid.setSelectionModel(gm);
		componentGrid.setBorders(true);
		corrPanel.add(componentGrid);

		ButtonBar buttonBar = new ButtonBar();
		buttonBar.setAutoWidth(true);
		buttonBar.setSpacing(10);
		buttonBar.setStyleAttribute("paddingTop", "3px");
		btnAddComp = new Button(Constants.ADD_COMPONENT, listener);
		btnDeleteComp = new Button(Constants.DELETE_COMPONENT, listener);
		btnCopyComp = new Button(Constants.COPY_COMPONENT, listener);
		btnSaveCorr = new Button(Constants.SAVE_CORRUGATE, listener);
		btnReturnSelection = new Button(Constants.RETURN_TO_SELECTION, listener);
		buttonBar.add(btnAddComp);
		buttonBar.add(btnDeleteComp);
		buttonBar.add(btnCopyComp);
		buttonBar.add(btnSaveCorr);
		buttonBar.add(btnReturnSelection);
		buttonBar.setAlignment(HorizontalAlignment.CENTER);
		setPermissions();
		if(readOnly){
			enableDisableButtons();
		}
		corrPanel.add(buttonBar);
		manufacturerCountryCombo.addListener(Events.Expand, new Listener<FieldEvent>() {
	            public void handleEvent(FieldEvent ke) {
	            	if(getDisplayDTO().getCountryId()!=null){
	            		manufacturersCountry.filter(PromoPeriodCountryDTO.COUNTRY_NAME, Service.getCountryMap().get(getDisplayDTO().getCountryId()).getCountryName());
	            	}
	            	manufacturerCountryCombo.setEnabled(true);
	            }
	        });
		 
		return corrPanel;
	}
	
	SelectionListener listener = new SelectionListener<ComponentEvent>() {
		public void componentSelected(ComponentEvent ce) {
			Button button = (Button) ce.getComponent();
			if (displayDTO.getDisplayId() == null) {
				MessageBox.alert("Validation Error",
						"There is no display defined ", null);
				return;
			}
			if (button.getText().equals(Constants.ADD_COMPONENT)) {
				try {
					/*if (getCorrugateInfoGroup().getCorrugateDTO() != null
							&& getCorrugateInfoGroup().getCorrugateDTO()
									.getCorrugateId() != null) {
						CorrugateComponentsDetailDTO corrugateComponentsDetailDTO = new CorrugateComponentsDetailDTO();
						corrugateComponentsDetailDTO.setTotalCost(0.00);
						corrugateComponentsDetailDTO.setTotalQty(0L);
						re.stopEditing(false);
						corrCompStore.insert(corrugateComponentsDetailDTO,
								corrCompStore.getCount() == 0 ? 0
										: corrCompStore.getCount());
						re.startEditing(corrCompStore
								.indexOf(corrugateComponentsDetailDTO), true);
					} else {
						MessageBox.alert("Validation Error",
								"No associated corrugate. Please save the current corrugate ", null);
						return;
					}*/
					if (getCorrugateInfoGroup().getCorrugateDTO() == null
							|| getCorrugateInfoGroup().getCorrugateDTO()
									.getCorrugateId() == null) {
							save(button, null, null);
					}
					CorrugateComponentsDetailDTO corrugateComponentsDetailDTO = new CorrugateComponentsDetailDTO();
					corrugateComponentsDetailDTO.setTotalCost(0.00);
					corrugateComponentsDetailDTO.setTotalQty(0L);
					re.stopEditing(false);
					corrCompStore.insert(corrugateComponentsDetailDTO,
							corrCompStore.getCount() == 0 ? 0
									: corrCompStore.getCount());
					re.startEditing(corrCompStore
							.indexOf(corrugateComponentsDetailDTO), true);
					
				} catch (Exception ex) {
					System.out.println("Add...error.." + ex.getMessage());
				}
			} else if (button.getText().equals(Constants.DELETE_COMPONENT)) {
				if (componentGrid.getSelectionModel().getSelectedItem() != null) {
					String title = "Delete?";
					String msg = "This component will be deleted. Are you sure?";
					MessageBox box = new MessageBox();
					box.setButtons(MessageBox.YESNO);
					box.setIcon(MessageBox.QUESTION);
					box.addCallback(new Listener<MessageBoxEvent>() {
						public void handleEvent(MessageBoxEvent mbe) {
							Button btn = mbe.getButtonClicked();
							if (btn.getText().equals("Yes")) {
								deleteCorrComponent();
							}
							if (btn.getText().equals("No")) {
							}

						}
					});
					box.setTitle(title);
					box.setMessage(msg);
					box.show();
				} else {
					MessageBox
							.alert(
									"Validation Error",
									"Select Corrugate Component, Then press Delete Component",
									null);
					return;
				}
			} else if (button.getText().equals(Constants.COPY_COMPONENT)) {
				if (componentGrid.getSelectionModel().getSelectedItem() != null) {
					String title = "Copy?";
					String msg = "This component will be copied. Are you sure?";
					MessageBox box = new MessageBox();
					box.setButtons(MessageBox.YESNO);
					box.setIcon(MessageBox.QUESTION);
					box.addCallback(new Listener<MessageBoxEvent>() {
						public void handleEvent(MessageBoxEvent mbe) {
							Button btn = mbe.getButtonClicked();
							if (btn.getText().equals("Yes")) {
								copyCorrComponent(btn);
							}
							if (btn.getText().equals("No")) {
							}

						}
					});
					box.setTitle(title);
					box.setMessage(msg);
					box.show();
				} else {
					MessageBox
							.alert(
									"Validation Error",
									"Select Corrugate Component, Then press Copy Component",
									null);
					return;
				}
			} else if (button.getText().equals(Constants.SAVE_CORRUGATE)) {
				/*validateCorrCost();*/
				save(button, null, null);
			} else if (button.getText().equals(Constants.RETURN_TO_SELECTION)) {
				try {
					if (isDirty()) {
						String title = "You have unsaved information!";
						String msg = "Would you like to save?";
						MessageBox box = new MessageBox();
						box.setButtons(MessageBox.YESNOCANCEL);
						box.setIcon(MessageBox.QUESTION);
						box.setTitle("Save Changes?");
						box.addCallback(l);
						box.setTitle(title);
						box.setMessage(msg);
						box.show();
					} else {
						forwardToDisplayProjectSelection();
					}
				} catch (Exception ex) {
					System.out.println("Add...error.." + ex.getMessage());
				}
			}
		}
	};

	private CorrugateComponentEditorGrid getCorrugateComponentEditorGrid() {
		return this;
	}

	private void setPermissions() {
		if (!App.isAuthorized("SelectionGrid.CREATE"))
			btnAddComp.disable();
	}

	public boolean validateCorrCost() {
		Double corrugateCost = getCorrugateInfoGroup().corrugateCost.getValue() != null ? getCorrugateInfoGroup().corrugateCost
				.getValue().doubleValue()
				: new Double(0.00);
		;
		Double totalCost = calcCorrTotalCost();
		if (!corrugateCost.equals(totalCost)) {
			MessageBox
					.alert(
							"Corrugate Validation",
							"The Grand Total for Total price doesn't match the Corrugate Cost/Unit",
							null);
			return false;
		}
		return true;
	}

	public void loadCorrugateComponents() {
		MessageBox box = MessageBox.wait("Progress",
				"loading corrugate components, please wait...", "Loading...");
		AppService.App.getInstance().getCorrugateComponentInfo(
				displayDTO,
				new Service.LoadCorrugateComponentInfo(this, corrCompStore,
						box, displayDTO));
	}

	public Double calcCorrTotalCost() {
		System.out.println("DisplayCostDataGroup:calctotalCogsVal starts");
		Double corrTotCost = new Double(0.00);
		try {
			List<CorrugateComponentsDetailDTO> mods = corrCompStore.getModels();
			for (int i = 0; i < mods.size(); i++) {
				CorrugateComponentsDetailDTO itemProj = mods.get(i);
				corrTotCost += itemProj.getTotalCost();
			}
			System.out.println("DisplayCostDataGroup:calctotalCogsVal ends");
		} catch (Exception ex) {
			System.out
					.println("=======DisplayCostDataGroup:calctotalCogsVal error "
							+ ex.toString());
		}
		return corrTotCost;
	}

	public Boolean validateControls(List<Record> mods) {
		System.out.println("SelectionGrid:validateControls starts");
		for (int i = 0; i < mods.size(); i++) {
			CorrugateComponentsDetailDTO itemComp = (CorrugateComponentsDetailDTO) ((Record) mods
					.get(i)).getModel();
			corrCompStore.commitChanges();
			try {
				if (itemComp.getName() == null
						|| itemComp.getName().toString().length() == 0) {
					MessageBox
							.alert(
									"Corrugate Validation",
									"A component exists with a blank vendor name. Please enter vendor name.",
									null);
					return false;
				}
			} catch (Exception ex) {
				MessageBox.alert("Validation Error", "Exception occurred : "
						+ ex.toString(), null);
			}
		}
		System.out.println("SelectionGrid:validateControls ends");
		return true;
	}

	public void save(Button btn) {
		try {
			if (corrCompStore.getModifiedRecords().size() == 0) {
				MessageBox.alert("Validation Error",
						"No record has been modified", null);
				return;
			}
			List<Record> mods = corrCompStore.getModifiedRecords();
			if (!validateControls(mods))
				return;
			List<CorrugateComponentsDetailDTO> corrCompDTOList = new ArrayList<CorrugateComponentsDetailDTO>();
			CorrugateComponentsDetailDTO corrCompDTO;
			String valueAttr = null;
			try {
				for (int i = 0; i < mods.size(); i++) {
					corrCompDTO = (CorrugateComponentsDetailDTO) ((Record) mods
							.get(i)).getModel();
					valueAttr = ((ComboBox<ManufacturerCountryDTO>) manufacturerCountryCombo)
							.getValueField();
					if (corrCompDTO.getName() != null) {
						corrCompDTO
								.setManufacturerId(((ComboBox<ManufacturerCountryDTO>) manufacturerCountryCombo)
										.getStore().findModel(valueAttr,
												corrCompDTO.getName())
										.getManufacturerId());
					}
					System.out
							.println("CorrugateComponentEditorGrid:save ..[VendorName,VendorId]=..["
									+ corrCompDTO.getName()
									+ ", "
									+ corrCompDTO.getManufacturerId()
									+ ","
									+ displayDTO.getDisplayId() + "]");
					valueAttr = ((ComboBox<CountryDTO>) countryCombo)
							.getValueField();
					if (corrCompDTO.getCountryName() != null) {
						corrCompDTO
								.setCountryId(((ComboBox<CountryDTO>) countryCombo)
										.getStore().findModel(valueAttr,
												corrCompDTO.getCountryName())
										.getCountryId());
					}
					corrCompDTO.setDisplayId(displayDTO.getDisplayId());
					corrCompDTOList.add(corrCompDTO);
					System.out
							.println("CorrugateComponentEditorGrid:save ..[CountryName,CountryId, displayId]=..["
									+ corrCompDTO.getCountryName()
									+ ", "
									+ corrCompDTO.getCountryId()
									+ ","
									+ displayDTO.getDisplayId() + "]");
				}
			} catch (Exception ex) {
				System.out.println("save ..ERROR.." + ex.getMessage());
			}
			final MessageBox box = MessageBox.wait("Progress",
					"Saving corrugate component, please wait...", "Saving...");
			AppService.App.getInstance().saveCorrugateComponentInfo(
					corrCompDTOList,
					displayDTO,
					new Service.SaveCorrugateComponentDTOList(
							getCorrugateComponentEditorGrid(), componentGrid,
							displayDTO, box, btn));
		} catch (Exception ex) {
			System.out.println("======CorrugateComponentEditorGrid:save error "
					+ ex.toString());
		}
	}

	public void copyCorrComponent(Button button) {
		try {
			CorrugateComponentsDetailDTO copiedComponent = null;
			List<CorrugateComponentsDetailDTO> corrugateCompList = new ArrayList<CorrugateComponentsDetailDTO>();
			copiedComponent = componentGrid.getSelectionModel()
					.getSelectedItem();
			copiedComponent.setCorrugateComponentId(null);// To create a new
															// record with the
															// copied row item.
			corrugateCompList.add(copiedComponent);
			final MessageBox box = MessageBox
					.wait("Progress",
							"Copying corrugate component, please wait...",
							"Copying...");
			AppService.App.getInstance().saveCorrugateComponentInfo(
					corrugateCompList,
					displayDTO,
					new Service.SaveCorrugateComponentDTOList(
							getCorrugateComponentEditorGrid(), componentGrid,
							displayDTO, box, button));
		} catch (Exception ex) {
			System.out
					.println("=======DisplayCostDataGroup:deleteKitComponent error "
							+ ex.toString());
		}
	}

	public void deleteCorrComponent() {
		try {
			if (componentGrid.getSelectionModel().getSelectedItem() != null) {
				CorrugateComponentsDetailDTO corrComponent = componentGrid
						.getSelectionModel().getSelectedItem();
				final MessageBox box = MessageBox.wait("Progress",
						"Deleting corrugate component data, please wait...",
						"Deleting...");
				AppService.App.getInstance().callCorrugateComponentDelete(
						corrComponent,
						new Service.DeleteCorrugateKitComponentInfo(
								getCorrugateComponentEditorGrid(),
								componentGrid, corrComponent, componentGrid
										.getStore(), box));
			}
		} catch (Exception ex) {
			System.out
					.println("=======DisplayCostDataGroup:deleteKitComponent error "
							+ ex.toString());
		}
	}

	public void showAllButtons() {
		btnDeleteComp.enable();
		btnAddComp.enable();
		setPermissions();
	}

	public void showOnlyUpdateButton() {
		btnDeleteComp.enable();
		btnAddComp.disable();
		setPermissions();
	}

	public void showOnlyCreateButton() {
		btnDeleteComp.disable();
		btnAddComp.enable();
		setPermissions();
	}

	public void rejectChanges() {
		corrCompStore.rejectChanges();
	}

	private void clearModel() {
	    getCorrugateInfoGroup().getCorrugateComponentInfo().clearModel();
		overrideDirty = false;
		setData("display", null);
	}

	public void forwardToDisplayProjectSelection() {
		clearModel();
		App.getVp().clear();
		App.getVp().add(new DisplayProjectSelection());
	}

	public void overrideDirty() {
		overrideDirty = true;
	}

	private boolean overrideDirty = false;

	public boolean isDirty() {
		System.out.println("CorrugateComponentEditorGrid:isDirty overrideDirty="+overrideDirty);
		if (overrideDirty == true)
			return false;
		return !different();
	}

	private boolean different() {
		boolean displayModelDirty = true;
		System.out.println("CorrugateComponentEditorGrid:different starts");
		try {
			/*if(!validateCorrCost()){
				return true;
			}*/
			if (getCorrugateInfoGroup().getCorrugateDTO() != null)
				displayModelDirty = getCorrugateInfoGroup()
						.getCorrugateDTO()
						.equalsCorrugateInfo(
								getCorrugateInfoGroup()
										.getCurrentCorrugateModelFromControls());
		} catch (Exception ex) {
			System.out.println("CostAnalysisInfo:different error "
					+ ex.toString());
		}
		System.out
				.println("CorrugateComponentEditorGrid:different displayDirty="
						+ displayModelDirty);
		System.out.println("CorrugateComponentEditorGrid:different ends");
		return displayModelDirty;
	}

	private final Listener<MessageBoxEvent> l = new Listener<MessageBoxEvent>() {
		public void handleEvent(MessageBoxEvent mbe) {
			Button btn = mbe.getButtonClicked();
			if (btn.getText().equals("Yes")) {
				save(btn, null, null);
				forwardToDisplayProjectSelection();
			}
			if (btn.getText().equals("No")) {
				forwardToDisplayProjectSelection();
			}
			if (btn.getText().equals("Cancel")) {
			}
		}
	};

	@Override
	public void save(Button btn, TabPanel panel, TabItem nextTab) {
		getCorrugateInfoGroup().saveCorrugate(displayDTO, null, null);
	}
}