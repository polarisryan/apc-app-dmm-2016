package com.averydennison.dmm.app.client.util;

public class Constants {
	public static final String DATE_PATTERN = "MM/dd/y";
	public static final String ADD_COMPONENT = "Add Component";
	public static final String DELETE_COMPONENT = "Delete Component";
	public static final String COPY_COMPONENT = "Copy Component";
	public static final String SAVE_CORRUGATE = "Save Corrugate";
	public static final String RETURN_TO_SELECTION = "Return to Selection";
	public static final String SAVE = "Save";
	public static final String EXPORT = "Export";
	public static final String RESET = "Reset";
	public static final String HEALTH = "health";
	public static final String BATTLE_FIELD = "battleField";
	public static final String REGION = "region";
	public static final String PHASE = "phase";
	public static final String TYPE = "type";
	public static final String SPONSOR = "sponsor";
	public static final String PROJECTNAME = "projectName";
	public static final String DELIMITER = ":";
	public static final String YES="yes";
	public static final String NO="no";
	public static final String VENDOR_NAME="<center>Vendor<br>Name</center>";
	public static final String VENDOR_PART_NO="<center>Vendor<br>Part #</center>";
	public static final String GLOVIA_PART_NO="<center>Glovia<br>Part #</center>";
	public static final String PART_DESCRIPTION="<center>Part/<br>Description</center>";
	public static final String BOARD_TEST="<center>Board<br>Test</center>";
	public static final String MATERIAL="<center>Material</center>";
	public static final String COUNTRY_OF_ORIGIN="<center>Country<br>of Origin</center";
	public static final String NUMBER_PER_DISPLAY="<center>Qty.Per<br> Display<center>";
	public static final String PIECE_PRICE="<center>Piece<br>Price<center>";
	public static final String TOTAL_COST="<center>Total<br>Cost<center>";
	public static final String TOTAL_QUANTITY="<center>Total<br>Quantity<center>";
	public static final String TOTAL="Total";
	public static final String DISPLAY_CORRUGATE="displayCorrugate";
	public static final String DISPLAY_COUNTRY_USA="United States";
	public static final String DISPLAY_COUNTRY_CAN="Canada";

	
}
