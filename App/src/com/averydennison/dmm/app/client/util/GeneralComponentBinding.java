package com.averydennison.dmm.app.client.util;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.user.client.Timer;

public abstract class GeneralComponentBinding {
	
	protected Component component;
	private Timer timer;
	private int interval = 500;
	private Listener<ComponentEvent> listener;
	private List<Button> buttons;
	
	public GeneralComponentBinding(Component component) {
		this.component = component;
		this.buttons = new ArrayList<Button>();
		this.timer = new Timer() {
			@Override
			public void run() {
				checkComponent();
			}
		};
		this.listener = new Listener<ComponentEvent>() {
			public void handleEvent(ComponentEvent be) {
				if (be.getType() == Events.Attach) {
					startMonitoring();
				} else if (be.getType() == Events.Detach) {
					stopMonitoring();
				}
			}
		};
		this.component.addListener(Events.Attach, this.listener);
		this.component.addListener(Events.Detach, this.listener);

		if (this.component.isAttached()) {
			startMonitoring();
		}

	}

	public void addButton(Button button) {
		buttons.add(button);
	}

	public int getInterval() {
		return interval;
	}

	public void removeButton(Button button) {
		buttons.remove(button);
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public void startMonitoring() {
		if (component.isAttached()) {
			timer.run();
			timer.scheduleRepeating(interval);
		}
	}

	public void stopMonitoring() {
		timer.cancel();
	}

	protected void checkComponent() {
		for (Button button : buttons) {
			boolean v = isButtonEnabled(button);
			if (v != button.isEnabled()) {
				button.setEnabled(v);
			}
		}
	}
	
	protected abstract boolean isButtonEnabled(Button button);


}
