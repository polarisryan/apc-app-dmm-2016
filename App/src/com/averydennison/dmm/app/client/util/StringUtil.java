package com.averydennison.dmm.app.client.util;

public class StringUtil {
    public static String format(String s){
        return removeNonPrintableCharacters(trimNewLines(s).trim());
    }
    public static native String trimNewLines(String s)/*-{
        return s.replace(new RegExp( "\\n", "g" ), "").replace(new RegExp("\\r","g"), "").replace(new RegExp("\\t", "g"), "");
    }-*/;
    
    public static native String removeNonAlphaNumeric(String s)/*-{
        return s.replace(new RegExp("[^a-zA-Z 0-9 .]", "g"), "");
    }-*/;
    //\x00-\x1F
    public static native String removeNonPrintableCharacters(String s)/*-{
        return s.replace(new RegExp("[\x00-\x1F]", "g"), "");
    }-*/;
}
