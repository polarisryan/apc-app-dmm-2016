package com.averydennison.dmm.app.client.util;


import com.google.gwt.event.dom.client.KeyCodes;

/**
 * User: Spart Arguello
 * Date: Nov 17, 2009
 * Time: 1:15:12 PM
 */
public class KeyUtil {
    /*
     * returns true if the key code is in fact a digit
     * either using the regular keys or number pad
     */
    public static boolean isDigit(int keyCode) {
        return keyCode >= 48 && keyCode <= 57
                ||
                keyCode >= 96 && keyCode <= 105;
    }

    public static boolean isNeeded(int keyCode) {
        return keyCode == KeyCodes.KEY_BACKSPACE || keyCode == KeyCodes.KEY_DELETE ||
                keyCode == KeyCodes.KEY_ENTER;
    }

    public static boolean isEnter(int keyCode) {
        return keyCode == KeyCodes.KEY_ENTER;
    }

    public static boolean isValidDateChar(int keyCode) {
        return keyCode >= 47 && keyCode <= 57;
    }
    
    public static boolean isValidCharForAmount(int keyCode) {
        return keyCode >= 48 && keyCode <= 57
                ||
                keyCode >= 96 && keyCode <= 105 || keyCode== 46;
    }
    
   
    
}
