package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplayScenarioDTO;
import com.averydennison.dmm.app.client.models.KitComponentDTO;
import com.extjs.gxt.ui.client.Style;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;

/**
 * User: Alvin Wong
 * Date: Dec 20, 2009
 * Time: 12:50:50 PM
 */
public class KitComponentInfoGrid extends LayoutContainer implements DirtyInterface{
    public static final String ADD_COMPONENT = "Add Component";
    public static final String EDIT_COMPONENT = "Edit Component";
    public static final String VIEW_COMPONENT = "View Component";
    public static final String DELETE_COMPONENT = "Delete Component";
    public static final String SAVE_COMPONENTS = "Save Components";
    public static final int PROMOVER_COL_INDEX=5;
    public static final int I2_COL_INDEX=6;

    public static final String SAVE = "Save";
    public static final String SEARCH = "Search";
    public static final String RETURN_TO_SELECTION = "Return to Selection";

    private Window window = new Window();
    private DisplayDTO displayDTO = new DisplayDTO();
    private DisplayScenarioDTO displayScenarioDTO ;

    private KitComponentInfo parent;
    private DisplayTabs displayTabs;
    private Boolean readOnly;
    
    private CheckBox skuMixFinalCheckBox = new CheckBox();
    private EditorGrid<KitComponentDTO> editorGrid;
    ListStore<KitComponentDTO> store = new ListStore<KitComponentDTO>();

    private SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
        public void componentSelected(ButtonEvent ce) {
            Button button = (Button) ce.getComponent();
            displayDTO=displayTabs.getDisplayModel();
            if (!button.getText().equals(RETURN_TO_SELECTION)) {
            	if (displayDTO.getDisplayId() == null) {
       		 		MessageBox.alert("Validation Error", "There is no display defined " , null);
       		 		return ;
            	}
            }
            if (button.getText().equals(ADD_COMPONENT)) {
                window.show();
            } else if (button.getText().equals(EDIT_COMPONENT) || button.getText().equals(VIEW_COMPONENT)) {
                if (editorGrid.getSelectionModel().getSelectedItem() != null) {
                    button.disable();
                    Long selectedIdx = 0L;
                    Long kitComponentId = editorGrid.getSelectionModel().getSelectedItem().getKitComponentId();
                    List<Long> kitComponentIds = new LinkedList<Long>();
                    List<KitComponentDTO> kitComponentList = editorGrid.getStore().getModels();
                    for (int i = 0; i < kitComponentList.size(); i++) {
                        Long id = kitComponentList.get(i).getKitComponentId();
                        if (id == kitComponentId) selectedIdx = new Long(i);
                        kitComponentIds.add(id);
                    }
                    final MessageBox loadProgressBar = MessageBox.wait("Progress", "Loading kit component...", "Loading..."); 
                    AppService.App.getInstance().getKitComponentDTO(kitComponentId, displayDTO,getDefaultScenarioData(),
                            new Service.LoadKitComponent(displayDTO,getDefaultScenarioData(), kitComponentIds, selectedIdx, App.getVp(), button, readOnly, loadProgressBar,false,null));
                } else {
                    //Info.display("Select Kit Component", "Then press Edit");
                }
            } else if (button.getText().equals(SAVE_COMPONENTS)) {
            	 save( button,null,null);
            } else if (button.getText().equals(DELETE_COMPONENT)) {
                if (editorGrid.getSelectionModel().getSelectedItem() != null) {
                	Listener<MessageBoxEvent>cb = new Listener<MessageBoxEvent>() {
                		public void handleEvent(MessageBoxEvent mbe) {
                			String id = mbe.getButtonClicked().getItemId();
                			if (Dialog.YES == id) {
                				btnDelete.disable();
                                KitComponentDTO kitComponent = editorGrid.getSelectionModel().getSelectedItem();
                                AppService.App.getInstance().deleteKitComponent(kitComponent, new Service.DeleteKitComponents(kitComponent, editorGrid.getStore(), btnDelete));
                                editorGrid.getSelectionModel().deselectAll();                				
                			}
                		}
                	};
                	MessageBox.confirm("Delete?", "Are you sure you want to delete this record?", cb);
                } else {
                    //Info.display("Select a Kit Component", "Then press Delete");
                }
            } else if (button.getText().equals(SAVE)) {
                button.disable();
                DisplayDTO dto = displayTabs.getData("display");
                dto.setSkuMixFinalFlg(true);
                displayTabs.setDisplayModel(dto);
                button.enable();
            } else if (button.getText().equals(RETURN_TO_SELECTION)) {
                /*App.getVp().clear();
                App.getVp().add(new DisplayProjectSelection());*/
            	
            	if(isDirty()){
            		String title = "You have unsaved information!";
            		String msg = "Would you like to save?"; 		           			
            		MessageBox box = new MessageBox();
                    box.setButtons(MessageBox.YESNOCANCEL);
                    box.setIcon(MessageBox.QUESTION);
                    box.setTitle("Save Changes?");
                    box.addCallback(l);
                    box.setTitle(title);
                    box.setMessage(msg);
                    box.show();
            	}else{
            		forwardToDisplayProjectSelection();
            	}        
            } else {
                //Info.display("Click Event", "The '{0}' button was clicked.", button.getText());
            }
        }
    };

    private final Button btnAdd = new Button(ADD_COMPONENT, listener);
    private final Button btnEdit = new Button(EDIT_COMPONENT, listener);
    private final Button btnDelete = new Button(DELETE_COMPONENT, listener);
    private final Button btnSave = new Button(SAVE_COMPONENTS, listener);
    private final Button btnReturn = new Button(RETURN_TO_SELECTION, listener);
   
    private void setPermissions(){
    	if(!App.isAuthorized("KitComponentInfoGrid.ADD")) btnAdd.disable();
    	if(!App.isAuthorized("KitComponentInfoGrid.EDIT")) btnEdit.disable();
    	if(!App.isAuthorized("KitComponentInfoGrid.DELETE")) btnDelete.disable();
    	if(!App.isAuthorized("KitComponentInfoGrid.SAVE")) btnSave.disable();
    }
    
 private boolean overrideDirty = false;
    
    public void overrideDirty(){
        this.overrideDirty = true;
    }
    
    public boolean isDirty(){
    	
        if(overrideDirty==true) return false;        
    	return different();
    }
    
    private boolean different(){
    	return (editorGrid.getStore().getModifiedRecords().size()>0);
    }
    
    public void reload(DisplayDTO display,List<DisplayScenarioDTO> displayScenarioDTOs) {
    	if (display.getDisplayId() != null ) {
    		if( displayScenarioDTOs!=null && displayScenarioDTOs.size()>0){
    			setDisplayScenarioDTO(displayScenarioDTOs.get(0));
    			//MessageBox.alert("KitComponentInfoGrid", " scenario Found for displayId="+display.getDisplayId() + " is scenarioId= "+displayScenarioDTOs.get(0).getDisplayScenarioId(),null);
    			AppService.App.getInstance().getKitComponents(display,displayScenarioDTOs.get(0), new Service.LoadKitComponents(store));
    		}else{
    			//MessageBox.alert("KitComponentInfoGrid", "No scenario Found for displayId="+display.getDisplayId(),null);
    			AppService.App.getInstance().getKitComponents(display,null, new Service.LoadKitComponents(store));
    		}
    	}
    }
    
    
    public KitComponentInfoGrid(DisplayDTO display, KitComponentInfo kitComponentInfo, Boolean access) {

        this.displayDTO = display;
        this.parent = kitComponentInfo;
        this.displayTabs = parent.getDisplayTabs();
        this.readOnly = access;
        setLayout(new FlowLayout(10));
        setupSelectionDialog();
        int x = 120;
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        ColumnConfig column = new ColumnConfig();
        column.setId(KitComponentDTO.SKU);
        column.setHeader("SKU");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);
        CheckColumnConfig pimCol = new CheckColumnConfig(KitComponentDTO.SOURCE_PIM_FLG, "PIM", 55);
        pimCol.setWidth(x / 3);
        configs.add(pimCol);
        column = new ColumnConfig();
        column.setId(KitComponentDTO.PRODUCT_NAME);
        column.setHeader("Description");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(KitComponentDTO.BU_DESC);
        column.setHeader("BU");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x - 25);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(KitComponentDTO.REG_CASE_PACK);
        column.setHeader("Ret Pk/In Ctn");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x - 25);
        configs.add(column);
        
       
        CheckColumnConfig pvCol = new CheckColumnConfig(KitComponentDTO.PROMO_FLG, "PV", 55);
        pvCol.setWidth(x / 3);
        configs.add(pvCol);
        
        //Editable checkbox for mass update
        CheckColumnConfig i2Col = new CheckColumnConfig(KitComponentDTO.I2_FORECAST_FLG, "I2", 55);
        if(!readOnly){
        	CellEditor checkBoxEditor3 = new CellEditor(new CheckBox());
        	i2Col.setEditor(checkBoxEditor3);
        }
        i2Col.setWidth(x / 3);
        configs.add(i2Col);

        column = new ColumnConfig();
        column.setId(KitComponentDTO.BREAK_CASE);
        column.setHeader("Ver Code Setup");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(KitComponentDTO.KIT_VERSION_CODE);
        column.setHeader("Kit Version Code");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x / 2);
        configs.add(column);

        CheckColumnConfig pbcCol = new CheckColumnConfig(KitComponentDTO.PLANT_BREAK_CASE_FLG, "PBC BOM", 55);
        pbcCol.setWidth(x / 3);
        configs.add(pbcCol);

        column = new ColumnConfig();
        column.setId(KitComponentDTO.PBC_VERSION);
        column.setHeader("PBC Version");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(KitComponentDTO.QTY_PER_FACING);
        column.setHeader("Qty Per Facing");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x / 2);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(KitComponentDTO.NUMBER_FACINGS);
        column.setHeader("# Facings");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x / 2);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(KitComponentDTO.QTY_PER_DISPLAY);
        column.setHeader("Qty Per Display");
        column.setWidth(x / 2);
        column.setAlignment(HorizontalAlignment.LEFT);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(KitComponentDTO.INVENTORY_REQUIREMENT);
        column.setHeader("Inv Req");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x / 2);
      
        configs.add(column);

        ColumnModel cm = new ColumnModel(configs);
        // call server
        
        ContentPanel cp = new ContentPanel();
        cp.setBodyBorder(false);
        cp.setHeaderVisible(false);
        cp.setButtonAlign(HorizontalAlignment.CENTER);
        cp.setLayout(new FlowLayout());
        cp.setSize(1190, 350);

        skuMixFinalCheckBox.setBoxLabel("SKU Mix Final");
        skuMixFinalCheckBox.setWidth(100);
        skuMixFinalCheckBox.setHeight(30);
        skuMixFinalCheckBox.setValue(displayDTO.isSkuMixFinalFlg());
        skuMixFinalCheckBox.addListener(Events.Change, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
        		if (displayDTO.getDisplayId() != null) {
            		displayDTO=displayTabs.getDisplayModel();
	       		    Boolean skuMix = skuMixFinalCheckBox.getValue();
	                displayDTO.setSkuMixFinalFlg(skuMix);
	                //displayTabs.setDisplayModel(displayDTO);
	                parent.getDisplayTabHeader().setSkuMixFinal(skuMix);
	                if (skuMix) {
	                    btnAdd.disable();
	                    btnEdit.disable();
	                    btnDelete.disable();
	                    btnSave.disable();
	                } else {
	                    btnAdd.enable();
	                    btnEdit.enable();
	                    btnDelete.enable();
	                    btnSave.enable();
	                }
	                final MessageBox box = MessageBox.wait("Progress",
	                        "Saving display general info, please wait...", "Saving...");
	                AppService.App.getInstance().saveDisplayDTO(displayDTO,
	                        new Service.SaveDisplayDTO(displayDTO, parent.getDisplayTabHeader(), new Button(), box));
	           }
            }
        });
        cp.add(skuMixFinalCheckBox);

        GridSelectionModel<KitComponentDTO> gridSelection = new GridSelectionModel<KitComponentDTO>();
        gridSelection.setSelectionMode(Style.SelectionMode.SINGLE);
        
        editorGrid = new EditorGrid<KitComponentDTO>(store, cm);
        editorGrid.setColumnResize(true);
        editorGrid.setStyleAttribute("borderTop", "none");
        editorGrid.setStripeRows(true);
        editorGrid.setBorders(true);
        editorGrid.setHeight(270);
        editorGrid.setSelectionModel(gridSelection);
        editorGrid.addListener(Events.RowDoubleClick, new Listener<GridEvent<ModelData>>() {
          	public void handleEvent(GridEvent<ModelData> be) {
          		editorGrid.disable();
                Long selectedIdx = 0L;
                Long kitComponentId = editorGrid.getSelectionModel().getSelectedItem().getKitComponentId();
                List<Long> kitComponentIds = new LinkedList<Long>();
                List<KitComponentDTO> kitComponentList = editorGrid.getStore().getModels();
                displayDTO=displayTabs.getDisplayModel();
                for (int i = 0; i < kitComponentList.size(); i++) {
                    Long id = kitComponentList.get(i).getKitComponentId();
                    if (id == kitComponentId) selectedIdx = new Long(i);
                    kitComponentIds.add(id);
                }
                final MessageBox loadProgressBar = MessageBox.wait("Progress", "Loading kit component...", "Loading..."); 
                AppService.App.getInstance().getKitComponentDTO(kitComponentId, displayDTO,getDefaultScenarioData(),
                        new Service.LoadKitComponent(displayDTO,getDefaultScenarioData(), kitComponentIds, selectedIdx, App.getVp(), btnEdit, readOnly, loadProgressBar,false, null));

          	}
        });
        editorGrid.addListener(Events.CellMouseDown, new Listener<GridEvent<ModelData>>() {
	       	public void handleEvent(GridEvent<ModelData> be) {
	       		int selectedRow = be.getRowIndex();
	       		int selectedCol = be.getColIndex();
	       		if((selectedRow > -1) && ( selectedCol==I2_COL_INDEX) && !readOnly){
	       			btnSave.enable();
	       		}
	       }
        });
        cp.add(editorGrid);
        btnSave.disable();
        cp.addButton(btnAdd);
        cp.addButton(btnEdit);
        cp.addButton(btnDelete);
        btnSave.setToolTip("Save I2 changes");
        cp.addButton(btnSave);
        cp.addButton(btnReturn);
        setPermissions();
        if (readOnly) {
        	btnAdd.disable();
        	btnDelete.disable();
        	btnEdit.setText(VIEW_COMPONENT);
        	btnSave.disable();
        }
        add(cp);
    }

    
    public DisplayTabs getDisplayTabs() {
        return displayTabs;
    }

    public void setDisplayTabs(DisplayTabs displayTabs) {
        this.displayTabs = displayTabs;
    }

    public DisplayScenarioDTO getDefaultScenarioData( ){
    	if(getDisplayScenarioDTO()!=null){
    		return getDisplayScenarioDTO();
    	}
    	displayDTO = displayTabs.getDisplayModel();
		DisplayScenarioDTO dScenarioDTO = new DisplayScenarioDTO();
		dScenarioDTO.setScenarioNum(new Long(1));
		dScenarioDTO.setActualQty(displayDTO.getActualQty());
		dScenarioDTO.setDisplayId(displayDTO.getDisplayId());
		dScenarioDTO.setDisplaySalesRepId(displayDTO.getDisplaySalesRepId());
		dScenarioDTO.setManufacturerId(displayDTO.getManufacturerId());
		dScenarioDTO.setPackoutVendorId(displayDTO.getPackoutVendorId());
		dScenarioDTO.setPromoPeriodId(displayDTO.getPromoPeriodId());
		dScenarioDTO.setPromoYear(displayDTO.getPromoYear());
		dScenarioDTO.setQtyForecast(displayDTO.getQtyForecast());
		dScenarioDTO.setSku(displayDTO.getSku());
		dScenarioDTO.setScenariodStatusId(displayDTO.getStatusId());
		dScenarioDTO.setDtCreated(new Date());
		dScenarioDTO.setDisplayScenarioId(null);
		dScenarioDTO.setUserCreated(App.getUser().getUsername());
		return dScenarioDTO;
	}
    
    private void setupSelectionDialog() {
        window.setSize(675, 425);
        window.setPlain(true);
        window.setHeading("Product SKU Selection");
        window.setLayout(new FitLayout());


        FormPanel panel = new FormPanel();
        panel.setLabelAlign(FormPanel.LabelAlign.TOP);
        panel.setHeaderVisible(false);

        // setup layout containers
        LayoutContainer top = new LayoutContainer();
        top.setLayout(new ColumnLayout());
        LayoutContainer bottom = new LayoutContainer();
        bottom.setLayout(new ColumnLayout());

        LayoutContainer topLeft = new LayoutContainer();
        LayoutContainer topRight = new LayoutContainer();

        // setup form layouts
        FormLayout topLeftFormLayout = new FormLayout();
        topLeftFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        topLeft.setStyleAttribute("padding", "5px");
        topLeft.setLayout(topLeftFormLayout);

        FormLayout topRightFormLayout = new FormLayout();
        topRightFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        topRight.setStyleAttribute("padding", "5px");
        topRight.setLayout(topRightFormLayout);

        FormLayout bottomFormLayout = new FormLayout();
        bottomFormLayout.setLabelAlign(FormPanel.LabelAlign.TOP);
        bottom.setStyleAttribute("padding", "5px");
        bottom.setLayout(bottomFormLayout);

        top.add(topLeft, new ColumnData(210));
        top.add(topRight, new ColumnData(300));
        panel.add(top);
        panel.add(bottom);

        final ProductSKUSelectionResultsGrid productResults = new ProductSKUSelectionResultsGrid(this,window, displayDTO,null, 0L,null);
        bottom.add(productResults);

        final Button btn;
        final TextField<String> skuSearchString = new TextField<String>();
        skuSearchString.setFieldLabel("SKU Search String");
        skuSearchString.setTabIndex(1);
        KeyListener skuListener = new KeyListener() {
            @Override
            public void componentKeyPress(ComponentEvent event) {
                if (event.getKeyCode() == 13) {
                    String sku = skuSearchString.getValue();
                    Long maxSeqNum = 0L;
                    List<Long> kitComponentIds = new LinkedList<Long>();
                    List<KitComponentDTO> kitComponentList = editorGrid.getStore().getModels();
                    for (int i = 0; i < kitComponentList.size(); i++) {
                        kitComponentIds.add(kitComponentList.get(i).getKitComponentId());
                        if (kitComponentList.get(i).getSequenceNum() > maxSeqNum)
                        	maxSeqNum = kitComponentList.get(i).getSequenceNum();
                    }
                    productResults.load(sku, kitComponentIds, maxSeqNum + 1);
                    event.cancelBubble();
                    event.preventDefault();
                    event.setCancelled(true);
                    return;
                } 
                else super.componentKeyPress(event);
            }

        };

        skuSearchString.addKeyListener(skuListener);
        topLeft.add(skuSearchString);

        SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
            public void componentSelected(ButtonEvent ce) {
                Button button = (Button) ce.getComponent();
                if (displayDTO.getDisplayId() == null) {
           		 	MessageBox.alert("Validation Error", "There is no display defined " , null);
           		 	return ;
            	}
                if (button.getText().equals(SEARCH)) {
                    String sku = skuSearchString.getValue();
                    Long maxSeqNum = 0L;
                    List<Long> kitComponentIds = new LinkedList<Long>();
                    List<KitComponentDTO> kitComponentList = editorGrid.getStore().getModels();
                    for (int i = 0; i < kitComponentList.size(); i++) {
                        kitComponentIds.add(kitComponentList.get(i).getKitComponentId());
                        if (kitComponentList.get(i).getSequenceNum() > maxSeqNum)
                        	maxSeqNum = kitComponentList.get(i).getSequenceNum();
                    }
                    productResults.load(sku, kitComponentIds, maxSeqNum + 1);
                }
            }
        };

        btn = new Button(SEARCH, listener);
        btn.setStyleAttribute("margin-top", "23px");
        btn.setTabIndex(2);
        topRight.add(btn);

        window.add(panel, new FitData(4));

    }
    
    public EditorGrid<KitComponentDTO> getEditorGrid() {
        return editorGrid;
    }

   private final Listener<MessageBoxEvent> l = new Listener<MessageBoxEvent>() {
        public void handleEvent(MessageBoxEvent mbe) {
            Button btn = mbe.getButtonClicked();
            if (btn.getText().equals("Yes")) {
                save(btn,null,null);
                forwardToDisplayProjectSelection();
            }
            if (btn.getText().equals("No")) {
                forwardToDisplayProjectSelection();
            }
            if (btn.getText().equals("Cancel")) {
            }
        }
    };
    
   private void clearModel() {
        overrideDirty = false;
        displayTabs.resetDisplayModel();
        setData("display", null);
    }
    
    private void forwardToDisplayProjectSelection(){
        clearModel();
        App.getVp().clear();
        App.getVp().add(new DisplayProjectSelection());
    }
    
	public void save(Button btn, TabPanel panel, TabItem tabItem) {
		if (displayDTO.getDisplayId() == null) {
   		 	MessageBox.alert("Validation Error", "There is no display defined " , null);
   		 	return;
    	}
		List<Record> mods = editorGrid.getStore().getModifiedRecords();
		for (int i = 0; i < mods.size(); i++) {
			KitComponentDTO kitDTO = (KitComponentDTO)((Record)mods.get(i)).getModel();
        	kitDTO.setDtLastChanged(new Date());
        	kitDTO.setUserLastChanged(App.getUser().getUsername());
        	if (kitDTO.getDtCreated() == null) {
        		kitDTO.setDtCreated(new Date());
        		kitDTO.setUserCreated(App.getUser().getUsername());
        	}
		}
		
 	    final MessageBox box = MessageBox.wait("Progress", "Saving kit component...", "Saving...");
        AppService.App.getInstance().saveKitComponentDTOList(editorGrid.getStore().getModels(),
                  new Service.SaveKitComponentDTOList(parent,editorGrid, box,btn,panel,tabItem));
   }

	public CheckBox getSkuMixFinalCheckBox() {
		return skuMixFinalCheckBox;
	}

	public void setSkuMixFinalCheckBox(CheckBox skuMixFinalCheckBox) {
		this.skuMixFinalCheckBox = skuMixFinalCheckBox;
	}

	public DisplayScenarioDTO getDisplayScenarioDTO() {
		return displayScenarioDTO;
	}

	public void setDisplayScenarioDTO(DisplayScenarioDTO displayScenarioDTO) {
		this.displayScenarioDTO = displayScenarioDTO;
	}
	
}