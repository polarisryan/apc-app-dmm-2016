package com.averydennison.dmm.app.client;

import com.averydennison.dmm.app.client.Service.SetCustomerCountryStore;
import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.CustomerCountryDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.ManufacturerCountryDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodCountryDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.averydennison.dmm.app.client.util.KeyUtil;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;

/**
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 4:02:06 PM
 */
public class FilterPanel extends LayoutContainer {
    public static final String ALL = "All";

    private CustomerCountryDTO selectedCustomer;
    private String selectedSku;
    private StructureDTO selectedStructure;
    private DisplaySalesRepDTO selectedProjectManager;
    private StatusDTO selectedStatus;
    private CountryDTO selectedCountry;
    private PromoPeriodCountryDTO selectedPromoPeriod;
    
    private ComboBox<CustomerCountryDTO> customer = new ComboBox<CustomerCountryDTO>();
    private final TextField<String> sku = new TextField<String>();
    private ComboBox<StructureDTO> structure = new ComboBox<StructureDTO>();
    private ComboBox<DisplaySalesRepDTO> projectManager = new ComboBox<DisplaySalesRepDTO>();
    private ComboBox<StatusDTO> status = new ComboBox<StatusDTO>();
    private ComboBox<PromoPeriodCountryDTO> promos = new ComboBox<PromoPeriodCountryDTO>();
    private ListStore<StatusDTO> statuses = new ListStore<StatusDTO>();
    private ListStore<CustomerCountryDTO> customers = new ListStore<CustomerCountryDTO>();
    private ListStore<StructureDTO> structures = new ListStore<StructureDTO>();
    private ListStore<CountryDTO> countries = new ListStore<CountryDTO>();
    private ListStore<DisplaySalesRepDTO> displaySalesReps = new ListStore<DisplaySalesRepDTO>();
    private ListStore<PromoPeriodCountryDTO> promoPeriods = new ListStore<PromoPeriodCountryDTO>();
    private ListStore<ManufacturerCountryDTO> manufacturers = new ListStore<ManufacturerCountryDTO>();
    
    
    private ComboBox<CountryDTO> country = new ComboBox<CountryDTO>();
    LabelField spaceLabel ;
    LabelField spaceStructureLabel ;
    public FilterPanel() {
        setLayout(new FlowLayout(10));
        
        ButtonBar toolBar = new ButtonBar();
        toolBar.setSize(1230, -1);
        if(App.getCustomerFilter()!=null) customer.setValue(App.getCustomerFilter());
        if(App.getSkuFilter()!=null) sku.setValue(App.getSkuFilter());
        if(App.getStructureFilter()!=null) structure.setValue(App.getStructureFilter());
        if(App.getManagerFilter()!=null) projectManager.setValue(App.getManagerFilter());
        if(App.getStatusFilter()!=null) status.setValue(App.getStatusFilter());
        if(App.getPromoPeriodFilter()!=null) promos.setValue(App.getPromoPeriodFilter());
        if(App.getCountryFilter()!=null) country.setValue(App.getCountryFilter());
        
        StatusDTO allStatusFilterItem = new StatusDTO();
        allStatusFilterItem.setStatusName(ALL);
        statuses.add(allStatusFilterItem);
        Service service = new Service();
        service.setStatusStore(statuses);
        
        CustomerCountryDTO allCustomerFilterItem = new CustomerCountryDTO();
        allCustomerFilterItem.setCustomerName(ALL);
        customers.add(allCustomerFilterItem);
        service.setCustomerCountryStore(null, customers);
       // AppService.App.getInstance().getCustomerCountryDTOs(new SetCustomerCountryStore(null, customers));

        StructureDTO allStructureFilterItem = new StructureDTO();
        allStructureFilterItem.setStructureName(ALL);
        structures.add(allStructureFilterItem);
        AppService.App.getInstance().getStructureDTOs(new Service.SetStructuresStore(structures));

        DisplaySalesRepDTO allDisplaySalesRep = new DisplaySalesRepDTO();
        allDisplaySalesRep.setFullName(ALL);
        displaySalesReps.add(allDisplaySalesRep);
        AppService.App.getInstance().getDisplaySalesRepDTOs(new Service.SetProjectManagerStore(displaySalesReps));
        
        PromoPeriodCountryDTO allPromoPeriod = new PromoPeriodCountryDTO();
        allPromoPeriod.setPromoPeriodName(ALL);
        promoPeriods.add(allPromoPeriod);
        AppService.App.getInstance().getPromoPeriodCountryDTOs(new Service.SetPromoPeriodsCountryStore(null,promoPeriods));

        //AppService.App.getInstance().getManufacturerCountryDTOs(new Service.SetManufacturersCountryStore(null,manufacturers));
        
        CountryDTO allCountryFilterItem = new CountryDTO();
        allCountryFilterItem.setCountryName(ALL);
        countries.add(allCountryFilterItem);
        AppService.App.getInstance().getCountryFlagDTOs(new Service.SetCountryStore(countries));
        
        int x = 125;
        
        customer.setEmptyText("Customer Filter...");
        customer.setDisplayField(CustomerCountryDTO.CUSTOMER_NAME);
        customer.setWidth(x);
        customer.setStore(customers);
        customer.setTypeAhead(true);
        customer.setTriggerAction(ComboBox.TriggerAction.ALL);
        customer.addSelectionChangedListener(new SelectionChangedListener<CustomerCountryDTO>() {
            public void selectionChanged(SelectionChangedEvent<CustomerCountryDTO> se) {
                setSelectedCustomer(se.getSelectedItem());
                App.setCustomerFilter(se.getSelectedItem());
                if(getSelectedCustomer()!=null && getSelectedCustomer().getCustomerName()!=null) 
                    selectionGrid.filterByCustomer(getSelectedCustomer().getCustomerName());
            }
        });
        
        spaceLabel = new LabelField();
        spaceLabel.setLabelSeparator(" ");
        spaceLabel.setFieldLabel(" ");
        spaceLabel.setWidth(40);
        toolBar.add(spaceLabel);
        toolBar.add(customer);
        toolBar.add(new SeparatorToolItem());

        sku.setEmptyText("SKU filter...");
        sku.setFieldLabel("SKU FILTER");
        sku.setAllowBlank(true);
        sku.setReadOnly(false);
        sku.setMinLength(1);
        sku.setMaxLength(15);
        sku.addListener(Events.KeyUp, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be) {
                if (!sku.validate()) return;
                int keyCode = be.getKeyCode();
                if (KeyUtil.isEnter(keyCode)) {
                    if (sku.getValue() == null || sku.getValue().equals("")) selectionGrid.filterBySku(ALL);
                    else selectionGrid.filterBySku(sku.getValue());
                }
            }
        });
        sku.addListener(Events.KeyDown, new Listener<FieldEvent>() {

            public void handleEvent(FieldEvent ke) {
                int keyCode = ke.getKeyCode();
                if (!KeyUtil.isDigit(keyCode) && !KeyUtil.isNeeded(keyCode) && !ke.isNavKeyPress()) {
                    ke.stopEvent();
                }
            }
        });
        toolBar.add(sku);
        toolBar.add(new SeparatorToolItem());

        spaceStructureLabel= new LabelField();
        spaceStructureLabel.setLabelSeparator(" ");
        spaceStructureLabel.setFieldLabel(" ");
        spaceStructureLabel.setWidth(70);
        toolBar.add(spaceStructureLabel);
        
        structure.setEmptyText("structure Filter...");
        structure.setDisplayField(StructureDTO.STRUCTURE_NAME);
        structure.setWidth(x);
        structure.setStore(structures);
        structure.setTypeAhead(true);
        structure.setTriggerAction(ComboBox.TriggerAction.ALL);
        structure.addSelectionChangedListener(new SelectionChangedListener<StructureDTO>() {
            public void selectionChanged(SelectionChangedEvent<StructureDTO> sce) {
                App.setStructureFilter(sce.getSelectedItem());
                selectionGrid.filterByStructure(sce.getSelectedItem().getStructureName());
            }
        });
        toolBar.add(structure);
        toolBar.add(new SeparatorToolItem());
        
        projectManager.setEmptyText("projectManager Filter...");
        projectManager.setDisplayField(DisplaySalesRepDTO.FULL_NAME);
        projectManager.setWidth(x);
        projectManager.setStore(displaySalesReps);
        projectManager.setTypeAhead(true);
        projectManager.setTriggerAction(ComboBox.TriggerAction.ALL);
        projectManager.addSelectionChangedListener(new SelectionChangedListener<DisplaySalesRepDTO>() {
            public void selectionChanged(SelectionChangedEvent<DisplaySalesRepDTO> sce) {
                App.setManagerFilter(sce.getSelectedItem());
                selectionGrid.filterByProjectManager(sce.getSelectedItem().getFullName());
            }
        });
        toolBar.add(projectManager);
        toolBar.add(new SeparatorToolItem());
        
        promos.setEmptyText("promos Filter...");
        promos.setDisplayField(PromoPeriodCountryDTO.PROMO_PERIOD_NAME);
        promos.setWidth(x+25);
        promos.setStore(promoPeriods);
        promos.setTypeAhead(true);
        promos.setTriggerAction(ComboBox.TriggerAction.ALL);
        promos.addSelectionChangedListener(new SelectionChangedListener<PromoPeriodCountryDTO>() {
            public void selectionChanged(SelectionChangedEvent<PromoPeriodCountryDTO> se) {
                setSelectedPromoPeriod(se.getSelectedItem());
                App.setPromoPeriodFilter(se.getSelectedItem());
                if(getSelectedPromoPeriod()!=null && getSelectedPromoPeriod().getPromoPeriodName()!=null) 
                    selectionGrid.filterByPromoPeriod(getSelectedPromoPeriod().getPromoPeriodName());
            }
        });
        toolBar.add(promos);
        toolBar.add(new SeparatorToolItem());
        
        status.setEmptyText("status Filter...");
        status.setDisplayField(StatusDTO.STATUS_NAME);
        status.setWidth(x-25);
        status.setStore(statuses);
        status.setTypeAhead(true);
        status.setTriggerAction(ComboBox.TriggerAction.ALL);
        status.addSelectionChangedListener(new SelectionChangedListener<StatusDTO>() {
            public void selectionChanged(SelectionChangedEvent<StatusDTO> sce) {
                App.setStatusFilter(sce.getSelectedItem());
                selectionGrid.filterByStatus(sce.getSelectedItem().getStatusName());
            }
        });
        toolBar.add(status);
        toolBar.add(new SeparatorToolItem());
        
        country.setEmptyText("country Filter...");
        country.setDisplayField(CountryDTO.COUNTRY_NAME);
        country.setWidth(x-20);
        country.setStore(countries);
        country.setTypeAhead(true);
        country.setTriggerAction(ComboBox.TriggerAction.ALL);
        country.addSelectionChangedListener(new SelectionChangedListener<CountryDTO>() {
            public void selectionChanged(SelectionChangedEvent<CountryDTO> sce) {
            	App.setCountryFilter(sce.getSelectedItem());
                selectionGrid.filterByCountry(sce.getSelectedItem().getCountryName());
            }
        });
        toolBar.add(country);
        add(toolBar);
    }

    public CustomerCountryDTO getSelectedCustomer() {
        return selectedCustomer;
    }

    public void setSelectedCustomer(CustomerCountryDTO selectedCustomer) {
        this.selectedCustomer = selectedCustomer;
    }

    public PromoPeriodCountryDTO getSelectedPromoPeriod() {
        return selectedPromoPeriod;
    }

    public void setSelectedPromoPeriod(PromoPeriodCountryDTO selectedPromoPeriod) {
        this.selectedPromoPeriod = selectedPromoPeriod;
    }
    
    public CountryDTO getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(CountryDTO selectedCountry) {
        this.selectedCountry = selectedCountry;
    }
    
    public String getSelectedSku() {
        return selectedSku;
    }

    public void setSelectedSku(String selectedSku) {
        this.selectedSku = selectedSku;
    }

    public StructureDTO getSelectedStructure() {
        return selectedStructure;
    }

    public void setSelectedStructure(StructureDTO selectedStructure) {
        this.selectedStructure = selectedStructure;
    }

    public DisplaySalesRepDTO getSelectedProjectManager() {
        return selectedProjectManager;
    }

    public void setSelectedProjectManager(DisplaySalesRepDTO selectedProjectManager) {
        this.selectedProjectManager = selectedProjectManager;
    }

    public StatusDTO getSelectedStatus() {
        return selectedStatus;
    }

    public void setSelectedStatus(StatusDTO selectedStatus) {
        this.selectedStatus = selectedStatus;
    }
    
    private SelectionGrid selectionGrid;

    public SelectionGrid getSelectionGrid() {
        return selectionGrid;
    }

    public void setSelectionGrid(SelectionGrid selectionGrid) {
        this.selectionGrid = selectionGrid;
    }
}