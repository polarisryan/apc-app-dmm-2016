package com.averydennison.dmm.app.client;






import java.util.Date;

import com.averydennison.dmm.app.client.models.DisplayCostDTO;
import com.averydennison.dmm.app.client.models.DisplayCostImageDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FormEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.form.FileUploadField;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Image;

public class FileUploadWindow extends LayoutContainer {
	private DisplayDTO display;
	public  Grid grid;
	private FormData formData;
	private Long displayId;
	private Long displayScenarioId;
	public static final String PREVIOUS = "Previous Image";
	public static final String NEXT ="Next Image";
	public LabelField lblImageCount = new LabelField();
	private static final int MAXIMUM_IMAGE_COUNT=5;
	public Button btnPreviousImage;
	public Button btnNextImage;
	public Button btnDelete;
	private Long selectedIdx=0L;
	private Long defaultSelectedIdx=1L;
	private Long totalIdx=0L;
	private CostAnalysisInfo costAnalysisInfo;

	public Long getTotalIdx() {
		return totalIdx;
	}
	public void setTotalIdx(Long totalIdx) {
		this.totalIdx = totalIdx;
	}
	public Long getSelectedIdx() {
		return selectedIdx;
	}
	public void setSelectedIdx(Long selectedIdx) {
		this.selectedIdx = selectedIdx;
	}
	public Long getDisplayId() {
		return displayId;
	}
	public void setDisplayId(Long displayId) {
		this.displayId = displayId;
	}
	public Long getDisplayScenarioId() {
		return displayScenarioId;
	}
	public void setDisplayScenarioId(Long displayScenarioId) {
		this.displayScenarioId = displayScenarioId;
	}
	public CostAnalysisInfo getCostAnalysisInfo() {
		return costAnalysisInfo;
	}
	public void setCostAnalysisInfo(CostAnalysisInfo costAnalysisInfo) {
		this.costAnalysisInfo = costAnalysisInfo;
	}
	LayoutContainer layoutFileUpload ;
	/*private Button btnUpload = new Button("Upload");
	private FileUploadField file = new FileUploadField();
	To FIx Enabling/Disabling buttons issue
	Date : 02-01-2011
	*/
	private Button btnUpload;
	private FileUploadField file;
/*	private Button btnUpload = new Button("Upload");
	private FileUploadField file = new FileUploadField();*/
	public LayoutContainer createFileUploadForm(){
		try{
		System.out.println("FileUploadWindow:createFileUploadForm starts");
		grid = new Grid(1,1);
		layoutFileUpload = new LayoutContainer();
		final FormPanel panel = new FormPanel();
		btnUpload = new Button("Upload");
		file = new FileUploadField();
		panel.setHeaderVisible(false);
		panel.setFrame(false);
		String url = GWT.getModuleBaseURL() + "upload";
		panel.setAction(url);
		panel.setEncoding(FormPanel.Encoding.MULTIPART);
		panel.setMethod(FormPanel.Method.POST);
		panel.setLayout(new ColumnLayout());
		panel.setSize(432, 80);
		if(getDisplayScenarioId()!=null) reloadDisplayCostImages(getDisplayId(), getDisplayScenarioId(),defaultSelectedIdx);
		layoutFileUpload.setLayout(new ColumnLayout());
		FieldSet imageUploadData = new FieldSet();
		FormLayout imageUploadDataFormLayout = new FormLayout();
		imageUploadData.setHeight(100);
		imageUploadData.setLayout(imageUploadDataFormLayout);
		imageUploadData.setHeading("Upload an Image or Rendering");
		file.setAutoValidate(true);
		file.setHideLabel(true);
		file.setName("fupload");
		file.setFieldLabel("File"); 
		file.setSize(380, 25);
		grid.setSize("415px", "305px");
		grid.getCellFormatter().setStyleName(0, 0, "tableCell-image");
		grid.getCellFormatter().setAlignment(0, 0, HasHorizontalAlignment.ALIGN_CENTER, HasVerticalAlignment.ALIGN_MIDDLE);
		btnUpload.setWidth(108);
		btnDelete = new Button("Delete");
		btnDelete.setWidth(108);
		btnUpload.setEnabled(false);
		btnUpload.setId("submit_button");
		btnDelete.setId("delete_button");
		formData = new FormData("95%");
		final Image image = new Image();
		panel.add(file);
		
		LabelField lblSpace= new LabelField("Upload");
		lblSpace.setFieldLabel("Upload");
		lblSpace.setWidth(30);
		lblSpace.setHeight(6);
		lblSpace.setVisible(false);
		ButtonBar buttonBar = new ButtonBar();
		buttonBar.setAlignment(HorizontalAlignment.CENTER);
		buttonBar.add(new FillToolItem());
		buttonBar.setSpacing(80);
		buttonBar.add(btnUpload);
		buttonBar.add(btnDelete);
		buttonBar.setAlignment(HorizontalAlignment.CENTER);
		panel.add(lblSpace);
		panel.add(buttonBar);
		file.addListener(Events.OnChange, new Listener<BaseEvent>() {
			/**
			 * Reacts on changes in the file to upload field: if no longer null, the OK button will be enabled.
			 * 
			 * @param BaseEvent
			 *          the change event indicating that the file to upload has been selected.
			 */
			public void handleEvent(BaseEvent BaseEvent) {
				btnUpload.setEnabled(true);
			}
		});
		panel.addListener(Events.Submit, new Listener<FormEvent>() {
			@Override
			public void handleEvent(FormEvent be) {
				final String filePath=file.getValue();
				if(filePath ==null || filePath.length()==0  ){
					MessageBox.alert("Warning", "No image has been selected. Try again...", null);
					be.setCancelled(true);
					return;
				}
				DisplayCostDTO copiedDisplayCostDTO =	getCostAnalysisInfo().getData("copiedDisplayCost");
				if(copiedDisplayCostDTO!=null && getDisplayScenarioId()!=null){
					MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
					be.setCancelled(true);
					return;
				}
				final MessageBox box =  MessageBox.wait("Progress", "Uploading image into the server. Please wait...", "Uploading...");
				Timer delay = new Timer() {
					public void run() {
						String fileName = filePath.substring(filePath.lastIndexOf("\\")+1);
						String baseURL = GWT.getHostPageBaseURL().replace("com.averydennison.dmm.app.App/", "");
						String imageFolder = AppService.App.getDmmConstants().imageUploadPath();
						String strEncodedUrlText = baseURL  +  imageFolder + "/" +  fileName;
						//image.setSize("485px", "185px");
						//image.setSize("385px", "185px");
						image.setSize("415px", "185px");
						image.setUrl(strEncodedUrlText);
						DisplayCostImageDTO displayCostImageDTO = new DisplayCostImageDTO();
						displayCostImageDTO.setDescription(fileName);
						displayCostImageDTO.setUrl(strEncodedUrlText);
						displayCostImageDTO.setCreateDate(new Date());
						displayCostImageDTO.setDisplayScenarioId(getDisplayScenarioId());
						displayCostImageDTO.setDisplayId(getDisplayId());
						displayCostImageDTO.setImageName(fileName);
						System.out.println("FileUploadWindow:run displayScnearioId=" + displayCostImageDTO.getDisplayScenarioId());
						if(displayCostImageDTO!=null && displayCostImageDTO.getDisplayScenarioId()!=null)
						{
							AppService.App.getInstance().saveCostAnalysisImage(displayCostImageDTO,new Service.SaveDisplayCostImageDTO(getFileUploadWindow(),store.getModels()));
						}else{
							MessageBox.alert("Validation Error", "No associated scenario. Please save the current scenario", null);
						}
						box.close();
					}};
					delay.schedule(3000);
			}
		});
		btnUpload.addSelectionListener(new SelectionListener<ButtonEvent>() {
			@Override
			public void componentSelected(ButtonEvent ce) {
				if (!panel.isValid()) {
					return;
				}
				if(getTotalIdx()>=MAXIMUM_IMAGE_COUNT){
					MessageBox.alert("Warning", "Image upload has been reached maximum.", null);
					return;
				}
				panel.submit();
			}
		});  
		btnDelete.addSelectionListener(new SelectionListener<ButtonEvent>() {
			@SuppressWarnings("deprecation")
			@Override
			public void componentSelected(ButtonEvent ce) {
				if(getSelectedIdx().intValue()>0){
					final MessageBox box = MessageBox.wait("Progress",
							"Deleting the image, please wait...", "Deleting...");
					AppService.App.getInstance().deleteCostAnalysisImage(store.getModels().get(getSelectedIdx().intValue()-1), new Service.DeleteDisplayCostImageDTO(getFileUploadWindow(),store.getModels(), box));
				}else{
					MessageBox.alert("Warning", "No image has been selected. Try again...", null);
				}
			}
		});
		imageUploadData.add(panel , formData);
		LayoutContainer imageButtons = new LayoutContainer();
		LayoutContainer imageButtonOne = new LayoutContainer();
		LayoutContainer imageButtonTwo = new LayoutContainer();
		LayoutContainer imageButtonThree = new LayoutContainer();
		btnPreviousImage = new Button(PREVIOUS, listener);
		btnNextImage = new Button(NEXT, listener);
		btnNextImage.setWidth(110);
		btnPreviousImage.setWidth(110);
		lblImageCount.setFieldLabel("<center>0 of 0</center>");
		lblImageCount.setWidth(6);
		imageButtons.setLayout(new ColumnLayout());
		imageButtons.setHeight(25);
		FormLayout imageButtonOneLayout = new FormLayout();
		imageButtonOne.setStyleAttribute("paddingLeft","42px");
		imageButtonOneLayout.setLabelAlign(FormPanel.LabelAlign.LEFT);
		imageButtonOne.setLayout(imageButtonOneLayout);
		imageButtonOne.add(btnPreviousImage, formData);
		FormLayout imageButtonOneLayoutTwo = new FormLayout();
		imageButtonOneLayoutTwo.setLabelAlign(FormPanel.LabelAlign.LEFT);
		imageButtonOneLayoutTwo.setDefaultWidth(6);
		imageButtonTwo.setLayout(imageButtonOneLayoutTwo);
		imageButtonTwo.setStyleAttribute("paddingLeft","42px");
		imageButtonTwo.add(lblImageCount, formData);
		FormLayout imageButtonOneLayoutThree = new FormLayout();
		imageButtonThree.setStyleAttribute("paddingLeft","55px");
		imageButtonOneLayoutThree.setLabelAlign(FormPanel.LabelAlign.LEFT);
		imageButtonThree.setLayout(imageButtonOneLayoutThree);
		imageButtonThree.add(btnNextImage, formData);
		imageButtons.add(imageButtonOne, new ColumnData(.290));  
		imageButtons.add(imageButtonTwo, new ColumnData(.250));  
		imageButtons.add(imageButtonThree, new ColumnData(.290));
		layoutFileUpload.add(grid);
		layoutFileUpload.add(imageButtons , new ColumnData(.990));  
		layoutFileUpload.add(imageUploadData, new ColumnData(.990)); 
		LabelField lblBottomSpace= new LabelField();
		lblBottomSpace.setFieldLabel(" ");
		lblBottomSpace.setHeight(20);
		layoutFileUpload.add(lblBottomSpace, new ColumnData(.990));
		}catch(Exception ex){
			System.out.println("FileUploadWindow:createFileUploadForm error "+ ex.toString());
		}
		System.out.println("FileUploadWindow:createFileUploadForm ends");
		return layoutFileUpload;
	}
	
	private SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
		public void componentSelected(ButtonEvent ce) {
			Button btn = (Button) ce.getComponent();
			Long idx=getSelectedIdx();
			if (btn.getText().equals(PREVIOUS)) {
				if (idx > defaultSelectedIdx) {
					setSelectedIdx(idx-1);
					AppService.App.getInstance().reloadDisplayCostImages(displayId,displayScenarioId,new Service.ReloadDisplayCostImage(store,getFileUploadWindow(),getCostAnalysisInfo(),(idx-1)));
				}
			}else if (btn.getText().equals(NEXT)) {
				if (idx < getTotalIdx()) {
					setSelectedIdx(idx+1);
					AppService.App.getInstance().reloadDisplayCostImages(displayId,displayScenarioId,new Service.ReloadDisplayCostImage(store,getFileUploadWindow(),getCostAnalysisInfo(),(idx+1)));
				}
			}
		}
	};
	ListStore<DisplayCostImageDTO> store = new ListStore<DisplayCostImageDTO>();
	
	public void reloadDisplayCostImages(Long displayId, Long displayScenarioId, Long defaulIndex){
		System.out.println("FileUploadWindow.reloadDisplayCostImages displayId="+displayId);
		System.out.println("FileUploadWindow.reloadDisplayCostImages displayScenarioId="+displayScenarioId);
		AppService.App.getInstance().reloadDisplayCostImages(displayId,displayScenarioId,new Service.ReloadDisplayCostImage(store,getFileUploadWindow(), getCostAnalysisInfo(), defaulIndex));
	}
	
	public FileUploadWindow getFileUploadWindow(){
		return this;
	}
	
	public void enableDisableButtons(boolean bValue){
		if(bValue){
			/*btnUpload.disable();
			file.disable();
			btnDelete.disable();*/
			layoutFileUpload.disable();
		}else{
		/*	btnUpload.enable();
			file.enable();
			btnDelete.enable();*/
			layoutFileUpload.enable();
			String filePath=file.getValue();
			if(filePath ==null || filePath.length()==0  ){
				btnUpload.disable();
			}
		}
	}
}
