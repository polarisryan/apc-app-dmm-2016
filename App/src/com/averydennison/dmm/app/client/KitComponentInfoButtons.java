package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;

/**
 * User: Spart Arguello
 * Date: Oct 23, 2009
 * Time: 12:43:23 PM
 */
public class KitComponentInfoButtons extends LayoutContainer {
    private ButtonBar buttonBar;
    private SelectionListener listener;

    public KitComponentInfoButtons() {
        SelectionListener listener = new SelectionListener<ComponentEvent>() {
            public void componentSelected(ComponentEvent ce) {
                Button btn = (Button) ce.getComponent();
                //Info.display("Click Event", "The '{0}' button was clicked.", btn.getText());
            }
        };
        buttonBar = new ButtonBar();
        buttonBar.setWidth(900);
        buttonBar.add(new FillToolItem());
        buttonBar.add(new Button("Add Component", listener));
        buttonBar.add(new Button("Edit Component", listener));
        buttonBar.add(new Button("Delete Component", listener));
        buttonBar.add(new Button("Save", listener));
        buttonBar.add(new Button("Return to Selection", listener));

        add(buttonBar);
    }
}
