package com.averydennison.dmm.app.client;

import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.button.Button;

/**
 * User: Spart Arguello
 * Date: Jan 4, 2010
 * Time: 2:33:46 PM
 */
public interface DirtyInterface {
    public boolean isDirty();
    public void overrideDirty();
    public void save(Button btn, TabPanel panel, TabItem nextTab);
}
