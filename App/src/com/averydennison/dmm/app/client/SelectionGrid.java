package com.averydennison.dmm.app.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.averydennison.dmm.app.client.models.CountryDTO;
import com.averydennison.dmm.app.client.models.CustomerDTO;
import com.averydennison.dmm.app.client.models.DisplayDTO;
import com.averydennison.dmm.app.client.models.DisplaySalesRepDTO;
import com.averydennison.dmm.app.client.models.PromoPeriodDTO;
import com.averydennison.dmm.app.client.models.SelectFieldToMassUpdateDTO;
import com.averydennison.dmm.app.client.models.StatusDTO;
import com.averydennison.dmm.app.client.models.StructureDTO;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ButtonBar;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.grid.CheckBoxSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;

/**
 * User: Spart Arguello
 * Date: Oct 14, 2009
 * Time: 4:15:50 PM
 */
public class SelectionGrid extends LayoutContainer {
    public static final String DATE_PATTERN = "MM/dd/y";
    public static final String VIEW = "View";
    public static final String EDIT = "Edit";
    public static final String CREATE = "Create";
    public static final String COPY = "Copy";
    public static final String PRINT = "Print";
    public static final String DELETE = "Delete";
    public static final String MASS_UPDATE = "Mass Update";
    public static final String CUSTOMER = "customer";
    public static final String SKU = "sku";
    public static final String STRUCTURE = "structure";
    public static final String COUNTRY = "country";
    public static final String DISPLAY_SALES_REP = "displaySalesRep";
    public static final String PROMO_PERIOD = "promoPeriod";
    public static final String STATUS = "status";
    public static final String DELIMITER = ":";
    public static final String YES="yes";
    public static final String NO="no";
    public static final String UPLOAD_PGM_PCT = "Upload Program %";

    private static final ListStore<DisplayDTO> displays = new ListStore<DisplayDTO>();
    private Button btnView;
    private Button btnCreate;
    private Button btnEdit;
    private Button btnCopy;
    private Button btnDelete;
    private Button btnMassUpdate;
    private Button btnUploadProgramPct;
    private SelectFieldToMassUpdateDTO selectFieldToMassUpdate;
    private MassUpdatePanel massUpdatePanel;
    private MassUpdateGrid massUpdateGrid;
    
    public SelectionGrid() {
    	
        initializeFilterProps();
        setLayout(new FlowLayout(11));
        int x = 110;
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        ColumnConfig column = new ColumnConfig();
        
        CheckBoxSelectionModel<DisplayDTO> sm = new CheckBoxSelectionModel<DisplayDTO>();
		configs.add(sm.getColumn());
        column = new ColumnConfig();
        column.setId(DisplayDTO.DISPLAY_ID);
        column.setHeader("ID");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x - 70);
        configs.add(column);
        
        column = new ColumnConfig();
        column.setId(DisplayDTO.CUSTOMER_BY_NAME);
        column.setHeader("Customer");

        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x - 5);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.SKU);
        column.setHeader("SKU");

        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.DESCRIPTION);
        column.setHeader("Description");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth((x+20)*2);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.STRUCTURE_BY_NAME);
        column.setHeader("Structure Type");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x+10);
        configs.add(column);

        column = new ColumnConfig();
        column.setId(DisplayDTO.DISPLAY_SALES_REP_BY_FULL_NAME);
        column.setHeader("Project Manager");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x-10);
        configs.add(column);
        
        column = new ColumnConfig();
        column.setId(DisplayDTO.PROMO_PERIOD_BY_PROMO_PERIOD_NAME);
        column.setHeader("Promo Period");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x+25);
        configs.add(column);
        
        column = new ColumnConfig();
        column.setId(DisplayDTO.PROMO_YEAR);
        column.setHeader("Promo Year");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth((x-10)/2);
        configs.add(column);
        
        column = new ColumnConfig();
        column.setId(DisplayDTO.STATUS_BY_NAME);
        column.setHeader("Status");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x-40 );
        configs.add(column);
        
        column = new ColumnConfig();
        column.setId(DisplayDTO.COUNTRY_BY_NAME);
        column.setHeader("Country");
        column.setAlignment(HorizontalAlignment.LEFT);
        column.setWidth(x-20 );
        configs.add(column);

        DateField dateField = new DateField();
        dateField.getPropertyEditor().setFormat(DateTimeFormat.getFormat(DATE_PATTERN));

    /*    column = new ColumnConfig();
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        column.setId(DisplayDTO.DISPLAY_SHIP_WAVES_BY_SHIP_DATE);
        column.setHeader("Must Arrive Date");
        column.setWidth(x-19 );
        column.setAlignment(HorizontalAlignment.LEFT);
        configs.add(column);*/

        column = new ColumnConfig();
        column.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        column.setId(DisplayDTO.DT_LAST_CHANGED);
        column.setHeader("Last Updated Date");
        column.setWidth(x-19 );
        column.setAlignment(HorizontalAlignment.LEFT);
        configs.add(column);

        ColumnModel cm = new ColumnModel(configs);
        loadDisplays();
        ContentPanel cp = new ContentPanel();
        cp.setHeading("Selection Screen");
        cp.setBodyBorder(false);
        cp.setButtonAlign(HorizontalAlignment.CENTER);
        cp.setFrame(true);
        cp.setLayout(new FitLayout());
        cp.setSize(1230, 450);

        final EditorGrid<DisplayDTO> grid = new EditorGrid<DisplayDTO>(displays, cm);
        grid.setColumnResize(true);
        grid.setStyleAttribute("borderTop", "none");
        grid.setStripeRows(true);
        grid.addListener(Events.RowDoubleClick, new Listener<GridEvent<DisplayDTO>>(){
			public void handleEvent(GridEvent<DisplayDTO> ge) {
                AppService.App.getInstance().getDisplayDTO(ge.getGrid().getSelectionModel().getSelectedItem(), 
                		new Service.LoadDisplay(App.getVp(), btnView, Boolean.TRUE, Boolean.FALSE));
			}
        });
        grid.addListener(Events.RowClick, new Listener<GridEvent<DisplayDTO>>(){
			public void handleEvent(GridEvent<DisplayDTO> ge) {
				 if(grid.getSelectionModel().getSelectedItems().size()==1){
					 showAllButtons();
				 }else  if(grid.getSelectionModel().getSelectedItems().size()>1){
					 showOnlyMassUpdateButton();
				 }else  if(grid.getSelectionModel().getSelectedItems().size()<1){
					 showOnlyCreateButton();
				 }
			}
        });
        grid.addListener(Events.HeaderClick, new Listener<GridEvent<DisplayDTO>>(){
			public void handleEvent(GridEvent<DisplayDTO> ge) {
				ColumnConfig c = grid.getColumnModel().getColumn(ge.getColIndex());
				if(ge.getColIndex()==0){
				 if(grid.getSelectionModel().getSelectedItems().size()==displays.getCount()){
					 showOnlyCreateButton();
				  }else {
				     showOnlyMassUpdateButton();
				 }
				}
			}
        });
        grid.setBorders(true);
        grid.setSelectionModel(sm);
        grid.addPlugin(sm);
        cp.add(grid);

        SelectionListener listener = new SelectionListener<ComponentEvent>() {
            public void componentSelected(ComponentEvent ce) {
                Button button = (Button) ce.getComponent();
                if (button.getText().equals(CREATE)) {
                	App.getVp().clear();
                    App.getVp().add(new DisplayTabs());
                	final MessageBox box = MessageBox.wait("Progress",
                    		"Loading display info, please wait...", "Loading...");
                	Timer t = new Timer() {
                	    public void run() {
    			              box.close();
    			        }
    			    };
    			    t.schedule(1000);                    
                } else if (button.getText().equals(EDIT)) {
                    if (grid.getSelectionModel().getSelectedItem() != null) {
                    	final MessageBox box = MessageBox.wait("Progress",
                        		"Loading display info, please wait...", "Loading...");
                    	Timer t = new Timer() {
                    	    public void run() {
        			              box.close();
        			        }
        			    };
        			    t.schedule(1000);
                        button.disable();
                        DisplayDTO dto = new DisplayDTO();
                        dto.setDisplayId(grid.getSelectionModel().getSelectedItem().getDisplayId());
                        AppService.App.getInstance().getDisplayDTO(dto, new Service.LoadDisplay(App.getVp(), button,
                                Boolean.FALSE, Boolean.FALSE));
                    }else{
                        //Info.display("Select display", "then press edit");
                    }
                } else if (button.getText().equals(COPY)) {
                    if (grid.getSelectionModel().getSelectedItem() != null) {
                        	Listener<MessageBoxEvent>cb = new Listener<MessageBoxEvent>() {
                            public void handleEvent(MessageBoxEvent mbe) {
                                String id = mbe.getButtonClicked().getItemId();
                                	if (Dialog.YES == id) {
                                		btnCopy.disable();
                                		DisplayDTO dto = new DisplayDTO();
                                		dto.setDisplayId(grid.getSelectionModel().getSelectedItem().getDisplayId());
                                		AppService.App.getInstance().getDisplayDTO(dto, new Service.LoadDisplay(App.getVp(), btnCopy,
                                		Boolean.FALSE, Boolean.TRUE));                              
                                	}
                            	}
                        	};
                        MessageBox.confirm("Copy?", "This will create a copy of this display. Do you wish to continue?", cb);
                    } 
               } else if (button.getText().equals(VIEW)) {
                    if (grid.getSelectionModel().getSelectedItem() != null) {
                        button.disable();
                        DisplayDTO dto = new DisplayDTO();
                        dto.setDisplayId(grid.getSelectionModel().getSelectedItem().getDisplayId());
                        AppService.App.getInstance().getDisplayDTO(dto, new Service.LoadDisplay(App.getVp(), button,
                                Boolean.TRUE, Boolean.FALSE));
                    } 
               } else if (button.getText().equals(PRINT)){
                    if(grid.getSelectionModel().getSelectedItem()!=null){
                        String displayid = grid.getSelectionModel().getSelectedItem().getDisplayId().toString();
                        Window.open(App.getServletPath() + "/pdfreport.rpc?id=" + displayid, "_blank", "");
                    }
               } else if (button.getText().equals(DELETE)){
                    if(grid.getSelectionModel().getSelectedItem()!=null){
                    	String title = "Delete?";
                        String msg = "This will delete the selected display permanently. Are you sure you would like to proceed?";                             
                        MessageBox box = new MessageBox();
                        box.setButtons(MessageBox.YESNO);
                        box.setIcon(MessageBox.QUESTION);
                        box.setTitle("Save Changes?");
                        box.addCallback(new Listener<MessageBoxEvent>() {
                            public void handleEvent(MessageBoxEvent mbe) {
                                Button btn = mbe.getButtonClicked();
                                if (btn.getText().equals("Yes")) {
                                    AppService.App.getInstance().callDisplayDeleteStoredProcedure(
                                            grid.getSelectionModel().getSelectedItem().getDisplayId(), 
                                            App.getUser().getUsername(), 
                                            new Service.CallDisplayDelete(getSelectionGrid(), btn));                                            
                                }
                                if (btn.getText().equals("No")) {
                                }
                               
                            }});
                        box.setTitle(title);
                        box.setMessage(msg);
                        box.show();
                    }
                }else if (button.getText().equals(MASS_UPDATE)){
                	 if(grid.getSelectionModel().getSelectedItems().size()>0){
                		 App.getVp().clear();
                		 App.getVp().add(new MassUpdateSelection(grid,getSelectionGrid())); 
                	 }else{
                		 
                	 }
                }else if (button.getText().equals(UPLOAD_PGM_PCT)){
                	List<DisplayDTO> displayDTOList= grid.getSelectionModel().getSelectedItems();
        	        if(displayDTOList!=null && displayDTOList.size()>0){
        	        	final MessageBox box = MessageBox.wait("Progress",
                        		"Uploading BU Program % Info, please wait...", "Uploading...");
        	        	 AppService.App.getInstance().saveProgramUploadBusForDisplays(displayDTOList, new Service.SaveProgramUploadBusForDisplays( box));
        	        }        
                }
            }
        };
        
        ButtonBar buttonBar = new ButtonBar();
        buttonBar.setSize(850, -1);
        buttonBar.setSpacing(10);
        
        btnView = new Button(VIEW, listener);
        btnEdit = new Button(EDIT, listener);
        btnCreate = new Button(CREATE, listener);
        btnCopy = new Button(COPY, listener);
        btnDelete = new Button(DELETE, listener);
        btnMassUpdate=new Button(MASS_UPDATE,listener);
        btnUploadProgramPct=new Button(UPLOAD_PGM_PCT, listener);
        showOnlyCreateButton();
        
        
        buttonBar.add(new FillToolItem());
        buttonBar.add(btnView);
        buttonBar.add(btnEdit);
        buttonBar.add(btnCreate);
        buttonBar.add(btnCopy);
        buttonBar.add(btnDelete);
        buttonBar.add(btnMassUpdate);
        buttonBar.add(btnUploadProgramPct);
        setPermissions();
        
        add(cp);
        add(buttonBar);

    }
    private SelectionGrid getSelectionGrid(){
        return this;
    }
    
    public SelectFieldToMassUpdateDTO getSelectFieldToMassUpdate(){
        return selectFieldToMassUpdate;
    }
    
    public void setSelectFieldToMassUpdate(SelectFieldToMassUpdateDTO selectFieldToMassUpdate){
        this.selectFieldToMassUpdate=selectFieldToMassUpdate;
    }
    
    public MassUpdatePanel getMassUpdatePanel(){
        return massUpdatePanel;
    }
    
    public void setMassUpdatePanel(MassUpdatePanel massUpdatePanel){
        this.massUpdatePanel=massUpdatePanel;
    }
    
    public MassUpdateGrid getMassUpdateGrid() {
		return massUpdateGrid;
	}
	public void setMassUpdateGrid(MassUpdateGrid massUpdateGrid) {
		this.massUpdateGrid = massUpdateGrid;
	}
	private void setPermissions(){
    	if(!App.isAuthorized("SelectionGrid.CREATE")) btnCreate.disable();
    	if(!App.isAuthorized("SelectionGrid.EDIT")) btnEdit.disable();
    	if(!App.isAuthorized("SelectionGrid.COPY")) btnCopy.disable();
    	if(!App.isAuthorized("SelectionGrid.VIEW")) btnView.disable();
    	if(!App.isAuthorized("SelectionGrid.DELETE")) btnDelete.disable();
    	if(!App.isAuthorized("SelectionGrid.MASSUPDATE")) btnMassUpdate.disable();
    	if(!App.isAuthorized("SelectionGrid.MASSUPDATE")) btnUploadProgramPct.disable();

    }
    public void filter(CustomerDTO customer, String sku, StructureDTO structure,
                              DisplaySalesRepDTO projectManager, StatusDTO status, PromoPeriodDTO promoPeriod, CountryDTO country) {
        String filters = customer.getCustomerName() + DELIMITER + sku + structure.getStructureName()
                + DELIMITER + projectManager.getFullName() + DELIMITER + status.getStatusName() + DELIMITER + promoPeriod.getPromoPeriodName() + DELIMITER + country.getCountryName();
        displays.applyFilters(filters);
    }

    public void filterByProjectManager(String projectManager) {
        filterProps.put(DISPLAY_SALES_REP, projectManager);
        displays.applyFilters(getFilterPropsString());
    }
    public void filterByCountry(String name) {
        filterProps.put(COUNTRY, name);
        displays.applyFilters(getFilterPropsString());
    }
    public void filterByCustomer(String name) {
        filterProps.put(CUSTOMER, name);
        displays.applyFilters(getFilterPropsString());
    }

    public void filterByStructure(String name) {
        filterProps.put(STRUCTURE, name);
        displays.applyFilters(getFilterPropsString());
    }

    public void filterByStatus(String status) {
        filterProps.put(STATUS, status);
        displays.applyFilters(getFilterPropsString());
    }

    public void filterBySku(String sku) {
        App.setSkuFilter(sku);
        filterProps.put(SKU, sku);
        displays.applyFilters(getFilterPropsString());
    }
    
    public void filterByPromoPeriod(String promoPeriod) {
        filterProps.put(PROMO_PERIOD, promoPeriod);
        displays.applyFilters(getFilterPropsString());
    }
    
    private Map<String, String> filterProps = new HashMap<String, String>();

    public Map<String, String> getFilterProps() {
        return filterProps;
    }
    public void setFilterProps(Map<String, String> filterProps) {
        this.filterProps = filterProps;
    }
    
    
   
	public void initializeFilterProps(){
        if(App.getFilters()==null || App.getFilters().length()==0){
            this.filterProps.put(CUSTOMER, FilterPanel.ALL);
            this.filterProps.put(SKU, FilterPanel.ALL);
            this.filterProps.put(STRUCTURE, FilterPanel.ALL);
            this.filterProps.put(DISPLAY_SALES_REP, FilterPanel.ALL);
            this.filterProps.put(STATUS, FilterPanel.ALL);
            this.filterProps.put(PROMO_PERIOD, FilterPanel.ALL);
            this.filterProps.put(COUNTRY, FilterPanel.ALL);
        }else{
            setFilterPropsString(App.getFilters());
        }
    }
    
    public String getFilterPropsString(){
        String result = "";
        result += filterProps.get(CUSTOMER) + DELIMITER;
        result += filterProps.get(SKU) + DELIMITER;
        result += filterProps.get(STRUCTURE) + DELIMITER;
        result += filterProps.get(DISPLAY_SALES_REP) + DELIMITER;
        result += filterProps.get(STATUS) + DELIMITER;
        result += filterProps.get(PROMO_PERIOD)+ DELIMITER;
        result += filterProps.get(COUNTRY);
        return result;
    }
    public void setFilterPropsString(String props){
        String[] filters = App.getFilters().split(SelectionGrid.DELIMITER);
        String customer = filters[0];
        String sku = filters[1];
        String structure = filters[2];
        String manager = filters[3];
        String status = filters[4];
        String promo = filters[5];
        String country = filters[6];
        this.filterProps.put(CUSTOMER, customer);
        this.filterProps.put(SKU, sku);
        this.filterProps.put(STRUCTURE, structure);
        this.filterProps.put(DISPLAY_SALES_REP, manager);
        this.filterProps.put(STATUS, status);
        this.filterProps.put(PROMO_PERIOD, promo);
        this.filterProps.put(COUNTRY, country);
    }
    public void loadDisplays(){
        MessageBox box = MessageBox.wait("Progress", "loading projects, please wait...", "Loading...");
        AppService.App.getInstance().getDisplays(new Service.LoadDisplays(displays, box));
    }
    public void showAllButtons(){
    	btnCopy.enable();
    	btnDelete.enable();
    	btnEdit.enable();
    	btnView.enable();
    	btnMassUpdate.enable();
    	btnUploadProgramPct.enable();
    	setPermissions();
    }
    public void showOnlyMassUpdateButton(){
    	btnCopy.disable();
    	btnDelete.disable();
    	btnEdit.disable();
    	btnView.disable();
    	btnMassUpdate.enable();
    	btnUploadProgramPct.enable();
    	setPermissions();

    }
    public void showOnlyCreateButton(){
    	btnCopy.disable();
    	btnDelete.disable();
    	btnEdit.disable();
    	btnView.disable();
    	btnMassUpdate.disable();
    	btnUploadProgramPct.disable();
    	btnCreate.enable();
    	setPermissions();
    }
}