package com.averydennison.dmm.mvc;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.averydennison.dmm.app.server.managers.PDFReportManager;
import com.itextpdf.text.Document;

public class ReportController implements Controller {
	private PDFReportManager pdfReportManager;
	
	@Override
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
	String displayId = (String)request.getParameter("id");
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Document document = pdfReportManager.createPDF(displayId, baos);
        document.close();
        
        // setting some response headers
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control",
            "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        // setting the content type
        response.setContentType("application/pdf");
        // the contentlength
        response.setContentLength(baos.size());
        // write ByteArrayOutputStream to the ServletOutputStream
        OutputStream os = response.getOutputStream();
        baos.writeTo(os);
        os.flush();
        os.close();
		
		
		return null;
	}
	public void setPdfReportManager(PDFReportManager pdfReportManager) {
		this.pdfReportManager = pdfReportManager;
	}


}
